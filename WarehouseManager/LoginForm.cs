﻿using AutoUpdaterDotNET;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using Microsoft.Exchange.WebServices.Auth.Validation;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Security;
using System.Reflection;
using System.Resources;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WarehouseManager.Helpers;
using WarehouseManager.Models;
using WarehouseManager.Repository;
using WarehouseManager.Requests;

namespace WarehouseManager
{
    public partial class LoginForm : DevExpress.XtraEditors.XtraForm
    {
        UserRepository userRepository;

        string directory;
        IniFile ini;
        string updateURL = "";

        public LoginForm()
        {
            InitializeComponent();
            userRepository = new UserRepository();
            this.StartPosition = FormStartPosition.CenterScreen;
            updateLanguage();
            popupSelectLanguage();
            this.KeyDown += new KeyEventHandler(Form1_KeyDown);

            System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate (object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };

            directory = AppDomain.CurrentDomain.BaseDirectory;
            ini = new IniFile(directory + "config.ini");
            updateURL = "http://" + ini.IniReadValue("config", "ip") + ":6666/update.xml";


            var currentDirectory = new DirectoryInfo(Application.StartupPath);
            string zipFile = currentDirectory + "\\Debug.zip";
            if (File.Exists(zipFile))
            {
                File.Delete(zipFile);
            }

            System.Timers.Timer timer = new System.Timers.Timer
            {
                Interval = 100,
                SynchronizingObject = this
            };
            timer.Elapsed += delegate
            {
                AutoUpdater.DownloadPath = Environment.CurrentDirectory;
                AutoUpdater.Mandatory = true;
                AutoUpdater.UpdateMode = AutoUpdaterDotNET.Mode.ForcedDownload;
                AutoUpdater.RunUpdateAsAdmin = true;
                var currentDirectory = new DirectoryInfo(Application.StartupPath);
                AutoUpdater.InstallationPath = currentDirectory.FullName;
                AutoUpdater.Start(updateURL);
            };
            timer.Start();
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                login();
            }
        }

        public async void login()
        {
            Helpers.Constant.BaseURL = "http://" + ini.IniReadValue("config", "ip") + ":6379/api/";
            string URI = Helpers.Constant.BaseURL+"user/login";

            string password = textPassword.Text;
            //string passwordSalt = "MTIzNDU2Nzg5MTIzNDU2Nw==";
            //var passwordHash = HashingHelper.HashUsingPbkdf2(password,passwordSalt);
            var username = textUserName.Text;
            LoginRequest loginRequest = new LoginRequest(username, password);


            using (var client = new HttpClient())
            {
                var content = new StringContent(JsonConvert.SerializeObject(loginRequest), Encoding.UTF8, "application/json");
                try
                {
                    using (var response = await client.PostAsync(URI, content))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            WMMessage.User = (await userRepository.getUserUnderUsernameAsync(textUserName.Text))[0];
                            openSplashForm();
                        }
                        else
                        {
                            MessageBox.Show("Login Fail " + response.StatusCode);
                            MessageBox.Show(await response.Content.ReadAsStringAsync());
                        }
                    }
                }
                catch (HttpRequestException e) { Console.WriteLine(e.InnerException.Message); }
            }
        }

        public void popupSelectLanguage()
        {
            barManager1.ForceInitialize();
            //DevExpress.XtraBars.PopupMenu menu = new DevExpress.XtraBars.PopupMenu();
            //menu.Manager = barManager1;
            //BarButtonItem itemVietnamese = new BarButtonItem(barManager1, "Tiếng Việt", 1);
            //BarButtonItem itemEnglish = new BarButtonItem(barManager1, "English", 2);
            //menu.AddItems(new BarItem[] { itemVietnamese, itemEnglish });
            //itemEnglish.Links[0].BeginGroup = true;
            barManager1.ItemClick += new ItemClickEventHandler(BarManager1_ItemClick);
            //barManager1.SetPopupContextMenu(dropDownButtonSelectLangue,menu);
        }
        private void BarManager1_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (e.Item.Caption.Equals("English") || e.Item.Caption.Equals("Tiếng Anh"))
            {
                WMMessage.msgLanguage = "en-US";
                ResourceManager resourceManager = new ResourceManager("WarehouseManager.Resource.en_local", Assembly.GetExecutingAssembly());
                updateLanguage();
                dropDownButtonSelectLangue.Text = e.Item.Caption;
            }
            else
            {
                WMMessage.msgLanguage = "vi-VN";
                ResourceManager resourceManager = new ResourceManager("WarehouseManager.Resource.vi_local", Assembly.GetExecutingAssembly());
                updateLanguage();
                dropDownButtonSelectLangue.Text = e.Item.Caption;
            }

            popupMenu1.HidePopup();
        }

        private void updateLanguage()
        {
            ResourceManager rm;
            String cultureName = WMMessage.msgLanguage;
            if (cultureName.Equals("vi-VN"))
            {
                rm = new
                   ResourceManager("WarehouseManager.Resource.vi_local", typeof(MainForm).Assembly);
            }
            else
            {
                rm = new
                    ResourceManager("WarehouseManager.Resource.en_local", typeof(MainForm).Assembly);
            }

           // LoginForm.ActiveForm.Text = rm.GetString("login_form");
            labelControl1.Text = rm.GetString("username");
            labelControl2.Text = rm.GetString("password");
            buttonLogin.Text = rm.GetString("login");
            dropDownButtonSelectLangue.Text = rm.GetString("choose_language");
            barStaticItem1.Caption = rm.GetString("english");
            barStaticItem2.Caption = rm.GetString("vietnamese");
        }


        public void openSplashForm()
        {
            this.Hide();
            SplashForm splashForm = new SplashForm();

            splashForm.Show();
        }

        private void buttonLogin_Click(object sender, EventArgs e)
        {
            login();
        }

        private void LoginForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
    }
}
