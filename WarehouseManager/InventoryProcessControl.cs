﻿using DevExpress.XtraBars.Docking2010;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraLayout.Filtering.Templates;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WarehouseManager.Models;
using WarehouseManager.Repository;
using webservice.Entities;
using LicenseContext = OfficeOpenXml.LicenseContext;
using DevExpress.XtraEditors.Repository;
using DevExpress.Utils.Extensions;

namespace WarehouseManager
{

    public partial class InventoryProcessControl : DevExpress.XtraEditors.XtraUserControl
    {
        BranchRepository branchRepository;
        WRHeaderRepository WRHeaderRepository;
        WRDetailRepository wRDetailRepository;
        IOHeaderRepository iOHeaderRepository;
        IODetailRepository iODetailRepository;
        WarehouseRepository warehouseRepository;
        InventoryRepository inventoryRepository;
        InventoryOfLastMonthsRepository inventoryOfLastMonthsRepository;
        String monnthYearNow = String.Format(DateTime.Now.ToString("MM/yyyy"), "MM/yyyy");
        String mCurrentDateTime = DateTime.Now.ToString("MM/yyyy");
        string mCurrentPeriod;
        string mLastPeriod;
        List<WRDetail> listWRDetailPublic = new List<WRDetail>();
        public InventoryProcessControl()
        {
            InitializeComponent();
            branchRepository = new BranchRepository();
            WRHeaderRepository = new WRHeaderRepository();
            wRDetailRepository = new WRDetailRepository();
            iOHeaderRepository = new IOHeaderRepository();
            iODetailRepository = new IODetailRepository();
            warehouseRepository = new WarehouseRepository();
            inventoryRepository = new InventoryRepository();
            inventoryOfLastMonthsRepository = new InventoryOfLastMonthsRepository();
            this.Load += InventoryProcess_Load;

        }

        private void InventoryProcess_Load(Object sender, EventArgs e)
        {
            getMonthYear();
            popupMenuSelectBranch();
            popupMenuSelectPeriod();
            popupMenuSelectWarehouse();
            windowsUIButtonPanel1.ButtonClick += button_Click;
            gridViewInventory.CustomColumnDisplayText += GridView_CustomColumnDisplayText;

        }

        private void GridView_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {

            if (e.Column == colNo)
            {
                int rowHandle = gridViewInventory.GetRowHandle(e.ListSourceRowIndex);
                if (rowHandle >= 0)
                {
                    e.DisplayText = (rowHandle + 1).ToString();
                }
            }
        }

        private async Task checDataChangeAsync()
        {
            if (await wRDetailRepository.CheckDataChanged() == true)
            {
                textEditMessage.Text = "Dữ liệu nhập thay đổi";
            }
            else if (await iODetailRepository.CheckDataChanged() == true)
            {
                textEditMessage.Text = "Dữ liệu xuất thay đổi";
            }
            else if (await wRDetailRepository.CheckDataChanged() == true && await iODetailRepository.CheckDataChanged() == true)
            {
                textEditMessage.Text = "Dữ liệu nhập xuất thay đổi";
            }
        }


        private void getMonthYear()
        {
            mCurrentPeriod = String.Format(mCurrentDateTime, "MM/yyyy");
            int mYear = int.Parse(mCurrentPeriod.Substring(3, 4));
            int mMonth = int.Parse(mCurrentPeriod.Substring(0, 2));
            int mLastMonth;
            int mLastYear;
            if (mMonth == 1)
            {
                mLastMonth = 12;
                mLastYear = mYear - 1;
            }
            else
            {
                mLastMonth = mMonth - 1;
                mLastYear = mYear;
            }
            //if (mMonth == 1)
            //{
            //    mLastMonth = 12;
            //    mLastYear = mYear - 1;
            //}
            //else
            //{
            //    mLastMonth = mMonth - 1;
            //    mLastYear = mYear;
            //}
            mLastPeriod = $"{mLastMonth:00}" + "/" + String.Format(mLastYear.ToString(), "####");
        }



        private async Task popupMenuSelectBranch()
        {
            List<Branch> branches = await branchRepository.getAllBranchAsync();
            foreach (Branch branch in branches)
            {
                comboBoxEditBranch.Properties.Items.Add(branch.BranchName);
            }
            comboBoxEditBranch.SelectedIndex = 1;
        }

        private async Task popupMenuSelectPeriod()
        {

            comboBoxEditProcessingPeriod.Properties.Items.Add(mLastPeriod);
            comboBoxEditProcessingPeriod.Properties.Items.Add(mCurrentPeriod);
            comboBoxEditProcessingPeriod.SelectedIndex = 0;
        }

        private async Task popupMenuSelectWarehouse()
        {
            List<Warehouse> warehouses = await warehouseRepository.getAll();
            foreach (Warehouse warehouse in warehouses)
            {
                comboBoxEditWarehouse.Properties.Items.Add(warehouse.WarehouseName);
            }
            comboBoxEditWarehouse.SelectedIndex = 0;
        }

        private void getTotalOpenStockQuantity()
        {
            Decimal total = 0;
            int i = 0;
            while (true)
            {
                int rowHandle = gridViewInventory.GetRowHandle(i);
                if (gridViewInventory.GetRowCellValue(rowHandle, "GoodsID") == null)
                {
                    break;
                }
                Decimal quantity = (Decimal)gridViewInventory.GetRowCellValue(rowHandle, "OpeningStockQuantity");
                total += quantity;
                i++;
            }
            textEditTotalOpenStockQuantity.Text = total.ToString("0.#####");
        }
        private void getTotalOpenStockAmount()
        {
            Decimal total = 0;
            int i = 0;
            while (true)
            {
                int rowHandle = gridViewInventory.GetRowHandle(i);
                if (gridViewInventory.GetRowCellValue(rowHandle, "GoodsID") == null)
                {
                    break;
                }
                Decimal amount = (Decimal)gridViewInventory.GetRowCellValue(rowHandle, "OpeningStockAmount");
                total += amount;
                i++;
            }
            textEditTotalOpenStockAmount.Text = total.ToString("0.#####");
        }

        private void getTotalReceiptQuantity()
        {
            Decimal total = 0;
            int i = 0;
            while (true)
            {
                int rowHandle = gridViewInventory.GetRowHandle(i);
                if (gridViewInventory.GetRowCellValue(rowHandle, "GoodsID") == null)
                {
                    break;
                }
                Decimal quantity = (Decimal)gridViewInventory.GetRowCellValue(rowHandle, "ReceiptQuantity");
                total += quantity;
                i++;
            }
            textEditTotalReceiptQuantity.Text = total.ToString("0.#####");
        }
        private void getTotalReceiptAmount()
        {
            Decimal total = 0;
            int i = 0;
            while (true)
            {
                int rowHandle = gridViewInventory.GetRowHandle(i);
                if (gridViewInventory.GetRowCellValue(rowHandle, "GoodsID") == null)
                {
                    break;
                }
                Decimal amount = (Decimal)gridViewInventory.GetRowCellValue(rowHandle, "ReceiptAmount");
                total += amount;
                i++;
            }
            textEditTotalReceiptAmount.Text = total.ToString("0.#####");
        }
        private void getTotalIssueQuantity()
        {
            Decimal total = 0;
            int i = 0;
            while (true)
            {
                int rowHandle = gridViewInventory.GetRowHandle(i);
                if (gridViewInventory.GetRowCellValue(rowHandle, "GoodsID") == null)
                {
                    break;
                }
                Decimal quantity = (Decimal)gridViewInventory.GetRowCellValue(rowHandle, "IssueQuantity");
                total += quantity;
                i++;
            }
            textEditTotalIssueQuantity.Text = total.ToString("0.#####");
        }
        private void getTotalIssueAmount()
        {
            Decimal total = 0;
            int i = 0;
            while (true)
            {
                int rowHandle = gridViewInventory.GetRowHandle(i);
                if (gridViewInventory.GetRowCellValue(rowHandle, "GoodsID") == null)
                {
                    break;
                }
                Decimal amount = (Decimal)gridViewInventory.GetRowCellValue(rowHandle, "IssueAmount");
                total += amount;
                i++;
            }
            textEditTotalIssueAmount.Text = total.ToString("0.#####");
        }
        private void getTotalCloseStockQuantity()
        {
            Decimal total = 0;
            int i = 0;
            while (true)
            {
                int rowHandle = gridViewInventory.GetRowHandle(i);
                if (gridViewInventory.GetRowCellValue(rowHandle, "GoodsID") == null)
                {
                    break;
                }
                Decimal quantity = (Decimal)gridViewInventory.GetRowCellValue(rowHandle, "ClosingStockQuantity");
                total += quantity;
                i++;
            }
            textEditCloseStockQuantity.Text = total.ToString("0.#####");
        }
        private void getTotalCloseStockAmount()
        {

            Decimal total = 0;
            int i = 0;
            while (true)
            {
                int rowHandle = gridViewInventory.GetRowHandle(i);
                if (gridViewInventory.GetRowCellValue(rowHandle, "GoodsID") == null)
                {
                    break;
                }
                Decimal amount = (Decimal)gridViewInventory.GetRowCellValue(rowHandle, "ClosingStockAmount");
                total += amount;
                i++;
            }
            textEditCloseStockAmount.Text = total.ToString("0.#####");
        }

        private async Task DeleteInventory(String monthYear)
        {
            Int16 month = Int16.Parse(monthYear.Substring(0, 2));
            Int16 year = Int16.Parse(monthYear.Substring(3, 4));
            await inventoryRepository.Delete(year, month, $"{int.Parse(comboBoxEditBranch.SelectedIndex.ToString()) - 1:D3}");
        }

        private async Task DeleteInventoryOfLastMonths(String monthYear)
        {
            Int16 month = Int16.Parse(monthYear.Substring(0, 2));
            Int16 year = Int16.Parse(monthYear.Substring(3, 4));
            await inventoryOfLastMonthsRepository.Delete(year, month, $"{int.Parse(comboBoxEditBranch.SelectedIndex.ToString()) - 1:D3}");
        }



        private async Task<List<WRHeader>> fetchWRHeaderUnderPeriodDate()
        {
            List<WRHeader> wRHeaders = new List<WRHeader>();
            wRHeaders = await WRHeaderRepository.GetUnderPriodDate(comboBoxEditProcessingPeriod.SelectedItem.ToString());
            return wRHeaders;
        }

        private async Task<List<IOHeader>> fetchIOHeaderUnderPeriodDate()
        {
            List<IOHeader> iOHeaders = new List<IOHeader>();
            iOHeaders = await iOHeaderRepository.GetUnderPriodDate(comboBoxEditProcessingPeriod.SelectedItem.ToString());
            return iOHeaders;
        }

        private async Task<List<WRHeader>> fetchWRHeaderUnderLastPeriodDate()
        {
            List<WRHeader> wRHeaders = new List<WRHeader>();
            wRHeaders = await WRHeaderRepository.GetUnderLastPriodDate(comboBoxEditProcessingPeriod.SelectedItem.ToString());
            return wRHeaders;
        }

        private async Task<List<IOHeader>> fetchIOHeaderUnderLastPeriodDate()
        {
            List<IOHeader> iOHeaders = new List<IOHeader>();
            iOHeaders = await iOHeaderRepository.GetUnderLastPriodDate(comboBoxEditProcessingPeriod.SelectedItem.ToString());
            return iOHeaders;
        }

        private async Task<List<WRDetail>> fetchWRDetailUnderPeriodDate()
        {
            List<WRDetail> wRDetails = new List<WRDetail>();
            List<WRHeader> wRHeaders = await fetchWRHeaderUnderPeriodDate();
            foreach (WRHeader wRHeader in wRHeaders)
            {
                String wrNumber = wRHeader.WRNumber;
                List<WRDetail> wRDetails1 = new List<WRDetail>();
                wRDetails1 = await wRDetailRepository.GetUnderID(wrNumber);
                foreach (WRDetail wRDetail in wRDetails1)
                {
                    wRDetails.Add(wRDetail);
                }
            }


            //for (int i = 0; i < wRDetails.Count; i++)
            //{
            //    for (int j=i+1; j < wRDetails.Count - 1; j++)
            //    {
            //        if (String.Compare(wRDetails[i].GoodsID,wRDetails[j].GoodsID,true)==0)
            //        {
            //            wRDetails[i].ReceiptQuantity = wRDetails[i].ReceiptQuantity + wRDetails[j].ReceiptQuantity;
            //            wRDetails.Remove(wRDetails[j]);
            //        }
            //    }
            //}

            var querys = wRDetails.GroupBy(x => x.GoodsID)
                 .Select(x => new WRDetail
                 {
                     GoodsID = x.Key,
                     GoodsName = x.First().GoodsName,
                     WRNumber = x.First().WRNumber,
                     Ordinal = x.First().Ordinal,
                     ReceiptUnitID = x.First().ReceiptUnitID,
                     UnitRate = x.First().UnitRate,
                     StockUnitID = x.First().StockUnitID,
                     PriceIncludedVAT = x.First().PriceIncludedVAT,
                     PriceExcludedVAT = x.First().PriceExcludedVAT,
                     Price = x.First().Price,
                     Discount = x.First().Discount,
                     DiscountAmount = x.First().DiscountAmount,
                     VAT = x.First().UnitRate,
                     VATAmount = x.First().VATAmount,
                     AmountExcludedVAT = x.First().AmountExcludedVAT,
                     AmountIncludedVAT = x.First().AmountIncludedVAT,
                     Amount = x.First().Amount,
                     ExpiryDate = x.First().ExpiryDate,
                     SerialNumber = x.First().SerialNumber,
                     Note = x.First().Note,
                     QuantityReceived = x.First().QuantityReceived,
                     PriceIncludedVATReceived = x.First().PriceIncludedVATReceived,
                     PriceExcludedVATReceived = x.First().PriceExcludedVATReceived,
                     PriceReceived = x.First().PriceReceived,
                     DiscountReceived = x.First().DiscountReceived,
                     DiscountAmountReceived = x.First().DiscountAmountReceived,
                     VATReceived = x.First().VATReceived,
                     VATAmountReceived = x.First().VATAmountReceived,
                     AmountExcludedVATReceived = x.First().AmountExcludedVATReceived,
                     AmountIncludedVATReceived = x.First().AmountIncludedVATReceived,
                     AmountReceived = x.First().AmountReceived,
                     QuantityOrdered = x.First().QuantityOrdered,
                     OrderQuantityReceived = x.First().OrderQuantityReceived,
                     ReceiptQuantityIssued = x.First().ReceiptQuantityIssued,
                     Quantity = x.First().Quantity,
                     ReceiptAmount = x.First().ReceiptAmount,
                     AverageCost = x.First().AverageCost,
                     Status = x.First().Status,
                     ReceiptQuantity = x.Sum(y => y.ReceiptQuantity)
                 })
                .ToList();
            return querys;
        }

        private async Task<List<IODetail>> fetchIODetailUnderPeriodDate()
        {
            List<IODetail> iODetails = new List<IODetail>();
            List<IOHeader> iOHeaders = await fetchIOHeaderUnderPeriodDate();
            foreach (IOHeader iOHeader in iOHeaders)
            {
                String ioNumber = iOHeader.IONumber;
                List<IODetail> iODetails1 = new List<IODetail>();
                iODetails = await iODetailRepository.GetUnderID(ioNumber);
                foreach (IODetail iODetail in iODetails1)
                {
                    iODetails.Add(iODetail);
                }
            }


            //for (int i = 0; i < iODetails.Count; i++)
            //{
            //    for (int j = i+1; j < iODetails.Count - 1; j++)
            //    {
            //        if (String.Compare(iODetails[i].GoodsID, iODetails[j].GoodsID, true) == 0)
            //        {
            //            iODetails[i].IssueQuantity = iODetails[i].IssueQuantity + iODetails[j].IssueQuantity;
            //            iODetails.Remove(iODetails[j]);
            //        }
            //    }
            //}

            var querys = iODetails.GroupBy(x => x.GoodsID)
                 .Select(x => new IODetail
                 {
                     GoodsID = x.Key,
                     IONumber = x.First().IONumber,
                     Ordinal = x.First().Ordinal,
                     GoodsName = x.First().GoodsName,
                     IssueUnitID = x.First().IssueUnitID,
                     UnitRate = x.First().UnitRate,
                     StockUnitID = x.First().StockUnitID,
                     Quantity = x.First().Quantity,
                     PriceIncludedVAT = x.First().PriceIncludedVAT,
                     PriceExcludedVAT = x.First().PriceExcludedVAT,
                     Price = x.First().Price,
                     Discount = x.First().Discount,
                     DiscountAmount = x.First().DiscountAmount,
                     VAT = x.First().VAT,
                     VATAmount = x.First().VATAmount,
                     AmountExcludedVAT = x.First().AmountExcludedVAT,
                     AmountIncludedVAT = x.First().AmountIncludedVAT,
                     Amount = x.First().Amount,
                     ExpiryDate = x.First().ExpiryDate,
                     SerialNumber = x.First().SerialNumber,
                     Note = x.First().Note,
                     QuantityIssued = x.First().QuantityIssued,
                     PriceExcludedVATIssued = x.First().PriceExcludedVATIssued,
                     PriceIncludedVATIssued = x.First().PriceIncludedVATIssued,
                     PriceIssued = x.First().PriceIssued,
                     DiscountIssued = x.First().DiscountIssued,
                     DiscountAmountIssued = x.First().DiscountAmountIssued,
                     VATIssued = x.First().VATIssued,
                     VATAmountIssued = x.First().VATAmountIssued,
                     AmountExcludedVATIssued = x.First().AmountExcludedVATIssued,
                     AmountIncludedVATIssued = x.First().AmountIncludedVATIssued,
                     AmountIssued = x.First().AmountIssued,
                     QuantityOrdered = x.First().QuantityOrdered,
                     OrderQuantityIssued = x.First().OrderQuantityIssued,
                     IssueQuantityReceived = x.First().IssueQuantityReceived,
                     IssueAmount = x.First().IssueAmount,
                     AverageCost = x.First().AverageCost,
                     SPSNumber = x.First().SPSNumber,
                     PSNumberSalePrice = x.First().PSNumberSalePrice,
                     PSNumberDiscount = x.First().PSNumberDiscount,
                     Status = x.First().Status,
                     IssueQuantity = x.Sum(y => y.IssueQuantity)
                 })
                .ToList();
            return querys;
        }

        private async Task<List<WRDetail>> fetchWRDetailUnderLastPeriod()
        {
            List<WRDetail> wRDetails = new List<WRDetail>();
            List<WRHeader> wRHeaders = await fetchWRHeaderUnderLastPeriodDate();
            foreach (WRHeader wRHeader in wRHeaders)
            {
                String wrNumber = wRHeader.WRNumber;
                List<WRDetail> wRDetails1 = new List<WRDetail>();
                wRDetails1 = await wRDetailRepository.GetUnderID(wrNumber);
                foreach (WRDetail wRDetail in wRDetails1)
                {
                    wRDetails.Add(wRDetail);
                }
            }


            //for (int i = 0; i < wRDetails.Count; i++)
            //{
            //    for (int j = i + 1; j < wRDetails.Count - 1; j++)
            //    {
            //        if (wRDetails[i].GoodsID.Trim().Equals(wRDetails[j].GoodsID.Trim()))
            //        {
            //            wRDetails[i].ReceiptQuantity = wRDetails[i].ReceiptQuantity + wRDetails[j].ReceiptQuantity;
            //            wRDetails.RemoveAt(j);
            //        }
            //    }
            //}

            var querys = wRDetails.GroupBy(x => x.GoodsID)
                 .Select(x => new WRDetail
                 {
                     GoodsID = x.Key,
                     GoodsName = x.First().GoodsName,
                     WRNumber = x.First().WRNumber,
                     Ordinal = x.First().Ordinal,
                     ReceiptUnitID = x.First().ReceiptUnitID,
                     UnitRate = x.First().UnitRate,
                     StockUnitID = x.First().StockUnitID,
                     PriceIncludedVAT = x.First().PriceIncludedVAT,
                     PriceExcludedVAT = x.First().PriceExcludedVAT,
                     Price = x.First().Price,
                     Discount = x.First().Discount,
                     DiscountAmount = x.First().DiscountAmount,
                     VAT = x.First().UnitRate,
                     VATAmount = x.First().VATAmount,
                     AmountExcludedVAT = x.First().AmountExcludedVAT,
                     AmountIncludedVAT = x.First().AmountIncludedVAT,
                     Amount = x.First().Amount,
                     ExpiryDate = x.First().ExpiryDate,
                     SerialNumber = x.First().SerialNumber,
                     Note = x.First().Note,
                     QuantityReceived = x.First().QuantityReceived,
                     PriceIncludedVATReceived = x.First().PriceIncludedVATReceived,
                     PriceExcludedVATReceived = x.First().PriceExcludedVATReceived,
                     PriceReceived = x.First().PriceReceived,
                     DiscountReceived = x.First().DiscountReceived,
                     DiscountAmountReceived = x.First().DiscountAmountReceived,
                     VATReceived = x.First().VATReceived,
                     VATAmountReceived = x.First().VATAmountReceived,
                     AmountExcludedVATReceived = x.First().AmountExcludedVATReceived,
                     AmountIncludedVATReceived = x.First().AmountIncludedVATReceived,
                     AmountReceived = x.First().AmountReceived,
                     QuantityOrdered = x.First().QuantityOrdered,
                     OrderQuantityReceived = x.First().OrderQuantityReceived,
                     ReceiptQuantityIssued = x.First().ReceiptQuantityIssued,
                     Quantity = x.First().Quantity,
                     ReceiptAmount = x.First().ReceiptAmount,
                     AverageCost = x.First().AverageCost,
                     Status = x.First().Status,
                     ReceiptQuantity = x.Sum(y => y.ReceiptQuantity)
                 })
                .ToList();
            return querys;
        }

        private async Task<List<IODetail>> fetchIODetailUnderLastPeriod()
        {
            List<IODetail> iODetails = new List<IODetail>();
            List<IOHeader> iOHeaders = await fetchIOHeaderUnderLastPeriodDate();
            foreach (IOHeader iOHeader in iOHeaders)
            {
                String ioNumber = iOHeader.IONumber;
                List<IODetail> iODetails1 = new List<IODetail>();
                iODetails = await iODetailRepository.GetUnderID(ioNumber);
                foreach (IODetail iODetail in iODetails1)
                {
                    iODetails.Add(iODetail);
                }
            }


            //for (int i = 0; i < iODetails.Count; i++)
            //{
            //    for (int j = i + 1; j < iODetails.Count - 1; j++)
            //    {
            //        if (String.Compare(iODetails[i].GoodsID, iODetails[j].GoodsID, true) == 0)
            //        {
            //            iODetails[i].IssueQuantity = iODetails[i].IssueQuantity + iODetails[j].IssueQuantity;
            //            iODetails.Remove(iODetails[j]);
            //        }
            //    }
            //}

            var querys = iODetails.GroupBy(x => x.GoodsID)
                 .Select(x => new IODetail
                 {
                     GoodsID = x.Key,
                     IONumber = x.First().IONumber,
                     Ordinal = x.First().Ordinal,
                     GoodsName = x.First().GoodsName,
                     IssueUnitID = x.First().IssueUnitID,
                     UnitRate = x.First().UnitRate,
                     StockUnitID = x.First().StockUnitID,
                     Quantity = x.First().Quantity,
                     PriceIncludedVAT = x.First().PriceIncludedVAT,
                     PriceExcludedVAT = x.First().PriceExcludedVAT,
                     Price = x.First().Price,
                     Discount = x.First().Discount,
                     DiscountAmount = x.First().DiscountAmount,
                     VAT = x.First().VAT,
                     VATAmount = x.First().VATAmount,
                     AmountExcludedVAT = x.First().AmountExcludedVAT,
                     AmountIncludedVAT = x.First().AmountIncludedVAT,
                     Amount = x.First().Amount,
                     ExpiryDate = x.First().ExpiryDate,
                     SerialNumber = x.First().SerialNumber,
                     Note = x.First().Note,
                     QuantityIssued = x.First().QuantityIssued,
                     PriceExcludedVATIssued = x.First().PriceExcludedVATIssued,
                     PriceIncludedVATIssued = x.First().PriceIncludedVATIssued,
                     PriceIssued = x.First().PriceIssued,
                     DiscountIssued = x.First().DiscountIssued,
                     DiscountAmountIssued = x.First().DiscountAmountIssued,
                     VATIssued = x.First().VATIssued,
                     VATAmountIssued = x.First().VATAmountIssued,
                     AmountExcludedVATIssued = x.First().AmountExcludedVATIssued,
                     AmountIncludedVATIssued = x.First().AmountIncludedVATIssued,
                     AmountIssued = x.First().AmountIssued,
                     QuantityOrdered = x.First().QuantityOrdered,
                     OrderQuantityIssued = x.First().OrderQuantityIssued,
                     IssueQuantityReceived = x.First().IssueQuantityReceived,
                     IssueAmount = x.First().IssueAmount,
                     AverageCost = x.First().AverageCost,
                     SPSNumber = x.First().SPSNumber,
                     PSNumberSalePrice = x.First().PSNumberSalePrice,
                     PSNumberDiscount = x.First().PSNumberDiscount,
                     Status = x.First().Status,
                     IssueQuantity = x.Sum(y => y.IssueQuantity)
                 })
                .ToList();

            return querys;
        }

        private async Task<List<Inventory>> processDataThisMonth()
        {
            List<WRDetail> wRDetails = await fetchWRDetailUnderPeriodDate();
            List<IODetail> iODetails = await fetchIODetailUnderPeriodDate();
            List<Inventory> inventories = new List<Inventory>();
            // List<Inventory> inventoriesWR = new List<Inventory>();
            foreach (WRDetail wRDetail in wRDetails)
            {
                Inventory inventory = new Inventory();
                inventory.Month = Int16.Parse(comboBoxEditProcessingPeriod.SelectedItem.ToString().Substring(0, 2));
                inventory.Year = Int16.Parse(comboBoxEditProcessingPeriod.SelectedItem.ToString().Substring(3, 4));
                inventory.WarehouseID = "000";
                inventory.GoodsID = wRDetail.GoodsID;
                inventory.OpeningStockQuantity = 0;
                inventory.OpeningStockAmount = 0;
                inventory.ReceiptQuantity = wRDetail.ReceiptQuantity;
                inventory.ReceiptAmount = wRDetail.ReceiptAmount;
                inventory.IssueQuantity = 0;
                inventory.IssueAmount = 0;
                inventory.ClosingStockQuantity = wRDetail.ReceiptQuantity;
                inventory.ClosingStockAmount = wRDetail.ReceiptAmount;
                inventory.AverageCost = 0;
                inventory.Status = "1";
                inventories.Add(inventory);
            }
            //List<Inventory> inventoriesIO = new List<Inventory>();
            foreach (IODetail iODetail in iODetails)
            {
                Inventory inventory = new Inventory();
                inventory.Month = Int16.Parse(comboBoxEditProcessingPeriod.SelectedItem.ToString().Substring(0, 2));
                inventory.Year = Int16.Parse(comboBoxEditProcessingPeriod.SelectedItem.ToString().Substring(3, 4));
                inventory.WarehouseID = "000";
                inventory.GoodsID = iODetail.GoodsID;
                inventory.OpeningStockQuantity = 0;
                inventory.OpeningStockAmount = 0;
                inventory.ReceiptQuantity = 0;
                inventory.ReceiptAmount = 0;
                inventory.IssueQuantity = iODetail.IssueQuantity;
                inventory.IssueAmount = iODetail.IssueAmount;
                inventory.ClosingStockQuantity = -iODetail.IssueQuantity;
                inventory.ClosingStockAmount = -iODetail.IssueQuantity;
                inventory.AverageCost = 0;
                inventory.Status = "1";
                inventories.Add(inventory);
            }
            //for (int i = 0; i < inventories.Count; i++)
            //{
            //    for (int j = i+1; j < inventories.Count-1; j++)
            //    {
            //        if (String.Compare(inventories[i].GoodsID, inventories[j].GoodsID, true) == 0)
            //        {
            //            inventories[i].ReceiptQuantity = inventories[i].ReceiptQuantity+ inventories[j].ReceiptQuantity;
            //            inventories[i].ReceiptAmount = inventories[i].ReceiptAmount+ inventories[j].ReceiptAmount;
            //            inventories[i].IssueQuantity = inventories[i].IssueQuantity+ inventories[j].IssueQuantity;
            //            inventories[i].IssueAmount = inventories[i].IssueAmount+ inventories[j].IssueAmount;
            //            inventories[i].ClosingStockQuantity = inventories[i].ClosingStockQuantity + inventories[j].ClosingStockQuantity;
            //            inventories[i].ClosingStockAmount = inventories[i].ClosingStockAmount + inventories[j].ClosingStockAmount;
            //            inventories.Remove(inventories[j]);
            //        }
            //    }
            //}

            var querys = inventories.GroupBy(x => x.GoodsID)
                 .Select(x => new Inventory
                 {
                     GoodsID = x.Key,
                     Year = x.First().Year,
                     Month = x.First().Month,
                     WarehouseID = x.First().WarehouseID,
                     OpeningStockQuantity = x.First().OpeningStockQuantity,
                     OpeningStockAmount = x.First().OpeningStockAmount,
                     AverageCost = x.First().AverageCost,
                     Status = x.First().Status,
                     ReceiptQuantity = x.Sum(y => y.ReceiptQuantity),
                     ReceiptAmount = x.Sum(y => y.ReceiptAmount),
                     IssueQuantity = x.Sum(y => y.IssueQuantity),
                     IssueAmount = x.Sum(y => y.IssueAmount),
                     ClosingStockQuantity = x.Sum(y => y.ClosingStockQuantity),
                     ClosingStockAmount = x.Sum(y => y.ClosingStockAmount)
                 })
                .ToList();
            return querys;
        }

        private async Task<List<Inventory>> processDataLastMonth()
        {
            List<WRDetail> wRDetails = await fetchWRDetailUnderLastPeriod();
            List<IODetail> iODetails = await fetchIODetailUnderLastPeriod();
            List<Inventory> inventories = new List<Inventory>();
            // List<Inventory> inventoriesWR = new List<Inventory>();
            foreach (WRDetail wRDetail in wRDetails)
            {
                Inventory inventory = new Inventory();
                inventory.Month = Int16.Parse(comboBoxEditProcessingPeriod.SelectedItem.ToString().Substring(0, 2));
                inventory.Year = Int16.Parse(comboBoxEditProcessingPeriod.SelectedItem.ToString().Substring(3, 4));
                inventory.WarehouseID = "000";
                inventory.GoodsID = wRDetail.GoodsID;
                inventory.OpeningStockQuantity = 0;
                inventory.OpeningStockAmount = 0;
                inventory.ReceiptQuantity = wRDetail.ReceiptQuantity;
                inventory.ReceiptAmount = wRDetail.ReceiptAmount;
                inventory.IssueQuantity = 0;
                inventory.IssueAmount = 0;
                inventory.ClosingStockQuantity = wRDetail.ReceiptQuantity;
                inventory.ClosingStockAmount = wRDetail.ReceiptAmount;
                inventory.AverageCost = 0;
                inventory.Status = "1";
                inventories.Add(inventory);
            }
            //List<Inventory> inventoriesIO = new List<Inventory>();
            foreach (IODetail iODetail in iODetails)
            {
                Inventory inventory = new Inventory();
                inventory.Month = Int16.Parse(comboBoxEditProcessingPeriod.SelectedItem.ToString().Substring(0, 2));
                inventory.Year = Int16.Parse(comboBoxEditProcessingPeriod.SelectedItem.ToString().Substring(3, 4));
                inventory.WarehouseID = "000";
                inventory.GoodsID = iODetail.GoodsID;
                inventory.OpeningStockQuantity = 0;
                inventory.OpeningStockAmount = 0;
                inventory.ReceiptQuantity = 0;
                inventory.ReceiptAmount = 0;
                inventory.IssueQuantity = iODetail.IssueQuantity;
                inventory.IssueAmount = iODetail.IssueAmount;
                inventory.ClosingStockQuantity = -iODetail.IssueQuantity;
                inventory.ClosingStockAmount = -iODetail.IssueQuantity;
                inventory.AverageCost = 0;
                inventory.Status = "1";
                inventories.Add(inventory);
            }
            //for (int i = 0; i < inventories.Count; i++)
            //{
            //    for (int j = i + 1; j < inventories.Count - 1; j++)
            //    {
            //        if (inventories[i].GoodsID.Equals(inventories[j].GoodsID))
            //        {
            //            inventories[i].ReceiptQuantity = inventories[i].ReceiptQuantity + inventories[j].ReceiptQuantity;
            //            inventories[i].ReceiptAmount = inventories[i].ReceiptAmount + inventories[j].ReceiptAmount;
            //            inventories[i].IssueQuantity = inventories[i].IssueQuantity + inventories[j].IssueQuantity;
            //            inventories[i].IssueAmount = inventories[i].IssueAmount + inventories[j].IssueAmount;
            //            inventories[i].ClosingStockQuantity = inventories[i].ClosingStockQuantity + inventories[j].ClosingStockQuantity;
            //            inventories[i].ClosingStockAmount = inventories[i].ClosingStockAmount + inventories[j].ClosingStockAmount;
            //            inventories.Remove(inventories[j]);
            //        }
            //    }
            //}
            //return inventories;
            var querys = inventories.GroupBy(x => x.GoodsID)
                .Select(x => new Inventory
                {
                    GoodsID = x.Key,
                    Year = x.First().Year,
                    Month = x.First().Month,
                    WarehouseID = x.First().WarehouseID,
                    OpeningStockQuantity = x.First().OpeningStockQuantity,
                    OpeningStockAmount = x.First().OpeningStockAmount,
                    AverageCost = x.First().AverageCost,
                    Status = x.First().Status,
                    ReceiptQuantity = x.Sum(y => y.ReceiptQuantity),
                    ReceiptAmount = x.Sum(y => y.ReceiptAmount),
                    IssueQuantity = x.Sum(y => y.IssueQuantity),
                    IssueAmount = x.Sum(y => y.IssueAmount),
                    ClosingStockQuantity = x.Sum(y => y.ClosingStockQuantity),
                    ClosingStockAmount = x.Sum(y => y.ClosingStockAmount)
                })
               .ToList();
            return querys;
        }

        private async Task<List<Inventory>> processData(List<Inventory> inventories,List<InventoryOfLastMonths> inventoryOfLastMonths)
        {
            var queryDuplicate = (from InventoryOfLastMonths in inventoryOfLastMonths
                         join Inventory in inventories
                         on InventoryOfLastMonths.GoodsID equals Inventory.GoodsID
                         select new Inventory
                         {
                             GoodsID = Inventory.GoodsID,
                             Year = Inventory.Year,
                             Month = Inventory.Month,
                             WarehouseID = Inventory.WarehouseID,
                             OpeningStockQuantity = InventoryOfLastMonths.ClosingStockQuantity,
                             OpeningStockAmount = InventoryOfLastMonths.ClosingStockAmount,
                             AverageCost = Inventory.AverageCost,
                             Status = Inventory.Status,
                             ReceiptQuantity = Inventory.ReceiptQuantity,
                             ReceiptAmount = Inventory.ReceiptAmount,
                             IssueQuantity = Inventory.IssueQuantity,
                             IssueAmount = Inventory.IssueAmount,
                             ClosingStockQuantity = InventoryOfLastMonths.ClosingStockQuantity + Inventory.ReceiptQuantity - Inventory.IssueQuantity,
                             ClosingStockAmount = InventoryOfLastMonths.ClosingStockAmount + Inventory.ReceiptAmount - Inventory.IssueAmount
                         }).ToList();
            var queryNonDuplicate = inventories.Where(x => !inventoryOfLastMonths.Select(y => y.GoodsID).Contains(x.GoodsID)).ToList();
            List<Inventory> inventories1 = new List<Inventory>();
            foreach(Inventory inventory in queryDuplicate)
            {
                inventories1.Add(inventory);
            }
            foreach(Inventory inventory1 in queryNonDuplicate)
            {
                inventories1.Add(inventory1);
            }
            return inventories1;
        }

        private async Task sbLoadDataForGridAsync(Int16 year, Int16 month, String warehouseID)
        {
            List<Inventory> inventories = await inventoryRepository.GetUnderMultiID(year, month, warehouseID);
            int mMaxRow = 100;
            if (inventories.Count < mMaxRow)
            {
                int num = mMaxRow - inventories.Count;
                int i = 1;
                while (true)
                {
                    int num2 = i;
                    int num3 = num;
                    if (num2 > num3)
                    {
                        break;
                    }
                    inventories.Add(new Inventory());
                    i++;
                }
            }
            gridControlInventory.DataSource = inventories;
            getTotalOpenStockQuantity();
            //getTotalOpenStockAmount();
            getTotalCloseStockQuantity();
            //();
            //getTotalCloseStockAmount();
            getTotalReceiptQuantity();
            //getTotalReceiptAmount();
            getTotalIssueQuantity();
            //getTotalIssueAmount();
        }

        private async Task<List<InventoryOfLastMonths>> getInventoryOfLastMonths(String monthYear)
        {
            int mYear = int.Parse(monthYear.Substring(3, 4));
            int mMonth = int.Parse(monthYear.Substring(0, 2));
            Int16 mLastMonth;
            Int16 mLastYear;
            if (mMonth == 1)
            {
                mLastMonth = 12;
                mLastYear = (short)(mYear - 1);
            }
            else
            {
                mLastMonth = (short)(mMonth - 1);
                mLastYear = (short)mYear;
            }
            if (mMonth == 1)
            {
                mLastMonth = 12;
                mLastYear = (short)(mYear - 1);
            }
            else
            {
                mLastMonth = (short)(mMonth - 1);
                mLastYear = (short)mYear;
            }
            string mLastMonthYear = $"{mLastMonth:00}" + "/" + String.Format(mLastYear.ToString(), "####");
            List<InventoryOfLastMonths> inventoryOfLastMonths = await inventoryOfLastMonthsRepository.GetUnderMultiID(
               mLastYear, mLastMonth, $"{int.Parse(comboBoxEditBranch.SelectedIndex.ToString()) - 1:D3}");
            return inventoryOfLastMonths;
        }

        public async Task createInventoryAsync(String monthYear)
        {
            List<InventoryOfLastMonths> inventoryOfLastMonths = await getInventoryOfLastMonths(monthYear);
            List<Inventory> inventories = await processDataThisMonth();
            List<WRHeader> wRHeaders = await fetchWRHeaderUnderLastPeriodDate();
            List<IOHeader> iOHeaders = await fetchIOHeaderUnderLastPeriodDate();
            if (inventoryOfLastMonths.Count == 0)
            {
                if(wRHeaders.Count == 0 && iOHeaders.Count == 0)
                {
                    if(comboBoxEditProcessingPeriod.SelectedIndex == 0)
                    {
                        textEditMessage.Text = "Dữ liệu tồn kho tháng trước" + mLastPeriod + " không tồn tại, Xử lý tồn kho tháng hiện tại";
                    }
                    else
                    {
                        DeleteInventory(monthYear);
                        List<Inventory> inventories1 = await processData(inventories, inventoryOfLastMonths);
                        foreach (Inventory inventory in inventories1)
                        {
                            await inventoryRepository.Create(inventory);
                        }
                        updateWRDetailAsync();
                        updateIODetailAsync();
                        textEditMessage.Text = "Đã xử lý tồn kho tháng" + monnthYearNow;
                    }
                }
                else
                {
                    DeleteInventory(monthYear);
                    List<Inventory> inventories2 = await processDataLastMonth();
                    foreach (Inventory inventory in inventories2)
                    {
                        await inventoryRepository.Create(inventory);
                    }
                    createInventoryOfLastMonth(monthYear);
                    textEditMessage.Text = "Đã Xử lý tồn kho tháng " + mLastPeriod;
                }
            }
            else
            {
                DeleteInventory(monthYear);
                List<Inventory> inventories1 = await processData(inventories, inventoryOfLastMonths);
                foreach (Inventory inventory in inventories1)
                {
                    await inventoryRepository.Create(inventory);
                }
                updateWRDetailAsync();
                updateIODetailAsync();
                textEditMessage.Text = "Đã xử lý tồn kho tháng" + monnthYearNow;
            }
            //if(inventoryOfLastMonths.Count==0 && comboBoxEditProcessingPeriod.SelectedIndex ==1)
            //{
            //    if (wRHeaders.Count==0 && iOHeaders.Count==0)
            //    {
            //        DeleteInventory(monthYear);
            //        List<Inventory> inventories1 = await processDataThisMonth();
            //        foreach (Inventory inventory in inventories1)
            //        {
            //            await inventoryRepository.Create(inventory);
            //        }
            //        updateWRDetailAsync();
            //        updateIODetailAsync();
            //        textEditMessage.Text = "Đã xử lý tồn kho tháng" + monnthYearNow;
            //    }
            //    else
            //    {
            //        textEditMessage.Text = "Chưa xử lý tồn kho tháng" + mLastPeriod;
            //    }
            //}
            //else if (inventoryOfLastMonths.Count > 0 && comboBoxEditProcessingPeriod.SelectedIndex == 0)
            //{
            //    textEditMessage.Text = "Đã xử lý tồn kho tháng" + monnthYearNow;

            //}
            ////Đã xử lý tồn kho tháng trước-> Xử lý tồn kho tháng hiện tại
            //else if (inventoryOfLastMonths.Count > 0 && comboBoxEditProcessingPeriod.SelectedIndex == 1)
            //{
            //    DeleteInventory(monthYear);
            //    List<Inventory> inventories1 = await processData(inventories,inventoryOfLastMonths);
            //    foreach (Inventory inventory in inventories1)
            //    {
            //        await inventoryRepository.Create(inventory);
            //    }
            //    updateWRDetailAsync();
            //    updateIODetailAsync();
            //    textEditMessage.Text = "Đã xử lý tồn kho tháng" + monnthYearNow;
            //}
            ////Chưa xử lý tồn kho tháng trước->Xử lý tồn kho tháng trước->Tạo dữ liệu tồn kho tháng trước
            //else if (inventoryOfLastMonths.Count == 0 && comboBoxEditProcessingPeriod.SelectedIndex == 0)
            //{
                
            //    if (wRHeaders.Count == 0 && iOHeaders.Count == 0)
            //    {
            //        textEditMessage.Text = "Dữ liệu tồn kho tháng trước" +mLastPeriod+" không tồn tại, Xử lý tồn kho tháng hiện tại";
            //        return;
            //    }

            //    DeleteInventory(monthYear);
            //    List<Inventory> inventories2 = await processDataLastMonth();
            //    foreach (Inventory inventory in inventories2)
            //    {
            //        await inventoryRepository.Create(inventory);
            //    }
            //    createInventoryOfLastMonth(monthYear);
            //    textEditMessage.Text = "Đã Xử lý tồn kho tháng trước->Tạo dữ liệu tồn kho tháng trước" +mLastPeriod;
            //}
           
        }

        public async Task createInventoryOfLastMonth(String monthYear)
        {
            DeleteInventoryOfLastMonths(monthYear);
            List<Inventory> inventories = await processDataLastMonth();
            List<InventoryOfLastMonths> inventoryOfLastMonthss = new List<InventoryOfLastMonths>();
            foreach (Inventory inventory in inventories)
            {
                InventoryOfLastMonths inventoryOfLastMonths = new InventoryOfLastMonths();
                inventoryOfLastMonths.Year = inventory.Year;
                inventoryOfLastMonths.Month = inventory.Month;
                inventoryOfLastMonths.WarehouseID = inventory.WarehouseID;
                inventoryOfLastMonths.GoodsID = inventory.GoodsID;
                inventoryOfLastMonths.OpeningStockQuantity = inventory.OpeningStockQuantity;
                inventoryOfLastMonths.OpeningStockAmount = inventory.OpeningStockAmount;
                inventoryOfLastMonths.ReceiptQuantity = inventory.ReceiptQuantity;
                inventoryOfLastMonths.ReceiptAmount = inventory.ReceiptAmount;
                inventoryOfLastMonths.IssueQuantity = inventory.IssueQuantity;
                inventoryOfLastMonths.IssueAmount = inventory.IssueAmount;
                inventoryOfLastMonths.ClosingStockQuantity = inventory.ClosingStockQuantity;
                inventoryOfLastMonths.ClosingStockAmount = inventory.ClosingStockAmount;
                inventoryOfLastMonths.AverageCost = inventory.AverageCost;
                inventoryOfLastMonths.Status = inventory.Status;
                await inventoryOfLastMonthsRepository.Create(inventoryOfLastMonths);
                //textEditMessage.Text = "Đã xử lý tồn kho tháng " + mLastPeriod + " Bạn có thể xử lý tồn kho tháng " + mCurrentPeriod;
            }
        }
        //public async Task updateInventoryAsync()
        //{
        //    List<WRDetail> wRDetails = await fetchWRDetailUnderWRHeader();
        //    for (int i = 0; i < wRDetails.Count; i++)
        //    {
        //        for (int j = 1; j < wRDetails.Count - 1; j++)
        //        {
        //            if (wRDetails[i].Equals(wRDetails[j]))
        //            {
        //                wRDetails[i].Quantity = wRDetails[i].Quantity + wRDetails[j].Quantity;
        //                wRDetails.Remove(wRDetails[j]);
        //            }
        //        }
        //    }
        //    Int16 month = Int16.Parse(comboBoxEditProcessingPeriod.SelectedItem.ToString().Substring(0, 2));
        //    Int16 year = Int16.Parse(comboBoxEditProcessingPeriod.SelectedItem.ToString().Substring(3, 4));
        //    String warehouseID = "000";
        //    foreach (WRDetail wRDetail in wRDetails)
        //    {
        //        Inventory inventory = new Inventory();
        //        inventory.Month = Int16.Parse(comboBoxEditProcessingPeriod.SelectedItem.ToString().Substring(0, 2));
        //        inventory.Year = Int16.Parse(comboBoxEditProcessingPeriod.SelectedItem.ToString().Substring(3, 4));
        //        inventory.WarehouseID = "000";
        //        inventory.GoodsID = wRDetail.GoodsID;
        //        inventory.OpeningStockQuantity = 0;
        //        inventory.OpeningStockAmount = 0;
        //        inventory.ReceiptQuantity = wRDetail.Quantity;
        //        inventory.ReceiptAmount = 0;
        //        inventory.IssueQuantity = 0;
        //        inventory.IssueAmount = 0;
        //        inventory.ClosingStockQuantity = 0;
        //        inventory.ClosingStockAmount = 0;
        //        inventory.AverageCost = 0;
        //        inventory.Status = "1";
        //        if (await inventoryRepository.Update(inventory, year, month, warehouseID, inventory.GoodsID) > 0)
        //        {
        //            MessageBox.Show("Đã cập nhật mở khóa chứng từ");
        //        }
        //    }
        //}

        public async Task updateWRDetailAsync()
        {
            List<WRDetail> wRDetails = await wRDetailRepository.GetAll();
            foreach (WRDetail wRDetail in wRDetails)
                if (wRDetail.Status == "0")
                {
                    wRDetail.Status = "1";
                    await wRDetailRepository.Update(wRDetail, wRDetail.WRNumber, wRDetail.GoodsID, wRDetail.Ordinal);
                }
        }

        public async Task updateIODetailAsync()
        {
            List<IODetail> iODetails = await iODetailRepository.GetAll();
            foreach (IODetail iODetail in iODetails)
                if (iODetail.Status == "0")
                {
                    iODetail.Status = "1";
                    await iODetailRepository.Update(iODetail, iODetail.IONumber, iODetail.GoodsID, iODetail.Ordinal);
                }
        }


        //barbutton click
        public async void button_Click(object sender, EventArgs e)
        {

            ButtonEventArgs eArg = (DevExpress.XtraBars.Docking2010.ButtonEventArgs)e;
            String tag = eArg.Button.Properties.Tag.ToString();
            switch (tag)
            {
                case "unlock":
                    ShowGridPreview(gridControlInventory);
                    PrintGrid(gridControlInventory);
                    break;
                case "process":
                    String monthYear = comboBoxEditProcessingPeriod.SelectedItem.ToString();
                    createInventoryAsync(monthYear);
                    break;
                case "refesh":
                    Int16 month = Int16.Parse(comboBoxEditProcessingPeriod.SelectedItem.ToString().Substring(0, 2));
                    Int16 year = Int16.Parse(comboBoxEditProcessingPeriod.SelectedItem.ToString().Substring(3, 4));
                    String warehouseID = "000";
                    sbLoadDataForGridAsync(year, month, warehouseID);
                    checDataChangeAsync();
                    break;
                case "close":
                    Dispose();
                    break;

            }

        }

        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            //e.Graphics.DrawImage(bitmap, 0, 0);
        }

        private void ShowGridPreview(GridControl grid)
        {
            // Check whether the GridControl can be previewed.
            if (!grid.IsPrintingAvailable)
            {
                MessageBox.Show("The 'DevExpress.XtraPrinting' library is not found", "Error");
                return;
            }

            // Open the Preview window.
            grid.ShowPrintPreview();
        }

        private void PrintGrid(GridControl grid)
        {
            // Check whether the GridControl can be printed.
            if (!grid.IsPrintingAvailable)
            {
                MessageBox.Show("The 'DevExpress.XtraPrinting' library is not found", "Error");
                return;
            }

            // Print.
            grid.Print();
        }

        private void simpleButtonShow_Click(object sender, EventArgs e)
        {
            Int16 month = Int16.Parse(comboBoxEditProcessingPeriod.SelectedItem.ToString().Substring(0, 2));
            Int16 year = Int16.Parse(comboBoxEditProcessingPeriod.SelectedItem.ToString().Substring(3, 4));
            String warehouseID = "000";
            sbLoadDataForGridAsync(year, month, warehouseID);
        }
    }
}
