﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WarehouseManager.Helpers;

namespace WarehouseManager
{
    public partial class frmSelectTables : Form
    {      
        string tableName = string.Empty;
        string[] Tables;       
        public frmSelectTables(String[] StrTable)
        {
            InitializeComponent();
            Tables = StrTable;
            this.Load += Select_Tables_Load;
        }
        
        private void Select_Tables_Load(object sender, EventArgs e)
        {
            for (int tables = 0; tables < Tables.Length; tables++)
            {
                ListViewItem item = new ListViewItem();
                item.Text = Tables[tables].ToString();
                item.Tag = tables;
                listViewSheetTable.Items.Add(item);
            }
        }

        private void simpleButtonOK_Click(object sender, EventArgs e)
        {
            if (listViewSheetTable.Items.Count > 0)
            {
                if (tableName != string.Empty)
                {
                    ReceiptRequisitionControl.SelectedTable = tableName;
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Select a Table");
                }
            }
            else
            {
                this.Close();
            }
        }

        private void listViewSheetTable_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listViewSheetTable.SelectedIndices.Count <= 0)
            {
                return;
            }
            int intselectedindex = listViewSheetTable.SelectedIndices[0];           
            if (intselectedindex >= 0)
            {
                tableName = listViewSheetTable.Items[intselectedindex].Text.ToString();
               
            }
        }
    }
}
