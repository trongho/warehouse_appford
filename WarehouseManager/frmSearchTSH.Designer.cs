﻿
namespace WarehouseManager
{
    partial class frmSearchTSH
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions1 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSearchTSH));
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions2 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition1 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition2 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition3 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition4 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition5 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition1 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition2 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition3 = new DevExpress.XtraLayout.RowDefinition();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.windowsUIButtonPanel1 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.goodsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.wARHOUSE_HPDataSet1 = new WarehouseManager.WARHOUSE_HPDataSet1();
            this.goodsTableAdapter = new WarehouseManager.WARHOUSE_HPDataSet1TableAdapters.GoodsTableAdapter();
            this.layoutControl3 = new DevExpress.XtraLayout.LayoutControl();
            this.gridControlWRRHeader = new DevExpress.XtraGrid.GridControl();
            this.wRRHeaderBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewWRRHeader = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTSNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTSDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReferenceNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNote = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControl4 = new DevExpress.XtraLayout.LayoutControl();
            this.comboBoxEditBranch = new DevExpress.XtraEditors.ComboBoxEdit();
            this.dateEditFromDate = new DevExpress.XtraEditors.DateEdit();
            this.dateEditToDate = new DevExpress.XtraEditors.DateEdit();
            this.comboBoxEditModality = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEditHandlingStatus = new DevExpress.XtraEditors.ComboBoxEdit();
            this.simpleButtonFilter = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemBranch = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemFromDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemToDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemModality = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemHandlingStatus = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.wRRHeaderTableAdapter = new WarehouseManager.WARHOUSE_HPDataSet1TableAdapters.WRRHeaderTableAdapter();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.searchControl11 = new DevExpress.XtraEditors.SearchControl();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.goodsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wARHOUSE_HPDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).BeginInit();
            this.layoutControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlWRRHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wRRHeaderBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewWRRHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl4)).BeginInit();
            this.layoutControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditBranch.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditModality.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditHandlingStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemBranch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemFromDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemToDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemModality)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemHandlingStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchControl11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.windowsUIButtonPanel1);
            this.layoutControl1.Location = new System.Drawing.Point(1, 0);
            this.layoutControl1.Margin = new System.Windows.Forms.Padding(2);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(666, 94, 812, 500);
            this.layoutControl1.Root = this.Root;
            this.layoutControl1.Size = new System.Drawing.Size(250, 40);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // windowsUIButtonPanel1
            // 
            this.windowsUIButtonPanel1.AllowGlyphSkinning = false;
            this.windowsUIButtonPanel1.AppearanceButton.Hovered.BackColor = System.Drawing.Color.Red;
            this.windowsUIButtonPanel1.AppearanceButton.Hovered.Options.UseBackColor = true;
            this.windowsUIButtonPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            windowsUIButtonImageOptions1.EnableTransparency = true;
            windowsUIButtonImageOptions1.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions1.Image")));
            windowsUIButtonImageOptions1.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions2.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions2.Image")));
            windowsUIButtonImageOptions2.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            this.windowsUIButtonPanel1.Buttons.AddRange(new DevExpress.XtraEditors.ButtonPanel.IBaseButton[] {
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F5) Làm mới", true, windowsUIButtonImageOptions1, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "refesh", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F10) Thoát", true, windowsUIButtonImageOptions2, DevExpress.XtraBars.Docking2010.ButtonStyle.CheckButton, "", -1, true, null, true, false, true, "close", -1, false)});
            this.windowsUIButtonPanel1.ContentAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.windowsUIButtonPanel1.Location = new System.Drawing.Point(2, 2);
            this.windowsUIButtonPanel1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.windowsUIButtonPanel1.Name = "windowsUIButtonPanel1";
            this.windowsUIButtonPanel1.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.windowsUIButtonPanel1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.windowsUIButtonPanel1.Size = new System.Drawing.Size(246, 32);
            this.windowsUIButtonPanel1.TabIndex = 7;
            this.windowsUIButtonPanel1.Text = "windowsUIButtonPanel1";
            this.windowsUIButtonPanel1.UseButtonBackgroundImages = false;
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.Root.Name = "Root";
            this.Root.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.Root.Size = new System.Drawing.Size(250, 40);
            this.Root.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.windowsUIButtonPanel1;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(250, 40);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // goodsBindingSource
            // 
            this.goodsBindingSource.DataMember = "Goods";
            this.goodsBindingSource.DataSource = this.wARHOUSE_HPDataSet1;
            // 
            // wARHOUSE_HPDataSet1
            // 
            this.wARHOUSE_HPDataSet1.DataSetName = "WARHOUSE_HPDataSet1";
            this.wARHOUSE_HPDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // goodsTableAdapter
            // 
            this.goodsTableAdapter.ClearBeforeFill = true;
            // 
            // layoutControl3
            // 
            this.layoutControl3.Controls.Add(this.gridControlWRRHeader);
            this.layoutControl3.Location = new System.Drawing.Point(1, 75);
            this.layoutControl3.Margin = new System.Windows.Forms.Padding(2);
            this.layoutControl3.Name = "layoutControl3";
            this.layoutControl3.Root = this.layoutControlGroup2;
            this.layoutControl3.Size = new System.Drawing.Size(722, 376);
            this.layoutControl3.TabIndex = 2;
            this.layoutControl3.Text = "layoutControl3";
            // 
            // gridControlWRRHeader
            // 
            this.gridControlWRRHeader.DataSource = this.wRRHeaderBindingSource;
            this.gridControlWRRHeader.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(2);
            this.gridControlWRRHeader.Location = new System.Drawing.Point(12, 12);
            this.gridControlWRRHeader.MainView = this.gridViewWRRHeader;
            this.gridControlWRRHeader.Margin = new System.Windows.Forms.Padding(2);
            this.gridControlWRRHeader.Name = "gridControlWRRHeader";
            this.gridControlWRRHeader.Size = new System.Drawing.Size(698, 352);
            this.gridControlWRRHeader.TabIndex = 5;
            this.gridControlWRRHeader.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewWRRHeader});
            // 
            // wRRHeaderBindingSource
            // 
            this.wRRHeaderBindingSource.DataMember = "WRRHeader";
            this.wRRHeaderBindingSource.DataSource = this.wARHOUSE_HPDataSet1;
            // 
            // gridViewWRRHeader
            // 
            this.gridViewWRRHeader.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTSNumber,
            this.colTSDate,
            this.colReferenceNumber,
            this.colNote});
            this.gridViewWRRHeader.DetailHeight = 284;
            this.gridViewWRRHeader.GridControl = this.gridControlWRRHeader;
            this.gridViewWRRHeader.Name = "gridViewWRRHeader";
            this.gridViewWRRHeader.OptionsBehavior.Editable = false;
            this.gridViewWRRHeader.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.Click;
            this.gridViewWRRHeader.OptionsBehavior.ReadOnly = true;
            this.gridViewWRRHeader.OptionsView.ShowGroupPanel = false;
            // 
            // colTSNumber
            // 
            this.colTSNumber.Caption = "Số phiếu kiểm kê";
            this.colTSNumber.FieldName = "TSNumber";
            this.colTSNumber.MinWidth = 19;
            this.colTSNumber.Name = "colTSNumber";
            this.colTSNumber.OptionsColumn.AllowEdit = false;
            this.colTSNumber.Visible = true;
            this.colTSNumber.VisibleIndex = 0;
            this.colTSNumber.Width = 195;
            // 
            // colTSDate
            // 
            this.colTSDate.Caption = "Ngày kiểm kê";
            this.colTSDate.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.colTSDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colTSDate.FieldName = "TSDate";
            this.colTSDate.MinWidth = 19;
            this.colTSDate.Name = "colTSDate";
            this.colTSDate.Visible = true;
            this.colTSDate.VisibleIndex = 1;
            this.colTSDate.Width = 214;
            // 
            // colReferenceNumber
            // 
            this.colReferenceNumber.Caption = "Số tham chiếu";
            this.colReferenceNumber.FieldName = "ReferenceNumber";
            this.colReferenceNumber.MinWidth = 19;
            this.colReferenceNumber.Name = "colReferenceNumber";
            this.colReferenceNumber.Visible = true;
            this.colReferenceNumber.VisibleIndex = 2;
            this.colReferenceNumber.Width = 232;
            // 
            // colNote
            // 
            this.colNote.Caption = "Ghi chú";
            this.colNote.FieldName = "Note";
            this.colNote.MinWidth = 19;
            this.colNote.Name = "colNote";
            this.colNote.Visible = true;
            this.colNote.VisibleIndex = 3;
            this.colNote.Width = 331;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3});
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(722, 376);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.gridControlWRRHeader;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(702, 356);
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControl4
            // 
            this.layoutControl4.Controls.Add(this.comboBoxEditBranch);
            this.layoutControl4.Controls.Add(this.dateEditFromDate);
            this.layoutControl4.Controls.Add(this.dateEditToDate);
            this.layoutControl4.Controls.Add(this.comboBoxEditModality);
            this.layoutControl4.Controls.Add(this.comboBoxEditHandlingStatus);
            this.layoutControl4.Controls.Add(this.simpleButtonFilter);
            this.layoutControl4.Location = new System.Drawing.Point(1, 446);
            this.layoutControl4.Margin = new System.Windows.Forms.Padding(2);
            this.layoutControl4.Name = "layoutControl4";
            this.layoutControl4.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1033, 376, 812, 500);
            this.layoutControl4.Root = this.layoutControlGroup3;
            this.layoutControl4.Size = new System.Drawing.Size(722, 98);
            this.layoutControl4.TabIndex = 3;
            this.layoutControl4.Text = "layoutControl4";
            // 
            // comboBoxEditBranch
            // 
            this.comboBoxEditBranch.Location = new System.Drawing.Point(91, 12);
            this.comboBoxEditBranch.Margin = new System.Windows.Forms.Padding(2);
            this.comboBoxEditBranch.Name = "comboBoxEditBranch";
            this.comboBoxEditBranch.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditBranch.Size = new System.Drawing.Size(267, 20);
            this.comboBoxEditBranch.StyleController = this.layoutControl4;
            this.comboBoxEditBranch.TabIndex = 4;
            // 
            // dateEditFromDate
            // 
            this.dateEditFromDate.EditValue = null;
            this.dateEditFromDate.Location = new System.Drawing.Point(91, 38);
            this.dateEditFromDate.Margin = new System.Windows.Forms.Padding(2);
            this.dateEditFromDate.Name = "dateEditFromDate";
            this.dateEditFromDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditFromDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditFromDate.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditFromDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditFromDate.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEditFromDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditFromDate.Size = new System.Drawing.Size(92, 20);
            this.dateEditFromDate.StyleController = this.layoutControl4;
            this.dateEditFromDate.TabIndex = 5;
            // 
            // dateEditToDate
            // 
            this.dateEditToDate.EditValue = null;
            this.dateEditToDate.Location = new System.Drawing.Point(266, 38);
            this.dateEditToDate.Margin = new System.Windows.Forms.Padding(2);
            this.dateEditToDate.Name = "dateEditToDate";
            this.dateEditToDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditToDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditToDate.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditToDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditToDate.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEditToDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditToDate.Size = new System.Drawing.Size(92, 20);
            this.dateEditToDate.StyleController = this.layoutControl4;
            this.dateEditToDate.TabIndex = 6;
            // 
            // comboBoxEditModality
            // 
            this.comboBoxEditModality.Location = new System.Drawing.Point(266, 64);
            this.comboBoxEditModality.Margin = new System.Windows.Forms.Padding(2);
            this.comboBoxEditModality.Name = "comboBoxEditModality";
            this.comboBoxEditModality.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditModality.Size = new System.Drawing.Size(92, 20);
            this.comboBoxEditModality.StyleController = this.layoutControl4;
            this.comboBoxEditModality.TabIndex = 7;
            // 
            // comboBoxEditHandlingStatus
            // 
            this.comboBoxEditHandlingStatus.Location = new System.Drawing.Point(511, 12);
            this.comboBoxEditHandlingStatus.Margin = new System.Windows.Forms.Padding(2);
            this.comboBoxEditHandlingStatus.Name = "comboBoxEditHandlingStatus";
            this.comboBoxEditHandlingStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditHandlingStatus.Size = new System.Drawing.Size(199, 20);
            this.comboBoxEditHandlingStatus.StyleController = this.layoutControl4;
            this.comboBoxEditHandlingStatus.TabIndex = 8;
            // 
            // simpleButtonFilter
            // 
            this.simpleButtonFilter.Location = new System.Drawing.Point(572, 64);
            this.simpleButtonFilter.Margin = new System.Windows.Forms.Padding(2);
            this.simpleButtonFilter.Name = "simpleButtonFilter";
            this.simpleButtonFilter.Size = new System.Drawing.Size(138, 22);
            this.simpleButtonFilter.StyleController = this.layoutControl4;
            this.simpleButtonFilter.TabIndex = 9;
            this.simpleButtonFilter.Text = "Lọc thông tin";
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemBranch,
            this.layoutControlItemFromDate,
            this.layoutControlItemToDate,
            this.layoutControlItemModality,
            this.layoutControlItemHandlingStatus,
            this.layoutControlItem9});
            this.layoutControlGroup3.LayoutMode = DevExpress.XtraLayout.Utils.LayoutMode.Table;
            this.layoutControlGroup3.Name = "Root";
            columnDefinition1.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition1.Width = 25D;
            columnDefinition2.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition2.Width = 25D;
            columnDefinition3.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition3.Width = 10D;
            columnDefinition4.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition4.Width = 20D;
            columnDefinition5.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition5.Width = 20D;
            this.layoutControlGroup3.OptionsTableLayoutGroup.ColumnDefinitions.AddRange(new DevExpress.XtraLayout.ColumnDefinition[] {
            columnDefinition1,
            columnDefinition2,
            columnDefinition3,
            columnDefinition4,
            columnDefinition5});
            rowDefinition1.Height = 33.333333333333336D;
            rowDefinition1.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition2.Height = 33.333333333333336D;
            rowDefinition2.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition3.Height = 33.333333333333336D;
            rowDefinition3.SizeType = System.Windows.Forms.SizeType.Percent;
            this.layoutControlGroup3.OptionsTableLayoutGroup.RowDefinitions.AddRange(new DevExpress.XtraLayout.RowDefinition[] {
            rowDefinition1,
            rowDefinition2,
            rowDefinition3});
            this.layoutControlGroup3.Size = new System.Drawing.Size(722, 98);
            this.layoutControlGroup3.TextVisible = false;
            // 
            // layoutControlItemBranch
            // 
            this.layoutControlItemBranch.Control = this.comboBoxEditBranch;
            this.layoutControlItemBranch.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemBranch.Name = "layoutControlItemBranch";
            this.layoutControlItemBranch.OptionsTableLayoutItem.ColumnSpan = 2;
            this.layoutControlItemBranch.Size = new System.Drawing.Size(350, 26);
            this.layoutControlItemBranch.Text = "Chi nhánh";
            this.layoutControlItemBranch.TextSize = new System.Drawing.Size(76, 13);
            // 
            // layoutControlItemFromDate
            // 
            this.layoutControlItemFromDate.Control = this.dateEditFromDate;
            this.layoutControlItemFromDate.Location = new System.Drawing.Point(0, 26);
            this.layoutControlItemFromDate.Name = "layoutControlItemFromDate";
            this.layoutControlItemFromDate.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItemFromDate.Size = new System.Drawing.Size(175, 26);
            this.layoutControlItemFromDate.Text = "Từ ngày";
            this.layoutControlItemFromDate.TextSize = new System.Drawing.Size(76, 13);
            // 
            // layoutControlItemToDate
            // 
            this.layoutControlItemToDate.Control = this.dateEditToDate;
            this.layoutControlItemToDate.Location = new System.Drawing.Point(175, 26);
            this.layoutControlItemToDate.Name = "layoutControlItemToDate";
            this.layoutControlItemToDate.OptionsTableLayoutItem.ColumnIndex = 1;
            this.layoutControlItemToDate.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItemToDate.Size = new System.Drawing.Size(175, 26);
            this.layoutControlItemToDate.Text = "Đến ngày";
            this.layoutControlItemToDate.TextSize = new System.Drawing.Size(76, 13);
            // 
            // layoutControlItemModality
            // 
            this.layoutControlItemModality.Control = this.comboBoxEditModality;
            this.layoutControlItemModality.Location = new System.Drawing.Point(175, 52);
            this.layoutControlItemModality.Name = "layoutControlItemModality";
            this.layoutControlItemModality.OptionsTableLayoutItem.ColumnIndex = 1;
            this.layoutControlItemModality.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlItemModality.Size = new System.Drawing.Size(175, 26);
            this.layoutControlItemModality.Text = "Phương thức";
            this.layoutControlItemModality.TextSize = new System.Drawing.Size(76, 13);
            // 
            // layoutControlItemHandlingStatus
            // 
            this.layoutControlItemHandlingStatus.Control = this.comboBoxEditHandlingStatus;
            this.layoutControlItemHandlingStatus.Location = new System.Drawing.Point(420, 0);
            this.layoutControlItemHandlingStatus.Name = "layoutControlItemHandlingStatus";
            this.layoutControlItemHandlingStatus.OptionsTableLayoutItem.ColumnIndex = 3;
            this.layoutControlItemHandlingStatus.OptionsTableLayoutItem.ColumnSpan = 2;
            this.layoutControlItemHandlingStatus.Size = new System.Drawing.Size(282, 26);
            this.layoutControlItemHandlingStatus.Text = "Trạng thái xử lý";
            this.layoutControlItemHandlingStatus.TextSize = new System.Drawing.Size(76, 13);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.simpleButtonFilter;
            this.layoutControlItem9.Location = new System.Drawing.Point(560, 52);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.OptionsTableLayoutItem.ColumnIndex = 4;
            this.layoutControlItem9.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlItem9.Size = new System.Drawing.Size(142, 26);
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextVisible = false;
            // 
            // wRRHeaderTableAdapter
            // 
            this.wRRHeaderTableAdapter.ClearBeforeFill = true;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(7, 7, 8, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(720, 37);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.searchControl11;
            this.layoutControlItem2.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(706, 29);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // searchControl11
            // 
            this.searchControl11.Client = this.gridControlWRRHeader;
            this.searchControl11.Location = new System.Drawing.Point(9, 10);
            this.searchControl11.Margin = new System.Windows.Forms.Padding(2);
            this.searchControl11.Name = "searchControl11";
            this.searchControl11.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Repository.ClearButton(),
            new DevExpress.XtraEditors.Repository.SearchButton()});
            this.searchControl11.Properties.Client = this.gridControlWRRHeader;
            this.searchControl11.Size = new System.Drawing.Size(702, 20);
            this.searchControl11.StyleController = this.layoutControl2;
            this.searchControl11.TabIndex = 5;
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.searchControl11);
            this.layoutControl2.Location = new System.Drawing.Point(1, 33);
            this.layoutControl2.Margin = new System.Windows.Forms.Padding(2);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(554, 64, 812, 436);
            this.layoutControl2.Root = this.layoutControlGroup1;
            this.layoutControl2.Size = new System.Drawing.Size(720, 37);
            this.layoutControl2.TabIndex = 1;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // frmSearchTSH
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(723, 560);
            this.Controls.Add(this.layoutControl4);
            this.Controls.Add(this.layoutControl3);
            this.Controls.Add(this.layoutControl2);
            this.Controls.Add(this.layoutControl1);
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "frmSearchTSH";
            this.Text = "frmSearchGoods";
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.goodsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wARHOUSE_HPDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).EndInit();
            this.layoutControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlWRRHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wRRHeaderBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewWRRHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl4)).EndInit();
            this.layoutControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditBranch.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditModality.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditHandlingStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemBranch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemFromDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemToDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemModality)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemHandlingStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchControl11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private WARHOUSE_HPDataSet1 wARHOUSE_HPDataSet1;
        private System.Windows.Forms.BindingSource goodsBindingSource;
        private WARHOUSE_HPDataSet1TableAdapters.GoodsTableAdapter goodsTableAdapter;
        private DevExpress.XtraLayout.LayoutControl layoutControl3;
        private DevExpress.XtraGrid.GridControl gridControlWRRHeader;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewWRRHeader;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControl layoutControl4;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditBranch;
        private DevExpress.XtraEditors.DateEdit dateEditFromDate;
        private DevExpress.XtraEditors.DateEdit dateEditToDate;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditModality;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditHandlingStatus;
        private DevExpress.XtraEditors.SimpleButton simpleButtonFilter;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemBranch;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemFromDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemToDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemModality;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemHandlingStatus;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private System.Windows.Forms.BindingSource wRRHeaderBindingSource;
        private WARHOUSE_HPDataSet1TableAdapters.WRRHeaderTableAdapter wRRHeaderTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colTSNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colTSDate;
        private DevExpress.XtraGrid.Columns.GridColumn colReferenceNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colNote;
        private DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel windowsUIButtonPanel1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.SearchControl searchControl11;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
    }
}