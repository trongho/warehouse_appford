﻿
namespace WarehouseManager
{
    partial class ReceiptRequisitionControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions1 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReceiptRequisitionControl));
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions2 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions3 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions4 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions5 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions6 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions7 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions8 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions9 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition1 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition2 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition3 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition4 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition5 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition6 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition1 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition2 = new DevExpress.XtraLayout.RowDefinition();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.windowsUIButtonPanel1 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.label1 = new System.Windows.Forms.Label();
            this.textEditWRRNumber = new DevExpress.XtraEditors.TextEdit();
            this.dateEditWRRDate = new DevExpress.XtraEditors.DateEdit();
            this.textEditTotalQuantityByItem = new DevExpress.XtraEditors.TextEdit();
            this.textEditTotalQuantityByPack = new DevExpress.XtraEditors.TextEdit();
            this.comboBoxEditHandlingStatus = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEditBranch = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit2 = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemWRRNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemBranch = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemWRRDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemHandlingStatus = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemTotalQuantityByItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemTotalQuantityByPack = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.wRRDetailBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.wARHOUSE_HPDataSet1 = new WarehouseManager.WARHOUSE_HPDataSet1();
            this.wRRDetailTableAdapter = new WarehouseManager.WARHOUSE_HPDataSet1TableAdapters.WRRDetailTableAdapter();
            this.layoutControl3 = new DevExpress.XtraLayout.LayoutControl();
            this.gridControlWRRDetail = new DevExpress.XtraGrid.GridControl();
            this.gridViewWRRDetail = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colWRRNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrdinal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGoodsID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGoodsName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOtherGoodsName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPackingVolume = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalQuantity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantityByItem = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantityByPack = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocationID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colScanOption = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNote = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWRRDetailNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSupplierCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colASNNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPackingSlip = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantityReceived = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReceiptStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSLPart = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditWRRNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditWRRDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditWRRDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalQuantityByItem.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalQuantityByPack.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditHandlingStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditBranch.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWRRNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemBranch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWRRDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemHandlingStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalQuantityByItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalQuantityByPack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wRRDetailBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wARHOUSE_HPDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).BeginInit();
            this.layoutControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlWRRDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewWRRDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.windowsUIButtonPanel1);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(400, 144, 812, 500);
            this.layoutControl1.Root = this.Root;
            this.layoutControl1.Size = new System.Drawing.Size(1240, 37);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // windowsUIButtonPanel1
            // 
            this.windowsUIButtonPanel1.AllowGlyphSkinning = false;
            this.windowsUIButtonPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            windowsUIButtonImageOptions1.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions1.Image")));
            windowsUIButtonImageOptions1.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions2.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions2.Image")));
            windowsUIButtonImageOptions2.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions3.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions3.Image")));
            windowsUIButtonImageOptions3.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions4.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions4.Image")));
            windowsUIButtonImageOptions4.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions5.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions5.Image")));
            windowsUIButtonImageOptions5.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions6.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions6.Image")));
            windowsUIButtonImageOptions6.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions7.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions7.Image")));
            windowsUIButtonImageOptions7.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions8.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions8.Image")));
            windowsUIButtonImageOptions8.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions9.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions9.Image")));
            windowsUIButtonImageOptions9.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            this.windowsUIButtonPanel1.Buttons.AddRange(new DevExpress.XtraEditors.ButtonPanel.IBaseButton[] {
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F2) Thêm mới", true, windowsUIButtonImageOptions1, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "new", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F3) Lưu", true, windowsUIButtonImageOptions2, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "save", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F4) Xóa", true, windowsUIButtonImageOptions3, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "delete", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F5) Tìm kiếm", true, windowsUIButtonImageOptions4, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "search", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F6) Làm mới", true, windowsUIButtonImageOptions5, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "refesh", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F7) Nhập dữ liệu", true, windowsUIButtonImageOptions6, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "import", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F8) Xuất dữ liệu", true, windowsUIButtonImageOptions7, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "export", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F9) In ấn", true, windowsUIButtonImageOptions8, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "print", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F10) Thoát", true, windowsUIButtonImageOptions9, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "close", -1, false)});
            this.windowsUIButtonPanel1.ContentAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.windowsUIButtonPanel1.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.windowsUIButtonPanel1.Location = new System.Drawing.Point(2, 2);
            this.windowsUIButtonPanel1.Name = "windowsUIButtonPanel1";
            this.windowsUIButtonPanel1.Padding = new System.Windows.Forms.Padding(5);
            this.windowsUIButtonPanel1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.windowsUIButtonPanel1.Size = new System.Drawing.Size(1236, 32);
            this.windowsUIButtonPanel1.TabIndex = 4;
            this.windowsUIButtonPanel1.Text = "windowsUIButtonPanel1";
            this.windowsUIButtonPanel1.UseButtonBackgroundImages = false;
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.Root.Name = "Root";
            this.Root.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.Root.Size = new System.Drawing.Size(1240, 37);
            this.Root.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.windowsUIButtonPanel1;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(1240, 37);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.label1);
            this.layoutControl2.Controls.Add(this.textEditWRRNumber);
            this.layoutControl2.Controls.Add(this.dateEditWRRDate);
            this.layoutControl2.Controls.Add(this.textEditTotalQuantityByItem);
            this.layoutControl2.Controls.Add(this.textEditTotalQuantityByPack);
            this.layoutControl2.Controls.Add(this.comboBoxEditHandlingStatus);
            this.layoutControl2.Controls.Add(this.comboBoxEditBranch);
            this.layoutControl2.Controls.Add(this.textEdit1);
            this.layoutControl2.Controls.Add(this.textEdit2);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl2.Location = new System.Drawing.Point(0, 37);
            this.layoutControl2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(879, 290, 812, 500);
            this.layoutControl2.Root = this.layoutControlGroup1;
            this.layoutControl2.Size = new System.Drawing.Size(1240, 79);
            this.layoutControl2.TabIndex = 1;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // label1
            // 
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(248, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 25);
            this.label1.TabIndex = 6;
            this.label1.Text = "(*)";
            // 
            // textEditWRRNumber
            // 
            this.textEditWRRNumber.Location = new System.Drawing.Point(134, 12);
            this.textEditWRRNumber.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textEditWRRNumber.Name = "textEditWRRNumber";
            this.textEditWRRNumber.Size = new System.Drawing.Size(110, 20);
            this.textEditWRRNumber.StyleController = this.layoutControl2;
            this.textEditWRRNumber.TabIndex = 4;
            // 
            // dateEditWRRDate
            // 
            this.dateEditWRRDate.EditValue = null;
            this.dateEditWRRDate.Location = new System.Drawing.Point(385, 12);
            this.dateEditWRRDate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dateEditWRRDate.Name = "dateEditWRRDate";
            this.dateEditWRRDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditWRRDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditWRRDate.Properties.CalendarTimeProperties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditWRRDate.Properties.CalendarTimeProperties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditWRRDate.Properties.CalendarTimeProperties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEditWRRDate.Properties.CalendarTimeProperties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditWRRDate.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditWRRDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditWRRDate.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEditWRRDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditWRRDate.Size = new System.Drawing.Size(132, 20);
            this.dateEditWRRDate.StyleController = this.layoutControl2;
            this.dateEditWRRDate.TabIndex = 7;
            // 
            // textEditTotalQuantityByItem
            // 
            this.textEditTotalQuantityByItem.Location = new System.Drawing.Point(859, 12);
            this.textEditTotalQuantityByItem.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textEditTotalQuantityByItem.Name = "textEditTotalQuantityByItem";
            this.textEditTotalQuantityByItem.Size = new System.Drawing.Size(132, 20);
            this.textEditTotalQuantityByItem.StyleController = this.layoutControl2;
            this.textEditTotalQuantityByItem.TabIndex = 10;
            // 
            // textEditTotalQuantityByPack
            // 
            this.textEditTotalQuantityByPack.Location = new System.Drawing.Point(859, 41);
            this.textEditTotalQuantityByPack.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textEditTotalQuantityByPack.Name = "textEditTotalQuantityByPack";
            this.textEditTotalQuantityByPack.Size = new System.Drawing.Size(132, 20);
            this.textEditTotalQuantityByPack.StyleController = this.layoutControl2;
            this.textEditTotalQuantityByPack.TabIndex = 11;
            // 
            // comboBoxEditHandlingStatus
            // 
            this.comboBoxEditHandlingStatus.Location = new System.Drawing.Point(385, 41);
            this.comboBoxEditHandlingStatus.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboBoxEditHandlingStatus.Name = "comboBoxEditHandlingStatus";
            this.comboBoxEditHandlingStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditHandlingStatus.Size = new System.Drawing.Size(132, 20);
            this.comboBoxEditHandlingStatus.StyleController = this.layoutControl2;
            this.comboBoxEditHandlingStatus.TabIndex = 13;
            // 
            // comboBoxEditBranch
            // 
            this.comboBoxEditBranch.Location = new System.Drawing.Point(113, 41);
            this.comboBoxEditBranch.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboBoxEditBranch.Name = "comboBoxEditBranch";
            this.comboBoxEditBranch.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditBranch.Size = new System.Drawing.Size(131, 20);
            this.comboBoxEditBranch.StyleController = this.layoutControl2;
            this.comboBoxEditBranch.TabIndex = 14;
            // 
            // textEdit1
            // 
            this.textEdit1.Location = new System.Drawing.Point(1096, 12);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Size = new System.Drawing.Size(132, 20);
            this.textEdit1.StyleController = this.layoutControl2;
            this.textEdit1.TabIndex = 15;
            // 
            // textEdit2
            // 
            this.textEdit2.Location = new System.Drawing.Point(1096, 41);
            this.textEdit2.Name = "textEdit2";
            this.textEdit2.Size = new System.Drawing.Size(132, 20);
            this.textEdit2.StyleController = this.layoutControl2;
            this.textEdit2.TabIndex = 16;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemWRRNumber,
            this.layoutControlItem3,
            this.layoutControlItemBranch,
            this.layoutControlItemWRRDate,
            this.layoutControlItemHandlingStatus,
            this.layoutControlItemTotalQuantityByItem,
            this.layoutControlItemTotalQuantityByPack,
            this.layoutControlItem4,
            this.layoutControlItem5});
            this.layoutControlGroup1.LayoutMode = DevExpress.XtraLayout.Utils.LayoutMode.Table;
            this.layoutControlGroup1.Name = "Root";
            columnDefinition1.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition1.Width = 19.405592722871681D;
            columnDefinition2.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition2.Width = 2.9720363856415997D;
            columnDefinition3.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition3.Width = 19.405592722871681D;
            columnDefinition4.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition4.Width = 19.405592722871681D;
            columnDefinition5.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition5.Width = 19.405592722871681D;
            columnDefinition6.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition6.Width = 19.405592722871681D;
            this.layoutControlGroup1.OptionsTableLayoutGroup.ColumnDefinitions.AddRange(new DevExpress.XtraLayout.ColumnDefinition[] {
            columnDefinition1,
            columnDefinition2,
            columnDefinition3,
            columnDefinition4,
            columnDefinition5,
            columnDefinition6});
            rowDefinition1.Height = 50D;
            rowDefinition1.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition2.Height = 50D;
            rowDefinition2.SizeType = System.Windows.Forms.SizeType.Percent;
            this.layoutControlGroup1.OptionsTableLayoutGroup.RowDefinitions.AddRange(new DevExpress.XtraLayout.RowDefinition[] {
            rowDefinition1,
            rowDefinition2});
            this.layoutControlGroup1.Size = new System.Drawing.Size(1240, 79);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItemWRRNumber
            // 
            this.layoutControlItemWRRNumber.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.layoutControlItemWRRNumber.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItemWRRNumber.Control = this.textEditWRRNumber;
            this.layoutControlItemWRRNumber.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemWRRNumber.Name = "layoutControlItemWRRNumber";
            this.layoutControlItemWRRNumber.Size = new System.Drawing.Size(236, 29);
            this.layoutControlItemWRRNumber.Text = "Conveyance Number";
            this.layoutControlItemWRRNumber.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItemWRRNumber.TextSize = new System.Drawing.Size(117, 16);
            this.layoutControlItemWRRNumber.TextToControlDistance = 5;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.label1;
            this.layoutControlItem3.Location = new System.Drawing.Point(236, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.OptionsTableLayoutItem.ColumnIndex = 1;
            this.layoutControlItem3.Size = new System.Drawing.Size(36, 29);
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItemBranch
            // 
            this.layoutControlItemBranch.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.layoutControlItemBranch.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItemBranch.Control = this.comboBoxEditBranch;
            this.layoutControlItemBranch.Location = new System.Drawing.Point(0, 29);
            this.layoutControlItemBranch.Name = "layoutControlItemBranch";
            this.layoutControlItemBranch.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItemBranch.Size = new System.Drawing.Size(236, 30);
            this.layoutControlItemBranch.Text = "Chi nhánh";
            this.layoutControlItemBranch.TextSize = new System.Drawing.Size(98, 16);
            // 
            // layoutControlItemWRRDate
            // 
            this.layoutControlItemWRRDate.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.layoutControlItemWRRDate.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItemWRRDate.Control = this.dateEditWRRDate;
            this.layoutControlItemWRRDate.Location = new System.Drawing.Point(272, 0);
            this.layoutControlItemWRRDate.Name = "layoutControlItemWRRDate";
            this.layoutControlItemWRRDate.OptionsTableLayoutItem.ColumnIndex = 2;
            this.layoutControlItemWRRDate.Size = new System.Drawing.Size(237, 29);
            this.layoutControlItemWRRDate.Text = "Ngày yêu cầu";
            this.layoutControlItemWRRDate.TextSize = new System.Drawing.Size(98, 17);
            // 
            // layoutControlItemHandlingStatus
            // 
            this.layoutControlItemHandlingStatus.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.layoutControlItemHandlingStatus.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItemHandlingStatus.Control = this.comboBoxEditHandlingStatus;
            this.layoutControlItemHandlingStatus.Location = new System.Drawing.Point(272, 29);
            this.layoutControlItemHandlingStatus.Name = "layoutControlItemHandlingStatus";
            this.layoutControlItemHandlingStatus.OptionsTableLayoutItem.ColumnIndex = 2;
            this.layoutControlItemHandlingStatus.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItemHandlingStatus.Size = new System.Drawing.Size(237, 30);
            this.layoutControlItemHandlingStatus.Text = "Trạng thái xử lý";
            this.layoutControlItemHandlingStatus.TextSize = new System.Drawing.Size(98, 17);
            // 
            // layoutControlItemTotalQuantityByItem
            // 
            this.layoutControlItemTotalQuantityByItem.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.layoutControlItemTotalQuantityByItem.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItemTotalQuantityByItem.Control = this.textEditTotalQuantityByItem;
            this.layoutControlItemTotalQuantityByItem.Location = new System.Drawing.Point(746, 0);
            this.layoutControlItemTotalQuantityByItem.Name = "layoutControlItemTotalQuantityByItem";
            this.layoutControlItemTotalQuantityByItem.OptionsTableLayoutItem.ColumnIndex = 4;
            this.layoutControlItemTotalQuantityByItem.Size = new System.Drawing.Size(237, 29);
            this.layoutControlItemTotalQuantityByItem.Text = "∑ SL yêu cầu";
            this.layoutControlItemTotalQuantityByItem.TextSize = new System.Drawing.Size(98, 17);
            // 
            // layoutControlItemTotalQuantityByPack
            // 
            this.layoutControlItemTotalQuantityByPack.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.layoutControlItemTotalQuantityByPack.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItemTotalQuantityByPack.Control = this.textEditTotalQuantityByPack;
            this.layoutControlItemTotalQuantityByPack.CustomizationFormText = "Σ Mã yêu cầu";
            this.layoutControlItemTotalQuantityByPack.Location = new System.Drawing.Point(746, 29);
            this.layoutControlItemTotalQuantityByPack.Name = "layoutControlItemTotalQuantityByPack";
            this.layoutControlItemTotalQuantityByPack.OptionsTableLayoutItem.ColumnIndex = 4;
            this.layoutControlItemTotalQuantityByPack.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItemTotalQuantityByPack.Size = new System.Drawing.Size(237, 30);
            this.layoutControlItemTotalQuantityByPack.Text = "Σ Mã yêu cầu";
            this.layoutControlItemTotalQuantityByPack.TextSize = new System.Drawing.Size(98, 17);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.layoutControlItem4.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem4.Control = this.textEdit1;
            this.layoutControlItem4.Location = new System.Drawing.Point(983, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.OptionsTableLayoutItem.ColumnIndex = 5;
            this.layoutControlItem4.Size = new System.Drawing.Size(237, 29);
            this.layoutControlItem4.Text = "∑ SL thực nhận";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(98, 17);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.layoutControlItem5.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem5.Control = this.textEdit2;
            this.layoutControlItem5.Location = new System.Drawing.Point(983, 29);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.OptionsTableLayoutItem.ColumnIndex = 5;
            this.layoutControlItem5.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItem5.Size = new System.Drawing.Size(237, 30);
            this.layoutControlItem5.Text = "∑ Mã thực nhận";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(98, 17);
            // 
            // wRRDetailBindingSource
            // 
            this.wRRDetailBindingSource.DataMember = "WRRDetail";
            this.wRRDetailBindingSource.DataSource = this.wARHOUSE_HPDataSet1;
            // 
            // wARHOUSE_HPDataSet1
            // 
            this.wARHOUSE_HPDataSet1.DataSetName = "WARHOUSE_HPDataSet1";
            this.wARHOUSE_HPDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // wRRDetailTableAdapter
            // 
            this.wRRDetailTableAdapter.ClearBeforeFill = true;
            // 
            // layoutControl3
            // 
            this.layoutControl3.Controls.Add(this.gridControlWRRDetail);
            this.layoutControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl3.Location = new System.Drawing.Point(0, 116);
            this.layoutControl3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.layoutControl3.Name = "layoutControl3";
            this.layoutControl3.Root = this.layoutControlGroup2;
            this.layoutControl3.Size = new System.Drawing.Size(1240, 530);
            this.layoutControl3.TabIndex = 2;
            this.layoutControl3.Text = "layoutControl3";
            // 
            // gridControlWRRDetail
            // 
            this.gridControlWRRDetail.DataSource = this.wRRDetailBindingSource;
            this.gridControlWRRDetail.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gridControlWRRDetail.Location = new System.Drawing.Point(12, 12);
            this.gridControlWRRDetail.MainView = this.gridViewWRRDetail;
            this.gridControlWRRDetail.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gridControlWRRDetail.Name = "gridControlWRRDetail";
            this.gridControlWRRDetail.Size = new System.Drawing.Size(1216, 506);
            this.gridControlWRRDetail.TabIndex = 5;
            this.gridControlWRRDetail.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewWRRDetail});
            // 
            // gridViewWRRDetail
            // 
            this.gridViewWRRDetail.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colWRRNumber,
            this.colOrdinal,
            this.colGoodsID,
            this.colGoodsName,
            this.colOtherGoodsName,
            this.colPackingVolume,
            this.colTotalQuantity,
            this.colQuantityByItem,
            this.colQuantityByPack,
            this.colLocationID,
            this.colScanOption,
            this.colNote,
            this.colStatus,
            this.colWRRDetailNo,
            this.colSupplierCode,
            this.colASNNumber,
            this.colPackingSlip,
            this.colQuantityReceived,
            this.colReceiptStatus,
            this.colSLPart,
            this.gridColumn1});
            this.gridViewWRRDetail.DetailHeight = 284;
            this.gridViewWRRDetail.GridControl = this.gridControlWRRDetail;
            this.gridViewWRRDetail.Name = "gridViewWRRDetail";
            this.gridViewWRRDetail.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.True;
            this.gridViewWRRDetail.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.Click;
            this.gridViewWRRDetail.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom;
            this.gridViewWRRDetail.OptionsView.RowAutoHeight = true;
            this.gridViewWRRDetail.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colQuantityByPack, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colWRRNumber
            // 
            this.colWRRNumber.AppearanceHeader.Options.UseTextOptions = true;
            this.colWRRNumber.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.colWRRNumber.FieldName = "WRRNumber";
            this.colWRRNumber.MinWidth = 21;
            this.colWRRNumber.Name = "colWRRNumber";
            this.colWRRNumber.Width = 81;
            // 
            // colOrdinal
            // 
            this.colOrdinal.FieldName = "Ordinal";
            this.colOrdinal.MinWidth = 21;
            this.colOrdinal.Name = "colOrdinal";
            this.colOrdinal.Width = 81;
            // 
            // colGoodsID
            // 
            this.colGoodsID.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colGoodsID.AppearanceCell.Options.UseFont = true;
            this.colGoodsID.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colGoodsID.AppearanceHeader.Options.UseFont = true;
            this.colGoodsID.Caption = "PART NUMBER";
            this.colGoodsID.FieldName = "GoodsID";
            this.colGoodsID.MinWidth = 86;
            this.colGoodsID.Name = "colGoodsID";
            this.colGoodsID.Visible = true;
            this.colGoodsID.VisibleIndex = 1;
            this.colGoodsID.Width = 87;
            // 
            // colGoodsName
            // 
            this.colGoodsName.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colGoodsName.AppearanceCell.Options.UseFont = true;
            this.colGoodsName.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colGoodsName.AppearanceHeader.Options.UseFont = true;
            this.colGoodsName.Caption = "PART NAME";
            this.colGoodsName.FieldName = "GoodsName";
            this.colGoodsName.MinWidth = 129;
            this.colGoodsName.Name = "colGoodsName";
            this.colGoodsName.Visible = true;
            this.colGoodsName.VisibleIndex = 2;
            this.colGoodsName.Width = 138;
            // 
            // colOtherGoodsName
            // 
            this.colOtherGoodsName.Caption = "Tên khác của hàng hóa";
            this.colOtherGoodsName.FieldName = "OtherGoodsName";
            this.colOtherGoodsName.MinWidth = 86;
            this.colOtherGoodsName.Name = "colOtherGoodsName";
            this.colOtherGoodsName.Width = 214;
            // 
            // colPackingVolume
            // 
            this.colPackingVolume.Caption = "VOL PACK";
            this.colPackingVolume.FieldName = "PackingVolume";
            this.colPackingVolume.MinWidth = 86;
            this.colPackingVolume.Name = "colPackingVolume";
            this.colPackingVolume.Width = 86;
            // 
            // colTotalQuantity
            // 
            this.colTotalQuantity.Caption = "Tổng lượng";
            this.colTotalQuantity.FieldName = "TotalQuantity";
            this.colTotalQuantity.MinWidth = 86;
            this.colTotalQuantity.Name = "colTotalQuantity";
            this.colTotalQuantity.Width = 86;
            // 
            // colQuantityByItem
            // 
            this.colQuantityByItem.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colQuantityByItem.AppearanceCell.Options.UseFont = true;
            this.colQuantityByItem.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colQuantityByItem.AppearanceHeader.Options.UseFont = true;
            this.colQuantityByItem.Caption = "SL ASN";
            this.colQuantityByItem.FieldName = "QuantityByItem";
            this.colQuantityByItem.MinWidth = 86;
            this.colQuantityByItem.Name = "colQuantityByItem";
            this.colQuantityByItem.Visible = true;
            this.colQuantityByItem.VisibleIndex = 4;
            this.colQuantityByItem.Width = 86;
            // 
            // colQuantityByPack
            // 
            this.colQuantityByPack.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colQuantityByPack.AppearanceCell.Options.UseFont = true;
            this.colQuantityByPack.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colQuantityByPack.AppearanceHeader.Options.UseFont = true;
            this.colQuantityByPack.Caption = "# of Containers / Coils";
            this.colQuantityByPack.FieldName = "QuantityByPack";
            this.colQuantityByPack.MinWidth = 30;
            this.colQuantityByPack.Name = "colQuantityByPack";
            this.colQuantityByPack.Visible = true;
            this.colQuantityByPack.VisibleIndex = 6;
            this.colQuantityByPack.Width = 129;
            // 
            // colLocationID
            // 
            this.colLocationID.Caption = "Vị trí";
            this.colLocationID.FieldName = "LocationID";
            this.colLocationID.MinWidth = 86;
            this.colLocationID.Name = "colLocationID";
            this.colLocationID.Width = 91;
            // 
            // colScanOption
            // 
            this.colScanOption.Caption = "Tùy chọn quét";
            this.colScanOption.FieldName = "ScanOption";
            this.colScanOption.MinWidth = 86;
            this.colScanOption.Name = "colScanOption";
            this.colScanOption.Width = 86;
            // 
            // colNote
            // 
            this.colNote.Caption = "Ghi chú";
            this.colNote.FieldName = "Note";
            this.colNote.MinWidth = 86;
            this.colNote.Name = "colNote";
            this.colNote.Width = 145;
            // 
            // colStatus
            // 
            this.colStatus.FieldName = "Status";
            this.colStatus.MinWidth = 21;
            this.colStatus.Name = "colStatus";
            this.colStatus.Width = 100;
            // 
            // colWRRDetailNo
            // 
            this.colWRRDetailNo.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colWRRDetailNo.AppearanceCell.Options.UseFont = true;
            this.colWRRDetailNo.AppearanceCell.Options.UseTextOptions = true;
            this.colWRRDetailNo.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colWRRDetailNo.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colWRRDetailNo.AppearanceHeader.Options.UseFont = true;
            this.colWRRDetailNo.AppearanceHeader.Options.UseTextOptions = true;
            this.colWRRDetailNo.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colWRRDetailNo.Caption = "NO";
            this.colWRRDetailNo.FieldName = "WRRDetailNo";
            this.colWRRDetailNo.Name = "colWRRDetailNo";
            this.colWRRDetailNo.Visible = true;
            this.colWRRDetailNo.VisibleIndex = 0;
            this.colWRRDetailNo.Width = 24;
            // 
            // colSupplierCode
            // 
            this.colSupplierCode.Caption = "SUPPLIER CODE";
            this.colSupplierCode.FieldName = "SupplierCode";
            this.colSupplierCode.MinWidth = 21;
            this.colSupplierCode.Name = "colSupplierCode";
            this.colSupplierCode.Width = 105;
            // 
            // colASNNumber
            // 
            this.colASNNumber.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colASNNumber.AppearanceCell.Options.UseFont = true;
            this.colASNNumber.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colASNNumber.AppearanceHeader.Options.UseFont = true;
            this.colASNNumber.Caption = "ASN Number";
            this.colASNNumber.FieldName = "ASNNumber";
            this.colASNNumber.MinWidth = 60;
            this.colASNNumber.Name = "colASNNumber";
            this.colASNNumber.Visible = true;
            this.colASNNumber.VisibleIndex = 3;
            this.colASNNumber.Width = 60;
            // 
            // colPackingSlip
            // 
            this.colPackingSlip.Caption = "PACKING SLIP";
            this.colPackingSlip.FieldName = "PackingSlip";
            this.colPackingSlip.MinWidth = 21;
            this.colPackingSlip.Name = "colPackingSlip";
            this.colPackingSlip.Width = 86;
            // 
            // colQuantityReceived
            // 
            this.colQuantityReceived.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colQuantityReceived.AppearanceCell.Options.UseFont = true;
            this.colQuantityReceived.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colQuantityReceived.AppearanceHeader.Options.UseFont = true;
            this.colQuantityReceived.Caption = "SL Thực Nhận";
            this.colQuantityReceived.FieldName = "QuantityReceived";
            this.colQuantityReceived.MinWidth = 60;
            this.colQuantityReceived.Name = "colQuantityReceived";
            this.colQuantityReceived.Visible = true;
            this.colQuantityReceived.VisibleIndex = 5;
            this.colQuantityReceived.Width = 143;
            // 
            // colReceiptStatus
            // 
            this.colReceiptStatus.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colReceiptStatus.AppearanceCell.Options.UseFont = true;
            this.colReceiptStatus.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colReceiptStatus.AppearanceHeader.Options.UseFont = true;
            this.colReceiptStatus.Caption = "Whse";
            this.colReceiptStatus.FieldName = "ReceiptStatus";
            this.colReceiptStatus.MinWidth = 40;
            this.colReceiptStatus.Name = "colReceiptStatus";
            this.colReceiptStatus.Visible = true;
            this.colReceiptStatus.VisibleIndex = 9;
            this.colReceiptStatus.Width = 40;
            // 
            // colSLPart
            // 
            this.colSLPart.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colSLPart.AppearanceCell.Options.UseFont = true;
            this.colSLPart.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colSLPart.AppearanceHeader.Options.UseFont = true;
            this.colSLPart.Caption = "SL Part";
            this.colSLPart.FieldName = "SLPart";
            this.colSLPart.MinWidth = 40;
            this.colSLPart.Name = "colSLPart";
            this.colSLPart.Visible = true;
            this.colSLPart.VisibleIndex = 8;
            this.colSLPart.Width = 40;
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.gridColumn1.AppearanceCell.Options.UseFont = true;
            this.gridColumn1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10F);
            this.gridColumn1.AppearanceHeader.Options.UseFont = true;
            this.gridColumn1.Caption = "SL kiện thực nhận";
            this.gridColumn1.MinWidth = 80;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 7;
            this.gridColumn1.Width = 80;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2});
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(1240, 530);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.gridControlWRRDetail;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(1220, 510);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // ReceiptRequisitionControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl3);
            this.Controls.Add(this.layoutControl2);
            this.Controls.Add(this.layoutControl1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "ReceiptRequisitionControl";
            this.Size = new System.Drawing.Size(1240, 646);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEditWRRNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditWRRDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditWRRDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalQuantityByItem.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalQuantityByPack.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditHandlingStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditBranch.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWRRNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemBranch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWRRDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemHandlingStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalQuantityByItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalQuantityByPack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wRRDetailBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wARHOUSE_HPDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).EndInit();
            this.layoutControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlWRRDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewWRRDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel windowsUIButtonPanel1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraEditors.TextEdit textEditWRRNumber;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemWRRNumber;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.DateEdit dateEditWRRDate;
        private DevExpress.XtraEditors.TextEdit textEditTotalQuantityByItem;
        private DevExpress.XtraEditors.TextEdit textEditTotalQuantityByPack;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private System.Windows.Forms.BindingSource wRRDetailBindingSource;
        private WARHOUSE_HPDataSet1 wARHOUSE_HPDataSet1;
        private WARHOUSE_HPDataSet1TableAdapters.WRRDetailTableAdapter wRRDetailTableAdapter;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditHandlingStatus;
        private DevExpress.XtraLayout.LayoutControl layoutControl3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraGrid.GridControl gridControlWRRDetail;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewWRRDetail;
        private DevExpress.XtraGrid.Columns.GridColumn colWRRNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colOrdinal;
        private DevExpress.XtraGrid.Columns.GridColumn colGoodsID;
        private DevExpress.XtraGrid.Columns.GridColumn colGoodsName;
        private DevExpress.XtraGrid.Columns.GridColumn colOtherGoodsName;
        private DevExpress.XtraGrid.Columns.GridColumn colPackingVolume;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalQuantity;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantityByItem;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantityByPack;
        private DevExpress.XtraGrid.Columns.GridColumn colLocationID;
        private DevExpress.XtraGrid.Columns.GridColumn colScanOption;
        private DevExpress.XtraGrid.Columns.GridColumn colNote;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colWRRDetailNo;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditBranch;
        private DevExpress.XtraGrid.Columns.GridColumn colSupplierCode;
        private DevExpress.XtraGrid.Columns.GridColumn colASNNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colPackingSlip;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemBranch;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemWRRDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemHandlingStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantityReceived;
        private DevExpress.XtraGrid.Columns.GridColumn colReceiptStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colSLPart;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private DevExpress.XtraEditors.TextEdit textEdit2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemTotalQuantityByItem;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemTotalQuantityByPack;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
    }
}
