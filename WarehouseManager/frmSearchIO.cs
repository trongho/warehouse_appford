﻿using DevExpress.XtraBars.Docking2010;
using DevExpress.XtraGrid.Views.Grid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WarehouseManager.Models;
using WarehouseManager.Repository;
using webservice.Entities;

namespace WarehouseManager
{
    public partial class frmSearchIO : Form
    {
        HandlingStatusRepository handlingStatusRepository;
        OrderStatusRepository orderStatusRepository;
        BranchRepository branchRepository;
        ModalityRepository modalityRepository;
        IOHeaderRepository iOHeaderRepository;
        public frmSearchIO()
        {
            InitializeComponent();
            this.Load += frmSearchIO_Load;
            handlingStatusRepository = new HandlingStatusRepository();
            branchRepository = new BranchRepository();
            iOHeaderRepository = new IOHeaderRepository();
            orderStatusRepository = new OrderStatusRepository();
            modalityRepository = new ModalityRepository();
        }

        private void frmSearchIO_Load(object sender, EventArgs e)
        {
            popupMenuSelectBranch();
            popupMenuSelectHandlingStatus();
            popupMenuSelectModality();
            sbLoadDataForGridIOHeaderAsync();
            gridViewIOHeader.DoubleClick += dataGridView_CellDoubleClick;
            simpleButtonFilter.Click += filterButton_Click;
            windowsUIButtonPanel1.ButtonClick += button_Click;
            this.KeyDown += new KeyEventHandler(Form1_KeyDown);

            if (int.Parse(DateTime.Now.Day.ToString()) == 1)
            {
                dateEditFromDate.Text = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day).ToString("yyyy-MM-dd");
            }
            else
            {
                dateEditFromDate.Text = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day - 1).ToString("yyyy-MM-dd");
            }
            dateEditToDate.Text = DateTime.Now.ToString("yyyy-MM-dd");
        }

        public async void button_Click(object sender, EventArgs e)
        {
            ButtonEventArgs eArg = (DevExpress.XtraBars.Docking2010.ButtonEventArgs)e;
            String tag = eArg.Button.Properties.Tag.ToString();
            switch (tag)
            {
                case "refesh":
                    sbLoadDataForGridIOHeaderAsync();
                    break;
                case "close":
                    this.Close();
                    break;
            }

        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F5)
            {
                sbLoadDataForGridIOHeaderAsync();
            }
            if (e.KeyCode == Keys.F10)
            {
                this.Close();
            }
        }

        private async Task popupMenuSelectHandlingStatus()
        {
            List<HandlingStatus> handlingStatuses = await handlingStatusRepository.GetAll();
            foreach (HandlingStatus handlingStatus in handlingStatuses)
            {
                comboBoxEditHandlingStatus.Properties.Items.Add(handlingStatus.HandlingStatusName);
            }
            comboBoxEditHandlingStatus.SelectedIndex = 0;
        }

        private async Task popupMenuSelectBranch()
        {
            List<Branch> branches = await branchRepository.getAllBranchAsync();
            foreach (Branch branch in branches)
            {
                comboBoxEditBranch.Properties.Items.Add(branch.BranchName);
            }
            comboBoxEditBranch.SelectedIndex = 0;
        }

        private async Task popupMenuSelectModality()
        {
            List<Modality> modalities = await modalityRepository.GetAll();
            foreach (Modality modality in modalities)
            {
                comboBoxEditModality.Properties.Items.Add(modality.ModalityName);
            }
            comboBoxEditModality.SelectedIndex = 0;
        }

        private async Task sbLoadDataForGridIOHeaderAsync()
        {
            List<IOHeader> iOHeaders = await iOHeaderRepository.GetAll();
            gridControlIOHeader.DataSource =iOHeaders;
        }

        private async void dataGridView_CellDoubleClick(object sender, EventArgs e)
        {
            if (gridViewIOHeader.RowCount > 0)
            {
                IOHeader iOHeader = await iOHeaderRepository.GetUnderID((string)(sender as GridView).GetFocusedRowCellValue("IONumber"));
                IssueOrderControl.selectedIONumber = (string)(sender as GridView).GetFocusedRowCellValue("IONumber");
                this.Close();
            }
        }

        private async void filterButton_Click(Object sender,EventArgs e)
        {
            String branchID = $"{int.Parse(comboBoxEditBranch.SelectedIndex.ToString()) - 1:D3}";
            String handlingStatusID = (comboBoxEditHandlingStatus.SelectedIndex - 1).ToString();
            String modalityID = $"{int.Parse(comboBoxEditModality.SelectedIndex.ToString()) + 1:D2}";
            DateTime fromDate = DateTime.Parse(dateEditFromDate.Text);
            DateTime toDate = DateTime.Parse(dateEditToDate.Text);

            List<IOHeader> iOHeadersUnderBranch = new List<IOHeader>();
            List<IOHeader> iOHeadersUnderDate = new List<IOHeader>();
            List<IOHeader> iOHeadersUnderHandlingStatus = new List<IOHeader>();
            List<IOHeader> iOHeadersUnderModality = new List<IOHeader>();
            List<IOHeader> iOHeaders = new List<IOHeader>();
            if (branchID.Equals("<>"))
            {
                iOHeadersUnderBranch = await iOHeaderRepository.GetAll();
            }
            else
            {
                iOHeadersUnderBranch = await iOHeaderRepository.GetUnderBranch(branchID);
            }
            if (handlingStatusID.Equals("<>"))
            {
                iOHeadersUnderHandlingStatus = await iOHeaderRepository.GetAll();
            }
            else
            {
                iOHeadersUnderHandlingStatus = await iOHeaderRepository.GetUnderHandlingStatus(handlingStatusID);
            }
            if (modalityID.Equals("<>"))
            {
                iOHeadersUnderModality = await iOHeaderRepository.GetAll();
            }
            else
            {
                iOHeadersUnderModality = await iOHeaderRepository.GetUnderModality(modalityID);
            }
            iOHeadersUnderDate = await iOHeaderRepository.GetUnderDate(fromDate, toDate);

            for (int i = 0; i < iOHeadersUnderBranch.Count; i++)
            {
                for (int j = 0; j < iOHeadersUnderDate.Count; j++)
                {
                    for (int k = 0; k < iOHeadersUnderHandlingStatus.Count; k++)
                    {
                        for (int h = 0; h < iOHeadersUnderModality.Count; h++)
                        {
                            if (iOHeadersUnderBranch[i].IONumber.Equals(iOHeadersUnderDate[j].IONumber)
                            && iOHeadersUnderDate[j].IONumber.Equals(iOHeadersUnderHandlingStatus[k].IONumber)
                            && iOHeadersUnderHandlingStatus[k].IONumber.Equals(iOHeadersUnderModality[h].IONumber))
                            {
                                iOHeaders.Add(iOHeadersUnderBranch[i]);
                            }
                        }
                    }
                }
            }
            gridControlIOHeader.DataSource = iOHeaders;
        }

    }
}
