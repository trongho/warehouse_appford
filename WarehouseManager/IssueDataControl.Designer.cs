﻿
namespace WarehouseManager
{
    partial class IssueDataControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions1 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(IssueDataControl));
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions2 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions3 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions4 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions5 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions6 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions7 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions8 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions9 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition1 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition2 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition3 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition4 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition5 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition6 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition7 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition8 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition9 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition10 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition1 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition2 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition3 = new DevExpress.XtraLayout.RowDefinition();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.windowsUIButtonPanel1 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControl3 = new DevExpress.XtraLayout.LayoutControl();
            this.gridControlWIDataDetail = new DevExpress.XtraGrid.GridControl();
            this.wRDataDetailBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.wARHOUSE_HPDataSet1 = new WarehouseManager.WARHOUSE_HPDataSet1();
            this.gridViewWIDataDetail = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colWRDNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrdinal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGoodsID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGoodsName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIDCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocationID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalQuantity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalGoods = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantityOrg = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalQuantityOrg = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalGoodsOrg = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocationIDOrg = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedDateTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEditerID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEditedDateTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPackingVolume = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantityByPack = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantityByItem = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNote = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colScanOption = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPackingQuantity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.textEditTotalQuantity = new DevExpress.XtraEditors.TextEdit();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textEditWIDNumber = new DevExpress.XtraEditors.TextEdit();
            this.dateEditWIDDate = new DevExpress.XtraEditors.DateEdit();
            this.textEdit4 = new DevExpress.XtraEditors.TextEdit();
            this.textEditTotalQuantityOrg = new DevExpress.XtraEditors.TextEdit();
            this.comboBoxEditHandlingStatus = new DevExpress.XtraEditors.ComboBoxEdit();
            this.simpleButtonOn = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonOff = new DevExpress.XtraEditors.SimpleButton();
            this.comboBoxEditGoodsGroupID = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEditPickerName = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEditScanOption = new DevExpress.XtraEditors.ComboBoxEdit();
            this.simpleButtonFilter = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemWIDNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemTotalQuantityOrg = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemTotalQuantity = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemWIDDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemHandlingStatus = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.wRDataDetailTableAdapter = new WarehouseManager.WARHOUSE_HPDataSet1TableAdapters.WRDataDetailTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).BeginInit();
            this.layoutControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlWIDataDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wRDataDetailBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wARHOUSE_HPDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewWIDataDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalQuantity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditWIDNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditWIDDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditWIDDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalQuantityOrg.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditHandlingStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditGoodsGroupID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditPickerName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditScanOption.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWIDNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalQuantityOrg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalQuantity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWIDDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemHandlingStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.windowsUIButtonPanel1);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(554, 208, 812, 495);
            this.layoutControl1.Root = this.Root;
            this.layoutControl1.Size = new System.Drawing.Size(1298, 36);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // windowsUIButtonPanel1
            // 
            this.windowsUIButtonPanel1.AllowGlyphSkinning = false;
            this.windowsUIButtonPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            windowsUIButtonImageOptions1.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions1.Image")));
            windowsUIButtonImageOptions1.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions2.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions2.Image")));
            windowsUIButtonImageOptions2.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions3.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions3.Image")));
            windowsUIButtonImageOptions3.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions4.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions4.Image")));
            windowsUIButtonImageOptions4.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions5.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions5.Image")));
            windowsUIButtonImageOptions5.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions6.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions6.Image")));
            windowsUIButtonImageOptions6.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions7.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions7.Image")));
            windowsUIButtonImageOptions7.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions8.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions8.Image")));
            windowsUIButtonImageOptions8.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions9.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions9.Image")));
            windowsUIButtonImageOptions9.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            this.windowsUIButtonPanel1.Buttons.AddRange(new DevExpress.XtraEditors.ButtonPanel.IBaseButton[] {
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F2) Thêm mới", true, windowsUIButtonImageOptions1, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "new", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F3) Lưu", true, windowsUIButtonImageOptions2, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "save", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F4) Xóa", true, windowsUIButtonImageOptions3, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "delete", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F5) Tìm kiếm", true, windowsUIButtonImageOptions4, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "search", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F6) Làm mới", true, windowsUIButtonImageOptions5, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "refesh", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F7) Nhập dữ liệu", true, windowsUIButtonImageOptions6, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "import", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F8) Xuất dữ liệu", true, windowsUIButtonImageOptions7, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "export", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F9) In ấn", true, windowsUIButtonImageOptions8, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "print", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F10) Thoát", true, windowsUIButtonImageOptions9, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "close", -1, false)});
            this.windowsUIButtonPanel1.ContentAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.windowsUIButtonPanel1.Font = new System.Drawing.Font("Tahoma", 10F);
            this.windowsUIButtonPanel1.Location = new System.Drawing.Point(2, 2);
            this.windowsUIButtonPanel1.Name = "windowsUIButtonPanel1";
            this.windowsUIButtonPanel1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.windowsUIButtonPanel1.Size = new System.Drawing.Size(1294, 32);
            this.windowsUIButtonPanel1.TabIndex = 6;
            this.windowsUIButtonPanel1.Text = "windowsUIButtonPanel1";
            this.windowsUIButtonPanel1.UseButtonBackgroundImages = false;
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.Root.Name = "Root";
            this.Root.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.Root.Size = new System.Drawing.Size(1298, 36);
            this.Root.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.windowsUIButtonPanel1;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(1298, 36);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControl3
            // 
            this.layoutControl3.Controls.Add(this.gridControlWIDataDetail);
            this.layoutControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl3.Location = new System.Drawing.Point(0, 142);
            this.layoutControl3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.layoutControl3.Name = "layoutControl3";
            this.layoutControl3.Root = this.layoutControlGroup2;
            this.layoutControl3.Size = new System.Drawing.Size(1298, 484);
            this.layoutControl3.TabIndex = 2;
            this.layoutControl3.Text = "layoutControl3";
            // 
            // gridControlWIDataDetail
            // 
            this.gridControlWIDataDetail.DataSource = this.wRDataDetailBindingSource;
            this.gridControlWIDataDetail.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            gridLevelNode1.RelationName = "Level1";
            this.gridControlWIDataDetail.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControlWIDataDetail.Location = new System.Drawing.Point(12, 12);
            this.gridControlWIDataDetail.MainView = this.gridViewWIDataDetail;
            this.gridControlWIDataDetail.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gridControlWIDataDetail.Name = "gridControlWIDataDetail";
            this.gridControlWIDataDetail.Size = new System.Drawing.Size(1274, 460);
            this.gridControlWIDataDetail.TabIndex = 4;
            this.gridControlWIDataDetail.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewWIDataDetail});
            // 
            // wRDataDetailBindingSource
            // 
            this.wRDataDetailBindingSource.DataMember = "WRDataDetail";
            this.wRDataDetailBindingSource.DataSource = this.wARHOUSE_HPDataSet1;
            // 
            // wARHOUSE_HPDataSet1
            // 
            this.wARHOUSE_HPDataSet1.DataSetName = "WARHOUSE_HPDataSet1";
            this.wARHOUSE_HPDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridViewWIDataDetail
            // 
            this.gridViewWIDataDetail.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridViewWIDataDetail.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridViewWIDataDetail.Appearance.HeaderPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.gridViewWIDataDetail.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colWRDNumber,
            this.colOrdinal,
            this.colGoodsID,
            this.colGoodsName,
            this.colIDCode,
            this.colLocationID,
            this.colQuantity,
            this.colTotalQuantity,
            this.colTotalGoods,
            this.colQuantityOrg,
            this.colTotalQuantityOrg,
            this.colTotalGoodsOrg,
            this.colLocationIDOrg,
            this.colCreatorID,
            this.colCreatedDateTime,
            this.colEditerID,
            this.colEditedDateTime,
            this.colStatus,
            this.colPackingVolume,
            this.colQuantityByPack,
            this.colQuantityByItem,
            this.colNote,
            this.colScanOption,
            this.colPackingQuantity,
            this.colNo});
            this.gridViewWIDataDetail.DetailHeight = 284;
            this.gridViewWIDataDetail.GridControl = this.gridControlWIDataDetail;
            this.gridViewWIDataDetail.Name = "gridViewWIDataDetail";
            this.gridViewWIDataDetail.OptionsView.ColumnHeaderAutoHeight = DevExpress.Utils.DefaultBoolean.True;
            this.gridViewWIDataDetail.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colQuantityByPack, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colWRDNumber
            // 
            this.colWRDNumber.Caption = "Số dữ liệu nhập kho";
            this.colWRDNumber.FieldName = "WRDNumber";
            this.colWRDNumber.MinWidth = 21;
            this.colWRDNumber.Name = "colWRDNumber";
            this.colWRDNumber.Width = 81;
            // 
            // colOrdinal
            // 
            this.colOrdinal.Caption = "Số thứ tự";
            this.colOrdinal.FieldName = "Ordinal";
            this.colOrdinal.MinWidth = 21;
            this.colOrdinal.Name = "colOrdinal";
            this.colOrdinal.Width = 81;
            // 
            // colGoodsID
            // 
            this.colGoodsID.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colGoodsID.AppearanceCell.Options.UseFont = true;
            this.colGoodsID.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colGoodsID.AppearanceHeader.Options.UseFont = true;
            this.colGoodsID.Caption = "PART NUMBER";
            this.colGoodsID.FieldName = "GoodsID";
            this.colGoodsID.MinWidth = 21;
            this.colGoodsID.Name = "colGoodsID";
            this.colGoodsID.Visible = true;
            this.colGoodsID.VisibleIndex = 1;
            this.colGoodsID.Width = 163;
            // 
            // colGoodsName
            // 
            this.colGoodsName.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colGoodsName.AppearanceCell.Options.UseFont = true;
            this.colGoodsName.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colGoodsName.AppearanceHeader.Options.UseFont = true;
            this.colGoodsName.Caption = "PARNAME";
            this.colGoodsName.FieldName = "GoodsName";
            this.colGoodsName.MinWidth = 21;
            this.colGoodsName.Name = "colGoodsName";
            this.colGoodsName.Visible = true;
            this.colGoodsName.VisibleIndex = 2;
            this.colGoodsName.Width = 329;
            // 
            // colIDCode
            // 
            this.colIDCode.Caption = "Mã định danh";
            this.colIDCode.FieldName = "IDCode";
            this.colIDCode.MinWidth = 21;
            this.colIDCode.Name = "colIDCode";
            this.colIDCode.Width = 81;
            // 
            // colLocationID
            // 
            this.colLocationID.Caption = "Vị trí ";
            this.colLocationID.FieldName = "LocationID";
            this.colLocationID.MinWidth = 21;
            this.colLocationID.Name = "colLocationID";
            this.colLocationID.Width = 81;
            // 
            // colQuantity
            // 
            this.colQuantity.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colQuantity.AppearanceCell.Options.UseFont = true;
            this.colQuantity.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colQuantity.AppearanceHeader.Options.UseFont = true;
            this.colQuantity.Caption = "SL THỰC XUẤT";
            this.colQuantity.DisplayFormat.FormatString = "G29";
            this.colQuantity.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colQuantity.FieldName = "Quantity";
            this.colQuantity.MinWidth = 21;
            this.colQuantity.Name = "colQuantity";
            this.colQuantity.Visible = true;
            this.colQuantity.VisibleIndex = 4;
            this.colQuantity.Width = 116;
            // 
            // colTotalQuantity
            // 
            this.colTotalQuantity.Caption = "Số lượng thực tế( mặt hàng)";
            this.colTotalQuantity.DisplayFormat.FormatString = "G29";
            this.colTotalQuantity.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colTotalQuantity.FieldName = "TotalQuantity";
            this.colTotalQuantity.MinWidth = 21;
            this.colTotalQuantity.Name = "colTotalQuantity";
            this.colTotalQuantity.Width = 81;
            // 
            // colTotalGoods
            // 
            this.colTotalGoods.Caption = "Số lượng thực tế hàng hóa";
            this.colTotalGoods.DisplayFormat.FormatString = "G29";
            this.colTotalGoods.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colTotalGoods.FieldName = "TotalGoods";
            this.colTotalGoods.MinWidth = 21;
            this.colTotalGoods.Name = "colTotalGoods";
            this.colTotalGoods.Width = 81;
            // 
            // colQuantityOrg
            // 
            this.colQuantityOrg.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colQuantityOrg.AppearanceCell.Options.UseFont = true;
            this.colQuantityOrg.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colQuantityOrg.AppearanceHeader.Options.UseFont = true;
            this.colQuantityOrg.Caption = "SL YÊU CẦU";
            this.colQuantityOrg.DisplayFormat.FormatString = "G29";
            this.colQuantityOrg.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colQuantityOrg.FieldName = "QuantityOrg";
            this.colQuantityOrg.MinWidth = 21;
            this.colQuantityOrg.Name = "colQuantityOrg";
            this.colQuantityOrg.Visible = true;
            this.colQuantityOrg.VisibleIndex = 3;
            this.colQuantityOrg.Width = 112;
            // 
            // colTotalQuantityOrg
            // 
            this.colTotalQuantityOrg.Caption = "Số lượng yêu cầu( mặt hàng)";
            this.colTotalQuantityOrg.DisplayFormat.FormatString = "G29";
            this.colTotalQuantityOrg.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colTotalQuantityOrg.FieldName = "TotalQuantityOrg";
            this.colTotalQuantityOrg.MinWidth = 21;
            this.colTotalQuantityOrg.Name = "colTotalQuantityOrg";
            this.colTotalQuantityOrg.Width = 81;
            // 
            // colTotalGoodsOrg
            // 
            this.colTotalGoodsOrg.Caption = "Số lượng yêu cầu hàng hóa";
            this.colTotalGoodsOrg.DisplayFormat.FormatString = "G29";
            this.colTotalGoodsOrg.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colTotalGoodsOrg.FieldName = "TotalGoodsOrg";
            this.colTotalGoodsOrg.MinWidth = 21;
            this.colTotalGoodsOrg.Name = "colTotalGoodsOrg";
            this.colTotalGoodsOrg.Width = 81;
            // 
            // colLocationIDOrg
            // 
            this.colLocationIDOrg.Caption = "Vị trí";
            this.colLocationIDOrg.FieldName = "LocationIDOrg";
            this.colLocationIDOrg.MinWidth = 21;
            this.colLocationIDOrg.Name = "colLocationIDOrg";
            this.colLocationIDOrg.Width = 81;
            // 
            // colCreatorID
            // 
            this.colCreatorID.Caption = "Người tạo";
            this.colCreatorID.FieldName = "CreatorID";
            this.colCreatorID.MinWidth = 21;
            this.colCreatorID.Name = "colCreatorID";
            this.colCreatorID.Width = 81;
            // 
            // colCreatedDateTime
            // 
            this.colCreatedDateTime.Caption = "Ngày tạo";
            this.colCreatedDateTime.FieldName = "CreatedDateTime";
            this.colCreatedDateTime.MinWidth = 21;
            this.colCreatedDateTime.Name = "colCreatedDateTime";
            this.colCreatedDateTime.Width = 81;
            // 
            // colEditerID
            // 
            this.colEditerID.Caption = "Người sửa";
            this.colEditerID.FieldName = "EditerID";
            this.colEditerID.MinWidth = 21;
            this.colEditerID.Name = "colEditerID";
            this.colEditerID.Width = 81;
            // 
            // colEditedDateTime
            // 
            this.colEditedDateTime.Caption = "Ngày sửa";
            this.colEditedDateTime.FieldName = "EditedDateTime";
            this.colEditedDateTime.MinWidth = 21;
            this.colEditedDateTime.Name = "colEditedDateTime";
            this.colEditedDateTime.Width = 81;
            // 
            // colStatus
            // 
            this.colStatus.Caption = "Trạng thái";
            this.colStatus.FieldName = "Status";
            this.colStatus.MinWidth = 21;
            this.colStatus.Name = "colStatus";
            this.colStatus.Width = 81;
            // 
            // colPackingVolume
            // 
            this.colPackingVolume.Caption = "Quy cách đóng gói";
            this.colPackingVolume.DisplayFormat.FormatString = "G29";
            this.colPackingVolume.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colPackingVolume.FieldName = "PackingVolume";
            this.colPackingVolume.MinWidth = 21;
            this.colPackingVolume.Name = "colPackingVolume";
            this.colPackingVolume.Width = 81;
            // 
            // colQuantityByPack
            // 
            this.colQuantityByPack.Caption = "# of Containers / Coils";
            this.colQuantityByPack.DisplayFormat.FormatString = "G29";
            this.colQuantityByPack.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colQuantityByPack.FieldName = "QuantityByPack";
            this.colQuantityByPack.MinWidth = 21;
            this.colQuantityByPack.Name = "colQuantityByPack";
            this.colQuantityByPack.Width = 163;
            // 
            // colQuantityByItem
            // 
            this.colQuantityByItem.Caption = "Lượng yêu cầu( mặt hàng)";
            this.colQuantityByItem.DisplayFormat.FormatString = "G29";
            this.colQuantityByItem.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colQuantityByItem.FieldName = "QuantityByItem";
            this.colQuantityByItem.MinWidth = 21;
            this.colQuantityByItem.Name = "colQuantityByItem";
            this.colQuantityByItem.Width = 81;
            // 
            // colNote
            // 
            this.colNote.Caption = "Ghi chú";
            this.colNote.FieldName = "Note";
            this.colNote.MinWidth = 21;
            this.colNote.Name = "colNote";
            this.colNote.Width = 81;
            // 
            // colScanOption
            // 
            this.colScanOption.FieldName = "ScanOption";
            this.colScanOption.MinWidth = 21;
            this.colScanOption.Name = "colScanOption";
            this.colScanOption.Width = 81;
            // 
            // colPackingQuantity
            // 
            this.colPackingQuantity.Caption = "NBR CNT";
            this.colPackingQuantity.DisplayFormat.FormatString = "G29";
            this.colPackingQuantity.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colPackingQuantity.FieldName = "PackingQuantity";
            this.colPackingQuantity.MinWidth = 21;
            this.colPackingQuantity.Name = "colPackingQuantity";
            this.colPackingQuantity.Width = 97;
            // 
            // colNo
            // 
            this.colNo.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colNo.AppearanceCell.Options.UseFont = true;
            this.colNo.AppearanceCell.Options.UseTextOptions = true;
            this.colNo.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNo.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colNo.AppearanceHeader.Options.UseFont = true;
            this.colNo.AppearanceHeader.Options.UseTextOptions = true;
            this.colNo.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNo.Caption = "NO";
            this.colNo.FieldName = "No";
            this.colNo.MinWidth = 21;
            this.colNo.Name = "colNo";
            this.colNo.Visible = true;
            this.colNo.VisibleIndex = 0;
            this.colNo.Width = 36;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem12});
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(1298, 484);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.gridControlWIDataDetail;
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(1278, 464);
            this.layoutControlItem12.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem12.TextVisible = false;
            // 
            // textEditTotalQuantity
            // 
            this.textEditTotalQuantity.Location = new System.Drawing.Point(1132, 40);
            this.textEditTotalQuantity.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textEditTotalQuantity.Name = "textEditTotalQuantity";
            this.textEditTotalQuantity.Size = new System.Drawing.Size(154, 20);
            this.textEditTotalQuantity.StyleController = this.layoutControl2;
            this.textEditTotalQuantity.TabIndex = 16;
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.pictureBox1);
            this.layoutControl2.Controls.Add(this.label1);
            this.layoutControl2.Controls.Add(this.textEditWIDNumber);
            this.layoutControl2.Controls.Add(this.dateEditWIDDate);
            this.layoutControl2.Controls.Add(this.textEdit4);
            this.layoutControl2.Controls.Add(this.textEditTotalQuantityOrg);
            this.layoutControl2.Controls.Add(this.textEditTotalQuantity);
            this.layoutControl2.Controls.Add(this.comboBoxEditHandlingStatus);
            this.layoutControl2.Controls.Add(this.simpleButtonOn);
            this.layoutControl2.Controls.Add(this.simpleButtonOff);
            this.layoutControl2.Controls.Add(this.comboBoxEditGoodsGroupID);
            this.layoutControl2.Controls.Add(this.comboBoxEditPickerName);
            this.layoutControl2.Controls.Add(this.comboBoxEditScanOption);
            this.layoutControl2.Controls.Add(this.simpleButtonFilter);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl2.Location = new System.Drawing.Point(0, 36);
            this.layoutControl2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(338, 174, 812, 500);
            this.layoutControl2.Root = this.layoutControlGroup1;
            this.layoutControl2.Size = new System.Drawing.Size(1298, 106);
            this.layoutControl2.TabIndex = 1;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureBox1.Location = new System.Drawing.Point(449, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(125, 82);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 23;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(324, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(121, 24);
            this.label1.TabIndex = 7;
            this.label1.Text = "(*)";
            // 
            // textEditWIDNumber
            // 
            this.textEditWIDNumber.Location = new System.Drawing.Point(107, 14);
            this.textEditWIDNumber.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textEditWIDNumber.Name = "textEditWIDNumber";
            this.textEditWIDNumber.Size = new System.Drawing.Size(213, 20);
            this.textEditWIDNumber.StyleController = this.layoutControl2;
            this.textEditWIDNumber.TabIndex = 4;
            // 
            // dateEditWIDDate
            // 
            this.dateEditWIDDate.EditValue = null;
            this.dateEditWIDDate.Location = new System.Drawing.Point(107, 42);
            this.dateEditWIDDate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dateEditWIDDate.Name = "dateEditWIDDate";
            this.dateEditWIDDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditWIDDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditWIDDate.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditWIDDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditWIDDate.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEditWIDDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditWIDDate.Size = new System.Drawing.Size(213, 20);
            this.dateEditWIDDate.StyleController = this.layoutControl2;
            this.dateEditWIDDate.TabIndex = 8;
            // 
            // textEdit4
            // 
            this.textEdit4.Location = new System.Drawing.Point(2540, 10);
            this.textEdit4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textEdit4.Name = "textEdit4";
            this.textEdit4.Size = new System.Drawing.Size(53, 20);
            this.textEdit4.StyleController = this.layoutControl2;
            this.textEdit4.TabIndex = 11;
            // 
            // textEditTotalQuantityOrg
            // 
            this.textEditTotalQuantityOrg.Location = new System.Drawing.Point(1132, 12);
            this.textEditTotalQuantityOrg.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textEditTotalQuantityOrg.Name = "textEditTotalQuantityOrg";
            this.textEditTotalQuantityOrg.Size = new System.Drawing.Size(154, 20);
            this.textEditTotalQuantityOrg.StyleController = this.layoutControl2;
            this.textEditTotalQuantityOrg.TabIndex = 12;
            // 
            // comboBoxEditHandlingStatus
            // 
            this.comboBoxEditHandlingStatus.Location = new System.Drawing.Point(107, 71);
            this.comboBoxEditHandlingStatus.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboBoxEditHandlingStatus.Name = "comboBoxEditHandlingStatus";
            this.comboBoxEditHandlingStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditHandlingStatus.Size = new System.Drawing.Size(213, 20);
            this.comboBoxEditHandlingStatus.StyleController = this.layoutControl2;
            this.comboBoxEditHandlingStatus.TabIndex = 17;
            // 
            // simpleButtonOn
            // 
            this.simpleButtonOn.Location = new System.Drawing.Point(1037, 69);
            this.simpleButtonOn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.simpleButtonOn.Name = "simpleButtonOn";
            this.simpleButtonOn.Size = new System.Drawing.Size(122, 22);
            this.simpleButtonOn.StyleController = this.layoutControl2;
            this.simpleButtonOn.TabIndex = 18;
            this.simpleButtonOn.Text = "Mở";
            // 
            // simpleButtonOff
            // 
            this.simpleButtonOff.Location = new System.Drawing.Point(1163, 69);
            this.simpleButtonOff.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.simpleButtonOff.Name = "simpleButtonOff";
            this.simpleButtonOff.Size = new System.Drawing.Size(123, 22);
            this.simpleButtonOff.StyleController = this.layoutControl2;
            this.simpleButtonOff.TabIndex = 19;
            this.simpleButtonOff.Text = "Tắt";
            // 
            // comboBoxEditGoodsGroupID
            // 
            this.comboBoxEditGoodsGroupID.Location = new System.Drawing.Point(737, 14);
            this.comboBoxEditGoodsGroupID.Name = "comboBoxEditGoodsGroupID";
            this.comboBoxEditGoodsGroupID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditGoodsGroupID.Size = new System.Drawing.Size(106, 20);
            this.comboBoxEditGoodsGroupID.StyleController = this.layoutControl2;
            this.comboBoxEditGoodsGroupID.TabIndex = 24;
            // 
            // comboBoxEditPickerName
            // 
            this.comboBoxEditPickerName.Location = new System.Drawing.Point(737, 42);
            this.comboBoxEditPickerName.Name = "comboBoxEditPickerName";
            this.comboBoxEditPickerName.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditPickerName.Size = new System.Drawing.Size(106, 20);
            this.comboBoxEditPickerName.StyleController = this.layoutControl2;
            this.comboBoxEditPickerName.TabIndex = 25;
            // 
            // comboBoxEditScanOption
            // 
            this.comboBoxEditScanOption.Location = new System.Drawing.Point(942, 12);
            this.comboBoxEditScanOption.Name = "comboBoxEditScanOption";
            this.comboBoxEditScanOption.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditScanOption.Size = new System.Drawing.Size(91, 20);
            this.comboBoxEditScanOption.StyleController = this.layoutControl2;
            this.comboBoxEditScanOption.TabIndex = 26;
            // 
            // simpleButtonFilter
            // 
            this.simpleButtonFilter.Location = new System.Drawing.Point(642, 69);
            this.simpleButtonFilter.Name = "simpleButtonFilter";
            this.simpleButtonFilter.Size = new System.Drawing.Size(201, 22);
            this.simpleButtonFilter.StyleController = this.layoutControl2;
            this.simpleButtonFilter.TabIndex = 27;
            this.simpleButtonFilter.Text = "Lọc";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemWIDNumber,
            this.layoutControlItem3,
            this.layoutControlItemTotalQuantityOrg,
            this.layoutControlItemTotalQuantity,
            this.layoutControlItemWIDDate,
            this.layoutControlItemHandlingStatus,
            this.layoutControlItem2,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem9});
            this.layoutControlGroup1.LayoutMode = DevExpress.XtraLayout.Utils.LayoutMode.Table;
            this.layoutControlGroup1.Name = "Root";
            columnDefinition1.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition1.Width = 24.380944189765319D;
            columnDefinition2.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition2.Width = 9.7523776759061267D;
            columnDefinition3.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition3.Width = 10.092730926707551D;
            columnDefinition4.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition4.Width = 5.0020220120587329D;
            columnDefinition5.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition5.Width = 16.006470438587947D;
            columnDefinition6.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition6.Width = 14.89948061013185D;
            columnDefinition7.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition7.Width = 4.9664935367106171D;
            columnDefinition8.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition8.Width = 4.9664935367106171D;
            columnDefinition9.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition9.Width = 4.9664935367106171D;
            columnDefinition10.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition10.Width = 4.9664935367106171D;
            this.layoutControlGroup1.OptionsTableLayoutGroup.ColumnDefinitions.AddRange(new DevExpress.XtraLayout.ColumnDefinition[] {
            columnDefinition1,
            columnDefinition2,
            columnDefinition3,
            columnDefinition4,
            columnDefinition5,
            columnDefinition6,
            columnDefinition7,
            columnDefinition8,
            columnDefinition9,
            columnDefinition10});
            rowDefinition1.Height = 33.333333333333336D;
            rowDefinition1.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition2.Height = 33.333333333333336D;
            rowDefinition2.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition3.Height = 33.333333333333336D;
            rowDefinition3.SizeType = System.Windows.Forms.SizeType.Percent;
            this.layoutControlGroup1.OptionsTableLayoutGroup.RowDefinitions.AddRange(new DevExpress.XtraLayout.RowDefinition[] {
            rowDefinition1,
            rowDefinition2,
            rowDefinition3});
            this.layoutControlGroup1.Size = new System.Drawing.Size(1298, 106);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItemWIDNumber
            // 
            this.layoutControlItemWIDNumber.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.layoutControlItemWIDNumber.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItemWIDNumber.ContentVertAlignment = DevExpress.Utils.VertAlignment.Center;
            this.layoutControlItemWIDNumber.Control = this.textEditWIDNumber;
            this.layoutControlItemWIDNumber.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemWIDNumber.Name = "layoutControlItemWIDNumber";
            this.layoutControlItemWIDNumber.OptionsPrint.AppearanceItem.Font = new System.Drawing.Font("Tahoma", 10F);
            this.layoutControlItemWIDNumber.OptionsPrint.AppearanceItem.Options.UseFont = true;
            this.layoutControlItemWIDNumber.Size = new System.Drawing.Size(312, 28);
            this.layoutControlItemWIDNumber.Text = "Số phiếu";
            this.layoutControlItemWIDNumber.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItemWIDNumber.TextSize = new System.Drawing.Size(92, 17);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.label1;
            this.layoutControlItem3.Location = new System.Drawing.Point(312, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.OptionsTableLayoutItem.ColumnIndex = 1;
            this.layoutControlItem3.Size = new System.Drawing.Size(125, 28);
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItemTotalQuantityOrg
            // 
            this.layoutControlItemTotalQuantityOrg.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.layoutControlItemTotalQuantityOrg.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItemTotalQuantityOrg.Control = this.textEditTotalQuantityOrg;
            this.layoutControlItemTotalQuantityOrg.Location = new System.Drawing.Point(1025, 0);
            this.layoutControlItemTotalQuantityOrg.Name = "layoutControlItemTotalQuantityOrg";
            this.layoutControlItemTotalQuantityOrg.OptionsTableLayoutItem.ColumnIndex = 6;
            this.layoutControlItemTotalQuantityOrg.OptionsTableLayoutItem.ColumnSpan = 4;
            this.layoutControlItemTotalQuantityOrg.Size = new System.Drawing.Size(253, 28);
            this.layoutControlItemTotalQuantityOrg.Text = "Σ SL yêu cầu";
            this.layoutControlItemTotalQuantityOrg.TextSize = new System.Drawing.Size(92, 17);
            // 
            // layoutControlItemTotalQuantity
            // 
            this.layoutControlItemTotalQuantity.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.layoutControlItemTotalQuantity.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItemTotalQuantity.Control = this.textEditTotalQuantity;
            this.layoutControlItemTotalQuantity.Location = new System.Drawing.Point(1025, 28);
            this.layoutControlItemTotalQuantity.Name = "layoutControlItemTotalQuantity";
            this.layoutControlItemTotalQuantity.OptionsTableLayoutItem.ColumnIndex = 6;
            this.layoutControlItemTotalQuantity.OptionsTableLayoutItem.ColumnSpan = 4;
            this.layoutControlItemTotalQuantity.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItemTotalQuantity.Size = new System.Drawing.Size(253, 29);
            this.layoutControlItemTotalQuantity.Text = "Σ SL Còn Lại";
            this.layoutControlItemTotalQuantity.TextSize = new System.Drawing.Size(92, 17);
            // 
            // layoutControlItemWIDDate
            // 
            this.layoutControlItemWIDDate.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.layoutControlItemWIDDate.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItemWIDDate.ContentVertAlignment = DevExpress.Utils.VertAlignment.Center;
            this.layoutControlItemWIDDate.Control = this.dateEditWIDDate;
            this.layoutControlItemWIDDate.Location = new System.Drawing.Point(0, 28);
            this.layoutControlItemWIDDate.Name = "layoutControlItemWIDDate";
            this.layoutControlItemWIDDate.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItemWIDDate.Size = new System.Drawing.Size(312, 29);
            this.layoutControlItemWIDDate.Text = "Ngày dữ liệu";
            this.layoutControlItemWIDDate.TextSize = new System.Drawing.Size(92, 17);
            // 
            // layoutControlItemHandlingStatus
            // 
            this.layoutControlItemHandlingStatus.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.layoutControlItemHandlingStatus.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItemHandlingStatus.ContentVertAlignment = DevExpress.Utils.VertAlignment.Center;
            this.layoutControlItemHandlingStatus.Control = this.comboBoxEditHandlingStatus;
            this.layoutControlItemHandlingStatus.Location = new System.Drawing.Point(0, 57);
            this.layoutControlItemHandlingStatus.Name = "layoutControlItemHandlingStatus";
            this.layoutControlItemHandlingStatus.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlItemHandlingStatus.Size = new System.Drawing.Size(312, 29);
            this.layoutControlItemHandlingStatus.Text = "HandlingStatus";
            this.layoutControlItemHandlingStatus.TextSize = new System.Drawing.Size(92, 16);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.simpleButtonOn;
            this.layoutControlItem2.Location = new System.Drawing.Point(1025, 57);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.OptionsTableLayoutItem.ColumnIndex = 6;
            this.layoutControlItem2.OptionsTableLayoutItem.ColumnSpan = 2;
            this.layoutControlItem2.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlItem2.Size = new System.Drawing.Size(126, 29);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.simpleButtonOff;
            this.layoutControlItem4.Location = new System.Drawing.Point(1151, 57);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.OptionsTableLayoutItem.ColumnIndex = 8;
            this.layoutControlItem4.OptionsTableLayoutItem.ColumnSpan = 2;
            this.layoutControlItem4.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlItem4.Size = new System.Drawing.Size(127, 29);
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.pictureBox1;
            this.layoutControlItem5.Location = new System.Drawing.Point(437, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.OptionsTableLayoutItem.ColumnIndex = 2;
            this.layoutControlItem5.OptionsTableLayoutItem.RowSpan = 3;
            this.layoutControlItem5.Size = new System.Drawing.Size(129, 86);
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.layoutControlItem6.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem6.ContentVertAlignment = DevExpress.Utils.VertAlignment.Center;
            this.layoutControlItem6.Control = this.comboBoxEditGoodsGroupID;
            this.layoutControlItem6.Location = new System.Drawing.Point(630, 0);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.OptionsTableLayoutItem.ColumnIndex = 4;
            this.layoutControlItem6.Size = new System.Drawing.Size(205, 28);
            this.layoutControlItem6.Text = "Mã xe";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(92, 16);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.layoutControlItem7.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem7.ContentVertAlignment = DevExpress.Utils.VertAlignment.Center;
            this.layoutControlItem7.Control = this.comboBoxEditPickerName;
            this.layoutControlItem7.Location = new System.Drawing.Point(630, 28);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.OptionsTableLayoutItem.ColumnIndex = 4;
            this.layoutControlItem7.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItem7.Size = new System.Drawing.Size(205, 29);
            this.layoutControlItem7.Text = "Người lấy hàng";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(92, 17);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.layoutControlItem8.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem8.Control = this.comboBoxEditScanOption;
            this.layoutControlItem8.Location = new System.Drawing.Point(835, 0);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.OptionsTableLayoutItem.ColumnIndex = 5;
            this.layoutControlItem8.Size = new System.Drawing.Size(190, 28);
            this.layoutControlItem8.Text = "Tùy chọn quét";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(92, 17);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.simpleButtonFilter;
            this.layoutControlItem9.Location = new System.Drawing.Point(630, 57);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.OptionsTableLayoutItem.ColumnIndex = 4;
            this.layoutControlItem9.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlItem9.Size = new System.Drawing.Size(205, 29);
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextVisible = false;
            // 
            // wRDataDetailTableAdapter
            // 
            this.wRDataDetailTableAdapter.ClearBeforeFill = true;
            // 
            // IssueDataControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl3);
            this.Controls.Add(this.layoutControl2);
            this.Controls.Add(this.layoutControl1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "IssueDataControl";
            this.Size = new System.Drawing.Size(1298, 626);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).EndInit();
            this.layoutControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlWIDataDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wRDataDetailBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wARHOUSE_HPDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewWIDataDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalQuantity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditWIDNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditWIDDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditWIDDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalQuantityOrg.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditHandlingStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditGoodsGroupID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditPickerName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditScanOption.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWIDNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalQuantityOrg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalQuantity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWIDDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemHandlingStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraLayout.LayoutControl layoutControl3;
        private DevExpress.XtraGrid.GridControl gridControlWIDataDetail;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewWIDataDetail;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraEditors.TextEdit textEditTotalQuantity;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.TextEdit textEditWIDNumber;
        private DevExpress.XtraEditors.DateEdit dateEditWIDDate;
        private DevExpress.XtraEditors.TextEdit textEdit4;
        private DevExpress.XtraEditors.TextEdit textEditTotalQuantityOrg;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditHandlingStatus;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemTotalQuantityOrg;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemTotalQuantity;
        private System.Windows.Forms.BindingSource wRDataDetailBindingSource;
        private WARHOUSE_HPDataSet1 wARHOUSE_HPDataSet1;
        private DevExpress.XtraGrid.Columns.GridColumn colWRDNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colOrdinal;
        private DevExpress.XtraGrid.Columns.GridColumn colGoodsID;
        private DevExpress.XtraGrid.Columns.GridColumn colGoodsName;
        private DevExpress.XtraGrid.Columns.GridColumn colIDCode;
        private DevExpress.XtraGrid.Columns.GridColumn colLocationID;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantity;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalQuantity;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalGoods;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantityOrg;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalQuantityOrg;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalGoodsOrg;
        private DevExpress.XtraGrid.Columns.GridColumn colLocationIDOrg;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatorID;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedDateTime;
        private DevExpress.XtraGrid.Columns.GridColumn colEditerID;
        private DevExpress.XtraGrid.Columns.GridColumn colEditedDateTime;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colPackingVolume;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantityByPack;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantityByItem;
        private DevExpress.XtraGrid.Columns.GridColumn colNote;
        private DevExpress.XtraGrid.Columns.GridColumn colScanOption;
        private WARHOUSE_HPDataSet1TableAdapters.WRDataDetailTableAdapter wRDataDetailTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colPackingQuantity;
        private DevExpress.XtraEditors.SimpleButton simpleButtonOn;
        private DevExpress.XtraEditors.SimpleButton simpleButtonOff;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemWIDDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemHandlingStatus;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemWIDNumber;
        private System.Windows.Forms.PictureBox pictureBox1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraGrid.Columns.GridColumn colNo;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditGoodsGroupID;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditPickerName;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditScanOption;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraEditors.SimpleButton simpleButtonFilter;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel windowsUIButtonPanel1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
    }
}
