﻿using DevExpress.XtraBars.Docking2010;
using DevExpress.XtraGrid.Views.Grid;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WarehouseManager.Models;
using WarehouseManager.Repository;
using LicenseContext = OfficeOpenXml.LicenseContext;

namespace WarehouseManager
{
   
    public partial class LocationControl : DevExpress.XtraEditors.XtraUserControl
    {
        LocationRepository locationRepository;
        int sumAddNew = 0;
        public LocationControl()
        {
            InitializeComponent();
            locationRepository=new LocationRepository();
            this.Load += LocationControl_Load;
        }

        private void LocationControl_Load(Object sender,EventArgs e)
        {
            gridViewData.ClearSorting();
            windowsUIButtonPanel1.ButtonClick += button_Click;
            gridViewData.RowClick += gridView_RowClick;
            gridViewData.CustomColumnDisplayText += GridView_CustomColumnDisplayText;
            sbLoadDataForGridAsync();
        }

        private void GridView_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {

            if (e.Column == colNo)
            {
                int rowHandle = gridViewData.GetRowHandle(e.ListSourceRowIndex);
                if (rowHandle >= 0)
                {
                    e.DisplayText = (rowHandle + 1).ToString();
                }
            }
        }

        private async Task CreatAsync() {
            Models.Location location=new Models.Location();
            if (textEditLocationID.Text.Equals(""))
            {
                return;
            }
            location.LocationID = textEditLocationID.Text;
            location.Description = textEditDescription.Text;
            location.Status = "New";
            location.WarehouseID = "";
            location.CreatedUserID = WMMessage.User.UserID;
            location.CreatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));
            if (await locationRepository.Create(location) > 0)
            {
                WMPublic.sbMessageSaveNewSuccess(this);
                sbLoadDataForGridAsync();
            }
        }
        private async Task UpdateAsync() {
            if (textEditLocationID.Text.Equals(""))
            {
                return;
            }
            Models.Location location = await locationRepository.GetUnderID(textEditLocationID.Text);
            location.LocationID = textEditLocationID.Text;
            location.Description = textEditDescription.Text;
            location.Status = "Updated";
            location.WarehouseID = "";
            location.UpdatedUserID = WMMessage.User.UserID;
            location.UpdatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));
            if (await locationRepository.Update(location, textEditLocationID.Text) > 0)
            {
                WMPublic.sbMessageSaveChangeSuccess(this);
                sbLoadDataForGridAsync();
            }
        }

        private async Task DeleteAsync()
        {
            if (await locationRepository.Delete(textEditLocationID.Text) > 0)
            {
                WMPublic.sbMessageDeleteSuccess(this);
                sbLoadDataForGridAsync();
            }
        }

        private async Task sbLoadDataForGridAsync() {
            List<Models.Location> locations = await locationRepository.GetAll();
            int mMaxRow = 100;
            if (locations.Count < mMaxRow)
            {
                int num = mMaxRow - locations.Count;
                int i = 1;
                while (true)
                {
                    int num2 = i;
                    int num3 = num;
                    if (num2 > num3)
                    {
                        break;
                    }
                    locations.Add(new Location());
                    i++;
                }
            }
            gridControlData.DataSource = locations;
        }
        private void clear() {
            textEditLocationID.Text = "";
            textEditDescription.Text = "";
        }
        private async Task<bool> checkExistAsync()
        {
            Boolean isExist = await locationRepository.checkExistAsync(textEditLocationID.Text);
            return isExist;
        }

        private void gridView_RowClick(Object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            textEditLocationID.Text = (string)(sender as GridView).GetFocusedRowCellValue("LocationID");
            textEditDescription.Text= (string)(sender as GridView).GetFocusedRowCellValue("Description");
        }

        private async Task selectFileExcelAsync()
        {
            OpenFileDialog fdlg = new OpenFileDialog();
            fdlg.Title = "Select file";
            fdlg.InitialDirectory = @"d:\";
            fdlg.Filter = "Excel Sheet(*.xls)|*.xlsx|All Files(*.*)|*.*";
            fdlg.FilterIndex = 1;
            fdlg.RestoreDirectory = true;
            if (fdlg.ShowDialog() == DialogResult.OK)
            {
                List<Location> locations = await ExcelToListAsync(fdlg.FileName);
                
                //compare with goodsData
                try
                {
                    sumAddNew = 0;
                    Cursor = Cursors.WaitCursor;
                    await compareAndCreate(locations);
                }
                finally
                {
                    MessageBox.Show("Đã thêm mới " + sumAddNew + " location id");
                    Cursor = Cursors.Default;
                }
                sbLoadDataForGridAsync();
                Application.DoEvents();
            }
        }

        private async Task compareAndCreate(List<Location> list)
        {
            List<Location> locationsSaved = await locationRepository.GetAll();
            if (locationsSaved.Count > 0)
            {
                foreach (Location location in list)
                {
                    textEditLocationCheck.Text = location.LocationID;
                    if (await locationRepository.checkExistAsync(location.LocationID) == false)
                    {
                        location.Description = "";
                        location.Status = "New";
                        location.WarehouseID = "";
                        location.CreatedUserID = WMMessage.User.UserID;
                        location.CreatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));
                        if (await locationRepository.Create(location) > 0)
                        {
                            sumAddNew++;
                        }
                    }
                }
            }
            else
            {
                foreach (Location location in list)
                {
                    location.Description = "";
                    location.Status = "New";
                    location.WarehouseID = "";
                    location.CreatedUserID = WMMessage.User.UserID;
                    location.CreatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));
                    if (await locationRepository.Create(location) > 0)
                    {
                        sumAddNew++;
                    };
                }
            }

        }

        private async Task<List<Location>> ExcelToListAsync(String fileName)
        {
            FileInfo file = new FileInfo(Path.Combine(fileName));

            ExcelPackage.LicenseContext = LicenseContext.Commercial;
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            using (ExcelPackage package = new ExcelPackage(file))
            {
                ExcelWorksheet workSheet = package.Workbook.Worksheets.First();
                int totalRows = workSheet.Dimension.Rows;
                List<Location> locations = new List<Location>();

                for (int i = 2; i <= totalRows; i++)
                {
                    if (workSheet?.Cells[i, 1].Text.Trim() != "")
                    {
                        locations.Add(new Location
                        {
                             LocationID= workSheet?.Cells[i, 1].Text.Trim().Replace("-", "").Replace(" ", ""),
                        });
                    }
                }
                return locations;
            }
        }

        //barbutton click
        public async void button_Click(object sender, EventArgs e)
        {

            ButtonEventArgs eArg = (DevExpress.XtraBars.Docking2010.ButtonEventArgs)e;
            String tag = eArg.Button.Properties.Tag.ToString();
            switch (tag)
            {
                case "new":
                    if (WMPublic.sbMessageAddNewRequest(this) == true)
                    {
                        clear();
                    }
                    break;
                case "save":

                    if (await checkExistAsync() == true)
                    {
                        if (WMPublic.sbMessageSaveChangeRequest(this) == true)
                        {
                            UpdateAsync();
                        }

                    }
                    else
                    {
                        if (WMPublic.sbMessageSaveNewRequest(this) == true)
                        {
                            CreatAsync();
                        }
                    }
                    break;
                case "delete":
                    if (WMPublic.sbMessageDeleteRequest(this) == true)
                    {
                        DeleteAsync();
                    }
                    break;
                case "search":
                    break;
                case "refesh":
                    sbLoadDataForGridAsync();
                    break;
                case "import":
                    selectFileExcelAsync();
                    break;
                case "export":
                    break;
                case "print":
                    break;
                case "close":
                    this.Dispose();
                    break;
            }

        }
    }
}
