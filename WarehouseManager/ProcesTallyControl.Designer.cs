﻿
namespace WarehouseManager
{
    partial class ProcesTallyControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions1 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProcesTallyControl));
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions2 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions3 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions4 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition1 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition2 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition3 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition4 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition5 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition6 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition1 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition2 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition3 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition7 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition8 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition9 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition10 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition11 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition12 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition13 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition14 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition4 = new DevExpress.XtraLayout.RowDefinition();
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.windowsUIButtonPanel1 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.searchControl1 = new DevExpress.XtraEditors.SearchControl();
            this.radioButtonBeforeTallyHour = new System.Windows.Forms.RadioButton();
            this.radioButtonBeforeTallyDate = new System.Windows.Forms.RadioButton();
            this.radioButtonFortuity = new System.Windows.Forms.RadioButton();
            this.radioButtonAll = new System.Windows.Forms.RadioButton();
            this.comboBoxEditBranch = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEditWarehouse = new DevExpress.XtraEditors.ComboBoxEdit();
            this.dateEditTallyDate = new DevExpress.XtraEditors.DateEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemBranch = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemWarehouse = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemTallyDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleLabelItem1 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleLabelItem2 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.textEditotalLedgerQuantity = new DevExpress.XtraEditors.TextEdit();
            this.textEditotalLedgerAmount = new DevExpress.XtraEditors.TextEdit();
            this.textEditTotalTallyQuantity = new DevExpress.XtraEditors.TextEdit();
            this.textEditTotalDifferenceQuantity = new DevExpress.XtraEditors.TextEdit();
            this.textEditTotalDifferenceAmount = new DevExpress.XtraEditors.TextEdit();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemTotalTallyQuantity = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemTotalLedgerQuantity = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemTotalDifferenceQuantity = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControl3 = new DevExpress.XtraLayout.LayoutControl();
            this.gridControlData = new DevExpress.XtraGrid.GridControl();
            this.gridViewData = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.searchControl1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditBranch.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditWarehouse.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTallyDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTallyDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemBranch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWarehouse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTallyDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditotalLedgerQuantity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditotalLedgerAmount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalTallyQuantity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalDifferenceQuantity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalDifferenceAmount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalTallyQuantity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalLedgerQuantity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalDifferenceQuantity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).BeginInit();
            this.layoutControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            this.SuspendLayout();
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.windowsUIButtonPanel1);
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 0);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(716, 0, 650, 400);
            this.dataLayoutControl1.Root = this.Root;
            this.dataLayoutControl1.Size = new System.Drawing.Size(1167, 37);
            this.dataLayoutControl1.TabIndex = 0;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // windowsUIButtonPanel1
            // 
            this.windowsUIButtonPanel1.AllowGlyphSkinning = false;
            this.windowsUIButtonPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            windowsUIButtonImageOptions1.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions1.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("windowsUIButtonImageOptions1.SvgImage")));
            windowsUIButtonImageOptions2.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions2.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("windowsUIButtonImageOptions2.SvgImage")));
            windowsUIButtonImageOptions3.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions3.Image")));
            windowsUIButtonImageOptions3.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions4.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions4.Image")));
            windowsUIButtonImageOptions4.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            this.windowsUIButtonPanel1.Buttons.AddRange(new DevExpress.XtraEditors.ButtonPanel.IBaseButton[] {
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("Tổng hợp dữ liệu", true, windowsUIButtonImageOptions1, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "collect", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("Xử lý dữ liệu", true, windowsUIButtonImageOptions2, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "process", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F6) Làm mới", true, windowsUIButtonImageOptions3, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "refesh", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F10) Thoát", true, windowsUIButtonImageOptions4, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "close", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator()});
            this.windowsUIButtonPanel1.ContentAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.windowsUIButtonPanel1.Location = new System.Drawing.Point(2, 2);
            this.windowsUIButtonPanel1.Name = "windowsUIButtonPanel1";
            this.windowsUIButtonPanel1.Padding = new System.Windows.Forms.Padding(5);
            this.windowsUIButtonPanel1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.windowsUIButtonPanel1.Size = new System.Drawing.Size(1163, 32);
            this.windowsUIButtonPanel1.TabIndex = 7;
            this.windowsUIButtonPanel1.Text = "windowsUIButtonPanel1";
            this.windowsUIButtonPanel1.UseButtonBackgroundImages = false;
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.Root.Name = "Root";
            this.Root.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.Root.Size = new System.Drawing.Size(1167, 37);
            this.Root.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.windowsUIButtonPanel1;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(1167, 37);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.searchControl1);
            this.layoutControl1.Controls.Add(this.radioButtonBeforeTallyHour);
            this.layoutControl1.Controls.Add(this.radioButtonBeforeTallyDate);
            this.layoutControl1.Controls.Add(this.radioButtonFortuity);
            this.layoutControl1.Controls.Add(this.radioButtonAll);
            this.layoutControl1.Controls.Add(this.comboBoxEditBranch);
            this.layoutControl1.Controls.Add(this.comboBoxEditWarehouse);
            this.layoutControl1.Controls.Add(this.dateEditTallyDate);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl1.Location = new System.Drawing.Point(0, 37);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(455, 214, 650, 400);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(1167, 101);
            this.layoutControl1.TabIndex = 1;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // searchControl1
            // 
            this.searchControl1.Location = new System.Drawing.Point(12, 72);
            this.searchControl1.Name = "searchControl1";
            this.searchControl1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Repository.ClearButton(),
            new DevExpress.XtraEditors.Repository.SearchButton()});
            this.searchControl1.Size = new System.Drawing.Size(1143, 20);
            this.searchControl1.StyleController = this.layoutControl1;
            this.searchControl1.TabIndex = 11;
            // 
            // radioButtonBeforeTallyHour
            // 
            this.radioButtonBeforeTallyHour.Location = new System.Drawing.Point(929, 42);
            this.radioButtonBeforeTallyHour.Name = "radioButtonBeforeTallyHour";
            this.radioButtonBeforeTallyHour.Size = new System.Drawing.Size(226, 25);
            this.radioButtonBeforeTallyHour.TabIndex = 10;
            this.radioButtonBeforeTallyHour.TabStop = true;
            this.radioButtonBeforeTallyHour.Text = "Trước giờ kiểm kê";
            this.radioButtonBeforeTallyHour.UseVisualStyleBackColor = true;
            // 
            // radioButtonBeforeTallyDate
            // 
            this.radioButtonBeforeTallyDate.Location = new System.Drawing.Point(757, 42);
            this.radioButtonBeforeTallyDate.Name = "radioButtonBeforeTallyDate";
            this.radioButtonBeforeTallyDate.Size = new System.Drawing.Size(168, 25);
            this.radioButtonBeforeTallyDate.TabIndex = 9;
            this.radioButtonBeforeTallyDate.TabStop = true;
            this.radioButtonBeforeTallyDate.Text = "Trước ngày kiểm kê";
            this.radioButtonBeforeTallyDate.UseVisualStyleBackColor = true;
            // 
            // radioButtonFortuity
            // 
            this.radioButtonFortuity.Location = new System.Drawing.Point(585, 42);
            this.radioButtonFortuity.Name = "radioButtonFortuity";
            this.radioButtonFortuity.Size = new System.Drawing.Size(168, 25);
            this.radioButtonFortuity.TabIndex = 8;
            this.radioButtonFortuity.TabStop = true;
            this.radioButtonFortuity.Text = "Đột xuất";
            this.radioButtonFortuity.UseVisualStyleBackColor = true;
            // 
            // radioButtonAll
            // 
            this.radioButtonAll.Location = new System.Drawing.Point(413, 42);
            this.radioButtonAll.Name = "radioButtonAll";
            this.radioButtonAll.Size = new System.Drawing.Size(168, 25);
            this.radioButtonAll.TabIndex = 7;
            this.radioButtonAll.TabStop = true;
            this.radioButtonAll.Text = "Tất cả";
            this.radioButtonAll.UseVisualStyleBackColor = true;
            // 
            // comboBoxEditBranch
            // 
            this.comboBoxEditBranch.Location = new System.Drawing.Point(107, 12);
            this.comboBoxEditBranch.Name = "comboBoxEditBranch";
            this.comboBoxEditBranch.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditBranch.Size = new System.Drawing.Size(130, 20);
            this.comboBoxEditBranch.StyleController = this.layoutControl1;
            this.comboBoxEditBranch.TabIndex = 4;
            // 
            // comboBoxEditWarehouse
            // 
            this.comboBoxEditWarehouse.Location = new System.Drawing.Point(107, 42);
            this.comboBoxEditWarehouse.Name = "comboBoxEditWarehouse";
            this.comboBoxEditWarehouse.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditWarehouse.Size = new System.Drawing.Size(130, 20);
            this.comboBoxEditWarehouse.StyleController = this.layoutControl1;
            this.comboBoxEditWarehouse.TabIndex = 5;
            // 
            // dateEditTallyDate
            // 
            this.dateEditTallyDate.EditValue = null;
            this.dateEditTallyDate.Location = new System.Drawing.Point(336, 12);
            this.dateEditTallyDate.Name = "dateEditTallyDate";
            this.dateEditTallyDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditTallyDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditTallyDate.Size = new System.Drawing.Size(73, 20);
            this.dateEditTallyDate.StyleController = this.layoutControl1;
            this.dateEditTallyDate.TabIndex = 6;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemBranch,
            this.layoutControlItemWarehouse,
            this.layoutControlItemTallyDate,
            this.simpleLabelItem1,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.simpleLabelItem2,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem9});
            this.layoutControlGroup1.LayoutMode = DevExpress.XtraLayout.Utils.LayoutMode.Table;
            this.layoutControlGroup1.Name = "Root";
            columnDefinition1.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition1.Width = 20D;
            columnDefinition2.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition2.Width = 15D;
            columnDefinition3.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition3.Width = 15D;
            columnDefinition4.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition4.Width = 15D;
            columnDefinition5.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition5.Width = 15D;
            columnDefinition6.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition6.Width = 20D;
            this.layoutControlGroup1.OptionsTableLayoutGroup.ColumnDefinitions.AddRange(new DevExpress.XtraLayout.ColumnDefinition[] {
            columnDefinition1,
            columnDefinition2,
            columnDefinition3,
            columnDefinition4,
            columnDefinition5,
            columnDefinition6});
            rowDefinition1.Height = 33.333333333333336D;
            rowDefinition1.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition2.Height = 33.333333333333336D;
            rowDefinition2.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition3.Height = 33.333333333333336D;
            rowDefinition3.SizeType = System.Windows.Forms.SizeType.Percent;
            this.layoutControlGroup1.OptionsTableLayoutGroup.RowDefinitions.AddRange(new DevExpress.XtraLayout.RowDefinition[] {
            rowDefinition1,
            rowDefinition2,
            rowDefinition3});
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(10, 10, 10, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(1167, 101);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItemBranch
            // 
            this.layoutControlItemBranch.Control = this.comboBoxEditBranch;
            this.layoutControlItemBranch.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemBranch.Name = "layoutControlItemBranch";
            this.layoutControlItemBranch.Size = new System.Drawing.Size(229, 30);
            this.layoutControlItemBranch.Text = "Chi nhánh";
            this.layoutControlItemBranch.TextSize = new System.Drawing.Size(92, 13);
            // 
            // layoutControlItemWarehouse
            // 
            this.layoutControlItemWarehouse.Control = this.comboBoxEditWarehouse;
            this.layoutControlItemWarehouse.Location = new System.Drawing.Point(0, 30);
            this.layoutControlItemWarehouse.Name = "layoutControlItemWarehouse";
            this.layoutControlItemWarehouse.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItemWarehouse.Size = new System.Drawing.Size(229, 30);
            this.layoutControlItemWarehouse.Text = "Kho kiểm kê";
            this.layoutControlItemWarehouse.TextSize = new System.Drawing.Size(92, 13);
            // 
            // layoutControlItemTallyDate
            // 
            this.layoutControlItemTallyDate.Control = this.dateEditTallyDate;
            this.layoutControlItemTallyDate.Location = new System.Drawing.Point(229, 0);
            this.layoutControlItemTallyDate.Name = "layoutControlItemTallyDate";
            this.layoutControlItemTallyDate.OptionsTableLayoutItem.ColumnIndex = 1;
            this.layoutControlItemTallyDate.Size = new System.Drawing.Size(172, 30);
            this.layoutControlItemTallyDate.Text = "Ngày kiểm kê";
            this.layoutControlItemTallyDate.TextSize = new System.Drawing.Size(92, 13);
            // 
            // simpleLabelItem1
            // 
            this.simpleLabelItem1.AllowHotTrack = false;
            this.simpleLabelItem1.Location = new System.Drawing.Point(401, 0);
            this.simpleLabelItem1.Name = "simpleLabelItem1";
            this.simpleLabelItem1.OptionsTableLayoutItem.ColumnIndex = 2;
            this.simpleLabelItem1.Size = new System.Drawing.Size(172, 30);
            this.simpleLabelItem1.Text = "Loại kiểm kê";
            this.simpleLabelItem1.TextSize = new System.Drawing.Size(92, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.radioButtonAll;
            this.layoutControlItem5.Location = new System.Drawing.Point(401, 30);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.OptionsTableLayoutItem.ColumnIndex = 2;
            this.layoutControlItem5.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItem5.Size = new System.Drawing.Size(172, 30);
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.radioButtonFortuity;
            this.layoutControlItem6.Location = new System.Drawing.Point(573, 30);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.OptionsTableLayoutItem.ColumnIndex = 3;
            this.layoutControlItem6.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItem6.Size = new System.Drawing.Size(172, 30);
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextVisible = false;
            // 
            // simpleLabelItem2
            // 
            this.simpleLabelItem2.AllowHotTrack = false;
            this.simpleLabelItem2.Location = new System.Drawing.Point(745, 0);
            this.simpleLabelItem2.Name = "simpleLabelItem2";
            this.simpleLabelItem2.OptionsTableLayoutItem.ColumnIndex = 4;
            this.simpleLabelItem2.Size = new System.Drawing.Size(172, 30);
            this.simpleLabelItem2.Text = "Loại dữ liệu kiểm kê";
            this.simpleLabelItem2.TextSize = new System.Drawing.Size(92, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.radioButtonBeforeTallyDate;
            this.layoutControlItem7.Location = new System.Drawing.Point(745, 30);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.OptionsTableLayoutItem.ColumnIndex = 4;
            this.layoutControlItem7.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItem7.Size = new System.Drawing.Size(172, 30);
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.radioButtonBeforeTallyHour;
            this.layoutControlItem8.Location = new System.Drawing.Point(917, 30);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.OptionsTableLayoutItem.ColumnIndex = 5;
            this.layoutControlItem8.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItem8.Size = new System.Drawing.Size(230, 30);
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextVisible = false;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.searchControl1;
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 60);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.OptionsTableLayoutItem.ColumnSpan = 6;
            this.layoutControlItem9.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlItem9.Size = new System.Drawing.Size(1147, 31);
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextVisible = false;
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.textEditotalLedgerQuantity);
            this.layoutControl2.Controls.Add(this.textEditotalLedgerAmount);
            this.layoutControl2.Controls.Add(this.textEditTotalTallyQuantity);
            this.layoutControl2.Controls.Add(this.textEditTotalDifferenceQuantity);
            this.layoutControl2.Controls.Add(this.textEditTotalDifferenceAmount);
            this.layoutControl2.Controls.Add(this.textEdit1);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.layoutControl2.Location = new System.Drawing.Point(0, 723);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(596, 51, 650, 400);
            this.layoutControl2.Root = this.layoutControlGroup2;
            this.layoutControl2.Size = new System.Drawing.Size(1167, 37);
            this.layoutControl2.TabIndex = 2;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // textEditotalLedgerQuantity
            // 
            this.textEditotalLedgerQuantity.Location = new System.Drawing.Point(554, 2);
            this.textEditotalLedgerQuantity.Name = "textEditotalLedgerQuantity";
            this.textEditotalLedgerQuantity.Size = new System.Drawing.Size(128, 20);
            this.textEditotalLedgerQuantity.StyleController = this.layoutControl2;
            this.textEditotalLedgerQuantity.TabIndex = 6;
            // 
            // textEditotalLedgerAmount
            // 
            this.textEditotalLedgerAmount.Location = new System.Drawing.Point(686, 2);
            this.textEditotalLedgerAmount.Name = "textEditotalLedgerAmount";
            this.textEditotalLedgerAmount.Size = new System.Drawing.Size(63, 20);
            this.textEditotalLedgerAmount.StyleController = this.layoutControl2;
            this.textEditotalLedgerAmount.TabIndex = 5;
            // 
            // textEditTotalTallyQuantity
            // 
            this.textEditTotalTallyQuantity.Location = new System.Drawing.Point(150, 2);
            this.textEditTotalTallyQuantity.Name = "textEditTotalTallyQuantity";
            this.textEditTotalTallyQuantity.Size = new System.Drawing.Size(128, 20);
            this.textEditTotalTallyQuantity.StyleController = this.layoutControl2;
            this.textEditTotalTallyQuantity.TabIndex = 4;
            // 
            // textEditTotalDifferenceQuantity
            // 
            this.textEditTotalDifferenceQuantity.Location = new System.Drawing.Point(958, 2);
            this.textEditTotalDifferenceQuantity.Name = "textEditTotalDifferenceQuantity";
            this.textEditTotalDifferenceQuantity.Size = new System.Drawing.Size(128, 20);
            this.textEditTotalDifferenceQuantity.StyleController = this.layoutControl2;
            this.textEditTotalDifferenceQuantity.TabIndex = 7;
            // 
            // textEditTotalDifferenceAmount
            // 
            this.textEditTotalDifferenceAmount.Location = new System.Drawing.Point(1090, 2);
            this.textEditTotalDifferenceAmount.Name = "textEditTotalDifferenceAmount";
            this.textEditTotalDifferenceAmount.Size = new System.Drawing.Size(65, 20);
            this.textEditTotalDifferenceAmount.StyleController = this.layoutControl2;
            this.textEditTotalDifferenceAmount.TabIndex = 8;
            // 
            // textEdit1
            // 
            this.textEdit1.Location = new System.Drawing.Point(282, 2);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Size = new System.Drawing.Size(63, 20);
            this.textEdit1.StyleController = this.layoutControl2;
            this.textEdit1.TabIndex = 9;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3,
            this.layoutControlItemTotalTallyQuantity,
            this.layoutControlItemTotalLedgerQuantity,
            this.layoutControlItemTotalDifferenceQuantity,
            this.layoutControlItem12,
            this.layoutControlItem2});
            this.layoutControlGroup2.LayoutMode = DevExpress.XtraLayout.Utils.LayoutMode.Table;
            this.layoutControlGroup2.Name = "Root";
            columnDefinition7.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition7.Width = 23.529411764705884D;
            columnDefinition8.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition8.Width = 5.882352941176471D;
            columnDefinition9.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition9.Width = 5.882352941176471D;
            columnDefinition10.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition10.Width = 23.529411764705884D;
            columnDefinition11.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition11.Width = 5.882352941176471D;
            columnDefinition12.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition12.Width = 5.882352941176471D;
            columnDefinition13.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition13.Width = 23.529411764705884D;
            columnDefinition14.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition14.Width = 5.882352941176471D;
            this.layoutControlGroup2.OptionsTableLayoutGroup.ColumnDefinitions.AddRange(new DevExpress.XtraLayout.ColumnDefinition[] {
            columnDefinition7,
            columnDefinition8,
            columnDefinition9,
            columnDefinition10,
            columnDefinition11,
            columnDefinition12,
            columnDefinition13,
            columnDefinition14});
            rowDefinition4.Height = 100D;
            rowDefinition4.SizeType = System.Windows.Forms.SizeType.Percent;
            this.layoutControlGroup2.OptionsTableLayoutGroup.RowDefinitions.AddRange(new DevExpress.XtraLayout.RowDefinition[] {
            rowDefinition4});
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(10, 10, 0, 10);
            this.layoutControlGroup2.Size = new System.Drawing.Size(1167, 37);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.textEditotalLedgerAmount;
            this.layoutControlItem3.Location = new System.Drawing.Point(674, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.OptionsTableLayoutItem.ColumnIndex = 4;
            this.layoutControlItem3.Size = new System.Drawing.Size(67, 27);
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItemTotalTallyQuantity
            // 
            this.layoutControlItemTotalTallyQuantity.Control = this.textEditTotalTallyQuantity;
            this.layoutControlItemTotalTallyQuantity.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItemTotalTallyQuantity.CustomizationFormText = "Σ Kiểm kê: Lượng/Giá trị:";
            this.layoutControlItemTotalTallyQuantity.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemTotalTallyQuantity.Name = "layoutControlItemTotalTallyQuantity";
            this.layoutControlItemTotalTallyQuantity.Size = new System.Drawing.Size(270, 27);
            this.layoutControlItemTotalTallyQuantity.Text = "Σ Kiểm kê: Lượng/Giá trị:";
            this.layoutControlItemTotalTallyQuantity.TextSize = new System.Drawing.Size(135, 13);
            // 
            // layoutControlItemTotalLedgerQuantity
            // 
            this.layoutControlItemTotalLedgerQuantity.Control = this.textEditotalLedgerQuantity;
            this.layoutControlItemTotalLedgerQuantity.Location = new System.Drawing.Point(404, 0);
            this.layoutControlItemTotalLedgerQuantity.Name = "layoutControlItemTotalLedgerQuantity";
            this.layoutControlItemTotalLedgerQuantity.OptionsTableLayoutItem.ColumnIndex = 3;
            this.layoutControlItemTotalLedgerQuantity.Size = new System.Drawing.Size(270, 27);
            this.layoutControlItemTotalLedgerQuantity.Text = "Σ Sổ sách: Lượng/Giá trị:";
            this.layoutControlItemTotalLedgerQuantity.TextSize = new System.Drawing.Size(135, 13);
            // 
            // layoutControlItemTotalDifferenceQuantity
            // 
            this.layoutControlItemTotalDifferenceQuantity.Control = this.textEditTotalDifferenceQuantity;
            this.layoutControlItemTotalDifferenceQuantity.Location = new System.Drawing.Point(808, 0);
            this.layoutControlItemTotalDifferenceQuantity.Name = "layoutControlItemTotalDifferenceQuantity";
            this.layoutControlItemTotalDifferenceQuantity.OptionsTableLayoutItem.ColumnIndex = 6;
            this.layoutControlItemTotalDifferenceQuantity.Size = new System.Drawing.Size(270, 27);
            this.layoutControlItemTotalDifferenceQuantity.Text = "Σ Chênh lệch: Lượng/Giá trị:";
            this.layoutControlItemTotalDifferenceQuantity.TextSize = new System.Drawing.Size(135, 13);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.textEditTotalDifferenceAmount;
            this.layoutControlItem12.Location = new System.Drawing.Point(1078, 0);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.OptionsTableLayoutItem.ColumnIndex = 7;
            this.layoutControlItem12.Size = new System.Drawing.Size(69, 27);
            this.layoutControlItem12.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem12.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.textEdit1;
            this.layoutControlItem2.Location = new System.Drawing.Point(270, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.OptionsTableLayoutItem.ColumnIndex = 1;
            this.layoutControlItem2.Size = new System.Drawing.Size(67, 27);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControl3
            // 
            this.layoutControl3.Controls.Add(this.gridControlData);
            this.layoutControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl3.Location = new System.Drawing.Point(0, 138);
            this.layoutControl3.Name = "layoutControl3";
            this.layoutControl3.Root = this.layoutControlGroup3;
            this.layoutControl3.Size = new System.Drawing.Size(1167, 585);
            this.layoutControl3.TabIndex = 3;
            this.layoutControl3.Text = "layoutControl3";
            // 
            // gridControlData
            // 
            this.gridControlData.Location = new System.Drawing.Point(12, 12);
            this.gridControlData.MainView = this.gridViewData;
            this.gridControlData.Name = "gridControlData";
            this.gridControlData.Size = new System.Drawing.Size(1143, 561);
            this.gridControlData.TabIndex = 4;
            this.gridControlData.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewData});
            // 
            // gridViewData
            // 
            this.gridViewData.GridControl = this.gridControlData;
            this.gridViewData.Name = "gridViewData";
            this.gridViewData.OptionsFind.ShowSearchNavButtons = false;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4});
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(1167, 585);
            this.layoutControlGroup3.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.gridControlData;
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(1147, 565);
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // ProcesTallyControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl3);
            this.Controls.Add(this.layoutControl2);
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.dataLayoutControl1);
            this.Name = "ProcesTallyControl";
            this.Size = new System.Drawing.Size(1167, 760);
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.searchControl1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditBranch.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditWarehouse.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTallyDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTallyDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemBranch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWarehouse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTallyDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEditotalLedgerQuantity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditotalLedgerAmount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalTallyQuantity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalDifferenceQuantity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalDifferenceAmount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalTallyQuantity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalLedgerQuantity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalDifferenceQuantity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).EndInit();
            this.layoutControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel windowsUIButtonPanel1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.SearchControl searchControl1;
        private System.Windows.Forms.RadioButton radioButtonBeforeTallyHour;
        private System.Windows.Forms.RadioButton radioButtonBeforeTallyDate;
        private System.Windows.Forms.RadioButton radioButtonFortuity;
        private System.Windows.Forms.RadioButton radioButtonAll;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditBranch;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditWarehouse;
        private DevExpress.XtraEditors.DateEdit dateEditTallyDate;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemBranch;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemWarehouse;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemTallyDate;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraEditors.TextEdit textEditotalLedgerAmount;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraEditors.TextEdit textEditotalLedgerQuantity;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemTotalLedgerQuantity;
        private DevExpress.XtraEditors.TextEdit textEditTotalTallyQuantity;
        private DevExpress.XtraEditors.TextEdit textEditTotalDifferenceQuantity;
        private DevExpress.XtraEditors.TextEdit textEditTotalDifferenceAmount;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemTotalTallyQuantity;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemTotalDifferenceQuantity;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControl layoutControl3;
        private DevExpress.XtraGrid.GridControl gridControlData;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewData;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
    }
}
