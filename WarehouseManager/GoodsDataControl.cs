﻿using DevExpress.XtraBars.Docking2010;
using DevExpress.XtraGrid.Views.Grid;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WarehouseManager.Models;
using WarehouseManager.Repository;
using LicenseContext = OfficeOpenXml.LicenseContext;

namespace WarehouseManager
{

    public partial class GoodsDataControl : DevExpress.XtraEditors.XtraUserControl
    {
        GoodsDataRepository goodsDataRepository;
        int sumAddNew = 0;
        int sumUpdate = 0;
        Boolean isExist = false;
        public GoodsDataControl()
        {
            InitializeComponent();
            goodsDataRepository = new GoodsDataRepository();
            this.Load += GoodsDataControl_Load;
        }

        private void GoodsDataControl_Load(Object sender, EventArgs e)
        {
            windowsUIButtonPanel1.ButtonClick += button_Click;
            gridViewData.RowClick += gridView_RowClick;
            gridViewData.CustomColumnDisplayText += GridView_CustomColumnDisplayText;
            sbLoadDataForGridAsync();
        }

        private void GridView_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {

            if (e.Column == colNo)
            {
                int rowHandle = gridViewData.GetRowHandle(e.ListSourceRowIndex);
                if (rowHandle >= 0)
                {
                    e.DisplayText = (rowHandle + 1).ToString();
                }
            }
        }

        private async Task CreatAsync()
        {
            GoodsData goodsData = new GoodsData();
            if (textEditGoodsID.Text.Equals("") || textEditGoodsName.Text.Equals(""))
            {
                return;
            }
            goodsData.GoodsID = textEditGoodsID.Text;
            goodsData.GoodsName = textEditGoodsName.Text;
            goodsData.GoodsNameEN = textEditGoodsNameEN.Text;
            goodsData.Description = textEditDescription.Text;
            goodsData.SLPart = textEditSLPart.Text;
            goodsData.Status = "NEW";
            goodsData.CreatedUserID = WMMessage.User.UserID;
            goodsData.CreatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));
            if (await goodsDataRepository.Create(goodsData) > 0)
            {
                WMPublic.sbMessageSaveNewSuccess(this);
                sbLoadDataForGridAsync();
            }
        }

        private async Task UpdateAsync()
        {
            if (textEditGoodsID.Text.Equals("") || textEditGoodsName.Text.Equals(""))
            {
                return;
            }
            GoodsData goodsData = await goodsDataRepository.GetUnderID(textEditGoodsID.Text);
            goodsData.GoodsID = textEditGoodsID.Text;
            goodsData.GoodsName = textEditGoodsName.Text;
            goodsData.GoodsNameEN = textEditGoodsNameEN.Text;
            goodsData.Description = textEditDescription.Text;
            goodsData.SLPart = textEditSLPart.Text;
            goodsData.Status = "UPDATED";
            goodsData.UpdatedUserID = WMMessage.User.UserID;
            goodsData.UpdatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));
            if (await goodsDataRepository.Update(goodsData, textEditGoodsID.Text) > 0)
            {
                WMPublic.sbMessageSaveChangeSuccess(this);
                sbLoadDataForGridAsync();
            }
        }

        private async Task CreatFromGridViewAsync()
        {
            int num = gridViewData.RowCount - 1;
            int i = 0;
            while (true)
            {
                int num2 = i;
                int num3 = num;
                if (num2 > num3 || gridViewData.GetRowCellValue(i, gridViewData.Columns["colGoodsID"]) == "")
                {
                    break;
                }
                int rowHandle = gridViewData.GetRowHandle(i);
                GoodsData goodsData = new GoodsData();
                goodsData.GoodsID = (string?)gridViewData.GetRowCellValue(rowHandle, "GoodsID");
                goodsData.GoodsName = (string?)gridViewData.GetRowCellValue(rowHandle, "GoodsName");
                goodsData.GoodsNameEN = (string?)gridViewData.GetRowCellValue(rowHandle, "GoodsNameEN");
                goodsData.Description = (string?)gridViewData.GetRowCellValue(rowHandle, "Description");
                goodsData.SLPart = (string?)gridViewData.GetRowCellValue(rowHandle, "SLPart");
                goodsData.Status = "NEW";
                goodsData.CreatedUserID = WMMessage.User.UserID;
                goodsData.CreatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));
                if (await goodsDataRepository.Create(goodsData) > 0)
                {
                    i++;
                }
            }
        }

        private async Task UpdateFromGridViewAsync()
        {
            int num = gridViewData.RowCount - 1;
            int i = 0;
            while (true)
            {
                int num2 = i;
                int num3 = num;
                if (num2 > num3 || gridViewData.GetRowCellValue(i, gridViewData.Columns["colGoodsID"]) == "")
                {
                    break;
                }
                int rowHandle = gridViewData.GetRowHandle(i);
                GoodsData goodsData = new GoodsData();
                goodsData.GoodsID = (string?)gridViewData.GetRowCellValue(rowHandle, "GoodsID");
                goodsData.GoodsName = (string?)gridViewData.GetRowCellValue(rowHandle, "GoodsName");
                goodsData.GoodsNameEN = (string?)gridViewData.GetRowCellValue(rowHandle, "GoodsNameEN");
                goodsData.Description = (string?)gridViewData.GetRowCellValue(rowHandle, "Description");
                goodsData.SLPart = (string?)gridViewData.GetRowCellValue(rowHandle, "SLPart");
                goodsData.Status = "UPDATED";
                goodsData.UpdatedUserID = WMMessage.User.UserID;
                goodsData.UpdatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));
                if (await goodsDataRepository.Update(goodsData, textEditGoodsID.Text) > 0)
                {
                    i++;
                }
            }
        }

        private async Task CreateOrUpdateFromGridViewAsync()
        {
            int num = gridViewData.RowCount - 1;
            int i = 0;
            while (true)
            {
                int num2 = i;
                int num3 = num;
                if (num2 > num3 || gridViewData.GetRowCellValue(i, gridViewData.Columns["colGoodsID"]) == "")
                {
                    break;
                }
                int rowHandle = gridViewData.GetRowHandle(i);
                GoodsData goodsData = new GoodsData();
                goodsData.GoodsID = (string?)gridViewData.GetRowCellValue(rowHandle, "GoodsID");
                goodsData.GoodsName = (string?)gridViewData.GetRowCellValue(rowHandle, "GoodsName");
                goodsData.GoodsNameEN = (string?)gridViewData.GetRowCellValue(rowHandle, "GoodsNameEN");
                goodsData.Description = (string?)gridViewData.GetRowCellValue(rowHandle, "Description");
                goodsData.SLPart = (string?)gridViewData.GetRowCellValue(rowHandle, "SLPart");
                goodsData.Status = "NEW";
                goodsData.CreatedUserID = WMMessage.User.UserID;
                goodsData.CreatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));
                if (await goodsDataRepository.checkExistAsync(goodsData.GoodsID) == false)
                {
                    await goodsDataRepository.Create(goodsData);
                }
                else
                {
                    await goodsDataRepository.Update(goodsData, goodsData.GoodsID);
                }
                i++;
            }
        }

        private async Task DeleteAsync()
        {
            if (await goodsDataRepository.Delete(textEditGoodsID.Text) > 0)
            {
                WMPublic.sbMessageDeleteSuccess(this);
                sbLoadDataForGridAsync();
            }
        }

        private async Task sbLoadDataForGridAsync()
        {
            List<GoodsData> goodsDatas = await goodsDataRepository.GetAll();
            int mMaxRow = 100;
            if (goodsDatas.Count < mMaxRow)
            {
                int num = mMaxRow - goodsDatas.Count;
                int i = 1;
                while (true)
                {
                    int num2 = i;
                    int num3 = num;
                    if (num2 > num3)
                    {
                        break;
                    }
                    goodsDatas.Add(new GoodsData());
                    i++;
                }
            }
            gridControlData.DataSource = goodsDatas;
            getTotalGoods();
        }
        private void clear()
        {
            textEditGoodsID.Text = "";
            textEditGoodsName.Text = "";
            textEditGoodsNameEN.Text = "";
            textEditSLPart.Text = "";
            textEditDescription.Text = "";
        }
        private async Task<bool> checkExistAsync()
        {
            Boolean isExist = await goodsDataRepository.checkExistAsync(textEditGoodsID.Text);
            return isExist;
        }

        private void gridView_RowClick(Object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            textEditGoodsID.Text = (string)(sender as GridView).GetFocusedRowCellValue("GoodsID");
            textEditECNPart.Text= (string)(sender as GridView).GetFocusedRowCellValue("ECNPart");
            textEditGoodsName.Text = (string)(sender as GridView).GetFocusedRowCellValue("GoodsName");
            textEditGoodsNameEN.Text = (string)(sender as GridView).GetFocusedRowCellValue("GoodsNameEN");
            textEditSLPart.Text = (string)(sender as GridView).GetFocusedRowCellValue("SLPart");
            textEditDescription.Text = (string)(sender as GridView).GetFocusedRowCellValue("Description");
        }

        private async Task getTotalGoods()
        {
            Decimal totalGoods = 0;
            int i = 0;
            while (true)
            {
                int rowHandle = gridViewData.GetRowHandle(i);
                if (gridViewData.GetRowCellValue(rowHandle, "GoodsID") == null)
                {
                    break;
                }
                totalGoods++;
                i++;
            }
            textEditTotalGoods.Text = totalGoods.ToString("0.#####");
        }

        private async Task selectFileExcelAsync()
        {
            OpenFileDialog fdlg = new OpenFileDialog();
            fdlg.Title = "Select file";
            fdlg.InitialDirectory = @"d:\";
            fdlg.Filter = "Excel Sheet(*.xls)|*.xlsx|All Files(*.*)|*.*";
            fdlg.FilterIndex = 1;
            fdlg.RestoreDirectory = true;
            if (fdlg.ShowDialog() == DialogResult.OK)
            {
                List<GoodsData> goodsDatas = await ExcelToListAsync(fdlg.FileName);
                //int mMaxRow = 100;
                //if (goodsDatas.Count < mMaxRow)
                //{
                //    int num = mMaxRow - goodsDatas.Count;
                //    int i = 1;
                //    while (true)
                //    {
                //        int num2 = i;
                //        int num3 = num;
                //        if (num2 > num3)
                //        {
                //            break;
                //        }
                //        goodsDatas.Add(new GoodsData());
                //        i++;
                //    }
                //}
                //gridControlData.DataSource = goodsDatas;
                //compare with goodsData
                try
                {
                    sumAddNew = 0;
                    sumUpdate = 0;
                    Cursor = Cursors.WaitCursor;
                    await compareGoodsIDAsync(goodsDatas);
                }
                finally
                {
                    MessageBox.Show("Đã thêm mới " + sumAddNew + " part number, cập nhật " + sumUpdate + " partnumber");
                    Cursor = Cursors.Default;
                }
                sbLoadDataForGridAsync();
                Application.DoEvents();
            }
        }

        private async Task compareGoodsIDAsync(List<GoodsData> list)
        {
            List<GoodsData> goodsDatasSaved = await goodsDataRepository.GetAll();
            if (goodsDatasSaved.Count > 0)
            {
                foreach (GoodsData goodsData in list)
                {
                    //isExist = await goodsDataRepository.checkExistAsync(goodsData.GoodsID, goodsData.GoodsName, goodsData.SLPart);
                    textEditGoodsIDCheck.Text = goodsData.GoodsID;
                    if (await goodsDataRepository.checkExistAsync(goodsData.GoodsID) == false)
                    {
                        goodsData.Status = "NEW";
                        if (await goodsDataRepository.Create(goodsData) > 0)
                        {
                            sumAddNew++;
                        }
                    }
                    else
                    {
                        //if (isExist == false)
                        //{
                        //    goodsData.Status = "UPDATED";
                        //    if (await goodsDataRepository.Update(goodsData, goodsData.GoodsID) > 0)
                        //    {
                        //        sumUpdate++;
                        //    }
                        //}
                        goodsData.Status = "UPDATED";
                        if (await goodsDataRepository.Update(goodsData, goodsData.GoodsID) > 0)
                        {
                            sumUpdate++;
                        }
                    }
                }
            }
            else
            {
                foreach (GoodsData goodsData in list)
                {
                    goodsData.Status = "NEW";
                    if (await goodsDataRepository.Create(goodsData) > 0)
                    {
                        sumAddNew++;
                    };
                }
            }

        }


        private async Task<List<GoodsData>> ExcelToListAsync(String fileName)
        {
            FileInfo file = new FileInfo(Path.Combine(fileName));

            ExcelPackage.LicenseContext = LicenseContext.Commercial;
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            using (ExcelPackage package = new ExcelPackage(file))
            {
                ExcelWorksheet workSheet = package.Workbook.Worksheets.First();
                int totalRows = workSheet.Dimension.Rows;
                List<GoodsData> goodsDatas = new List<GoodsData>();

                for (int i = 2; i <= totalRows; i++)
                {
                    if (workSheet?.Cells[i, 2].Text.Trim() != "")
                    {
                        goodsDatas.Add(new GoodsData
                        {
                            GoodsID = workSheet?.Cells[i, 1].Text.Trim().Replace("-", "").Replace(" ", ""),
                            ECNPart = workSheet?.Cells[i, 2].Text.Trim().Replace("-", "").Replace(" ", ""),
                            GoodsName = workSheet?.Cells[i, 3].Text.Trim(),
                            SLPart = workSheet?.Cells[i, 4].Text.Trim(),
                        });
                    }
                    else
                    {
                        goodsDatas.Add(new GoodsData
                        {
                            GoodsID = workSheet?.Cells[i, 1].Text.Trim().Replace("-", "").Replace(" ", ""),
                            ECNPart = "",
                            GoodsName = workSheet?.Cells[i, 3].Text.Trim(),
                            SLPart = workSheet?.Cells[i, 4].Text.Trim(),
                        });
                    }

                }
                return goodsDatas;
            }
        }


        //barbutton click
        public async void button_Click(object sender, EventArgs e)
        {

            ButtonEventArgs eArg = (DevExpress.XtraBars.Docking2010.ButtonEventArgs)e;
            String tag = eArg.Button.Properties.Tag.ToString();
            switch (tag)
            {
                case "new":
                    if (WMPublic.sbMessageAddNewRequest(this) == true)
                    {
                        clear();
                    }
                    break;
                case "save":
                    //if (WMPublic.sbMessageSaveNewRequest(this) == true)
                    //{
                    //    try
                    //    {
                    //        Cursor = Cursors.WaitCursor;
                    //        await CreateOrUpdateFromGridViewAsync();
                    //    }
                    //    finally
                    //    {
                    //        MessageBox.Show("Đã lưu dữ liệu hàng hóa");
                    //        Cursor = Cursors.Default;
                    //    }
                    //}
                    if (await checkExistAsync() == true)
                    {
                        if (WMPublic.sbMessageSaveChangeRequest(this) == true)
                        {
                            UpdateAsync();
                        }

                    }
                    else
                    {
                        if (WMPublic.sbMessageSaveNewRequest(this) == true)
                        {
                            CreatAsync();
                        }
                    }
                    break;
                case "delete":
                    if (WMPublic.sbMessageDeleteRequest(this) == true)
                    {
                        DeleteAsync();
                    }
                    break;
                case "search":
                    break;
                case "refesh":
                    sbLoadDataForGridAsync();
                    break;
                case "import":
                    selectFileExcelAsync();
                    break;
                case "export":
                    break;
                case "print":
                    break;
                case "close":
                    this.Dispose();
                    break;
            }

        }

    }
}
