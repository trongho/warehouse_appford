﻿using AutoUpdaterDotNET;
using DevExpress.XtraBars;
using DevExpress.XtraTab;
using DevExpress.XtraTab.ViewInfo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls.Ribbon;
using System.Windows.Forms;
using WarehouseManager.Helpers;
using WarehouseManager.Models;
using WarehouseManager.Repository;
using webservice.Entities;

namespace WarehouseManager
{
    public partial class MainForm : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        const int LEADING_SPACE = 12;
        const int CLOSE_SPACE = 15;
        const int CLOSE_AREA = 15;
        String tabUserManagerText = "";

        WRRHeaderRepository wRRHeaderRepository;
        WRRDetailRepository wRRDetailRepository;
        WRDataHeaderRepository wRDataHeaderRepository;
        WRDataGeneralRepository wRDataGeneralRepository;

        WIRHeaderRepository wIRHeaderRepository;
        WIRDetailRepository wIRDetailRepository;
        WIDataHeaderRepository wIDataHeaderRepository;
        WIDataGeneralRepository wIDataGeneralRepository;

        string directory;
        IniFile ini;
        string updateURL = "";

        public MainForm()
        {
            InitializeComponent();
            this.Load += MainForm_Load;
            updateLanguage();
            this.WindowState = FormWindowState.Maximized;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
            this.Dock = DockStyle.Fill;


            ribbon.ItemClick += barButtonItem_ItemClick;
            xtraTabControl1.CloseButtonClick += closeButtonTabpage_Click;

        }

        private void MainForm_Load(Object sender, EventArgs e)
        {
            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            System.Diagnostics.FileVersionInfo fvi = System.Diagnostics.FileVersionInfo.GetVersionInfo(assembly.Location);
            //AutoUpdater.CheckForUpdateEvent += AutoUpdaterOnCheckForUpdateEvent;
            string version = fvi.FileVersion;
            barStaticItemVersion.Caption = "Version: " + version;

            barHeaderUserInfo.Caption = WMMessage.User.LoginName;
            wRRHeaderRepository = new WRRHeaderRepository();
            wRRDetailRepository = new WRRDetailRepository();
            wRDataHeaderRepository = new WRDataHeaderRepository();
            wRDataGeneralRepository = new WRDataGeneralRepository();
            wIRHeaderRepository = new WIRHeaderRepository();
            wIRDetailRepository = new WIRDetailRepository();
            wIDataHeaderRepository = new WIDataHeaderRepository();
            wIDataGeneralRepository = new WIDataGeneralRepository();

            checkToDeleteReceiptDataAsync();
            checkToDeleteIssueDataAsync();

            directory = AppDomain.CurrentDomain.BaseDirectory;
            ini = new IniFile(directory + "config.ini");
            updateURL = "http://" + ini.IniReadValue("config", "ip") + ":6666/update.xml";


        }


        //xóa dữ liệu nhập hàng có thời gian quá 11 tháng
        private async void checkToDeleteReceiptDataAsync()
        {
            int count = 0;
            List<WRRHeader> wRRHeaders = await wRRHeaderRepository.GetUnderDate(DateTime.Now.Date.AddYears(-50), DateTime.Now.AddMonths(-10));
            if (wRRHeaders.Count > 0)
            {
                foreach (WRRHeader wRRHeader in wRRHeaders)
                {
                    if (await wRRDetailRepository.Delete(wRRHeader.WRRNumber) > 0)
                    {
                        await wRRHeaderRepository.Delete(wRRHeader.WRRNumber);
                        if (await wRDataGeneralRepository.Delete(wRRHeader.WRRNumber) > 0)
                        {
                            await wRDataHeaderRepository.Delete(wRRHeader.WRRNumber);
                            count++;
                            Console.WriteLine(count);
                        }
                    }
                }
            }

            //List<WRDataGeneral> wRDataGenerals = (await wRDataGeneralRepository.GetAll()).GroupBy(g=>g.WRDNumber).Select(s=>new WRDataGeneral { 
            //    WRDNumber=s.Key,
            //    CreatedDateTime=s.First().CreatedDateTime
            //}).ToList();
            //if (wRDataGenerals.Count > 0)
            //{
            //    foreach (WRDataGeneral wRDataGeneral in wRDataGenerals)
            //    {
            //        if (wRDataGeneral.CreatedDateTime.Value < DateTime.Now.AddMonths(-11))
            //        {
            //            count++;
            //            Console.WriteLine(count + ": " + wRDataGenerals[0].WRDNumber);
            //            await wRDataGeneralRepository.Delete(wRDataGeneral.WRDNumber);
            //        }
            //    }
            //}
        }

        //xóa dữ liệu xuất hàng có thời gian quá 11 tháng
        private async void checkToDeleteIssueDataAsync()
        {
            List<WIRHeader> wIRHeaders = await wIRHeaderRepository.GetAll();
            foreach (WIRHeader wIRHeader in wIRHeaders)
            {
                if (wIRHeader.WIRDate < System.DateTime.Now.AddMonths(-11))
                {
                    if (await wIRHeaderRepository.Delete(wIRHeader.WIRNumber) > 0)
                    {
                        await wIRDetailRepository.Delete(wIRHeader.WIRNumber);
                        if (await wIDataHeaderRepository.Delete(wIRHeader.WIRNumber) > 0)
                        {
                            await wIDataGeneralRepository.Delete(wIRHeader.WIRNumber);
                        }
                    }
                }

            }
        }



        private void updateLanguage()
        {
            ResourceManager rm;
            String cultureName = WMMessage.msgLanguage;
            if (cultureName.Equals("vi-VN"))
            {
                rm = new
                   ResourceManager("WarehouseManager.Resource.vi_local", typeof(MainForm).Assembly);
            }
            else
            {
                rm = new
                    ResourceManager("WarehouseManager.Resource.en_local", typeof(MainForm).Assembly);
            }

            // LoginForm.ActiveForm.Text = rm.GetString("login_form");
            ribbonPageSystem.Text = rm.GetString("system");
            ribbonPageCategory.Text = rm.GetString("category");
            ribbonPageWarehouse.Text = rm.GetString("warehouse");
            ribbonPageHelp.Text = rm.GetString("help");
            barButtonItemVietnam.Caption = rm.GetString("english");
            barButtonItemEnglish.Caption = rm.GetString("vietnamese");
            ribbonPageGroupUser.Text = rm.GetString("user");
            ribbonPageGroupUserManager.Text = rm.GetString("user_manager");
            ribbonPageGroupGoodsManager.Text = rm.GetString("goods_manager");
            barButtonItemChangePW.Caption = rm.GetString("change_password");
            barButtonItemAddUser.Caption = rm.GetString("user_role");
            barButtonItemGoods.Caption = rm.GetString("goods");
            barButtonItemGoodsLine.Caption = rm.GetString("goods_line");
            barButtonItemGoodsType.Caption = rm.GetString("goods_type");
            barButtonItemGoodsCategory.Caption = rm.GetString("goods_category");
            tabUserManagerText = rm.GetString("user_manager");
        }

        void barButtonItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            String tag = e.Item.Name;

            switch (tag)
            {
                case "barButtonItemAddUser":
                    /* Navigate to page A */
                    WMPublic.TabCreating(xtraTabControl1, tabUserManagerText, "tabUserManager", new UserManagerControl());
                    break;
                case "barButtonItemLogin":
                    /* Navigate to page B */
                    this.Dispose();
                    if (Application.OpenForms["LoginForm"] != null)
                    {
                        var Main = Application.OpenForms["LoginForm"] as LoginForm;
                        LoginForm loginForm = new LoginForm();
                        loginForm.Show();

                    }
                    break;
                case "barButtonItemGoods":
                    /* Navigate to page D */
                    WMPublic.TabCreating(xtraTabControl1, "Quản lý hàng hóa", "tabGoods", new GoodManagerControl());
                    break;
                case "barButtonItemGoodsType":
                    /* Navigate to page E */
                    WMPublic.TabCreating(xtraTabControl1, "Quản lý loại hàng hóa", "tabGoodsType", new GoodTypeControl());
                    break;
                case "barButtonItemGoodsLine":
                    /* Navigate to page E */
                    WMPublic.TabCreating(xtraTabControl1, "Quản lý ngành hàng", "tabGoodsLine", new GoodLineControl());
                    break;
                case "barButtonItemReceiptRequisition":
                    /* Navigate to page E */
                    WMPublic.TabCreating(xtraTabControl1, "Yêu cầu nhập kho", "tabReceiptRequisition", new ReceiptRequisitionControl());
                    break;
                //case "barButtonItemReceiptData":
                //    /* Navigate to page E */
                //    WMPublic.TabCreating(xtraTabControl1, "Dữ liệu nhập", "tabReceiptData", new ReceiptDataControl());
                //    break;
                case "barButtonItemReceiptData":
                    /* Navigate to page E */
                    WMPublic.TabCreating2(xtraTabControl1, "CONVEYANCE CONTENS LIST", "tabReceiptData", new ReceiptDataControl());
                    break;
                case "barButtonItemIssueRequisition":
                    /* Navigate to page E */
                    WMPublic.TabCreating(xtraTabControl1, "Yêu cầu xuất kho", "tabIssueRequisition", new IssueRequisitionControl());
                    break;
                case "barButtonItemIssueData":
                    /* Navigate to page E */
                    WMPublic.TabCreating(xtraTabControl1, "Dữ liệu xuất", "tabIssueData", new IssueDataControl());
                    break;
                case "barButtonItemWarehouseReceipt":
                    /* Navigate to page E */
                    WMPublic.TabCreating(xtraTabControl1, "Phiếu nhập kho", "tabWarehouseReceipt", new WarehouseReceiptControl());
                    break;
                case "barButtonItemProcessInventory":
                    /* Navigate to page E */
                    WMPublic.TabCreating(xtraTabControl1, "Xử lý tồn kho", "tabProcessInventory", new InventoryProcessControl());
                    break;
                case "barButtonItemIssueOrder":
                    /* Navigate to page E */
                    WMPublic.TabCreating(xtraTabControl1, "Phiếu xuất kho", "tabIssueOrder", new IssueOrderControl());
                    break;
                    break;
                case "barButtonItemReport":
                    /* Navigate to page E */
                    WMPublic.TabCreating(xtraTabControl1, "Báo cáo nhận hàng", "tabBaoCaoNhapKho", new frmBaoCaoNhapKho());
                    break;
                case "barButtonItemColor":
                    /* Navigate to page E */
                    WMPublic.TabCreating(xtraTabControl1, "Màu sắc", "tabColor", new ColorControl());
                    break;
                case "barButtonItemUnit":
                    /* Navigate to page E */
                    WMPublic.TabCreating(xtraTabControl1, "Đơn vị", "tabUnit", new UnitControl());
                    break;
                case "barButtonItemTallySheet":
                    /* Navigate to page E */
                    WMPublic.TabCreating(xtraTabControl1, "Phiếu kiểm kê", "tabTallSheet", new TallySheetControl());
                    break;
                case "barButtonItemProcessTally":
                    /* Navigate to page E */
                    WMPublic.TabCreating(xtraTabControl1, "Xử lý kiểm kê", "tabProcessTally", new ProcesTallyControl());
                    break;
                case "barButtonItemBackupRestore":
                    /* Navigate to page E */
                    WMPublic.TabCreating(xtraTabControl1, "Lưu trữ và phục hồi", "tabBackupRestore", new BackupRestoreDatabaseControl());
                    break;
                case "barButtonItemCheckGoodsData":
                    /* Navigate to page E */
                    WMPublic.TabCreating2(xtraTabControl1, "Điền hàng", "tabCheckGoodsData", new CheckGoodsDataControl());
                    break;
                case "barButtonItemGoodsData":
                    /* Navigate to page E */
                    WMPublic.TabCreating2(xtraTabControl1, "Dữ liệu hàng hóa", "tabGoodsData", new GoodsDataControl());
                    break;
                case "barButtonItemZPLPrint":
                    /* Navigate to page E */
                    WMPublic.TabCreating2(xtraTabControl1, "In tem", "tabZPLPrint", new ZPLPrintControl());
                    break;
                case "barButtonItemTestRGBLight":
                    /* Navigate to page E */
                    WMPublic.TabCreating(xtraTabControl1, "Test đèn", "tabTestRGBLight", new TestRGBLight());
                    break;
                case "barButtonItemLocation":
                    /* Navigate to page E */
                    WMPublic.TabCreating(xtraTabControl1, "Location", "tabLocation", new LocationControl());
                    break;
                case "barButtonItemInventoryData":
                    /* Navigate to page E */
                    WMPublic.TabCreating(xtraTabControl1, "Inventory Data", "tabInventoryData", new InventoryDataControl());
                    break;
                case "barButtonItemCountGoods":
                    /* Navigate to page E */
                    WMPublic.TabCreating(xtraTabControl1, "Count Goods", "tabCountGoods", new CountGoodsDataControl());
                    break;
                case "barButtonItemChangePW":
                    /* Navigate to page E */
                    frmChangePW frmChangePW = new frmChangePW();
                    frmChangePW.ShowDialog(this);
                    frmChangePW.Dispose();
                    break;
            }
        }

        private void closeButtonTabpage_Click(object sender, EventArgs e)
        {
            ClosePageButtonEventArgs EArg = (DevExpress.XtraTab.ViewInfo.ClosePageButtonEventArgs)e;
            string name = EArg.Page.Text;//Get the text of the closed tab
            foreach (XtraTabPage page in xtraTabControl1.TabPages)//Traverse to get the same text as the closed tab
            {
                if (page.Text == name)
                {
                    if (WMPublic.sbMessageCloseTab(this) == true)
                    {
                        xtraTabControl1.TabPages.Remove(page);
                        page.Dispose();
                        return;
                    }
                }
            }
        }

        private void barButtonItemVietnam_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (xtraTabControl1.TabPages.Count > 0)
            {
                WMPublic.sbMessageCloseTabpageRequest(this);
            }
            else
            {
                WMMessage.msgLanguage = "vi-VN";
                ResourceManager resourceManager = new ResourceManager("WarehouseManager.Resource.vi_local", Assembly.GetExecutingAssembly());
                updateLanguage();
            }
        }

        private void barButtonItemEnglish_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (xtraTabControl1.TabPages.Count > 0)
            {
                WMPublic.sbMessageCloseTabpageRequest(this);
            }
            else
            {
                WMMessage.msgLanguage = "en-US";
                ResourceManager resourceManager = new ResourceManager("WarehouseManager.Resource.en_local", Assembly.GetExecutingAssembly());
                updateLanguage();
            }
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void barButtonItemUpdate_ItemClick(object sender, ItemClickEventArgs e)
        {
            AutoUpdater.Start(updateURL);
        }
    }
}