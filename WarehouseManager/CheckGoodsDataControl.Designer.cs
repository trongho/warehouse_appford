﻿
namespace WarehouseManager
{
    partial class CheckGoodsDataControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions1 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CheckGoodsDataControl));
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions2 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions3 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions4 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions5 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions6 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions7 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions8 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions9 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition1 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition2 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition3 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition4 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition5 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition6 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition7 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition8 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition9 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition1 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition2 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition3 = new DevExpress.XtraLayout.RowDefinition();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.windowsUIButtonPanel1 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControl3 = new DevExpress.XtraLayout.LayoutControl();
            this.gridControlWRDataDetail = new DevExpress.XtraGrid.GridControl();
            this.wRDataDetailBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.wARHOUSE_HPDataSet1 = new WarehouseManager.WARHOUSE_HPDataSet1();
            this.gridViewWRDataDetail = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCGDDNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrdinal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGoodsID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGoodsName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIDCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocationID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalQuantity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalGoods = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantityOrg = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalQuantityOrg = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalGoodsOrg = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocationIDOrg = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedDateTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEditerID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEditedDateTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPackingVolume = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantityByPack = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantityByItem = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNote = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colScanOption = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPackingQuantity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.textEditTotalGoodsChecked = new DevExpress.XtraEditors.TextEdit();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textEditCGDNumber = new DevExpress.XtraEditors.TextEdit();
            this.dateEditCGDDate = new DevExpress.XtraEditors.DateEdit();
            this.textEditWRRNumber2 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit4 = new DevExpress.XtraEditors.TextEdit();
            this.textEditTotalGoods = new DevExpress.XtraEditors.TextEdit();
            this.textEditNote = new DevExpress.XtraEditors.TextEdit();
            this.comboBoxEditHandlingStatus = new DevExpress.XtraEditors.ComboBoxEdit();
            this.simpleButtonOn = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonOff = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemWRDNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemTotalQuantityOrg = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemNote = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemTotalQuantity = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemWRRReference = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemWRRNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.wRDataDetailTableAdapter = new WarehouseManager.WARHOUSE_HPDataSet1TableAdapters.WRDataDetailTableAdapter();
            this.layoutControlItemWRDDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemWRRNumber2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemHandlingStatus = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).BeginInit();
            this.layoutControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlWRDataDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wRDataDetailBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wARHOUSE_HPDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewWRDataDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalGoodsChecked.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditCGDNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditCGDDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditCGDDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditWRRNumber2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalGoods.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditNote.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditHandlingStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWRDNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalQuantityOrg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemNote)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalQuantity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWRRReference)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWRRNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWRDDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWRRNumber2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemHandlingStatus)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.windowsUIButtonPanel1);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(684, 208, 812, 495);
            this.layoutControl1.Root = this.Root;
            this.layoutControl1.Size = new System.Drawing.Size(1298, 37);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // windowsUIButtonPanel1
            // 
            this.windowsUIButtonPanel1.AllowGlyphSkinning = false;
            this.windowsUIButtonPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            windowsUIButtonImageOptions1.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions1.Image")));
            windowsUIButtonImageOptions1.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions2.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions2.Image")));
            windowsUIButtonImageOptions2.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions3.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions3.Image")));
            windowsUIButtonImageOptions3.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions4.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions4.Image")));
            windowsUIButtonImageOptions4.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions5.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions5.Image")));
            windowsUIButtonImageOptions5.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions6.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions6.Image")));
            windowsUIButtonImageOptions6.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions7.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions7.Image")));
            windowsUIButtonImageOptions7.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions8.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions8.Image")));
            windowsUIButtonImageOptions8.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions9.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions9.Image")));
            windowsUIButtonImageOptions9.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            this.windowsUIButtonPanel1.Buttons.AddRange(new DevExpress.XtraEditors.ButtonPanel.IBaseButton[] {
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F2) Thêm mới", true, windowsUIButtonImageOptions1, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "new", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F3) Lưu", true, windowsUIButtonImageOptions2, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "save", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F4) Xóa", true, windowsUIButtonImageOptions3, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "delete", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F5) Tìm kiếm", true, windowsUIButtonImageOptions4, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "search", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F6) Làm mới", true, windowsUIButtonImageOptions5, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "refesh", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F7) Nhập dữ liệu", true, windowsUIButtonImageOptions6, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "import", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F8) Xuất dữ liệu", true, windowsUIButtonImageOptions7, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "export", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F9) In ấn", true, windowsUIButtonImageOptions8, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "print", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F10) Thoát", true, windowsUIButtonImageOptions9, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "close", -1, false)});
            this.windowsUIButtonPanel1.ContentAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.windowsUIButtonPanel1.Location = new System.Drawing.Point(2, 2);
            this.windowsUIButtonPanel1.Name = "windowsUIButtonPanel1";
            this.windowsUIButtonPanel1.Padding = new System.Windows.Forms.Padding(5);
            this.windowsUIButtonPanel1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.windowsUIButtonPanel1.Size = new System.Drawing.Size(1294, 32);
            this.windowsUIButtonPanel1.TabIndex = 5;
            this.windowsUIButtonPanel1.Text = "windowsUIButtonPanel1";
            this.windowsUIButtonPanel1.UseButtonBackgroundImages = false;
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.Root.Name = "Root";
            this.Root.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.Root.Size = new System.Drawing.Size(1298, 37);
            this.Root.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.windowsUIButtonPanel1;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(1298, 37);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControl3
            // 
            this.layoutControl3.Controls.Add(this.gridControlWRDataDetail);
            this.layoutControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl3.Location = new System.Drawing.Point(0, 139);
            this.layoutControl3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.layoutControl3.Name = "layoutControl3";
            this.layoutControl3.Root = this.layoutControlGroup2;
            this.layoutControl3.Size = new System.Drawing.Size(1298, 487);
            this.layoutControl3.TabIndex = 2;
            this.layoutControl3.Text = "layoutControl3";
            // 
            // gridControlWRDataDetail
            // 
            this.gridControlWRDataDetail.DataSource = this.wRDataDetailBindingSource;
            this.gridControlWRDataDetail.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gridControlWRDataDetail.Location = new System.Drawing.Point(12, 12);
            this.gridControlWRDataDetail.MainView = this.gridViewWRDataDetail;
            this.gridControlWRDataDetail.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gridControlWRDataDetail.Name = "gridControlWRDataDetail";
            this.gridControlWRDataDetail.Size = new System.Drawing.Size(1274, 463);
            this.gridControlWRDataDetail.TabIndex = 4;
            this.gridControlWRDataDetail.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewWRDataDetail});
            // 
            // wRDataDetailBindingSource
            // 
            this.wRDataDetailBindingSource.DataMember = "WRDataDetail";
            this.wRDataDetailBindingSource.DataSource = this.wARHOUSE_HPDataSet1;
            // 
            // wARHOUSE_HPDataSet1
            // 
            this.wARHOUSE_HPDataSet1.DataSetName = "WARHOUSE_HPDataSet1";
            this.wARHOUSE_HPDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridViewWRDataDetail
            // 
            this.gridViewWRDataDetail.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridViewWRDataDetail.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridViewWRDataDetail.Appearance.HeaderPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.gridViewWRDataDetail.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCGDDNumber,
            this.colOrdinal,
            this.colGoodsID,
            this.colGoodsName,
            this.colIDCode,
            this.colLocationID,
            this.colQuantity,
            this.colTotalQuantity,
            this.colTotalGoods,
            this.colQuantityOrg,
            this.colTotalQuantityOrg,
            this.colTotalGoodsOrg,
            this.colLocationIDOrg,
            this.colCreatorID,
            this.colCreatedDateTime,
            this.colEditerID,
            this.colEditedDateTime,
            this.colStatus,
            this.colPackingVolume,
            this.colQuantityByPack,
            this.colQuantityByItem,
            this.colNote,
            this.colScanOption,
            this.colPackingQuantity});
            this.gridViewWRDataDetail.DetailHeight = 284;
            this.gridViewWRDataDetail.GridControl = this.gridControlWRDataDetail;
            this.gridViewWRDataDetail.Name = "gridViewWRDataDetail";
            this.gridViewWRDataDetail.OptionsView.ColumnHeaderAutoHeight = DevExpress.Utils.DefaultBoolean.True;
            // 
            // colCGDDNumber
            // 
            this.colCGDDNumber.Caption = "Số dữ liệu nhập kho";
            this.colCGDDNumber.FieldName = "CGDNumber";
            this.colCGDDNumber.MinWidth = 21;
            this.colCGDDNumber.Name = "colCGDDNumber";
            this.colCGDDNumber.Width = 81;
            // 
            // colOrdinal
            // 
            this.colOrdinal.Caption = "Số thứ tự";
            this.colOrdinal.FieldName = "Ordinal";
            this.colOrdinal.MinWidth = 21;
            this.colOrdinal.Name = "colOrdinal";
            this.colOrdinal.Width = 81;
            // 
            // colGoodsID
            // 
            this.colGoodsID.Caption = "Mã hàng hóa";
            this.colGoodsID.FieldName = "GoodsID";
            this.colGoodsID.MinWidth = 21;
            this.colGoodsID.Name = "colGoodsID";
            this.colGoodsID.Visible = true;
            this.colGoodsID.VisibleIndex = 0;
            this.colGoodsID.Width = 114;
            // 
            // colGoodsName
            // 
            this.colGoodsName.FieldName = "GoodsName";
            this.colGoodsName.MinWidth = 21;
            this.colGoodsName.Name = "colGoodsName";
            this.colGoodsName.Visible = true;
            this.colGoodsName.VisibleIndex = 1;
            this.colGoodsName.Width = 114;
            // 
            // colIDCode
            // 
            this.colIDCode.Caption = "Mã định danh";
            this.colIDCode.FieldName = "IDCode";
            this.colIDCode.MinWidth = 21;
            this.colIDCode.Name = "colIDCode";
            this.colIDCode.Width = 114;
            // 
            // colLocationID
            // 
            this.colLocationID.Caption = "Vị trí ";
            this.colLocationID.FieldName = "LocationID";
            this.colLocationID.MinWidth = 21;
            this.colLocationID.Name = "colLocationID";
            this.colLocationID.Width = 81;
            // 
            // colQuantity
            // 
            this.colQuantity.Caption = "Lượng thực tế( mặt hàng)";
            this.colQuantity.DisplayFormat.FormatString = "G29";
            this.colQuantity.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colQuantity.FieldName = "Quantity";
            this.colQuantity.MinWidth = 21;
            this.colQuantity.Name = "colQuantity";
            this.colQuantity.Width = 114;
            // 
            // colTotalQuantity
            // 
            this.colTotalQuantity.Caption = "Số lượng thực tế( mặt hàng)";
            this.colTotalQuantity.DisplayFormat.FormatString = "G29";
            this.colTotalQuantity.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colTotalQuantity.FieldName = "TotalQuantity";
            this.colTotalQuantity.MinWidth = 21;
            this.colTotalQuantity.Name = "colTotalQuantity";
            this.colTotalQuantity.Width = 114;
            // 
            // colTotalGoods
            // 
            this.colTotalGoods.Caption = "Số lượng thực tế hàng hóa";
            this.colTotalGoods.DisplayFormat.FormatString = "G29";
            this.colTotalGoods.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colTotalGoods.FieldName = "TotalGoods";
            this.colTotalGoods.MinWidth = 21;
            this.colTotalGoods.Name = "colTotalGoods";
            this.colTotalGoods.Width = 81;
            // 
            // colQuantityOrg
            // 
            this.colQuantityOrg.Caption = "Lượng yêu cầu( mặt hàng)";
            this.colQuantityOrg.DisplayFormat.FormatString = "G29";
            this.colQuantityOrg.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colQuantityOrg.FieldName = "QuantityOrg";
            this.colQuantityOrg.MinWidth = 21;
            this.colQuantityOrg.Name = "colQuantityOrg";
            this.colQuantityOrg.Width = 114;
            // 
            // colTotalQuantityOrg
            // 
            this.colTotalQuantityOrg.Caption = "Số lượng yêu cầu( mặt hàng)";
            this.colTotalQuantityOrg.DisplayFormat.FormatString = "G29";
            this.colTotalQuantityOrg.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colTotalQuantityOrg.FieldName = "TotalQuantityOrg";
            this.colTotalQuantityOrg.MinWidth = 21;
            this.colTotalQuantityOrg.Name = "colTotalQuantityOrg";
            this.colTotalQuantityOrg.Width = 81;
            // 
            // colTotalGoodsOrg
            // 
            this.colTotalGoodsOrg.Caption = "Số lượng yêu cầu hàng hóa";
            this.colTotalGoodsOrg.DisplayFormat.FormatString = "G29";
            this.colTotalGoodsOrg.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colTotalGoodsOrg.FieldName = "TotalGoodsOrg";
            this.colTotalGoodsOrg.MinWidth = 21;
            this.colTotalGoodsOrg.Name = "colTotalGoodsOrg";
            this.colTotalGoodsOrg.Width = 81;
            // 
            // colLocationIDOrg
            // 
            this.colLocationIDOrg.Caption = "Vị trí";
            this.colLocationIDOrg.FieldName = "LocationIDOrg";
            this.colLocationIDOrg.MinWidth = 21;
            this.colLocationIDOrg.Name = "colLocationIDOrg";
            this.colLocationIDOrg.Width = 81;
            // 
            // colCreatorID
            // 
            this.colCreatorID.Caption = "Người tạo";
            this.colCreatorID.FieldName = "CreatorID";
            this.colCreatorID.MinWidth = 21;
            this.colCreatorID.Name = "colCreatorID";
            this.colCreatorID.Width = 81;
            // 
            // colCreatedDateTime
            // 
            this.colCreatedDateTime.Caption = "Ngày tạo";
            this.colCreatedDateTime.FieldName = "CreatedDateTime";
            this.colCreatedDateTime.MinWidth = 21;
            this.colCreatedDateTime.Name = "colCreatedDateTime";
            this.colCreatedDateTime.Width = 81;
            // 
            // colEditerID
            // 
            this.colEditerID.Caption = "Người sửa";
            this.colEditerID.FieldName = "EditerID";
            this.colEditerID.MinWidth = 21;
            this.colEditerID.Name = "colEditerID";
            this.colEditerID.Width = 81;
            // 
            // colEditedDateTime
            // 
            this.colEditedDateTime.Caption = "Ngày sửa";
            this.colEditedDateTime.FieldName = "EditedDateTime";
            this.colEditedDateTime.MinWidth = 21;
            this.colEditedDateTime.Name = "colEditedDateTime";
            this.colEditedDateTime.Width = 81;
            // 
            // colStatus
            // 
            this.colStatus.Caption = "Trạng thái";
            this.colStatus.FieldName = "Status";
            this.colStatus.MinWidth = 21;
            this.colStatus.Name = "colStatus";
            this.colStatus.Visible = true;
            this.colStatus.VisibleIndex = 2;
            this.colStatus.Width = 81;
            // 
            // colPackingVolume
            // 
            this.colPackingVolume.Caption = "Quy cách đóng gói";
            this.colPackingVolume.DisplayFormat.FormatString = "G29";
            this.colPackingVolume.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colPackingVolume.FieldName = "PackingVolume";
            this.colPackingVolume.MinWidth = 21;
            this.colPackingVolume.Name = "colPackingVolume";
            this.colPackingVolume.Width = 114;
            // 
            // colQuantityByPack
            // 
            this.colQuantityByPack.Caption = "Lượng yêu cầu( kiện hàng)";
            this.colQuantityByPack.DisplayFormat.FormatString = "G29";
            this.colQuantityByPack.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colQuantityByPack.FieldName = "QuantityByPack";
            this.colQuantityByPack.MinWidth = 21;
            this.colQuantityByPack.Name = "colQuantityByPack";
            this.colQuantityByPack.Width = 114;
            // 
            // colQuantityByItem
            // 
            this.colQuantityByItem.Caption = "Lượng yêu cầu( mặt hàng)";
            this.colQuantityByItem.DisplayFormat.FormatString = "G29";
            this.colQuantityByItem.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colQuantityByItem.FieldName = "QuantityByItem";
            this.colQuantityByItem.MinWidth = 21;
            this.colQuantityByItem.Name = "colQuantityByItem";
            this.colQuantityByItem.Width = 114;
            // 
            // colNote
            // 
            this.colNote.Caption = "Ghi chú";
            this.colNote.FieldName = "Note";
            this.colNote.MinWidth = 21;
            this.colNote.Name = "colNote";
            this.colNote.Width = 38;
            // 
            // colScanOption
            // 
            this.colScanOption.FieldName = "ScanOption";
            this.colScanOption.MinWidth = 21;
            this.colScanOption.Name = "colScanOption";
            this.colScanOption.Width = 81;
            // 
            // colPackingQuantity
            // 
            this.colPackingQuantity.Caption = "Lượng thực tế( Kiện hàng)";
            this.colPackingQuantity.DisplayFormat.FormatString = "G29";
            this.colPackingQuantity.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colPackingQuantity.FieldName = "PackingQuantity";
            this.colPackingQuantity.MinWidth = 21;
            this.colPackingQuantity.Name = "colPackingQuantity";
            this.colPackingQuantity.Width = 193;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem12});
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(1298, 487);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.gridControlWRDataDetail;
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(1278, 467);
            this.layoutControlItem12.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem12.TextVisible = false;
            // 
            // textEditTotalGoodsChecked
            // 
            this.textEditTotalGoodsChecked.AllowDrop = true;
            this.textEditTotalGoodsChecked.Location = new System.Drawing.Point(1077, 39);
            this.textEditTotalGoodsChecked.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textEditTotalGoodsChecked.Name = "textEditTotalGoodsChecked";
            this.textEditTotalGoodsChecked.Size = new System.Drawing.Size(209, 20);
            this.textEditTotalGoodsChecked.StyleController = this.layoutControl2;
            this.textEditTotalGoodsChecked.TabIndex = 16;
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.pictureBox1);
            this.layoutControl2.Controls.Add(this.label1);
            this.layoutControl2.Controls.Add(this.textEditCGDNumber);
            this.layoutControl2.Controls.Add(this.dateEditCGDDate);
            this.layoutControl2.Controls.Add(this.textEditWRRNumber2);
            this.layoutControl2.Controls.Add(this.textEdit4);
            this.layoutControl2.Controls.Add(this.textEditTotalGoods);
            this.layoutControl2.Controls.Add(this.textEditNote);
            this.layoutControl2.Controls.Add(this.textEditTotalGoodsChecked);
            this.layoutControl2.Controls.Add(this.comboBoxEditHandlingStatus);
            this.layoutControl2.Controls.Add(this.simpleButtonOn);
            this.layoutControl2.Controls.Add(this.simpleButtonOff);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl2.Location = new System.Drawing.Point(0, 37);
            this.layoutControl2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(219, 207, 812, 500);
            this.layoutControl2.Root = this.layoutControlGroup1;
            this.layoutControl2.Size = new System.Drawing.Size(1298, 102);
            this.layoutControl2.TabIndex = 1;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(12, 39);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(233, 51);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 22;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(249, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 23);
            this.label1.TabIndex = 7;
            this.label1.Text = "(*)";
            // 
            // textEditCGDNumber
            // 
            this.textEditCGDNumber.Location = new System.Drawing.Point(115, 12);
            this.textEditCGDNumber.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textEditCGDNumber.Name = "textEditCGDNumber";
            this.textEditCGDNumber.Size = new System.Drawing.Size(130, 20);
            this.textEditCGDNumber.StyleController = this.layoutControl2;
            this.textEditCGDNumber.TabIndex = 4;
            // 
            // dateEditCGDDate
            // 
            this.dateEditCGDDate.EditValue = null;
            this.dateEditCGDDate.Location = new System.Drawing.Point(399, 12);
            this.dateEditCGDDate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dateEditCGDDate.Name = "dateEditCGDDate";
            this.dateEditCGDDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditCGDDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditCGDDate.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditCGDDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditCGDDate.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEditCGDDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditCGDDate.Size = new System.Drawing.Size(119, 20);
            this.dateEditCGDDate.StyleController = this.layoutControl2;
            this.dateEditCGDDate.TabIndex = 8;
            // 
            // textEditWRRNumber2
            // 
            this.textEditWRRNumber2.Location = new System.Drawing.Point(625, 12);
            this.textEditWRRNumber2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textEditWRRNumber2.Name = "textEditWRRNumber2";
            this.textEditWRRNumber2.Size = new System.Drawing.Size(119, 20);
            this.textEditWRRNumber2.StyleController = this.layoutControl2;
            this.textEditWRRNumber2.TabIndex = 10;
            // 
            // textEdit4
            // 
            this.textEdit4.Location = new System.Drawing.Point(2540, 10);
            this.textEdit4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textEdit4.Name = "textEdit4";
            this.textEdit4.Size = new System.Drawing.Size(53, 20);
            this.textEdit4.StyleController = this.layoutControl2;
            this.textEdit4.TabIndex = 11;
            // 
            // textEditTotalGoods
            // 
            this.textEditTotalGoods.Location = new System.Drawing.Point(1077, 12);
            this.textEditTotalGoods.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textEditTotalGoods.Name = "textEditTotalGoods";
            this.textEditTotalGoods.Size = new System.Drawing.Size(209, 20);
            this.textEditTotalGoods.StyleController = this.layoutControl2;
            this.textEditTotalGoods.TabIndex = 12;
            // 
            // textEditNote
            // 
            this.textEditNote.Location = new System.Drawing.Point(352, 39);
            this.textEditNote.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textEditNote.Name = "textEditNote";
            this.textEditNote.Properties.AutoHeight = false;
            this.textEditNote.Size = new System.Drawing.Size(618, 51);
            this.textEditNote.StyleController = this.layoutControl2;
            this.textEditNote.TabIndex = 13;
            // 
            // comboBoxEditHandlingStatus
            // 
            this.comboBoxEditHandlingStatus.Location = new System.Drawing.Point(851, 12);
            this.comboBoxEditHandlingStatus.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboBoxEditHandlingStatus.Name = "comboBoxEditHandlingStatus";
            this.comboBoxEditHandlingStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditHandlingStatus.Size = new System.Drawing.Size(119, 20);
            this.comboBoxEditHandlingStatus.StyleController = this.layoutControl2;
            this.comboBoxEditHandlingStatus.TabIndex = 17;
            // 
            // simpleButtonOn
            // 
            this.simpleButtonOn.Location = new System.Drawing.Point(974, 66);
            this.simpleButtonOn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.simpleButtonOn.Name = "simpleButtonOn";
            this.simpleButtonOn.Size = new System.Drawing.Size(75, 22);
            this.simpleButtonOn.StyleController = this.layoutControl2;
            this.simpleButtonOn.TabIndex = 18;
            this.simpleButtonOn.Text = "Mở";
            // 
            // simpleButtonOff
            // 
            this.simpleButtonOff.Location = new System.Drawing.Point(1053, 66);
            this.simpleButtonOff.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.simpleButtonOff.Name = "simpleButtonOff";
            this.simpleButtonOff.Size = new System.Drawing.Size(75, 22);
            this.simpleButtonOff.StyleController = this.layoutControl2;
            this.simpleButtonOff.TabIndex = 19;
            this.simpleButtonOff.Text = "Tắt";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemWRDNumber,
            this.layoutControlItem3,
            this.layoutControlItemTotalQuantityOrg,
            this.layoutControlItemNote,
            this.layoutControlItemTotalQuantity,
            this.layoutControlItem7,
            this.layoutControlItem2,
            this.layoutControlItem4,
            this.layoutControlItemWRDDate,
            this.layoutControlItemWRRNumber2,
            this.layoutControlItemHandlingStatus});
            this.layoutControlGroup1.LayoutMode = DevExpress.XtraLayout.Utils.LayoutMode.Table;
            this.layoutControlGroup1.Name = "Root";
            columnDefinition1.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition1.Width = 19D;
            columnDefinition2.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition2.Width = 3.6952487460281005D;
            columnDefinition3.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition3.Width = 17.714294201569935D;
            columnDefinition4.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition4.Width = 17.714294201569935D;
            columnDefinition5.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition5.Width = 17.714294201569935D;
            columnDefinition6.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition6.Width = 6.1587479100468343D;
            columnDefinition7.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition7.Width = 6.1587479100468343D;
            columnDefinition8.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition8.Width = 6.1587479100468343D;
            columnDefinition9.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition9.Width = 6.1587479100468343D;
            this.layoutControlGroup1.OptionsTableLayoutGroup.ColumnDefinitions.AddRange(new DevExpress.XtraLayout.ColumnDefinition[] {
            columnDefinition1,
            columnDefinition2,
            columnDefinition3,
            columnDefinition4,
            columnDefinition5,
            columnDefinition6,
            columnDefinition7,
            columnDefinition8,
            columnDefinition9});
            rowDefinition1.Height = 33.333333333333336D;
            rowDefinition1.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition2.Height = 33.333333333333336D;
            rowDefinition2.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition3.Height = 33.333333333333336D;
            rowDefinition3.SizeType = System.Windows.Forms.SizeType.Percent;
            this.layoutControlGroup1.OptionsTableLayoutGroup.RowDefinitions.AddRange(new DevExpress.XtraLayout.RowDefinition[] {
            rowDefinition1,
            rowDefinition2,
            rowDefinition3});
            this.layoutControlGroup1.Size = new System.Drawing.Size(1298, 102);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItemWRDNumber
            // 
            this.layoutControlItemWRDNumber.Control = this.textEditCGDNumber;
            this.layoutControlItemWRDNumber.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemWRDNumber.Name = "layoutControlItemWRDNumber";
            this.layoutControlItemWRDNumber.Size = new System.Drawing.Size(237, 27);
            this.layoutControlItemWRDNumber.Text = "Số dữ liệu";
            this.layoutControlItemWRDNumber.TextSize = new System.Drawing.Size(100, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.label1;
            this.layoutControlItem3.Location = new System.Drawing.Point(237, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.OptionsTableLayoutItem.ColumnIndex = 1;
            this.layoutControlItem3.Size = new System.Drawing.Size(47, 27);
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItemTotalQuantityOrg
            // 
            this.layoutControlItemTotalQuantityOrg.Control = this.textEditTotalGoods;
            this.layoutControlItemTotalQuantityOrg.CustomizationFormText = "Σ Mã Hàng";
            this.layoutControlItemTotalQuantityOrg.Location = new System.Drawing.Point(962, 0);
            this.layoutControlItemTotalQuantityOrg.Name = "layoutControlItemTotalQuantityOrg";
            this.layoutControlItemTotalQuantityOrg.OptionsTableLayoutItem.ColumnIndex = 5;
            this.layoutControlItemTotalQuantityOrg.OptionsTableLayoutItem.ColumnSpan = 4;
            this.layoutControlItemTotalQuantityOrg.Size = new System.Drawing.Size(316, 27);
            this.layoutControlItemTotalQuantityOrg.Text = "Σ Mã Hàng";
            this.layoutControlItemTotalQuantityOrg.TextSize = new System.Drawing.Size(100, 13);
            // 
            // layoutControlItemNote
            // 
            this.layoutControlItemNote.Control = this.textEditNote;
            this.layoutControlItemNote.Location = new System.Drawing.Point(237, 27);
            this.layoutControlItemNote.Name = "layoutControlItemNote";
            this.layoutControlItemNote.OptionsTableLayoutItem.ColumnIndex = 1;
            this.layoutControlItemNote.OptionsTableLayoutItem.ColumnSpan = 4;
            this.layoutControlItemNote.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItemNote.OptionsTableLayoutItem.RowSpan = 2;
            this.layoutControlItemNote.Size = new System.Drawing.Size(725, 55);
            this.layoutControlItemNote.Text = "Ghi chú";
            this.layoutControlItemNote.TextSize = new System.Drawing.Size(100, 13);
            // 
            // layoutControlItemTotalQuantity
            // 
            this.layoutControlItemTotalQuantity.Control = this.textEditTotalGoodsChecked;
            this.layoutControlItemTotalQuantity.CustomizationFormText = "Σ Mã Hàng Đã Check";
            this.layoutControlItemTotalQuantity.Location = new System.Drawing.Point(962, 27);
            this.layoutControlItemTotalQuantity.Name = "layoutControlItemTotalQuantity";
            this.layoutControlItemTotalQuantity.OptionsTableLayoutItem.ColumnIndex = 5;
            this.layoutControlItemTotalQuantity.OptionsTableLayoutItem.ColumnSpan = 4;
            this.layoutControlItemTotalQuantity.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItemTotalQuantity.Size = new System.Drawing.Size(316, 27);
            this.layoutControlItemTotalQuantity.Text = "Σ Mã Hàng Đã Check";
            this.layoutControlItemTotalQuantity.TextSize = new System.Drawing.Size(100, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.pictureBox1;
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 27);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItem7.OptionsTableLayoutItem.RowSpan = 2;
            this.layoutControlItem7.Size = new System.Drawing.Size(237, 55);
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.simpleButtonOn;
            this.layoutControlItem2.Location = new System.Drawing.Point(962, 54);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.OptionsTableLayoutItem.ColumnIndex = 5;
            this.layoutControlItem2.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlItem2.Size = new System.Drawing.Size(79, 28);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.simpleButtonOff;
            this.layoutControlItem4.Location = new System.Drawing.Point(1041, 54);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.OptionsTableLayoutItem.ColumnIndex = 6;
            this.layoutControlItem4.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlItem4.Size = new System.Drawing.Size(79, 28);
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItemWRRReference
            // 
            this.layoutControlItemWRRReference.Control = this.textEditWRRNumber2;
            this.layoutControlItemWRRReference.Location = new System.Drawing.Point(904, 0);
            this.layoutControlItemWRRReference.Name = "layoutControlItem6";
            this.layoutControlItemWRRReference.OptionsTableLayoutItem.ColumnIndex = 4;
            this.layoutControlItemWRRReference.Size = new System.Drawing.Size(290, 33);
            this.layoutControlItemWRRReference.Text = "Số phiếu yêu cầu";
            this.layoutControlItemWRRReference.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlItemWRRNumber
            // 
            this.layoutControlItemWRRNumber.Control = this.textEditWRRNumber2;
            this.layoutControlItemWRRNumber.Location = new System.Drawing.Point(904, 0);
            this.layoutControlItemWRRNumber.Name = "layoutControlItemWRRReference";
            this.layoutControlItemWRRNumber.OptionsTableLayoutItem.ColumnIndex = 4;
            this.layoutControlItemWRRNumber.Size = new System.Drawing.Size(290, 33);
            this.layoutControlItemWRRNumber.Text = "Số phiếu yêu cầu";
            this.layoutControlItemWRRNumber.TextSize = new System.Drawing.Size(50, 20);
            // 
            // wRDataDetailTableAdapter
            // 
            this.wRDataDetailTableAdapter.ClearBeforeFill = true;
            // 
            // layoutControlItemWRDDate
            // 
            this.layoutControlItemWRDDate.Control = this.dateEditCGDDate;
            this.layoutControlItemWRDDate.Location = new System.Drawing.Point(284, 0);
            this.layoutControlItemWRDDate.Name = "layoutControlItemWRDDate";
            this.layoutControlItemWRDDate.OptionsTableLayoutItem.ColumnIndex = 2;
            this.layoutControlItemWRDDate.Size = new System.Drawing.Size(226, 27);
            this.layoutControlItemWRDDate.Text = "Ngày dữ liệu";
            this.layoutControlItemWRDDate.TextSize = new System.Drawing.Size(100, 13);
            // 
            // layoutControlItemWRRNumber2
            // 
            this.layoutControlItemWRRNumber2.Control = this.textEditWRRNumber2;
            this.layoutControlItemWRRNumber2.Location = new System.Drawing.Point(510, 0);
            this.layoutControlItemWRRNumber2.Name = "layoutControlItemWRRNumber2";
            this.layoutControlItemWRRNumber2.OptionsTableLayoutItem.ColumnIndex = 3;
            this.layoutControlItemWRRNumber2.Size = new System.Drawing.Size(226, 27);
            this.layoutControlItemWRRNumber2.Text = "Số phiếu yêu cầu";
            this.layoutControlItemWRRNumber2.TextSize = new System.Drawing.Size(100, 13);
            // 
            // layoutControlItemHandlingStatus
            // 
            this.layoutControlItemHandlingStatus.Control = this.comboBoxEditHandlingStatus;
            this.layoutControlItemHandlingStatus.Location = new System.Drawing.Point(736, 0);
            this.layoutControlItemHandlingStatus.Name = "layoutControlItemHandlingStatus";
            this.layoutControlItemHandlingStatus.OptionsTableLayoutItem.ColumnIndex = 4;
            this.layoutControlItemHandlingStatus.Size = new System.Drawing.Size(226, 27);
            this.layoutControlItemHandlingStatus.Text = "HandlingStatus";
            this.layoutControlItemHandlingStatus.TextSize = new System.Drawing.Size(100, 13);
            // 
            // CheckGoodsDataControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl3);
            this.Controls.Add(this.layoutControl2);
            this.Controls.Add(this.layoutControl1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "CheckGoodsDataControl";
            this.Size = new System.Drawing.Size(1298, 626);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).EndInit();
            this.layoutControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlWRDataDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wRDataDetailBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wARHOUSE_HPDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewWRDataDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalGoodsChecked.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditCGDNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditCGDDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditCGDDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditWRRNumber2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalGoods.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditNote.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditHandlingStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWRDNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalQuantityOrg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemNote)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalQuantity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWRRReference)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWRRNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWRDDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWRRNumber2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemHandlingStatus)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraLayout.LayoutControl layoutControl3;
        private DevExpress.XtraGrid.GridControl gridControlWRDataDetail;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewWRDataDetail;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraEditors.TextEdit textEditTotalGoodsChecked;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.TextEdit textEditCGDNumber;
        private DevExpress.XtraEditors.DateEdit dateEditCGDDate;
        private DevExpress.XtraEditors.TextEdit textEditWRRNumber2;
        private DevExpress.XtraEditors.TextEdit textEdit4;
        private DevExpress.XtraEditors.TextEdit textEditTotalGoods;
        private DevExpress.XtraEditors.TextEdit textEditNote;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditHandlingStatus;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemWRDNumber;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemTotalQuantityOrg;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemNote;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemTotalQuantity;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemWRRReference;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemWRRNumber;
        private System.Windows.Forms.BindingSource wRDataDetailBindingSource;
        private WARHOUSE_HPDataSet1 wARHOUSE_HPDataSet1;
        private DevExpress.XtraGrid.Columns.GridColumn colCGDDNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colOrdinal;
        private DevExpress.XtraGrid.Columns.GridColumn colGoodsID;
        private DevExpress.XtraGrid.Columns.GridColumn colGoodsName;
        private DevExpress.XtraGrid.Columns.GridColumn colIDCode;
        private DevExpress.XtraGrid.Columns.GridColumn colLocationID;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantity;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalQuantity;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalGoods;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantityOrg;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalQuantityOrg;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalGoodsOrg;
        private DevExpress.XtraGrid.Columns.GridColumn colLocationIDOrg;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatorID;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedDateTime;
        private DevExpress.XtraGrid.Columns.GridColumn colEditerID;
        private DevExpress.XtraGrid.Columns.GridColumn colEditedDateTime;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colPackingVolume;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantityByPack;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantityByItem;
        private DevExpress.XtraGrid.Columns.GridColumn colNote;
        private DevExpress.XtraGrid.Columns.GridColumn colScanOption;
        private WARHOUSE_HPDataSet1TableAdapters.WRDataDetailTableAdapter wRDataDetailTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colPackingQuantity;
        private DevExpress.XtraEditors.SimpleButton simpleButtonOn;
        private DevExpress.XtraEditors.SimpleButton simpleButtonOff;
        private DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel windowsUIButtonPanel1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemWRDDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemWRRNumber2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemHandlingStatus;
    }
}
