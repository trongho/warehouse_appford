﻿using DevExpress.Utils;
using DevExpress.XtraBars.Docking2010;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using OfficeOpenXml.FormulaParsing.Excel.Operators;
using QRCoder;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Media;
using WarehouseManager.Models;
using WarehouseManager.Repository;
using webservice.Entities;

namespace WarehouseManager
{
    public partial class IssueDataControl : DevExpress.XtraEditors.XtraUserControl
    {
        WIDataHeaderRepository wIDataHeaderRepository;
        WIDataDetailRepository wIDataDetailRepository;
        WIDataGeneralRepository wIDataGeneralRepository;
        WIRHeaderRepository wIRHeaderRepository;
        WIRDetailRepository wIRDetailRepository;
        HandlingStatusRepository handlingStatusRepository;
        IOHeaderRepository iOHeaderRepository;
        IODetailRepository iODetailRepository;
        public static String selectedWIDNumber;
        Timer t = new Timer();
        Boolean isGeneralClick = false;
        Boolean isFilterClick = false;
        List<WIDataGeneral> listPublic;
        List<WIDataGeneral> listPublic2;
        String selectedGoodsGroupID = "All";
        String selectedPickerName = "All";
        String selectedScanOption = "All";
        public IssueDataControl()
        {
            InitializeComponent();
            this.Load += IssueDataControl_Load;
            wIDataHeaderRepository = new WIDataHeaderRepository();
            wIDataDetailRepository = new WIDataDetailRepository();
            wIRHeaderRepository = new WIRHeaderRepository();
            wIRDetailRepository = new WIRDetailRepository();
            wIDataGeneralRepository = new WIDataGeneralRepository();
            iOHeaderRepository = new IOHeaderRepository();
            iODetailRepository = new IODetailRepository();
            handlingStatusRepository = new HandlingStatusRepository();
            listPublic = new List<WIDataGeneral>();
            popupMenuSelectHandlingStatus();
            popupMenuSelectGoodsGroupID();
            popupMenuSelectPickerName();
            popupMenuSelectScanOption();
        }

        private void IssueDataControl_Load(Object sender, EventArgs e)
        {
            gridViewWIDataDetail.ClearSorting();
            windowsUIButtonPanel1.ButtonClick += button_Click;
            textEditWIDNumber.KeyDown += new KeyEventHandler(WIDNumber_KeyDown);
            textEditWIDNumber.DoubleClick += textEditWDRNumer_CellDoubleClick;
            gridViewWIDataDetail.CustomDrawCell += grdData_CustomDrawCell;
            gridViewWIDataDetail.CustomColumnDisplayText += GridView_CustomColumnDisplayText;
            simpleButtonOn.Click += butOn_Click;
            simpleButtonOff.Click += butOff_Click;
            sbLoadDataForGridWIDataGeneralAsync(textEditWIDNumber.Text);
            simpleButtonFilter.Click += simpleButtonFilter_ClickAsync;
            t.Tick += new EventHandler(this.Timer_Tick);
            t.Start();
        }

        private void GridView_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {

            if (e.Column == colNo)
            {
                int rowHandle = gridViewWIDataDetail.GetRowHandle(e.ListSourceRowIndex);
                if (rowHandle >= 0)
                {
                    e.DisplayText = (rowHandle + 1).ToString();
                }
            }
        }

        private async Task popupMenuSelectHandlingStatus()
        {
            List<HandlingStatus> handlingStatuses = await handlingStatusRepository.GetAll();
            foreach (HandlingStatus handlingStatus in handlingStatuses)
            {
                comboBoxEditHandlingStatus.Properties.Items.Add(handlingStatus.HandlingStatusName);
            }
            comboBoxEditHandlingStatus.SelectedIndex = 1;
        }

        private void popupMenuSelectGoodsGroupID()
        {
            comboBoxEditGoodsGroupID.Properties.Items.Clear();
            Dictionary<string, string> MyDic4 = new Dictionary<string, string>();
            MyDic4.Add("All", "AllValue");
            for (int i = 0; i < listPublic.Count; i++)
            {
                string goodsGroupsID = listPublic[i].GoodsGroupID;
                if (!MyDic4.ContainsKey(listPublic[i].GoodsGroupID))
                {
                    MyDic4.Add(goodsGroupsID, goodsGroupsID + "Value");
                }
            }
            foreach (KeyValuePair<string, string> item in MyDic4)
            {
                comboBoxEditGoodsGroupID.Properties.Items.Add(item.Key);
            }
            comboBoxEditGoodsGroupID.SelectedIndex = 0;
        }

        private void popupMenuSelectPickerName()
        {
            comboBoxEditPickerName.Properties.Items.Clear();
            Dictionary<string, string> MyDic4 = new Dictionary<string, string>();
            MyDic4.Add("All", "AllValue");
            for (int i = 0; i < listPublic.Count; i++)
            {
                string pickerName = listPublic[i].PickerName;
                if (!MyDic4.ContainsKey(listPublic[i].PickerName))
                {
                    MyDic4.Add(pickerName, pickerName + "Value");
                }
            }
            foreach (KeyValuePair<string, string> item in MyDic4)
            {
                comboBoxEditPickerName.Properties.Items.Add(item.Key);
            }
            comboBoxEditPickerName.SelectedIndex = 0;
        }

        private void popupMenuSelectScanOption()
        {
            comboBoxEditScanOption.Properties.Items.Add("All");
            comboBoxEditScanOption.Properties.Items.Add("0");
            comboBoxEditScanOption.Properties.Items.Add("1");
            comboBoxEditScanOption.SelectedIndex = 0;
        }


        private async Task sbLoadDataForGridWIDataGeneralAsync(String wIDNumber)
        {
            List<WIDataGeneral> wIDataGenerals = await wIDataGeneralRepository.GetUnderID(wIDNumber);
            int mMaxRow = 100;
            if (wIDataGenerals.Count < mMaxRow)
            {
                int num = mMaxRow - wIDataGenerals.Count;
                int i = 1;
                while (true)
                {
                    int num2 = i;
                    int num3 = num;
                    if (num2 > num3)
                    {
                        break;
                    }
                    wIDataGenerals.Add(new WIDataGeneral());
                    i++;
                }
            }
            gridControlWIDataDetail.DataSource = wIDataGenerals;
            getTotalQuantityOrg();
            getTotalQuantity();
            //getPackingQuantity();
            textTotalQuantity_CustomDraw();
        }

        private async Task sbLoadDataForGridWIDataGeneralAsync2(String wIDNumber)
        {
            List<WIDataGeneral> wIDataGenerals = await wIDataGeneralRepository.Filter(textEditWIDNumber.Text, selectedGoodsGroupID, selectedPickerName, selectedScanOption);
            int mMaxRow = 100;
            if (wIDataGenerals.Count < mMaxRow)
            {
                int num = mMaxRow - wIDataGenerals.Count;
                int i = 1;
                while (true)
                {
                    int num2 = i;
                    int num3 = num;
                    if (num2 > num3)
                    {
                        break;
                    }
                    wIDataGenerals.Add(new WIDataGeneral());
                    i++;
                }
            }
            gridControlWIDataDetail.DataSource = wIDataGenerals;
            getTotalQuantityOrg();
            getTotalQuantity();
            //getPackingQuantity();
            textTotalQuantity_CustomDraw();
        }

        private async Task sbLoadDataForGridWIDataGeneralByGoodsGroupID(String wIDNumber, String goodsGroupID)
        {
            List<WIDataGeneral> wIDataGenerals = await wIDataGeneralRepository.GetUnderGoodsGroupID(wIDNumber, goodsGroupID);
            int mMaxRow = 100;
            if (wIDataGenerals.Count < mMaxRow)
            {
                int num = mMaxRow - wIDataGenerals.Count;
                int i = 1;
                while (true)
                {
                    int num2 = i;
                    int num3 = num;
                    if (num2 > num3)
                    {
                        break;
                    }
                    wIDataGenerals.Add(new WIDataGeneral());
                    i++;
                }
            }
            gridControlWIDataDetail.DataSource = wIDataGenerals;
            getTotalQuantityOrg();
            getTotalQuantity();
            //getPackingQuantity();
            textTotalQuantity_CustomDraw();
        }

        private async Task sbLoadDataForGridWIDataGeneralByPickerName(String wIDNumber, String pickerName)
        {
            List<WIDataGeneral> wIDataGenerals = await wIDataGeneralRepository.GetUnderPickerName(wIDNumber, pickerName);
            int mMaxRow = 100;
            if (wIDataGenerals.Count < mMaxRow)
            {
                int num = mMaxRow - wIDataGenerals.Count;
                int i = 1;
                while (true)
                {
                    int num2 = i;
                    int num3 = num;
                    if (num2 > num3)
                    {
                        break;
                    }
                    wIDataGenerals.Add(new WIDataGeneral());
                    i++;
                }
            }
            gridControlWIDataDetail.DataSource = wIDataGenerals;
            getTotalQuantityOrg();
            getTotalQuantity();
            //getPackingQuantity();
            textTotalQuantity_CustomDraw();
        }

        //private void getPackingQuantity()
        //{
        //    Decimal packingQuantity = 0;
        //    int i = 0;
        //    while (true)
        //    {
        //        int rowHandle = gridViewWIDataDetail.GetRowHandle(i);
        //        if (gridViewWIDataDetail.GetRowCellValue(rowHandle, "GoodsID") == null)
        //        {
        //            break;
        //        }
        //        Decimal quantityOrg = (Decimal)gridViewWIDataDetail.GetRowCellValue(rowHandle, "QuantityOrg");
        //        Decimal quantity = (Decimal)gridViewWIDataDetail.GetRowCellValue(rowHandle, "Quantity");
        //        packingQuantity = quantity / quantityOrg;
        //        if (packingQuantity >= 1)
        //        {
        //            packingQuantity = Math.Ceiling(packingQuantity);
        //        }
        //        else
        //        {
        //            packingQuantity = Math.Floor(packingQuantity);

        //        }
        //        gridViewWIDataDetail.SetRowCellValue(rowHandle, "PackingQuantity", packingQuantity);
        //        i++;
        //    }
        //}


        private void getTotalQuantityOrg()
        {
            Decimal totalQuantityOrg = 0;
            int i = 0;
            while (true)
            {
                int rowHandle = gridViewWIDataDetail.GetRowHandle(i);
                if (gridViewWIDataDetail.GetRowCellValue(rowHandle, "GoodsID") == null)
                {
                    break;
                }
                Decimal quantityOrg = (Decimal)gridViewWIDataDetail.GetRowCellValue(rowHandle, "QuantityOrg");
                totalQuantityOrg += quantityOrg;
                i++;
            }
            textEditTotalQuantityOrg.Text = totalQuantityOrg.ToString("0.#####");
        }

        private void getTotalQuantity()
        {
            Decimal totalQuantity = 0;
            int i = 0;
            while (true)
            {
                int rowHandle = gridViewWIDataDetail.GetRowHandle(i);
                if (gridViewWIDataDetail.GetRowCellValue(rowHandle, "GoodsID") == null)
                {
                    break;
                }
                Decimal quantityOrg = (Decimal)gridViewWIDataDetail.GetRowCellValue(rowHandle, "QuantityOrg");
                Decimal quantity = (Decimal)gridViewWIDataDetail.GetRowCellValue(rowHandle, "Quantity");
                totalQuantity += quantityOrg - quantity;
                i++;
            }
            textEditTotalQuantity.Text = totalQuantity.ToString("0.#####");
        }

        private void QRCodeGeneral(String code)
        {
            QRCodeGenerator.ECCLevel eccLevel = QRCodeGenerator.ECCLevel.Q;
            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeData qrCodeData = qrGenerator.CreateQrCode(code, eccLevel);
            QRCode qrCode = new QRCode(qrCodeData);
            Bitmap qrCodeBitmap = qrCode.GetGraphic(2, System.Drawing.Color.Black, System.Drawing.Color.White, new Bitmap(10, 10));
            pictureBox1.Image = qrCodeBitmap;
            Helpers.ImageHelper.resizeImage(pictureBox1.Image, new Size(100, 100));
            pictureBox1.SizeMode = PictureBoxSizeMode.CenterImage;
        }

        public async Task updateWIDataHeaderAsync()
        {
            WIDataHeader wIDataHeader = await wIDataHeaderRepository.GetUnderID(textEditWIDNumber.Text);
            if (wIDataHeader.HandlingStatusID.Equals("2"))
            {
                MessageBox.Show("Dữ liệu nhập đã duyệt mức 2, không thể chỉnh sửa");
                return;
            }
            Decimal totalQuantityOrg = Decimal.Parse(textEditTotalQuantityOrg.Text);
            Decimal totalQuantity = Decimal.Parse(textEditTotalQuantity.Text);
            Decimal subQuantity = totalQuantityOrg - totalQuantity;
            if (subQuantity == totalQuantityOrg)
            {
                wIDataHeader.Status = "0";
            }
            else if (subQuantity < totalQuantityOrg)
            {
                wIDataHeader.Status = "1";
            }
            else if (subQuantity == 0)
            {
                wIDataHeader.Status = "2";
            }
            else
            {
                wIDataHeader.Status = "3";
            }

            wIDataHeader.WIDNumber = textEditWIDNumber.Text;
            wIDataHeader.WIDDate = DateTime.Parse(dateEditWIDDate.DateTime.ToString("yyyy-MM-dd"));
            wIDataHeader.ReferenceNumber = textEditWIDNumber.Text;
            wIDataHeader.WIRNumber = textEditWIDNumber.Text;
            wIDataHeader.WIRReference = textEditWIDNumber.Text;
            wIDataHeader.HandlingStatusID = (comboBoxEditHandlingStatus.SelectedIndex - 1).ToString();
            wIDataHeader.HandlingStatusName = comboBoxEditHandlingStatus.SelectedItem.ToString();
            wIDataHeader.Note = "";
            wIDataHeader.TotalQuantity = Decimal.Parse(textEditTotalQuantity.Text);
            wIDataHeader.TotalQuantityOrg = Decimal.Parse(textEditTotalQuantityOrg.Text);
            wIDataHeader.CreatedUserID = wIDataHeader.CreatedUserID;
            wIDataHeader.CreatedDate = wIDataHeader.CreatedDate;
            wIDataHeader.UpdatedUserID = WMMessage.User.UserID;
            wIDataHeader.UpdatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));
            if (await wIDataHeaderRepository.Update(wIDataHeader, textEditWIDNumber.Text) > 0)
            {
                WMPublic.sbMessageSaveChangeSuccess(this);
                if (comboBoxEditHandlingStatus.SelectedIndex == 3)
                {

                    createIOHeaderAsync();

                }
            }
        }

        public async Task updateWIDataDetailAsync()
        {
            int num = gridViewWIDataDetail.RowCount - 1;
            int i = 0;
            while (true)
            {
                int num2 = i;
                int num3 = num;
                if (num2 > num3 || gridViewWIDataDetail.GetRowCellValue(i, gridViewWIDataDetail.Columns["colGoodsID"]) == "")
                {
                    break;
                }
                int rowHandle = gridViewWIDataDetail.GetRowHandle(i);
                WIDataDetail wIDataDetail = await wIDataDetailRepository.GetUnderMultiID(textEditWIDNumber.Text, (string?)gridViewWIDataDetail.GetRowCellValue(rowHandle, "GoodsID"), i + 1);
                wIDataDetail.WIDNumber = textEditWIDNumber.Text;
                wIDataDetail.GoodsID = (string?)gridViewWIDataDetail.GetRowCellValue(rowHandle, "GoodsID");
                wIDataDetail.GoodsName = (string?)gridViewWIDataDetail.GetRowCellValue(rowHandle, "GoodsName");
                wIDataDetail.Ordinal = wIDataDetail.Ordinal;
                wIDataDetail.IDCode = wIDataDetail.IDCode;
                wIDataDetail.LocationID = (string?)gridViewWIDataDetail.GetRowCellValue(rowHandle, "LocationID");
                wIDataDetail.Quantity = (Decimal?)gridViewWIDataDetail.GetRowCellValue(rowHandle, "Quantity");
                wIDataDetail.TotalQuantity = (Decimal?)gridViewWIDataDetail.GetRowCellValue(rowHandle, "TotalQuantity");
                wIDataDetail.TotalGoods = (Decimal?)gridViewWIDataDetail.GetRowCellValue(rowHandle, "TotalGoods");
                wIDataDetail.QuantityOrg = (Decimal?)gridViewWIDataDetail.GetRowCellValue(rowHandle, "QuantityOrg");
                wIDataDetail.TotalQuantityOrg = (Decimal?)gridViewWIDataDetail.GetRowCellValue(rowHandle, "TotalQuantityOrg");
                wIDataDetail.TotalGoodsOrg = (Decimal?)gridViewWIDataDetail.GetRowCellValue(rowHandle, "TotalGoodsOrg");
                wIDataDetail.LocationIDOrg = (string?)gridViewWIDataDetail.GetRowCellValue(rowHandle, "LocationIDOrg");
                wIDataDetail.EditerID = WMMessage.User.UserID; ;
                wIDataDetail.EditedDateTime = DateTime.Now.ToString("yyyy-MM-dd");
                wIDataDetail.Status = "1";
                wIDataDetail.PackingVolume = (Decimal?)gridViewWIDataDetail.GetRowCellValue(rowHandle, "PackingVolume");
                wIDataDetail.QuantityByPack = (Decimal?)gridViewWIDataDetail.GetRowCellValue(rowHandle, "QuantityByPack");
                wIDataDetail.QuantityByItem = (Decimal?)gridViewWIDataDetail.GetRowCellValue(rowHandle, "QuantityByItem");
                wIDataDetail.Note = "";
                wIDataDetail.ScanOption = (Int16?)gridViewWIDataDetail.GetRowCellValue(rowHandle, "ScanOption");
                wIDataDetail.PackingQuantity = (Decimal?)gridViewWIDataDetail.GetRowCellValue(rowHandle, "PackingQuantity");


                if (await wIDataDetailRepository.Update(wIDataDetail, textEditWIDNumber.Text, wIDataDetail.GoodsID, wIDataDetail.Ordinal) > 0)
                {
                    i++;
                }
            }
        }

        public async Task updateWIDataGeneralAsync()
        {
            int num = gridViewWIDataDetail.RowCount - 1;
            int i = 0;
            while (true)
            {
                int num2 = i;
                int num3 = num;
                if (num2 > num3 || gridViewWIDataDetail.GetRowCellValue(i, gridViewWIDataDetail.Columns["colGoodsID"]) == "")
                {
                    break;
                }
                int rowHandle = gridViewWIDataDetail.GetRowHandle(i);
                WIDataGeneral wIDataGeneral = new WIDataGeneral();
                wIDataGeneral.WIDNumber = textEditWIDNumber.Text;
                wIDataGeneral.GoodsID = (string?)gridViewWIDataDetail.GetRowCellValue(rowHandle, "GoodsID");
                wIDataGeneral.GoodsName = (string?)gridViewWIDataDetail.GetRowCellValue(rowHandle, "GoodsName");
                wIDataGeneral.Ordinal = i + 1;
                wIDataGeneral.IDCode = "";
                wIDataGeneral.LocationID = (string?)gridViewWIDataDetail.GetRowCellValue(rowHandle, "LocationID");
                wIDataGeneral.Quantity = (Decimal?)gridViewWIDataDetail.GetRowCellValue(rowHandle, "Quantity");
                wIDataGeneral.TotalQuantity = (Decimal?)gridViewWIDataDetail.GetRowCellValue(rowHandle, "TotalQuantity");
                wIDataGeneral.TotalGoods = (Decimal?)gridViewWIDataDetail.GetRowCellValue(rowHandle, "TotalGoods");
                wIDataGeneral.QuantityOrg = (Decimal?)gridViewWIDataDetail.GetRowCellValue(rowHandle, "QuantityOrg");
                wIDataGeneral.TotalQuantityOrg = (Decimal?)gridViewWIDataDetail.GetRowCellValue(rowHandle, "TotalQuantityOrg");
                wIDataGeneral.TotalGoodsOrg = (Decimal?)gridViewWIDataDetail.GetRowCellValue(rowHandle, "TotalGoodsOrg");
                wIDataGeneral.LocationIDOrg = (string?)gridViewWIDataDetail.GetRowCellValue(rowHandle, "LocationIDOrg");
                wIDataGeneral.EditerID = WMMessage.User.UserID; ;
                wIDataGeneral.EditedDateTime = DateTime.Now.ToString("yyyy-MM-dd");
                wIDataGeneral.Status = "1";
                wIDataGeneral.PackingVolume = (Decimal?)gridViewWIDataDetail.GetRowCellValue(rowHandle, "PackingVolume");
                wIDataGeneral.QuantityByPack = (Decimal?)gridViewWIDataDetail.GetRowCellValue(rowHandle, "QuantityByPack");
                wIDataGeneral.QuantityByItem = (Decimal?)gridViewWIDataDetail.GetRowCellValue(rowHandle, "QuantityByItem");
                wIDataGeneral.Note = "";
                wIDataGeneral.ScanOption = (Int16?)gridViewWIDataDetail.GetRowCellValue(rowHandle, "ScanOption");
                wIDataGeneral.PackingQuantity = (Decimal?)gridViewWIDataDetail.GetRowCellValue(rowHandle, "PackingQuantity");


                if (await wIDataGeneralRepository.Update(wIDataGeneral, textEditWIDNumber.Text, wIDataGeneral.GoodsID, wIDataGeneral.Ordinal, wIDataGeneral.GoodsGroupID) > 0)
                {
                    i++;
                }
            }
        }

        public async Task DeleteWIDataHeaderAsync()
        {
            if ((await wIDataHeaderRepository.Delete(textEditWIDNumber.Text)) > 0)
            {
                await DeleteWIDataGeneralAsync();
                sbLoadDataForGridWIDataGeneralAsync(textEditWIDNumber.Text);
                WMPublic.sbMessageDeleteSuccess(this);
            }
        }

        //public async Task DeleteWIDataDetailAsync()
        //{
        //    await wIDataDetailRepository.Delete(textEditWIDNumber.Text);
        //    gridControlWIDataDetail.DataSource = null;
        //}

        public async Task DeleteWIDataGeneralAsync()
        {
            await wIDataGeneralRepository.Delete(textEditWIDNumber.Text);
            clearAsync();
            gridControlWIDataDetail.DataSource = null;
        }

        private void clearAsync()
        {
            textEditWIDNumber.Text = "";
            dateEditWIDDate.Text = DateTime.Now.ToString("dd-MM-yyyy");
            comboBoxEditHandlingStatus.SelectedIndex = 1;
            textEditTotalQuantityOrg.Text = "0";
            textEditTotalQuantity.Text = "0";
            List<WIDataDetail> wIDataDetails = new List<WIDataDetail>();
            List<WIDataGeneral> wIDataGenerals = new List<WIDataGeneral>();

            if (isGeneralClick == false)
            {
                for (int i = 0; i < 100; i++)
                {
                    wIDataDetails.Add(new WIDataDetail());
                }
                gridControlWIDataDetail.DataSource = wIDataDetails;
            }
            else
            {
                for (int i = 0; i < 100; i++)
                {
                    wIDataGenerals.Add(new WIDataGeneral());
                }
                gridControlWIDataDetail.DataSource = wIDataGenerals;
            }
        }

        public async Task createIOHeaderAsync()
        {
            WIDataHeader wIDataHeader = await wIDataHeaderRepository.GetUnderID(textEditWIDNumber.Text);
            IOHeader iOHeader = new IOHeader();
            iOHeader.IONumber = textEditWIDNumber.Text;
            iOHeader.IODate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));
            iOHeader.ReferenceNumber = textEditWIDNumber.Text;
            iOHeader.WIRNumber = textEditWIDNumber.Text;
            iOHeader.WIRReference = textEditWIDNumber.Text;
            iOHeader.BranchID = wIDataHeader.BranchID;
            iOHeader.BranchName = wIDataHeader.BranchName;
            iOHeader.TotalQuantity = Decimal.Parse(textEditTotalQuantity.Text);
            iOHeader.HandlingStatusID = "0";
            iOHeader.HandlingStatusName = "Chưa duyệt";
            iOHeader.ModalityID = "09";
            iOHeader.ModalityName = "Khác";
            iOHeader.CreatedUserID = WMMessage.User.UserID;
            iOHeader.CreatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));
            iOHeader.ToWarehouseID = "<>";

            if (await iOHeaderRepository.Create(iOHeader) > 0)
            {
                createIODetailAsync();
                MessageBox.Show("Đã tạo phiếu xuất");
            }
        }

        public async Task createIODetailAsync()
        {
            int num = gridViewWIDataDetail.RowCount - 1;
            int i = 0;
            while (true)
            {
                int num2 = i;
                int num3 = num;
                if (num2 > num3 || gridViewWIDataDetail.GetRowCellValue(i, gridViewWIDataDetail.Columns["colGoodsID"]) == "")
                {
                    break;
                }
                int rowHandle = gridViewWIDataDetail.GetRowHandle(i);
                IODetail iODetail = new IODetail();
                iODetail.IONumber = textEditWIDNumber.Text;
                iODetail.Ordinal = i + 1;
                iODetail.GoodsID = (string?)gridViewWIDataDetail.GetRowCellValue(rowHandle, "GoodsID");
                iODetail.GoodsName = (string?)gridViewWIDataDetail.GetRowCellValue(i, "GoodsName");
                iODetail.Quantity = (Decimal?)gridViewWIDataDetail.GetRowCellValue(i, "Quantity");
                iODetail.IssueAmount = 0m;
                iODetail.IssueQuantity = (Decimal?)gridViewWIDataDetail.GetRowCellValue(i, "Quantity");
                iODetail.QuantityOrdered = (Decimal?)gridViewWIDataDetail.GetRowCellValue(i, "QuantityOrg");
                iODetail.QuantityIssued = (Decimal?)gridViewWIDataDetail.GetRowCellValue(i, "Quantity");
                iODetail.Note = "";
                iODetail.Status = "-1";
                if (await iODetailRepository.Create(iODetail) > 0)
                {
                    i++;
                }
            }
        }


        private async void WIDNumber_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {

                WIDataHeader wIDataHeader = new WIDataHeader();
                wIDataHeader = await wIDataHeaderRepository.GetUnderID(textEditWIDNumber.Text);
                if (wIDataHeader != null)
                {
                    dateEditWIDDate.Text = wIDataHeader.WIDDate.ToString();
                    comboBoxEditHandlingStatus.SelectedIndex = int.Parse(wIDataHeader.HandlingStatusID);
                    sbLoadDataForGridWIDataGeneralAsync(textEditWIDNumber.Text);
                }
                else
                {
                    MessageBox.Show("Số phiếu yêu cầu không tồn tại");
                    return;
                }

            }
        }

        private async void textEditWDRNumer_CellDoubleClick(object sender, EventArgs e)
        {
            frmSearchWID frmSearchWID = new frmSearchWID();
            frmSearchWID.ShowDialog(this);
            frmSearchWID.Dispose();
            if (selectedWIDNumber != null)
            {
                textEditWIDNumber.Text = selectedWIDNumber;
                WIDataHeader wIDataHeader = await wIDataHeaderRepository.GetUnderID(textEditWIDNumber.Text);
                dateEditWIDDate.Text = wIDataHeader.WIDDate.ToString();
                comboBoxEditHandlingStatus.SelectedIndex = int.Parse(wIDataHeader.HandlingStatusID) + 1;
                await sbLoadDataForGridWIDataGeneralAsync(textEditWIDNumber.Text);
            }
            QRCodeGeneral(selectedWIDNumber);
            isFilterClick = false;

            listPublic = await wIDataGeneralRepository.GetUnderID(textEditWIDNumber.Text);
            popupMenuSelectGoodsGroupID();
            popupMenuSelectPickerName();


        }

        private async void Timer_Tick(object sender, EventArgs e)
        {
            if (textEditWIDNumber.Text != "")
            {
                if (isFilterClick == true)
                {

                    sbLoadDataForGridWIDataGeneralAsync2(textEditWIDNumber.Text);
                }
                else
                {
                    sbLoadDataForGridWIDataGeneralAsync(textEditWIDNumber.Text);
                }
            }
        }

        private void grdData_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            int i = 0;
            i = e.RowHandle;
            if ((i >= 0) && (String.Compare(e.Column.FieldName, "Quantity", false) == 0))
            {
                double mQuantity = ((gridViewWIDataDetail.GetRowCellValue(i, gridViewWIDataDetail.Columns["Quantity"]) == "") ? 0.0 : Convert.ToDouble(gridViewWIDataDetail.GetRowCellValue(i, gridViewWIDataDetail.Columns["Quantity"])));
                double mQuantityOrg = ((gridViewWIDataDetail.GetRowCellValue(i, gridViewWIDataDetail.Columns["QuantityOrg"]) == "") ? 0.0 : Convert.ToDouble(gridViewWIDataDetail.GetRowCellValue(i, gridViewWIDataDetail.Columns["QuantityOrg"])));
                double mPackingQuantity = ((gridViewWIDataDetail.GetRowCellValue(i, gridViewWIDataDetail.Columns["PackingQuantity"]) == "") ? 0.0 : Convert.ToDouble(gridViewWIDataDetail.GetRowCellValue(i, gridViewWIDataDetail.Columns["PackingQuantity"])));
                e.Appearance.Font = new Font("Tahoma", 8f, FontStyle.Bold);
                if (mQuantity == mQuantityOrg && mQuantity != 0.0)
                {
                    e.Appearance.BackColor = System.Drawing.Color.LightGreen;
                }
                if (mQuantity > mQuantityOrg && mQuantity != 0.0)
                {
                    e.Appearance.BackColor = System.Drawing.Color.Yellow;
                }
                if (mQuantity < mQuantityOrg && mQuantity != 0.0)
                {
                    e.Appearance.BackColor = System.Drawing.Color.Red;
                }
            }
            if ((i >= 0) && (String.Compare(e.Column.FieldName, "PackingQuantity", false) == 0))
            {
                double mQuantity = ((gridViewWIDataDetail.GetRowCellValue(i, gridViewWIDataDetail.Columns["Quantity"]) == "") ? 0.0 : Convert.ToDouble(gridViewWIDataDetail.GetRowCellValue(i, gridViewWIDataDetail.Columns["Quantity"])));
                double mQuantityOrg = ((gridViewWIDataDetail.GetRowCellValue(i, gridViewWIDataDetail.Columns["QuantityOrg"]) == "") ? 0.0 : Convert.ToDouble(gridViewWIDataDetail.GetRowCellValue(i, gridViewWIDataDetail.Columns["QuantityOrg"])));
                e.Appearance.Font = new Font("Tahoma", 8f, FontStyle.Bold);
                if (mQuantity == mQuantityOrg && mQuantity != 0.0)
                {
                    e.Appearance.BackColor = System.Drawing.Color.LightGreen;
                }
                if (mQuantity > mQuantityOrg && mQuantity != 0.0)
                {
                    e.Appearance.BackColor = System.Drawing.Color.Yellow;
                }
                if (mQuantity < mQuantityOrg && mQuantity != 0.0)
                {
                    e.Appearance.BackColor = System.Drawing.Color.Red;
                }
            }
        }

        private void textTotalQuantity_CustomDraw()
        {
            //if (String.Compare(textEditTotalQuantityOrg.Text, "", false) == 0)
            //{
            double mTotalQuantity = ((textEditTotalQuantity.Text == "") ? 0.0 : Convert.ToDouble(textEditTotalQuantity.Text));
            double mTotalQuantityOrg = ((textEditTotalQuantityOrg.Text == "") ? 0.0 : Convert.ToDouble(textEditTotalQuantityOrg.Text));
            textEditTotalQuantity.Font = new Font("Tahoma", 8f, FontStyle.Bold);
            textEditTotalQuantityOrg.Font = new Font("Tahoma", 8f, FontStyle.Bold);
            if (mTotalQuantity == mTotalQuantityOrg && mTotalQuantity != 0.0)
            {
                textEditTotalQuantity.BackColor = System.Drawing.Color.LightGreen;
                textEditTotalQuantityOrg.BackColor = System.Drawing.Color.LightGreen;
            }
            if (mTotalQuantity > mTotalQuantityOrg && mTotalQuantity != 0.0)
            {
                textEditTotalQuantity.BackColor = System.Drawing.Color.Yellow;
                textEditTotalQuantityOrg.BackColor = System.Drawing.Color.LightGreen;
            }
            if (mTotalQuantity < mTotalQuantityOrg && mTotalQuantity != 0.0)
            {
                textEditTotalQuantity.BackColor = System.Drawing.Color.Red;
                textEditTotalQuantityOrg.BackColor = System.Drawing.Color.LightGreen;
            }
            //}
        }

        private void butOn_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Bắt đầu quét xuất kho");
            t.Enabled = true;
        }

        private void butOff_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Kết thúc quét xuất kho");
            t.Enabled = false;
        }



        //barbutton click
        public async void button_Click(object sender, EventArgs e)
        {
            ButtonEventArgs eArg = (DevExpress.XtraBars.Docking2010.ButtonEventArgs)e;
            String tag = eArg.Button.Properties.Tag.ToString();
            switch (tag)
            {
                case "new":
                    if (WMPublic.sbMessageAddNewRequest(this) == true)
                    {
                        clearAsync();
                    }
                    break;
                case "save":
                    if (WMPublic.sbMessageSaveChangeRequest(this) == true)
                    {
                        updateWIDataHeaderAsync();
                    }
                    break;
                case "delete":
                    if (WMPublic.sbMessageDeleteRequest(this) == true)
                    {
                        DeleteWIDataHeaderAsync();
                    }
                    break;
                case "search":

                    break;
                case "refesh":
                    comboBoxEditGoodsGroupID.SelectedIndex = 0;
                    comboBoxEditPickerName.SelectedIndex = 0;
                    comboBoxEditScanOption.SelectedIndex = 0;
                    sbLoadDataForGridWIDataGeneralAsync(textEditWIDNumber.Text);
                    break;
                case "import":
                    break;
                case "export":
                    break;
                case "print":
                    break;
                case "close":
                    break;
            }

        }

        private async void simpleButtonFilter_ClickAsync(object sender, EventArgs e)
        {
            if (textEditWIDNumber.Text != "")
            {
                isFilterClick = true;
                //await sortPopupAsync(textEditWIDNumber.Text);
                selectedGoodsGroupID = comboBoxEditGoodsGroupID.SelectedItem.ToString();
                selectedPickerName = comboBoxEditPickerName.SelectedItem.ToString();
                selectedScanOption = comboBoxEditScanOption.SelectedItem.ToString();
                listPublic2 = await wIDataGeneralRepository.Filter(textEditWIDNumber.Text, selectedGoodsGroupID, selectedPickerName, selectedScanOption);
                int mMaxRow = 100;
                if (listPublic2.Count < mMaxRow)
                {
                    int num = mMaxRow - listPublic2.Count;
                    int i = 1;
                    while (true)
                    {
                        int num2 = i;
                        int num3 = num;
                        if (num2 > num3)
                        {
                            break;
                        }
                        listPublic2.Add(new WIDataGeneral());
                        i++;
                    }
                }
                gridControlWIDataDetail.DataSource = listPublic2;
                getTotalQuantityOrg();
                getTotalQuantity();
                textTotalQuantity_CustomDraw();
                MessageBox.Show("Filter Done");
            }
        }
    }
}
