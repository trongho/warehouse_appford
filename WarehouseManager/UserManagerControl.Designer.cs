﻿
using DevExpress.XtraLayout.Utils;

namespace WarehouseManager
{
    partial class UserManagerControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions1 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserManagerControl));
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions2 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions3 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions4 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions5 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions6 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions7 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions8 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions9 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition1 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition2 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition3 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition4 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition1 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition2 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition3 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition4 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition5 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition6 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition7 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition8 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition9 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition10 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition11 = new DevExpress.XtraLayout.RowDefinition();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.windowsUIButtonPanel1 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.gridControlUser = new DevExpress.XtraGrid.GridControl();
            this.userBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.wARHOUSE_HPDataSet = new WarehouseManager.WARHOUSE_HPDataSet();
            this.gridViewUser = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colNo2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLoginName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBlocked = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBranchID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBranchName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAdress = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPosition = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRole = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.textEditUserID = new DevExpress.XtraEditors.TextEdit();
            this.textEditFullName = new DevExpress.XtraEditors.TextEdit();
            this.textEditAdress = new DevExpress.XtraEditors.TextEdit();
            this.comboBoxEditBranch = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEditPhone = new DevExpress.XtraEditors.TextEdit();
            this.textEditEmail = new DevExpress.XtraEditors.TextEdit();
            this.textEditUsername = new DevExpress.XtraEditors.TextEdit();
            this.textEditPassword = new DevExpress.XtraEditors.TextEdit();
            this.checkEditActive = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditBlocked = new DevExpress.XtraEditors.CheckEdit();
            this.comboBoxEditRole = new DevExpress.XtraEditors.ComboBoxEdit();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.textEditRePassword = new DevExpress.XtraEditors.TextEdit();
            this.comboBoxEditPosition = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEditDescription = new DevExpress.XtraEditors.MemoEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemUserId = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemFullname = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemAdress = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemBranch = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemPhone = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemEmail = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemPassword = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemUsername = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControl3 = new DevExpress.XtraLayout.LayoutControl();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPageScreen = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlScreen = new DevExpress.XtraGrid.GridControl();
            this.gridViewScreen = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colNo3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colScreenName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colScreenNameEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSaveNewRight = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSaveChangeRight = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDeleteRight = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrintRight = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colImportRight = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExportRight = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colScreenID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrdinal1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckedComboBoxEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit();
            this.repositoryItemComboBox2 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.xtraTabPageReport = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlReport = new DevExpress.XtraGrid.GridControl();
            this.reportBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewReport = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colReportID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReportName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReportNameEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrdinal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNo4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colViewRight = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrintRight2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckedComboBoxEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit();
            this.repositoryItemComboBox3 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.xtraTabPageOther = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlRight = new DevExpress.XtraGrid.GridControl();
            this.gridViewRight = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRightID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRightName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRightNameEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGrantRight = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrdinal3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckedComboBoxEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit();
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.screenBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.userTableAdapter = new WarehouseManager.WARHOUSE_HPDataSetTableAdapters.UserTableAdapter();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.screenTableAdapter = new WarehouseManager.WARHOUSE_HPDataSetTableAdapters.ScreenTableAdapter();
            this.reportTableAdapter = new WarehouseManager.WARHOUSE_HPDataSetTableAdapters.ReportTableAdapter();
            this.popupMenu1 = new DevExpress.XtraBars.PopupMenu(this.components);
            this.svgImageCollection1 = new DevExpress.Utils.SvgImageCollection(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.userBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wARHOUSE_HPDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditUserID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditFullName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditAdress.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditBranch.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPhone.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditEmail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditUsername.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPassword.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditActive.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditBlocked.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditRole.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditRePassword.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditPosition.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditDescription.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemUserId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemFullname)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemAdress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemBranch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPhone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPassword)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemUsername)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).BeginInit();
            this.layoutControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPageScreen.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlScreen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewScreen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            this.xtraTabPageReport.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            this.xtraTabPageOther.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlRight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewRight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.screenBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.svgImageCollection1)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.windowsUIButtonPanel1);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(779, 194, 650, 400);
            this.layoutControl1.Root = this.Root;
            this.layoutControl1.Size = new System.Drawing.Size(1161, 40);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // windowsUIButtonPanel1
            // 
            this.windowsUIButtonPanel1.AllowGlyphSkinning = false;
            this.windowsUIButtonPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            windowsUIButtonImageOptions1.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions1.Image")));
            windowsUIButtonImageOptions1.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions2.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions2.Image")));
            windowsUIButtonImageOptions2.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions3.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions3.Image")));
            windowsUIButtonImageOptions3.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions4.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions4.Image")));
            windowsUIButtonImageOptions4.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions5.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions5.Image")));
            windowsUIButtonImageOptions5.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions6.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions6.Image")));
            windowsUIButtonImageOptions6.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions7.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions7.Image")));
            windowsUIButtonImageOptions7.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions8.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions8.Image")));
            windowsUIButtonImageOptions8.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions9.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions9.Image")));
            windowsUIButtonImageOptions9.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            this.windowsUIButtonPanel1.Buttons.AddRange(new DevExpress.XtraEditors.ButtonPanel.IBaseButton[] {
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F2) Thêm mới", true, windowsUIButtonImageOptions1, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "new", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F3) Lưu", true, windowsUIButtonImageOptions2, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "save", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F4) Xóa", true, windowsUIButtonImageOptions3, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "delete", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F5) Tìm kiếm", true, windowsUIButtonImageOptions4, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "search", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F6) Làm mới", true, windowsUIButtonImageOptions5, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "refesh", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F7) Nhập dữ liệu", true, windowsUIButtonImageOptions6, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "import", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F8) Xuất dữ liệu", true, windowsUIButtonImageOptions7, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "export", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F9) In ấn", true, windowsUIButtonImageOptions8, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "print", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F10) Thoát", true, windowsUIButtonImageOptions9, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "close", -1, false)});
            this.windowsUIButtonPanel1.ContentAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.windowsUIButtonPanel1.Location = new System.Drawing.Point(2, 2);
            this.windowsUIButtonPanel1.Name = "windowsUIButtonPanel1";
            this.windowsUIButtonPanel1.Padding = new System.Windows.Forms.Padding(5);
            this.windowsUIButtonPanel1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.windowsUIButtonPanel1.Size = new System.Drawing.Size(1157, 32);
            this.windowsUIButtonPanel1.TabIndex = 6;
            this.windowsUIButtonPanel1.Text = "windowsUIButtonPanel1";
            this.windowsUIButtonPanel1.UseButtonBackgroundImages = false;
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.Root.Name = "Root";
            this.Root.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.Root.Size = new System.Drawing.Size(1161, 40);
            this.Root.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.windowsUIButtonPanel1;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(1161, 40);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // gridControlUser
            // 
            this.gridControlUser.DataSource = this.userBindingSource;
            this.gridControlUser.Location = new System.Drawing.Point(490, 2);
            this.gridControlUser.MainView = this.gridViewUser;
            this.gridControlUser.Name = "gridControlUser";
            this.gridControlUser.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit4,
            this.repositoryItemCheckEdit5});
            this.gridControlUser.Size = new System.Drawing.Size(669, 342);
            this.gridControlUser.TabIndex = 4;
            this.gridControlUser.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewUser});
            // 
            // userBindingSource
            // 
            this.userBindingSource.DataMember = "User";
            this.userBindingSource.DataSource = this.wARHOUSE_HPDataSet;
            // 
            // wARHOUSE_HPDataSet
            // 
            this.wARHOUSE_HPDataSet.DataSetName = "WARHOUSE_HPDataSet";
            this.wARHOUSE_HPDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridViewUser
            // 
            this.gridViewUser.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colNo2,
            this.colUserID,
            this.colUserName,
            this.colLoginName,
            this.colActive,
            this.colBlocked,
            this.colBranchID,
            this.colBranchName,
            this.colAdress,
            this.colEmail,
            this.colPosition,
            this.colDescription,
            this.colRole});
            this.gridViewUser.GridControl = this.gridControlUser;
            this.gridViewUser.Name = "gridViewUser";
            // 
            // colNo2
            // 
            this.colNo2.Caption = "No";
            this.colNo2.FieldName = "No2";
            this.colNo2.Name = "colNo2";
            this.colNo2.OptionsColumn.ReadOnly = true;
            this.colNo2.Visible = true;
            this.colNo2.VisibleIndex = 0;
            this.colNo2.Width = 50;
            // 
            // colUserID
            // 
            this.colUserID.FieldName = "UserID";
            this.colUserID.Name = "colUserID";
            this.colUserID.Visible = true;
            this.colUserID.VisibleIndex = 1;
            this.colUserID.Width = 132;
            // 
            // colUserName
            // 
            this.colUserName.FieldName = "UserName";
            this.colUserName.Name = "colUserName";
            this.colUserName.Visible = true;
            this.colUserName.VisibleIndex = 2;
            this.colUserName.Width = 251;
            // 
            // colLoginName
            // 
            this.colLoginName.FieldName = "LoginName";
            this.colLoginName.Name = "colLoginName";
            this.colLoginName.Visible = true;
            this.colLoginName.VisibleIndex = 4;
            this.colLoginName.Width = 258;
            // 
            // colActive
            // 
            this.colActive.FieldName = "Active";
            this.colActive.MinWidth = 21;
            this.colActive.Name = "colActive";
            this.colActive.Visible = true;
            this.colActive.VisibleIndex = 5;
            // 
            // colBlocked
            // 
            this.colBlocked.FieldName = "Blocked";
            this.colBlocked.MinWidth = 21;
            this.colBlocked.Name = "colBlocked";
            this.colBlocked.Visible = true;
            this.colBlocked.VisibleIndex = 6;
            // 
            // colBranchID
            // 
            this.colBranchID.FieldName = "BranchID";
            this.colBranchID.MinWidth = 21;
            this.colBranchID.Name = "colBranchID";
            // 
            // colBranchName
            // 
            this.colBranchName.FieldName = "BranchName";
            this.colBranchName.MinWidth = 21;
            this.colBranchName.Name = "colBranchName";
            // 
            // colAdress
            // 
            this.colAdress.FieldName = "Adress";
            this.colAdress.MinWidth = 21;
            this.colAdress.Name = "colAdress";
            // 
            // colEmail
            // 
            this.colEmail.FieldName = "Email";
            this.colEmail.MinWidth = 21;
            this.colEmail.Name = "colEmail";
            // 
            // colPosition
            // 
            this.colPosition.FieldName = "Position";
            this.colPosition.MinWidth = 21;
            this.colPosition.Name = "colPosition";
            // 
            // colDescription
            // 
            this.colDescription.FieldName = "Description";
            this.colDescription.MinWidth = 21;
            this.colDescription.Name = "colDescription";
            // 
            // colRole
            // 
            this.colRole.Caption = "Role";
            this.colRole.FieldName = "Role";
            this.colRole.Name = "colRole";
            this.colRole.Visible = true;
            this.colRole.VisibleIndex = 3;
            // 
            // repositoryItemCheckEdit4
            // 
            this.repositoryItemCheckEdit4.AutoHeight = false;
            this.repositoryItemCheckEdit4.FullFocusRect = true;
            this.repositoryItemCheckEdit4.Name = "repositoryItemCheckEdit4";
            this.repositoryItemCheckEdit4.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            // 
            // repositoryItemCheckEdit5
            // 
            this.repositoryItemCheckEdit5.AutoHeight = false;
            this.repositoryItemCheckEdit5.Name = "repositoryItemCheckEdit5";
            this.repositoryItemCheckEdit5.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            this.repositoryItemCheckEdit5.ValueChecked = ((short)(1));
            this.repositoryItemCheckEdit5.ValueUnchecked = ((short)(0));
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.gridControlUser);
            this.layoutControl2.Controls.Add(this.textEditUserID);
            this.layoutControl2.Controls.Add(this.textEditFullName);
            this.layoutControl2.Controls.Add(this.textEditAdress);
            this.layoutControl2.Controls.Add(this.comboBoxEditBranch);
            this.layoutControl2.Controls.Add(this.textEditPhone);
            this.layoutControl2.Controls.Add(this.textEditEmail);
            this.layoutControl2.Controls.Add(this.textEditUsername);
            this.layoutControl2.Controls.Add(this.textEditPassword);
            this.layoutControl2.Controls.Add(this.checkEditActive);
            this.layoutControl2.Controls.Add(this.checkEditBlocked);
            this.layoutControl2.Controls.Add(this.comboBoxEditRole);
            this.layoutControl2.Controls.Add(this.textEditRePassword);
            this.layoutControl2.Controls.Add(this.comboBoxEditPosition);
            this.layoutControl2.Controls.Add(this.textEditDescription);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl2.Location = new System.Drawing.Point(0, 40);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(474, 289, 650, 400);
            this.layoutControl2.Root = this.layoutControlGroup1;
            this.layoutControl2.Size = new System.Drawing.Size(1161, 346);
            this.layoutControl2.TabIndex = 1;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // textEditUserID
            // 
            this.textEditUserID.Location = new System.Drawing.Point(91, 2);
            this.textEditUserID.Name = "textEditUserID";
            this.textEditUserID.Properties.ReadOnly = true;
            this.textEditUserID.Properties.UseReadOnlyAppearance = false;
            this.textEditUserID.Size = new System.Drawing.Size(151, 20);
            this.textEditUserID.StyleController = this.layoutControl2;
            this.textEditUserID.TabIndex = 5;
            this.textEditUserID.Validating += new System.ComponentModel.CancelEventHandler(this.textEditUserID_Validating);
            // 
            // textEditFullName
            // 
            this.textEditFullName.Location = new System.Drawing.Point(91, 33);
            this.textEditFullName.Name = "textEditFullName";
            this.textEditFullName.Size = new System.Drawing.Size(395, 20);
            this.textEditFullName.StyleController = this.layoutControl2;
            this.textEditFullName.TabIndex = 6;
            this.textEditFullName.Validating += new System.ComponentModel.CancelEventHandler(this.textEditFullName_Validating);
            // 
            // textEditAdress
            // 
            this.textEditAdress.Location = new System.Drawing.Point(91, 95);
            this.textEditAdress.Name = "textEditAdress";
            this.textEditAdress.Size = new System.Drawing.Size(395, 20);
            this.textEditAdress.StyleController = this.layoutControl2;
            this.textEditAdress.TabIndex = 7;
            // 
            // comboBoxEditBranch
            // 
            this.comboBoxEditBranch.Location = new System.Drawing.Point(91, 64);
            this.comboBoxEditBranch.Name = "comboBoxEditBranch";
            this.comboBoxEditBranch.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditBranch.Size = new System.Drawing.Size(395, 20);
            this.comboBoxEditBranch.StyleController = this.layoutControl2;
            this.comboBoxEditBranch.TabIndex = 9;
            // 
            // textEditPhone
            // 
            this.textEditPhone.Location = new System.Drawing.Point(335, 126);
            this.textEditPhone.Name = "textEditPhone";
            this.textEditPhone.Size = new System.Drawing.Size(824, 20);
            this.textEditPhone.StyleController = this.layoutControl2;
            this.textEditPhone.TabIndex = 10;
            // 
            // textEditEmail
            // 
            this.textEditEmail.Location = new System.Drawing.Point(91, 157);
            this.textEditEmail.Name = "textEditEmail";
            this.textEditEmail.Size = new System.Drawing.Size(395, 20);
            this.textEditEmail.StyleController = this.layoutControl2;
            this.textEditEmail.TabIndex = 11;
            // 
            // textEditUsername
            // 
            this.textEditUsername.Location = new System.Drawing.Point(91, 188);
            this.textEditUsername.Name = "textEditUsername";
            this.textEditUsername.Size = new System.Drawing.Size(395, 20);
            this.textEditUsername.StyleController = this.layoutControl2;
            this.textEditUsername.TabIndex = 12;
            this.textEditUsername.Validating += new System.ComponentModel.CancelEventHandler(this.textEditUsername_Validating);
            // 
            // textEditPassword
            // 
            this.textEditPassword.Location = new System.Drawing.Point(91, 219);
            this.textEditPassword.Name = "textEditPassword";
            this.textEditPassword.Properties.PasswordChar = '*';
            this.textEditPassword.Properties.UseSystemPasswordChar = true;
            this.textEditPassword.Size = new System.Drawing.Size(151, 20);
            this.textEditPassword.StyleController = this.layoutControl2;
            this.textEditPassword.TabIndex = 13;
            this.textEditPassword.Validating += new System.ComponentModel.CancelEventHandler(this.textEditPassword_Validating);
            // 
            // checkEditActive
            // 
            this.checkEditActive.Location = new System.Drawing.Point(2, 312);
            this.checkEditActive.Name = "checkEditActive";
            this.checkEditActive.Properties.Caption = "Active";
            this.checkEditActive.Size = new System.Drawing.Size(179, 19);
            this.checkEditActive.StyleController = this.layoutControl2;
            this.checkEditActive.TabIndex = 15;
            // 
            // checkEditBlocked
            // 
            this.checkEditBlocked.Location = new System.Drawing.Point(246, 312);
            this.checkEditBlocked.Name = "checkEditBlocked";
            this.checkEditBlocked.Properties.Caption = "Blocked";
            this.checkEditBlocked.Size = new System.Drawing.Size(240, 19);
            this.checkEditBlocked.StyleController = this.layoutControl2;
            this.checkEditBlocked.TabIndex = 16;
            // 
            // comboBoxEditRole
            // 
            this.comboBoxEditRole.Location = new System.Drawing.Point(335, 2);
            this.comboBoxEditRole.MenuManager = this.barManager1;
            this.comboBoxEditRole.Name = "comboBoxEditRole";
            this.comboBoxEditRole.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditRole.Size = new System.Drawing.Size(151, 20);
            this.comboBoxEditRole.StyleController = this.layoutControl2;
            this.comboBoxEditRole.TabIndex = 17;
            this.comboBoxEditRole.SelectedValueChanged += new System.EventHandler(this.comboBoxEditRole_SelectedValueChanged);
            // 
            // barManager1
            // 
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(1161, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 616);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(1161, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 616);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1161, 0);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 616);
            // 
            // textEditRePassword
            // 
            this.textEditRePassword.Location = new System.Drawing.Point(335, 219);
            this.textEditRePassword.MenuManager = this.barManager1;
            this.textEditRePassword.Name = "textEditRePassword";
            this.textEditRePassword.Properties.PasswordChar = '*';
            this.textEditRePassword.Properties.UseSystemPasswordChar = true;
            this.textEditRePassword.Size = new System.Drawing.Size(151, 20);
            this.textEditRePassword.StyleController = this.layoutControl2;
            this.textEditRePassword.TabIndex = 18;
            this.textEditRePassword.Validating += new System.ComponentModel.CancelEventHandler(this.textEditRePassword_Validating);
            // 
            // comboBoxEditPosition
            // 
            this.comboBoxEditPosition.Location = new System.Drawing.Point(91, 126);
            this.comboBoxEditPosition.MenuManager = this.barManager1;
            this.comboBoxEditPosition.Name = "comboBoxEditPosition";
            this.comboBoxEditPosition.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditPosition.Size = new System.Drawing.Size(151, 20);
            this.comboBoxEditPosition.StyleController = this.layoutControl2;
            this.comboBoxEditPosition.TabIndex = 19;
            // 
            // textEditDescription
            // 
            this.textEditDescription.Location = new System.Drawing.Point(91, 250);
            this.textEditDescription.Name = "textEditDescription";
            this.textEditDescription.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.textEditDescription.Size = new System.Drawing.Size(395, 58);
            this.textEditDescription.StyleController = this.layoutControl2;
            this.textEditDescription.TabIndex = 14;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItemUserId,
            this.layoutControlItemFullname,
            this.layoutControlItem5,
            this.layoutControlItemAdress,
            this.layoutControlItemBranch,
            this.layoutControlItem8,
            this.layoutControlItemPhone,
            this.layoutControlItemEmail,
            this.layoutControlItemPassword,
            this.layoutControlItem7,
            this.layoutControlItem6,
            this.layoutControlItemUsername,
            this.layoutControlItem4,
            this.layoutControlItemDescription});
            this.layoutControlGroup1.LayoutMode = DevExpress.XtraLayout.Utils.LayoutMode.Table;
            this.layoutControlGroup1.Name = "Root";
            columnDefinition1.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition1.Width = 15.789473684210526D;
            columnDefinition2.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition2.Width = 5.2631578947368416D;
            columnDefinition3.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition3.Width = 21.052631578947366D;
            columnDefinition4.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition4.Width = 57.89473684210526D;
            this.layoutControlGroup1.OptionsTableLayoutGroup.ColumnDefinitions.AddRange(new DevExpress.XtraLayout.ColumnDefinition[] {
            columnDefinition1,
            columnDefinition2,
            columnDefinition3,
            columnDefinition4});
            rowDefinition1.Height = 9.09090909090909D;
            rowDefinition1.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition2.Height = 9.09090909090909D;
            rowDefinition2.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition3.Height = 9.09090909090909D;
            rowDefinition3.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition4.Height = 9.09090909090909D;
            rowDefinition4.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition5.Height = 9.09090909090909D;
            rowDefinition5.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition6.Height = 9.09090909090909D;
            rowDefinition6.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition7.Height = 9.09090909090909D;
            rowDefinition7.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition8.Height = 9.09090909090909D;
            rowDefinition8.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition9.Height = 9.09090909090909D;
            rowDefinition9.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition10.Height = 9.09090909090909D;
            rowDefinition10.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition11.Height = 9.09090909090909D;
            rowDefinition11.SizeType = System.Windows.Forms.SizeType.Percent;
            this.layoutControlGroup1.OptionsTableLayoutGroup.RowDefinitions.AddRange(new DevExpress.XtraLayout.RowDefinition[] {
            rowDefinition1,
            rowDefinition2,
            rowDefinition3,
            rowDefinition4,
            rowDefinition5,
            rowDefinition6,
            rowDefinition7,
            rowDefinition8,
            rowDefinition9,
            rowDefinition10,
            rowDefinition11});
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(1161, 346);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.gridControlUser;
            this.layoutControlItem2.Location = new System.Drawing.Point(488, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.OptionsTableLayoutItem.ColumnIndex = 3;
            this.layoutControlItem2.OptionsTableLayoutItem.RowSpan = 11;
            this.layoutControlItem2.Size = new System.Drawing.Size(673, 346);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItemUserId
            // 
            this.layoutControlItemUserId.Control = this.textEditUserID;
            this.layoutControlItemUserId.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemUserId.Name = "layoutControlItemUserId";
            this.layoutControlItemUserId.OptionsTableLayoutItem.ColumnSpan = 2;
            this.layoutControlItemUserId.Size = new System.Drawing.Size(244, 31);
            this.layoutControlItemUserId.Text = "Mã người dùng";
            this.layoutControlItemUserId.TextSize = new System.Drawing.Size(85, 13);
            // 
            // layoutControlItemFullname
            // 
            this.layoutControlItemFullname.Control = this.textEditFullName;
            this.layoutControlItemFullname.Location = new System.Drawing.Point(0, 31);
            this.layoutControlItemFullname.Name = "layoutControlItemFullname";
            this.layoutControlItemFullname.OptionsTableLayoutItem.ColumnSpan = 3;
            this.layoutControlItemFullname.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItemFullname.Size = new System.Drawing.Size(488, 31);
            this.layoutControlItemFullname.Text = "Tên người dùng";
            this.layoutControlItemFullname.TextSize = new System.Drawing.Size(85, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.checkEditBlocked;
            this.layoutControlItem5.Location = new System.Drawing.Point(244, 310);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.OptionsTableLayoutItem.ColumnIndex = 2;
            this.layoutControlItem5.OptionsTableLayoutItem.RowIndex = 10;
            this.layoutControlItem5.Size = new System.Drawing.Size(244, 36);
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlItemAdress
            // 
            this.layoutControlItemAdress.Control = this.textEditAdress;
            this.layoutControlItemAdress.Location = new System.Drawing.Point(0, 93);
            this.layoutControlItemAdress.Name = "layoutControlItemAdress";
            this.layoutControlItemAdress.OptionsTableLayoutItem.ColumnSpan = 3;
            this.layoutControlItemAdress.OptionsTableLayoutItem.RowIndex = 3;
            this.layoutControlItemAdress.Size = new System.Drawing.Size(488, 31);
            this.layoutControlItemAdress.Text = "Địa chỉ";
            this.layoutControlItemAdress.TextSize = new System.Drawing.Size(85, 13);
            // 
            // layoutControlItemBranch
            // 
            this.layoutControlItemBranch.Control = this.comboBoxEditBranch;
            this.layoutControlItemBranch.Location = new System.Drawing.Point(0, 62);
            this.layoutControlItemBranch.Name = "layoutControlItemBranch";
            this.layoutControlItemBranch.OptionsTableLayoutItem.ColumnSpan = 3;
            this.layoutControlItemBranch.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlItemBranch.Size = new System.Drawing.Size(488, 31);
            this.layoutControlItemBranch.Text = "Chi nhánh";
            this.layoutControlItemBranch.TextSize = new System.Drawing.Size(85, 13);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.comboBoxEditPosition;
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 124);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.OptionsTableLayoutItem.ColumnSpan = 2;
            this.layoutControlItem8.OptionsTableLayoutItem.RowIndex = 4;
            this.layoutControlItem8.Size = new System.Drawing.Size(244, 31);
            this.layoutControlItem8.Text = "Chức vụ";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(85, 13);
            // 
            // layoutControlItemPhone
            // 
            this.layoutControlItemPhone.Control = this.textEditPhone;
            this.layoutControlItemPhone.CustomizationFormText = "Số điện thoại";
            this.layoutControlItemPhone.Location = new System.Drawing.Point(244, 124);
            this.layoutControlItemPhone.Name = "layoutControlItemPhone";
            this.layoutControlItemPhone.OptionsTableLayoutItem.ColumnIndex = 2;
            this.layoutControlItemPhone.OptionsTableLayoutItem.ColumnSpan = 2;
            this.layoutControlItemPhone.OptionsTableLayoutItem.RowIndex = 4;
            this.layoutControlItemPhone.Size = new System.Drawing.Size(917, 31);
            this.layoutControlItemPhone.Text = "Số điện thoại";
            this.layoutControlItemPhone.TextSize = new System.Drawing.Size(85, 13);
            // 
            // layoutControlItemEmail
            // 
            this.layoutControlItemEmail.Control = this.textEditEmail;
            this.layoutControlItemEmail.Location = new System.Drawing.Point(0, 155);
            this.layoutControlItemEmail.Name = "layoutControlItemEmail";
            this.layoutControlItemEmail.OptionsTableLayoutItem.ColumnSpan = 3;
            this.layoutControlItemEmail.OptionsTableLayoutItem.RowIndex = 5;
            this.layoutControlItemEmail.Size = new System.Drawing.Size(488, 31);
            this.layoutControlItemEmail.Text = "Email";
            this.layoutControlItemEmail.TextSize = new System.Drawing.Size(85, 13);
            // 
            // layoutControlItemPassword
            // 
            this.layoutControlItemPassword.Control = this.textEditPassword;
            this.layoutControlItemPassword.Location = new System.Drawing.Point(0, 217);
            this.layoutControlItemPassword.Name = "layoutControlItemPassword";
            this.layoutControlItemPassword.OptionsTableLayoutItem.ColumnSpan = 2;
            this.layoutControlItemPassword.OptionsTableLayoutItem.RowIndex = 7;
            this.layoutControlItemPassword.Size = new System.Drawing.Size(244, 31);
            this.layoutControlItemPassword.Text = "Mật khẩu";
            this.layoutControlItemPassword.TextSize = new System.Drawing.Size(85, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.textEditRePassword;
            this.layoutControlItem7.Location = new System.Drawing.Point(244, 217);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.OptionsTableLayoutItem.ColumnIndex = 2;
            this.layoutControlItem7.OptionsTableLayoutItem.RowIndex = 7;
            this.layoutControlItem7.Size = new System.Drawing.Size(244, 31);
            this.layoutControlItem7.Text = "Nhập lại mật khẩu";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(85, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.comboBoxEditRole;
            this.layoutControlItem6.Location = new System.Drawing.Point(244, 0);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.OptionsTableLayoutItem.ColumnIndex = 2;
            this.layoutControlItem6.Size = new System.Drawing.Size(244, 31);
            this.layoutControlItem6.Text = "Role";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(85, 13);
            // 
            // layoutControlItemUsername
            // 
            this.layoutControlItemUsername.Control = this.textEditUsername;
            this.layoutControlItemUsername.Location = new System.Drawing.Point(0, 186);
            this.layoutControlItemUsername.Name = "layoutControlItemUsername";
            this.layoutControlItemUsername.OptionsTableLayoutItem.ColumnSpan = 3;
            this.layoutControlItemUsername.OptionsTableLayoutItem.RowIndex = 6;
            this.layoutControlItemUsername.Size = new System.Drawing.Size(488, 31);
            this.layoutControlItemUsername.Text = "Tên đăng nhập";
            this.layoutControlItemUsername.TextSize = new System.Drawing.Size(85, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.checkEditActive;
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 310);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.OptionsTableLayoutItem.RowIndex = 10;
            this.layoutControlItem4.Size = new System.Drawing.Size(183, 36);
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItemDescription
            // 
            this.layoutControlItemDescription.Control = this.textEditDescription;
            this.layoutControlItemDescription.Location = new System.Drawing.Point(0, 248);
            this.layoutControlItemDescription.Name = "layoutControlItemDescription";
            this.layoutControlItemDescription.OptionsTableLayoutItem.ColumnSpan = 3;
            this.layoutControlItemDescription.OptionsTableLayoutItem.RowIndex = 8;
            this.layoutControlItemDescription.OptionsTableLayoutItem.RowSpan = 2;
            this.layoutControlItemDescription.Size = new System.Drawing.Size(488, 62);
            this.layoutControlItemDescription.Text = "Mô tả";
            this.layoutControlItemDescription.TextSize = new System.Drawing.Size(85, 13);
            // 
            // layoutControl3
            // 
            this.layoutControl3.Controls.Add(this.xtraTabControl1);
            this.layoutControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl3.Location = new System.Drawing.Point(0, 386);
            this.layoutControl3.Name = "layoutControl3";
            this.layoutControl3.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(414, 128, 650, 400);
            this.layoutControl3.Root = this.layoutControlGroup2;
            this.layoutControl3.Size = new System.Drawing.Size(1161, 230);
            this.layoutControl3.TabIndex = 2;
            this.layoutControl3.Text = "layoutControl3";
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Location = new System.Drawing.Point(12, 12);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPageScreen;
            this.xtraTabControl1.Size = new System.Drawing.Size(1137, 206);
            this.xtraTabControl1.TabIndex = 4;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPageScreen,
            this.xtraTabPageReport,
            this.xtraTabPageOther});
            // 
            // xtraTabPageScreen
            // 
            this.xtraTabPageScreen.Controls.Add(this.gridControlScreen);
            this.xtraTabPageScreen.Name = "xtraTabPageScreen";
            this.xtraTabPageScreen.Size = new System.Drawing.Size(1132, 180);
            this.xtraTabPageScreen.Text = "Phân quyền trên màn hình";
            // 
            // gridControlScreen
            // 
            this.gridControlScreen.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlScreen.Location = new System.Drawing.Point(0, 0);
            this.gridControlScreen.MainView = this.gridViewScreen;
            this.gridControlScreen.Name = "gridControlScreen";
            this.gridControlScreen.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit2,
            this.repositoryItemCheckedComboBoxEdit2,
            this.repositoryItemComboBox2,
            this.repositoryItemTextEdit2});
            this.gridControlScreen.Size = new System.Drawing.Size(1132, 180);
            this.gridControlScreen.TabIndex = 2;
            this.gridControlScreen.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewScreen});
            // 
            // gridViewScreen
            // 
            this.gridViewScreen.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colNo3,
            this.colScreenName,
            this.colScreenNameEN,
            this.colSaveNewRight,
            this.colSaveChangeRight,
            this.colDeleteRight,
            this.colPrintRight,
            this.colImportRight,
            this.colExportRight,
            this.colScreenID,
            this.colOrdinal1});
            this.gridViewScreen.GridControl = this.gridControlScreen;
            this.gridViewScreen.Name = "gridViewScreen";
            this.gridViewScreen.OptionsSelection.MultiSelect = true;
            // 
            // colNo3
            // 
            this.colNo3.Caption = "No";
            this.colNo3.Name = "colNo3";
            this.colNo3.OptionsColumn.ReadOnly = true;
            this.colNo3.Visible = true;
            this.colNo3.VisibleIndex = 0;
            this.colNo3.Width = 38;
            // 
            // colScreenName
            // 
            this.colScreenName.Caption = "Tên màn hình";
            this.colScreenName.FieldName = "ScreenName";
            this.colScreenName.Name = "colScreenName";
            this.colScreenName.Visible = true;
            this.colScreenName.VisibleIndex = 1;
            this.colScreenName.Width = 176;
            // 
            // colScreenNameEN
            // 
            this.colScreenNameEN.Caption = "Screen Name";
            this.colScreenNameEN.FieldName = "ScreenNameEN";
            this.colScreenNameEN.Name = "colScreenNameEN";
            this.colScreenNameEN.Visible = true;
            this.colScreenNameEN.VisibleIndex = 2;
            this.colScreenNameEN.Width = 277;
            // 
            // colSaveNewRight
            // 
            this.colSaveNewRight.Caption = "Lưu mới";
            this.colSaveNewRight.FieldName = "SaveNewRight";
            this.colSaveNewRight.Name = "colSaveNewRight";
            this.colSaveNewRight.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            this.colSaveNewRight.Visible = true;
            this.colSaveNewRight.VisibleIndex = 3;
            // 
            // colSaveChangeRight
            // 
            this.colSaveChangeRight.Caption = "Lưu thay đổi";
            this.colSaveChangeRight.FieldName = "SaveChangeRight";
            this.colSaveChangeRight.Name = "colSaveChangeRight";
            this.colSaveChangeRight.Visible = true;
            this.colSaveChangeRight.VisibleIndex = 4;
            // 
            // colDeleteRight
            // 
            this.colDeleteRight.Caption = "Xóa";
            this.colDeleteRight.FieldName = "DeleteRight";
            this.colDeleteRight.Name = "colDeleteRight";
            this.colDeleteRight.Visible = true;
            this.colDeleteRight.VisibleIndex = 5;
            // 
            // colPrintRight
            // 
            this.colPrintRight.Caption = "In ấn";
            this.colPrintRight.FieldName = "PrintRight";
            this.colPrintRight.Name = "colPrintRight";
            this.colPrintRight.Visible = true;
            this.colPrintRight.VisibleIndex = 6;
            // 
            // colImportRight
            // 
            this.colImportRight.Caption = "Nhập dữ liệu";
            this.colImportRight.FieldName = "ImportRight";
            this.colImportRight.Name = "colImportRight";
            this.colImportRight.Visible = true;
            this.colImportRight.VisibleIndex = 7;
            // 
            // colExportRight
            // 
            this.colExportRight.Caption = "Xuất dữ liệu";
            this.colExportRight.FieldName = "ExportRight";
            this.colExportRight.Name = "colExportRight";
            this.colExportRight.Visible = true;
            this.colExportRight.VisibleIndex = 8;
            // 
            // colScreenID
            // 
            this.colScreenID.Caption = "Screen ID";
            this.colScreenID.FieldName = "ScreenID";
            this.colScreenID.Name = "colScreenID";
            // 
            // colOrdinal1
            // 
            this.colOrdinal1.Caption = "Ordinal";
            this.colOrdinal1.FieldName = "Ordinal";
            this.colOrdinal1.MinWidth = 21;
            this.colOrdinal1.Name = "colOrdinal1";
            this.colOrdinal1.ShowUnboundExpressionMenu = true;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AllowGrayed = true;
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.RadioGroupIndex = 0;
            this.repositoryItemCheckEdit2.ValueChecked = "1";
            this.repositoryItemCheckEdit2.ValueUnchecked = "0";
            // 
            // repositoryItemCheckedComboBoxEdit2
            // 
            this.repositoryItemCheckedComboBoxEdit2.AllowMultiSelect = true;
            this.repositoryItemCheckedComboBoxEdit2.AutoHeight = false;
            this.repositoryItemCheckedComboBoxEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCheckedComboBoxEdit2.Name = "repositoryItemCheckedComboBoxEdit2";
            // 
            // repositoryItemComboBox2
            // 
            this.repositoryItemComboBox2.AutoHeight = false;
            this.repositoryItemComboBox2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox2.Name = "repositoryItemComboBox2";
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // xtraTabPageReport
            // 
            this.xtraTabPageReport.Controls.Add(this.gridControlReport);
            this.xtraTabPageReport.Name = "xtraTabPageReport";
            this.xtraTabPageReport.Size = new System.Drawing.Size(1132, 180);
            this.xtraTabPageReport.Text = "Phân quyền trên báo cáo";
            // 
            // gridControlReport
            // 
            this.gridControlReport.DataSource = this.reportBindingSource;
            this.gridControlReport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlReport.Location = new System.Drawing.Point(0, 0);
            this.gridControlReport.MainView = this.gridViewReport;
            this.gridControlReport.Name = "gridControlReport";
            this.gridControlReport.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit3,
            this.repositoryItemCheckedComboBoxEdit3,
            this.repositoryItemComboBox3,
            this.repositoryItemTextEdit3});
            this.gridControlReport.Size = new System.Drawing.Size(1132, 180);
            this.gridControlReport.TabIndex = 2;
            this.gridControlReport.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewReport});
            // 
            // reportBindingSource
            // 
            this.reportBindingSource.DataMember = "Report";
            this.reportBindingSource.DataSource = this.wARHOUSE_HPDataSet;
            // 
            // gridViewReport
            // 
            this.gridViewReport.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colReportID,
            this.colReportName,
            this.colReportNameEN,
            this.colOrdinal,
            this.colNo4,
            this.colViewRight,
            this.colPrintRight2});
            this.gridViewReport.GridControl = this.gridControlReport;
            this.gridViewReport.Name = "gridViewReport";
            // 
            // colReportID
            // 
            this.colReportID.Caption = "Report ID";
            this.colReportID.FieldName = "ReportID";
            this.colReportID.Name = "colReportID";
            // 
            // colReportName
            // 
            this.colReportName.Caption = "Tên báo cáo";
            this.colReportName.FieldName = "ReportName";
            this.colReportName.Name = "colReportName";
            this.colReportName.Visible = true;
            this.colReportName.VisibleIndex = 1;
            this.colReportName.Width = 250;
            // 
            // colReportNameEN
            // 
            this.colReportNameEN.Caption = "Report Name EN";
            this.colReportNameEN.FieldName = "ReportNameEN";
            this.colReportNameEN.Name = "colReportNameEN";
            this.colReportNameEN.Visible = true;
            this.colReportNameEN.VisibleIndex = 2;
            this.colReportNameEN.Width = 250;
            // 
            // colOrdinal
            // 
            this.colOrdinal.Caption = "Ordinal";
            this.colOrdinal.FieldName = "Ordinal";
            this.colOrdinal.Name = "colOrdinal";
            this.colOrdinal.Width = 254;
            // 
            // colNo4
            // 
            this.colNo4.Caption = "No";
            this.colNo4.FieldName = "No";
            this.colNo4.Name = "colNo4";
            this.colNo4.Visible = true;
            this.colNo4.VisibleIndex = 0;
            this.colNo4.Width = 67;
            // 
            // colViewRight
            // 
            this.colViewRight.Caption = "View Right";
            this.colViewRight.FieldName = "ViewRight";
            this.colViewRight.Name = "colViewRight";
            this.colViewRight.Visible = true;
            this.colViewRight.VisibleIndex = 3;
            // 
            // colPrintRight2
            // 
            this.colPrintRight2.Caption = "Print Right";
            this.colPrintRight2.FieldName = "PrintRight";
            this.colPrintRight2.Name = "colPrintRight2";
            this.colPrintRight2.Visible = true;
            this.colPrintRight2.VisibleIndex = 4;
            // 
            // repositoryItemCheckEdit3
            // 
            this.repositoryItemCheckEdit3.AllowGrayed = true;
            this.repositoryItemCheckEdit3.AutoHeight = false;
            this.repositoryItemCheckEdit3.Name = "repositoryItemCheckEdit3";
            this.repositoryItemCheckEdit3.RadioGroupIndex = 0;
            this.repositoryItemCheckEdit3.ValueChecked = "1";
            this.repositoryItemCheckEdit3.ValueUnchecked = "0";
            // 
            // repositoryItemCheckedComboBoxEdit3
            // 
            this.repositoryItemCheckedComboBoxEdit3.AllowMultiSelect = true;
            this.repositoryItemCheckedComboBoxEdit3.AutoHeight = false;
            this.repositoryItemCheckedComboBoxEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCheckedComboBoxEdit3.Name = "repositoryItemCheckedComboBoxEdit3";
            // 
            // repositoryItemComboBox3
            // 
            this.repositoryItemComboBox3.AutoHeight = false;
            this.repositoryItemComboBox3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox3.Name = "repositoryItemComboBox3";
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // xtraTabPageOther
            // 
            this.xtraTabPageOther.Controls.Add(this.gridControlRight);
            this.xtraTabPageOther.Name = "xtraTabPageOther";
            this.xtraTabPageOther.Size = new System.Drawing.Size(1132, 180);
            this.xtraTabPageOther.Text = "Phân quyền khác";
            // 
            // gridControlRight
            // 
            this.gridControlRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlRight.Location = new System.Drawing.Point(0, 0);
            this.gridControlRight.MainView = this.gridViewRight;
            this.gridControlRight.Name = "gridControlRight";
            this.gridControlRight.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1,
            this.repositoryItemCheckedComboBoxEdit1,
            this.repositoryItemComboBox1,
            this.repositoryItemTextEdit1});
            this.gridControlRight.Size = new System.Drawing.Size(1132, 180);
            this.gridControlRight.TabIndex = 1;
            this.gridControlRight.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewRight});
            // 
            // gridViewRight
            // 
            this.gridViewRight.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colNo,
            this.colRightID,
            this.colRightName,
            this.colRightNameEN,
            this.colGrantRight,
            this.colOrdinal3});
            this.gridViewRight.GridControl = this.gridControlRight;
            this.gridViewRight.Name = "gridViewRight";
            // 
            // colNo
            // 
            this.colNo.Caption = "No";
            this.colNo.Name = "colNo";
            this.colNo.OptionsColumn.ReadOnly = true;
            this.colNo.Visible = true;
            this.colNo.VisibleIndex = 0;
            this.colNo.Width = 38;
            // 
            // colRightID
            // 
            this.colRightID.FieldName = "RightID";
            this.colRightID.Name = "colRightID";
            this.colRightID.Width = 119;
            // 
            // colRightName
            // 
            this.colRightName.FieldName = "RightName";
            this.colRightName.Name = "colRightName";
            this.colRightName.Visible = true;
            this.colRightName.VisibleIndex = 1;
            this.colRightName.Width = 176;
            // 
            // colRightNameEN
            // 
            this.colRightNameEN.FieldName = "RightNameEN";
            this.colRightNameEN.Name = "colRightNameEN";
            this.colRightNameEN.Visible = true;
            this.colRightNameEN.VisibleIndex = 2;
            this.colRightNameEN.Width = 277;
            // 
            // colGrantRight
            // 
            this.colGrantRight.Caption = "Cấp quyền";
            this.colGrantRight.FieldName = "GrantRight";
            this.colGrantRight.Name = "colGrantRight";
            this.colGrantRight.Visible = true;
            this.colGrantRight.VisibleIndex = 3;
            // 
            // colOrdinal3
            // 
            this.colOrdinal3.Caption = "Ordinal";
            this.colOrdinal3.FieldName = "Ordinal";
            this.colOrdinal3.MinWidth = 21;
            this.colOrdinal3.Name = "colOrdinal3";
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AllowGrayed = true;
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.RadioGroupIndex = 0;
            this.repositoryItemCheckEdit1.ValueChecked = "1";
            this.repositoryItemCheckEdit1.ValueUnchecked = "0";
            // 
            // repositoryItemCheckedComboBoxEdit1
            // 
            this.repositoryItemCheckedComboBoxEdit1.AllowMultiSelect = true;
            this.repositoryItemCheckedComboBoxEdit1.AutoHeight = false;
            this.repositoryItemCheckedComboBoxEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCheckedComboBoxEdit1.Name = "repositoryItemCheckedComboBoxEdit1";
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.AutoHeight = false;
            this.repositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3});
            this.layoutControlGroup2.Name = "Root";
            this.layoutControlGroup2.Size = new System.Drawing.Size(1161, 230);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.xtraTabControl1;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(1141, 210);
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // screenBindingSource
            // 
            this.screenBindingSource.DataMember = "Screen";
            this.screenBindingSource.DataSource = this.wARHOUSE_HPDataSet;
            // 
            // userTableAdapter
            // 
            this.userTableAdapter.ClearBeforeFill = true;
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // screenTableAdapter
            // 
            this.screenTableAdapter.ClearBeforeFill = true;
            // 
            // reportTableAdapter
            // 
            this.reportTableAdapter.ClearBeforeFill = true;
            // 
            // popupMenu1
            // 
            this.popupMenu1.Manager = this.barManager1;
            this.popupMenu1.Name = "popupMenu1";
            // 
            // svgImageCollection1
            // 
            this.svgImageCollection1.Add("close", "image://svgimages/outlook inspired/close.svg");
            // 
            // UserManagerControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl3);
            this.Controls.Add(this.layoutControl2);
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "UserManagerControl";
            this.Size = new System.Drawing.Size(1161, 616);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.userBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wARHOUSE_HPDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEditUserID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditFullName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditAdress.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditBranch.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPhone.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditEmail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditUsername.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPassword.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditActive.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditBlocked.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditRole.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditRePassword.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditPosition.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditDescription.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemUserId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemFullname)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemAdress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemBranch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPhone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPassword)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemUsername)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).EndInit();
            this.layoutControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPageScreen.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlScreen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewScreen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            this.xtraTabPageReport.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            this.xtraTabPageOther.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlRight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewRight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.screenBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.svgImageCollection1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraGrid.GridControl gridControlUser;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewUser;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControl layoutControl3;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageScreen;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageReport;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageOther;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraGrid.GridControl gridControlRight;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewRight;
        private DevExpress.XtraGrid.Columns.GridColumn colNo;
        private DevExpress.XtraGrid.Columns.GridColumn colRightID;
        private DevExpress.XtraGrid.Columns.GridColumn colRightName;
        private DevExpress.XtraGrid.Columns.GridColumn colRightNameEN;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit repositoryItemCheckedComboBoxEdit1;
        private System.Windows.Forms.BindingSource userBindingSource;
        private WARHOUSE_HPDataSet wARHOUSE_HPDataSet;
        private DevExpress.XtraGrid.Columns.GridColumn colUserID;
        private DevExpress.XtraGrid.Columns.GridColumn colUserName;
        private DevExpress.XtraGrid.Columns.GridColumn colLoginName;
        private WARHOUSE_HPDataSetTableAdapters.UserTableAdapter userTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colNo2;
        private DevExpress.XtraEditors.TextEdit textEditUserID;
        private DevExpress.XtraEditors.TextEdit textEditFullName;
        private DevExpress.XtraEditors.TextEdit textEditAdress;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditBranch;
        private DevExpress.XtraEditors.TextEdit textEditPhone;
        private DevExpress.XtraEditors.TextEdit textEditEmail;
        private DevExpress.XtraEditors.TextEdit textEditUsername;
        private DevExpress.XtraEditors.TextEdit textEditPassword;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemUserId;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemFullname;
        private DevExpress.XtraEditors.CheckEdit checkEditActive;
        private DevExpress.XtraEditors.CheckEdit checkEditBlocked;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
        private DevExpress.XtraGrid.Columns.GridColumn colGrantRight;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraGrid.GridControl gridControlScreen;
        private System.Windows.Forms.BindingSource screenBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewScreen;
        private DevExpress.XtraGrid.Columns.GridColumn colNo3;
        private DevExpress.XtraGrid.Columns.GridColumn colScreenName;
        private DevExpress.XtraGrid.Columns.GridColumn colScreenNameEN;
        private DevExpress.XtraGrid.Columns.GridColumn colSaveNewRight;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit repositoryItemCheckedComboBoxEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private WARHOUSE_HPDataSetTableAdapters.ScreenTableAdapter screenTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colSaveChangeRight;
        private DevExpress.XtraGrid.Columns.GridColumn colDeleteRight;
        private DevExpress.XtraGrid.Columns.GridColumn colPrintRight;
        private DevExpress.XtraGrid.Columns.GridColumn colImportRight;
        private DevExpress.XtraGrid.Columns.GridColumn colExportRight;
        private DevExpress.XtraGrid.Columns.GridColumn colScreenID;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraGrid.Columns.GridColumn colOrdinal1;
        private System.Windows.Forms.BindingSource reportBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colOrdinal3;
        private WARHOUSE_HPDataSetTableAdapters.ReportTableAdapter reportTableAdapter;
        private DevExpress.XtraGrid.GridControl gridControlReport;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewReport;
        private DevExpress.XtraGrid.Columns.GridColumn colReportID;
        private DevExpress.XtraGrid.Columns.GridColumn colReportName;
        private DevExpress.XtraGrid.Columns.GridColumn colReportNameEN;
        private DevExpress.XtraGrid.Columns.GridColumn colOrdinal;
        private DevExpress.XtraGrid.Columns.GridColumn colNo4;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit repositoryItemCheckedComboBoxEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn colViewRight;
        private DevExpress.XtraGrid.Columns.GridColumn colPrintRight2;
        private DevExpress.XtraBars.PopupMenu popupMenu1;
        private DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel windowsUIButtonPanel1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraGrid.Columns.GridColumn colActive;
        private DevExpress.XtraGrid.Columns.GridColumn colBlocked;
        private DevExpress.XtraGrid.Columns.GridColumn colBranchID;
        private DevExpress.XtraGrid.Columns.GridColumn colBranchName;
        private DevExpress.XtraGrid.Columns.GridColumn colAdress;
        private DevExpress.XtraGrid.Columns.GridColumn colEmail;
        private DevExpress.XtraGrid.Columns.GridColumn colPosition;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditRole;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraEditors.TextEdit textEditRePassword;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditPosition;
        private DevExpress.XtraGrid.Columns.GridColumn colRole;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit4;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit5;
        private DevExpress.XtraEditors.MemoEdit textEditDescription;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemAdress;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemBranch;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemPhone;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemEmail;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemPassword;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemUsername;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemDescription;
        private DevExpress.Utils.SvgImageCollection svgImageCollection1;
    }
}
