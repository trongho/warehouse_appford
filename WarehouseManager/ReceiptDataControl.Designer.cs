﻿
namespace WarehouseManager
{
    partial class ReceiptDataControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions1 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReceiptDataControl));
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions2 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions3 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions4 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions5 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions6 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions7 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions8 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions9 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition1 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition2 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition3 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition4 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition5 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition6 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition7 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition1 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition2 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition3 = new DevExpress.XtraLayout.RowDefinition();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.windowsUIButtonPanel1 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControl3 = new DevExpress.XtraLayout.LayoutControl();
            this.gridControlWRDataDetail = new DevExpress.XtraGrid.GridControl();
            this.wRDataDetailBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.wARHOUSE_HPDataSet1 = new WarehouseManager.WARHOUSE_HPDataSet1();
            this.gridViewWRDataDetail = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colWRDNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrdinal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGoodsID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGoodsName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIDCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocationID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalQuantity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalGoods = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantityOrg = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalQuantityOrg = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalGoodsOrg = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocationIDOrg = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedDateTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEditerID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEditedDateTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPackingVolume = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantityByPack = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantityByItem = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNote = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colScanOption = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPackingQuantity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReceiptStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSLPart = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.textEditTotalQuantity = new DevExpress.XtraEditors.TextEdit();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textEditWRDNumber = new DevExpress.XtraEditors.TextEdit();
            this.dateEditWRDDate = new DevExpress.XtraEditors.DateEdit();
            this.textEdit4 = new DevExpress.XtraEditors.TextEdit();
            this.textEditTotalQuantityOrg = new DevExpress.XtraEditors.TextEdit();
            this.comboBoxEditHandlingStatus = new DevExpress.XtraEditors.ComboBoxEdit();
            this.simpleButtonOn = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonOff = new DevExpress.XtraEditors.SimpleButton();
            this.textEditTotalGoodsOrg = new DevExpress.XtraEditors.TextEdit();
            this.textEditTotalGood = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemWRDNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemWRDDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemHandlingStatus = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemTotalQuantityOrg = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemTotalQuantity = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.wRDataDetailTableAdapter = new WarehouseManager.WARHOUSE_HPDataSet1TableAdapters.WRDataDetailTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).BeginInit();
            this.layoutControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlWRDataDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wRDataDetailBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wARHOUSE_HPDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewWRDataDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalQuantity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditWRDNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditWRDDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditWRDDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalQuantityOrg.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditHandlingStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalGoodsOrg.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalGood.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWRDNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWRDDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemHandlingStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalQuantityOrg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalQuantity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.windowsUIButtonPanel1);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(684, 208, 812, 495);
            this.layoutControl1.Root = this.Root;
            this.layoutControl1.Size = new System.Drawing.Size(1514, 46);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // windowsUIButtonPanel1
            // 
            this.windowsUIButtonPanel1.AllowGlyphSkinning = false;
            this.windowsUIButtonPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            windowsUIButtonImageOptions1.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions1.Image")));
            windowsUIButtonImageOptions1.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions2.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions2.Image")));
            windowsUIButtonImageOptions2.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions3.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions3.Image")));
            windowsUIButtonImageOptions3.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions4.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions4.Image")));
            windowsUIButtonImageOptions4.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions5.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions5.Image")));
            windowsUIButtonImageOptions5.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions6.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions6.Image")));
            windowsUIButtonImageOptions6.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions7.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions7.Image")));
            windowsUIButtonImageOptions7.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions8.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions8.Image")));
            windowsUIButtonImageOptions8.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions9.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions9.Image")));
            windowsUIButtonImageOptions9.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            this.windowsUIButtonPanel1.Buttons.AddRange(new DevExpress.XtraEditors.ButtonPanel.IBaseButton[] {
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F2) Thêm mới", true, windowsUIButtonImageOptions1, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "new", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F3) Lưu", true, windowsUIButtonImageOptions2, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "save", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F4) Xóa", true, windowsUIButtonImageOptions3, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "delete", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F5) Tìm kiếm", true, windowsUIButtonImageOptions4, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "search", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F6) Làm mới", true, windowsUIButtonImageOptions5, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "refesh", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F7) Nhập dữ liệu", true, windowsUIButtonImageOptions6, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "import", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F8) Xuất dữ liệu", true, windowsUIButtonImageOptions7, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "export", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F9) In ấn", true, windowsUIButtonImageOptions8, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "print", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F10) Thoát", true, windowsUIButtonImageOptions9, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "close", -1, false)});
            this.windowsUIButtonPanel1.ContentAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.windowsUIButtonPanel1.Font = new System.Drawing.Font("Tahoma", 10F);
            this.windowsUIButtonPanel1.Location = new System.Drawing.Point(2, 2);
            this.windowsUIButtonPanel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.windowsUIButtonPanel1.Name = "windowsUIButtonPanel1";
            this.windowsUIButtonPanel1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.windowsUIButtonPanel1.Size = new System.Drawing.Size(1510, 32);
            this.windowsUIButtonPanel1.TabIndex = 5;
            this.windowsUIButtonPanel1.Text = "windowsUIButtonPanel1";
            this.windowsUIButtonPanel1.UseButtonBackgroundImages = false;
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.Root.Name = "Root";
            this.Root.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.Root.Size = new System.Drawing.Size(1514, 46);
            this.Root.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.windowsUIButtonPanel1;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(1514, 46);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControl3
            // 
            this.layoutControl3.Controls.Add(this.gridControlWRDataDetail);
            this.layoutControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl3.Location = new System.Drawing.Point(0, 172);
            this.layoutControl3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.layoutControl3.Name = "layoutControl3";
            this.layoutControl3.Root = this.layoutControlGroup2;
            this.layoutControl3.Size = new System.Drawing.Size(1514, 598);
            this.layoutControl3.TabIndex = 2;
            this.layoutControl3.Text = "layoutControl3";
            // 
            // gridControlWRDataDetail
            // 
            this.gridControlWRDataDetail.DataSource = this.wRDataDetailBindingSource;
            this.gridControlWRDataDetail.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gridControlWRDataDetail.Location = new System.Drawing.Point(12, 12);
            this.gridControlWRDataDetail.MainView = this.gridViewWRDataDetail;
            this.gridControlWRDataDetail.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridControlWRDataDetail.Name = "gridControlWRDataDetail";
            this.gridControlWRDataDetail.Size = new System.Drawing.Size(1490, 574);
            this.gridControlWRDataDetail.TabIndex = 4;
            this.gridControlWRDataDetail.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewWRDataDetail});
            // 
            // wRDataDetailBindingSource
            // 
            this.wRDataDetailBindingSource.DataMember = "WRDataDetail";
            this.wRDataDetailBindingSource.DataSource = this.wARHOUSE_HPDataSet1;
            // 
            // wARHOUSE_HPDataSet1
            // 
            this.wARHOUSE_HPDataSet1.DataSetName = "WARHOUSE_HPDataSet1";
            this.wARHOUSE_HPDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridViewWRDataDetail
            // 
            this.gridViewWRDataDetail.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridViewWRDataDetail.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridViewWRDataDetail.Appearance.HeaderPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.gridViewWRDataDetail.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colWRDNumber,
            this.colOrdinal,
            this.colGoodsID,
            this.colGoodsName,
            this.colIDCode,
            this.colLocationID,
            this.colQuantity,
            this.colTotalQuantity,
            this.colTotalGoods,
            this.colQuantityOrg,
            this.colTotalQuantityOrg,
            this.colTotalGoodsOrg,
            this.colLocationIDOrg,
            this.colCreatorID,
            this.colCreatedDateTime,
            this.colEditerID,
            this.colEditedDateTime,
            this.colStatus,
            this.colPackingVolume,
            this.colQuantityByPack,
            this.colQuantityByItem,
            this.colNote,
            this.colScanOption,
            this.colPackingQuantity,
            this.colNo,
            this.colReceiptStatus,
            this.colSLPart,
            this.gridColumn1});
            this.gridViewWRDataDetail.GridControl = this.gridControlWRDataDetail;
            this.gridViewWRDataDetail.Name = "gridViewWRDataDetail";
            this.gridViewWRDataDetail.OptionsView.ColumnHeaderAutoHeight = DevExpress.Utils.DefaultBoolean.True;
            this.gridViewWRDataDetail.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colQuantity, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colWRDNumber
            // 
            this.colWRDNumber.Caption = "CONVEYANCE NUMBER";
            this.colWRDNumber.FieldName = "WRDNumber";
            this.colWRDNumber.MinWidth = 35;
            this.colWRDNumber.Name = "colWRDNumber";
            this.colWRDNumber.Width = 94;
            // 
            // colOrdinal
            // 
            this.colOrdinal.Caption = "Số thứ tự";
            this.colOrdinal.FieldName = "Ordinal";
            this.colOrdinal.MinWidth = 24;
            this.colOrdinal.Name = "colOrdinal";
            this.colOrdinal.Width = 94;
            // 
            // colGoodsID
            // 
            this.colGoodsID.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colGoodsID.AppearanceCell.Options.UseFont = true;
            this.colGoodsID.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colGoodsID.AppearanceHeader.Options.UseFont = true;
            this.colGoodsID.Caption = "PART NUMBER";
            this.colGoodsID.FieldName = "GoodsID";
            this.colGoodsID.MinWidth = 100;
            this.colGoodsID.Name = "colGoodsID";
            this.colGoodsID.Visible = true;
            this.colGoodsID.VisibleIndex = 1;
            this.colGoodsID.Width = 100;
            // 
            // colGoodsName
            // 
            this.colGoodsName.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colGoodsName.AppearanceCell.Options.UseFont = true;
            this.colGoodsName.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colGoodsName.AppearanceHeader.Options.UseFont = true;
            this.colGoodsName.Caption = "PART NAME";
            this.colGoodsName.FieldName = "GoodsName";
            this.colGoodsName.MinWidth = 120;
            this.colGoodsName.Name = "colGoodsName";
            this.colGoodsName.Visible = true;
            this.colGoodsName.VisibleIndex = 2;
            this.colGoodsName.Width = 120;
            // 
            // colIDCode
            // 
            this.colIDCode.Caption = "Mã định danh";
            this.colIDCode.FieldName = "IDCode";
            this.colIDCode.MinWidth = 24;
            this.colIDCode.Name = "colIDCode";
            this.colIDCode.Width = 133;
            // 
            // colLocationID
            // 
            this.colLocationID.Caption = "Vị trí ";
            this.colLocationID.FieldName = "LocationID";
            this.colLocationID.MinWidth = 24;
            this.colLocationID.Name = "colLocationID";
            this.colLocationID.Width = 94;
            // 
            // colQuantity
            // 
            this.colQuantity.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colQuantity.AppearanceCell.Options.UseFont = true;
            this.colQuantity.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colQuantity.AppearanceHeader.Options.UseFont = true;
            this.colQuantity.Caption = "SL thực nhận";
            this.colQuantity.DisplayFormat.FormatString = "G29";
            this.colQuantity.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colQuantity.FieldName = "Quantity";
            this.colQuantity.MinWidth = 60;
            this.colQuantity.Name = "colQuantity";
            this.colQuantity.Visible = true;
            this.colQuantity.VisibleIndex = 4;
            this.colQuantity.Width = 88;
            // 
            // colTotalQuantity
            // 
            this.colTotalQuantity.Caption = "Số lượng thực tế( mặt hàng)";
            this.colTotalQuantity.DisplayFormat.FormatString = "G29";
            this.colTotalQuantity.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colTotalQuantity.FieldName = "TotalQuantity";
            this.colTotalQuantity.MinWidth = 24;
            this.colTotalQuantity.Name = "colTotalQuantity";
            this.colTotalQuantity.Width = 133;
            // 
            // colTotalGoods
            // 
            this.colTotalGoods.Caption = "Số lượng thực tế hàng hóa";
            this.colTotalGoods.DisplayFormat.FormatString = "G29";
            this.colTotalGoods.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colTotalGoods.FieldName = "TotalGoods";
            this.colTotalGoods.MinWidth = 24;
            this.colTotalGoods.Name = "colTotalGoods";
            this.colTotalGoods.Width = 94;
            // 
            // colQuantityOrg
            // 
            this.colQuantityOrg.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colQuantityOrg.AppearanceCell.Options.UseFont = true;
            this.colQuantityOrg.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colQuantityOrg.AppearanceHeader.Options.UseFont = true;
            this.colQuantityOrg.Caption = "ASN QTY";
            this.colQuantityOrg.DisplayFormat.FormatString = "G29";
            this.colQuantityOrg.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colQuantityOrg.FieldName = "QuantityOrg";
            this.colQuantityOrg.MinWidth = 60;
            this.colQuantityOrg.Name = "colQuantityOrg";
            this.colQuantityOrg.Visible = true;
            this.colQuantityOrg.VisibleIndex = 3;
            this.colQuantityOrg.Width = 88;
            // 
            // colTotalQuantityOrg
            // 
            this.colTotalQuantityOrg.Caption = "Số lượng yêu cầu( mặt hàng)";
            this.colTotalQuantityOrg.DisplayFormat.FormatString = "G29";
            this.colTotalQuantityOrg.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colTotalQuantityOrg.FieldName = "TotalQuantityOrg";
            this.colTotalQuantityOrg.MinWidth = 24;
            this.colTotalQuantityOrg.Name = "colTotalQuantityOrg";
            this.colTotalQuantityOrg.Width = 94;
            // 
            // colTotalGoodsOrg
            // 
            this.colTotalGoodsOrg.Caption = "Số lượng yêu cầu hàng hóa";
            this.colTotalGoodsOrg.DisplayFormat.FormatString = "G29";
            this.colTotalGoodsOrg.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colTotalGoodsOrg.FieldName = "TotalGoodsOrg";
            this.colTotalGoodsOrg.MinWidth = 24;
            this.colTotalGoodsOrg.Name = "colTotalGoodsOrg";
            this.colTotalGoodsOrg.Width = 94;
            // 
            // colLocationIDOrg
            // 
            this.colLocationIDOrg.Caption = "Vị trí";
            this.colLocationIDOrg.FieldName = "LocationIDOrg";
            this.colLocationIDOrg.MinWidth = 24;
            this.colLocationIDOrg.Name = "colLocationIDOrg";
            this.colLocationIDOrg.Width = 94;
            // 
            // colCreatorID
            // 
            this.colCreatorID.Caption = "Người tạo";
            this.colCreatorID.FieldName = "CreatorID";
            this.colCreatorID.MinWidth = 24;
            this.colCreatorID.Name = "colCreatorID";
            this.colCreatorID.Width = 94;
            // 
            // colCreatedDateTime
            // 
            this.colCreatedDateTime.Caption = "Ngày tạo";
            this.colCreatedDateTime.FieldName = "CreatedDateTime";
            this.colCreatedDateTime.MinWidth = 24;
            this.colCreatedDateTime.Name = "colCreatedDateTime";
            this.colCreatedDateTime.Width = 94;
            // 
            // colEditerID
            // 
            this.colEditerID.Caption = "Người sửa";
            this.colEditerID.FieldName = "EditerID";
            this.colEditerID.MinWidth = 24;
            this.colEditerID.Name = "colEditerID";
            this.colEditerID.Width = 94;
            // 
            // colEditedDateTime
            // 
            this.colEditedDateTime.Caption = "Thời gian kết thúc nhận hàng";
            this.colEditedDateTime.DisplayFormat.FormatString = "dd/MM/yy HH:mm";
            this.colEditedDateTime.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colEditedDateTime.FieldName = "EditedDateTime";
            this.colEditedDateTime.MinWidth = 100;
            this.colEditedDateTime.Name = "colEditedDateTime";
            this.colEditedDateTime.Visible = true;
            this.colEditedDateTime.VisibleIndex = 10;
            this.colEditedDateTime.Width = 100;
            // 
            // colStatus
            // 
            this.colStatus.Caption = "Trạng thái";
            this.colStatus.FieldName = "Status";
            this.colStatus.MinWidth = 24;
            this.colStatus.Name = "colStatus";
            this.colStatus.Width = 94;
            // 
            // colPackingVolume
            // 
            this.colPackingVolume.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colPackingVolume.AppearanceCell.Options.UseFont = true;
            this.colPackingVolume.Caption = "Quy cách đóng gói";
            this.colPackingVolume.DisplayFormat.FormatString = "G29";
            this.colPackingVolume.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colPackingVolume.FieldName = "PackingVolume";
            this.colPackingVolume.MinWidth = 24;
            this.colPackingVolume.Name = "colPackingVolume";
            this.colPackingVolume.Width = 133;
            // 
            // colQuantityByPack
            // 
            this.colQuantityByPack.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colQuantityByPack.AppearanceCell.Options.UseFont = true;
            this.colQuantityByPack.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colQuantityByPack.AppearanceHeader.Options.UseFont = true;
            this.colQuantityByPack.Caption = "ASN NBR CNT";
            this.colQuantityByPack.DisplayFormat.FormatString = "G29";
            this.colQuantityByPack.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colQuantityByPack.FieldName = "QuantityByPack";
            this.colQuantityByPack.MinWidth = 60;
            this.colQuantityByPack.Name = "colQuantityByPack";
            this.colQuantityByPack.Visible = true;
            this.colQuantityByPack.VisibleIndex = 5;
            this.colQuantityByPack.Width = 88;
            // 
            // colQuantityByItem
            // 
            this.colQuantityByItem.Caption = "QuantityByItem";
            this.colQuantityByItem.DisplayFormat.FormatString = "G29";
            this.colQuantityByItem.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colQuantityByItem.FieldName = "QuantityByItem";
            this.colQuantityByItem.MinWidth = 24;
            this.colQuantityByItem.Name = "colQuantityByItem";
            this.colQuantityByItem.Width = 133;
            // 
            // colNote
            // 
            this.colNote.Caption = "Ghi chú khác";
            this.colNote.FieldName = "Note";
            this.colNote.MinWidth = 120;
            this.colNote.Name = "colNote";
            this.colNote.Visible = true;
            this.colNote.VisibleIndex = 11;
            this.colNote.Width = 120;
            // 
            // colScanOption
            // 
            this.colScanOption.FieldName = "ScanOption";
            this.colScanOption.MinWidth = 24;
            this.colScanOption.Name = "colScanOption";
            this.colScanOption.Width = 94;
            // 
            // colPackingQuantity
            // 
            this.colPackingQuantity.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colPackingQuantity.AppearanceCell.Options.UseFont = true;
            this.colPackingQuantity.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colPackingQuantity.AppearanceHeader.Options.UseFont = true;
            this.colPackingQuantity.Caption = "NBR CNT THỰC NHẬN";
            this.colPackingQuantity.DisplayFormat.FormatString = "G29";
            this.colPackingQuantity.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colPackingQuantity.FieldName = "PackingQuantity";
            this.colPackingQuantity.MinWidth = 60;
            this.colPackingQuantity.Name = "colPackingQuantity";
            this.colPackingQuantity.Visible = true;
            this.colPackingQuantity.VisibleIndex = 6;
            this.colPackingQuantity.Width = 88;
            // 
            // colNo
            // 
            this.colNo.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colNo.AppearanceCell.Options.UseFont = true;
            this.colNo.AppearanceCell.Options.UseTextOptions = true;
            this.colNo.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNo.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colNo.AppearanceHeader.Options.UseFont = true;
            this.colNo.AppearanceHeader.Options.UseTextOptions = true;
            this.colNo.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNo.Caption = "NO";
            this.colNo.FieldName = "No";
            this.colNo.MinWidth = 60;
            this.colNo.Name = "colNo";
            this.colNo.Visible = true;
            this.colNo.VisibleIndex = 0;
            this.colNo.Width = 69;
            // 
            // colReceiptStatus
            // 
            this.colReceiptStatus.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colReceiptStatus.AppearanceCell.Options.UseFont = true;
            this.colReceiptStatus.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colReceiptStatus.AppearanceHeader.Options.UseFont = true;
            this.colReceiptStatus.Caption = "Whse";
            this.colReceiptStatus.FieldName = "ReceiptStatus";
            this.colReceiptStatus.MinWidth = 60;
            this.colReceiptStatus.Name = "colReceiptStatus";
            this.colReceiptStatus.Visible = true;
            this.colReceiptStatus.VisibleIndex = 7;
            this.colReceiptStatus.Width = 69;
            // 
            // colSLPart
            // 
            this.colSLPart.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colSLPart.AppearanceCell.Options.UseFont = true;
            this.colSLPart.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colSLPart.AppearanceHeader.Options.UseFont = true;
            this.colSLPart.Caption = "SL Part";
            this.colSLPart.FieldName = "SLPart";
            this.colSLPart.MinWidth = 60;
            this.colSLPart.Name = "colSLPart";
            this.colSLPart.Visible = true;
            this.colSLPart.VisibleIndex = 8;
            this.colSLPart.Width = 69;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Thời gian bắt đầu nhận hàng";
            this.gridColumn1.DisplayFormat.FormatString = "dd/MM/yy HH:mm";
            this.gridColumn1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.gridColumn1.FieldName = "StartedDateTime";
            this.gridColumn1.MinWidth = 100;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 9;
            this.gridColumn1.Width = 100;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem12});
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(1514, 598);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.gridControlWRDataDetail;
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(1494, 578);
            this.layoutControlItem12.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem12.TextVisible = false;
            // 
            // textEditTotalQuantity
            // 
            this.textEditTotalQuantity.Location = new System.Drawing.Point(977, 47);
            this.textEditTotalQuantity.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textEditTotalQuantity.Name = "textEditTotalQuantity";
            this.textEditTotalQuantity.Size = new System.Drawing.Size(161, 22);
            this.textEditTotalQuantity.StyleController = this.layoutControl2;
            this.textEditTotalQuantity.TabIndex = 16;
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.label2);
            this.layoutControl2.Controls.Add(this.pictureBox1);
            this.layoutControl2.Controls.Add(this.label1);
            this.layoutControl2.Controls.Add(this.textEditWRDNumber);
            this.layoutControl2.Controls.Add(this.dateEditWRDDate);
            this.layoutControl2.Controls.Add(this.textEdit4);
            this.layoutControl2.Controls.Add(this.textEditTotalQuantityOrg);
            this.layoutControl2.Controls.Add(this.textEditTotalQuantity);
            this.layoutControl2.Controls.Add(this.comboBoxEditHandlingStatus);
            this.layoutControl2.Controls.Add(this.simpleButtonOn);
            this.layoutControl2.Controls.Add(this.simpleButtonOff);
            this.layoutControl2.Controls.Add(this.textEditTotalGoodsOrg);
            this.layoutControl2.Controls.Add(this.textEditTotalGood);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl2.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3});
            this.layoutControl2.Location = new System.Drawing.Point(0, 46);
            this.layoutControl2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(475, 228, 812, 500);
            this.layoutControl2.Root = this.layoutControlGroup1;
            this.layoutControl2.Size = new System.Drawing.Size(1514, 126);
            this.layoutControl2.TabIndex = 1;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // label2
            // 
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(391, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 31);
            this.label2.TabIndex = 23;
            this.label2.Text = "(*)";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureBox1.Location = new System.Drawing.Point(471, 12);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(157, 102);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 22;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(616, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(261, 31);
            this.label1.TabIndex = 7;
            this.label1.Text = "(*)";
            // 
            // textEditWRDNumber
            // 
            this.textEditWRDNumber.Location = new System.Drawing.Point(132, 12);
            this.textEditWRDNumber.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textEditWRDNumber.Name = "textEditWRDNumber";
            this.textEditWRDNumber.Size = new System.Drawing.Size(255, 22);
            this.textEditWRDNumber.StyleController = this.layoutControl2;
            this.textEditWRDNumber.TabIndex = 4;
            // 
            // dateEditWRDDate
            // 
            this.dateEditWRDDate.EditValue = null;
            this.dateEditWRDDate.Location = new System.Drawing.Point(132, 47);
            this.dateEditWRDDate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dateEditWRDDate.Name = "dateEditWRDDate";
            this.dateEditWRDDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditWRDDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditWRDDate.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditWRDDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditWRDDate.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEditWRDDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditWRDDate.Size = new System.Drawing.Size(255, 22);
            this.dateEditWRDDate.StyleController = this.layoutControl2;
            this.dateEditWRDDate.TabIndex = 8;
            // 
            // textEdit4
            // 
            this.textEdit4.Location = new System.Drawing.Point(2963, 12);
            this.textEdit4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textEdit4.Name = "textEdit4";
            this.textEdit4.Size = new System.Drawing.Size(62, 22);
            this.textEdit4.StyleController = this.layoutControl2;
            this.textEdit4.TabIndex = 11;
            // 
            // textEditTotalQuantityOrg
            // 
            this.textEditTotalQuantityOrg.Location = new System.Drawing.Point(977, 12);
            this.textEditTotalQuantityOrg.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textEditTotalQuantityOrg.Name = "textEditTotalQuantityOrg";
            this.textEditTotalQuantityOrg.Size = new System.Drawing.Size(161, 22);
            this.textEditTotalQuantityOrg.StyleController = this.layoutControl2;
            this.textEditTotalQuantityOrg.TabIndex = 12;
            // 
            // comboBoxEditHandlingStatus
            // 
            this.comboBoxEditHandlingStatus.Location = new System.Drawing.Point(132, 82);
            this.comboBoxEditHandlingStatus.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboBoxEditHandlingStatus.Name = "comboBoxEditHandlingStatus";
            this.comboBoxEditHandlingStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditHandlingStatus.Size = new System.Drawing.Size(255, 22);
            this.comboBoxEditHandlingStatus.StyleController = this.layoutControl2;
            this.comboBoxEditHandlingStatus.TabIndex = 17;
            // 
            // simpleButtonOn
            // 
            this.simpleButtonOn.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.simpleButtonOn.Location = new System.Drawing.Point(857, 82);
            this.simpleButtonOn.Margin = new System.Windows.Forms.Padding(0);
            this.simpleButtonOn.Name = "simpleButtonOn";
            this.simpleButtonOn.Size = new System.Drawing.Size(281, 23);
            this.simpleButtonOn.StyleController = this.layoutControl2;
            this.simpleButtonOn.TabIndex = 18;
            this.simpleButtonOn.Text = "Mở";
            // 
            // simpleButtonOff
            // 
            this.simpleButtonOff.Location = new System.Drawing.Point(1221, 82);
            this.simpleButtonOff.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.simpleButtonOff.Name = "simpleButtonOff";
            this.simpleButtonOff.Size = new System.Drawing.Size(281, 23);
            this.simpleButtonOff.StyleController = this.layoutControl2;
            this.simpleButtonOff.TabIndex = 19;
            this.simpleButtonOff.Text = "Tắt";
            // 
            // textEditTotalGoodsOrg
            // 
            this.textEditTotalGoodsOrg.Location = new System.Drawing.Point(1341, 12);
            this.textEditTotalGoodsOrg.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textEditTotalGoodsOrg.Name = "textEditTotalGoodsOrg";
            this.textEditTotalGoodsOrg.Size = new System.Drawing.Size(161, 22);
            this.textEditTotalGoodsOrg.StyleController = this.layoutControl2;
            this.textEditTotalGoodsOrg.TabIndex = 24;
            // 
            // textEditTotalGood
            // 
            this.textEditTotalGood.Location = new System.Drawing.Point(1341, 47);
            this.textEditTotalGood.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textEditTotalGood.Name = "textEditTotalGood";
            this.textEditTotalGood.Size = new System.Drawing.Size(161, 22);
            this.textEditTotalGood.StyleController = this.layoutControl2;
            this.textEditTotalGood.TabIndex = 25;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.label1;
            this.layoutControlItem3.Location = new System.Drawing.Point(604, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.OptionsTableLayoutItem.ColumnIndex = 3;
            this.layoutControlItem3.Size = new System.Drawing.Size(265, 35);
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem7,
            this.layoutControlItemWRDNumber,
            this.layoutControlItemWRDDate,
            this.layoutControlItemHandlingStatus,
            this.layoutControlItem5,
            this.layoutControlItem4,
            this.layoutControlItemTotalQuantityOrg,
            this.layoutControlItemTotalQuantity,
            this.layoutControlItem6,
            this.layoutControlItem8,
            this.layoutControlItem2});
            this.layoutControlGroup1.LayoutMode = DevExpress.XtraLayout.Utils.LayoutMode.Table;
            this.layoutControlGroup1.Name = "Root";
            columnDefinition1.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition1.Width = 25.37405669048221D;
            columnDefinition2.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition2.Width = 5.3669406912529807D;
            columnDefinition3.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition3.Width = 10.733881382505961D;
            columnDefinition4.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition4.Width = 15D;
            columnDefinition5.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition5.Width = 19D;
            columnDefinition6.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition6.Width = 5.2939083529638467D;
            columnDefinition7.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition7.Width = 19D;
            this.layoutControlGroup1.OptionsTableLayoutGroup.ColumnDefinitions.AddRange(new DevExpress.XtraLayout.ColumnDefinition[] {
            columnDefinition1,
            columnDefinition2,
            columnDefinition3,
            columnDefinition4,
            columnDefinition5,
            columnDefinition6,
            columnDefinition7});
            rowDefinition1.Height = 33.333333333333336D;
            rowDefinition1.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition2.Height = 33.333333333333336D;
            rowDefinition2.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition3.Height = 33.333333333333336D;
            rowDefinition3.SizeType = System.Windows.Forms.SizeType.Percent;
            this.layoutControlGroup1.OptionsTableLayoutGroup.RowDefinitions.AddRange(new DevExpress.XtraLayout.RowDefinition[] {
            rowDefinition1,
            rowDefinition2,
            rowDefinition3});
            this.layoutControlGroup1.Size = new System.Drawing.Size(1514, 126);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.pictureBox1;
            this.layoutControlItem7.Location = new System.Drawing.Point(459, 0);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.OptionsTableLayoutItem.ColumnIndex = 2;
            this.layoutControlItem7.OptionsTableLayoutItem.RowSpan = 3;
            this.layoutControlItem7.Size = new System.Drawing.Size(161, 106);
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlItemWRDNumber
            // 
            this.layoutControlItemWRDNumber.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.layoutControlItemWRDNumber.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItemWRDNumber.Control = this.textEditWRDNumber;
            this.layoutControlItemWRDNumber.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemWRDNumber.Name = "layoutControlItemWRDNumber";
            this.layoutControlItemWRDNumber.Size = new System.Drawing.Size(379, 35);
            this.layoutControlItemWRDNumber.Text = "Conveyance number";
            this.layoutControlItemWRDNumber.TextSize = new System.Drawing.Size(116, 16);
            // 
            // layoutControlItemWRDDate
            // 
            this.layoutControlItemWRDDate.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.layoutControlItemWRDDate.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItemWRDDate.Control = this.dateEditWRDDate;
            this.layoutControlItemWRDDate.Location = new System.Drawing.Point(0, 35);
            this.layoutControlItemWRDDate.Name = "layoutControlItemWRDDate";
            this.layoutControlItemWRDDate.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItemWRDDate.Size = new System.Drawing.Size(379, 35);
            this.layoutControlItemWRDDate.Text = "Ngày dữ liệu";
            this.layoutControlItemWRDDate.TextSize = new System.Drawing.Size(116, 17);
            // 
            // layoutControlItemHandlingStatus
            // 
            this.layoutControlItemHandlingStatus.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.layoutControlItemHandlingStatus.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItemHandlingStatus.Control = this.comboBoxEditHandlingStatus;
            this.layoutControlItemHandlingStatus.Location = new System.Drawing.Point(0, 70);
            this.layoutControlItemHandlingStatus.Name = "layoutControlItemHandlingStatus";
            this.layoutControlItemHandlingStatus.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlItemHandlingStatus.Size = new System.Drawing.Size(379, 36);
            this.layoutControlItemHandlingStatus.Text = "HandlingStatus";
            this.layoutControlItemHandlingStatus.TextSize = new System.Drawing.Size(116, 16);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.label2;
            this.layoutControlItem5.Location = new System.Drawing.Point(379, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.OptionsTableLayoutItem.ColumnIndex = 1;
            this.layoutControlItem5.Size = new System.Drawing.Size(80, 35);
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.simpleButtonOff;
            this.layoutControlItem4.Location = new System.Drawing.Point(1209, 70);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.OptionsTableLayoutItem.ColumnIndex = 6;
            this.layoutControlItem4.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlItem4.Size = new System.Drawing.Size(285, 36);
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItemTotalQuantityOrg
            // 
            this.layoutControlItemTotalQuantityOrg.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.layoutControlItemTotalQuantityOrg.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItemTotalQuantityOrg.Control = this.textEditTotalQuantityOrg;
            this.layoutControlItemTotalQuantityOrg.Location = new System.Drawing.Point(845, 0);
            this.layoutControlItemTotalQuantityOrg.Name = "layoutControlItemTotalQuantityOrg";
            this.layoutControlItemTotalQuantityOrg.OptionsTableLayoutItem.ColumnIndex = 4;
            this.layoutControlItemTotalQuantityOrg.Size = new System.Drawing.Size(285, 35);
            this.layoutControlItemTotalQuantityOrg.Text = "∑ SL yêu cầu";
            this.layoutControlItemTotalQuantityOrg.TextSize = new System.Drawing.Size(116, 17);
            // 
            // layoutControlItemTotalQuantity
            // 
            this.layoutControlItemTotalQuantity.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.layoutControlItemTotalQuantity.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItemTotalQuantity.Control = this.textEditTotalQuantity;
            this.layoutControlItemTotalQuantity.Location = new System.Drawing.Point(845, 35);
            this.layoutControlItemTotalQuantity.Name = "layoutControlItemTotalQuantity";
            this.layoutControlItemTotalQuantity.OptionsTableLayoutItem.ColumnIndex = 4;
            this.layoutControlItemTotalQuantity.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItemTotalQuantity.Size = new System.Drawing.Size(285, 35);
            this.layoutControlItemTotalQuantity.Text = "∑ SL Thực Nhận";
            this.layoutControlItemTotalQuantity.TextSize = new System.Drawing.Size(116, 17);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.layoutControlItem6.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem6.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem6.AppearanceItemCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.layoutControlItem6.Control = this.textEditTotalGoodsOrg;
            this.layoutControlItem6.Location = new System.Drawing.Point(1209, 0);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.OptionsTableLayoutItem.ColumnIndex = 6;
            this.layoutControlItem6.Size = new System.Drawing.Size(285, 35);
            this.layoutControlItem6.Text = "∑ Mã yêu cầu";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(116, 17);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.layoutControlItem8.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem8.Control = this.textEditTotalGood;
            this.layoutControlItem8.Location = new System.Drawing.Point(1209, 35);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.OptionsTableLayoutItem.ColumnIndex = 6;
            this.layoutControlItem8.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItem8.Size = new System.Drawing.Size(285, 35);
            this.layoutControlItem8.Text = "∑ Mã thực nhận";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(116, 17);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.simpleButtonOn;
            this.layoutControlItem2.Location = new System.Drawing.Point(845, 70);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.OptionsTableLayoutItem.ColumnIndex = 4;
            this.layoutControlItem2.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlItem2.Size = new System.Drawing.Size(285, 36);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // wRDataDetailTableAdapter
            // 
            this.wRDataDetailTableAdapter.ClearBeforeFill = true;
            // 
            // ReceiptDataControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl3);
            this.Controls.Add(this.layoutControl2);
            this.Controls.Add(this.layoutControl1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "ReceiptDataControl";
            this.Size = new System.Drawing.Size(1514, 770);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).EndInit();
            this.layoutControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlWRDataDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wRDataDetailBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wARHOUSE_HPDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewWRDataDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalQuantity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditWRDNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditWRDDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditWRDDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalQuantityOrg.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditHandlingStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalGoodsOrg.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalGood.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWRDNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWRDDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemHandlingStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalQuantityOrg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalQuantity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraLayout.LayoutControl layoutControl3;
        private DevExpress.XtraGrid.GridControl gridControlWRDataDetail;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewWRDataDetail;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraEditors.TextEdit textEditTotalQuantity;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.TextEdit textEditWRDNumber;
        private DevExpress.XtraEditors.DateEdit dateEditWRDDate;
        private DevExpress.XtraEditors.TextEdit textEdit4;
        private DevExpress.XtraEditors.TextEdit textEditTotalQuantityOrg;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditHandlingStatus;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private System.Windows.Forms.BindingSource wRDataDetailBindingSource;
        private WARHOUSE_HPDataSet1 wARHOUSE_HPDataSet1;
        private DevExpress.XtraGrid.Columns.GridColumn colWRDNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colOrdinal;
        private DevExpress.XtraGrid.Columns.GridColumn colGoodsID;
        private DevExpress.XtraGrid.Columns.GridColumn colGoodsName;
        private DevExpress.XtraGrid.Columns.GridColumn colIDCode;
        private DevExpress.XtraGrid.Columns.GridColumn colLocationID;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantity;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalQuantity;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalGoods;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantityOrg;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalQuantityOrg;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalGoodsOrg;
        private DevExpress.XtraGrid.Columns.GridColumn colLocationIDOrg;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatorID;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedDateTime;
        private DevExpress.XtraGrid.Columns.GridColumn colEditerID;
        private DevExpress.XtraGrid.Columns.GridColumn colEditedDateTime;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colPackingVolume;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantityByPack;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantityByItem;
        private DevExpress.XtraGrid.Columns.GridColumn colNote;
        private DevExpress.XtraGrid.Columns.GridColumn colScanOption;
        private WARHOUSE_HPDataSet1TableAdapters.WRDataDetailTableAdapter wRDataDetailTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colPackingQuantity;
        private DevExpress.XtraEditors.SimpleButton simpleButtonOn;
        private DevExpress.XtraEditors.SimpleButton simpleButtonOff;
        private DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel windowsUIButtonPanel1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemWRDNumber;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemWRDDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemHandlingStatus;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraGrid.Columns.GridColumn colNo;
        private DevExpress.XtraGrid.Columns.GridColumn colReceiptStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colSLPart;
        private DevExpress.XtraEditors.TextEdit textEditTotalGoodsOrg;
        private DevExpress.XtraEditors.TextEdit textEditTotalGood;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemTotalQuantityOrg;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemTotalQuantity;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
    }
}
