﻿using DevExpress.XtraBars.Docking2010;
using DevExpress.XtraBars.Docking2010.Views.WindowsUI;
using DevExpress.XtraGrid.Views.Grid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WarehouseManager.Models;
using WarehouseManager.Repository;
using webservice.Entities;

namespace WarehouseManager
{
    public partial class frmSearchWRR : Form
    {
        HandlingStatusRepository handlingStatusRepository;
        BranchRepository branchRepository;
        WRRHeaderRepository wRRHeaderRepository;
        public frmSearchWRR()
        {
            InitializeComponent();
            this.Load += frmSearchWRR_Load;
            handlingStatusRepository = new HandlingStatusRepository();
            branchRepository = new BranchRepository();
            wRRHeaderRepository = new WRRHeaderRepository();


        }

        private void frmSearchWRR_Load(object sender, EventArgs e)
        {
            popupMenuSelectBranch();
            popupMenuSelectHandlingStatus();
            sbLoadDataForGridWRRHeaderAsync();
            gridViewWRRHeader.DoubleClick += dataGridView_CellDoubleClick;
            simpleButtonFilter.Click += filterButton_Click;
            windowsUIButtonPanel1.ButtonClick += button_Click;
            windowsUIButtonPanel1.Cursor = Cursors.Hand;
            this.KeyDown +=new KeyEventHandler(Form1_KeyDown);

            if (int.Parse(DateTime.Now.Day.ToString()) == 1)
            {
                dateEditFromDate.Text = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day).ToString("yyyy-MM-dd");
            }
            else
            {
                dateEditFromDate.Text = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day - 1).ToString("yyyy-MM-dd");
            }
            dateEditToDate.Text = DateTime.Now.ToString("yyyy-MM-dd");

        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F5)
            {
                sbLoadDataForGridWRRHeaderAsync();
            }
            if (e.KeyCode == Keys.F10)
            {
                this.Close();
            }
        }

        private void OnMouseEnterButton1(object sender, EventArgs e)
        {
            windowsUIButtonPanel1.BackColor = SystemColors.ButtonHighlight; // or Color.Red or whatever you want       
        }

        private async Task popupMenuSelectHandlingStatus()
        {
            List<HandlingStatus> handlingStatuses = await handlingStatusRepository.GetAll();
            foreach (HandlingStatus handlingStatus in handlingStatuses)
            {
                comboBoxEditHandlingStatus.Properties.Items.Add(handlingStatus.HandlingStatusName);
            }
            comboBoxEditHandlingStatus.SelectedIndex = 1;
        }

        private async Task popupMenuSelectBranch()
        {
            List<Branch> branches = await branchRepository.getAllBranchAsync();
            foreach (Branch branch in branches)
            {
                comboBoxEditBranch.Properties.Items.Add(branch.BranchName);
            }
            comboBoxEditBranch.SelectedIndex = 1;
        }

        private async Task sbLoadDataForGridWRRHeaderAsync()
        {
            List<WRRHeader> wRRHeaders = (await wRRHeaderRepository.GetAll()).OrderByDescending(o => o.CreatedDate.Value.Date).ToList();
            gridControlWRRHeader.DataSource =wRRHeaders;
        }

        private async void dataGridView_CellDoubleClick(object sender, EventArgs e)
        {
            if (gridViewWRRHeader.RowCount > 0)
            {
                WRRHeader wRRHeader = await wRRHeaderRepository.GetUnderID((string)(sender as GridView).GetFocusedRowCellValue("WRRNumber"));
                ReceiptRequisitionControl.selectedWRRNumber = (string)(sender as GridView).GetFocusedRowCellValue("WRRNumber");
                ReceiptRequisitionControl.selectedReferenceNumber = (string)(sender as GridView).GetFocusedRowCellValue("ReferenceNumber");
                ReceiptRequisitionControl.selectedWRRDate = (DateTime)(sender as GridView).GetFocusedRowCellValue("WRRDate");
                ReceiptRequisitionControl.selectedNote = (string)(sender as GridView).GetFocusedRowCellValue("Note");
                ReceiptRequisitionControl.selectedHandlingID = wRRHeader.HandlingStatusID;
                ReceiptRequisitionControl.selectedHandlingName = wRRHeader.HandlingStatusName;
                ReceiptRequisitionControl.selectedBranchID = $"{int.Parse(wRRHeader.BranchID) + 1:D1}";
                ReceiptRequisitionControl.selectedBranchName = wRRHeader.BranchName;
                this.Close();
            }
        }

        private async void filterButton_Click(Object sender,EventArgs e)
        {
            String branchID = "<>";
            String handlingStatusID = "<>";
            if (!comboBoxEditBranch.SelectedItem.ToString().Equals("<>"))
            {
                branchID = $"{int.Parse(comboBoxEditBranch.SelectedIndex.ToString()) - 1:D3}";
            }
            if (!comboBoxEditHandlingStatus.SelectedItem.ToString().Equals("<>"))
            {
                handlingStatusID = (comboBoxEditHandlingStatus.SelectedIndex - 1).ToString();
            }
            DateTime fromDate = DateTime.Parse(dateEditFromDate.Text);
            DateTime toDate = DateTime.Parse(dateEditToDate.Text);

            List<WRRHeader> wRRHeaders = new List<WRRHeader>();

            wRRHeaders = await wRRHeaderRepository.FilterFinal(branchID, fromDate, toDate, handlingStatusID);
            gridControlWRRHeader.DataSource = wRRHeaders;

        }

        public async void button_Click(object sender, EventArgs e)
        {
            ButtonEventArgs eArg = (DevExpress.XtraBars.Docking2010.ButtonEventArgs)e;
            String tag = eArg.Button.Properties.Tag.ToString();
            switch (tag)
            {
                case "refesh":
                    sbLoadDataForGridWRRHeaderAsync();
                    break;
                case "close":
                    this.Close();
                    break;
            }

        }   

    }
}
