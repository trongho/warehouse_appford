﻿
namespace WarehouseManager
{
    partial class TallySheetControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions1 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TallySheetControl));
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions2 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions3 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions4 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions5 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions6 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions7 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions8 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions9 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition1 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition2 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition3 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition4 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition5 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition6 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition7 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition1 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition2 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition3 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition8 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition9 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition10 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition4 = new DevExpress.XtraLayout.RowDefinition();
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.windowsUIButtonPanel1 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.label1 = new System.Windows.Forms.Label();
            this.textEditTSNumber = new DevExpress.XtraEditors.TextEdit();
            this.dateEditTallyDate = new DevExpress.XtraEditors.DateEdit();
            this.textEditReferenceNumber = new DevExpress.XtraEditors.TextEdit();
            this.comboBoxEditWarehouse = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEditPriceType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEditHandlingStatus = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEditTotalQuantity = new DevExpress.XtraEditors.TextEdit();
            this.textEditTotalAmountIncludedVAT = new DevExpress.XtraEditors.TextEdit();
            this.textEditTotalAmount = new DevExpress.XtraEditors.TextEdit();
            this.textEditNote = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemTSNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemTallyDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemReferenceNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemWarehouse = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemPriceType = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemHandlingStatus = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemTotalQuantity = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemTotalAmountIncludedVAT = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemTotalAmount = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemNote = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.radioButtonScanOption = new System.Windows.Forms.RadioButton();
            this.textEditChecker1 = new DevExpress.XtraEditors.TextEdit();
            this.textEditChecker2 = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemChecker1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemChecker2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControl3 = new DevExpress.XtraLayout.LayoutControl();
            this.gridControlData = new DevExpress.XtraGrid.GridControl();
            this.tSDetailBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.wARHOUSE_HPDataSet4 = new WarehouseManager.WARHOUSE_HPDataSet4();
            this.gridViewData = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTSNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrdinal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGoodsID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGoodsName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTallyUnitID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnitRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStockUnitID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPriceIncludedVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPriceExcludedVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDiscount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDiscountAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVATAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAmountExcludedVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAmountIncludedVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpiryDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSerialNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNote = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.tSHeaderBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tSHeaderTableAdapter = new WarehouseManager.WARHOUSE_HPDataSet4TableAdapters.TSHeaderTableAdapter();
            this.tSDetailTableAdapter = new WarehouseManager.WARHOUSE_HPDataSet4TableAdapters.TSDetailTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTSNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTallyDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTallyDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditReferenceNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditWarehouse.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditPriceType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditHandlingStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalQuantity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalAmountIncludedVAT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalAmount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditNote.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTSNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTallyDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemReferenceNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWarehouse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPriceType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemHandlingStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalQuantity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalAmountIncludedVAT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemNote)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditChecker1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditChecker2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemChecker1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemChecker2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).BeginInit();
            this.layoutControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tSDetailBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wARHOUSE_HPDataSet4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tSHeaderBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.windowsUIButtonPanel1);
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 0);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(403, 0, 650, 400);
            this.dataLayoutControl1.Root = this.Root;
            this.dataLayoutControl1.Size = new System.Drawing.Size(1311, 36);
            this.dataLayoutControl1.TabIndex = 0;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // windowsUIButtonPanel1
            // 
            this.windowsUIButtonPanel1.AllowGlyphSkinning = false;
            this.windowsUIButtonPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            windowsUIButtonImageOptions1.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions1.Image")));
            windowsUIButtonImageOptions1.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions2.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions2.Image")));
            windowsUIButtonImageOptions2.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions3.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions3.Image")));
            windowsUIButtonImageOptions3.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions4.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions4.Image")));
            windowsUIButtonImageOptions4.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions5.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions5.Image")));
            windowsUIButtonImageOptions5.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions6.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions6.Image")));
            windowsUIButtonImageOptions6.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions7.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions7.Image")));
            windowsUIButtonImageOptions7.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions8.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions8.Image")));
            windowsUIButtonImageOptions8.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions9.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions9.Image")));
            windowsUIButtonImageOptions9.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            this.windowsUIButtonPanel1.Buttons.AddRange(new DevExpress.XtraEditors.ButtonPanel.IBaseButton[] {
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F2) Thêm mới", true, windowsUIButtonImageOptions1, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "new", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F3) Lưu", true, windowsUIButtonImageOptions2, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "save", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F4) Xóa", true, windowsUIButtonImageOptions3, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "delete", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F5) Tìm kiếm", true, windowsUIButtonImageOptions4, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "search", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F6) Làm mới", true, windowsUIButtonImageOptions5, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "refesh", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F7) Nhập dữ liệu", true, windowsUIButtonImageOptions6, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "import", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F8) Xuất dữ liệu", true, windowsUIButtonImageOptions7, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "export", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F9) In ấn", true, windowsUIButtonImageOptions8, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "print", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F10) Thoát", true, windowsUIButtonImageOptions9, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "close", -1, false)});
            this.windowsUIButtonPanel1.ContentAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.windowsUIButtonPanel1.Location = new System.Drawing.Point(2, 2);
            this.windowsUIButtonPanel1.Name = "windowsUIButtonPanel1";
            this.windowsUIButtonPanel1.Padding = new System.Windows.Forms.Padding(5);
            this.windowsUIButtonPanel1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.windowsUIButtonPanel1.Size = new System.Drawing.Size(1307, 32);
            this.windowsUIButtonPanel1.TabIndex = 6;
            this.windowsUIButtonPanel1.Text = "windowsUIButtonPanel1";
            this.windowsUIButtonPanel1.UseButtonBackgroundImages = false;
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.Root.Name = "Root";
            this.Root.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.Root.Size = new System.Drawing.Size(1311, 36);
            this.Root.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.windowsUIButtonPanel1;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(1311, 36);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.label1);
            this.layoutControl1.Controls.Add(this.textEditTSNumber);
            this.layoutControl1.Controls.Add(this.dateEditTallyDate);
            this.layoutControl1.Controls.Add(this.textEditReferenceNumber);
            this.layoutControl1.Controls.Add(this.comboBoxEditWarehouse);
            this.layoutControl1.Controls.Add(this.comboBoxEditPriceType);
            this.layoutControl1.Controls.Add(this.comboBoxEditHandlingStatus);
            this.layoutControl1.Controls.Add(this.textEditTotalQuantity);
            this.layoutControl1.Controls.Add(this.textEditTotalAmountIncludedVAT);
            this.layoutControl1.Controls.Add(this.textEditTotalAmount);
            this.layoutControl1.Controls.Add(this.textEditNote);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl1.Location = new System.Drawing.Point(0, 36);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(716, 0, 650, 400);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(1311, 98);
            this.layoutControl1.TabIndex = 1;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // label1
            // 
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(227, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(139, 22);
            this.label1.TabIndex = 8;
            this.label1.Text = "(*)";
            // 
            // textEditTSNumber
            // 
            this.textEditTSNumber.Location = new System.Drawing.Point(94, 12);
            this.textEditTSNumber.Name = "textEditTSNumber";
            this.textEditTSNumber.Size = new System.Drawing.Size(129, 20);
            this.textEditTSNumber.StyleController = this.layoutControl1;
            this.textEditTSNumber.TabIndex = 4;
            // 
            // dateEditTallyDate
            // 
            this.dateEditTallyDate.EditValue = null;
            this.dateEditTallyDate.Location = new System.Drawing.Point(94, 38);
            this.dateEditTallyDate.Name = "dateEditTallyDate";
            this.dateEditTallyDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditTallyDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditTallyDate.Size = new System.Drawing.Size(129, 20);
            this.dateEditTallyDate.StyleController = this.layoutControl1;
            this.dateEditTallyDate.TabIndex = 9;
            // 
            // textEditReferenceNumber
            // 
            this.textEditReferenceNumber.Location = new System.Drawing.Point(452, 12);
            this.textEditReferenceNumber.Name = "textEditReferenceNumber";
            this.textEditReferenceNumber.Size = new System.Drawing.Size(129, 20);
            this.textEditReferenceNumber.StyleController = this.layoutControl1;
            this.textEditReferenceNumber.TabIndex = 10;
            // 
            // comboBoxEditWarehouse
            // 
            this.comboBoxEditWarehouse.Location = new System.Drawing.Point(452, 38);
            this.comboBoxEditWarehouse.Name = "comboBoxEditWarehouse";
            this.comboBoxEditWarehouse.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditWarehouse.Size = new System.Drawing.Size(129, 20);
            this.comboBoxEditWarehouse.StyleController = this.layoutControl1;
            this.comboBoxEditWarehouse.TabIndex = 11;
            // 
            // comboBoxEditPriceType
            // 
            this.comboBoxEditPriceType.Location = new System.Drawing.Point(810, 12);
            this.comboBoxEditPriceType.Name = "comboBoxEditPriceType";
            this.comboBoxEditPriceType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditPriceType.Size = new System.Drawing.Size(129, 20);
            this.comboBoxEditPriceType.StyleController = this.layoutControl1;
            this.comboBoxEditPriceType.TabIndex = 12;
            // 
            // comboBoxEditHandlingStatus
            // 
            this.comboBoxEditHandlingStatus.Location = new System.Drawing.Point(810, 38);
            this.comboBoxEditHandlingStatus.Name = "comboBoxEditHandlingStatus";
            this.comboBoxEditHandlingStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditHandlingStatus.Size = new System.Drawing.Size(129, 20);
            this.comboBoxEditHandlingStatus.StyleController = this.layoutControl1;
            this.comboBoxEditHandlingStatus.TabIndex = 13;
            // 
            // textEditTotalQuantity
            // 
            this.textEditTotalQuantity.Location = new System.Drawing.Point(1176, 12);
            this.textEditTotalQuantity.Name = "textEditTotalQuantity";
            this.textEditTotalQuantity.Size = new System.Drawing.Size(123, 20);
            this.textEditTotalQuantity.StyleController = this.layoutControl1;
            this.textEditTotalQuantity.TabIndex = 14;
            // 
            // textEditTotalAmountIncludedVAT
            // 
            this.textEditTotalAmountIncludedVAT.Location = new System.Drawing.Point(1176, 38);
            this.textEditTotalAmountIncludedVAT.Name = "textEditTotalAmountIncludedVAT";
            this.textEditTotalAmountIncludedVAT.Size = new System.Drawing.Size(123, 20);
            this.textEditTotalAmountIncludedVAT.StyleController = this.layoutControl1;
            this.textEditTotalAmountIncludedVAT.TabIndex = 15;
            // 
            // textEditTotalAmount
            // 
            this.textEditTotalAmount.Location = new System.Drawing.Point(1176, 64);
            this.textEditTotalAmount.Name = "textEditTotalAmount";
            this.textEditTotalAmount.Size = new System.Drawing.Size(123, 20);
            this.textEditTotalAmount.StyleController = this.layoutControl1;
            this.textEditTotalAmount.TabIndex = 16;
            // 
            // textEditNote
            // 
            this.textEditNote.Location = new System.Drawing.Point(94, 64);
            this.textEditNote.Name = "textEditNote";
            this.textEditNote.Size = new System.Drawing.Size(988, 20);
            this.textEditNote.StyleController = this.layoutControl1;
            this.textEditNote.TabIndex = 17;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemTSNumber,
            this.layoutControlItem3,
            this.layoutControlItemTallyDate,
            this.layoutControlItemReferenceNumber,
            this.layoutControlItemWarehouse,
            this.layoutControlItemPriceType,
            this.layoutControlItemHandlingStatus,
            this.layoutControlItemTotalQuantity,
            this.layoutControlItemTotalAmountIncludedVAT,
            this.layoutControlItemTotalAmount,
            this.layoutControlItemNote});
            this.layoutControlGroup1.LayoutMode = DevExpress.XtraLayout.Utils.LayoutMode.Table;
            this.layoutControlGroup1.Name = "Root";
            columnDefinition1.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition1.Width = 16.666666666666668D;
            columnDefinition2.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition2.Width = 11.111111111111111D;
            columnDefinition3.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition3.Width = 16.666666666666668D;
            columnDefinition4.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition4.Width = 11.111111111111111D;
            columnDefinition5.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition5.Width = 16.666666666666668D;
            columnDefinition6.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition6.Width = 11.111111111111111D;
            columnDefinition7.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition7.Width = 16.666666666666668D;
            this.layoutControlGroup1.OptionsTableLayoutGroup.ColumnDefinitions.AddRange(new DevExpress.XtraLayout.ColumnDefinition[] {
            columnDefinition1,
            columnDefinition2,
            columnDefinition3,
            columnDefinition4,
            columnDefinition5,
            columnDefinition6,
            columnDefinition7});
            rowDefinition1.Height = 33.333333333333336D;
            rowDefinition1.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition2.Height = 33.333333333333336D;
            rowDefinition2.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition3.Height = 33.333333333333336D;
            rowDefinition3.SizeType = System.Windows.Forms.SizeType.Percent;
            this.layoutControlGroup1.OptionsTableLayoutGroup.RowDefinitions.AddRange(new DevExpress.XtraLayout.RowDefinition[] {
            rowDefinition1,
            rowDefinition2,
            rowDefinition3});
            this.layoutControlGroup1.Size = new System.Drawing.Size(1311, 98);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItemTSNumber
            // 
            this.layoutControlItemTSNumber.Control = this.textEditTSNumber;
            this.layoutControlItemTSNumber.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemTSNumber.Name = "layoutControlItemTSNumber";
            this.layoutControlItemTSNumber.Size = new System.Drawing.Size(215, 26);
            this.layoutControlItemTSNumber.Text = "Số phiếu kiểm kê";
            this.layoutControlItemTSNumber.TextSize = new System.Drawing.Size(79, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.label1;
            this.layoutControlItem3.Location = new System.Drawing.Point(215, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.OptionsTableLayoutItem.ColumnIndex = 1;
            this.layoutControlItem3.Size = new System.Drawing.Size(143, 26);
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItemTallyDate
            // 
            this.layoutControlItemTallyDate.Control = this.dateEditTallyDate;
            this.layoutControlItemTallyDate.Location = new System.Drawing.Point(0, 26);
            this.layoutControlItemTallyDate.Name = "layoutControlItemTallyDate";
            this.layoutControlItemTallyDate.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItemTallyDate.Size = new System.Drawing.Size(215, 26);
            this.layoutControlItemTallyDate.Text = "Ngày kiểm kê";
            this.layoutControlItemTallyDate.TextSize = new System.Drawing.Size(79, 13);
            // 
            // layoutControlItemReferenceNumber
            // 
            this.layoutControlItemReferenceNumber.Control = this.textEditReferenceNumber;
            this.layoutControlItemReferenceNumber.Location = new System.Drawing.Point(358, 0);
            this.layoutControlItemReferenceNumber.Name = "layoutControlItemReferenceNumber";
            this.layoutControlItemReferenceNumber.OptionsTableLayoutItem.ColumnIndex = 2;
            this.layoutControlItemReferenceNumber.Size = new System.Drawing.Size(215, 26);
            this.layoutControlItemReferenceNumber.Text = "Số tham chiếu";
            this.layoutControlItemReferenceNumber.TextSize = new System.Drawing.Size(79, 13);
            // 
            // layoutControlItemWarehouse
            // 
            this.layoutControlItemWarehouse.Control = this.comboBoxEditWarehouse;
            this.layoutControlItemWarehouse.Location = new System.Drawing.Point(358, 26);
            this.layoutControlItemWarehouse.Name = "layoutControlItemWarehouse";
            this.layoutControlItemWarehouse.OptionsTableLayoutItem.ColumnIndex = 2;
            this.layoutControlItemWarehouse.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItemWarehouse.Size = new System.Drawing.Size(215, 26);
            this.layoutControlItemWarehouse.Text = "Kho kiểm kê";
            this.layoutControlItemWarehouse.TextSize = new System.Drawing.Size(79, 13);
            // 
            // layoutControlItemPriceType
            // 
            this.layoutControlItemPriceType.Control = this.comboBoxEditPriceType;
            this.layoutControlItemPriceType.Location = new System.Drawing.Point(716, 0);
            this.layoutControlItemPriceType.Name = "layoutControlItemPriceType";
            this.layoutControlItemPriceType.OptionsTableLayoutItem.ColumnIndex = 4;
            this.layoutControlItemPriceType.Size = new System.Drawing.Size(215, 26);
            this.layoutControlItemPriceType.Text = "Loại giá";
            this.layoutControlItemPriceType.TextSize = new System.Drawing.Size(79, 13);
            // 
            // layoutControlItemHandlingStatus
            // 
            this.layoutControlItemHandlingStatus.Control = this.comboBoxEditHandlingStatus;
            this.layoutControlItemHandlingStatus.Location = new System.Drawing.Point(716, 26);
            this.layoutControlItemHandlingStatus.Name = "layoutControlItemHandlingStatus";
            this.layoutControlItemHandlingStatus.OptionsTableLayoutItem.ColumnIndex = 4;
            this.layoutControlItemHandlingStatus.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItemHandlingStatus.Size = new System.Drawing.Size(215, 26);
            this.layoutControlItemHandlingStatus.Text = "Trạng thái xử lý";
            this.layoutControlItemHandlingStatus.TextSize = new System.Drawing.Size(79, 13);
            // 
            // layoutControlItemTotalQuantity
            // 
            this.layoutControlItemTotalQuantity.Control = this.textEditTotalQuantity;
            this.layoutControlItemTotalQuantity.Location = new System.Drawing.Point(1074, 0);
            this.layoutControlItemTotalQuantity.Name = "layoutControlItemTotalQuantity";
            this.layoutControlItemTotalQuantity.OptionsTableLayoutItem.ColumnIndex = 6;
            this.layoutControlItemTotalQuantity.Padding = new DevExpress.XtraLayout.Utils.Padding(10, 2, 2, 2);
            this.layoutControlItemTotalQuantity.Size = new System.Drawing.Size(217, 26);
            this.layoutControlItemTotalQuantity.Text = "Σ Lượng:";
            this.layoutControlItemTotalQuantity.TextSize = new System.Drawing.Size(79, 13);
            // 
            // layoutControlItemTotalAmountIncludedVAT
            // 
            this.layoutControlItemTotalAmountIncludedVAT.Control = this.textEditTotalAmountIncludedVAT;
            this.layoutControlItemTotalAmountIncludedVAT.Location = new System.Drawing.Point(1074, 26);
            this.layoutControlItemTotalAmountIncludedVAT.Name = "layoutControlItemTotalAmountIncludedVAT";
            this.layoutControlItemTotalAmountIncludedVAT.OptionsTableLayoutItem.ColumnIndex = 6;
            this.layoutControlItemTotalAmountIncludedVAT.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItemTotalAmountIncludedVAT.Padding = new DevExpress.XtraLayout.Utils.Padding(10, 2, 2, 2);
            this.layoutControlItemTotalAmountIncludedVAT.Size = new System.Drawing.Size(217, 26);
            this.layoutControlItemTotalAmountIncludedVAT.Text = "Σ Thuế:";
            this.layoutControlItemTotalAmountIncludedVAT.TextSize = new System.Drawing.Size(79, 13);
            // 
            // layoutControlItemTotalAmount
            // 
            this.layoutControlItemTotalAmount.Control = this.textEditTotalAmount;
            this.layoutControlItemTotalAmount.Location = new System.Drawing.Point(1074, 52);
            this.layoutControlItemTotalAmount.Name = "layoutControlItemTotalAmount";
            this.layoutControlItemTotalAmount.OptionsTableLayoutItem.ColumnIndex = 6;
            this.layoutControlItemTotalAmount.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlItemTotalAmount.Padding = new DevExpress.XtraLayout.Utils.Padding(10, 2, 2, 2);
            this.layoutControlItemTotalAmount.Size = new System.Drawing.Size(217, 26);
            this.layoutControlItemTotalAmount.Text = "Σ Tiền:";
            this.layoutControlItemTotalAmount.TextSize = new System.Drawing.Size(79, 13);
            // 
            // layoutControlItemNote
            // 
            this.layoutControlItemNote.Control = this.textEditNote;
            this.layoutControlItemNote.Location = new System.Drawing.Point(0, 52);
            this.layoutControlItemNote.Name = "layoutControlItemNote";
            this.layoutControlItemNote.OptionsTableLayoutItem.ColumnSpan = 6;
            this.layoutControlItemNote.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlItemNote.Size = new System.Drawing.Size(1074, 26);
            this.layoutControlItemNote.Text = "Ghi chú";
            this.layoutControlItemNote.TextSize = new System.Drawing.Size(79, 13);
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.radioButtonScanOption);
            this.layoutControl2.Controls.Add(this.textEditChecker1);
            this.layoutControl2.Controls.Add(this.textEditChecker2);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.layoutControl2.Location = new System.Drawing.Point(0, 629);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.Root = this.layoutControlGroup2;
            this.layoutControl2.Size = new System.Drawing.Size(1311, 29);
            this.layoutControl2.TabIndex = 2;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // radioButtonScanOption
            // 
            this.radioButtonScanOption.Location = new System.Drawing.Point(2, 2);
            this.radioButtonScanOption.Name = "radioButtonScanOption";
            this.radioButtonScanOption.Size = new System.Drawing.Size(433, 25);
            this.radioButtonScanOption.TabIndex = 4;
            this.radioButtonScanOption.TabStop = true;
            this.radioButtonScanOption.Text = "Sử dụng máy quét mã vạch";
            this.radioButtonScanOption.UseVisualStyleBackColor = true;
            // 
            // textEditChecker1
            // 
            this.textEditChecker1.Location = new System.Drawing.Point(520, 2);
            this.textEditChecker1.Name = "textEditChecker1";
            this.textEditChecker1.Size = new System.Drawing.Size(352, 20);
            this.textEditChecker1.StyleController = this.layoutControl2;
            this.textEditChecker1.TabIndex = 5;
            // 
            // textEditChecker2
            // 
            this.textEditChecker2.Location = new System.Drawing.Point(957, 2);
            this.textEditChecker2.Name = "textEditChecker2";
            this.textEditChecker2.Size = new System.Drawing.Size(352, 20);
            this.textEditChecker2.StyleController = this.layoutControl2;
            this.textEditChecker2.TabIndex = 6;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItemChecker1,
            this.layoutControlItemChecker2});
            this.layoutControlGroup2.LayoutMode = DevExpress.XtraLayout.Utils.LayoutMode.Table;
            this.layoutControlGroup2.Name = "Root";
            columnDefinition8.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition8.Width = 33.333333333333336D;
            columnDefinition9.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition9.Width = 33.333333333333336D;
            columnDefinition10.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition10.Width = 33.333333333333336D;
            this.layoutControlGroup2.OptionsTableLayoutGroup.ColumnDefinitions.AddRange(new DevExpress.XtraLayout.ColumnDefinition[] {
            columnDefinition8,
            columnDefinition9,
            columnDefinition10});
            rowDefinition4.Height = 100D;
            rowDefinition4.SizeType = System.Windows.Forms.SizeType.Percent;
            this.layoutControlGroup2.OptionsTableLayoutGroup.RowDefinitions.AddRange(new DevExpress.XtraLayout.RowDefinition[] {
            rowDefinition4});
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup2.Size = new System.Drawing.Size(1311, 29);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.radioButtonScanOption;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(437, 29);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItemChecker1
            // 
            this.layoutControlItemChecker1.Control = this.textEditChecker1;
            this.layoutControlItemChecker1.Location = new System.Drawing.Point(437, 0);
            this.layoutControlItemChecker1.Name = "layoutControlItemChecker1";
            this.layoutControlItemChecker1.OptionsTableLayoutItem.ColumnIndex = 1;
            this.layoutControlItemChecker1.Size = new System.Drawing.Size(437, 29);
            this.layoutControlItemChecker1.Text = "Người kiểm tra 1";
            this.layoutControlItemChecker1.TextSize = new System.Drawing.Size(78, 13);
            // 
            // layoutControlItemChecker2
            // 
            this.layoutControlItemChecker2.Control = this.textEditChecker2;
            this.layoutControlItemChecker2.Location = new System.Drawing.Point(874, 0);
            this.layoutControlItemChecker2.Name = "layoutControlItemChecker2";
            this.layoutControlItemChecker2.OptionsTableLayoutItem.ColumnIndex = 2;
            this.layoutControlItemChecker2.Size = new System.Drawing.Size(437, 29);
            this.layoutControlItemChecker2.Text = "Người kiểm tra 2";
            this.layoutControlItemChecker2.TextSize = new System.Drawing.Size(78, 13);
            // 
            // layoutControl3
            // 
            this.layoutControl3.Controls.Add(this.gridControlData);
            this.layoutControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl3.Location = new System.Drawing.Point(0, 134);
            this.layoutControl3.Name = "layoutControl3";
            this.layoutControl3.Root = this.layoutControlGroup3;
            this.layoutControl3.Size = new System.Drawing.Size(1311, 495);
            this.layoutControl3.TabIndex = 3;
            this.layoutControl3.Text = "layoutControl3";
            // 
            // gridControlData
            // 
            this.gridControlData.DataSource = this.tSDetailBindingSource;
            this.gridControlData.Location = new System.Drawing.Point(12, 2);
            this.gridControlData.MainView = this.gridViewData;
            this.gridControlData.Name = "gridControlData";
            this.gridControlData.Size = new System.Drawing.Size(1287, 491);
            this.gridControlData.TabIndex = 4;
            this.gridControlData.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewData});
            // 
            // tSDetailBindingSource
            // 
            this.tSDetailBindingSource.DataMember = "TSDetail";
            this.tSDetailBindingSource.DataSource = this.wARHOUSE_HPDataSet4;
            // 
            // wARHOUSE_HPDataSet4
            // 
            this.wARHOUSE_HPDataSet4.DataSetName = "WARHOUSE_HPDataSet4";
            this.wARHOUSE_HPDataSet4.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridViewData
            // 
            this.gridViewData.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTSNumber,
            this.colOrdinal,
            this.colGoodsID,
            this.colGoodsName,
            this.colTallyUnitID,
            this.colUnitRate,
            this.colStockUnitID,
            this.colQuantity,
            this.colPriceIncludedVAT,
            this.colPriceExcludedVAT,
            this.colPrice,
            this.colDiscount,
            this.colDiscountAmount,
            this.colVAT,
            this.colVATAmount,
            this.colAmountExcludedVAT,
            this.colAmountIncludedVAT,
            this.colAmount,
            this.colExpiryDate,
            this.colSerialNumber,
            this.colNote,
            this.colStatus});
            this.gridViewData.GridControl = this.gridControlData;
            this.gridViewData.Name = "gridViewData";
            // 
            // colTSNumber
            // 
            this.colTSNumber.FieldName = "TSNumber";
            this.colTSNumber.Name = "colTSNumber";
            // 
            // colOrdinal
            // 
            this.colOrdinal.FieldName = "Ordinal";
            this.colOrdinal.Name = "colOrdinal";
            // 
            // colGoodsID
            // 
            this.colGoodsID.Caption = "Mã hàng";
            this.colGoodsID.FieldName = "GoodsID";
            this.colGoodsID.MinWidth = 120;
            this.colGoodsID.Name = "colGoodsID";
            this.colGoodsID.Visible = true;
            this.colGoodsID.VisibleIndex = 0;
            this.colGoodsID.Width = 120;
            // 
            // colGoodsName
            // 
            this.colGoodsName.Caption = "Tên hàng";
            this.colGoodsName.FieldName = "GoodsName";
            this.colGoodsName.MinWidth = 200;
            this.colGoodsName.Name = "colGoodsName";
            this.colGoodsName.Visible = true;
            this.colGoodsName.VisibleIndex = 1;
            this.colGoodsName.Width = 200;
            // 
            // colTallyUnitID
            // 
            this.colTallyUnitID.Caption = "Đơn vị kiểm kê";
            this.colTallyUnitID.FieldName = "TallyUnitID";
            this.colTallyUnitID.MinWidth = 80;
            this.colTallyUnitID.Name = "colTallyUnitID";
            this.colTallyUnitID.Visible = true;
            this.colTallyUnitID.VisibleIndex = 2;
            this.colTallyUnitID.Width = 80;
            // 
            // colUnitRate
            // 
            this.colUnitRate.Caption = "Tỉ lệ đơn vị";
            this.colUnitRate.FieldName = "UnitRate";
            this.colUnitRate.MinWidth = 80;
            this.colUnitRate.Name = "colUnitRate";
            this.colUnitRate.Visible = true;
            this.colUnitRate.VisibleIndex = 3;
            this.colUnitRate.Width = 800;
            // 
            // colStockUnitID
            // 
            this.colStockUnitID.Caption = "Đơn vị kho";
            this.colStockUnitID.FieldName = "StockUnitID";
            this.colStockUnitID.MinWidth = 80;
            this.colStockUnitID.Name = "colStockUnitID";
            this.colStockUnitID.Visible = true;
            this.colStockUnitID.VisibleIndex = 4;
            this.colStockUnitID.Width = 80;
            // 
            // colQuantity
            // 
            this.colQuantity.Caption = "Số lượng";
            this.colQuantity.FieldName = "Quantity";
            this.colQuantity.MinWidth = 50;
            this.colQuantity.Name = "colQuantity";
            this.colQuantity.Visible = true;
            this.colQuantity.VisibleIndex = 5;
            // 
            // colPriceIncludedVAT
            // 
            this.colPriceIncludedVAT.FieldName = "PriceIncludedVAT";
            this.colPriceIncludedVAT.Name = "colPriceIncludedVAT";
            // 
            // colPriceExcludedVAT
            // 
            this.colPriceExcludedVAT.FieldName = "PriceExcludedVAT";
            this.colPriceExcludedVAT.Name = "colPriceExcludedVAT";
            // 
            // colPrice
            // 
            this.colPrice.Caption = "Đơn giá";
            this.colPrice.FieldName = "Price";
            this.colPrice.MinWidth = 50;
            this.colPrice.Name = "colPrice";
            this.colPrice.Visible = true;
            this.colPrice.VisibleIndex = 6;
            // 
            // colDiscount
            // 
            this.colDiscount.FieldName = "Discount";
            this.colDiscount.Name = "colDiscount";
            // 
            // colDiscountAmount
            // 
            this.colDiscountAmount.FieldName = "DiscountAmount";
            this.colDiscountAmount.Name = "colDiscountAmount";
            // 
            // colVAT
            // 
            this.colVAT.FieldName = "VAT";
            this.colVAT.Name = "colVAT";
            // 
            // colVATAmount
            // 
            this.colVATAmount.FieldName = "VATAmount";
            this.colVATAmount.Name = "colVATAmount";
            // 
            // colAmountExcludedVAT
            // 
            this.colAmountExcludedVAT.FieldName = "AmountExcludedVAT";
            this.colAmountExcludedVAT.Name = "colAmountExcludedVAT";
            // 
            // colAmountIncludedVAT
            // 
            this.colAmountIncludedVAT.FieldName = "AmountIncludedVAT";
            this.colAmountIncludedVAT.Name = "colAmountIncludedVAT";
            // 
            // colAmount
            // 
            this.colAmount.Caption = "Thành tiền";
            this.colAmount.FieldName = "Amount";
            this.colAmount.MinWidth = 80;
            this.colAmount.Name = "colAmount";
            this.colAmount.Visible = true;
            this.colAmount.VisibleIndex = 7;
            this.colAmount.Width = 80;
            // 
            // colExpiryDate
            // 
            this.colExpiryDate.Caption = "Hạn sử dụng";
            this.colExpiryDate.FieldName = "ExpiryDate";
            this.colExpiryDate.MinWidth = 80;
            this.colExpiryDate.Name = "colExpiryDate";
            this.colExpiryDate.Visible = true;
            this.colExpiryDate.VisibleIndex = 8;
            this.colExpiryDate.Width = 80;
            // 
            // colSerialNumber
            // 
            this.colSerialNumber.Caption = "Số Serial";
            this.colSerialNumber.FieldName = "SerialNumber";
            this.colSerialNumber.MinWidth = 100;
            this.colSerialNumber.Name = "colSerialNumber";
            this.colSerialNumber.Visible = true;
            this.colSerialNumber.VisibleIndex = 9;
            this.colSerialNumber.Width = 100;
            // 
            // colNote
            // 
            this.colNote.Caption = "Ghi chú";
            this.colNote.FieldName = "Note";
            this.colNote.MinWidth = 150;
            this.colNote.Name = "colNote";
            this.colNote.Visible = true;
            this.colNote.VisibleIndex = 10;
            this.colNote.Width = 150;
            // 
            // colStatus
            // 
            this.colStatus.FieldName = "Status";
            this.colStatus.Name = "colStatus";
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4});
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(10, 10, 0, 0);
            this.layoutControlGroup3.Size = new System.Drawing.Size(1311, 495);
            this.layoutControlGroup3.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.gridControlData;
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(1291, 495);
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // tSHeaderBindingSource
            // 
            this.tSHeaderBindingSource.DataMember = "TSHeader";
            this.tSHeaderBindingSource.DataSource = this.wARHOUSE_HPDataSet4;
            // 
            // tSHeaderTableAdapter
            // 
            this.tSHeaderTableAdapter.ClearBeforeFill = true;
            // 
            // tSDetailTableAdapter
            // 
            this.tSDetailTableAdapter.ClearBeforeFill = true;
            // 
            // TallySheetControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl3);
            this.Controls.Add(this.layoutControl2);
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.dataLayoutControl1);
            this.Name = "TallySheetControl";
            this.Size = new System.Drawing.Size(1311, 658);
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEditTSNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTallyDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTallyDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditReferenceNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditWarehouse.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditPriceType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditHandlingStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalQuantity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalAmountIncludedVAT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalAmount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditNote.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTSNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTallyDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemReferenceNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWarehouse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPriceType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemHandlingStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalQuantity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalAmountIncludedVAT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemNote)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEditChecker1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditChecker2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemChecker1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemChecker2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).EndInit();
            this.layoutControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tSDetailBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wARHOUSE_HPDataSet4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tSHeaderBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel windowsUIButtonPanel1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.TextEdit textEditTSNumber;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemTSNumber;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.DateEdit dateEditTallyDate;
        private DevExpress.XtraEditors.TextEdit textEditReferenceNumber;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditWarehouse;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditPriceType;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditHandlingStatus;
        private DevExpress.XtraEditors.TextEdit textEditTotalQuantity;
        private DevExpress.XtraEditors.TextEdit textEditTotalAmountIncludedVAT;
        private DevExpress.XtraEditors.TextEdit textEditTotalAmount;
        private DevExpress.XtraEditors.TextEdit textEditNote;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemTallyDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemReferenceNumber;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemWarehouse;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemPriceType;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemHandlingStatus;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemTotalQuantity;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemTotalAmountIncludedVAT;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemTotalAmount;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemNote;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private System.Windows.Forms.RadioButton radioButtonScanOption;
        private DevExpress.XtraEditors.TextEdit textEditChecker1;
        private DevExpress.XtraEditors.TextEdit textEditChecker2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemChecker1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemChecker2;
        private DevExpress.XtraLayout.LayoutControl layoutControl3;
        private DevExpress.XtraGrid.GridControl gridControlData;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewData;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private System.Windows.Forms.BindingSource tSDetailBindingSource;
        private WARHOUSE_HPDataSet4 wARHOUSE_HPDataSet4;
        private DevExpress.XtraGrid.Columns.GridColumn colTSNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colOrdinal;
        private DevExpress.XtraGrid.Columns.GridColumn colGoodsID;
        private DevExpress.XtraGrid.Columns.GridColumn colGoodsName;
        private DevExpress.XtraGrid.Columns.GridColumn colTallyUnitID;
        private DevExpress.XtraGrid.Columns.GridColumn colUnitRate;
        private DevExpress.XtraGrid.Columns.GridColumn colStockUnitID;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantity;
        private DevExpress.XtraGrid.Columns.GridColumn colPriceIncludedVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colPriceExcludedVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colPrice;
        private DevExpress.XtraGrid.Columns.GridColumn colDiscount;
        private DevExpress.XtraGrid.Columns.GridColumn colDiscountAmount;
        private DevExpress.XtraGrid.Columns.GridColumn colVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colVATAmount;
        private DevExpress.XtraGrid.Columns.GridColumn colAmountExcludedVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colAmountIncludedVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colAmount;
        private DevExpress.XtraGrid.Columns.GridColumn colExpiryDate;
        private DevExpress.XtraGrid.Columns.GridColumn colSerialNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colNote;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus;
        private System.Windows.Forms.BindingSource tSHeaderBindingSource;
        private WARHOUSE_HPDataSet4TableAdapters.TSHeaderTableAdapter tSHeaderTableAdapter;
        private WARHOUSE_HPDataSet4TableAdapters.TSDetailTableAdapter tSDetailTableAdapter;
    }
}
