﻿using DevExpress.XtraBars.Docking2010;
using DevExpress.XtraBars.Docking2010.Views.WindowsUI;
using DevExpress.XtraGrid.Views.Grid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WarehouseManager.Models;
using WarehouseManager.Repository;
using webservice.Entities;

namespace WarehouseManager
{
    public partial class frmSearchTSH : Form
    {
        HandlingStatusRepository handlingStatusRepository;
        BranchRepository branchRepository;
        TSHeaderRepository tSHeaderRepository;
        public frmSearchTSH()
        {
            InitializeComponent();
            this.Load += frmSearchTSH_Load;
            handlingStatusRepository = new HandlingStatusRepository();
            branchRepository = new BranchRepository();
            tSHeaderRepository = new TSHeaderRepository();
        }

        private void frmSearchTSH_Load(object sender, EventArgs e)
        {
            popupMenuSelectBranch();
            popupMenuSelectHandlingStatus();
            sbLoadDataForGridAsync();
            gridViewWRRHeader.DoubleClick += dataGridView_CellDoubleClick;
            simpleButtonFilter.Click += filterButton_Click;
            windowsUIButtonPanel1.ButtonClick += button_Click;
            this.KeyDown +=new KeyEventHandler(Form1_KeyDown);

            if (int.Parse(DateTime.Now.Day.ToString()) == 1)
            {
                dateEditFromDate.Text = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day).ToString("yyyy-MM-dd");
            }
            else
            {
                dateEditFromDate.Text = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day - 1).ToString("yyyy-MM-dd");
            }
            dateEditToDate.Text = DateTime.Now.ToString("yyyy-MM-dd");

        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F5)
            {
                sbLoadDataForGridAsync();
            }
            if (e.KeyCode == Keys.F10)
            {
                this.Close();
            }
        }

        private void OnMouseEnterButton1(object sender, EventArgs e)
        {
            windowsUIButtonPanel1.BackColor = SystemColors.ButtonHighlight; // or Color.Red or whatever you want       
        }

        private async Task popupMenuSelectHandlingStatus()
        {
            List<HandlingStatus> handlingStatuses = await handlingStatusRepository.GetAll();
            foreach (HandlingStatus handlingStatus in handlingStatuses)
            {
                comboBoxEditHandlingStatus.Properties.Items.Add(handlingStatus.HandlingStatusName);
            }
            comboBoxEditHandlingStatus.SelectedIndex = 1;
        }

        private async Task popupMenuSelectBranch()
        {
            List<Branch> branches = await branchRepository.getAllBranchAsync();
            foreach (Branch branch in branches)
            {
                comboBoxEditBranch.Properties.Items.Add(branch.BranchName);
            }
            comboBoxEditBranch.SelectedIndex = 1;
        }

        private async Task sbLoadDataForGridAsync()
        {
            List<TSHeader> tSHeaders = await tSHeaderRepository.GetUnderBranch(WMMessage.User.BranchID);
            gridControlWRRHeader.DataSource =tSHeaders;
        }

        private async void dataGridView_CellDoubleClick(object sender, EventArgs e)
        {
            if (gridViewWRRHeader.RowCount > 0)
            {
                TSHeader tSHeader = await tSHeaderRepository.GetUnderID((string)(sender as GridView).GetFocusedRowCellValue("TSNumber"));
                TallySheetControl.selectedID = (string)(sender as GridView).GetFocusedRowCellValue("TSNumber");
                this.Close();
            }
        }

        private async void filterButton_Click(Object sender,EventArgs e)
        {
            String branchID = $"{int.Parse(comboBoxEditBranch.SelectedIndex.ToString()) - 1:D3}";
            String handlingStatusID= (comboBoxEditHandlingStatus.SelectedIndex-1).ToString();
            DateTime fromDate = DateTime.Parse(dateEditFromDate.Text);
            DateTime toDate = DateTime.Parse(dateEditToDate.Text);

            List<TSHeader> listUnderBranch = new List<TSHeader>();
            List<TSHeader> listUnderDate = new List<TSHeader>();
            List<TSHeader> listUnderHandlingStatus = new List<TSHeader>();
            List<TSHeader> tSHeaders = new List<TSHeader>();
            if (branchID.Equals("<>"))
            {
                listUnderBranch= await tSHeaderRepository.GetUnderBranch(WMMessage.User.BranchID);
            }
            else
            {
                listUnderBranch = await tSHeaderRepository.GetUnderBranch(WMMessage.User.BranchID);
            }
            if (handlingStatusID.Equals("<>"))
            {
                listUnderHandlingStatus = await tSHeaderRepository.GetUnderBranch(WMMessage.User.BranchID);
            }
            else
            {
                listUnderHandlingStatus = await tSHeaderRepository.GetUnderHandlingStatus(handlingStatusID);
            }
            listUnderDate = await tSHeaderRepository.GetUnderDate(fromDate, toDate);

            for(int i = 0; i <listUnderBranch.Count; i++)
            {
                for(int j = 0; j < listUnderDate.Count; j++)
                {
                    for(int k = 0; k < listUnderHandlingStatus.Count; k++)
                    {
                        if(listUnderBranch[i].TSNumber.Equals(listUnderDate[j].TSNumber)
                            && listUnderDate[j].TSNumber.Equals(listUnderHandlingStatus[j].TSNumber))
                        {
                            tSHeaders.Add(listUnderBranch[i]);
                        }
                    }
                }
            }
            gridControlWRRHeader.DataSource = tSHeaders;

        }

        public async void button_Click(object sender, EventArgs e)
        {
            ButtonEventArgs eArg = (DevExpress.XtraBars.Docking2010.ButtonEventArgs)e;
            String tag = eArg.Button.Properties.Tag.ToString();
            switch (tag)
            {
                case "refesh":
                    sbLoadDataForGridAsync();
                    break;
                case "close":
                    this.Close();
                    break;
            }

        }   

    }
}
