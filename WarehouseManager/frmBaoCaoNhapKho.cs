﻿using DevExpress.XtraEditors.Controls;
using DevExpress.XtraSplashScreen;
using DevExpress.XtraWaitForm;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WarehouseManager.Models;
using WarehouseManager.Repository;
using webservice.Entities;
using LicenseContext = OfficeOpenXml.LicenseContext;

namespace WarehouseManager
{
    public partial class frmBaoCaoNhapKho : UserControl
    {
        WRDataGeneralRepository wRDataGeneralRepository = null;
        WRDataHeaderRepository wRDataHeaderRepository = null;
        public frmBaoCaoNhapKho()
        {
            InitializeComponent();
            wRDataGeneralRepository = new WRDataGeneralRepository();
            wRDataHeaderRepository = new WRDataHeaderRepository();

            dateEdit1.DateTime =DateTime.Now.Date;
            dateEdit2.DateTime= DateTime.Now.Date;

            loadData();
        }

        private void windowsUIButtonPanel1_ButtonClick(object sender, DevExpress.XtraBars.Docking2010.ButtonEventArgs e)
        {
            String tag = e.Button.Properties.Tag.ToString();
            switch (tag)
            {
                case "refesh":
                    loadData();
                    break;
                case "export_date":
                    exportDate();
                    break;
                case "export_month":
                    exportMonth();
                    break;
                case "close":
                    this.Dispose();
                    break;
            }
        }

        private void exportDate()
        {
            Int32[] selectedRowHandles = gridView1.GetSelectedRows();
            if (selectedRowHandles.Length > 0)
            {
                DialogResult dialogResult = MessageBox.Show("Xuất báo cáo dữ liệu đã chọn", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dialogResult == DialogResult.Yes)
                {
                    using (SaveFileDialog sfd = new SaveFileDialog() { Filter = "Excel Workbook|*.xlsx", FileName = "Receipt Report " + DateTime.Now.ToString("ddMMyyyy")})
                    {
                        if (sfd.ShowDialog() == DialogResult.OK)
                        {
                            var fileInfo = new FileInfo(sfd.FileName);
                            if (fileInfo.Exists)
                            {
                                fileInfo.Delete();
                            }

                           
                            using (ExcelPackage excelPackage = new ExcelPackage(fileInfo))
                            {
                                excelPackage.Workbook.Protection.LockRevision = false;
                                excelPackage.Workbook.Protection.LockStructure = false;
                                excelPackage.Workbook.Protection.LockWindows = false;

                                ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets.Add(DateTime.Now.ToString("ddMMyyyy"));

                                worksheet.Protection.IsProtected = false;
                                worksheet.Protection.AllowFormatCells = true;
                                worksheet.Protection.AllowSelectLockedCells = true;
                                worksheet.Protection.AllowSelectUnlockedCells = true;
                                worksheet.Protection.AllowEditObject = true;
                                worksheet.Protection.AllowEditScenarios = true;
                                worksheet.Protection.AllowFormatColumns = true;
                                worksheet.Protection.AllowFormatRows = true;
                                worksheet.Column(1).Style.Locked = false;
                                worksheet.Column(2).Style.Locked = false;
                                worksheet.Column(3).Style.Locked = false;
                                worksheet.Column(4).Style.Locked = false;
                                worksheet.Column(5).Style.Locked = false;
                                worksheet.Column(6).Style.Locked = false;
                                worksheet.Column(7).Style.Locked = false;
                                worksheet.Column(8).Style.Locked = false;
                               

                                worksheet.Cells[1, 1].Value = "BÁO CÁO NHẬN HÀNG";
                                worksheet.Row(1).Style.Font.Bold = true;
                                worksheet.Cells[1, 1, 1, 9].Merge = true;
                                worksheet.Row(1).Style.Font.Size = 18;
                                worksheet.Row(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                worksheet.Cells[3, 1].Value = "Thời gian";
                                worksheet.Cells[3, 2].Value = "Từ:";
                                worksheet.Cells[3, 3].Value = dateEdit1.DateTime.ToString("HH:mm");
                                worksheet.Cells[3, 4].Value = "Ngày";
                                worksheet.Cells[3, 5].Value = dateEdit1.DateTime.ToString("dd-MMM-yy");
                                worksheet.Cells[4, 2].Value = "Tới";
                                worksheet.Cells[4, 3].Value = dateEdit2.DateTime.ToString("HH:mm");
                                worksheet.Cells[4, 4].Value = "Ngày";
                                worksheet.Cells[4, 5].Value = dateEdit2.DateTime.ToString("dd-MMM-yy"); ;

                                worksheet.TabColor = System.Drawing.Color.Black;
                                worksheet.DefaultRowHeight = 12;
                                //Header of table  
                                //  

                                worksheet.Row(6).Height = 50;
                                worksheet.Row(6).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                worksheet.Row(6).Style.Font.Bold = true;

                                worksheet.Cells[6, 1].Value = "Conveyance_number";
                                worksheet.Cells[6, 2].Value = "Part_No";
                                worksheet.Cells[6, 3].Value = "ASN_Qty";
                                worksheet.Cells[6, 4].Value = "SL_Thực_nhận";
                                worksheet.Cells[6, 5].Value = "Ngày_nhận_hàng";
                                worksheet.Cells[6, 6].Value = "Thời_gian_yêu_cầu";
                                worksheet.Cells[6, 7].Value = "Thời_gian_nhận_hàng";
                                worksheet.Cells[6, 8].Value = "Time_Interval";
                                worksheet.Cells[6, 9].Value = "Ghi_chú_khác";
                               

                                //Body of table  
                                //  
                                SplashScreenManager.ShowForm(this, typeof(WaitForm), true, true);
                                int recordIndex = 7;
                                for (int i = 0; i < selectedRowHandles.Length; i++)
                                {
                                    string goodsID = (string)gridView1.GetRowCellValue(selectedRowHandles[i], "GoodsID");
                                    string goodsName = (string)gridView1.GetRowCellValue(selectedRowHandles[i], "GoodsName");
                                    string wrdNumber = (string)gridView1.GetRowCellValue(selectedRowHandles[i], "WRDNumber");
                                    decimal quantityOrg= (decimal)gridView1.GetRowCellValue(selectedRowHandles[i], "QuantityOrg");
                                    decimal quantity = (decimal)gridView1.GetRowCellValue(selectedRowHandles[i], "Quantity");

                                    if (!string.IsNullOrEmpty(gridView1.GetRowCellValue(selectedRowHandles[i], "EditedDateTime") + ""))
                                    {
                                        worksheet.Cells[recordIndex, 5].Value = ((DateTime)gridView1.GetRowCellValue(selectedRowHandles[i], "EditedDateTime")).Date.ToString("dd/MM/yyyy");
                                    }

                                    if (!string.IsNullOrEmpty(gridView1.GetRowCellValue(selectedRowHandles[i], "CreatedDateTime") + ""))
                                    {
                                        worksheet.Cells[recordIndex, 6].Value = ((DateTime)gridView1.GetRowCellValue(selectedRowHandles[i], "CreatedDateTime")).ToString("dd/MM/yyyy HH:mm");
                                    }
                                    if (!string.IsNullOrEmpty(gridView1.GetRowCellValue(selectedRowHandles[i], "EditedDateTime") + ""))
                                    {
                                        worksheet.Cells[recordIndex, 7].Value = ((DateTime)gridView1.GetRowCellValue(selectedRowHandles[i], "EditedDateTime")).ToString("dd/MM/yyyy HH:mm");
                                    }
                                    if (!string.IsNullOrEmpty(gridView1.GetRowCellValue(selectedRowHandles[i], "CreatedDateTime") + "")&&!string.IsNullOrEmpty(gridView1.GetRowCellValue(selectedRowHandles[i], "EditedDateTime") + ""))
                                    {
                                        worksheet.Cells[recordIndex, 8].Value = ((DateTime)gridView1.GetRowCellValue(selectedRowHandles[i], "EditedDateTime"))-((DateTime)gridView1.GetRowCellValue(selectedRowHandles[i], "CreatedDateTime"))+"";
                                    }

                                    string note = (string)gridView1.GetRowCellValue(selectedRowHandles[i], "Note");

                                    worksheet.Cells[recordIndex, 1].Value = wrdNumber;
                                    worksheet.Cells[recordIndex, 2].Value = goodsID;
                                    worksheet.Cells[recordIndex, 3].Value = quantityOrg;
                                    worksheet.Cells[recordIndex, 4].Value = quantity;
                                    worksheet.Cells[recordIndex, 9].Value = note;

                                    recordIndex++;
                                }

                                worksheet.Column(1).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                worksheet.Column(2).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                worksheet.Column(3).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                worksheet.Column(4).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                worksheet.Column(5).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                worksheet.Column(6).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                worksheet.Column(7).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                worksheet.Column(8).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                worksheet.Column(9).Style.VerticalAlignment = ExcelVerticalAlignment.Center;


                                worksheet.Column(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                worksheet.Column(2).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                worksheet.Column(3).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                worksheet.Column(4).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                worksheet.Column(5).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                worksheet.Column(6).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                worksheet.Column(7).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                worksheet.Column(8).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                worksheet.Column(9).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;


                                worksheet.Column(1).Width = 20;
                                worksheet.Column(2).Width = 20;
                                worksheet.Column(3).Width = 20;
                                worksheet.Column(4).Width = 20;
                                worksheet.Column(5).Width = 20;
                                worksheet.Column(6).Width = 30;
                                worksheet.Column(7).Width = 20;
                                worksheet.Column(8).Width = 20;
                                worksheet.Column(9).Width = 20;


                                var modelTable = worksheet.Cells[6, 1, recordIndex - 1,9];
                                modelTable.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                modelTable.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                modelTable.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                modelTable.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                modelTable.AutoFitColumns();

                                excelPackage.Save();
                            }
                            SplashScreenManager.CloseForm(false);
                            MessageBox.Show("Xuất báo cáo thành công");
                            Process.Start(sfd.InitialDirectory + sfd.FileName);
                        }
                    }
                    
                }
            }
        }

        private void exportMonth()
        {

        }

        private void dateEdit1_DateTimeChanged(object sender, EventArgs e)
        {
            loadData();
        }

        private void dateEdit2_DateTimeChanged(object sender, EventArgs e)
        {
            loadData();
        }

        private async void loadData()
        {
            //List<WRDataHeader> wRDataHeaders = await wRDataHeaderRepository.GetUnderDate(dateEdit1.DateTime.Date, dateEdit2.DateTime.Date);
            //if (wRDataHeaders.Count > 0)
            //{
            //    List<WRDataGeneral> wRDataGenerals1 = new List<WRDataGeneral>();
            //    foreach (WRDataHeader wRDataHeader in wRDataHeaders)
            //    {
            //        List<WRDataGeneral> wRDataGenerals = await wRDataGeneralRepository.GetUnderID(wRDataHeader.WRDNumber);
            //        if (wRDataGenerals.Count > 0)
            //        {
            //            wRDataGenerals1.AddRange(wRDataGenerals);
            //        }
            //    }
            //    gridControl1.DataSource = wRDataGenerals1;
            //}
          
            List<WRDataGeneral> wRDataGenerals = await wRDataGeneralRepository.GetBeetwenNgayNhanHang(dateEdit1.DateTime,dateEdit2.DateTime);
            if (wRDataGenerals.Count > 0)
            {
                //List<WRDataGeneral> wRDataGenerals1 = new List<WRDataGeneral>();
                //foreach (WRDataGeneral wRDataGeneral in wRDataGenerals)
                //{
                //    wRDataGeneral.EditedDateTime = DateTime.Parse(wRDataGeneral.EditedDateTime).ToString("dd/MM/yyyy");
                //    wRDataGenerals1.Add(wRDataGeneral);
                //}
                gridControl1.DataSource = wRDataGenerals;
            }
            else
            {
                gridControl1.DataSource = null;
            }


        }

        private void gridView1_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            if (e.Column.FieldName.Equals("No"))
            {
                int rowHandle = gridView1.GetRowHandle(e.ListSourceRowIndex);
                if (rowHandle >= 0)
                {
                    e.DisplayText = (rowHandle + 1).ToString();
                }
            }
        }

        private void timeEdit2_EditValueChanged(object sender, EventArgs e)
        {
            loadData();
        }

        private void timeEdit1_EditValueChanged(object sender, EventArgs e)
        {
            loadData();
        }
    }
}
