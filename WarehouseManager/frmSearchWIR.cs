﻿using DevExpress.XtraBars.Docking2010;
using DevExpress.XtraGrid.Views.Grid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WarehouseManager.Models;
using WarehouseManager.Repository;
using webservice.Entities;

namespace WarehouseManager
{
    public partial class frmSearchWIR : Form
    {
        HandlingStatusRepository handlingStatusRepository;
        BranchRepository branchRepository;
        WIRHeaderRepository wIRHeaderRepository;
        public frmSearchWIR()
        {
            InitializeComponent();
            this.Load += frmSearchWIR_Load;
            handlingStatusRepository = new HandlingStatusRepository();
            branchRepository = new BranchRepository();
            wIRHeaderRepository = new WIRHeaderRepository();

            
        }

        private void frmSearchWIR_Load(object sender, EventArgs e)
        {
            popupMenuSelectBranch();
            popupMenuSelectHandlingStatus();
            sbLoadDataForGridWIRHeaderAsync();
            gridViewWIRHeader.DoubleClick += dataGridView_CellDoubleClick;
            simpleButtonFilter.Click += filterButton_Click;
            windowsUIButtonPanel1.ButtonClick += button_Click;
            this.KeyDown += new KeyEventHandler(Form1_KeyDown);

            if (int.Parse(DateTime.Now.Day.ToString()) == 1)
            {
                dateEditFromDate.Text = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day).ToString("yyyy-MM-dd");
            }
            else
            {
                dateEditFromDate.Text = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day - 1).ToString("yyyy-MM-dd");
            }
            dateEditToDate.Text = DateTime.Now.ToString("yyyy-MM-dd");
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F5)
            {
                sbLoadDataForGridWIRHeaderAsync();
            }
            if (e.KeyCode == Keys.F10)
            {
                this.Close();
            }
        }

        public async void button_Click(object sender, EventArgs e)
        {
            ButtonEventArgs eArg = (DevExpress.XtraBars.Docking2010.ButtonEventArgs)e;
            String tag = eArg.Button.Properties.Tag.ToString();
            switch (tag)
            {
                case "refesh":
                    sbLoadDataForGridWIRHeaderAsync();
                    break;
                case "close":
                    this.Close();
                    break;
            }

        }

        private async Task popupMenuSelectHandlingStatus()
        {
            List<HandlingStatus> handlingStatuses = await handlingStatusRepository.GetAll();
            foreach (HandlingStatus handlingStatus in handlingStatuses)
            {
                comboBoxEditHandlingStatus.Properties.Items.Add(handlingStatus.HandlingStatusName);
            }
            comboBoxEditHandlingStatus.SelectedIndex = 1;
        }

        private async Task popupMenuSelectBranch()
        {
            List<Branch> branches = await branchRepository.getAllBranchAsync();
            foreach (Branch branch in branches)
            {
                comboBoxEditBranch.Properties.Items.Add(branch.BranchName);
            }
            comboBoxEditBranch.SelectedIndex = 1;
        }

        private async Task sbLoadDataForGridWIRHeaderAsync()
        {
            List<WIRHeader> wIRHeaders = await wIRHeaderRepository.GetAll();
            gridControlWIRHeader.DataSource =wIRHeaders;
        }

        private async void dataGridView_CellDoubleClick(object sender, EventArgs e)
        {
            if (gridViewWIRHeader.RowCount > 0)
            {
                WIRHeader wIRHeader = await wIRHeaderRepository.GetUnderID((string)(sender as GridView).GetFocusedRowCellValue("WIRNumber"));
                IssueRequisitionControl.selectedWIRNumber = (string)(sender as GridView).GetFocusedRowCellValue("WIRNumber");
                this.Close();
            }
        }

        private async void filterButton_Click(Object sender,EventArgs e)
        {
            //String branchID = $"{int.Parse(comboBoxEditBranch.SelectedIndex.ToString()) - 1:D3}";
            //String handlingStatusID = (comboBoxEditHandlingStatus.SelectedIndex - 1).ToString();
            //DateTime fromDate = DateTime.Parse(dateEditFromDate.Text);
            //DateTime toDate = DateTime.Parse(dateEditToDate.Text);

            //List<WIRHeader> wIRHeadersUnderBranch = new List<WIRHeader>();
            //List<WIRHeader> wIRHeadersUnderDate = new List<WIRHeader>();
            //List<WIRHeader> wIRHeadersUnderHandlingStatus = new List<WIRHeader>();
            //List<WIRHeader> wIRHeaders = new List<WIRHeader>();
            //if (branchID.Equals("<>"))
            //{
            //    wIRHeadersUnderBranch = await wIRHeaderRepository.GetAll();
            //}
            //else
            //{
            //    wIRHeadersUnderBranch = await wIRHeaderRepository.GetUnderBranch(branchID);
            //}
            //if (handlingStatusID.Equals("<>"))
            //{
            //    wIRHeadersUnderHandlingStatus = await wIRHeaderRepository.GetAll();
            //}
            //else
            //{
            //    wIRHeadersUnderHandlingStatus = await wIRHeaderRepository.GetUnderHandlingStatus(handlingStatusID);
            //}
            //wIRHeadersUnderDate = await wIRHeaderRepository.GetUnderDate(fromDate, toDate);

            //for (int i = 0; i < wIRHeadersUnderBranch.Count; i++)
            //{
            //    for (int j = 0; j < wIRHeadersUnderDate.Count; j++)
            //    {
            //        for (int k = 0; k < wIRHeadersUnderHandlingStatus.Count; k++)
            //        {
            //            if (wIRHeadersUnderBranch[i].WIRNumber.Equals(wIRHeadersUnderDate[j].WIRNumber)
            //                && wIRHeadersUnderDate[j].WIRNumber.Equals(wIRHeadersUnderHandlingStatus[j].WIRNumber))
            //            {
            //                wIRHeaders.Add(wIRHeadersUnderBranch[i]);
            //            }
            //        }
            //    }
            //}
            String branchID = "<>";
            String handlingStatusID = "<>";
            if (!comboBoxEditBranch.SelectedItem.ToString().Equals("<>"))
            {
                branchID = $"{int.Parse(comboBoxEditBranch.SelectedIndex.ToString()) - 1:D3}";
            }
            if (!comboBoxEditHandlingStatus.SelectedItem.ToString().Equals("<>"))
            {
                handlingStatusID = (comboBoxEditHandlingStatus.SelectedIndex - 1).ToString();
            }
            DateTime fromDate = DateTime.Parse(dateEditFromDate.Text);
            DateTime toDate = DateTime.Parse(dateEditToDate.Text);

            List<WIRHeader> wIRHeaders = new List<WIRHeader>();

            wIRHeaders = await wIRHeaderRepository.FilterFinal(branchID, fromDate, toDate, handlingStatusID);
            gridControlWIRHeader.DataSource = wIRHeaders;

        }

    }
}
