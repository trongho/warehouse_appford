﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WarehouseManager
{
    class WMPublic
    {
		static String culture = WMMessage.msgLanguage;

        public static void sbMessageCloseTabpageRequest(Form form)
        {
            if (culture.Equals("vi-VN"))
            {
                MessageBoxEx.Show(form, WMMessage.msgCloseTabpageRequest, WMMessage.msgCaption, MessageBoxButtons.OK);

            }
            else
            {
                MessageBoxEx.Show(form, WMMessage.msgCloseTabpageRequestEN, WMMessage.msgCaptionEN, MessageBoxButtons.OK);
            }
        }
        public static void sbMessageEmptyFieldError(UserControl userControl)
        {
            if (culture.Equals("vi-VN"))
            {
                MessageBoxEx.Show(userControl, WMMessage.msgEmptyFieldError, WMMessage.msgCaption, MessageBoxButtons.OK);

            }
            else
            {
                MessageBoxEx.Show(userControl, WMMessage.msgEmptyFieldErrorEN, WMMessage.msgCaptionEN, MessageBoxButtons.OK);
            }
        }
        public static void sbMessageEmptyPasswordError(UserControl userControl)
        {
            if (culture.Equals("vi-VN"))
            {
                MessageBoxEx.Show(userControl, WMMessage.msgEmptyPasswordError, WMMessage.msgCaption, MessageBoxButtons.OK);

            }
            else
            {
                MessageBoxEx.Show(userControl, WMMessage.msgEmptyPasswordErrorEN, WMMessage.msgCaptionEN, MessageBoxButtons.OK);
            }
        }
        public static void sbMessageCreateUserSuccess(UserControl userControl)
		{
			if (culture.Equals("vi-VN"))
			{
				MessageBoxEx.Show(userControl, WMMessage.msgCreateUserSuccess,WMMessage.msgCaption,MessageBoxButtons.OK);

			}
			else
			{
				MessageBoxEx.Show(userControl, WMMessage.msgCreateUserSuccessEN, WMMessage.msgCaptionEN, MessageBoxButtons.OK);
			}
		}

        public static void sbMessageUpdateUserSuccess(UserControl userControl)
        {
            if (culture.Equals("vi-VN"))
            {
                MessageBoxEx.Show(userControl, WMMessage.msgUpdateUserSuccess, WMMessage.msgCaption, MessageBoxButtons.OK);

            }
            else
            {
                MessageBoxEx.Show(userControl, WMMessage.msgUpdateUserSuccessEN, WMMessage.msgCaptionEN, MessageBoxButtons.OK);
            }
        }

        public static void sbMessageDeleteUserSuccess(UserControl userControl)
        {
            if (culture.Equals("vi-VN"))
            {
                MessageBoxEx.Show(userControl, WMMessage.msgDeleteUserSuccess, WMMessage.msgCaption, MessageBoxButtons.OK);

            }
            else
            {
                MessageBoxEx.Show(userControl, WMMessage.msgDeleteUserSuccessEN, WMMessage.msgCaptionEN, MessageBoxButtons.OK);
            }
        }

        public static void sbMessageCreateGoodsLineSuccess(UserControl userControl)
        {
            if (culture.Equals("vi-VN"))
            {
                MessageBoxEx.Show(userControl, WMMessage.msgCreateGoodsLineSuccess, WMMessage.msgCaption, MessageBoxButtons.OK);

            }
            else
            {
                MessageBoxEx.Show(userControl, WMMessage.msgCreateGoodsLineSuccessEN, WMMessage.msgCaptionEN, MessageBoxButtons.OK);
            }
        }

        public static void sbMessageUpdateGoodsLineSuccess(UserControl userControl)
        {
            if (culture.Equals("vi-VN"))
            {
                MessageBoxEx.Show(userControl, WMMessage.msgUpdateGoodsLineSuccess, WMMessage.msgCaption, MessageBoxButtons.OK);

            }
            else
            {
                MessageBoxEx.Show(userControl, WMMessage.msgUpdateGoodsLineSuccessEN, WMMessage.msgCaptionEN, MessageBoxButtons.OK);
            }
        }

        public static void sbMessageDeleteGoodsLineSuccess(UserControl userControl)
        {
            if (culture.Equals("vi-VN"))
            {
                MessageBoxEx.Show(userControl, WMMessage.msgDeleteGoodsLineSuccess, WMMessage.msgCaption, MessageBoxButtons.OK);

            }
            else
            {
                MessageBoxEx.Show(userControl, WMMessage.msgDeleteGoodsLineSuccessEN, WMMessage.msgCaptionEN, MessageBoxButtons.OK);
            }
        }

        public static void sbMessageSaveNewSuccess(UserControl userControl)
        {
            if (culture.Equals("vi-VN"))
            {
                MessageBoxEx.Show(userControl, WMMessage.msgSaveNewSuccess, WMMessage.msgCaption, MessageBoxButtons.OK);

            }
            else
            {
                MessageBoxEx.Show(userControl, WMMessage.msgSaveNewSuccessEN, WMMessage.msgCaptionEN, MessageBoxButtons.OK);
            }
        }

        public static void sbMessageSaveChangeSuccess(UserControl userControl)
        {
            if (culture.Equals("vi-VN"))
            {
                MessageBoxEx.Show(userControl, WMMessage.msgSaveChangeSuccess, WMMessage.msgCaption, MessageBoxButtons.OK);

            }
            else
            {
                MessageBoxEx.Show(userControl, WMMessage.msgSaveChangeSuccessEN, WMMessage.msgCaptionEN, MessageBoxButtons.OK);
            }
        }

        public static void sbMessageDeleteSuccess(UserControl userControl)
        {
            if (culture.Equals("vi-VN"))
            {
                MessageBoxEx.Show(userControl, WMMessage.msgDeleteSuccess, WMMessage.msgCaption, MessageBoxButtons.OK);

            }
            else
            {
                MessageBoxEx.Show(userControl, WMMessage.msgDeleteSuccessEN, WMMessage.msgCaptionEN, MessageBoxButtons.OK);
            }
        }

        public static Boolean sbMessageCloseTab(Form CurrentForm)
        {
            Boolean flag=false;
            if (culture.Equals("vi-VN"))
            {
                DialogResult dialogResult= MessageBoxEx.Show(CurrentForm, WMMessage.msgCloseTab, WMMessage.msgCaption, MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    flag=true;
                }
            }
            else
            {
                DialogResult dialogResult2= MessageBoxEx.Show(CurrentForm, WMMessage.msgCloseTabEN, WMMessage.msgCaptionEN, MessageBoxButtons.YesNo);
                if (dialogResult2 == DialogResult.Yes)
                {
                    flag = true;
                }               
            }
            return flag;
        }

        public static Boolean sbMessageCloseTab(UserControl control)
        {
            Boolean flag = false;
            if (culture.Equals("vi-VN"))
            {
                DialogResult dialogResult = MessageBoxEx.Show(control, WMMessage.msgCloseTab, WMMessage.msgCaption, MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    flag = true;
                }
            }
            else
            {
                DialogResult dialogResult2 = MessageBoxEx.Show(control, WMMessage.msgCloseTabEN, WMMessage.msgCaptionEN, MessageBoxButtons.YesNo);
                if (dialogResult2 == DialogResult.Yes)
                {
                    flag = true;
                }
            }
            return flag;
        }



        public static void TabCreating(DevExpress.XtraTab.XtraTabControl tabControl, string Text, string Name, UserControl userControl)
        {       
                int index = isExistTabpage(tabControl, Name);
                if (index >=0)
                {
                
                tabControl.SelectedTabPage = tabControl.TabPages[index];
                tabControl.SelectedTabPage.Text = Text;
             

                }               
                else
                {
                    DevExpress.XtraTab.XtraTabPage tabpage = new DevExpress.XtraTab.XtraTabPage { Text = Text, Name = Name };

                    tabControl.TabPages.Add(tabpage);
                    tabControl.SelectedTabPage = tabpage;
                  

                    tabpage.Controls.Add(userControl);                   
                    userControl.Parent = tabpage;
                    userControl.Show();
                    userControl.Dock = DockStyle.Fill;
                }
   
        }

        public static void TabCreating(DevExpress.XtraTab.XtraTabControl tabControl, string Text, string Name, Form form)
        {
            int index = isExistTabpage(tabControl, Name);
            if (index >= 0)
            {

                tabControl.SelectedTabPage = tabControl.TabPages[index];
                tabControl.SelectedTabPage.Text = Text;


            }
            else
            {
                DevExpress.XtraTab.XtraTabPage tabpage = new DevExpress.XtraTab.XtraTabPage { Text = Text, Name = Name };

                tabControl.TabPages.Add(tabpage);
                tabControl.SelectedTabPage = tabpage;

                form.TopLevel = false;
                form.Parent = tabpage;
                form.Show();
                form.Dock = DockStyle.Fill;
                form.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                tabpage.Controls.Add(form);
                
            }

        }

        public static void TabCreating2(DevExpress.XtraTab.XtraTabControl tabControl, string Text, string Name, UserControl form)
        {
                DevExpress.XtraTab.XtraTabPage tabpage = new DevExpress.XtraTab.XtraTabPage { Text = Text, Name = Name };

                tabControl.TabPages.Add(tabpage);
                tabControl.SelectedTabPage = tabpage;

                form.Parent = tabpage;
                form.Show();
                form.Dock = DockStyle.Fill;
                tabpage.Controls.Add(form);
        }

        public static int isExistTabpage(DevExpress.XtraTab.XtraTabControl tabControl, String Name)
        {
            int index = -1;
            foreach (DevExpress.XtraTab.XtraTabPage tab in tabControl.TabPages)
            {
                if (tab.Name == Name)
                {
                    index = tabControl.TabPages.IndexOf(tab);
                    break;
                }
            }
            return index;
        }

        public static Boolean sbMessageAddNewRequest(UserControl control)
        {
            Boolean flag = false;
            if (culture.Equals("vi-VN"))
            {
                DialogResult dialogResult = MessageBoxEx.Show(control, WMMessage.msgAddNewRequest, WMMessage.msgCaption, MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    flag = true;
                }
            }
            else
            {
                DialogResult dialogResult2 = MessageBoxEx.Show(control, WMMessage.msgAddNewRequestEN, WMMessage.msgCaptionEN, MessageBoxButtons.YesNo);
                if (dialogResult2 == DialogResult.Yes)
                {
                    flag = true;
                }
            }
            return flag;
        }

        public static Boolean sbMessageAddNewGoodsDataRequest(UserControl control,String msgHeader,String msgContent)
        {
            Boolean flag = false;
            if (culture.Equals("vi-VN"))
            {
                DialogResult dialogResult = MessageBoxEx.Show(control, msgHeader, msgContent, MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    flag = true;
                }
            }
            else
            {
                DialogResult dialogResult2 = MessageBoxEx.Show(control, msgHeader, msgContent, MessageBoxButtons.YesNo);
                if (dialogResult2 == DialogResult.Yes)
                {
                    flag = true;
                }
            }
            return flag;
        }

        public static Boolean sbMessageSaveNewRequest(UserControl control)
        {
            Boolean flag = false;
            if (culture.Equals("vi-VN"))
            {
                DialogResult dialogResult = MessageBoxEx.Show(control, WMMessage.msgSaveNewRequest, WMMessage.msgCaption, MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    flag = true;
                }
            }
            else
            {
                DialogResult dialogResult2 = MessageBoxEx.Show(control, WMMessage.msgSaveNewRequestEN, WMMessage.msgCaptionEN, MessageBoxButtons.YesNo);
                if (dialogResult2 == DialogResult.Yes)
                {
                    flag = true;
                }
            }
            return flag;
        }

        public static Boolean sbMessageSaveChangeRequest(UserControl control)
        {
            Boolean flag = false;
            if (culture.Equals("vi-VN"))
            {
                DialogResult dialogResult = MessageBoxEx.Show(control, WMMessage.msgSaveChangeRequest, WMMessage.msgCaption, MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    flag = true;
                }
            }
            else
            {
                DialogResult dialogResult2 = MessageBoxEx.Show(control, WMMessage.msgSaveChangeRequestEN, WMMessage.msgCaptionEN, MessageBoxButtons.YesNo);
                if (dialogResult2 == DialogResult.Yes)
                {
                    flag = true;
                }
            }
            return flag;
        }


        public static Boolean sbMessageDeleteRequest(UserControl control)
        {
            Boolean flag = false;
            if (culture.Equals("vi-VN"))
            {
                DialogResult dialogResult = MessageBoxEx.Show(control, WMMessage.msgDeleteRequest, WMMessage.msgCaption, MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    flag = true;
                }
            }
            else
            {
                DialogResult dialogResult2 = MessageBoxEx.Show(control, WMMessage.msgDeleteRequestEN, WMMessage.msgCaptionEN, MessageBoxButtons.YesNo);
                if (dialogResult2 == DialogResult.Yes)
                {
                    flag = true;
                }
            }
            return flag;
        }

    }
}
