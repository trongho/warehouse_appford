﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarehouseManager.Models;

namespace WarehouseManager
{
    class WMMessage
    {
        public static Boolean newButtonClick = false;
        public static Boolean saveButtonClick = false;
        public static Boolean deleteButtonClick = false;
        public static Boolean searchButtonClick = false;
        public static User User;
        public static string msgLanguage = "vi-VN";
        public static string msgCaption = "Thông báo";

        public static string msgCaptionEN = "Notice";
        public static string msgCloseTabpageRequest = "Đóng các cửa sổ trước khi thay đổi ngôn ngữ";

        public static string msgCloseTabpageRequestEN = "Please close tabpages before change language";
        public static string msgEmptyFieldError = "Nhập thiếu dữ liệu";

        public static string msgEmptyFieldErrorEN = "Field empty";
        public static string msgEmptyPasswordError = "Nhập thiếu dữ liệu";

        public static string msgEmptyPasswordErrorEN = "Field empty";

        public static string msgCreateUserSuccess = "Tạo người dùng thành công.";

        public static string msgCreateUserSuccessEN = "Create user success.";
        public static string msgCreateGoodsLineSuccess = "Tạo ngành hàng thành công.";
        public static string msgUpdateUserSuccess = "Cập nhật người dùng thành công.";

        public static string msgUpdateUserSuccessEN = "Update user success.";
        public static string msgDeleteUserSuccess = "Xóa người dùng thành công.";

        public static string msgDeleteUserSuccessEN = "Delete user success.";

        public static string msgCreateGoodsLineSuccessEN = "Create goods line success.";
        public static string msgUpdateGoodsLineSuccess = "Cập nhật ngành hàng thành công.";

        public static string msgUpdateGoodsLineSuccessEN = "Update goods line success.";
         public static string msgDeleteGoodsLineSuccess = "Xóa ngành hàng thành công.";

        public static string msgDeleteGoodsLineSuccessEN = "Delete goods line success.";

        public static string msgSaveNewSuccess = "Lưu mới dữ liệu thành công";
        public static string msgSaveNewSuccessEN = "Create data success.";
        public static string msgSaveChangeSuccess = "Điều chỉnh dữ liệu thành công.";
        public static string msgSaveChangeSuccessEN = "Update data success.";
        public static string msgDeleteSuccess = "Xóa dữ liệu thành công.";
        public static string msgDeleteSuccessEN = "Delete data success.";

        public static string msgCloseTab = "Bạn có muốn đóng cửa sổ này";

        public static string msgCloseTabEN = "Do you want to close this tab";

        public static string msgAddNewRequest = "Bạn có muốn thêm mới?";
        public static string msgAddNewRequestEN = "Are you want to add new?";
        public static string msgSaveNewRequest = "Bạn có muốn lưu mới không ?";
        public static string msgSaveNewRequestEN = "Are you want to save new?";
        public static string msgSaveChangeRequest = "Bạn có muốn lưu thay đổi không ?";
        public static string msgSaveChangeRequestEN = "Are you want to save change?";
        public static string msgDeleteRequest = "Bạn có muốn xóa không?";
        public static string msgDeleteRequestEN = "Are you want to delete?";
    }
}
