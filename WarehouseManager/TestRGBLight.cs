﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WarehouseManager
{
    public partial class TestRGBLight : Form
    {
        public TestRGBLight()
        {
            InitializeComponent();
        }


        private void buttonOn_Click(object sender, EventArgs e)
        {

            Helpers.RGBLightHelper.turnOnYellow();
            textBoxStatus.Text = "Yellow on";
            System.Threading.Thread.Sleep(3000);
            Helpers.RGBLightHelper.turnOff();
            textBoxStatus.Text = "Yellow off";
        }

        private void buttonOff_Click(object sender, EventArgs e)
        {
            Helpers.RGBLightHelper.turnOff();
            textBoxStatus.Text = "Off";
        }

        private void buttonOnGreen_Click(object sender, EventArgs e)
        {
            Helpers.RGBLightHelper.turnOnGreen();
            textBoxStatus.Text = "Green on";
            System.Threading.Thread.Sleep(3000);
            Helpers.RGBLightHelper.turnOff();
            textBoxStatus.Text = "Green Off";
        }

        private void buttonOnRed_Click(object sender, EventArgs e)
        {
            Helpers.RGBLightHelper.turnOnRed();
            textBoxStatus.Text = "Red On";
            System.Threading.Thread.Sleep(3000);
            Helpers.RGBLightHelper.turnOff();
            textBoxStatus.Text = "Red Off";
        }
    }
}
