﻿using DevExpress.XtraBars.Docking2010;
using DevExpress.XtraGrid.Views.Grid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WarehouseManager.Models;
using WarehouseManager.Repository;
using webservice.Entities;

namespace WarehouseManager
{
    public partial class frmSearchWR : Form
    {
        HandlingStatusRepository handlingStatusRepository;
        OrderStatusRepository orderStatusRepository;
        BranchRepository branchRepository;
        ModalityRepository modalityRepository;
        WRHeaderRepository wRHeaderRepository;
        public frmSearchWR()
        {
            InitializeComponent();
            this.Load += frmSearchWR_Load;
            handlingStatusRepository = new HandlingStatusRepository();
            branchRepository = new BranchRepository();
            wRHeaderRepository = new WRHeaderRepository();
            orderStatusRepository = new OrderStatusRepository();
            modalityRepository = new ModalityRepository();
        }

        private void frmSearchWR_Load(object sender, EventArgs e)
        {
            popupMenuSelectBranch();
            popupMenuSelectHandlingStatus();
            popupMenuSelectModality();
            sbLoadDataForGridWRHeaderAsync();
            gridViewWRHeader.DoubleClick += dataGridView_CellDoubleClick;
            simpleButtonFilter.Click += filterButton_Click;
            windowsUIButtonPanel1.ButtonClick += button_Click;
            this.KeyDown += new KeyEventHandler(Form1_KeyDown);

            if (int.Parse(DateTime.Now.Day.ToString()) == 1)
            {
                dateEditFromDate.Text = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day).ToString("yyyy-MM-dd");
            }
            else
            {
                dateEditFromDate.Text = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day - 1).ToString("yyyy-MM-dd");
            }
            
            dateEditToDate.Text = DateTime.Now.ToString("yyyy-MM-dd");
        }

        public async void button_Click(object sender, EventArgs e)
        {
            ButtonEventArgs eArg = (DevExpress.XtraBars.Docking2010.ButtonEventArgs)e;
            String tag = eArg.Button.Properties.Tag.ToString();
            switch (tag)
            {
                case "refesh":
                    sbLoadDataForGridWRHeaderAsync();
                    break;
                case "close":
                    this.Close();
                    break;
            }

        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F5)
            {
                sbLoadDataForGridWRHeaderAsync();
            }
            if (e.KeyCode == Keys.F10)
            {
                this.Close();
            }
        }

        private async Task popupMenuSelectHandlingStatus()
        {
            List<HandlingStatus> handlingStatuses = await handlingStatusRepository.GetAll();
            foreach (HandlingStatus handlingStatus in handlingStatuses)
            {
                comboBoxEditHandlingStatus.Properties.Items.Add(handlingStatus.HandlingStatusName);
            }
            comboBoxEditHandlingStatus.SelectedIndex = 0;
        }

       

        private async Task popupMenuSelectBranch()
        {
            List<Branch> branches = await branchRepository.getAllBranchAsync();
            foreach (Branch branch in branches)
            {
                comboBoxEditBranch.Properties.Items.Add(branch.BranchName);
            }
            comboBoxEditBranch.SelectedIndex = 0;
        }

        private async Task popupMenuSelectModality()
        {
            List<Modality> modalities = await modalityRepository.GetAll();
            foreach (Modality modality in modalities)
            {
                comboBoxEditModality.Properties.Items.Add(modality.ModalityName);
            }
            comboBoxEditModality.SelectedIndex = 0;
        }

        private async Task sbLoadDataForGridWRHeaderAsync()
        {
            List<WRHeader> wRHeaders = await wRHeaderRepository.GetAll();
            gridControlWRHeader.DataSource =wRHeaders;
        }

        private async void dataGridView_CellDoubleClick(object sender, EventArgs e)
        {
            if (gridViewWRHeader.RowCount > 0)
            {
                WRHeader wRHeader = await wRHeaderRepository.GetUnderID((string)(sender as GridView).GetFocusedRowCellValue("WRNumber"));
                WarehouseReceiptControl.selectedWRNumber = (string)(sender as GridView).GetFocusedRowCellValue("WRNumber");
                this.Close();
            }
        }

        private async void filterButton_Click(Object sender,EventArgs e)
        {
            String branchID = $"{int.Parse(comboBoxEditBranch.SelectedIndex.ToString()) - 1:D3}";
            String handlingStatusID = (comboBoxEditHandlingStatus.SelectedIndex - 1).ToString();
            String modalityID = $"{int.Parse(comboBoxEditModality.SelectedIndex.ToString()) + 1:D2}";
            DateTime fromDate = DateTime.Parse(dateEditFromDate.Text);
            DateTime toDate = DateTime.Parse(dateEditToDate.Text);

            List<WRHeader> wRHeadersUnderBranch = new List<WRHeader>();
            List<WRHeader> wRHeadersUnderDate = new List<WRHeader>();
            List<WRHeader> wRHeadersUnderHandlingStatus = new List<WRHeader>();
            List<WRHeader> wRHeadersUnderModality = new List<WRHeader>();
            List<WRHeader> wRHeaders = new List<WRHeader>();
            if (branchID.Equals("<>"))
            {
                wRHeadersUnderBranch = await wRHeaderRepository.GetAll();
            }
            else
            {
                wRHeadersUnderBranch = await wRHeaderRepository.GetUnderBranch(branchID);
            }
            if (handlingStatusID.Equals("<>"))
            {
                wRHeadersUnderHandlingStatus = await wRHeaderRepository.GetAll();
            }
            else
            {
                wRHeadersUnderHandlingStatus = await wRHeaderRepository.GetUnderHandlingStatus(handlingStatusID);
            }
            if (modalityID.Equals("<>"))
            {
                wRHeadersUnderModality = await wRHeaderRepository.GetAll();
            }
            else
            {
                wRHeadersUnderModality = await wRHeaderRepository.GetUnderModality(modalityID);
            }
            wRHeadersUnderDate = await wRHeaderRepository.GetUnderDate(fromDate, toDate);

            for (int i = 0; i < wRHeadersUnderBranch.Count; i++)
            {
                for (int j = 0; j < wRHeadersUnderDate.Count; j++)
                {
                    for (int k = 0; k < wRHeadersUnderHandlingStatus.Count; k++)
                    {
                        for (int h = 0; h < wRHeadersUnderModality.Count; h++)
                        {
                            if (wRHeadersUnderBranch[i].WRNumber.Equals(wRHeadersUnderDate[j].WRNumber)
                            && wRHeadersUnderDate[j].WRNumber.Equals(wRHeadersUnderHandlingStatus[k].WRNumber)
                            && wRHeadersUnderHandlingStatus[k].WRNumber.Equals(wRHeadersUnderModality[h].WRNumber))
                            {
                                wRHeaders.Add(wRHeadersUnderBranch[i]);
                            }
                        }
                    }
                }
            }
            gridControlWRHeader.DataSource = wRHeaders;
        }

    }
}
