﻿using DevExpress.XtraTab;
using DevExpress.XtraTab.ViewInfo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WarehouseManager
{
    public partial class ReportControl : DevExpress.XtraEditors.XtraUserControl
    {
        public ReportControl()
        {
            InitializeComponent();
            tabCreate();
            xtraTabControl1.CloseButtonClick += closeButtonTabpage_Click;
        }

        private void tabCreate()
        {
            WMPublic.TabCreating(xtraTabControl1,"Phiếu nhập kho", "tabRPTWarehouseReceipt", new rptWarehouseReceipt());
        }

        private void closeButtonTabpage_Click(object sender, EventArgs e)
        {
            ClosePageButtonEventArgs EArg = (DevExpress.XtraTab.ViewInfo.ClosePageButtonEventArgs)e;
            string name = EArg.Page.Text;//Get the text of the closed tab
            foreach (XtraTabPage page in xtraTabControl1.TabPages)//Traverse to get the same text as the closed tab
            {
                if (page.Text == name)
                {
                    if (WMPublic.sbMessageCloseTab(this) == true)
                    {
                        xtraTabControl1.TabPages.Remove(page);
                        page.Dispose();
                        return;
                    }
                }
            }
        }
    }
}
