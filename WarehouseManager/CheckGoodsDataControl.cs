﻿using DevExpress.Utils;
using DevExpress.XtraBars.Docking2010;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using OfficeOpenXml.FormulaParsing.Excel.Operators;
using QRCoder;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Media;
using WarehouseManager.Models;
using WarehouseManager.Repository;
using webservice.Entities;

namespace WarehouseManager
{
    public partial class CheckGoodsDataControl : DevExpress.XtraEditors.XtraUserControl
    {
        CGDataHeaderRepository CGDataHeaderRepository;
        CGDataGeneralRepository CGDataGeneralRepository;
        HandlingStatusRepository handlingStatusRepository;
        public static String selectedCGDNumber;
        Timer t = new Timer();

        public CheckGoodsDataControl()
        {
            InitializeComponent();
            CGDataHeaderRepository = new CGDataHeaderRepository();
            CGDataGeneralRepository = new CGDataGeneralRepository();
            handlingStatusRepository = new HandlingStatusRepository();
            popupMenuSelectHandlingStatus();
            sbLoadDataForGridCGDataGeneralAsync(textEditCGDNumber.Text);
            this.Load += CheckGoodsDataControl_Load;
        }

        private void CheckGoodsDataControl_Load(Object sender, EventArgs e)
        {
            windowsUIButtonPanel1.Cursor = Cursors.Hand;
            windowsUIButtonPanel1.ButtonClick += button_Click;
            textEditCGDNumber.KeyDown += new KeyEventHandler(CGDNumber_KeyDown);
            textEditCGDNumber.DoubleClick += textEditWDRNumer_CellDoubleClick;
            gridViewWRDataDetail.CustomDrawCell += grdData_CustomDrawCell;
            simpleButtonOn.Click += butOn_Click;
            simpleButtonOff.Click += butOff_Click;
            t.Tick += new EventHandler(this.Timer_Tick);
            t.Start();
        }

        private async Task popupMenuSelectHandlingStatus()
        {
            List<HandlingStatus> handlingStatuses = await handlingStatusRepository.GetAll();
            foreach (HandlingStatus handlingStatus in handlingStatuses)
            {
                comboBoxEditHandlingStatus.Properties.Items.Add(handlingStatus.HandlingStatusName);
            }
            comboBoxEditHandlingStatus.SelectedIndex = 1;
        }


        private async Task sbLoadDataForGridCGDataGeneralAsync(String CGDNumber)
        {
            List<CGDataGeneral> CGDataGenerals = await CGDataGeneralRepository.GetUnderID(CGDNumber);


            int mMaxRow = 100;
            if (CGDataGenerals.Count < mMaxRow)
            {
                int num = mMaxRow - CGDataGenerals.Count;
                int i = 1;
                while (true)
                {
                    int num2 = i;
                    int num3 = num;
                    if (num2 > num3)
                    {
                        break;
                    }
                    CGDataGenerals.Add(new CGDataGeneral());
                    i++;
                }
            }
            gridControlWRDataDetail.DataSource = CGDataGenerals;
            getTotalGoodsOrgAsync();
            getTotalGoods();
            textTotalQuantity_CustomDraw();
        }


        private async Task getTotalGoodsOrgAsync()
        {
            Decimal totalGoodsOrg = 0;
            List<CGDataGeneral> cGDataGenerals = await CGDataGeneralRepository.GetUnderID(textEditCGDNumber.Text);
            totalGoodsOrg += Convert.ToDecimal(cGDataGenerals.Count);
            textEditTotalGoods.Text = totalGoodsOrg.ToString("0.#####");
        }

        private void getTotalGoods()
        {
            Decimal totalGoods = 0;
            int i = 0;
            while (true)
            {
                int rowHandle = gridViewWRDataDetail.GetRowHandle(i);
                if (gridViewWRDataDetail.GetRowCellValue(rowHandle, "GoodsID") == null)
                {
                    break;
                }
                if (!gridViewWRDataDetail.GetRowCellValue(rowHandle, "Status").Equals(""))
                {
                    totalGoods++;
                }
                i++;
            }
            textEditTotalGoodsChecked.Text = totalGoods.ToString("0.#####");
        }

        private void QRCodeGeneral(String code)
        {
            QRCodeGenerator.ECCLevel eccLevel = QRCodeGenerator.ECCLevel.Q;
            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeData qrCodeData = qrGenerator.CreateQrCode(code, eccLevel);
            QRCode qrCode = new QRCode(qrCodeData);
            Bitmap qrCodeBitmap = qrCode.GetGraphic(2, System.Drawing.Color.Black, System.Drawing.Color.White, new Bitmap(10, 10));
            pictureBox1.Image = qrCodeBitmap;

            //QRCodeGenerator.ECCLevel eccLevel = QRCodeGenerator.ECCLevel.L;
            //using (QRCodeGenerator qrGenerator = new QRCodeGenerator())
            //{
            //    using (QRCodeData qrCodeData = qrGenerator.CreateQrCode(code, eccLevel))
            //    {
            //        using (QRCode qrCode = new QRCode(qrCodeData))
            //        {

            //            pictureBox1.BackgroundImage = qrCode.GetGraphic(20);

            //            this.pictureBox1.Size = new System.Drawing.Size(pictureBox1.Width, pictureBox1.Height);
            //            //Set the SizeMode to center the image.
            //            this.pictureBox1.SizeMode = PictureBoxSizeMode.CenterImage;

            //            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            //        }
            //    }
            //}
        }

        public async Task updateCGDataHeaderAsync()
        {
            CGDataHeader CGDataHeader = await CGDataHeaderRepository.GetUnderID(textEditCGDNumber.Text);
            if (CGDataHeader.HandlingStatusID.Equals("2"))
            {
                MessageBox.Show("Dữ liệu nhập đã duyệt mức 2, không thể chỉnh sửa");
                return;
            }

            Decimal totalQuantityOrg = Decimal.Parse(textEditTotalGoods.Text);
            Decimal totalQuantity = Decimal.Parse(textEditTotalGoodsChecked.Text);
            Decimal subQuantity = totalQuantityOrg - totalQuantity;
            if (subQuantity == totalQuantityOrg)
            {
                CGDataHeader.Status = "0";
            }
            else if (subQuantity < totalQuantityOrg)
            {
                CGDataHeader.Status = "1";
            }
            else if (subQuantity == 0)
            {
                CGDataHeader.Status = "2";
            }
            else
            {
                CGDataHeader.Status = "3";
            }


            CGDataHeader.CGDNumber = textEditCGDNumber.Text;
            CGDataHeader.CGDDate = DateTime.Parse(dateEditCGDDate.DateTime.ToString("yyyy-MM-dd"));
            CGDataHeader.ReferenceNumber = textEditCGDNumber.Text;
            CGDataHeader.WRRNumber = textEditWRRNumber2.Text;
            CGDataHeader.WRRReference = textEditCGDNumber.Text;
            CGDataHeader.HandlingStatusID = (comboBoxEditHandlingStatus.SelectedIndex - 1).ToString();
            CGDataHeader.HandlingStatusName = comboBoxEditHandlingStatus.SelectedItem.ToString();
            CGDataHeader.Note = textEditNote.Text;
            CGDataHeader.TotalQuantity = Decimal.Parse(textEditTotalGoodsChecked.Text);
            CGDataHeader.TotalQuantityOrg = Decimal.Parse(textEditTotalGoods.Text);
            CGDataHeader.UpdatedUserID = WMMessage.User.UserID;
            CGDataHeader.UpdatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));
            CGDataHeader.Status = "OK";

            await CGDataHeaderRepository.Update(CGDataHeader, textEditCGDNumber.Text);
        }


        public async Task updateCGDataGeneralAsync()
        {
            int num = gridViewWRDataDetail.RowCount - 1;
            int i = 0;
            while (true)
            {
                int num2 = i;
                int num3 = num;
                if (num2 > num3 || gridViewWRDataDetail.GetRowCellValue(i, gridViewWRDataDetail.Columns["colGoodsID"]) == "")
                {
                    break;
                }
                int rowHandle = gridViewWRDataDetail.GetRowHandle(i);
                CGDataGeneral CGDataGeneral = new CGDataGeneral();
                CGDataGeneral.CGDNumber = textEditCGDNumber.Text;
                CGDataGeneral.GoodsID = (string?)gridViewWRDataDetail.GetRowCellValue(rowHandle, "GoodsID");
                CGDataGeneral.GoodsName = (string?)gridViewWRDataDetail.GetRowCellValue(rowHandle, "GoodsName");
                CGDataGeneral.Ordinal = i + 1;
                CGDataGeneral.IDCode = "";
                CGDataGeneral.LocationID = (string?)gridViewWRDataDetail.GetRowCellValue(rowHandle, "LocationID");
                CGDataGeneral.Quantity = (Decimal?)gridViewWRDataDetail.GetRowCellValue(rowHandle, "Quantity");
                CGDataGeneral.TotalQuantity = (Decimal?)gridViewWRDataDetail.GetRowCellValue(rowHandle, "TotalQuantity");
                CGDataGeneral.TotalGoods = (Decimal?)gridViewWRDataDetail.GetRowCellValue(rowHandle, "TotalGoods");
                CGDataGeneral.QuantityOrg = (Decimal?)gridViewWRDataDetail.GetRowCellValue(rowHandle, "QuantityOrg");
                CGDataGeneral.TotalQuantityOrg = (Decimal?)gridViewWRDataDetail.GetRowCellValue(rowHandle, "TotalQuantityOrg");
                CGDataGeneral.TotalGoodsOrg = (Decimal?)gridViewWRDataDetail.GetRowCellValue(rowHandle, "TotalGoodsOrg");
                CGDataGeneral.LocationIDOrg = (string?)gridViewWRDataDetail.GetRowCellValue(rowHandle, "LocationIDOrg");
                CGDataGeneral.EditerID = WMMessage.User.UserID; ;
                CGDataGeneral.EditedDateTime = DateTime.Now.ToString("yyyy-MM-dd");
                CGDataGeneral.Status = (string?)gridViewWRDataDetail.GetRowCellValue(rowHandle, "Status");
                CGDataGeneral.PackingVolume = (Decimal?)gridViewWRDataDetail.GetRowCellValue(rowHandle, "PackingVolume");
                CGDataGeneral.QuantityByPack = (Decimal?)gridViewWRDataDetail.GetRowCellValue(rowHandle, "QuantityByPack");
                CGDataGeneral.QuantityByItem = (Decimal?)gridViewWRDataDetail.GetRowCellValue(rowHandle, "QuantityByItem");
                CGDataGeneral.Note = textEditNote.Text;
                CGDataGeneral.ScanOption = (Int16?)gridViewWRDataDetail.GetRowCellValue(rowHandle, "ScanOption");
                CGDataGeneral.PackingQuantity = (Decimal?)gridViewWRDataDetail.GetRowCellValue(rowHandle, "PackingQuantity");
                if (await CGDataGeneralRepository.Update(CGDataGeneral, textEditCGDNumber.Text, CGDataGeneral.GoodsID, CGDataGeneral.Ordinal) > 0)
                {
                    i++;
                }
            }
        }

        public async Task DeleteCGDataHeaderAsync()
        {
            if ((await CGDataHeaderRepository.Delete(textEditCGDNumber.Text)) > 0)
            {
                await DeleteCGDataGeneralAsync();
                sbLoadDataForGridCGDataGeneralAsync(textEditCGDNumber.Text);
                WMPublic.sbMessageDeleteSuccess(this);
            }
        }


        public async Task DeleteCGDataGeneralAsync()
        {
            await CGDataGeneralRepository.Delete(textEditCGDNumber.Text);
            clearAsync();
            gridControlWRDataDetail.DataSource = null;
        }

        private void clearAsync()
        {
            textEditCGDNumber.Text = "";
            dateEditCGDDate.Text = DateTime.Now.ToString("dd-MM-yyyy");
            comboBoxEditHandlingStatus.SelectedIndex = 1;
            textEditNote.Text = "";
            textEditWRRNumber2.Text = "";
            textEditTotalGoods.Text = "0";
            textEditTotalGoodsChecked.Text = "0";
            List<WRDataDetail> wRDataDetails = new List<WRDataDetail>();
            List<CGDataGeneral> CGDataGenerals = new List<CGDataGeneral>();
            for (int i = 0; i < 100; i++)
            {
                CGDataGenerals.Add(new CGDataGeneral());
            }
            gridControlWRDataDetail.DataSource = CGDataGenerals;
        }

        private async void CGDNumber_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {

                CGDataHeader CGDataHeader = new CGDataHeader();
                CGDataHeader = await CGDataHeaderRepository.GetUnderID(textEditCGDNumber.Text);
                if (CGDataHeader != null)
                {
                    textEditWRRNumber2.Text = CGDataHeader.WRRNumber;
                    dateEditCGDDate.Text = CGDataHeader.CGDDate.ToString();
                    textEditNote.Text = CGDataHeader.Note;
                    comboBoxEditHandlingStatus.SelectedIndex = int.Parse(CGDataHeader.HandlingStatusID) + 1;
                    sbLoadDataForGridCGDataGeneralAsync(textEditCGDNumber.Text);
                }
                else
                {
                    MessageBox.Show("Số phiếu điền hàng không tồn tại");
                    return;
                }

            }
        }

        private async void textEditWDRNumer_CellDoubleClick(object sender, EventArgs e)
        {
            frmSearchCGD frmSearchCGD = new frmSearchCGD();
            frmSearchCGD.ShowDialog(this);
            frmSearchCGD.Dispose();
            if (selectedCGDNumber != null)
            {
                textEditCGDNumber.Text = selectedCGDNumber;
                CGDataHeader CGDataHeader = await CGDataHeaderRepository.GetUnderID(textEditCGDNumber.Text);
                if (CGDataHeader != null)
                {
                    textEditWRRNumber2.Text = CGDataHeader.WRRNumber;
                    dateEditCGDDate.Text = CGDataHeader.CGDDate.ToString();
                    textEditNote.Text = CGDataHeader.Note;
                    comboBoxEditHandlingStatus.SelectedIndex = int.Parse(CGDataHeader.HandlingStatusID) + 1;
                    await sbLoadDataForGridCGDataGeneralAsync(textEditCGDNumber.Text);
                }
                QRCodeGeneral(selectedCGDNumber);
            }

        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            if (textEditCGDNumber.Text != "")
            {
                sbLoadDataForGridCGDataGeneralAsync(textEditCGDNumber.Text);
            }
        }

        private void grdData_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            int i = 0;
            i = e.RowHandle;
            if ((i >= 0) && (String.Compare(e.Column.FieldName, "Status", false) == 0))
            {
                string mStatus =(string) ((gridViewWRDataDetail.GetRowCellValue(i, gridViewWRDataDetail.Columns["Status"])=="") ?"" :gridViewWRDataDetail.GetRowCellValue(i, gridViewWRDataDetail.Columns["Status"]));
                e.Appearance.Font = new Font("Tahoma", 8f, FontStyle.Bold);
                if (mStatus=="checkOK")
                {
                    e.Appearance.BackColor = System.Drawing.Color.LightGreen;
                }
            }
        }

        private void textTotalQuantity_CustomDraw()
        {
            //if (String.Compare(textEditTotalQuantityOrg.Text, "", false) == 0)
            //{
            double mTotalQuantity = ((textEditTotalGoodsChecked.Text == "") ? 0.0 : Convert.ToDouble(textEditTotalGoodsChecked.Text));
            double mTotalQuantityOrg = ((textEditTotalGoods.Text == "") ? 0.0 : Convert.ToDouble(textEditTotalGoods.Text));
            textEditTotalGoodsChecked.Font = new Font("Tahoma", 8f, FontStyle.Bold);
            textEditTotalGoods.Font = new Font("Tahoma", 8f, FontStyle.Bold);
            if (mTotalQuantity == mTotalQuantityOrg && mTotalQuantity != 0.0)
            {
                textEditTotalGoodsChecked.BackColor = System.Drawing.Color.LightGreen;
                textEditTotalGoods.BackColor = System.Drawing.Color.LightGreen;
            }
            if (mTotalQuantity > mTotalQuantityOrg && mTotalQuantity != 0.0)
            {
                textEditTotalGoodsChecked.BackColor = System.Drawing.Color.Yellow;
                textEditTotalGoods.BackColor = System.Drawing.Color.LightGreen;
            }
            if (mTotalQuantity < mTotalQuantityOrg && mTotalQuantity != 0.0)
            {
                textEditTotalGoodsChecked.BackColor = System.Drawing.Color.Red;
                textEditTotalGoods.BackColor = System.Drawing.Color.LightGreen;
            }
            //}
        }

        private void butOn_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Bắt đầu quét điền hàng");
            t.Enabled = true;
        }

        private void butOff_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Kết thúc quét điền hàng");
            t.Enabled = false;
        }

        //barbutton click
        public async void button_Click(object sender, EventArgs e)
        {
            ButtonEventArgs eArg = (DevExpress.XtraBars.Docking2010.ButtonEventArgs)e;
            String tag = eArg.Button.Properties.Tag.ToString();
            switch (tag)
            {
                case "new":
                    if (WMPublic.sbMessageAddNewRequest(this) == true)
                    {
                        clearAsync();
                    }
                    break;
                case "save":
                    if (WMPublic.sbMessageSaveChangeRequest(this) == true)
                    {
                        updateCGDataHeaderAsync();
                    }
                    break;
                case "delete":
                    if (WMPublic.sbMessageDeleteRequest(this) == true)
                    {
                        DeleteCGDataHeaderAsync();
                    }
                    break;
                case "search":

                    break;
                case "refesh":
                    sbLoadDataForGridCGDataGeneralAsync(textEditCGDNumber.Text);
                    break;
                case "import":
                    break;
                case "export":
                    break;
                case "print":
                    break;
                case "close":
                    Demo demo = new Demo();
                    demo.Show();
                    break;
            }

        }


    }
}
