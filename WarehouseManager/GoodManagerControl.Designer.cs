﻿
namespace WarehouseManager
{
    partial class GoodManagerControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions1 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GoodManagerControl));
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions2 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions3 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions4 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions5 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions6 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions7 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions8 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions9 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition1 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition2 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition3 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition4 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition5 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition6 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition7 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition1 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition2 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition3 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition4 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition5 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition6 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition8 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition9 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition10 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition7 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition8 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition9 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition10 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition11 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition12 = new DevExpress.XtraLayout.RowDefinition();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.windowsUIButtonPanel1 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.xtraTabControlGoods = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPageGoodsGeneral = new DevExpress.XtraTab.XtraTabPage();
            this.layoutControl4 = new DevExpress.XtraLayout.LayoutControl();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textEditGoodsID = new DevExpress.XtraEditors.TextEdit();
            this.textEditGoodsName = new DevExpress.XtraEditors.TextEdit();
            this.textEditOtherGoodsName = new DevExpress.XtraEditors.TextEdit();
            this.textEditStockUnitID = new DevExpress.XtraEditors.TextEdit();
            this.comboBoxEditGoodsType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEditGoodsLine = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEditGoodsGroup = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEditGoodsCategory = new DevExpress.XtraEditors.TextEdit();
            this.textEditDescription = new DevExpress.XtraEditors.TextEdit();
            this.textEditInternalGoodsID = new DevExpress.XtraEditors.TextEdit();
            this.textEditGeneralGoodsID = new DevExpress.XtraEditors.TextEdit();
            this.textEditModel = new DevExpress.XtraEditors.TextEdit();
            this.textEditSerialNumber = new DevExpress.XtraEditors.TextEdit();
            this.textEditPacking = new DevExpress.XtraEditors.TextEdit();
            this.textEditProductionDate = new DevExpress.XtraEditors.TextEdit();
            this.textEditExpiryDate = new DevExpress.XtraEditors.TextEdit();
            this.textEditWarrantyEndDate = new DevExpress.XtraEditors.TextEdit();
            this.checkEditScanOption = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditPLUType = new DevExpress.XtraEditors.CheckEdit();
            this.comboBoxEditGoodsStatus = new DevExpress.XtraEditors.ComboBoxEdit();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemGoodsID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemGoodsName = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemOtherGoodsName = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemStockUnitID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemGoodsType = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemGoodsLine = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemGoodsGroup = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemGoodsCategory = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemInternalGoodsID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemGeneralGoodsID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemModel = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemSerialNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemPacking = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemProductionDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemExpiryDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemWarrantyEndDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemGoodsStatus = new DevExpress.XtraLayout.LayoutControlItem();
            this.xtraTabPageGoodsOther = new DevExpress.XtraTab.XtraTabPage();
            this.layoutControl3 = new DevExpress.XtraLayout.LayoutControl();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.xtraTabPagePriceTax = new DevExpress.XtraTab.XtraTabPage();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.goodsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.wARHOUSE_HPDataSet = new WarehouseManager.WARHOUSE_HPDataSet();
            this.goodsTableAdapter = new WarehouseManager.WARHOUSE_HPDataSetTableAdapters.GoodsTableAdapter();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.layoutControl5 = new DevExpress.XtraLayout.LayoutControl();
            this.gridControlGoods = new DevExpress.XtraGrid.GridControl();
            this.gridViewGoods = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGoodsID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGoodsName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOtherGoodsName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStockUnitID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGoodsTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGoodsLineID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGoodsGroupID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGoodsCategoryID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInternalGoodsID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGeneralGoodsID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colModel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSerialNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProductionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpiryDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWarrantyEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colScanOption = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPLUType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPacking = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGoodsStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSupplierID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colManufacturerID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPurchaseUnitID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPurchaseUnitRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRetailUnitID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRetailUnitRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWholesaleUnitID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWholesaleUnitRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStyleID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSizeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colColorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSeasonID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMaterialID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLength = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLengthUnit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWidth = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWidthUnit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHeight = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHeightUnit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWeight = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWeightUnit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDiameter = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDiameterUnit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGauge = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGaugeUnit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVolume = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVolumeUnit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDensity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVATInput = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVATOutput = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colImportTax = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpecialTax = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTransportRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOtherRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDiscountInput = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDiscountOutput = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRetailSalePrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWholesalePrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInternalSalePrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMargin = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDiscountInternal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMinQuantity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMaxQuantity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedUserID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUpdatedUserID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUpdatedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControlGoods)).BeginInit();
            this.xtraTabControlGoods.SuspendLayout();
            this.xtraTabPageGoodsGeneral.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl4)).BeginInit();
            this.layoutControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditGoodsID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditGoodsName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditOtherGoodsName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditStockUnitID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditGoodsType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditGoodsLine.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditGoodsGroup.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditGoodsCategory.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditDescription.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditInternalGoodsID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditGeneralGoodsID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditModel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditSerialNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPacking.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditProductionDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditExpiryDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditWarrantyEndDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditScanOption.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditPLUType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditGoodsStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemGoodsID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemGoodsName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemOtherGoodsName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemStockUnitID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemGoodsType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemGoodsLine)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemGoodsGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemGoodsCategory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemInternalGoodsID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemGeneralGoodsID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemModel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemSerialNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPacking)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemProductionDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemExpiryDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWarrantyEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemGoodsStatus)).BeginInit();
            this.xtraTabPageGoodsOther.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.goodsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wARHOUSE_HPDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl5)).BeginInit();
            this.layoutControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlGoods)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewGoods)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.windowsUIButtonPanel1);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(800, 204, 812, 500);
            this.layoutControl1.Root = this.Root;
            this.layoutControl1.Size = new System.Drawing.Size(1451, 55);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // windowsUIButtonPanel1
            // 
            this.windowsUIButtonPanel1.AllowGlyphSkinning = false;
            this.windowsUIButtonPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            windowsUIButtonImageOptions1.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions1.Image")));
            windowsUIButtonImageOptions2.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions2.Image")));
            windowsUIButtonImageOptions3.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions3.Image")));
            windowsUIButtonImageOptions4.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions4.Image")));
            windowsUIButtonImageOptions5.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions5.Image")));
            windowsUIButtonImageOptions6.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions6.Image")));
            windowsUIButtonImageOptions7.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions7.Image")));
            windowsUIButtonImageOptions8.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions8.Image")));
            windowsUIButtonImageOptions9.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions9.Image")));
            this.windowsUIButtonPanel1.Buttons.AddRange(new DevExpress.XtraEditors.ButtonPanel.IBaseButton[] {
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("Thêm mới", true, windowsUIButtonImageOptions1, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "new", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("Lưu", true, windowsUIButtonImageOptions2, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "save", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("Xóa", true, windowsUIButtonImageOptions3, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "delete", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("Tìm kiếm", true, windowsUIButtonImageOptions4, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "search", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("Làm mới", true, windowsUIButtonImageOptions5, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "refesh", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("Nhập dữ liệu", true, windowsUIButtonImageOptions6, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "import", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("Xuất dữ liệu", true, windowsUIButtonImageOptions7, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "export", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("In ấn", true, windowsUIButtonImageOptions8, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "print", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("Thoát", true, windowsUIButtonImageOptions9, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "close", -1, false)});
            this.windowsUIButtonPanel1.ContentAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.windowsUIButtonPanel1.Location = new System.Drawing.Point(2, 2);
            this.windowsUIButtonPanel1.Name = "windowsUIButtonPanel1";
            this.windowsUIButtonPanel1.Padding = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.windowsUIButtonPanel1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.windowsUIButtonPanel1.Size = new System.Drawing.Size(1430, 47);
            this.windowsUIButtonPanel1.TabIndex = 4;
            this.windowsUIButtonPanel1.Text = "windowsUIButtonPanel1";
            this.windowsUIButtonPanel1.UseButtonBackgroundImages = false;
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.emptySpaceItem1});
            this.Root.Name = "Root";
            this.Root.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.Root.Size = new System.Drawing.Size(1434, 61);
            this.Root.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.windowsUIButtonPanel1;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(1434, 51);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 51);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(1434, 10);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.xtraTabControlGoods);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl2.Location = new System.Drawing.Point(0, 55);
            this.layoutControl2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(972, 257, 812, 500);
            this.layoutControl2.Root = this.layoutControlGroup1;
            this.layoutControl2.Size = new System.Drawing.Size(1451, 237);
            this.layoutControl2.TabIndex = 1;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // xtraTabControlGoods
            // 
            this.xtraTabControlGoods.Location = new System.Drawing.Point(11, 10);
            this.xtraTabControlGoods.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.xtraTabControlGoods.Name = "xtraTabControlGoods";
            this.xtraTabControlGoods.SelectedTabPage = this.xtraTabPageGoodsGeneral;
            this.xtraTabControlGoods.Size = new System.Drawing.Size(1429, 217);
            this.xtraTabControlGoods.TabIndex = 4;
            this.xtraTabControlGoods.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPageGoodsGeneral,
            this.xtraTabPageGoodsOther,
            this.xtraTabPagePriceTax});
            // 
            // xtraTabPageGoodsGeneral
            // 
            this.xtraTabPageGoodsGeneral.Controls.Add(this.layoutControl4);
            this.xtraTabPageGoodsGeneral.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.xtraTabPageGoodsGeneral.Name = "xtraTabPageGoodsGeneral";
            this.xtraTabPageGoodsGeneral.Size = new System.Drawing.Size(1424, 191);
            this.xtraTabPageGoodsGeneral.Text = "Thông tin chung";
            // 
            // layoutControl4
            // 
            this.layoutControl4.Controls.Add(this.label3);
            this.layoutControl4.Controls.Add(this.label2);
            this.layoutControl4.Controls.Add(this.label1);
            this.layoutControl4.Controls.Add(this.textEditGoodsID);
            this.layoutControl4.Controls.Add(this.textEditGoodsName);
            this.layoutControl4.Controls.Add(this.textEditOtherGoodsName);
            this.layoutControl4.Controls.Add(this.textEditStockUnitID);
            this.layoutControl4.Controls.Add(this.comboBoxEditGoodsType);
            this.layoutControl4.Controls.Add(this.comboBoxEditGoodsLine);
            this.layoutControl4.Controls.Add(this.comboBoxEditGoodsGroup);
            this.layoutControl4.Controls.Add(this.textEditGoodsCategory);
            this.layoutControl4.Controls.Add(this.textEditDescription);
            this.layoutControl4.Controls.Add(this.textEditInternalGoodsID);
            this.layoutControl4.Controls.Add(this.textEditGeneralGoodsID);
            this.layoutControl4.Controls.Add(this.textEditModel);
            this.layoutControl4.Controls.Add(this.textEditSerialNumber);
            this.layoutControl4.Controls.Add(this.textEditPacking);
            this.layoutControl4.Controls.Add(this.textEditProductionDate);
            this.layoutControl4.Controls.Add(this.textEditExpiryDate);
            this.layoutControl4.Controls.Add(this.textEditWarrantyEndDate);
            this.layoutControl4.Controls.Add(this.checkEditScanOption);
            this.layoutControl4.Controls.Add(this.checkEditPLUType);
            this.layoutControl4.Controls.Add(this.comboBoxEditGoodsStatus);
            this.layoutControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl4.Location = new System.Drawing.Point(0, 0);
            this.layoutControl4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.layoutControl4.Name = "layoutControl4";
            this.layoutControl4.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1016, 148, 812, 500);
            this.layoutControl4.Root = this.layoutControlGroup3;
            this.layoutControl4.Size = new System.Drawing.Size(1424, 191);
            this.layoutControl4.TabIndex = 0;
            this.layoutControl4.Text = "layoutControl4";
            // 
            // label3
            // 
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(311, 97);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(25, 25);
            this.label3.TabIndex = 5;
            this.label3.Text = "(*)";
            // 
            // label2
            // 
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(527, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(25, 25);
            this.label2.TabIndex = 5;
            this.label2.Text = "(*)";
            // 
            // label1
            // 
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(311, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(25, 25);
            this.label1.TabIndex = 5;
            this.label1.Text = "(*)";
            // 
            // textEditGoodsID
            // 
            this.textEditGoodsID.Location = new System.Drawing.Point(125, 10);
            this.textEditGoodsID.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textEditGoodsID.Name = "textEditGoodsID";
            this.textEditGoodsID.Size = new System.Drawing.Size(182, 20);
            this.textEditGoodsID.StyleController = this.layoutControl4;
            this.textEditGoodsID.TabIndex = 4;
            // 
            // textEditGoodsName
            // 
            this.textEditGoodsName.Location = new System.Drawing.Point(125, 39);
            this.textEditGoodsName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textEditGoodsName.Name = "textEditGoodsName";
            this.textEditGoodsName.Size = new System.Drawing.Size(398, 20);
            this.textEditGoodsName.StyleController = this.layoutControl4;
            this.textEditGoodsName.TabIndex = 6;
            // 
            // textEditOtherGoodsName
            // 
            this.textEditOtherGoodsName.Location = new System.Drawing.Point(125, 68);
            this.textEditOtherGoodsName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textEditOtherGoodsName.Name = "textEditOtherGoodsName";
            this.textEditOtherGoodsName.Size = new System.Drawing.Size(398, 20);
            this.textEditOtherGoodsName.StyleController = this.layoutControl4;
            this.textEditOtherGoodsName.TabIndex = 7;
            // 
            // textEditStockUnitID
            // 
            this.textEditStockUnitID.Location = new System.Drawing.Point(125, 97);
            this.textEditStockUnitID.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textEditStockUnitID.Name = "textEditStockUnitID";
            this.textEditStockUnitID.Size = new System.Drawing.Size(182, 20);
            this.textEditStockUnitID.StyleController = this.layoutControl4;
            this.textEditStockUnitID.TabIndex = 8;
            // 
            // comboBoxEditGoodsType
            // 
            this.comboBoxEditGoodsType.Location = new System.Drawing.Point(454, 97);
            this.comboBoxEditGoodsType.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboBoxEditGoodsType.Name = "comboBoxEditGoodsType";
            this.comboBoxEditGoodsType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditGoodsType.Size = new System.Drawing.Size(69, 20);
            this.comboBoxEditGoodsType.StyleController = this.layoutControl4;
            this.comboBoxEditGoodsType.TabIndex = 9;
            // 
            // comboBoxEditGoodsLine
            // 
            this.comboBoxEditGoodsLine.Location = new System.Drawing.Point(125, 126);
            this.comboBoxEditGoodsLine.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboBoxEditGoodsLine.Name = "comboBoxEditGoodsLine";
            this.comboBoxEditGoodsLine.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditGoodsLine.Size = new System.Drawing.Size(398, 20);
            this.comboBoxEditGoodsLine.StyleController = this.layoutControl4;
            this.comboBoxEditGoodsLine.TabIndex = 10;
            // 
            // comboBoxEditGoodsGroup
            // 
            this.comboBoxEditGoodsGroup.Location = new System.Drawing.Point(125, 155);
            this.comboBoxEditGoodsGroup.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboBoxEditGoodsGroup.Name = "comboBoxEditGoodsGroup";
            this.comboBoxEditGoodsGroup.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditGoodsGroup.Size = new System.Drawing.Size(398, 20);
            this.comboBoxEditGoodsGroup.StyleController = this.layoutControl4;
            this.comboBoxEditGoodsGroup.TabIndex = 11;
            // 
            // textEditGoodsCategory
            // 
            this.textEditGoodsCategory.Location = new System.Drawing.Point(670, 10);
            this.textEditGoodsCategory.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textEditGoodsCategory.Name = "textEditGoodsCategory";
            this.textEditGoodsCategory.Size = new System.Drawing.Size(456, 20);
            this.textEditGoodsCategory.StyleController = this.layoutControl4;
            this.textEditGoodsCategory.TabIndex = 12;
            // 
            // textEditDescription
            // 
            this.textEditDescription.Location = new System.Drawing.Point(670, 39);
            this.textEditDescription.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textEditDescription.Name = "textEditDescription";
            this.textEditDescription.Size = new System.Drawing.Size(743, 20);
            this.textEditDescription.StyleController = this.layoutControl4;
            this.textEditDescription.TabIndex = 14;
            // 
            // textEditInternalGoodsID
            // 
            this.textEditInternalGoodsID.Location = new System.Drawing.Point(670, 68);
            this.textEditInternalGoodsID.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textEditInternalGoodsID.Name = "textEditInternalGoodsID";
            this.textEditInternalGoodsID.Size = new System.Drawing.Size(169, 20);
            this.textEditInternalGoodsID.StyleController = this.layoutControl4;
            this.textEditInternalGoodsID.TabIndex = 15;
            // 
            // textEditGeneralGoodsID
            // 
            this.textEditGeneralGoodsID.Location = new System.Drawing.Point(670, 97);
            this.textEditGeneralGoodsID.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textEditGeneralGoodsID.Name = "textEditGeneralGoodsID";
            this.textEditGeneralGoodsID.Size = new System.Drawing.Size(169, 20);
            this.textEditGeneralGoodsID.StyleController = this.layoutControl4;
            this.textEditGeneralGoodsID.TabIndex = 16;
            // 
            // textEditModel
            // 
            this.textEditModel.Location = new System.Drawing.Point(670, 126);
            this.textEditModel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textEditModel.Name = "textEditModel";
            this.textEditModel.Size = new System.Drawing.Size(169, 20);
            this.textEditModel.StyleController = this.layoutControl4;
            this.textEditModel.TabIndex = 17;
            // 
            // textEditSerialNumber
            // 
            this.textEditSerialNumber.Location = new System.Drawing.Point(670, 155);
            this.textEditSerialNumber.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textEditSerialNumber.Name = "textEditSerialNumber";
            this.textEditSerialNumber.Size = new System.Drawing.Size(169, 20);
            this.textEditSerialNumber.StyleController = this.layoutControl4;
            this.textEditSerialNumber.TabIndex = 18;
            // 
            // textEditPacking
            // 
            this.textEditPacking.Location = new System.Drawing.Point(957, 68);
            this.textEditPacking.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textEditPacking.Name = "textEditPacking";
            this.textEditPacking.Size = new System.Drawing.Size(456, 20);
            this.textEditPacking.StyleController = this.layoutControl4;
            this.textEditPacking.TabIndex = 19;
            // 
            // textEditProductionDate
            // 
            this.textEditProductionDate.Location = new System.Drawing.Point(957, 97);
            this.textEditProductionDate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textEditProductionDate.Name = "textEditProductionDate";
            this.textEditProductionDate.Size = new System.Drawing.Size(169, 20);
            this.textEditProductionDate.StyleController = this.layoutControl4;
            this.textEditProductionDate.TabIndex = 20;
            // 
            // textEditExpiryDate
            // 
            this.textEditExpiryDate.Location = new System.Drawing.Point(957, 126);
            this.textEditExpiryDate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textEditExpiryDate.Name = "textEditExpiryDate";
            this.textEditExpiryDate.Size = new System.Drawing.Size(169, 20);
            this.textEditExpiryDate.StyleController = this.layoutControl4;
            this.textEditExpiryDate.TabIndex = 21;
            // 
            // textEditWarrantyEndDate
            // 
            this.textEditWarrantyEndDate.Location = new System.Drawing.Point(957, 155);
            this.textEditWarrantyEndDate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textEditWarrantyEndDate.Name = "textEditWarrantyEndDate";
            this.textEditWarrantyEndDate.Size = new System.Drawing.Size(169, 20);
            this.textEditWarrantyEndDate.StyleController = this.layoutControl4;
            this.textEditWarrantyEndDate.TabIndex = 22;
            // 
            // checkEditScanOption
            // 
            this.checkEditScanOption.Location = new System.Drawing.Point(1130, 97);
            this.checkEditScanOption.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkEditScanOption.Name = "checkEditScanOption";
            this.checkEditScanOption.Properties.Caption = "Quét mã";
            this.checkEditScanOption.Size = new System.Drawing.Size(283, 19);
            this.checkEditScanOption.StyleController = this.layoutControl4;
            this.checkEditScanOption.TabIndex = 23;
            // 
            // checkEditPLUType
            // 
            this.checkEditPLUType.Location = new System.Drawing.Point(1130, 126);
            this.checkEditPLUType.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkEditPLUType.Name = "checkEditPLUType";
            this.checkEditPLUType.Properties.Caption = "Loại PLU";
            this.checkEditPLUType.Size = new System.Drawing.Size(283, 19);
            this.checkEditPLUType.StyleController = this.layoutControl4;
            this.checkEditPLUType.TabIndex = 24;
            // 
            // comboBoxEditGoodsStatus
            // 
            this.comboBoxEditGoodsStatus.Location = new System.Drawing.Point(1244, 10);
            this.comboBoxEditGoodsStatus.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboBoxEditGoodsStatus.Name = "comboBoxEditGoodsStatus";
            this.comboBoxEditGoodsStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditGoodsStatus.Size = new System.Drawing.Size(169, 20);
            this.comboBoxEditGoodsStatus.StyleController = this.layoutControl4;
            this.comboBoxEditGoodsStatus.TabIndex = 25;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemGoodsID,
            this.layoutControlItem3,
            this.layoutControlItemGoodsName,
            this.layoutControlItem4,
            this.layoutControlItemOtherGoodsName,
            this.layoutControlItemStockUnitID,
            this.layoutControlItem7,
            this.layoutControlItemGoodsType,
            this.layoutControlItemGoodsLine,
            this.layoutControlItemGoodsGroup,
            this.layoutControlItemGoodsCategory,
            this.layoutControlItemDescription,
            this.layoutControlItemInternalGoodsID,
            this.layoutControlItemGeneralGoodsID,
            this.layoutControlItemModel,
            this.layoutControlItemSerialNumber,
            this.layoutControlItemPacking,
            this.layoutControlItemProductionDate,
            this.layoutControlItemExpiryDate,
            this.layoutControlItemWarrantyEndDate,
            this.layoutControlItem14,
            this.layoutControlItem15,
            this.layoutControlItemGoodsStatus});
            this.layoutControlGroup3.LayoutMode = DevExpress.XtraLayout.Utils.LayoutMode.Table;
            this.layoutControlGroup3.Name = "Root";
            this.layoutControlGroup3.OptionsPrint.AppearanceItem.BackColor = System.Drawing.Color.Red;
            this.layoutControlGroup3.OptionsPrint.AppearanceItem.BackColor2 = System.Drawing.Color.Red;
            this.layoutControlGroup3.OptionsPrint.AppearanceItem.BorderColor = System.Drawing.Color.Red;
            this.layoutControlGroup3.OptionsPrint.AppearanceItem.ForeColor = System.Drawing.Color.Red;
            this.layoutControlGroup3.OptionsPrint.AppearanceItem.Options.UseBackColor = true;
            this.layoutControlGroup3.OptionsPrint.AppearanceItem.Options.UseBorderColor = true;
            this.layoutControlGroup3.OptionsPrint.AppearanceItem.Options.UseForeColor = true;
            columnDefinition1.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition1.Width = 21.428571428571431D;
            columnDefinition2.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition2.Width = 2.0408163265306123D;
            columnDefinition3.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition3.Width = 13.26530612244898D;
            columnDefinition4.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition4.Width = 2.0408163265306123D;
            columnDefinition5.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition5.Width = 20.408163265306122D;
            columnDefinition6.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition6.Width = 20.408163265306122D;
            columnDefinition7.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition7.Width = 20.408163265306122D;
            this.layoutControlGroup3.OptionsTableLayoutGroup.ColumnDefinitions.AddRange(new DevExpress.XtraLayout.ColumnDefinition[] {
            columnDefinition1,
            columnDefinition2,
            columnDefinition3,
            columnDefinition4,
            columnDefinition5,
            columnDefinition6,
            columnDefinition7});
            rowDefinition1.Height = 16.666666666666668D;
            rowDefinition1.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition2.Height = 16.666666666666668D;
            rowDefinition2.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition3.Height = 16.666666666666668D;
            rowDefinition3.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition4.Height = 16.666666666666668D;
            rowDefinition4.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition5.Height = 16.666666666666668D;
            rowDefinition5.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition6.Height = 16.666666666666668D;
            rowDefinition6.SizeType = System.Windows.Forms.SizeType.Percent;
            this.layoutControlGroup3.OptionsTableLayoutGroup.RowDefinitions.AddRange(new DevExpress.XtraLayout.RowDefinition[] {
            rowDefinition1,
            rowDefinition2,
            rowDefinition3,
            rowDefinition4,
            rowDefinition5,
            rowDefinition6});
            this.layoutControlGroup3.Size = new System.Drawing.Size(1424, 191);
            this.layoutControlGroup3.TextVisible = false;
            // 
            // layoutControlItemGoodsID
            // 
            this.layoutControlItemGoodsID.Control = this.textEditGoodsID;
            this.layoutControlItemGoodsID.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemGoodsID.Name = "layoutControlItemGoodsID";
            this.layoutControlItemGoodsID.Size = new System.Drawing.Size(300, 29);
            this.layoutControlItemGoodsID.Text = "Mã hàng hóa";
            this.layoutControlItemGoodsID.TextSize = new System.Drawing.Size(111, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.label1;
            this.layoutControlItem3.Location = new System.Drawing.Point(300, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.OptionsTableLayoutItem.ColumnIndex = 1;
            this.layoutControlItem3.Size = new System.Drawing.Size(29, 29);
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItemGoodsName
            // 
            this.layoutControlItemGoodsName.Control = this.textEditGoodsName;
            this.layoutControlItemGoodsName.Location = new System.Drawing.Point(0, 29);
            this.layoutControlItemGoodsName.Name = "layoutControlItemGoodsName";
            this.layoutControlItemGoodsName.OptionsTableLayoutItem.ColumnSpan = 3;
            this.layoutControlItemGoodsName.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItemGoodsName.Size = new System.Drawing.Size(516, 29);
            this.layoutControlItemGoodsName.Text = "Tên hàng hóa";
            this.layoutControlItemGoodsName.TextSize = new System.Drawing.Size(111, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.label2;
            this.layoutControlItem4.Location = new System.Drawing.Point(516, 29);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.OptionsTableLayoutItem.ColumnIndex = 3;
            this.layoutControlItem4.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItem4.Size = new System.Drawing.Size(29, 29);
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItemOtherGoodsName
            // 
            this.layoutControlItemOtherGoodsName.Control = this.textEditOtherGoodsName;
            this.layoutControlItemOtherGoodsName.Location = new System.Drawing.Point(0, 58);
            this.layoutControlItemOtherGoodsName.Name = "layoutControlItemOtherGoodsName";
            this.layoutControlItemOtherGoodsName.OptionsTableLayoutItem.ColumnSpan = 3;
            this.layoutControlItemOtherGoodsName.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlItemOtherGoodsName.Size = new System.Drawing.Size(516, 29);
            this.layoutControlItemOtherGoodsName.Text = "Tên khác của hàng hóa";
            this.layoutControlItemOtherGoodsName.TextSize = new System.Drawing.Size(111, 13);
            // 
            // layoutControlItemStockUnitID
            // 
            this.layoutControlItemStockUnitID.Control = this.textEditStockUnitID;
            this.layoutControlItemStockUnitID.Location = new System.Drawing.Point(0, 87);
            this.layoutControlItemStockUnitID.Name = "layoutControlItemStockUnitID";
            this.layoutControlItemStockUnitID.OptionsTableLayoutItem.RowIndex = 3;
            this.layoutControlItemStockUnitID.Size = new System.Drawing.Size(300, 29);
            this.layoutControlItemStockUnitID.Text = "Đơn vị kho";
            this.layoutControlItemStockUnitID.TextSize = new System.Drawing.Size(111, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.label3;
            this.layoutControlItem7.Location = new System.Drawing.Point(300, 87);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.OptionsTableLayoutItem.ColumnIndex = 1;
            this.layoutControlItem7.OptionsTableLayoutItem.RowIndex = 3;
            this.layoutControlItem7.Size = new System.Drawing.Size(29, 29);
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlItemGoodsType
            // 
            this.layoutControlItemGoodsType.Control = this.comboBoxEditGoodsType;
            this.layoutControlItemGoodsType.Location = new System.Drawing.Point(329, 87);
            this.layoutControlItemGoodsType.Name = "layoutControlItemGoodsType";
            this.layoutControlItemGoodsType.OptionsTableLayoutItem.ColumnIndex = 2;
            this.layoutControlItemGoodsType.OptionsTableLayoutItem.RowIndex = 3;
            this.layoutControlItemGoodsType.Size = new System.Drawing.Size(187, 29);
            this.layoutControlItemGoodsType.Text = "Loại";
            this.layoutControlItemGoodsType.TextSize = new System.Drawing.Size(111, 13);
            // 
            // layoutControlItemGoodsLine
            // 
            this.layoutControlItemGoodsLine.Control = this.comboBoxEditGoodsLine;
            this.layoutControlItemGoodsLine.Location = new System.Drawing.Point(0, 116);
            this.layoutControlItemGoodsLine.Name = "layoutControlItemGoodsLine";
            this.layoutControlItemGoodsLine.OptionsTableLayoutItem.ColumnSpan = 3;
            this.layoutControlItemGoodsLine.OptionsTableLayoutItem.RowIndex = 4;
            this.layoutControlItemGoodsLine.Size = new System.Drawing.Size(516, 29);
            this.layoutControlItemGoodsLine.Text = "Ngành hàng";
            this.layoutControlItemGoodsLine.TextSize = new System.Drawing.Size(111, 13);
            // 
            // layoutControlItemGoodsGroup
            // 
            this.layoutControlItemGoodsGroup.Control = this.comboBoxEditGoodsGroup;
            this.layoutControlItemGoodsGroup.Location = new System.Drawing.Point(0, 145);
            this.layoutControlItemGoodsGroup.Name = "layoutControlItemGoodsGroup";
            this.layoutControlItemGoodsGroup.OptionsTableLayoutItem.ColumnSpan = 3;
            this.layoutControlItemGoodsGroup.OptionsTableLayoutItem.RowIndex = 5;
            this.layoutControlItemGoodsGroup.Size = new System.Drawing.Size(516, 30);
            this.layoutControlItemGoodsGroup.Text = "Nhóm hàng";
            this.layoutControlItemGoodsGroup.TextSize = new System.Drawing.Size(111, 13);
            // 
            // layoutControlItemGoodsCategory
            // 
            this.layoutControlItemGoodsCategory.Control = this.textEditGoodsCategory;
            this.layoutControlItemGoodsCategory.Location = new System.Drawing.Point(545, 0);
            this.layoutControlItemGoodsCategory.Name = "layoutControlItemGoodsCategory";
            this.layoutControlItemGoodsCategory.OptionsTableLayoutItem.ColumnIndex = 4;
            this.layoutControlItemGoodsCategory.OptionsTableLayoutItem.ColumnSpan = 2;
            this.layoutControlItemGoodsCategory.Size = new System.Drawing.Size(574, 29);
            this.layoutControlItemGoodsCategory.Text = "Loại hàng";
            this.layoutControlItemGoodsCategory.TextSize = new System.Drawing.Size(111, 13);
            // 
            // layoutControlItemDescription
            // 
            this.layoutControlItemDescription.Control = this.textEditDescription;
            this.layoutControlItemDescription.Location = new System.Drawing.Point(545, 29);
            this.layoutControlItemDescription.Name = "layoutControlItemDescription";
            this.layoutControlItemDescription.OptionsTableLayoutItem.ColumnIndex = 4;
            this.layoutControlItemDescription.OptionsTableLayoutItem.ColumnSpan = 3;
            this.layoutControlItemDescription.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItemDescription.Size = new System.Drawing.Size(861, 29);
            this.layoutControlItemDescription.Text = "Mô tả";
            this.layoutControlItemDescription.TextSize = new System.Drawing.Size(111, 13);
            // 
            // layoutControlItemInternalGoodsID
            // 
            this.layoutControlItemInternalGoodsID.Control = this.textEditInternalGoodsID;
            this.layoutControlItemInternalGoodsID.Location = new System.Drawing.Point(545, 58);
            this.layoutControlItemInternalGoodsID.Name = "layoutControlItemInternalGoodsID";
            this.layoutControlItemInternalGoodsID.OptionsTableLayoutItem.ColumnIndex = 4;
            this.layoutControlItemInternalGoodsID.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlItemInternalGoodsID.Size = new System.Drawing.Size(287, 29);
            this.layoutControlItemInternalGoodsID.Text = "Mã nội bộ";
            this.layoutControlItemInternalGoodsID.TextSize = new System.Drawing.Size(111, 13);
            // 
            // layoutControlItemGeneralGoodsID
            // 
            this.layoutControlItemGeneralGoodsID.Control = this.textEditGeneralGoodsID;
            this.layoutControlItemGeneralGoodsID.Location = new System.Drawing.Point(545, 87);
            this.layoutControlItemGeneralGoodsID.Name = "layoutControlItemGeneralGoodsID";
            this.layoutControlItemGeneralGoodsID.OptionsTableLayoutItem.ColumnIndex = 4;
            this.layoutControlItemGeneralGoodsID.OptionsTableLayoutItem.RowIndex = 3;
            this.layoutControlItemGeneralGoodsID.Size = new System.Drawing.Size(287, 29);
            this.layoutControlItemGeneralGoodsID.Text = "Mã tổng hợp";
            this.layoutControlItemGeneralGoodsID.TextSize = new System.Drawing.Size(111, 13);
            // 
            // layoutControlItemModel
            // 
            this.layoutControlItemModel.Control = this.textEditModel;
            this.layoutControlItemModel.Location = new System.Drawing.Point(545, 116);
            this.layoutControlItemModel.Name = "layoutControlItemModel";
            this.layoutControlItemModel.OptionsTableLayoutItem.ColumnIndex = 4;
            this.layoutControlItemModel.OptionsTableLayoutItem.RowIndex = 4;
            this.layoutControlItemModel.Size = new System.Drawing.Size(287, 29);
            this.layoutControlItemModel.Text = "Kiểu mẫu";
            this.layoutControlItemModel.TextSize = new System.Drawing.Size(111, 13);
            // 
            // layoutControlItemSerialNumber
            // 
            this.layoutControlItemSerialNumber.Control = this.textEditSerialNumber;
            this.layoutControlItemSerialNumber.Location = new System.Drawing.Point(545, 145);
            this.layoutControlItemSerialNumber.Name = "layoutControlItemSerialNumber";
            this.layoutControlItemSerialNumber.OptionsTableLayoutItem.ColumnIndex = 4;
            this.layoutControlItemSerialNumber.OptionsTableLayoutItem.RowIndex = 5;
            this.layoutControlItemSerialNumber.Size = new System.Drawing.Size(287, 30);
            this.layoutControlItemSerialNumber.Text = "Số Serial";
            this.layoutControlItemSerialNumber.TextSize = new System.Drawing.Size(111, 13);
            // 
            // layoutControlItemPacking
            // 
            this.layoutControlItemPacking.Control = this.textEditPacking;
            this.layoutControlItemPacking.Location = new System.Drawing.Point(832, 58);
            this.layoutControlItemPacking.Name = "layoutControlItemPacking";
            this.layoutControlItemPacking.OptionsTableLayoutItem.ColumnIndex = 5;
            this.layoutControlItemPacking.OptionsTableLayoutItem.ColumnSpan = 2;
            this.layoutControlItemPacking.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlItemPacking.Size = new System.Drawing.Size(574, 29);
            this.layoutControlItemPacking.Text = "Quy cách đóng gói";
            this.layoutControlItemPacking.TextSize = new System.Drawing.Size(111, 13);
            // 
            // layoutControlItemProductionDate
            // 
            this.layoutControlItemProductionDate.Control = this.textEditProductionDate;
            this.layoutControlItemProductionDate.Location = new System.Drawing.Point(832, 87);
            this.layoutControlItemProductionDate.Name = "layoutControlItemProductionDate";
            this.layoutControlItemProductionDate.OptionsTableLayoutItem.ColumnIndex = 5;
            this.layoutControlItemProductionDate.OptionsTableLayoutItem.RowIndex = 3;
            this.layoutControlItemProductionDate.Size = new System.Drawing.Size(287, 29);
            this.layoutControlItemProductionDate.Text = "Ngày sản xuất";
            this.layoutControlItemProductionDate.TextSize = new System.Drawing.Size(111, 13);
            // 
            // layoutControlItemExpiryDate
            // 
            this.layoutControlItemExpiryDate.Control = this.textEditExpiryDate;
            this.layoutControlItemExpiryDate.Location = new System.Drawing.Point(832, 116);
            this.layoutControlItemExpiryDate.Name = "layoutControlItemExpiryDate";
            this.layoutControlItemExpiryDate.OptionsTableLayoutItem.ColumnIndex = 5;
            this.layoutControlItemExpiryDate.OptionsTableLayoutItem.RowIndex = 4;
            this.layoutControlItemExpiryDate.Size = new System.Drawing.Size(287, 29);
            this.layoutControlItemExpiryDate.Text = "Hạn sử dụng";
            this.layoutControlItemExpiryDate.TextSize = new System.Drawing.Size(111, 13);
            // 
            // layoutControlItemWarrantyEndDate
            // 
            this.layoutControlItemWarrantyEndDate.Control = this.textEditWarrantyEndDate;
            this.layoutControlItemWarrantyEndDate.Location = new System.Drawing.Point(832, 145);
            this.layoutControlItemWarrantyEndDate.Name = "layoutControlItemWarrantyEndDate";
            this.layoutControlItemWarrantyEndDate.OptionsTableLayoutItem.ColumnIndex = 5;
            this.layoutControlItemWarrantyEndDate.OptionsTableLayoutItem.RowIndex = 5;
            this.layoutControlItemWarrantyEndDate.Size = new System.Drawing.Size(287, 30);
            this.layoutControlItemWarrantyEndDate.Text = "Hạn bảo hành";
            this.layoutControlItemWarrantyEndDate.TextSize = new System.Drawing.Size(111, 13);
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.checkEditScanOption;
            this.layoutControlItem14.Location = new System.Drawing.Point(1119, 87);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.OptionsTableLayoutItem.ColumnIndex = 6;
            this.layoutControlItem14.OptionsTableLayoutItem.RowIndex = 3;
            this.layoutControlItem14.Size = new System.Drawing.Size(287, 29);
            this.layoutControlItem14.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem14.TextVisible = false;
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.checkEditPLUType;
            this.layoutControlItem15.Location = new System.Drawing.Point(1119, 116);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.OptionsTableLayoutItem.ColumnIndex = 6;
            this.layoutControlItem15.OptionsTableLayoutItem.RowIndex = 4;
            this.layoutControlItem15.Size = new System.Drawing.Size(287, 29);
            this.layoutControlItem15.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem15.TextVisible = false;
            // 
            // layoutControlItemGoodsStatus
            // 
            this.layoutControlItemGoodsStatus.Control = this.comboBoxEditGoodsStatus;
            this.layoutControlItemGoodsStatus.Location = new System.Drawing.Point(1119, 0);
            this.layoutControlItemGoodsStatus.Name = "layoutControlItemGoodsStatus";
            this.layoutControlItemGoodsStatus.OptionsTableLayoutItem.ColumnIndex = 6;
            this.layoutControlItemGoodsStatus.Size = new System.Drawing.Size(287, 29);
            this.layoutControlItemGoodsStatus.Text = "Trạng thái";
            this.layoutControlItemGoodsStatus.TextSize = new System.Drawing.Size(111, 13);
            // 
            // xtraTabPageGoodsOther
            // 
            this.xtraTabPageGoodsOther.Controls.Add(this.layoutControl3);
            this.xtraTabPageGoodsOther.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.xtraTabPageGoodsOther.Name = "xtraTabPageGoodsOther";
            this.xtraTabPageGoodsOther.Size = new System.Drawing.Size(1424, 191);
            this.xtraTabPageGoodsOther.Text = "Thông tin khác";
            // 
            // layoutControl3
            // 
            this.layoutControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl3.Location = new System.Drawing.Point(0, 0);
            this.layoutControl3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.layoutControl3.Name = "layoutControl3";
            this.layoutControl3.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(743, 221, 812, 500);
            this.layoutControl3.Root = this.layoutControlGroup2;
            this.layoutControl3.Size = new System.Drawing.Size(1424, 191);
            this.layoutControl3.TabIndex = 0;
            this.layoutControl3.Text = "layoutControl3";
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.LayoutMode = DevExpress.XtraLayout.Utils.LayoutMode.Table;
            this.layoutControlGroup2.Name = "Root";
            columnDefinition8.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition8.Width = 50D;
            columnDefinition9.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition9.Width = 50D;
            columnDefinition10.SizeType = System.Windows.Forms.SizeType.AutoSize;
            columnDefinition10.Width = 472D;
            this.layoutControlGroup2.OptionsTableLayoutGroup.ColumnDefinitions.AddRange(new DevExpress.XtraLayout.ColumnDefinition[] {
            columnDefinition8,
            columnDefinition9,
            columnDefinition10});
            rowDefinition7.Height = 16.666666666666668D;
            rowDefinition7.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition8.Height = 16.666666666666668D;
            rowDefinition8.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition9.Height = 16.666666666666668D;
            rowDefinition9.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition10.Height = 16.666666666666668D;
            rowDefinition10.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition11.Height = 16.666666666666668D;
            rowDefinition11.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition12.Height = 16.666666666666668D;
            rowDefinition12.SizeType = System.Windows.Forms.SizeType.Percent;
            this.layoutControlGroup2.OptionsTableLayoutGroup.RowDefinitions.AddRange(new DevExpress.XtraLayout.RowDefinition[] {
            rowDefinition7,
            rowDefinition8,
            rowDefinition9,
            rowDefinition10,
            rowDefinition11,
            rowDefinition12});
            this.layoutControlGroup2.Size = new System.Drawing.Size(1424, 191);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // xtraTabPagePriceTax
            // 
            this.xtraTabPagePriceTax.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.xtraTabPagePriceTax.Name = "xtraTabPagePriceTax";
            this.xtraTabPagePriceTax.Size = new System.Drawing.Size(1424, 191);
            this.xtraTabPagePriceTax.Text = "Thông tin giá mua, giá bán, thuế";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1451, 237);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.xtraTabControlGoods;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(89, 19);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(1433, 221);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // goodsBindingSource
            // 
            this.goodsBindingSource.DataMember = "Goods";
            this.goodsBindingSource.DataSource = this.wARHOUSE_HPDataSet;
            // 
            // wARHOUSE_HPDataSet
            // 
            this.wARHOUSE_HPDataSet.DataSetName = "WARHOUSE_HPDataSet";
            this.wARHOUSE_HPDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // goodsTableAdapter
            // 
            this.goodsTableAdapter.ClearBeforeFill = true;
            // 
            // gridView1
            // 
            this.gridView1.Name = "gridView1";
            // 
            // layoutControl5
            // 
            this.layoutControl5.Controls.Add(this.gridControlGoods);
            this.layoutControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl5.Location = new System.Drawing.Point(0, 292);
            this.layoutControl5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.layoutControl5.Name = "layoutControl5";
            this.layoutControl5.Root = this.layoutControlGroup4;
            this.layoutControl5.Size = new System.Drawing.Size(1451, 350);
            this.layoutControl5.TabIndex = 2;
            this.layoutControl5.Text = "layoutControl5";
            // 
            // gridControlGoods
            // 
            this.gridControlGoods.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gridControlGoods.Location = new System.Drawing.Point(11, 10);
            this.gridControlGoods.MainView = this.gridViewGoods;
            this.gridControlGoods.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gridControlGoods.Name = "gridControlGoods";
            this.gridControlGoods.Size = new System.Drawing.Size(1429, 330);
            this.gridControlGoods.TabIndex = 6;
            this.gridControlGoods.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewGoods});
            // 
            // gridViewGoods
            // 
            this.gridViewGoods.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colNo,
            this.colGoodsID,
            this.colGoodsName,
            this.colOtherGoodsName,
            this.colStockUnitID,
            this.colGoodsTypeID,
            this.colGoodsLineID,
            this.colGoodsGroupID,
            this.colGoodsCategoryID,
            this.colInternalGoodsID,
            this.colGeneralGoodsID,
            this.colModel,
            this.colSerialNumber,
            this.colProductionDate,
            this.colExpiryDate,
            this.colWarrantyEndDate,
            this.colScanOption,
            this.colPLUType,
            this.colPacking,
            this.colDescription,
            this.colGoodsStatus,
            this.colSupplierID,
            this.colManufacturerID,
            this.colPurchaseUnitID,
            this.colPurchaseUnitRate,
            this.colRetailUnitID,
            this.colRetailUnitRate,
            this.colWholesaleUnitID,
            this.colWholesaleUnitRate,
            this.colStyleID,
            this.colSizeID,
            this.colColorID,
            this.colSeasonID,
            this.colMaterialID,
            this.colLength,
            this.colLengthUnit,
            this.colWidth,
            this.colWidthUnit,
            this.colHeight,
            this.colHeightUnit,
            this.colWeight,
            this.colWeightUnit,
            this.colDiameter,
            this.colDiameterUnit,
            this.colGauge,
            this.colGaugeUnit,
            this.colVolume,
            this.colVolumeUnit,
            this.colDensity,
            this.colVATInput,
            this.colVATOutput,
            this.colImportTax,
            this.colSpecialTax,
            this.colTransportRate,
            this.colOtherRate,
            this.colDiscountInput,
            this.colDiscountOutput,
            this.colCostPrice,
            this.colRetailSalePrice,
            this.colWholesalePrice,
            this.colInternalSalePrice,
            this.colMargin,
            this.colDiscountInternal,
            this.colMinQuantity,
            this.colMaxQuantity,
            this.colStatus,
            this.colCreatedUserID,
            this.colCreatedDate,
            this.colUpdatedUserID,
            this.colUpdatedDate});
            this.gridViewGoods.DetailHeight = 284;
            this.gridViewGoods.GridControl = this.gridControlGoods;
            this.gridViewGoods.Name = "gridViewGoods";
            // 
            // colNo
            // 
            this.colNo.Caption = "No";
            this.colNo.FieldName = "No";
            this.colNo.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colNo.MinWidth = 34;
            this.colNo.Name = "colNo";
            this.colNo.Visible = true;
            this.colNo.VisibleIndex = 0;
            this.colNo.Width = 34;
            // 
            // colGoodsID
            // 
            this.colGoodsID.Caption = "Mã hàng hóa";
            this.colGoodsID.FieldName = "GoodsID";
            this.colGoodsID.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colGoodsID.MinWidth = 21;
            this.colGoodsID.Name = "colGoodsID";
            this.colGoodsID.Visible = true;
            this.colGoodsID.VisibleIndex = 1;
            this.colGoodsID.Width = 126;
            // 
            // colGoodsName
            // 
            this.colGoodsName.Caption = "Tên hàng hóa";
            this.colGoodsName.FieldName = "GoodsName";
            this.colGoodsName.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colGoodsName.MinWidth = 21;
            this.colGoodsName.Name = "colGoodsName";
            this.colGoodsName.Visible = true;
            this.colGoodsName.VisibleIndex = 2;
            this.colGoodsName.Width = 140;
            // 
            // colOtherGoodsName
            // 
            this.colOtherGoodsName.Caption = "Tên khác của hàng hóa";
            this.colOtherGoodsName.FieldName = "OtherGoodsName";
            this.colOtherGoodsName.MinWidth = 129;
            this.colOtherGoodsName.Name = "colOtherGoodsName";
            this.colOtherGoodsName.Visible = true;
            this.colOtherGoodsName.VisibleIndex = 3;
            this.colOtherGoodsName.Width = 131;
            // 
            // colStockUnitID
            // 
            this.colStockUnitID.Caption = "Đơn vị kho";
            this.colStockUnitID.FieldName = "StockUnitID";
            this.colStockUnitID.MinWidth = 86;
            this.colStockUnitID.Name = "colStockUnitID";
            this.colStockUnitID.Visible = true;
            this.colStockUnitID.VisibleIndex = 4;
            this.colStockUnitID.Width = 86;
            // 
            // colGoodsTypeID
            // 
            this.colGoodsTypeID.Caption = "Loại";
            this.colGoodsTypeID.FieldName = "GoodsTypeID";
            this.colGoodsTypeID.MinWidth = 86;
            this.colGoodsTypeID.Name = "colGoodsTypeID";
            this.colGoodsTypeID.Visible = true;
            this.colGoodsTypeID.VisibleIndex = 5;
            this.colGoodsTypeID.Width = 86;
            // 
            // colGoodsLineID
            // 
            this.colGoodsLineID.Caption = "Mã ngành hàng";
            this.colGoodsLineID.FieldName = "GoodsLineID";
            this.colGoodsLineID.MinWidth = 86;
            this.colGoodsLineID.Name = "colGoodsLineID";
            this.colGoodsLineID.Visible = true;
            this.colGoodsLineID.VisibleIndex = 6;
            this.colGoodsLineID.Width = 86;
            // 
            // colGoodsGroupID
            // 
            this.colGoodsGroupID.FieldName = "GoodsGroupID";
            this.colGoodsGroupID.MinWidth = 86;
            this.colGoodsGroupID.Name = "colGoodsGroupID";
            this.colGoodsGroupID.Visible = true;
            this.colGoodsGroupID.VisibleIndex = 7;
            this.colGoodsGroupID.Width = 86;
            // 
            // colGoodsCategoryID
            // 
            this.colGoodsCategoryID.FieldName = "GoodsCategoryID";
            this.colGoodsCategoryID.MinWidth = 86;
            this.colGoodsCategoryID.Name = "colGoodsCategoryID";
            this.colGoodsCategoryID.Visible = true;
            this.colGoodsCategoryID.VisibleIndex = 8;
            this.colGoodsCategoryID.Width = 86;
            // 
            // colInternalGoodsID
            // 
            this.colInternalGoodsID.FieldName = "InternalGoodsID";
            this.colInternalGoodsID.MinWidth = 86;
            this.colInternalGoodsID.Name = "colInternalGoodsID";
            this.colInternalGoodsID.Visible = true;
            this.colInternalGoodsID.VisibleIndex = 9;
            this.colInternalGoodsID.Width = 86;
            // 
            // colGeneralGoodsID
            // 
            this.colGeneralGoodsID.FieldName = "GeneralGoodsID";
            this.colGeneralGoodsID.MinWidth = 86;
            this.colGeneralGoodsID.Name = "colGeneralGoodsID";
            this.colGeneralGoodsID.Visible = true;
            this.colGeneralGoodsID.VisibleIndex = 10;
            this.colGeneralGoodsID.Width = 86;
            // 
            // colModel
            // 
            this.colModel.FieldName = "Model";
            this.colModel.MinWidth = 86;
            this.colModel.Name = "colModel";
            this.colModel.Visible = true;
            this.colModel.VisibleIndex = 11;
            this.colModel.Width = 86;
            // 
            // colSerialNumber
            // 
            this.colSerialNumber.FieldName = "SerialNumber";
            this.colSerialNumber.MinWidth = 86;
            this.colSerialNumber.Name = "colSerialNumber";
            this.colSerialNumber.Visible = true;
            this.colSerialNumber.VisibleIndex = 12;
            this.colSerialNumber.Width = 86;
            // 
            // colProductionDate
            // 
            this.colProductionDate.FieldName = "ProductionDate";
            this.colProductionDate.MinWidth = 86;
            this.colProductionDate.Name = "colProductionDate";
            this.colProductionDate.Visible = true;
            this.colProductionDate.VisibleIndex = 13;
            this.colProductionDate.Width = 86;
            // 
            // colExpiryDate
            // 
            this.colExpiryDate.FieldName = "ExpiryDate";
            this.colExpiryDate.MinWidth = 86;
            this.colExpiryDate.Name = "colExpiryDate";
            this.colExpiryDate.Visible = true;
            this.colExpiryDate.VisibleIndex = 14;
            this.colExpiryDate.Width = 86;
            // 
            // colWarrantyEndDate
            // 
            this.colWarrantyEndDate.FieldName = "WarrantyEndDate";
            this.colWarrantyEndDate.MinWidth = 86;
            this.colWarrantyEndDate.Name = "colWarrantyEndDate";
            this.colWarrantyEndDate.Visible = true;
            this.colWarrantyEndDate.VisibleIndex = 15;
            this.colWarrantyEndDate.Width = 86;
            // 
            // colScanOption
            // 
            this.colScanOption.FieldName = "ScanOption";
            this.colScanOption.MinWidth = 86;
            this.colScanOption.Name = "colScanOption";
            this.colScanOption.Visible = true;
            this.colScanOption.VisibleIndex = 16;
            this.colScanOption.Width = 86;
            // 
            // colPLUType
            // 
            this.colPLUType.FieldName = "PLUType";
            this.colPLUType.MinWidth = 86;
            this.colPLUType.Name = "colPLUType";
            this.colPLUType.Visible = true;
            this.colPLUType.VisibleIndex = 17;
            this.colPLUType.Width = 86;
            // 
            // colPacking
            // 
            this.colPacking.FieldName = "Packing";
            this.colPacking.MinWidth = 86;
            this.colPacking.Name = "colPacking";
            this.colPacking.Visible = true;
            this.colPacking.VisibleIndex = 18;
            this.colPacking.Width = 86;
            // 
            // colDescription
            // 
            this.colDescription.FieldName = "Description";
            this.colDescription.MinWidth = 86;
            this.colDescription.Name = "colDescription";
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 19;
            this.colDescription.Width = 86;
            // 
            // colGoodsStatus
            // 
            this.colGoodsStatus.FieldName = "GoodsStatus";
            this.colGoodsStatus.MinWidth = 86;
            this.colGoodsStatus.Name = "colGoodsStatus";
            this.colGoodsStatus.Visible = true;
            this.colGoodsStatus.VisibleIndex = 20;
            this.colGoodsStatus.Width = 86;
            // 
            // colSupplierID
            // 
            this.colSupplierID.FieldName = "SupplierID";
            this.colSupplierID.MinWidth = 86;
            this.colSupplierID.Name = "colSupplierID";
            this.colSupplierID.Visible = true;
            this.colSupplierID.VisibleIndex = 21;
            this.colSupplierID.Width = 86;
            // 
            // colManufacturerID
            // 
            this.colManufacturerID.FieldName = "ManufacturerID";
            this.colManufacturerID.MinWidth = 86;
            this.colManufacturerID.Name = "colManufacturerID";
            this.colManufacturerID.Visible = true;
            this.colManufacturerID.VisibleIndex = 22;
            this.colManufacturerID.Width = 86;
            // 
            // colPurchaseUnitID
            // 
            this.colPurchaseUnitID.FieldName = "PurchaseUnitID";
            this.colPurchaseUnitID.MinWidth = 86;
            this.colPurchaseUnitID.Name = "colPurchaseUnitID";
            this.colPurchaseUnitID.Visible = true;
            this.colPurchaseUnitID.VisibleIndex = 23;
            this.colPurchaseUnitID.Width = 86;
            // 
            // colPurchaseUnitRate
            // 
            this.colPurchaseUnitRate.FieldName = "PurchaseUnitRate";
            this.colPurchaseUnitRate.MinWidth = 86;
            this.colPurchaseUnitRate.Name = "colPurchaseUnitRate";
            this.colPurchaseUnitRate.Visible = true;
            this.colPurchaseUnitRate.VisibleIndex = 24;
            this.colPurchaseUnitRate.Width = 86;
            // 
            // colRetailUnitID
            // 
            this.colRetailUnitID.FieldName = "RetailUnitID";
            this.colRetailUnitID.MinWidth = 86;
            this.colRetailUnitID.Name = "colRetailUnitID";
            this.colRetailUnitID.Visible = true;
            this.colRetailUnitID.VisibleIndex = 25;
            this.colRetailUnitID.Width = 86;
            // 
            // colRetailUnitRate
            // 
            this.colRetailUnitRate.FieldName = "RetailUnitRate";
            this.colRetailUnitRate.MinWidth = 86;
            this.colRetailUnitRate.Name = "colRetailUnitRate";
            this.colRetailUnitRate.Visible = true;
            this.colRetailUnitRate.VisibleIndex = 26;
            this.colRetailUnitRate.Width = 86;
            // 
            // colWholesaleUnitID
            // 
            this.colWholesaleUnitID.FieldName = "WholesaleUnitID";
            this.colWholesaleUnitID.MinWidth = 86;
            this.colWholesaleUnitID.Name = "colWholesaleUnitID";
            this.colWholesaleUnitID.Visible = true;
            this.colWholesaleUnitID.VisibleIndex = 27;
            this.colWholesaleUnitID.Width = 86;
            // 
            // colWholesaleUnitRate
            // 
            this.colWholesaleUnitRate.FieldName = "WholesaleUnitRate";
            this.colWholesaleUnitRate.MinWidth = 86;
            this.colWholesaleUnitRate.Name = "colWholesaleUnitRate";
            this.colWholesaleUnitRate.Visible = true;
            this.colWholesaleUnitRate.VisibleIndex = 28;
            this.colWholesaleUnitRate.Width = 86;
            // 
            // colStyleID
            // 
            this.colStyleID.FieldName = "StyleID";
            this.colStyleID.MinWidth = 86;
            this.colStyleID.Name = "colStyleID";
            this.colStyleID.Visible = true;
            this.colStyleID.VisibleIndex = 29;
            this.colStyleID.Width = 86;
            // 
            // colSizeID
            // 
            this.colSizeID.FieldName = "SizeID";
            this.colSizeID.MinWidth = 86;
            this.colSizeID.Name = "colSizeID";
            this.colSizeID.Visible = true;
            this.colSizeID.VisibleIndex = 30;
            this.colSizeID.Width = 86;
            // 
            // colColorID
            // 
            this.colColorID.FieldName = "ColorID";
            this.colColorID.MinWidth = 86;
            this.colColorID.Name = "colColorID";
            this.colColorID.Visible = true;
            this.colColorID.VisibleIndex = 31;
            this.colColorID.Width = 86;
            // 
            // colSeasonID
            // 
            this.colSeasonID.FieldName = "SeasonID";
            this.colSeasonID.MinWidth = 86;
            this.colSeasonID.Name = "colSeasonID";
            this.colSeasonID.Visible = true;
            this.colSeasonID.VisibleIndex = 32;
            this.colSeasonID.Width = 86;
            // 
            // colMaterialID
            // 
            this.colMaterialID.FieldName = "MaterialID";
            this.colMaterialID.MinWidth = 86;
            this.colMaterialID.Name = "colMaterialID";
            this.colMaterialID.Visible = true;
            this.colMaterialID.VisibleIndex = 33;
            this.colMaterialID.Width = 86;
            // 
            // colLength
            // 
            this.colLength.FieldName = "Length";
            this.colLength.MinWidth = 86;
            this.colLength.Name = "colLength";
            this.colLength.Visible = true;
            this.colLength.VisibleIndex = 34;
            this.colLength.Width = 86;
            // 
            // colLengthUnit
            // 
            this.colLengthUnit.FieldName = "LengthUnit";
            this.colLengthUnit.MinWidth = 86;
            this.colLengthUnit.Name = "colLengthUnit";
            this.colLengthUnit.Visible = true;
            this.colLengthUnit.VisibleIndex = 35;
            this.colLengthUnit.Width = 86;
            // 
            // colWidth
            // 
            this.colWidth.FieldName = "Width";
            this.colWidth.MinWidth = 86;
            this.colWidth.Name = "colWidth";
            this.colWidth.Visible = true;
            this.colWidth.VisibleIndex = 36;
            this.colWidth.Width = 86;
            // 
            // colWidthUnit
            // 
            this.colWidthUnit.FieldName = "WidthUnit";
            this.colWidthUnit.MinWidth = 86;
            this.colWidthUnit.Name = "colWidthUnit";
            this.colWidthUnit.Visible = true;
            this.colWidthUnit.VisibleIndex = 37;
            this.colWidthUnit.Width = 86;
            // 
            // colHeight
            // 
            this.colHeight.FieldName = "Height";
            this.colHeight.MinWidth = 86;
            this.colHeight.Name = "colHeight";
            this.colHeight.Visible = true;
            this.colHeight.VisibleIndex = 38;
            this.colHeight.Width = 86;
            // 
            // colHeightUnit
            // 
            this.colHeightUnit.FieldName = "HeightUnit";
            this.colHeightUnit.MinWidth = 86;
            this.colHeightUnit.Name = "colHeightUnit";
            this.colHeightUnit.Visible = true;
            this.colHeightUnit.VisibleIndex = 39;
            this.colHeightUnit.Width = 86;
            // 
            // colWeight
            // 
            this.colWeight.FieldName = "Weight";
            this.colWeight.MinWidth = 86;
            this.colWeight.Name = "colWeight";
            this.colWeight.Visible = true;
            this.colWeight.VisibleIndex = 40;
            this.colWeight.Width = 86;
            // 
            // colWeightUnit
            // 
            this.colWeightUnit.FieldName = "WeightUnit";
            this.colWeightUnit.MinWidth = 86;
            this.colWeightUnit.Name = "colWeightUnit";
            this.colWeightUnit.Visible = true;
            this.colWeightUnit.VisibleIndex = 41;
            this.colWeightUnit.Width = 86;
            // 
            // colDiameter
            // 
            this.colDiameter.FieldName = "Diameter";
            this.colDiameter.MinWidth = 86;
            this.colDiameter.Name = "colDiameter";
            this.colDiameter.Visible = true;
            this.colDiameter.VisibleIndex = 42;
            this.colDiameter.Width = 86;
            // 
            // colDiameterUnit
            // 
            this.colDiameterUnit.FieldName = "DiameterUnit";
            this.colDiameterUnit.MinWidth = 86;
            this.colDiameterUnit.Name = "colDiameterUnit";
            this.colDiameterUnit.Visible = true;
            this.colDiameterUnit.VisibleIndex = 43;
            this.colDiameterUnit.Width = 86;
            // 
            // colGauge
            // 
            this.colGauge.FieldName = "Gauge";
            this.colGauge.MinWidth = 86;
            this.colGauge.Name = "colGauge";
            this.colGauge.Visible = true;
            this.colGauge.VisibleIndex = 44;
            this.colGauge.Width = 86;
            // 
            // colGaugeUnit
            // 
            this.colGaugeUnit.FieldName = "GaugeUnit";
            this.colGaugeUnit.MinWidth = 86;
            this.colGaugeUnit.Name = "colGaugeUnit";
            this.colGaugeUnit.Visible = true;
            this.colGaugeUnit.VisibleIndex = 45;
            this.colGaugeUnit.Width = 86;
            // 
            // colVolume
            // 
            this.colVolume.FieldName = "Volume";
            this.colVolume.MinWidth = 86;
            this.colVolume.Name = "colVolume";
            this.colVolume.Visible = true;
            this.colVolume.VisibleIndex = 46;
            this.colVolume.Width = 86;
            // 
            // colVolumeUnit
            // 
            this.colVolumeUnit.FieldName = "VolumeUnit";
            this.colVolumeUnit.MinWidth = 86;
            this.colVolumeUnit.Name = "colVolumeUnit";
            this.colVolumeUnit.Visible = true;
            this.colVolumeUnit.VisibleIndex = 47;
            this.colVolumeUnit.Width = 86;
            // 
            // colDensity
            // 
            this.colDensity.FieldName = "Density";
            this.colDensity.MinWidth = 86;
            this.colDensity.Name = "colDensity";
            this.colDensity.Visible = true;
            this.colDensity.VisibleIndex = 48;
            this.colDensity.Width = 86;
            // 
            // colVATInput
            // 
            this.colVATInput.FieldName = "VATInput";
            this.colVATInput.MinWidth = 86;
            this.colVATInput.Name = "colVATInput";
            this.colVATInput.Visible = true;
            this.colVATInput.VisibleIndex = 49;
            this.colVATInput.Width = 86;
            // 
            // colVATOutput
            // 
            this.colVATOutput.FieldName = "VATOutput";
            this.colVATOutput.MinWidth = 86;
            this.colVATOutput.Name = "colVATOutput";
            this.colVATOutput.Visible = true;
            this.colVATOutput.VisibleIndex = 50;
            this.colVATOutput.Width = 86;
            // 
            // colImportTax
            // 
            this.colImportTax.FieldName = "ImportTax";
            this.colImportTax.MinWidth = 86;
            this.colImportTax.Name = "colImportTax";
            this.colImportTax.Visible = true;
            this.colImportTax.VisibleIndex = 51;
            this.colImportTax.Width = 86;
            // 
            // colSpecialTax
            // 
            this.colSpecialTax.FieldName = "SpecialTax";
            this.colSpecialTax.MinWidth = 86;
            this.colSpecialTax.Name = "colSpecialTax";
            this.colSpecialTax.Visible = true;
            this.colSpecialTax.VisibleIndex = 52;
            this.colSpecialTax.Width = 86;
            // 
            // colTransportRate
            // 
            this.colTransportRate.FieldName = "TransportRate";
            this.colTransportRate.MinWidth = 86;
            this.colTransportRate.Name = "colTransportRate";
            this.colTransportRate.Visible = true;
            this.colTransportRate.VisibleIndex = 53;
            this.colTransportRate.Width = 86;
            // 
            // colOtherRate
            // 
            this.colOtherRate.FieldName = "OtherRate";
            this.colOtherRate.MinWidth = 86;
            this.colOtherRate.Name = "colOtherRate";
            this.colOtherRate.Visible = true;
            this.colOtherRate.VisibleIndex = 54;
            this.colOtherRate.Width = 86;
            // 
            // colDiscountInput
            // 
            this.colDiscountInput.FieldName = "DiscountInput";
            this.colDiscountInput.MinWidth = 86;
            this.colDiscountInput.Name = "colDiscountInput";
            this.colDiscountInput.Visible = true;
            this.colDiscountInput.VisibleIndex = 55;
            this.colDiscountInput.Width = 86;
            // 
            // colDiscountOutput
            // 
            this.colDiscountOutput.FieldName = "DiscountOutput";
            this.colDiscountOutput.MinWidth = 86;
            this.colDiscountOutput.Name = "colDiscountOutput";
            this.colDiscountOutput.Visible = true;
            this.colDiscountOutput.VisibleIndex = 56;
            this.colDiscountOutput.Width = 86;
            // 
            // colCostPrice
            // 
            this.colCostPrice.FieldName = "CostPrice";
            this.colCostPrice.MinWidth = 86;
            this.colCostPrice.Name = "colCostPrice";
            this.colCostPrice.Visible = true;
            this.colCostPrice.VisibleIndex = 57;
            this.colCostPrice.Width = 86;
            // 
            // colRetailSalePrice
            // 
            this.colRetailSalePrice.FieldName = "RetailSalePrice";
            this.colRetailSalePrice.MinWidth = 86;
            this.colRetailSalePrice.Name = "colRetailSalePrice";
            this.colRetailSalePrice.Visible = true;
            this.colRetailSalePrice.VisibleIndex = 58;
            this.colRetailSalePrice.Width = 86;
            // 
            // colWholesalePrice
            // 
            this.colWholesalePrice.FieldName = "WholesalePrice";
            this.colWholesalePrice.MinWidth = 86;
            this.colWholesalePrice.Name = "colWholesalePrice";
            this.colWholesalePrice.Visible = true;
            this.colWholesalePrice.VisibleIndex = 59;
            this.colWholesalePrice.Width = 86;
            // 
            // colInternalSalePrice
            // 
            this.colInternalSalePrice.FieldName = "InternalSalePrice";
            this.colInternalSalePrice.MinWidth = 86;
            this.colInternalSalePrice.Name = "colInternalSalePrice";
            this.colInternalSalePrice.Visible = true;
            this.colInternalSalePrice.VisibleIndex = 60;
            this.colInternalSalePrice.Width = 86;
            // 
            // colMargin
            // 
            this.colMargin.FieldName = "Margin";
            this.colMargin.MinWidth = 86;
            this.colMargin.Name = "colMargin";
            this.colMargin.Visible = true;
            this.colMargin.VisibleIndex = 61;
            this.colMargin.Width = 86;
            // 
            // colDiscountInternal
            // 
            this.colDiscountInternal.FieldName = "DiscountInternal";
            this.colDiscountInternal.MinWidth = 86;
            this.colDiscountInternal.Name = "colDiscountInternal";
            this.colDiscountInternal.Visible = true;
            this.colDiscountInternal.VisibleIndex = 62;
            this.colDiscountInternal.Width = 86;
            // 
            // colMinQuantity
            // 
            this.colMinQuantity.FieldName = "MinQuantity";
            this.colMinQuantity.MinWidth = 86;
            this.colMinQuantity.Name = "colMinQuantity";
            this.colMinQuantity.Visible = true;
            this.colMinQuantity.VisibleIndex = 63;
            this.colMinQuantity.Width = 86;
            // 
            // colMaxQuantity
            // 
            this.colMaxQuantity.FieldName = "MaxQuantity";
            this.colMaxQuantity.MinWidth = 86;
            this.colMaxQuantity.Name = "colMaxQuantity";
            this.colMaxQuantity.Visible = true;
            this.colMaxQuantity.VisibleIndex = 64;
            this.colMaxQuantity.Width = 86;
            // 
            // colStatus
            // 
            this.colStatus.FieldName = "Status";
            this.colStatus.MinWidth = 86;
            this.colStatus.Name = "colStatus";
            this.colStatus.Visible = true;
            this.colStatus.VisibleIndex = 65;
            this.colStatus.Width = 86;
            // 
            // colCreatedUserID
            // 
            this.colCreatedUserID.FieldName = "CreatedUserID";
            this.colCreatedUserID.MinWidth = 86;
            this.colCreatedUserID.Name = "colCreatedUserID";
            this.colCreatedUserID.Visible = true;
            this.colCreatedUserID.VisibleIndex = 66;
            this.colCreatedUserID.Width = 86;
            // 
            // colCreatedDate
            // 
            this.colCreatedDate.FieldName = "CreatedDate";
            this.colCreatedDate.MinWidth = 86;
            this.colCreatedDate.Name = "colCreatedDate";
            this.colCreatedDate.Visible = true;
            this.colCreatedDate.VisibleIndex = 67;
            this.colCreatedDate.Width = 86;
            // 
            // colUpdatedUserID
            // 
            this.colUpdatedUserID.FieldName = "UpdatedUserID";
            this.colUpdatedUserID.MinWidth = 86;
            this.colUpdatedUserID.Name = "colUpdatedUserID";
            this.colUpdatedUserID.Visible = true;
            this.colUpdatedUserID.VisibleIndex = 68;
            this.colUpdatedUserID.Width = 86;
            // 
            // colUpdatedDate
            // 
            this.colUpdatedDate.FieldName = "UpdatedDate";
            this.colUpdatedDate.MinWidth = 86;
            this.colUpdatedDate.Name = "colUpdatedDate";
            this.colUpdatedDate.Visible = true;
            this.colUpdatedDate.VisibleIndex = 69;
            this.colUpdatedDate.Width = 86;
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup4.GroupBordersVisible = false;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem5});
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(1451, 350);
            this.layoutControlGroup4.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.gridControlGoods;
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(1433, 334);
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextVisible = false;
            // 
            // GoodManagerControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl5);
            this.Controls.Add(this.layoutControl2);
            this.Controls.Add(this.layoutControl1);
            this.Name = "GoodManagerControl";
            this.Size = new System.Drawing.Size(1451, 642);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControlGoods)).EndInit();
            this.xtraTabControlGoods.ResumeLayout(false);
            this.xtraTabPageGoodsGeneral.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl4)).EndInit();
            this.layoutControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEditGoodsID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditGoodsName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditOtherGoodsName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditStockUnitID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditGoodsType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditGoodsLine.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditGoodsGroup.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditGoodsCategory.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditDescription.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditInternalGoodsID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditGeneralGoodsID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditModel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditSerialNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPacking.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditProductionDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditExpiryDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditWarrantyEndDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditScanOption.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditPLUType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditGoodsStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemGoodsID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemGoodsName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemOtherGoodsName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemStockUnitID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemGoodsType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemGoodsLine)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemGoodsGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemGoodsCategory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemInternalGoodsID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemGeneralGoodsID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemModel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemSerialNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPacking)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemProductionDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemExpiryDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWarrantyEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemGoodsStatus)).EndInit();
            this.xtraTabPageGoodsOther.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.goodsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wARHOUSE_HPDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl5)).EndInit();
            this.layoutControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlGoods)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewGoods)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel windowsUIButtonPanel1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraTab.XtraTabControl xtraTabControlGoods;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageGoodsGeneral;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageGoodsOther;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraTab.XtraTabPage xtraTabPagePriceTax;
        private System.Windows.Forms.BindingSource goodsBindingSource;
        private WARHOUSE_HPDataSet wARHOUSE_HPDataSet;
        private WARHOUSE_HPDataSetTableAdapters.GoodsTableAdapter goodsTableAdapter;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraLayout.LayoutControl layoutControl3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControl layoutControl4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.TextEdit textEditGoodsID;
        private DevExpress.XtraEditors.TextEdit textEditGoodsName;
        private DevExpress.XtraEditors.TextEdit textEditOtherGoodsName;
        private DevExpress.XtraEditors.TextEdit textEditStockUnitID;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditGoodsType;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditGoodsLine;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditGoodsGroup;
        private DevExpress.XtraEditors.TextEdit textEditGoodsCategory;
        private DevExpress.XtraEditors.TextEdit textEditDescription;
        private DevExpress.XtraEditors.TextEdit textEditInternalGoodsID;
        private DevExpress.XtraEditors.TextEdit textEditGeneralGoodsID;
        private DevExpress.XtraEditors.TextEdit textEditModel;
        private DevExpress.XtraEditors.TextEdit textEditSerialNumber;
        private DevExpress.XtraEditors.TextEdit textEditPacking;
        private DevExpress.XtraEditors.TextEdit textEditProductionDate;
        private DevExpress.XtraEditors.TextEdit textEditExpiryDate;
        private DevExpress.XtraEditors.TextEdit textEditWarrantyEndDate;
        private DevExpress.XtraEditors.CheckEdit checkEditScanOption;
        private DevExpress.XtraEditors.CheckEdit checkEditPLUType;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditGoodsStatus;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemGoodsID;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemGoodsName;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemOtherGoodsName;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemStockUnitID;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemGoodsType;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemGoodsLine;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemGoodsGroup;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemGoodsCategory;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemDescription;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemInternalGoodsID;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemGeneralGoodsID;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemModel;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemSerialNumber;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemPacking;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemProductionDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemExpiryDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemWarrantyEndDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemGoodsStatus;
        private DevExpress.XtraLayout.LayoutControl layoutControl5;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraGrid.GridControl gridControlGoods;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewGoods;
        private DevExpress.XtraGrid.Columns.GridColumn colNo;
        private DevExpress.XtraGrid.Columns.GridColumn colGoodsID;
        private DevExpress.XtraGrid.Columns.GridColumn colGoodsName;
        private DevExpress.XtraGrid.Columns.GridColumn colOtherGoodsName;
        private DevExpress.XtraGrid.Columns.GridColumn colStockUnitID;
        private DevExpress.XtraGrid.Columns.GridColumn colGoodsTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colGoodsLineID;
        private DevExpress.XtraGrid.Columns.GridColumn colGoodsGroupID;
        private DevExpress.XtraGrid.Columns.GridColumn colGoodsCategoryID;
        private DevExpress.XtraGrid.Columns.GridColumn colInternalGoodsID;
        private DevExpress.XtraGrid.Columns.GridColumn colGeneralGoodsID;
        private DevExpress.XtraGrid.Columns.GridColumn colModel;
        private DevExpress.XtraGrid.Columns.GridColumn colSerialNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colProductionDate;
        private DevExpress.XtraGrid.Columns.GridColumn colExpiryDate;
        private DevExpress.XtraGrid.Columns.GridColumn colWarrantyEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colScanOption;
        private DevExpress.XtraGrid.Columns.GridColumn colPLUType;
        private DevExpress.XtraGrid.Columns.GridColumn colPacking;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colGoodsStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colSupplierID;
        private DevExpress.XtraGrid.Columns.GridColumn colManufacturerID;
        private DevExpress.XtraGrid.Columns.GridColumn colPurchaseUnitID;
        private DevExpress.XtraGrid.Columns.GridColumn colPurchaseUnitRate;
        private DevExpress.XtraGrid.Columns.GridColumn colRetailUnitID;
        private DevExpress.XtraGrid.Columns.GridColumn colRetailUnitRate;
        private DevExpress.XtraGrid.Columns.GridColumn colWholesaleUnitID;
        private DevExpress.XtraGrid.Columns.GridColumn colWholesaleUnitRate;
        private DevExpress.XtraGrid.Columns.GridColumn colStyleID;
        private DevExpress.XtraGrid.Columns.GridColumn colSizeID;
        private DevExpress.XtraGrid.Columns.GridColumn colColorID;
        private DevExpress.XtraGrid.Columns.GridColumn colSeasonID;
        private DevExpress.XtraGrid.Columns.GridColumn colMaterialID;
        private DevExpress.XtraGrid.Columns.GridColumn colLength;
        private DevExpress.XtraGrid.Columns.GridColumn colLengthUnit;
        private DevExpress.XtraGrid.Columns.GridColumn colWidth;
        private DevExpress.XtraGrid.Columns.GridColumn colWidthUnit;
        private DevExpress.XtraGrid.Columns.GridColumn colHeight;
        private DevExpress.XtraGrid.Columns.GridColumn colHeightUnit;
        private DevExpress.XtraGrid.Columns.GridColumn colWeight;
        private DevExpress.XtraGrid.Columns.GridColumn colWeightUnit;
        private DevExpress.XtraGrid.Columns.GridColumn colDiameter;
        private DevExpress.XtraGrid.Columns.GridColumn colDiameterUnit;
        private DevExpress.XtraGrid.Columns.GridColumn colGauge;
        private DevExpress.XtraGrid.Columns.GridColumn colGaugeUnit;
        private DevExpress.XtraGrid.Columns.GridColumn colVolume;
        private DevExpress.XtraGrid.Columns.GridColumn colVolumeUnit;
        private DevExpress.XtraGrid.Columns.GridColumn colDensity;
        private DevExpress.XtraGrid.Columns.GridColumn colVATInput;
        private DevExpress.XtraGrid.Columns.GridColumn colVATOutput;
        private DevExpress.XtraGrid.Columns.GridColumn colImportTax;
        private DevExpress.XtraGrid.Columns.GridColumn colSpecialTax;
        private DevExpress.XtraGrid.Columns.GridColumn colTransportRate;
        private DevExpress.XtraGrid.Columns.GridColumn colOtherRate;
        private DevExpress.XtraGrid.Columns.GridColumn colDiscountInput;
        private DevExpress.XtraGrid.Columns.GridColumn colDiscountOutput;
        private DevExpress.XtraGrid.Columns.GridColumn colCostPrice;
        private DevExpress.XtraGrid.Columns.GridColumn colRetailSalePrice;
        private DevExpress.XtraGrid.Columns.GridColumn colWholesalePrice;
        private DevExpress.XtraGrid.Columns.GridColumn colInternalSalePrice;
        private DevExpress.XtraGrid.Columns.GridColumn colMargin;
        private DevExpress.XtraGrid.Columns.GridColumn colDiscountInternal;
        private DevExpress.XtraGrid.Columns.GridColumn colMinQuantity;
        private DevExpress.XtraGrid.Columns.GridColumn colMaxQuantity;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedUserID;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedDate;
        private DevExpress.XtraGrid.Columns.GridColumn colUpdatedUserID;
        private DevExpress.XtraGrid.Columns.GridColumn colUpdatedDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
    }
}
