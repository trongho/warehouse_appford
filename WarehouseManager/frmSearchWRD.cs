﻿using DevExpress.XtraBars.Docking2010;
using DevExpress.XtraGrid.Views.Grid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WarehouseManager.Models;
using WarehouseManager.Repository;
using webservice.Entities;

namespace WarehouseManager
{
    public partial class frmSearchWRD : Form
    {
        HandlingStatusRepository handlingStatusRepository;
        OrderStatusRepository orderStatusRepository;
        BranchRepository branchRepository;
        WRDataHeaderRepository wRDataHeaderRepository;
        public frmSearchWRD()
        {
            InitializeComponent();
            this.Load += frmSearchWRR_Load;
            handlingStatusRepository = new HandlingStatusRepository();
            branchRepository = new BranchRepository();
            wRDataHeaderRepository = new WRDataHeaderRepository();
            orderStatusRepository = new OrderStatusRepository();
        }

        private void frmSearchWRR_Load(object sender, EventArgs e)
        {
            popupMenuSelectBranch();
            popupMenuSelectHandlingStatus();
            popupMenuSelectOrderStatus();
            sbLoadDataForGridWRDHeaderAsync();
            gridViewWRDataHeader.DoubleClick += dataGridView_CellDoubleClick;
            simpleButtonFilter.Click += filterButton_Click;
            windowsUIButtonPanel1.ButtonClick += button_Click;
            windowsUIButtonPanel1.Cursor = Cursors.Hand;
            this.KeyDown += new KeyEventHandler(Form1_KeyDown);

            if (int.Parse(DateTime.Now.Day.ToString()) == 1)
            {
                dateEditFromDate.Text = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day).ToString("yyyy-MM-dd");
            }
            else
            {
                dateEditFromDate.Text = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day - 1).ToString("yyyy-MM-dd");
            }
            dateEditToDate.Text = DateTime.Now.ToString("yyyy-MM-dd");
        }

        public async void button_Click(object sender, EventArgs e)
        {
            ButtonEventArgs eArg = (DevExpress.XtraBars.Docking2010.ButtonEventArgs)e;
            String tag = eArg.Button.Properties.Tag.ToString();
            switch (tag)
            {
                case "refesh":
                    sbLoadDataForGridWRDHeaderAsync();
                    break;
                case "close":
                    this.Close();
                    break;
            }

        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F5)
            {
                sbLoadDataForGridWRDHeaderAsync();
            }
            if (e.KeyCode == Keys.F10)
            {
                this.Close();
            }
        }

        private async Task popupMenuSelectHandlingStatus()
        {
            List<HandlingStatus> handlingStatuses = await handlingStatusRepository.GetAll();
            foreach (HandlingStatus handlingStatus in handlingStatuses)
            {
                comboBoxEditHandlingStatus.Properties.Items.Add(handlingStatus.HandlingStatusName);
            }
            comboBoxEditHandlingStatus.SelectedIndex = 0;
        }

        private async Task popupMenuSelectOrderStatus()
        {
            List<OrderStatus> orderStatuses = await orderStatusRepository.GetAll();
            foreach (OrderStatus orderStatus in orderStatuses)
            {
                comboBoxEditOrderStatus.Properties.Items.Add(orderStatus.OrderStatusName);
            }
            comboBoxEditOrderStatus.SelectedIndex = 0;
        }

        private async Task popupMenuSelectBranch()
        {
            List<Branch> branches = await branchRepository.getAllBranchAsync();
            foreach (Branch branch in branches)
            {
                comboBoxEditBranch.Properties.Items.Add(branch.BranchName);
            }
            comboBoxEditBranch.SelectedIndex = 0;
        }

        private async Task sbLoadDataForGridWRDHeaderAsync()
        {
            List<WRDataHeader> wRDataHeaders = (await wRDataHeaderRepository.GetAll()).OrderByDescending(o => o.CreatedDate.Value.Date).ToList();
            gridControlWRDataHeader.DataSource =wRDataHeaders;
        }

        private async void dataGridView_CellDoubleClick(object sender, EventArgs e)
        {
            if (gridViewWRDataHeader.RowCount > 0)
            {
                WRDataHeader wRDataHeader = await wRDataHeaderRepository.GetUnderID((string)(sender as GridView).GetFocusedRowCellValue("WRDNumber"));
                ReceiptDataControl.selectedWRDNumber = (string)(sender as GridView).GetFocusedRowCellValue("WRDNumber");
                this.Close();
            }
        }

        private async void filterButton_Click(Object sender,EventArgs e)
        {
            String branchID = "<>";
            String handlingStatusID = "<>";
            if (!comboBoxEditBranch.SelectedItem.ToString().Equals("<>"))
            {
                branchID = $"{int.Parse(comboBoxEditBranch.SelectedIndex.ToString()) - 1:D3}";
            }
            if (!comboBoxEditHandlingStatus.SelectedItem.ToString().Equals("<>"))
            {
                handlingStatusID = (comboBoxEditHandlingStatus.SelectedIndex- 1).ToString();
            }
            String orderStatusID = (comboBoxEditOrderStatus.SelectedIndex-1).ToString();
            DateTime fromDate = DateTime.Parse(dateEditFromDate.Text);
            DateTime toDate = DateTime.Parse(dateEditToDate.Text);

            List<WRDataHeader> wRDataHeaders = new List<WRDataHeader>();
            
            wRDataHeaders = await wRDataHeaderRepository.FilterFinal(branchID, fromDate, toDate, handlingStatusID);
            

            //List<WRDataHeader> wRDataHeadersUnderBranch = new List<WRDataHeader>();
            //List<WRDataHeader> wRDataHeadersUnderDate = new List<WRDataHeader>();
            //List<WRDataHeader> wRDataHeadersUnderHandlingStatus = new List<WRDataHeader>();
            //List<WRDataHeader> wRDataHeadersUnderOrderStatus = new List<WRDataHeader>();

            //if (branchID.Equals("<>"))
            //{
            //    wRDataHeadersUnderBranch = await wRDataHeaderRepository.GetAll();
            //}
            //else
            //{
            //    wRDataHeadersUnderBranch = await wRDataHeaderRepository.GetUnderBranch(branchID);
            //}
            //if (handlingStatusID.Equals("<>"))
            //{
            //    wRDataHeadersUnderHandlingStatus = await wRDataHeaderRepository.GetAll();
            //}
            //else
            //{
            //    wRDataHeadersUnderHandlingStatus = await wRDataHeaderRepository.GetUnderHandlingStatus(handlingStatusID);
            //}
            //if (orderStatusID.Equals("<>"))
            //{
            //   wRDataHeadersUnderOrderStatus = await wRDataHeaderRepository.GetAll();
            //}
            //else
            //{
            //    wRDataHeadersUnderOrderStatus = await wRDataHeaderRepository.GetUnderOrderStatus(orderStatusID);
            //}
            //wRDataHeadersUnderDate = await wRDataHeaderRepository.GetUnderDate(fromDate, toDate);

            //for (int i = 0; i < wRDataHeadersUnderBranch.Count; i++)
            //{
            //    for (int j = 0; j < wRDataHeadersUnderDate.Count; j++)
            //    {
            //        for (int k = 0; k < wRDataHeadersUnderHandlingStatus.Count; k++)
            //        {
            //            for (int h = 0; h < wRDataHeadersUnderOrderStatus.Count; h++)
            //            {
            //                if (wRDataHeadersUnderBranch[i].WRRNumber.Equals(wRDataHeadersUnderDate[j].WRRNumber)
            //                && wRDataHeadersUnderDate[j].WRRNumber.Equals(wRDataHeadersUnderHandlingStatus[k].WRRNumber)
            //                && wRDataHeadersUnderHandlingStatus[k].WRRNumber.Equals(wRDataHeadersUnderOrderStatus[h].WRRNumber))
            //                {
            //                    wRDataHeaders.Add(wRDataHeadersUnderBranch[i]);
            //                }
            //            }
            //        }
            //    }
            //}
            gridControlWRDataHeader.DataSource = wRDataHeaders;
        }


    }
}
