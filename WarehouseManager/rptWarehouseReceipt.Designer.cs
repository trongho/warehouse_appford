﻿
namespace WarehouseManager
{
    partial class rptWarehouseReceipt
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions1 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptWarehouseReceipt));
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions2 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions3 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions4 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition1 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition2 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition3 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition4 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition1 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition2 = new DevExpress.XtraLayout.RowDefinition();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.windowsUIButtonPanel1 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.comboBoxEditBranch = new DevExpress.XtraEditors.ComboBoxEdit();
            this.dateEditFromDate = new DevExpress.XtraEditors.DateEdit();
            this.dateEditToDate = new DevExpress.XtraEditors.DateEdit();
            this.comboBoxEditHandlingStatus = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEditmodality = new DevExpress.XtraEditors.ComboBoxEdit();
            this.simpleButtonFilter = new DevExpress.XtraEditors.SimpleButton();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemBranch = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemFromDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemToDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemModality = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControl3 = new DevExpress.XtraLayout.LayoutControl();
            this.gridControlData = new DevExpress.XtraGrid.GridControl();
            this.gridViewData = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colWRNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colModalityID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colModalityName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWRDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReferenceNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWRRNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWRRReference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWarehouseID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWarehouseName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSupplierID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSupplierName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCustomerID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCustomerName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrderNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrderDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIONumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPriceTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPriceTypeName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPaymentTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPaymentTypeName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHandlingStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHandlingStatusName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBranchID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBranchName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocalOrderType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalDiscountAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalVATAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalAmountExcludedVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalAmountIncludedVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalQuantity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNote = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCheckerID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCheckerName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCheckerID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCheckerName2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedUserID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUpdatedUserID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUpdatedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemHandlingStatus = new DevExpress.XtraLayout.LayoutControlItem();
            this.textEditTotalQuantity = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlItemTotalQuantity = new DevExpress.XtraLayout.LayoutControlItem();
            this.textEditTotalAmount = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlItemTotalAmount = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditBranch.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditHandlingStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditmodality.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemBranch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemFromDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemToDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemModality)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).BeginInit();
            this.layoutControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemHandlingStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalQuantity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalQuantity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalAmount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalAmount)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.windowsUIButtonPanel1);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl2.Location = new System.Drawing.Point(0, 0);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(716, 0, 650, 400);
            this.layoutControl2.Root = this.layoutControlGroup1;
            this.layoutControl2.Size = new System.Drawing.Size(1074, 40);
            this.layoutControl2.TabIndex = 1;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // windowsUIButtonPanel1
            // 
            this.windowsUIButtonPanel1.AllowGlyphSkinning = false;
            this.windowsUIButtonPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            windowsUIButtonImageOptions1.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions1.Image")));
            windowsUIButtonImageOptions1.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions2.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions2.Image")));
            windowsUIButtonImageOptions2.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions3.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions3.Image")));
            windowsUIButtonImageOptions3.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions4.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions4.Image")));
            windowsUIButtonImageOptions4.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            this.windowsUIButtonPanel1.Buttons.AddRange(new DevExpress.XtraEditors.ButtonPanel.IBaseButton[] {
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F6) Làm mới", true, windowsUIButtonImageOptions1, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "refesh", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F8) Xuất dữ liệu", true, windowsUIButtonImageOptions2, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "export", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F9) In ấn", true, windowsUIButtonImageOptions3, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "print", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F10) Thoát", true, windowsUIButtonImageOptions4, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "close", -1, false)});
            this.windowsUIButtonPanel1.Location = new System.Drawing.Point(2, 2);
            this.windowsUIButtonPanel1.Name = "windowsUIButtonPanel1";
            this.windowsUIButtonPanel1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.windowsUIButtonPanel1.Size = new System.Drawing.Size(1070, 32);
            this.windowsUIButtonPanel1.TabIndex = 7;
            this.windowsUIButtonPanel1.Text = "windowsUIButtonPanel1";
            this.windowsUIButtonPanel1.UseButtonBackgroundImages = false;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(1074, 40);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.windowsUIButtonPanel1;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(1074, 40);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.comboBoxEditBranch);
            this.layoutControl1.Controls.Add(this.dateEditFromDate);
            this.layoutControl1.Controls.Add(this.dateEditToDate);
            this.layoutControl1.Controls.Add(this.comboBoxEditHandlingStatus);
            this.layoutControl1.Controls.Add(this.comboBoxEditmodality);
            this.layoutControl1.Controls.Add(this.simpleButtonFilter);
            this.layoutControl1.Controls.Add(this.textEditTotalQuantity);
            this.layoutControl1.Controls.Add(this.textEditTotalAmount);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl1.Location = new System.Drawing.Point(0, 40);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(307, 227, 650, 400);
            this.layoutControl1.Root = this.Root;
            this.layoutControl1.Size = new System.Drawing.Size(1074, 81);
            this.layoutControl1.TabIndex = 2;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // comboBoxEditBranch
            // 
            this.comboBoxEditBranch.Location = new System.Drawing.Point(91, 12);
            this.comboBoxEditBranch.Name = "comboBoxEditBranch";
            this.comboBoxEditBranch.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditBranch.Size = new System.Drawing.Size(338, 20);
            this.comboBoxEditBranch.StyleController = this.layoutControl1;
            this.comboBoxEditBranch.TabIndex = 4;
            // 
            // dateEditFromDate
            // 
            this.dateEditFromDate.EditValue = null;
            this.dateEditFromDate.Location = new System.Drawing.Point(512, 12);
            this.dateEditFromDate.Name = "dateEditFromDate";
            this.dateEditFromDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditFromDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditFromDate.Size = new System.Drawing.Size(128, 20);
            this.dateEditFromDate.StyleController = this.layoutControl1;
            this.dateEditFromDate.TabIndex = 9;
            // 
            // dateEditToDate
            // 
            this.dateEditToDate.EditValue = null;
            this.dateEditToDate.Location = new System.Drawing.Point(723, 12);
            this.dateEditToDate.Name = "dateEditToDate";
            this.dateEditToDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditToDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditToDate.Size = new System.Drawing.Size(128, 20);
            this.dateEditToDate.StyleController = this.layoutControl1;
            this.dateEditToDate.TabIndex = 10;
            // 
            // comboBoxEditHandlingStatus
            // 
            this.comboBoxEditHandlingStatus.Location = new System.Drawing.Point(512, 42);
            this.comboBoxEditHandlingStatus.Name = "comboBoxEditHandlingStatus";
            this.comboBoxEditHandlingStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditHandlingStatus.Size = new System.Drawing.Size(128, 20);
            this.comboBoxEditHandlingStatus.StyleController = this.layoutControl1;
            this.comboBoxEditHandlingStatus.TabIndex = 7;
            // 
            // comboBoxEditmodality
            // 
            this.comboBoxEditmodality.Location = new System.Drawing.Point(91, 42);
            this.comboBoxEditmodality.Name = "comboBoxEditmodality";
            this.comboBoxEditmodality.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditmodality.Size = new System.Drawing.Size(338, 20);
            this.comboBoxEditmodality.StyleController = this.layoutControl1;
            this.comboBoxEditmodality.TabIndex = 11;
            // 
            // simpleButtonFilter
            // 
            this.simpleButtonFilter.Location = new System.Drawing.Point(644, 42);
            this.simpleButtonFilter.Name = "simpleButtonFilter";
            this.simpleButtonFilter.Size = new System.Drawing.Size(207, 22);
            this.simpleButtonFilter.StyleController = this.layoutControl1;
            this.simpleButtonFilter.TabIndex = 13;
            this.simpleButtonFilter.Text = "Lọc báo cáo";
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemBranch,
            this.layoutControlItemFromDate,
            this.layoutControlItemToDate,
            this.layoutControlItemModality,
            this.layoutControlItem4,
            this.layoutControlItemHandlingStatus,
            this.layoutControlItemTotalQuantity,
            this.layoutControlItemTotalAmount});
            this.Root.LayoutMode = DevExpress.XtraLayout.Utils.LayoutMode.Table;
            this.Root.Name = "Root";
            columnDefinition1.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition1.Width = 40D;
            columnDefinition2.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition2.Width = 20D;
            columnDefinition3.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition3.Width = 20D;
            columnDefinition4.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition4.Width = 20D;
            this.Root.OptionsTableLayoutGroup.ColumnDefinitions.AddRange(new DevExpress.XtraLayout.ColumnDefinition[] {
            columnDefinition1,
            columnDefinition2,
            columnDefinition3,
            columnDefinition4});
            rowDefinition1.Height = 50D;
            rowDefinition1.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition2.Height = 50D;
            rowDefinition2.SizeType = System.Windows.Forms.SizeType.Percent;
            this.Root.OptionsTableLayoutGroup.RowDefinitions.AddRange(new DevExpress.XtraLayout.RowDefinition[] {
            rowDefinition1,
            rowDefinition2});
            this.Root.Size = new System.Drawing.Size(1074, 81);
            this.Root.TextVisible = false;
            // 
            // layoutControlItemBranch
            // 
            this.layoutControlItemBranch.Control = this.comboBoxEditBranch;
            this.layoutControlItemBranch.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItemBranch.CustomizationFormText = "Chi nhánh";
            this.layoutControlItemBranch.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemBranch.Name = "layoutControlItemBranch";
            this.layoutControlItemBranch.Size = new System.Drawing.Size(421, 30);
            this.layoutControlItemBranch.Text = "Chi nhánh";
            this.layoutControlItemBranch.TextSize = new System.Drawing.Size(76, 13);
            // 
            // layoutControlItemFromDate
            // 
            this.layoutControlItemFromDate.Control = this.dateEditFromDate;
            this.layoutControlItemFromDate.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItemFromDate.CustomizationFormText = "Từ ngày";
            this.layoutControlItemFromDate.Location = new System.Drawing.Point(421, 0);
            this.layoutControlItemFromDate.Name = "layoutControlItemFromDate";
            this.layoutControlItemFromDate.OptionsTableLayoutItem.ColumnIndex = 1;
            this.layoutControlItemFromDate.Size = new System.Drawing.Size(211, 30);
            this.layoutControlItemFromDate.Text = "Từ ngày";
            this.layoutControlItemFromDate.TextSize = new System.Drawing.Size(76, 13);
            // 
            // layoutControlItemToDate
            // 
            this.layoutControlItemToDate.Control = this.dateEditToDate;
            this.layoutControlItemToDate.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItemToDate.CustomizationFormText = "Đến ngày";
            this.layoutControlItemToDate.Location = new System.Drawing.Point(632, 0);
            this.layoutControlItemToDate.Name = "layoutControlItemToDate";
            this.layoutControlItemToDate.OptionsTableLayoutItem.ColumnIndex = 2;
            this.layoutControlItemToDate.Size = new System.Drawing.Size(211, 30);
            this.layoutControlItemToDate.Text = "Đến ngày";
            this.layoutControlItemToDate.TextSize = new System.Drawing.Size(76, 13);
            // 
            // layoutControlItemModality
            // 
            this.layoutControlItemModality.Control = this.comboBoxEditmodality;
            this.layoutControlItemModality.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItemModality.CustomizationFormText = "Phương thức";
            this.layoutControlItemModality.Location = new System.Drawing.Point(0, 30);
            this.layoutControlItemModality.Name = "layoutControlItemModality";
            this.layoutControlItemModality.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItemModality.Size = new System.Drawing.Size(421, 31);
            this.layoutControlItemModality.Text = "Phương thức";
            this.layoutControlItemModality.TextSize = new System.Drawing.Size(76, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.simpleButtonFilter;
            this.layoutControlItem4.Location = new System.Drawing.Point(632, 30);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.OptionsTableLayoutItem.ColumnIndex = 2;
            this.layoutControlItem4.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItem4.Size = new System.Drawing.Size(211, 31);
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControl3
            // 
            this.layoutControl3.Controls.Add(this.gridControlData);
            this.layoutControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl3.Location = new System.Drawing.Point(0, 121);
            this.layoutControl3.Name = "layoutControl3";
            this.layoutControl3.Root = this.layoutControlGroup2;
            this.layoutControl3.Size = new System.Drawing.Size(1074, 500);
            this.layoutControl3.TabIndex = 3;
            this.layoutControl3.Text = "layoutControl3";
            // 
            // gridControlData
            // 
            this.gridControlData.Location = new System.Drawing.Point(12, 12);
            this.gridControlData.MainView = this.gridViewData;
            this.gridControlData.Name = "gridControlData";
            this.gridControlData.Size = new System.Drawing.Size(1050, 476);
            this.gridControlData.TabIndex = 7;
            this.gridControlData.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewData});
            // 
            // gridViewData
            // 
            this.gridViewData.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colWRNumber,
            this.colModalityID,
            this.colModalityName,
            this.colWRDate,
            this.colReferenceNumber,
            this.colWRRNumber,
            this.colWRRReference,
            this.colWarehouseID,
            this.colWarehouseName,
            this.colSupplierID,
            this.colSupplierName,
            this.colCustomerID,
            this.colCustomerName,
            this.colOrderNumber,
            this.colOrderDate,
            this.colContractNumber,
            this.colContractDate,
            this.colIONumber,
            this.colPriceTypeID,
            this.colPriceTypeName,
            this.colPaymentTypeID,
            this.colPaymentTypeName,
            this.colHandlingStatusID,
            this.colHandlingStatusName,
            this.colBranchID,
            this.colBranchName,
            this.colLocalOrderType,
            this.colTotalDiscountAmount,
            this.colTotalVATAmount,
            this.colTotalAmountExcludedVAT,
            this.colTotalAmountIncludedVAT,
            this.colTotalAmount,
            this.colTotalQuantity,
            this.colNote,
            this.colCheckerID1,
            this.colCheckerName1,
            this.colCheckerID2,
            this.colCheckerName2,
            this.colStatus,
            this.colCreatedUserID,
            this.colCreatedDate,
            this.colUpdatedUserID,
            this.colUpdatedDate});
            this.gridViewData.GridControl = this.gridControlData;
            this.gridViewData.Name = "gridViewData";
            this.gridViewData.OptionsFind.AllowFindPanel = false;
            // 
            // colWRNumber
            // 
            this.colWRNumber.FieldName = "WRNumber";
            this.colWRNumber.Name = "colWRNumber";
            this.colWRNumber.Visible = true;
            this.colWRNumber.VisibleIndex = 0;
            // 
            // colModalityID
            // 
            this.colModalityID.FieldName = "ModalityID";
            this.colModalityID.Name = "colModalityID";
            // 
            // colModalityName
            // 
            this.colModalityName.FieldName = "ModalityName";
            this.colModalityName.Name = "colModalityName";
            // 
            // colWRDate
            // 
            this.colWRDate.FieldName = "WRDate";
            this.colWRDate.Name = "colWRDate";
            this.colWRDate.Visible = true;
            this.colWRDate.VisibleIndex = 1;
            // 
            // colReferenceNumber
            // 
            this.colReferenceNumber.FieldName = "ReferenceNumber";
            this.colReferenceNumber.Name = "colReferenceNumber";
            this.colReferenceNumber.Visible = true;
            this.colReferenceNumber.VisibleIndex = 2;
            // 
            // colWRRNumber
            // 
            this.colWRRNumber.FieldName = "WRRNumber";
            this.colWRRNumber.Name = "colWRRNumber";
            // 
            // colWRRReference
            // 
            this.colWRRReference.FieldName = "WRRReference";
            this.colWRRReference.Name = "colWRRReference";
            // 
            // colWarehouseID
            // 
            this.colWarehouseID.FieldName = "WarehouseID";
            this.colWarehouseID.Name = "colWarehouseID";
            // 
            // colWarehouseName
            // 
            this.colWarehouseName.FieldName = "WarehouseName";
            this.colWarehouseName.Name = "colWarehouseName";
            this.colWarehouseName.Visible = true;
            this.colWarehouseName.VisibleIndex = 3;
            // 
            // colSupplierID
            // 
            this.colSupplierID.FieldName = "SupplierID";
            this.colSupplierID.Name = "colSupplierID";
            // 
            // colSupplierName
            // 
            this.colSupplierName.FieldName = "SupplierName";
            this.colSupplierName.Name = "colSupplierName";
            // 
            // colCustomerID
            // 
            this.colCustomerID.FieldName = "CustomerID";
            this.colCustomerID.Name = "colCustomerID";
            // 
            // colCustomerName
            // 
            this.colCustomerName.FieldName = "CustomerName";
            this.colCustomerName.Name = "colCustomerName";
            // 
            // colOrderNumber
            // 
            this.colOrderNumber.FieldName = "OrderNumber";
            this.colOrderNumber.Name = "colOrderNumber";
            // 
            // colOrderDate
            // 
            this.colOrderDate.FieldName = "OrderDate";
            this.colOrderDate.Name = "colOrderDate";
            // 
            // colContractNumber
            // 
            this.colContractNumber.FieldName = "ContractNumber";
            this.colContractNumber.Name = "colContractNumber";
            // 
            // colContractDate
            // 
            this.colContractDate.FieldName = "ContractDate";
            this.colContractDate.Name = "colContractDate";
            // 
            // colIONumber
            // 
            this.colIONumber.FieldName = "IONumber";
            this.colIONumber.Name = "colIONumber";
            // 
            // colPriceTypeID
            // 
            this.colPriceTypeID.FieldName = "PriceTypeID";
            this.colPriceTypeID.Name = "colPriceTypeID";
            // 
            // colPriceTypeName
            // 
            this.colPriceTypeName.FieldName = "PriceTypeName";
            this.colPriceTypeName.Name = "colPriceTypeName";
            // 
            // colPaymentTypeID
            // 
            this.colPaymentTypeID.FieldName = "PaymentTypeID";
            this.colPaymentTypeID.Name = "colPaymentTypeID";
            // 
            // colPaymentTypeName
            // 
            this.colPaymentTypeName.FieldName = "PaymentTypeName";
            this.colPaymentTypeName.Name = "colPaymentTypeName";
            // 
            // colHandlingStatusID
            // 
            this.colHandlingStatusID.FieldName = "HandlingStatusID";
            this.colHandlingStatusID.Name = "colHandlingStatusID";
            // 
            // colHandlingStatusName
            // 
            this.colHandlingStatusName.FieldName = "HandlingStatusName";
            this.colHandlingStatusName.Name = "colHandlingStatusName";
            this.colHandlingStatusName.Visible = true;
            this.colHandlingStatusName.VisibleIndex = 4;
            // 
            // colBranchID
            // 
            this.colBranchID.FieldName = "BranchID";
            this.colBranchID.Name = "colBranchID";
            // 
            // colBranchName
            // 
            this.colBranchName.FieldName = "BranchName";
            this.colBranchName.Name = "colBranchName";
            // 
            // colLocalOrderType
            // 
            this.colLocalOrderType.FieldName = "LocalOrderType";
            this.colLocalOrderType.Name = "colLocalOrderType";
            // 
            // colTotalDiscountAmount
            // 
            this.colTotalDiscountAmount.FieldName = "TotalDiscountAmount";
            this.colTotalDiscountAmount.Name = "colTotalDiscountAmount";
            // 
            // colTotalVATAmount
            // 
            this.colTotalVATAmount.FieldName = "TotalVATAmount";
            this.colTotalVATAmount.Name = "colTotalVATAmount";
            // 
            // colTotalAmountExcludedVAT
            // 
            this.colTotalAmountExcludedVAT.FieldName = "TotalAmountExcludedVAT";
            this.colTotalAmountExcludedVAT.Name = "colTotalAmountExcludedVAT";
            // 
            // colTotalAmountIncludedVAT
            // 
            this.colTotalAmountIncludedVAT.FieldName = "TotalAmountIncludedVAT";
            this.colTotalAmountIncludedVAT.Name = "colTotalAmountIncludedVAT";
            // 
            // colTotalAmount
            // 
            this.colTotalAmount.FieldName = "TotalAmount";
            this.colTotalAmount.Name = "colTotalAmount";
            this.colTotalAmount.Visible = true;
            this.colTotalAmount.VisibleIndex = 5;
            // 
            // colTotalQuantity
            // 
            this.colTotalQuantity.FieldName = "TotalQuantity";
            this.colTotalQuantity.Name = "colTotalQuantity";
            this.colTotalQuantity.Visible = true;
            this.colTotalQuantity.VisibleIndex = 6;
            // 
            // colNote
            // 
            this.colNote.FieldName = "Note";
            this.colNote.Name = "colNote";
            this.colNote.Visible = true;
            this.colNote.VisibleIndex = 7;
            // 
            // colCheckerID1
            // 
            this.colCheckerID1.FieldName = "CheckerID1";
            this.colCheckerID1.Name = "colCheckerID1";
            // 
            // colCheckerName1
            // 
            this.colCheckerName1.FieldName = "CheckerName1";
            this.colCheckerName1.Name = "colCheckerName1";
            // 
            // colCheckerID2
            // 
            this.colCheckerID2.FieldName = "CheckerID2";
            this.colCheckerID2.Name = "colCheckerID2";
            // 
            // colCheckerName2
            // 
            this.colCheckerName2.FieldName = "CheckerName2";
            this.colCheckerName2.Name = "colCheckerName2";
            // 
            // colStatus
            // 
            this.colStatus.FieldName = "Status";
            this.colStatus.Name = "colStatus";
            // 
            // colCreatedUserID
            // 
            this.colCreatedUserID.FieldName = "CreatedUserID";
            this.colCreatedUserID.Name = "colCreatedUserID";
            // 
            // colCreatedDate
            // 
            this.colCreatedDate.FieldName = "CreatedDate";
            this.colCreatedDate.Name = "colCreatedDate";
            // 
            // colUpdatedUserID
            // 
            this.colUpdatedUserID.FieldName = "UpdatedUserID";
            this.colUpdatedUserID.Name = "colUpdatedUserID";
            // 
            // colUpdatedDate
            // 
            this.colUpdatedDate.FieldName = "UpdatedDate";
            this.colUpdatedDate.Name = "colUpdatedDate";
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3});
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(1074, 500);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.gridControlData;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(1054, 480);
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItemHandlingStatus
            // 
            this.layoutControlItemHandlingStatus.Control = this.comboBoxEditHandlingStatus;
            this.layoutControlItemHandlingStatus.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItemHandlingStatus.CustomizationFormText = "Trạng thái xử lý";
            this.layoutControlItemHandlingStatus.Location = new System.Drawing.Point(421, 30);
            this.layoutControlItemHandlingStatus.Name = "layoutControlItemHandlingStatus";
            this.layoutControlItemHandlingStatus.OptionsTableLayoutItem.ColumnIndex = 1;
            this.layoutControlItemHandlingStatus.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItemHandlingStatus.Size = new System.Drawing.Size(211, 31);
            this.layoutControlItemHandlingStatus.Text = "Trạng thái xử lý";
            this.layoutControlItemHandlingStatus.TextSize = new System.Drawing.Size(76, 13);
            // 
            // textEditTotalQuantity
            // 
            this.textEditTotalQuantity.Location = new System.Drawing.Point(934, 12);
            this.textEditTotalQuantity.Name = "textEditTotalQuantity";
            this.textEditTotalQuantity.Size = new System.Drawing.Size(128, 20);
            this.textEditTotalQuantity.StyleController = this.layoutControl1;
            this.textEditTotalQuantity.TabIndex = 14;
            // 
            // layoutControlItemTotalQuantity
            // 
            this.layoutControlItemTotalQuantity.Control = this.textEditTotalQuantity;
            this.layoutControlItemTotalQuantity.Location = new System.Drawing.Point(843, 0);
            this.layoutControlItemTotalQuantity.Name = "layoutControlItemTotalQuantity";
            this.layoutControlItemTotalQuantity.OptionsTableLayoutItem.ColumnIndex = 3;
            this.layoutControlItemTotalQuantity.Size = new System.Drawing.Size(211, 30);
            this.layoutControlItemTotalQuantity.Text = "Σ Lượng nhập";
            this.layoutControlItemTotalQuantity.TextSize = new System.Drawing.Size(76, 13);
            // 
            // textEditTotalAmount
            // 
            this.textEditTotalAmount.Location = new System.Drawing.Point(934, 42);
            this.textEditTotalAmount.Name = "textEditTotalAmount";
            this.textEditTotalAmount.Size = new System.Drawing.Size(128, 20);
            this.textEditTotalAmount.StyleController = this.layoutControl1;
            this.textEditTotalAmount.TabIndex = 15;
            // 
            // layoutControlItemTotalAmount
            // 
            this.layoutControlItemTotalAmount.Control = this.textEditTotalAmount;
            this.layoutControlItemTotalAmount.Location = new System.Drawing.Point(843, 30);
            this.layoutControlItemTotalAmount.Name = "layoutControlItemTotalAmount";
            this.layoutControlItemTotalAmount.OptionsTableLayoutItem.ColumnIndex = 3;
            this.layoutControlItemTotalAmount.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItemTotalAmount.Size = new System.Drawing.Size(211, 31);
            this.layoutControlItemTotalAmount.Text = "Σ Trị nhập";
            this.layoutControlItemTotalAmount.TextSize = new System.Drawing.Size(76, 13);
            // 
            // rptWarehouseReceipt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl3);
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.layoutControl2);
            this.Name = "rptWarehouseReceipt";
            this.Size = new System.Drawing.Size(1074, 621);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditBranch.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditHandlingStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditmodality.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemBranch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemFromDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemToDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemModality)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).EndInit();
            this.layoutControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemHandlingStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalQuantity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalQuantity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalAmount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalAmount)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel windowsUIButtonPanel1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditBranch;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemBranch;
        private DevExpress.XtraEditors.DateEdit dateEditFromDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemFromDate;
        private DevExpress.XtraEditors.DateEdit dateEditToDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemToDate;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditHandlingStatus;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditmodality;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemModality;
        private DevExpress.XtraLayout.LayoutControl layoutControl3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraGrid.GridControl gridControlData;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewData;
        private DevExpress.XtraGrid.Columns.GridColumn colWRNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colModalityID;
        private DevExpress.XtraGrid.Columns.GridColumn colModalityName;
        private DevExpress.XtraGrid.Columns.GridColumn colWRDate;
        private DevExpress.XtraGrid.Columns.GridColumn colReferenceNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colWRRNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colWRRReference;
        private DevExpress.XtraGrid.Columns.GridColumn colWarehouseID;
        private DevExpress.XtraGrid.Columns.GridColumn colWarehouseName;
        private DevExpress.XtraGrid.Columns.GridColumn colSupplierID;
        private DevExpress.XtraGrid.Columns.GridColumn colSupplierName;
        private DevExpress.XtraGrid.Columns.GridColumn colCustomerID;
        private DevExpress.XtraGrid.Columns.GridColumn colCustomerName;
        private DevExpress.XtraGrid.Columns.GridColumn colOrderNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colOrderDate;
        private DevExpress.XtraGrid.Columns.GridColumn colContractNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colContractDate;
        private DevExpress.XtraGrid.Columns.GridColumn colIONumber;
        private DevExpress.XtraGrid.Columns.GridColumn colPriceTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colPriceTypeName;
        private DevExpress.XtraGrid.Columns.GridColumn colPaymentTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colPaymentTypeName;
        private DevExpress.XtraGrid.Columns.GridColumn colHandlingStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colHandlingStatusName;
        private DevExpress.XtraGrid.Columns.GridColumn colBranchID;
        private DevExpress.XtraGrid.Columns.GridColumn colBranchName;
        private DevExpress.XtraGrid.Columns.GridColumn colLocalOrderType;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalDiscountAmount;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalVATAmount;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalAmountExcludedVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalAmountIncludedVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalAmount;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalQuantity;
        private DevExpress.XtraGrid.Columns.GridColumn colNote;
        private DevExpress.XtraGrid.Columns.GridColumn colCheckerID1;
        private DevExpress.XtraGrid.Columns.GridColumn colCheckerName1;
        private DevExpress.XtraGrid.Columns.GridColumn colCheckerID2;
        private DevExpress.XtraGrid.Columns.GridColumn colCheckerName2;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedUserID;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedDate;
        private DevExpress.XtraGrid.Columns.GridColumn colUpdatedUserID;
        private DevExpress.XtraGrid.Columns.GridColumn colUpdatedDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraEditors.SimpleButton simpleButtonFilter;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.TextEdit textEditTotalQuantity;
        private DevExpress.XtraEditors.TextEdit textEditTotalAmount;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemHandlingStatus;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemTotalQuantity;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemTotalAmount;
    }
}
