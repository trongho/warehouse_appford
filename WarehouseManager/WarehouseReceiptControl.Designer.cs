﻿
namespace WarehouseManager
{
    partial class WarehouseReceiptControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions1 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WarehouseReceiptControl));
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions2 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions3 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions4 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions5 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions6 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions7 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions8 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions9 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition1 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition2 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition3 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition4 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition5 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition6 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition7 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition1 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition2 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition3 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition4 = new DevExpress.XtraLayout.RowDefinition();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.windowsUIButtonPanel1 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.wRDataDetailBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.wARHOUSE_HPDataSet1 = new WarehouseManager.WARHOUSE_HPDataSet1();
            this.wRDataDetailTableAdapter = new WarehouseManager.WARHOUSE_HPDataSet1TableAdapters.WRDataDetailTableAdapter();
            this.layoutControl3 = new DevExpress.XtraLayout.LayoutControl();
            this.gridControlWRDetail = new DevExpress.XtraGrid.GridControl();
            this.wRDetailBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewWRDetail = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colWRNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrdinal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGoodsID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGoodsName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReceiptUnitID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnitRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStockUnitID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPriceIncludedVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPriceExcludedVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDiscount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDiscountAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVATAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAmountExcludedVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAmountIncludedVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpiryDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSerialNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNote = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantityReceived = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPriceIncludedVATReceived = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPriceExcludedVATReceived = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPriceReceived = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDiscountReceived = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDiscountAmountReceived = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVATReceived = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVATAmountReceived = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAmountExcludedVATReceived = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAmountIncludedVATReceived = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAmountReceived = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantityOrdered = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrderQuantityReceived = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReceiptQuantityIssued = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReceiptQuantity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReceiptAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAverageCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.wRDetailBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.wRDetailTableAdapter = new WarehouseManager.WARHOUSE_HPDataSet1TableAdapters.WRDetailTableAdapter();
            this.dateEditContractDate = new DevExpress.XtraEditors.DateEdit();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.label1 = new System.Windows.Forms.Label();
            this.textEdit4 = new DevExpress.XtraEditors.TextEdit();
            this.textEditWRNumber = new DevExpress.XtraEditors.TextEdit();
            this.comboBoxEditModality = new DevExpress.XtraEditors.ComboBoxEdit();
            this.dateEditWRDate = new DevExpress.XtraEditors.DateEdit();
            this.textEditNote = new DevExpress.XtraEditors.TextEdit();
            this.comboBoxEditWarehouse = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEditOrderNumber = new DevExpress.XtraEditors.TextEdit();
            this.dateEditOrderDate = new DevExpress.XtraEditors.DateEdit();
            this.textEditSupplier = new DevExpress.XtraEditors.TextEdit();
            this.textEditContractNumber = new DevExpress.XtraEditors.TextEdit();
            this.comboBoxEditPriceType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEditPaymentType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEditHandlingStatus = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEditTotalQuantity = new DevExpress.XtraEditors.TextEdit();
            this.textEditTotalDiscountAmount = new DevExpress.XtraEditors.TextEdit();
            this.textEditTotalVATAmount = new DevExpress.XtraEditors.TextEdit();
            this.textEditTotalAmount = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemWRNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemModality = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemWRDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemWarehouse = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemOrderNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemOrderDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemTotalQuantity = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemTotalDiscountAmount = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemTotalVATAmount = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemTotalAmount = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemContractNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemContractDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemPriceType = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemPaymentType = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemHandlingStatus = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemSupplier = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemNote = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wRDataDetailBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wARHOUSE_HPDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).BeginInit();
            this.layoutControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlWRDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wRDetailBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewWRDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wRDetailBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditContractDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditContractDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditWRNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditModality.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditWRDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditWRDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditNote.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditWarehouse.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditOrderNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditOrderDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditOrderDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditSupplier.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditContractNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditPriceType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditPaymentType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditHandlingStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalQuantity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalDiscountAmount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalVATAmount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalAmount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWRNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemModality)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWRDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWarehouse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemOrderNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemOrderDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalQuantity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalDiscountAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalVATAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemContractNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemContractDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPriceType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPaymentType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemHandlingStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemSupplier)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemNote)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.windowsUIButtonPanel1);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(444, 166, 812, 495);
            this.layoutControl1.Root = this.Root;
            this.layoutControl1.Size = new System.Drawing.Size(1298, 37);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // windowsUIButtonPanel1
            // 
            this.windowsUIButtonPanel1.AllowGlyphSkinning = false;
            this.windowsUIButtonPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            windowsUIButtonImageOptions1.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions1.Image")));
            windowsUIButtonImageOptions1.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions2.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions2.Image")));
            windowsUIButtonImageOptions2.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions3.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions3.Image")));
            windowsUIButtonImageOptions3.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions4.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions4.Image")));
            windowsUIButtonImageOptions4.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions5.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions5.Image")));
            windowsUIButtonImageOptions5.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions6.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions6.Image")));
            windowsUIButtonImageOptions6.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions7.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions7.Image")));
            windowsUIButtonImageOptions7.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions8.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions8.Image")));
            windowsUIButtonImageOptions8.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions9.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions9.Image")));
            windowsUIButtonImageOptions9.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            this.windowsUIButtonPanel1.Buttons.AddRange(new DevExpress.XtraEditors.ButtonPanel.IBaseButton[] {
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("Thêm mới", true, windowsUIButtonImageOptions1, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "new", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("Lưu", true, windowsUIButtonImageOptions2, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "save", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("Xóa", true, windowsUIButtonImageOptions3, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "delete", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("Tìm kiếm", true, windowsUIButtonImageOptions4, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "search", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("Làm mới", true, windowsUIButtonImageOptions5, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "refesh", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("Nhập dữ liệu", true, windowsUIButtonImageOptions6, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "import", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("Xuất dữ liệu", true, windowsUIButtonImageOptions7, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "export", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("In ấn", true, windowsUIButtonImageOptions8, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "print", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("Thoát", true, windowsUIButtonImageOptions9, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "close", -1, false)});
            this.windowsUIButtonPanel1.ContentAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.windowsUIButtonPanel1.Location = new System.Drawing.Point(2, 2);
            this.windowsUIButtonPanel1.Name = "windowsUIButtonPanel1";
            this.windowsUIButtonPanel1.Padding = new System.Windows.Forms.Padding(5);
            this.windowsUIButtonPanel1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.windowsUIButtonPanel1.Size = new System.Drawing.Size(1294, 32);
            this.windowsUIButtonPanel1.TabIndex = 4;
            this.windowsUIButtonPanel1.Text = "windowsUIButtonPanel1";
            this.windowsUIButtonPanel1.UseButtonBackgroundImages = false;
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.Root.Name = "Root";
            this.Root.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.Root.Size = new System.Drawing.Size(1298, 37);
            this.Root.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.windowsUIButtonPanel1;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(1298, 37);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // wRDataDetailBindingSource
            // 
            this.wRDataDetailBindingSource.DataMember = "WRDataDetail";
            this.wRDataDetailBindingSource.DataSource = this.wARHOUSE_HPDataSet1;
            // 
            // wARHOUSE_HPDataSet1
            // 
            this.wARHOUSE_HPDataSet1.DataSetName = "WARHOUSE_HPDataSet1";
            this.wARHOUSE_HPDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // wRDataDetailTableAdapter
            // 
            this.wRDataDetailTableAdapter.ClearBeforeFill = true;
            // 
            // layoutControl3
            // 
            this.layoutControl3.Controls.Add(this.gridControlWRDetail);
            this.layoutControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl3.Location = new System.Drawing.Point(0, 160);
            this.layoutControl3.Name = "layoutControl3";
            this.layoutControl3.Root = this.layoutControlGroup2;
            this.layoutControl3.Size = new System.Drawing.Size(1298, 466);
            this.layoutControl3.TabIndex = 2;
            this.layoutControl3.Text = "layoutControl3";
            // 
            // gridControlWRDetail
            // 
            this.gridControlWRDetail.DataSource = this.wRDetailBindingSource1;
            this.gridControlWRDetail.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gridControlWRDetail.Location = new System.Drawing.Point(12, 12);
            this.gridControlWRDetail.MainView = this.gridViewWRDetail;
            this.gridControlWRDetail.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gridControlWRDetail.Name = "gridControlWRDetail";
            this.gridControlWRDetail.Size = new System.Drawing.Size(1274, 442);
            this.gridControlWRDetail.TabIndex = 5;
            this.gridControlWRDetail.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewWRDetail});
            // 
            // wRDetailBindingSource1
            // 
            this.wRDetailBindingSource1.DataMember = "WRDetail";
            this.wRDetailBindingSource1.DataSource = this.wARHOUSE_HPDataSet1;
            // 
            // gridViewWRDetail
            // 
            this.gridViewWRDetail.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridViewWRDetail.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridViewWRDetail.Appearance.HeaderPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.gridViewWRDetail.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colWRNumber,
            this.colOrdinal,
            this.colGoodsID,
            this.colGoodsName,
            this.colReceiptUnitID,
            this.colUnitRate,
            this.colStockUnitID,
            this.colQuantity,
            this.colPriceIncludedVAT,
            this.colPriceExcludedVAT,
            this.colPrice,
            this.colDiscount,
            this.colDiscountAmount,
            this.colVAT,
            this.colVATAmount,
            this.colAmountExcludedVAT,
            this.colAmountIncludedVAT,
            this.colAmount,
            this.colExpiryDate,
            this.colSerialNumber,
            this.colNote,
            this.colQuantityReceived,
            this.colPriceIncludedVATReceived,
            this.colPriceExcludedVATReceived,
            this.colPriceReceived,
            this.colDiscountReceived,
            this.colDiscountAmountReceived,
            this.colVATReceived,
            this.colVATAmountReceived,
            this.colAmountExcludedVATReceived,
            this.colAmountIncludedVATReceived,
            this.colAmountReceived,
            this.colQuantityOrdered,
            this.colOrderQuantityReceived,
            this.colReceiptQuantityIssued,
            this.colReceiptQuantity,
            this.colReceiptAmount,
            this.colAverageCost,
            this.colStatus,
            this.colNo});
            this.gridViewWRDetail.DetailHeight = 284;
            this.gridViewWRDetail.GridControl = this.gridControlWRDetail;
            this.gridViewWRDetail.Name = "gridViewWRDetail";
            this.gridViewWRDetail.OptionsView.ColumnHeaderAutoHeight = DevExpress.Utils.DefaultBoolean.True;
            this.gridViewWRDetail.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colQuantityReceived, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colWRNumber
            // 
            this.colWRNumber.FieldName = "WRNumber";
            this.colWRNumber.Name = "colWRNumber";
            // 
            // colOrdinal
            // 
            this.colOrdinal.FieldName = "Ordinal";
            this.colOrdinal.Name = "colOrdinal";
            // 
            // colGoodsID
            // 
            this.colGoodsID.Caption = "Part Number";
            this.colGoodsID.FieldName = "GoodsID";
            this.colGoodsID.MinWidth = 150;
            this.colGoodsID.Name = "colGoodsID";
            this.colGoodsID.Visible = true;
            this.colGoodsID.VisibleIndex = 1;
            this.colGoodsID.Width = 207;
            // 
            // colGoodsName
            // 
            this.colGoodsName.Caption = "Part Name";
            this.colGoodsName.FieldName = "GoodsName";
            this.colGoodsName.MinWidth = 200;
            this.colGoodsName.Name = "colGoodsName";
            this.colGoodsName.Visible = true;
            this.colGoodsName.VisibleIndex = 2;
            this.colGoodsName.Width = 200;
            // 
            // colReceiptUnitID
            // 
            this.colReceiptUnitID.Caption = "Đơn vị nhập";
            this.colReceiptUnitID.FieldName = "ReceiptUnitID";
            this.colReceiptUnitID.MinWidth = 50;
            this.colReceiptUnitID.Name = "colReceiptUnitID";
            this.colReceiptUnitID.Visible = true;
            this.colReceiptUnitID.VisibleIndex = 3;
            this.colReceiptUnitID.Width = 50;
            // 
            // colUnitRate
            // 
            this.colUnitRate.Caption = "Tỉ lệ đơn vị";
            this.colUnitRate.FieldName = "UnitRate";
            this.colUnitRate.MinWidth = 70;
            this.colUnitRate.Name = "colUnitRate";
            this.colUnitRate.Visible = true;
            this.colUnitRate.VisibleIndex = 4;
            this.colUnitRate.Width = 70;
            // 
            // colStockUnitID
            // 
            this.colStockUnitID.Caption = "Đơn vị kho";
            this.colStockUnitID.FieldName = "StockUnitID";
            this.colStockUnitID.MinWidth = 50;
            this.colStockUnitID.Name = "colStockUnitID";
            this.colStockUnitID.Visible = true;
            this.colStockUnitID.VisibleIndex = 5;
            this.colStockUnitID.Width = 50;
            // 
            // colQuantity
            // 
            this.colQuantity.Caption = "Lượng nhập";
            this.colQuantity.FieldName = "Quantity";
            this.colQuantity.MinWidth = 50;
            this.colQuantity.Name = "colQuantity";
            // 
            // colPriceIncludedVAT
            // 
            this.colPriceIncludedVAT.FieldName = "PriceIncludedVAT";
            this.colPriceIncludedVAT.Name = "colPriceIncludedVAT";
            // 
            // colPriceExcludedVAT
            // 
            this.colPriceExcludedVAT.FieldName = "PriceExcludedVAT";
            this.colPriceExcludedVAT.Name = "colPriceExcludedVAT";
            // 
            // colPrice
            // 
            this.colPrice.Caption = "Đơn giá";
            this.colPrice.FieldName = "Price";
            this.colPrice.MinWidth = 50;
            this.colPrice.Name = "colPrice";
            this.colPrice.Visible = true;
            this.colPrice.VisibleIndex = 6;
            this.colPrice.Width = 50;
            // 
            // colDiscount
            // 
            this.colDiscount.Caption = "%Chiết khấu";
            this.colDiscount.FieldName = "Discount";
            this.colDiscount.MinWidth = 60;
            this.colDiscount.Name = "colDiscount";
            this.colDiscount.Visible = true;
            this.colDiscount.VisibleIndex = 8;
            this.colDiscount.Width = 60;
            // 
            // colDiscountAmount
            // 
            this.colDiscountAmount.Caption = "Tiền chiết khấu";
            this.colDiscountAmount.FieldName = "DiscountAmount";
            this.colDiscountAmount.MinWidth = 70;
            this.colDiscountAmount.Name = "colDiscountAmount";
            this.colDiscountAmount.Visible = true;
            this.colDiscountAmount.VisibleIndex = 9;
            this.colDiscountAmount.Width = 70;
            // 
            // colVAT
            // 
            this.colVAT.Caption = "%Thuế GTGT";
            this.colVAT.FieldName = "VAT";
            this.colVAT.MinWidth = 60;
            this.colVAT.Name = "colVAT";
            this.colVAT.Visible = true;
            this.colVAT.VisibleIndex = 10;
            this.colVAT.Width = 60;
            // 
            // colVATAmount
            // 
            this.colVATAmount.Caption = "Tiền thuế GTGT";
            this.colVATAmount.FieldName = "VATAmount";
            this.colVATAmount.MinWidth = 70;
            this.colVATAmount.Name = "colVATAmount";
            this.colVATAmount.Visible = true;
            this.colVATAmount.VisibleIndex = 11;
            this.colVATAmount.Width = 70;
            // 
            // colAmountExcludedVAT
            // 
            this.colAmountExcludedVAT.FieldName = "AmountExcludedVAT";
            this.colAmountExcludedVAT.Name = "colAmountExcludedVAT";
            // 
            // colAmountIncludedVAT
            // 
            this.colAmountIncludedVAT.FieldName = "AmountIncludedVAT";
            this.colAmountIncludedVAT.Name = "colAmountIncludedVAT";
            // 
            // colAmount
            // 
            this.colAmount.Caption = "Thành tiền";
            this.colAmount.FieldName = "Amount";
            this.colAmount.MinWidth = 50;
            this.colAmount.Name = "colAmount";
            this.colAmount.Visible = true;
            this.colAmount.VisibleIndex = 12;
            this.colAmount.Width = 50;
            // 
            // colExpiryDate
            // 
            this.colExpiryDate.Caption = "Hạn sử dụng";
            this.colExpiryDate.FieldName = "ExpiryDate";
            this.colExpiryDate.MinWidth = 70;
            this.colExpiryDate.Name = "colExpiryDate";
            this.colExpiryDate.Visible = true;
            this.colExpiryDate.VisibleIndex = 13;
            this.colExpiryDate.Width = 70;
            // 
            // colSerialNumber
            // 
            this.colSerialNumber.Caption = "Số Serial";
            this.colSerialNumber.FieldName = "SerialNumber";
            this.colSerialNumber.MinWidth = 50;
            this.colSerialNumber.Name = "colSerialNumber";
            this.colSerialNumber.Visible = true;
            this.colSerialNumber.VisibleIndex = 14;
            this.colSerialNumber.Width = 50;
            // 
            // colNote
            // 
            this.colNote.Caption = "Ghi chú";
            this.colNote.FieldName = "Note";
            this.colNote.MinWidth = 50;
            this.colNote.Name = "colNote";
            this.colNote.Visible = true;
            this.colNote.VisibleIndex = 15;
            this.colNote.Width = 50;
            // 
            // colQuantityReceived
            // 
            this.colQuantityReceived.Caption = "Lượng đã nhập";
            this.colQuantityReceived.FieldName = "QuantityReceived";
            this.colQuantityReceived.MinWidth = 120;
            this.colQuantityReceived.Name = "colQuantityReceived";
            this.colQuantityReceived.Visible = true;
            this.colQuantityReceived.VisibleIndex = 17;
            this.colQuantityReceived.Width = 120;
            // 
            // colPriceIncludedVATReceived
            // 
            this.colPriceIncludedVATReceived.FieldName = "PriceIncludedVATReceived";
            this.colPriceIncludedVATReceived.Name = "colPriceIncludedVATReceived";
            // 
            // colPriceExcludedVATReceived
            // 
            this.colPriceExcludedVATReceived.FieldName = "PriceExcludedVATReceived";
            this.colPriceExcludedVATReceived.Name = "colPriceExcludedVATReceived";
            // 
            // colPriceReceived
            // 
            this.colPriceReceived.FieldName = "PriceReceived";
            this.colPriceReceived.Name = "colPriceReceived";
            // 
            // colDiscountReceived
            // 
            this.colDiscountReceived.FieldName = "DiscountReceived";
            this.colDiscountReceived.Name = "colDiscountReceived";
            // 
            // colDiscountAmountReceived
            // 
            this.colDiscountAmountReceived.FieldName = "DiscountAmountReceived";
            this.colDiscountAmountReceived.Name = "colDiscountAmountReceived";
            // 
            // colVATReceived
            // 
            this.colVATReceived.FieldName = "VATReceived";
            this.colVATReceived.Name = "colVATReceived";
            // 
            // colVATAmountReceived
            // 
            this.colVATAmountReceived.FieldName = "VATAmountReceived";
            this.colVATAmountReceived.Name = "colVATAmountReceived";
            // 
            // colAmountExcludedVATReceived
            // 
            this.colAmountExcludedVATReceived.FieldName = "AmountExcludedVATReceived";
            this.colAmountExcludedVATReceived.Name = "colAmountExcludedVATReceived";
            // 
            // colAmountIncludedVATReceived
            // 
            this.colAmountIncludedVATReceived.FieldName = "AmountIncludedVATReceived";
            this.colAmountIncludedVATReceived.Name = "colAmountIncludedVATReceived";
            // 
            // colAmountReceived
            // 
            this.colAmountReceived.FieldName = "AmountReceived";
            this.colAmountReceived.Name = "colAmountReceived";
            // 
            // colQuantityOrdered
            // 
            this.colQuantityOrdered.Caption = "Lượng yêu cầu";
            this.colQuantityOrdered.FieldName = "QuantityOrdered";
            this.colQuantityOrdered.MinWidth = 120;
            this.colQuantityOrdered.Name = "colQuantityOrdered";
            this.colQuantityOrdered.Visible = true;
            this.colQuantityOrdered.VisibleIndex = 16;
            this.colQuantityOrdered.Width = 120;
            // 
            // colOrderQuantityReceived
            // 
            this.colOrderQuantityReceived.FieldName = "OrderQuantityReceived";
            this.colOrderQuantityReceived.Name = "colOrderQuantityReceived";
            // 
            // colReceiptQuantityIssued
            // 
            this.colReceiptQuantityIssued.Caption = "ReceiptQuantityIssued";
            this.colReceiptQuantityIssued.FieldName = "ReceiptQuantityIssued";
            this.colReceiptQuantityIssued.MinWidth = 70;
            this.colReceiptQuantityIssued.Name = "colReceiptQuantityIssued";
            // 
            // colReceiptQuantity
            // 
            this.colReceiptQuantity.Caption = "Lượng nhập";
            this.colReceiptQuantity.FieldName = "ReceiptQuantity";
            this.colReceiptQuantity.MinWidth = 120;
            this.colReceiptQuantity.Name = "colReceiptQuantity";
            this.colReceiptQuantity.Visible = true;
            this.colReceiptQuantity.VisibleIndex = 7;
            this.colReceiptQuantity.Width = 120;
            // 
            // colReceiptAmount
            // 
            this.colReceiptAmount.FieldName = "ReceiptAmount";
            this.colReceiptAmount.Name = "colReceiptAmount";
            // 
            // colAverageCost
            // 
            this.colAverageCost.FieldName = "AverageCost";
            this.colAverageCost.Name = "colAverageCost";
            // 
            // colStatus
            // 
            this.colStatus.FieldName = "Status";
            this.colStatus.Name = "colStatus";
            // 
            // colNo
            // 
            this.colNo.Caption = "Line No";
            this.colNo.FieldName = "No";
            this.colNo.MinWidth = 43;
            this.colNo.Name = "colNo";
            this.colNo.Visible = true;
            this.colNo.VisibleIndex = 0;
            this.colNo.Width = 43;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2});
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(1298, 466);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.gridControlWRDetail;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(1278, 446);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // wRDetailBindingSource
            // 
            this.wRDetailBindingSource.DataMember = "WRDetail";
            this.wRDetailBindingSource.DataSource = this.wARHOUSE_HPDataSet1;
            // 
            // wRDetailTableAdapter
            // 
            this.wRDetailTableAdapter.ClearBeforeFill = true;
            // 
            // dateEditContractDate
            // 
            this.dateEditContractDate.EditValue = null;
            this.dateEditContractDate.Location = new System.Drawing.Point(774, 37);
            this.dateEditContractDate.Name = "dateEditContractDate";
            this.dateEditContractDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditContractDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditContractDate.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditContractDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditContractDate.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEditContractDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditContractDate.Size = new System.Drawing.Size(99, 20);
            this.dateEditContractDate.StyleController = this.layoutControl2;
            this.dateEditContractDate.TabIndex = 32;
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.label1);
            this.layoutControl2.Controls.Add(this.textEdit4);
            this.layoutControl2.Controls.Add(this.textEditWRNumber);
            this.layoutControl2.Controls.Add(this.comboBoxEditModality);
            this.layoutControl2.Controls.Add(this.dateEditWRDate);
            this.layoutControl2.Controls.Add(this.textEditNote);
            this.layoutControl2.Controls.Add(this.comboBoxEditWarehouse);
            this.layoutControl2.Controls.Add(this.textEditOrderNumber);
            this.layoutControl2.Controls.Add(this.dateEditOrderDate);
            this.layoutControl2.Controls.Add(this.textEditSupplier);
            this.layoutControl2.Controls.Add(this.textEditContractNumber);
            this.layoutControl2.Controls.Add(this.comboBoxEditPriceType);
            this.layoutControl2.Controls.Add(this.comboBoxEditPaymentType);
            this.layoutControl2.Controls.Add(this.comboBoxEditHandlingStatus);
            this.layoutControl2.Controls.Add(this.textEditTotalQuantity);
            this.layoutControl2.Controls.Add(this.textEditTotalDiscountAmount);
            this.layoutControl2.Controls.Add(this.textEditTotalVATAmount);
            this.layoutControl2.Controls.Add(this.textEditTotalAmount);
            this.layoutControl2.Controls.Add(this.dateEditContractDate);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl2.Location = new System.Drawing.Point(0, 37);
            this.layoutControl2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(330, 209, 812, 500);
            this.layoutControl2.Root = this.layoutControlGroup1;
            this.layoutControl2.Size = new System.Drawing.Size(1298, 123);
            this.layoutControl2.TabIndex = 1;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // label1
            // 
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(218, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 21);
            this.label1.TabIndex = 7;
            this.label1.Text = "(*)";
            // 
            // textEdit4
            // 
            this.textEdit4.Location = new System.Drawing.Point(2540, 10);
            this.textEdit4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textEdit4.Name = "textEdit4";
            this.textEdit4.Size = new System.Drawing.Size(53, 20);
            this.textEdit4.StyleController = this.layoutControl2;
            this.textEdit4.TabIndex = 11;
            // 
            // textEditWRNumber
            // 
            this.textEditWRNumber.Location = new System.Drawing.Point(115, 12);
            this.textEditWRNumber.Name = "textEditWRNumber";
            this.textEditWRNumber.Size = new System.Drawing.Size(99, 20);
            this.textEditWRNumber.StyleController = this.layoutControl2;
            this.textEditWRNumber.TabIndex = 12;
            // 
            // comboBoxEditModality
            // 
            this.comboBoxEditModality.Location = new System.Drawing.Point(115, 37);
            this.comboBoxEditModality.Name = "comboBoxEditModality";
            this.comboBoxEditModality.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditModality.Size = new System.Drawing.Size(99, 20);
            this.comboBoxEditModality.StyleController = this.layoutControl2;
            this.comboBoxEditModality.TabIndex = 13;
            // 
            // dateEditWRDate
            // 
            this.dateEditWRDate.EditValue = null;
            this.dateEditWRDate.Location = new System.Drawing.Point(115, 63);
            this.dateEditWRDate.Name = "dateEditWRDate";
            this.dateEditWRDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditWRDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditWRDate.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditWRDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditWRDate.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEditWRDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditWRDate.Size = new System.Drawing.Size(99, 20);
            this.dateEditWRDate.StyleController = this.layoutControl2;
            this.dateEditWRDate.TabIndex = 14;
            // 
            // textEditNote
            // 
            this.textEditNote.Location = new System.Drawing.Point(115, 89);
            this.textEditNote.Name = "textEditNote";
            this.textEditNote.Size = new System.Drawing.Size(964, 20);
            this.textEditNote.StyleController = this.layoutControl2;
            this.textEditNote.TabIndex = 16;
            // 
            // comboBoxEditWarehouse
            // 
            this.comboBoxEditWarehouse.Location = new System.Drawing.Point(362, 12);
            this.comboBoxEditWarehouse.Name = "comboBoxEditWarehouse";
            this.comboBoxEditWarehouse.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditWarehouse.Size = new System.Drawing.Size(305, 20);
            this.comboBoxEditWarehouse.StyleController = this.layoutControl2;
            this.comboBoxEditWarehouse.TabIndex = 17;
            // 
            // textEditOrderNumber
            // 
            this.textEditOrderNumber.Location = new System.Drawing.Point(362, 37);
            this.textEditOrderNumber.Name = "textEditOrderNumber";
            this.textEditOrderNumber.Size = new System.Drawing.Size(99, 20);
            this.textEditOrderNumber.StyleController = this.layoutControl2;
            this.textEditOrderNumber.TabIndex = 18;
            // 
            // dateEditOrderDate
            // 
            this.dateEditOrderDate.EditValue = null;
            this.dateEditOrderDate.Location = new System.Drawing.Point(362, 63);
            this.dateEditOrderDate.Name = "dateEditOrderDate";
            this.dateEditOrderDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditOrderDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditOrderDate.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditOrderDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditOrderDate.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEditOrderDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditOrderDate.Size = new System.Drawing.Size(99, 20);
            this.dateEditOrderDate.StyleController = this.layoutControl2;
            this.dateEditOrderDate.TabIndex = 19;
            // 
            // textEditSupplier
            // 
            this.textEditSupplier.Location = new System.Drawing.Point(568, 37);
            this.textEditSupplier.Name = "textEditSupplier";
            this.textEditSupplier.Size = new System.Drawing.Size(99, 20);
            this.textEditSupplier.StyleController = this.layoutControl2;
            this.textEditSupplier.TabIndex = 20;
            // 
            // textEditContractNumber
            // 
            this.textEditContractNumber.Location = new System.Drawing.Point(774, 12);
            this.textEditContractNumber.Name = "textEditContractNumber";
            this.textEditContractNumber.Size = new System.Drawing.Size(99, 20);
            this.textEditContractNumber.StyleController = this.layoutControl2;
            this.textEditContractNumber.TabIndex = 21;
            // 
            // comboBoxEditPriceType
            // 
            this.comboBoxEditPriceType.Location = new System.Drawing.Point(980, 12);
            this.comboBoxEditPriceType.Name = "comboBoxEditPriceType";
            this.comboBoxEditPriceType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditPriceType.Size = new System.Drawing.Size(99, 20);
            this.comboBoxEditPriceType.StyleController = this.layoutControl2;
            this.comboBoxEditPriceType.TabIndex = 23;
            // 
            // comboBoxEditPaymentType
            // 
            this.comboBoxEditPaymentType.Location = new System.Drawing.Point(980, 37);
            this.comboBoxEditPaymentType.Name = "comboBoxEditPaymentType";
            this.comboBoxEditPaymentType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditPaymentType.Size = new System.Drawing.Size(99, 20);
            this.comboBoxEditPaymentType.StyleController = this.layoutControl2;
            this.comboBoxEditPaymentType.TabIndex = 24;
            // 
            // comboBoxEditHandlingStatus
            // 
            this.comboBoxEditHandlingStatus.Location = new System.Drawing.Point(980, 63);
            this.comboBoxEditHandlingStatus.Name = "comboBoxEditHandlingStatus";
            this.comboBoxEditHandlingStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditHandlingStatus.Size = new System.Drawing.Size(99, 20);
            this.comboBoxEditHandlingStatus.StyleController = this.layoutControl2;
            this.comboBoxEditHandlingStatus.TabIndex = 25;
            // 
            // textEditTotalQuantity
            // 
            this.textEditTotalQuantity.Location = new System.Drawing.Point(1186, 12);
            this.textEditTotalQuantity.Name = "textEditTotalQuantity";
            this.textEditTotalQuantity.Size = new System.Drawing.Size(100, 20);
            this.textEditTotalQuantity.StyleController = this.layoutControl2;
            this.textEditTotalQuantity.TabIndex = 27;
            // 
            // textEditTotalDiscountAmount
            // 
            this.textEditTotalDiscountAmount.Location = new System.Drawing.Point(1186, 37);
            this.textEditTotalDiscountAmount.Name = "textEditTotalDiscountAmount";
            this.textEditTotalDiscountAmount.Size = new System.Drawing.Size(100, 20);
            this.textEditTotalDiscountAmount.StyleController = this.layoutControl2;
            this.textEditTotalDiscountAmount.TabIndex = 28;
            // 
            // textEditTotalVATAmount
            // 
            this.textEditTotalVATAmount.Location = new System.Drawing.Point(1186, 63);
            this.textEditTotalVATAmount.Name = "textEditTotalVATAmount";
            this.textEditTotalVATAmount.Size = new System.Drawing.Size(100, 20);
            this.textEditTotalVATAmount.StyleController = this.layoutControl2;
            this.textEditTotalVATAmount.TabIndex = 29;
            // 
            // textEditTotalAmount
            // 
            this.textEditTotalAmount.Location = new System.Drawing.Point(1186, 89);
            this.textEditTotalAmount.Name = "textEditTotalAmount";
            this.textEditTotalAmount.Size = new System.Drawing.Size(100, 20);
            this.textEditTotalAmount.StyleController = this.layoutControl2;
            this.textEditTotalAmount.TabIndex = 30;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3,
            this.layoutControlItemWRNumber,
            this.layoutControlItemModality,
            this.layoutControlItemWRDate,
            this.layoutControlItemWarehouse,
            this.layoutControlItemOrderNumber,
            this.layoutControlItemOrderDate,
            this.layoutControlItemTotalQuantity,
            this.layoutControlItemTotalDiscountAmount,
            this.layoutControlItemTotalVATAmount,
            this.layoutControlItemTotalAmount,
            this.layoutControlItemContractNumber,
            this.layoutControlItemContractDate,
            this.layoutControlItemPriceType,
            this.layoutControlItemPaymentType,
            this.layoutControlItemHandlingStatus,
            this.layoutControlItemSupplier,
            this.layoutControlItemNote});
            this.layoutControlGroup1.LayoutMode = DevExpress.XtraLayout.Utils.LayoutMode.Table;
            this.layoutControlGroup1.Name = "Root";
            columnDefinition1.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition1.Width = 16.129032258064516D;
            columnDefinition2.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition2.Width = 3.225806451612903D;
            columnDefinition3.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition3.Width = 16.129032258064516D;
            columnDefinition4.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition4.Width = 16.129032258064516D;
            columnDefinition5.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition5.Width = 16.129032258064516D;
            columnDefinition6.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition6.Width = 16.129032258064516D;
            columnDefinition7.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition7.Width = 16.129032258064516D;
            this.layoutControlGroup1.OptionsTableLayoutGroup.ColumnDefinitions.AddRange(new DevExpress.XtraLayout.ColumnDefinition[] {
            columnDefinition1,
            columnDefinition2,
            columnDefinition3,
            columnDefinition4,
            columnDefinition5,
            columnDefinition6,
            columnDefinition7});
            rowDefinition1.Height = 25D;
            rowDefinition1.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition2.Height = 25D;
            rowDefinition2.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition3.Height = 25D;
            rowDefinition3.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition4.Height = 25D;
            rowDefinition4.SizeType = System.Windows.Forms.SizeType.Percent;
            this.layoutControlGroup1.OptionsTableLayoutGroup.RowDefinitions.AddRange(new DevExpress.XtraLayout.RowDefinition[] {
            rowDefinition1,
            rowDefinition2,
            rowDefinition3,
            rowDefinition4});
            this.layoutControlGroup1.Size = new System.Drawing.Size(1298, 123);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.label1;
            this.layoutControlItem3.Location = new System.Drawing.Point(206, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.OptionsTableLayoutItem.ColumnIndex = 1;
            this.layoutControlItem3.Size = new System.Drawing.Size(41, 25);
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItemWRNumber
            // 
            this.layoutControlItemWRNumber.Control = this.textEditWRNumber;
            this.layoutControlItemWRNumber.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemWRNumber.Name = "layoutControlItemWRNumber";
            this.layoutControlItemWRNumber.Size = new System.Drawing.Size(206, 25);
            this.layoutControlItemWRNumber.Text = "Conveyance Number";
            this.layoutControlItemWRNumber.TextSize = new System.Drawing.Size(100, 13);
            // 
            // layoutControlItemModality
            // 
            this.layoutControlItemModality.Control = this.comboBoxEditModality;
            this.layoutControlItemModality.Location = new System.Drawing.Point(0, 25);
            this.layoutControlItemModality.Name = "layoutControlItemModality";
            this.layoutControlItemModality.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItemModality.Size = new System.Drawing.Size(206, 26);
            this.layoutControlItemModality.Text = "Phương thức";
            this.layoutControlItemModality.TextSize = new System.Drawing.Size(100, 13);
            // 
            // layoutControlItemWRDate
            // 
            this.layoutControlItemWRDate.Control = this.dateEditWRDate;
            this.layoutControlItemWRDate.Location = new System.Drawing.Point(0, 51);
            this.layoutControlItemWRDate.Name = "layoutControlItemWRDate";
            this.layoutControlItemWRDate.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlItemWRDate.Size = new System.Drawing.Size(206, 26);
            this.layoutControlItemWRDate.Text = "Ngày phiếu nhập";
            this.layoutControlItemWRDate.TextSize = new System.Drawing.Size(100, 13);
            // 
            // layoutControlItemWarehouse
            // 
            this.layoutControlItemWarehouse.Control = this.comboBoxEditWarehouse;
            this.layoutControlItemWarehouse.Location = new System.Drawing.Point(247, 0);
            this.layoutControlItemWarehouse.Name = "layoutControlItemWarehouse";
            this.layoutControlItemWarehouse.OptionsTableLayoutItem.ColumnIndex = 2;
            this.layoutControlItemWarehouse.OptionsTableLayoutItem.ColumnSpan = 2;
            this.layoutControlItemWarehouse.Size = new System.Drawing.Size(412, 25);
            this.layoutControlItemWarehouse.Text = "Kho nhập hàng";
            this.layoutControlItemWarehouse.TextSize = new System.Drawing.Size(100, 13);
            // 
            // layoutControlItemOrderNumber
            // 
            this.layoutControlItemOrderNumber.Control = this.textEditOrderNumber;
            this.layoutControlItemOrderNumber.Location = new System.Drawing.Point(247, 25);
            this.layoutControlItemOrderNumber.Name = "layoutControlItemOrderNumber";
            this.layoutControlItemOrderNumber.OptionsTableLayoutItem.ColumnIndex = 2;
            this.layoutControlItemOrderNumber.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItemOrderNumber.Size = new System.Drawing.Size(206, 26);
            this.layoutControlItemOrderNumber.Text = "Số đơn hàng";
            this.layoutControlItemOrderNumber.TextSize = new System.Drawing.Size(100, 13);
            // 
            // layoutControlItemOrderDate
            // 
            this.layoutControlItemOrderDate.Control = this.dateEditOrderDate;
            this.layoutControlItemOrderDate.Location = new System.Drawing.Point(247, 51);
            this.layoutControlItemOrderDate.Name = "layoutControlItemOrderDate";
            this.layoutControlItemOrderDate.OptionsTableLayoutItem.ColumnIndex = 2;
            this.layoutControlItemOrderDate.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlItemOrderDate.Size = new System.Drawing.Size(206, 26);
            this.layoutControlItemOrderDate.Text = "Ngày đơn hàng";
            this.layoutControlItemOrderDate.TextSize = new System.Drawing.Size(100, 13);
            // 
            // layoutControlItemTotalQuantity
            // 
            this.layoutControlItemTotalQuantity.Control = this.textEditTotalQuantity;
            this.layoutControlItemTotalQuantity.Location = new System.Drawing.Point(1071, 0);
            this.layoutControlItemTotalQuantity.Name = "layoutControlItemTotalQuantity";
            this.layoutControlItemTotalQuantity.OptionsTableLayoutItem.ColumnIndex = 6;
            this.layoutControlItemTotalQuantity.Size = new System.Drawing.Size(207, 25);
            this.layoutControlItemTotalQuantity.Text = "Tổng lượng";
            this.layoutControlItemTotalQuantity.TextSize = new System.Drawing.Size(100, 13);
            // 
            // layoutControlItemTotalDiscountAmount
            // 
            this.layoutControlItemTotalDiscountAmount.Control = this.textEditTotalDiscountAmount;
            this.layoutControlItemTotalDiscountAmount.Location = new System.Drawing.Point(1071, 25);
            this.layoutControlItemTotalDiscountAmount.Name = "layoutControlItemTotalDiscountAmount";
            this.layoutControlItemTotalDiscountAmount.OptionsTableLayoutItem.ColumnIndex = 6;
            this.layoutControlItemTotalDiscountAmount.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItemTotalDiscountAmount.Size = new System.Drawing.Size(207, 26);
            this.layoutControlItemTotalDiscountAmount.Text = "Tổng chiết khấu";
            this.layoutControlItemTotalDiscountAmount.TextSize = new System.Drawing.Size(100, 13);
            // 
            // layoutControlItemTotalVATAmount
            // 
            this.layoutControlItemTotalVATAmount.Control = this.textEditTotalVATAmount;
            this.layoutControlItemTotalVATAmount.Location = new System.Drawing.Point(1071, 51);
            this.layoutControlItemTotalVATAmount.Name = "layoutControlItemTotalVATAmount";
            this.layoutControlItemTotalVATAmount.OptionsTableLayoutItem.ColumnIndex = 6;
            this.layoutControlItemTotalVATAmount.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlItemTotalVATAmount.Size = new System.Drawing.Size(207, 26);
            this.layoutControlItemTotalVATAmount.Text = "Tổng thuế";
            this.layoutControlItemTotalVATAmount.TextSize = new System.Drawing.Size(100, 13);
            // 
            // layoutControlItemTotalAmount
            // 
            this.layoutControlItemTotalAmount.Control = this.textEditTotalAmount;
            this.layoutControlItemTotalAmount.Location = new System.Drawing.Point(1071, 77);
            this.layoutControlItemTotalAmount.Name = "layoutControlItemTotalAmount";
            this.layoutControlItemTotalAmount.OptionsTableLayoutItem.ColumnIndex = 6;
            this.layoutControlItemTotalAmount.OptionsTableLayoutItem.RowIndex = 3;
            this.layoutControlItemTotalAmount.Size = new System.Drawing.Size(207, 26);
            this.layoutControlItemTotalAmount.Text = "Tổng tiền";
            this.layoutControlItemTotalAmount.TextSize = new System.Drawing.Size(100, 13);
            // 
            // layoutControlItemContractNumber
            // 
            this.layoutControlItemContractNumber.Control = this.textEditContractNumber;
            this.layoutControlItemContractNumber.Location = new System.Drawing.Point(659, 0);
            this.layoutControlItemContractNumber.Name = "layoutControlItemContractNumber";
            this.layoutControlItemContractNumber.OptionsTableLayoutItem.ColumnIndex = 4;
            this.layoutControlItemContractNumber.Size = new System.Drawing.Size(206, 25);
            this.layoutControlItemContractNumber.Text = "Số hợp đồng";
            this.layoutControlItemContractNumber.TextSize = new System.Drawing.Size(100, 13);
            // 
            // layoutControlItemContractDate
            // 
            this.layoutControlItemContractDate.Control = this.dateEditContractDate;
            this.layoutControlItemContractDate.Location = new System.Drawing.Point(659, 25);
            this.layoutControlItemContractDate.Name = "layoutControlItemContractDate";
            this.layoutControlItemContractDate.OptionsTableLayoutItem.ColumnIndex = 4;
            this.layoutControlItemContractDate.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItemContractDate.Size = new System.Drawing.Size(206, 26);
            this.layoutControlItemContractDate.Text = "Ngày hợp đồng";
            this.layoutControlItemContractDate.TextSize = new System.Drawing.Size(100, 13);
            // 
            // layoutControlItemPriceType
            // 
            this.layoutControlItemPriceType.Control = this.comboBoxEditPriceType;
            this.layoutControlItemPriceType.Location = new System.Drawing.Point(865, 0);
            this.layoutControlItemPriceType.Name = "layoutControlItemPriceType";
            this.layoutControlItemPriceType.OptionsTableLayoutItem.ColumnIndex = 5;
            this.layoutControlItemPriceType.Size = new System.Drawing.Size(206, 25);
            this.layoutControlItemPriceType.Text = "Loại giá";
            this.layoutControlItemPriceType.TextSize = new System.Drawing.Size(100, 13);
            // 
            // layoutControlItemPaymentType
            // 
            this.layoutControlItemPaymentType.Control = this.comboBoxEditPaymentType;
            this.layoutControlItemPaymentType.Location = new System.Drawing.Point(865, 25);
            this.layoutControlItemPaymentType.Name = "layoutControlItemPaymentType";
            this.layoutControlItemPaymentType.OptionsTableLayoutItem.ColumnIndex = 5;
            this.layoutControlItemPaymentType.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItemPaymentType.Size = new System.Drawing.Size(206, 26);
            this.layoutControlItemPaymentType.Text = "Loại thanh toán";
            this.layoutControlItemPaymentType.TextSize = new System.Drawing.Size(100, 13);
            // 
            // layoutControlItemHandlingStatus
            // 
            this.layoutControlItemHandlingStatus.Control = this.comboBoxEditHandlingStatus;
            this.layoutControlItemHandlingStatus.Location = new System.Drawing.Point(865, 51);
            this.layoutControlItemHandlingStatus.Name = "layoutControlItemHandlingStatus";
            this.layoutControlItemHandlingStatus.OptionsTableLayoutItem.ColumnIndex = 5;
            this.layoutControlItemHandlingStatus.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlItemHandlingStatus.Size = new System.Drawing.Size(206, 26);
            this.layoutControlItemHandlingStatus.Text = "Trạng thái xử lý";
            this.layoutControlItemHandlingStatus.TextSize = new System.Drawing.Size(100, 13);
            // 
            // layoutControlItemSupplier
            // 
            this.layoutControlItemSupplier.Control = this.textEditSupplier;
            this.layoutControlItemSupplier.Location = new System.Drawing.Point(453, 25);
            this.layoutControlItemSupplier.Name = "layoutControlItemSupplier";
            this.layoutControlItemSupplier.OptionsTableLayoutItem.ColumnIndex = 3;
            this.layoutControlItemSupplier.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItemSupplier.Size = new System.Drawing.Size(206, 26);
            this.layoutControlItemSupplier.Text = "Nhà cung cấp";
            this.layoutControlItemSupplier.TextSize = new System.Drawing.Size(100, 13);
            // 
            // layoutControlItemNote
            // 
            this.layoutControlItemNote.Control = this.textEditNote;
            this.layoutControlItemNote.Location = new System.Drawing.Point(0, 77);
            this.layoutControlItemNote.Name = "layoutControlItemNote";
            this.layoutControlItemNote.OptionsTableLayoutItem.ColumnSpan = 6;
            this.layoutControlItemNote.OptionsTableLayoutItem.RowIndex = 3;
            this.layoutControlItemNote.Size = new System.Drawing.Size(1071, 26);
            this.layoutControlItemNote.Text = "Ghi chú";
            this.layoutControlItemNote.TextSize = new System.Drawing.Size(100, 13);
            // 
            // WarehouseReceiptControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl3);
            this.Controls.Add(this.layoutControl2);
            this.Controls.Add(this.layoutControl1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "WarehouseReceiptControl";
            this.Size = new System.Drawing.Size(1298, 626);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wRDataDetailBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wARHOUSE_HPDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).EndInit();
            this.layoutControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlWRDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wRDetailBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewWRDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wRDetailBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditContractDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditContractDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditWRNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditModality.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditWRDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditWRDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditNote.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditWarehouse.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditOrderNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditOrderDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditOrderDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditSupplier.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditContractNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditPriceType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditPaymentType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditHandlingStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalQuantity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalDiscountAmount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalVATAmount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalAmount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWRNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemModality)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWRDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWarehouse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemOrderNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemOrderDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalQuantity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalDiscountAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalVATAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemContractNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemContractDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPriceType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPaymentType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemHandlingStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemSupplier)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemNote)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel windowsUIButtonPanel1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private System.Windows.Forms.BindingSource wRDataDetailBindingSource;
        private WARHOUSE_HPDataSet1 wARHOUSE_HPDataSet1;
        private WARHOUSE_HPDataSet1TableAdapters.WRDataDetailTableAdapter wRDataDetailTableAdapter;
        private DevExpress.XtraLayout.LayoutControl layoutControl3;
        private DevExpress.XtraGrid.GridControl gridControlWRDetail;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewWRDetail;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private System.Windows.Forms.BindingSource wRDetailBindingSource1;
        private DevExpress.XtraGrid.Columns.GridColumn colWRNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colOrdinal;
        private DevExpress.XtraGrid.Columns.GridColumn colGoodsID;
        private DevExpress.XtraGrid.Columns.GridColumn colGoodsName;
        private DevExpress.XtraGrid.Columns.GridColumn colReceiptUnitID;
        private DevExpress.XtraGrid.Columns.GridColumn colUnitRate;
        private DevExpress.XtraGrid.Columns.GridColumn colStockUnitID;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantity;
        private DevExpress.XtraGrid.Columns.GridColumn colPriceIncludedVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colPriceExcludedVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colPrice;
        private DevExpress.XtraGrid.Columns.GridColumn colDiscount;
        private DevExpress.XtraGrid.Columns.GridColumn colDiscountAmount;
        private DevExpress.XtraGrid.Columns.GridColumn colVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colVATAmount;
        private DevExpress.XtraGrid.Columns.GridColumn colAmountExcludedVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colAmountIncludedVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colAmount;
        private DevExpress.XtraGrid.Columns.GridColumn colExpiryDate;
        private DevExpress.XtraGrid.Columns.GridColumn colSerialNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colNote;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantityReceived;
        private DevExpress.XtraGrid.Columns.GridColumn colPriceIncludedVATReceived;
        private DevExpress.XtraGrid.Columns.GridColumn colPriceExcludedVATReceived;
        private DevExpress.XtraGrid.Columns.GridColumn colPriceReceived;
        private DevExpress.XtraGrid.Columns.GridColumn colDiscountReceived;
        private DevExpress.XtraGrid.Columns.GridColumn colDiscountAmountReceived;
        private DevExpress.XtraGrid.Columns.GridColumn colVATReceived;
        private DevExpress.XtraGrid.Columns.GridColumn colVATAmountReceived;
        private DevExpress.XtraGrid.Columns.GridColumn colAmountExcludedVATReceived;
        private DevExpress.XtraGrid.Columns.GridColumn colAmountIncludedVATReceived;
        private DevExpress.XtraGrid.Columns.GridColumn colAmountReceived;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantityOrdered;
        private DevExpress.XtraGrid.Columns.GridColumn colOrderQuantityReceived;
        private DevExpress.XtraGrid.Columns.GridColumn colReceiptQuantityIssued;
        private DevExpress.XtraGrid.Columns.GridColumn colReceiptQuantity;
        private DevExpress.XtraGrid.Columns.GridColumn colReceiptAmount;
        private DevExpress.XtraGrid.Columns.GridColumn colAverageCost;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus;
        private System.Windows.Forms.BindingSource wRDetailBindingSource;
        private WARHOUSE_HPDataSet1TableAdapters.WRDetailTableAdapter wRDetailTableAdapter;
        private DevExpress.XtraEditors.DateEdit dateEditContractDate;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.TextEdit textEdit4;
        private DevExpress.XtraEditors.TextEdit textEditWRNumber;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditModality;
        private DevExpress.XtraEditors.DateEdit dateEditWRDate;
        private DevExpress.XtraEditors.TextEdit textEditNote;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditWarehouse;
        private DevExpress.XtraEditors.TextEdit textEditOrderNumber;
        private DevExpress.XtraEditors.DateEdit dateEditOrderDate;
        private DevExpress.XtraEditors.TextEdit textEditSupplier;
        private DevExpress.XtraEditors.TextEdit textEditContractNumber;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditPriceType;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditPaymentType;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditHandlingStatus;
        private DevExpress.XtraEditors.TextEdit textEditTotalQuantity;
        private DevExpress.XtraEditors.TextEdit textEditTotalDiscountAmount;
        private DevExpress.XtraEditors.TextEdit textEditTotalVATAmount;
        private DevExpress.XtraEditors.TextEdit textEditTotalAmount;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemWRNumber;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemModality;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemWRDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemWarehouse;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemOrderNumber;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemOrderDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemTotalQuantity;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemTotalDiscountAmount;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemTotalVATAmount;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemTotalAmount;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemContractNumber;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemContractDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemPriceType;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemPaymentType;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemHandlingStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colNo;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemSupplier;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemNote;
    }
}
