﻿using DevExpress.Utils;
using DevExpress.XtraBars.Docking2010;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using OfficeOpenXml.FormulaParsing.Excel.Operators;
using QRCoder;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Media;
using WarehouseManager.Models;
using WarehouseManager.Repository;
using webservice.Entities;

namespace WarehouseManager
{
    public partial class CountGoodsDataControl : DevExpress.XtraEditors.XtraUserControl
    {
        CountGoodsRepository countGoodsRepository;
        LocationRepository locationRepository;
        GoodsDataRepository goodsDataRepository;
        Int32[] selectedRowHandles = null;
        List<GoodsData> goodsDatasPublic = null;


        public CountGoodsDataControl()
        {
            InitializeComponent();
            this.Load += CountGoodsDataControl_Load;
            countGoodsRepository = new CountGoodsRepository();
            locationRepository = new LocationRepository();
            goodsDataRepository = new GoodsDataRepository();
            gridViewData.ClearSorting();
            gridViewData.OptionsSelection.MultiSelect = true;
            gridViewData.OptionsSelection.MultiSelectMode = GridMultiSelectMode.CheckBoxRowSelect;
            autoCompleteLocationIDAsync();
            sbLoadDataForGridlAsync(textEditLocationID.Text);
            
        }

        private void CountGoodsDataControl_Load(Object sender, EventArgs e)
        {
            windowsUIButtonPanel1.Cursor = Cursors.Hand;
            windowsUIButtonPanel1.ButtonClick += button_Click;
            gridViewData.CustomColumnDisplayText += GridView_CustomColumnDisplayText;
            windowsUIButtonPanel1.Cursor = Cursors.Hand;
            textEditLocationID.KeyDown += new KeyEventHandler(edittextLocationID_KeyDown);
            gridViewData.CellValueChanged += YourDGV_CellValueChangedAsync;
        }

        private void YourDGV_CellValueChangedAsync(object sender, CellValueChangedEventArgs e)
        {
            String goodsID = gridViewData.GetRowCellValue(e.RowHandle, "GoodsID").ToString();
            if (e.Column.FieldName.Equals("GoodsID"))
            {
                foreach (GoodsData goodsData in goodsDatasPublic)
                {
                    if (goodsData.GoodsID.Equals(goodsID) || goodsData.ECNPart.Equals(goodsID))
                    {
                        gridViewData.SetRowCellValue(e.RowHandle, "GoodsName",goodsData.GoodsName);

                    }
                }

            }
        }

        private void GridView_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {

            if (e.Column == colNo)
            {
                int rowHandle = gridViewData.GetRowHandle(e.ListSourceRowIndex);
                if (rowHandle >= 0)
                {
                    e.DisplayText = (rowHandle + 1).ToString();
                }
            }
        }

        private async Task autoCompleteLocationIDAsync()
        {
            AutoCompleteStringCollection data = new AutoCompleteStringCollection();
            List<Location> locations = await locationRepository.GetAll();
            foreach (Location location in locations)
            {
                data.Add(location.LocationID);
            }
            textEditLocationID.MaskBox.AutoCompleteSource = AutoCompleteSource.CustomSource;
            textEditLocationID.MaskBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            textEditLocationID.MaskBox.AutoCompleteCustomSource = data;
        }


        private async Task sbLoadDataForGridlAsync(String locationID)
        {
            List<CountGoods> countGoods = await countGoodsRepository.GetUnderID(locationID);
            goodsDatasPublic = await goodsDataRepository.GetAll();
            for (int i = 0; i < countGoods.Count; i++)
            {
                foreach (GoodsData goodsData in goodsDatasPublic)
                {
                    if (goodsData.GoodsID.Equals(countGoods[i].GoodsID) || goodsData.ECNPart.Equals(countGoods[i].GoodsID))
                    {
                        if (goodsData.ECNPart.Equals(""))
                        {
                            countGoods[i].GoodsName = goodsData.GoodsName;
                        }
                        else
                        {
                            countGoods[i].GoodsName = goodsData.GoodsName;
                        }

                    }
                }
            }

            int mMaxRow = 100;
            if (countGoods.Count < mMaxRow)
            {
                int num = mMaxRow - countGoods.Count;
                int i = 1;
                while (true)
                {
                    int num2 = i;
                    int num3 = num;
                    if (num2 > num3)
                    {
                        break;
                    }
                    countGoods.Add(new CountGoods());
                    i++;
                }
            }
            gridControlData.DataSource = countGoods;
            getTotalQuantity();
            getTotalGoods();
        }


        private async Task getTotalQuantity()
        {
            Decimal totalQuantity = 0;
            int i = 0;
            while (true)
            {
                int rowHandle = gridViewData.GetRowHandle(i);
                if (gridViewData.GetRowCellValue(rowHandle, "GoodsID") == null)
                {
                    break;
                }
                Decimal quantity = (Decimal)gridViewData.GetRowCellValue(rowHandle, "Quantity");
                totalQuantity += quantity;
                i++;
            }
            textEditTotalQuantity.Text = totalQuantity.ToString("0.#####");
        }

        private void getTotalGoods()
        {
            Decimal totalGoods = 0;
            int i = 0;
            while (true)
            {
                int rowHandle = gridViewData.GetRowHandle(i);
                if (gridViewData.GetRowCellValue(rowHandle, "GoodsID") == null)
                {
                    break;
                }
                if (!gridViewData.GetRowCellValue(rowHandle, "Status").Equals(""))
                {
                    totalGoods++;
                }
                i++;
            }
            textEditTotalGoods.Text = totalGoods.ToString("0.#####");
        }


        public async Task updateCountGoods()
        {
            int num = gridViewData.RowCount - 1;
            int i = 0;
            while (true)
            {
                int num2 = i;
                int num3 = num;
                if (num2 > num3 || gridViewData.GetRowCellValue(i, gridViewData.Columns["colGoodsID"]) == "")
                {
                    break;
                }
                int rowHandle = gridViewData.GetRowHandle(i);
                String goodsID = (string?)gridViewData.GetRowCellValue(rowHandle, "GoodsID");
                CountGoods countGoods =await countGoodsRepository.GetUnderMultiID(textEditLocationID.Text, goodsID);
                countGoods.LocationID = (string?)gridViewData.GetRowCellValue(rowHandle, "LocationID");
                countGoods.GoodsID = (string?)gridViewData.GetRowCellValue(rowHandle, "GoodsID");
                countGoods.GoodsName = (string?)gridViewData.GetRowCellValue(rowHandle, "GoodsName");
                countGoods.Quantity = (Decimal?)gridViewData.GetRowCellValue(rowHandle, "Quantity");
                countGoods.Status = (string?)gridViewData.GetRowCellValue(rowHandle, "Status");
                countGoods.CountDate = Convert.ToDateTime(gridViewData.GetRowCellValue(rowHandle, "CountDate").ToString());
                if (await countGoodsRepository.Update(countGoods, textEditLocationID.Text, goodsID) > 0)
                {
                    i++;
                }
                WMPublic.sbMessageSaveChangeSuccess(this);

            }
        }

        public async Task Delete()
        {
            if ((await countGoodsRepository.Delete(textEditLocationID.Text)) > 0)
            {
                sbLoadDataForGridlAsync(textEditLocationID.Text);
                WMPublic.sbMessageDeleteSuccess(this);
            }
        }


        public async Task DeleteByMultiID()
        {
            selectedRowHandles = gridViewData.GetSelectedRows();
            for (int i = 0; i < selectedRowHandles.Length; i++)
            {
                await countGoodsRepository.Delete(textEditLocationID.Text, gridViewData.GetRowCellDisplayText(selectedRowHandles[i], colGoodsID));
            }
            sbLoadDataForGridlAsync(textEditLocationID.Text);
        }

        private async void edittextLocationID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                try
                {
                    Location location = new Location();
                    location = await locationRepository.GetUnderID(textEditLocationID.Text);
                    if (location == null)
                    {
                        MessageBox.Show("Location không tồn tại");
                        return;
                    }
                    else
                    {
                        await sbLoadDataForGridlAsync(textEditLocationID.Text);
                    }
                }catch(Exception ex)
                {
                    MessageBox.Show("Location không tồn tại");
                }

            }
        }



        //barbutton click
        public async void button_Click(object sender, EventArgs e)
        {
            ButtonEventArgs eArg = (DevExpress.XtraBars.Docking2010.ButtonEventArgs)e;
            String tag = eArg.Button.Properties.Tag.ToString();
            switch (tag)
            {
                case "new":

                    break;
                case "save":
                    if (WMPublic.sbMessageSaveChangeRequest(this) == true)
                    {
                        updateCountGoods();
                    }
                    break;
                case "delete":
                    if (gridViewData.RowCount > 0)
                    {
                        if (selectedRowHandles != null && selectedRowHandles.Length > 0)
                        {
                            if (WMPublic.sbMessageDeleteRequest(this) == true)
                            {
                                DeleteByMultiID();

                            }
                        }
                        else
                        {
                            if (WMPublic.sbMessageDeleteRequest(this) == true)
                            {
                                Delete();

                            }
                        }
                    }

                    break;
                case "search":

                    break;
                case "refesh":
                    sbLoadDataForGridlAsync(textEditLocationID.Text);
                    break;
                case "import":
                    break;
                case "export":
                    break;
                case "print":
                    break;
                case "close":
                    break;
            }

        }


    }
}
