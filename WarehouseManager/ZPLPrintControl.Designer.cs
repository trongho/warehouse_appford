﻿
namespace WarehouseManager
{
    partial class ZPLPrintControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions10 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ZPLPrintControl));
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions11 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions12 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions13 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions14 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions15 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions16 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions17 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions18 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition6 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition7 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition8 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition9 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition10 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition3 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition4 = new DevExpress.XtraLayout.RowDefinition();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.windowsUIButtonPanel1 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.textEditDataPrintNumber = new DevExpress.XtraEditors.TextEdit();
            this.textEditDataPrintDate = new DevExpress.XtraEditors.TextEdit();
            this.textEditStatus = new DevExpress.XtraEditors.TextEdit();
            this.textEditTotalGoodsID = new DevExpress.XtraEditors.TextEdit();
            this.textEditTotalLabel = new DevExpress.XtraEditors.TextEdit();
            this.textEditTotalGoodsIDPrinted = new DevExpress.XtraEditors.TextEdit();
            this.textEditTotalLabelPrinted = new DevExpress.XtraEditors.TextEdit();
            this.simpleButtonShow = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonHide = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControl3 = new DevExpress.XtraLayout.LayoutControl();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.layoutControl5 = new DevExpress.XtraLayout.LayoutControl();
            this.gridControlDataPrintDetail = new DevExpress.XtraGrid.GridControl();
            this.gridViewDataPrintDetail = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colLineNo2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDataPrintNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGoodsID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrdinal2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateString = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantity2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.layoutControl4 = new DevExpress.XtraLayout.LayoutControl();
            this.gridControlDataPrintGeneral = new DevExpress.XtraGrid.GridControl();
            this.gridViewDataPrintGeneral = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colLineNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrdinal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGoodsID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPackingVolume = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPackingQuantity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGoodsName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDataPrintNumber1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.textEditWidth = new DevExpress.XtraEditors.TextEdit();
            this.textEditHeight = new DevExpress.XtraEditors.TextEdit();
            this.simpleButtonSetWidthHeight = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditDataPrintNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditDataPrintDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalGoodsID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalLabel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalGoodsIDPrinted.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalLabelPrinted.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).BeginInit();
            this.layoutControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl5)).BeginInit();
            this.layoutControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlDataPrintDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDataPrintDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl4)).BeginInit();
            this.layoutControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlDataPrintGeneral)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDataPrintGeneral)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditWidth.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditHeight.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.windowsUIButtonPanel1);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(339, 57, 650, 400);
            this.layoutControl1.Root = this.Root;
            this.layoutControl1.Size = new System.Drawing.Size(1319, 40);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // windowsUIButtonPanel1
            // 
            this.windowsUIButtonPanel1.AllowGlyphSkinning = false;
            this.windowsUIButtonPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            windowsUIButtonImageOptions10.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions10.Image")));
            windowsUIButtonImageOptions10.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions11.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions11.Image")));
            windowsUIButtonImageOptions11.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions12.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions12.Image")));
            windowsUIButtonImageOptions12.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions13.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions13.Image")));
            windowsUIButtonImageOptions13.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions14.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions14.Image")));
            windowsUIButtonImageOptions14.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions15.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions15.Image")));
            windowsUIButtonImageOptions15.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions16.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions16.Image")));
            windowsUIButtonImageOptions16.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions17.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions17.Image")));
            windowsUIButtonImageOptions17.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions18.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions18.Image")));
            windowsUIButtonImageOptions18.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            this.windowsUIButtonPanel1.Buttons.AddRange(new DevExpress.XtraEditors.ButtonPanel.IBaseButton[] {
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F2) Thêm mới", true, windowsUIButtonImageOptions10, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "new", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F3) Lưu", true, windowsUIButtonImageOptions11, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "save", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F4) Xóa", true, windowsUIButtonImageOptions12, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "delete", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F6) Làm mới", true, windowsUIButtonImageOptions13, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "refesh", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F7) Nhập dữ liệu", true, windowsUIButtonImageOptions14, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "import", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F8) Xuất dữ liệu", true, windowsUIButtonImageOptions15, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "export", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F9) In ấn", true, windowsUIButtonImageOptions16, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "print", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F5) Cài đặt", true, windowsUIButtonImageOptions17, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "setting", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F10) Thoát", true, windowsUIButtonImageOptions18, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "close", -1, false)});
            this.windowsUIButtonPanel1.ContentAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.windowsUIButtonPanel1.Font = new System.Drawing.Font("Tahoma", 10F);
            this.windowsUIButtonPanel1.Location = new System.Drawing.Point(0, 0);
            this.windowsUIButtonPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.windowsUIButtonPanel1.Name = "windowsUIButtonPanel1";
            this.windowsUIButtonPanel1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.windowsUIButtonPanel1.Size = new System.Drawing.Size(1319, 32);
            this.windowsUIButtonPanel1.TabIndex = 6;
            this.windowsUIButtonPanel1.Text = "windowsUIButtonPanel1";
            this.windowsUIButtonPanel1.UseButtonBackgroundImages = false;
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.Root.Name = "Root";
            this.Root.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.Root.Size = new System.Drawing.Size(1319, 40);
            this.Root.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.windowsUIButtonPanel1;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem1.Size = new System.Drawing.Size(1319, 40);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.textEditDataPrintNumber);
            this.layoutControl2.Controls.Add(this.textEditDataPrintDate);
            this.layoutControl2.Controls.Add(this.textEditStatus);
            this.layoutControl2.Controls.Add(this.textEditTotalGoodsID);
            this.layoutControl2.Controls.Add(this.textEditTotalLabel);
            this.layoutControl2.Controls.Add(this.textEditTotalGoodsIDPrinted);
            this.layoutControl2.Controls.Add(this.textEditTotalLabelPrinted);
            this.layoutControl2.Controls.Add(this.simpleButtonShow);
            this.layoutControl2.Controls.Add(this.simpleButtonHide);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl2.Location = new System.Drawing.Point(0, 40);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(333, 77, 650, 400);
            this.layoutControl2.Root = this.layoutControlGroup1;
            this.layoutControl2.Size = new System.Drawing.Size(1319, 66);
            this.layoutControl2.TabIndex = 1;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // textEditDataPrintNumber
            // 
            this.textEditDataPrintNumber.Location = new System.Drawing.Point(81, 2);
            this.textEditDataPrintNumber.Name = "textEditDataPrintNumber";
            this.textEditDataPrintNumber.Size = new System.Drawing.Size(180, 20);
            this.textEditDataPrintNumber.StyleController = this.layoutControl2;
            this.textEditDataPrintNumber.TabIndex = 4;
            // 
            // textEditDataPrintDate
            // 
            this.textEditDataPrintDate.Location = new System.Drawing.Point(344, 2);
            this.textEditDataPrintDate.Name = "textEditDataPrintDate";
            this.textEditDataPrintDate.Size = new System.Drawing.Size(181, 20);
            this.textEditDataPrintDate.StyleController = this.layoutControl2;
            this.textEditDataPrintDate.TabIndex = 5;
            // 
            // textEditStatus
            // 
            this.textEditStatus.Location = new System.Drawing.Point(608, 2);
            this.textEditStatus.Name = "textEditStatus";
            this.textEditStatus.Size = new System.Drawing.Size(181, 20);
            this.textEditStatus.StyleController = this.layoutControl2;
            this.textEditStatus.TabIndex = 6;
            // 
            // textEditTotalGoodsID
            // 
            this.textEditTotalGoodsID.Location = new System.Drawing.Point(872, 2);
            this.textEditTotalGoodsID.Name = "textEditTotalGoodsID";
            this.textEditTotalGoodsID.Size = new System.Drawing.Size(181, 20);
            this.textEditTotalGoodsID.StyleController = this.layoutControl2;
            this.textEditTotalGoodsID.TabIndex = 7;
            // 
            // textEditTotalLabel
            // 
            this.textEditTotalLabel.Location = new System.Drawing.Point(1136, 2);
            this.textEditTotalLabel.Name = "textEditTotalLabel";
            this.textEditTotalLabel.Size = new System.Drawing.Size(181, 20);
            this.textEditTotalLabel.StyleController = this.layoutControl2;
            this.textEditTotalLabel.TabIndex = 8;
            // 
            // textEditTotalGoodsIDPrinted
            // 
            this.textEditTotalGoodsIDPrinted.Location = new System.Drawing.Point(872, 35);
            this.textEditTotalGoodsIDPrinted.Name = "textEditTotalGoodsIDPrinted";
            this.textEditTotalGoodsIDPrinted.Size = new System.Drawing.Size(181, 20);
            this.textEditTotalGoodsIDPrinted.StyleController = this.layoutControl2;
            this.textEditTotalGoodsIDPrinted.TabIndex = 9;
            // 
            // textEditTotalLabelPrinted
            // 
            this.textEditTotalLabelPrinted.Location = new System.Drawing.Point(1136, 35);
            this.textEditTotalLabelPrinted.Name = "textEditTotalLabelPrinted";
            this.textEditTotalLabelPrinted.Size = new System.Drawing.Size(181, 20);
            this.textEditTotalLabelPrinted.StyleController = this.layoutControl2;
            this.textEditTotalLabelPrinted.TabIndex = 10;
            // 
            // simpleButtonShow
            // 
            this.simpleButtonShow.Location = new System.Drawing.Point(2, 35);
            this.simpleButtonShow.Name = "simpleButtonShow";
            this.simpleButtonShow.Size = new System.Drawing.Size(259, 22);
            this.simpleButtonShow.StyleController = this.layoutControl2;
            this.simpleButtonShow.TabIndex = 11;
            this.simpleButtonShow.Text = "Show";
            this.simpleButtonShow.Click += new System.EventHandler(this.simpleButtonShow_Click);
            // 
            // simpleButtonHide
            // 
            this.simpleButtonHide.Location = new System.Drawing.Point(265, 35);
            this.simpleButtonHide.Name = "simpleButtonHide";
            this.simpleButtonHide.Size = new System.Drawing.Size(260, 22);
            this.simpleButtonHide.StyleController = this.layoutControl2;
            this.simpleButtonHide.TabIndex = 12;
            this.simpleButtonHide.Text = "Print";
            this.simpleButtonHide.Click += new System.EventHandler(this.simpleButtonHide_Click);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem13,
            this.layoutControlItem14});
            this.layoutControlGroup1.LayoutMode = DevExpress.XtraLayout.Utils.LayoutMode.Table;
            this.layoutControlGroup1.Name = "Root";
            columnDefinition6.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition6.Width = 20D;
            columnDefinition7.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition7.Width = 20D;
            columnDefinition8.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition8.Width = 20D;
            columnDefinition9.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition9.Width = 20D;
            columnDefinition10.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition10.Width = 20D;
            this.layoutControlGroup1.OptionsTableLayoutGroup.ColumnDefinitions.AddRange(new DevExpress.XtraLayout.ColumnDefinition[] {
            columnDefinition6,
            columnDefinition7,
            columnDefinition8,
            columnDefinition9,
            columnDefinition10});
            rowDefinition3.Height = 50D;
            rowDefinition3.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition4.Height = 50D;
            rowDefinition4.SizeType = System.Windows.Forms.SizeType.Percent;
            this.layoutControlGroup1.OptionsTableLayoutGroup.RowDefinitions.AddRange(new DevExpress.XtraLayout.RowDefinition[] {
            rowDefinition3,
            rowDefinition4});
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(1319, 66);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.textEditDataPrintNumber;
            this.layoutControlItem2.CustomizationFormText = "Số dữ liệu in";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(263, 33);
            this.layoutControlItem2.Text = "Số dữ liệu";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(76, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.textEditDataPrintDate;
            this.layoutControlItem3.Location = new System.Drawing.Point(263, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.OptionsTableLayoutItem.ColumnIndex = 1;
            this.layoutControlItem3.Size = new System.Drawing.Size(264, 33);
            this.layoutControlItem3.Text = "Ngày dữ liệu";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(76, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.textEditStatus;
            this.layoutControlItem4.Location = new System.Drawing.Point(527, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.OptionsTableLayoutItem.ColumnIndex = 2;
            this.layoutControlItem4.Size = new System.Drawing.Size(264, 33);
            this.layoutControlItem4.Text = "Trạng thái";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(76, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.textEditTotalGoodsID;
            this.layoutControlItem5.Location = new System.Drawing.Point(791, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.OptionsTableLayoutItem.ColumnIndex = 3;
            this.layoutControlItem5.Size = new System.Drawing.Size(264, 33);
            this.layoutControlItem5.Text = "Σ Mã hàng";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(76, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.textEditTotalLabel;
            this.layoutControlItem6.Location = new System.Drawing.Point(1055, 0);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.OptionsTableLayoutItem.ColumnIndex = 4;
            this.layoutControlItem6.Size = new System.Drawing.Size(264, 33);
            this.layoutControlItem6.Text = "Σ Tem";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(76, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.textEditTotalGoodsIDPrinted;
            this.layoutControlItem7.Location = new System.Drawing.Point(791, 33);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.OptionsTableLayoutItem.ColumnIndex = 3;
            this.layoutControlItem7.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItem7.Size = new System.Drawing.Size(264, 33);
            this.layoutControlItem7.Text = "Σ Mã hàng đã in";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(76, 13);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.textEditTotalLabelPrinted;
            this.layoutControlItem8.Location = new System.Drawing.Point(1055, 33);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.OptionsTableLayoutItem.ColumnIndex = 4;
            this.layoutControlItem8.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItem8.Size = new System.Drawing.Size(264, 33);
            this.layoutControlItem8.Text = "Σ Tem đã in";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(76, 13);
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.simpleButtonShow;
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 33);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItem13.Size = new System.Drawing.Size(263, 33);
            this.layoutControlItem13.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem13.TextVisible = false;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.simpleButtonHide;
            this.layoutControlItem14.Location = new System.Drawing.Point(263, 33);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.OptionsTableLayoutItem.ColumnIndex = 1;
            this.layoutControlItem14.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItem14.Size = new System.Drawing.Size(264, 33);
            this.layoutControlItem14.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem14.TextVisible = false;
            // 
            // layoutControl3
            // 
            this.layoutControl3.Controls.Add(this.xtraTabControl1);
            this.layoutControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.layoutControl3.Location = new System.Drawing.Point(0, 106);
            this.layoutControl3.Name = "layoutControl3";
            this.layoutControl3.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(221, 224, 650, 400);
            this.layoutControl3.Root = this.layoutControlGroup2;
            this.layoutControl3.Size = new System.Drawing.Size(600, 548);
            this.layoutControl3.TabIndex = 2;
            this.layoutControl3.Text = "layoutControl3";
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Location = new System.Drawing.Point(12, 12);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage2;
            this.xtraTabControl1.Size = new System.Drawing.Size(576, 524);
            this.xtraTabControl1.TabIndex = 4;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2});
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.layoutControl5);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(442, 498);
            this.xtraTabPage2.Text = "Tem";
            // 
            // layoutControl5
            // 
            this.layoutControl5.Controls.Add(this.gridControlDataPrintDetail);
            this.layoutControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl5.Location = new System.Drawing.Point(0, 0);
            this.layoutControl5.Name = "layoutControl5";
            this.layoutControl5.Root = this.layoutControlGroup5;
            this.layoutControl5.Size = new System.Drawing.Size(442, 498);
            this.layoutControl5.TabIndex = 0;
            this.layoutControl5.Text = "layoutControl5";
            // 
            // gridControlDataPrintDetail
            // 
            this.gridControlDataPrintDetail.Location = new System.Drawing.Point(12, 12);
            this.gridControlDataPrintDetail.MainView = this.gridViewDataPrintDetail;
            this.gridControlDataPrintDetail.Name = "gridControlDataPrintDetail";
            this.gridControlDataPrintDetail.Size = new System.Drawing.Size(418, 474);
            this.gridControlDataPrintDetail.TabIndex = 4;
            this.gridControlDataPrintDetail.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewDataPrintDetail});
            // 
            // gridViewDataPrintDetail
            // 
            this.gridViewDataPrintDetail.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colLineNo2,
            this.colDataPrintNumber,
            this.colGoodsID2,
            this.colOrdinal2,
            this.colDateString,
            this.colQuantity2,
            this.colStatus2});
            this.gridViewDataPrintDetail.GridControl = this.gridControlDataPrintDetail;
            this.gridViewDataPrintDetail.Name = "gridViewDataPrintDetail";
            // 
            // colLineNo2
            // 
            this.colLineNo2.Caption = "No";
            this.colLineNo2.FieldName = "LineNo";
            this.colLineNo2.Name = "colLineNo2";
            this.colLineNo2.Visible = true;
            this.colLineNo2.VisibleIndex = 0;
            this.colLineNo2.Width = 70;
            // 
            // colDataPrintNumber
            // 
            this.colDataPrintNumber.Caption = "DataPrintNumber";
            this.colDataPrintNumber.FieldName = "DataPrintNumber";
            this.colDataPrintNumber.Name = "colDataPrintNumber";
            // 
            // colGoodsID2
            // 
            this.colGoodsID2.Caption = "GoodsID";
            this.colGoodsID2.FieldName = "GoodsID";
            this.colGoodsID2.Name = "colGoodsID2";
            this.colGoodsID2.Visible = true;
            this.colGoodsID2.VisibleIndex = 1;
            this.colGoodsID2.Width = 171;
            // 
            // colOrdinal2
            // 
            this.colOrdinal2.Caption = "Ordinal";
            this.colOrdinal2.FieldName = "Ordinal";
            this.colOrdinal2.Name = "colOrdinal2";
            // 
            // colDateString
            // 
            this.colDateString.Caption = "DateString";
            this.colDateString.FieldName = "DateString";
            this.colDateString.Name = "colDateString";
            this.colDateString.Visible = true;
            this.colDateString.VisibleIndex = 3;
            this.colDateString.Width = 211;
            // 
            // colQuantity2
            // 
            this.colQuantity2.Caption = "Quantity";
            this.colQuantity2.FieldName = "Quantity";
            this.colQuantity2.Name = "colQuantity2";
            this.colQuantity2.Visible = true;
            this.colQuantity2.VisibleIndex = 2;
            this.colQuantity2.Width = 89;
            // 
            // colStatus2
            // 
            this.colStatus2.Caption = "Status";
            this.colStatus2.FieldName = "Status";
            this.colStatus2.Name = "colStatus2";
            this.colStatus2.Visible = true;
            this.colStatus2.VisibleIndex = 4;
            this.colStatus2.Width = 215;
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup5.GroupBordersVisible = false;
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem11});
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(442, 498);
            this.layoutControlGroup5.TextVisible = false;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.gridControlDataPrintDetail;
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(422, 478);
            this.layoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem11.TextVisible = false;
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.layoutControl4);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(571, 498);
            this.xtraTabPage1.Text = "Hàng hóa";
            // 
            // layoutControl4
            // 
            this.layoutControl4.Controls.Add(this.gridControlDataPrintGeneral);
            this.layoutControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl4.Location = new System.Drawing.Point(0, 0);
            this.layoutControl4.Name = "layoutControl4";
            this.layoutControl4.Root = this.layoutControlGroup4;
            this.layoutControl4.Size = new System.Drawing.Size(571, 498);
            this.layoutControl4.TabIndex = 0;
            this.layoutControl4.Text = "layoutControl4";
            // 
            // gridControlDataPrintGeneral
            // 
            this.gridControlDataPrintGeneral.Location = new System.Drawing.Point(12, 12);
            this.gridControlDataPrintGeneral.MainView = this.gridViewDataPrintGeneral;
            this.gridControlDataPrintGeneral.Name = "gridControlDataPrintGeneral";
            this.gridControlDataPrintGeneral.Size = new System.Drawing.Size(547, 474);
            this.gridControlDataPrintGeneral.TabIndex = 4;
            this.gridControlDataPrintGeneral.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewDataPrintGeneral});
            // 
            // gridViewDataPrintGeneral
            // 
            this.gridViewDataPrintGeneral.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colLineNo,
            this.colOrdinal,
            this.colGoodsID,
            this.colQuantity,
            this.colStatus,
            this.colPackingVolume,
            this.colPackingQuantity,
            this.colGoodsName,
            this.colDataPrintNumber1});
            this.gridViewDataPrintGeneral.GridControl = this.gridControlDataPrintGeneral;
            this.gridViewDataPrintGeneral.Name = "gridViewDataPrintGeneral";
            // 
            // colLineNo
            // 
            this.colLineNo.Caption = "No";
            this.colLineNo.FieldName = "LineNo";
            this.colLineNo.Name = "colLineNo";
            this.colLineNo.Visible = true;
            this.colLineNo.VisibleIndex = 0;
            this.colLineNo.Width = 39;
            // 
            // colOrdinal
            // 
            this.colOrdinal.Caption = "Ordinal";
            this.colOrdinal.FieldName = "Ordinal";
            this.colOrdinal.Name = "colOrdinal";
            // 
            // colGoodsID
            // 
            this.colGoodsID.Caption = "GoodsID";
            this.colGoodsID.FieldName = "GoodsID";
            this.colGoodsID.Name = "colGoodsID";
            this.colGoodsID.Visible = true;
            this.colGoodsID.VisibleIndex = 1;
            this.colGoodsID.Width = 133;
            // 
            // colQuantity
            // 
            this.colQuantity.Caption = "Quantity";
            this.colQuantity.FieldName = "Quantity";
            this.colQuantity.Name = "colQuantity";
            this.colQuantity.Visible = true;
            this.colQuantity.VisibleIndex = 2;
            this.colQuantity.Width = 77;
            // 
            // colStatus
            // 
            this.colStatus.Caption = "Status";
            this.colStatus.FieldName = "Status";
            this.colStatus.Name = "colStatus";
            this.colStatus.Visible = true;
            this.colStatus.VisibleIndex = 4;
            this.colStatus.Width = 394;
            // 
            // colPackingVolume
            // 
            this.colPackingVolume.Caption = "PackingVolume";
            this.colPackingVolume.FieldName = "PackingVolume";
            this.colPackingVolume.Name = "colPackingVolume";
            // 
            // colPackingQuantity
            // 
            this.colPackingQuantity.Caption = "PackingQuantity";
            this.colPackingQuantity.FieldName = "PackingQuantity";
            this.colPackingQuantity.Name = "colPackingQuantity";
            this.colPackingQuantity.Visible = true;
            this.colPackingQuantity.VisibleIndex = 3;
            this.colPackingQuantity.Width = 113;
            // 
            // colGoodsName
            // 
            this.colGoodsName.Caption = "GoodsName";
            this.colGoodsName.FieldName = "GoodsName";
            this.colGoodsName.Name = "colGoodsName";
            // 
            // colDataPrintNumber1
            // 
            this.colDataPrintNumber1.Caption = "DataPrintNumber";
            this.colDataPrintNumber1.FieldName = "DataPrintNumber";
            this.colDataPrintNumber1.Name = "colDataPrintNumber1";
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup4.GroupBordersVisible = false;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem12});
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(571, 498);
            this.layoutControlGroup4.TextVisible = false;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.gridControlDataPrintGeneral;
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(551, 478);
            this.layoutControlItem12.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem12.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem9});
            this.layoutControlGroup2.Name = "Root";
            this.layoutControlGroup2.Size = new System.Drawing.Size(600, 548);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.xtraTabControl1;
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(580, 528);
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextVisible = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(590, 140);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(428, 288);
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // textEditWidth
            // 
            this.textEditWidth.Location = new System.Drawing.Point(661, 112);
            this.textEditWidth.Name = "textEditWidth";
            this.textEditWidth.Size = new System.Drawing.Size(100, 20);
            this.textEditWidth.TabIndex = 4;
            // 
            // textEditHeight
            // 
            this.textEditHeight.Location = new System.Drawing.Point(787, 112);
            this.textEditHeight.Name = "textEditHeight";
            this.textEditHeight.Size = new System.Drawing.Size(100, 20);
            this.textEditHeight.TabIndex = 5;
            // 
            // simpleButtonSetWidthHeight
            // 
            this.simpleButtonSetWidthHeight.Location = new System.Drawing.Point(914, 111);
            this.simpleButtonSetWidthHeight.Name = "simpleButtonSetWidthHeight";
            this.simpleButtonSetWidthHeight.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonSetWidthHeight.TabIndex = 6;
            this.simpleButtonSetWidthHeight.Text = "Set";
            this.simpleButtonSetWidthHeight.Click += new System.EventHandler(this.simpleButtonSetWidthHeight_Click);
            // 
            // ZPLPrintControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.simpleButtonSetWidthHeight);
            this.Controls.Add(this.textEditHeight);
            this.Controls.Add(this.textEditWidth);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.layoutControl3);
            this.Controls.Add(this.layoutControl2);
            this.Controls.Add(this.layoutControl1);
            this.Name = "ZPLPrintControl";
            this.Size = new System.Drawing.Size(1319, 654);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEditDataPrintNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditDataPrintDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalGoodsID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalLabel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalGoodsIDPrinted.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalLabelPrinted.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).EndInit();
            this.layoutControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl5)).EndInit();
            this.layoutControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlDataPrintDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDataPrintDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl4)).EndInit();
            this.layoutControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlDataPrintGeneral)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDataPrintGeneral)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditWidth.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditHeight.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel windowsUIButtonPanel1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraEditors.TextEdit textEditDataPrintNumber;
        private DevExpress.XtraEditors.TextEdit textEditDataPrintDate;
        private DevExpress.XtraEditors.TextEdit textEditStatus;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.TextEdit textEditTotalGoodsID;
        private DevExpress.XtraEditors.TextEdit textEditTotalLabel;
        private DevExpress.XtraEditors.TextEdit textEditTotalGoodsIDPrinted;
        private DevExpress.XtraEditors.TextEdit textEditTotalLabelPrinted;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControl layoutControl3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraLayout.LayoutControl layoutControl4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControl layoutControl5;
        private DevExpress.XtraGrid.GridControl gridControlDataPrintDetail;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewDataPrintDetail;
        private DevExpress.XtraGrid.Columns.GridColumn colLineNo2;
        private DevExpress.XtraGrid.Columns.GridColumn colDataPrintNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colGoodsID2;
        private DevExpress.XtraGrid.Columns.GridColumn colOrdinal2;
        private DevExpress.XtraGrid.Columns.GridColumn colDateString;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantity2;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraGrid.GridControl gridControlDataPrintGeneral;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewDataPrintGeneral;
        private DevExpress.XtraGrid.Columns.GridColumn colLineNo;
        private DevExpress.XtraGrid.Columns.GridColumn colOrdinal;
        private DevExpress.XtraGrid.Columns.GridColumn colGoodsID;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantity;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colPackingVolume;
        private DevExpress.XtraGrid.Columns.GridColumn colPackingQuantity;
        private DevExpress.XtraGrid.Columns.GridColumn colGoodsName;
        private DevExpress.XtraGrid.Columns.GridColumn colDataPrintNumber1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraEditors.SimpleButton simpleButtonShow;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraEditors.SimpleButton simpleButtonHide;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private System.Windows.Forms.PictureBox pictureBox1;
        private DevExpress.XtraEditors.TextEdit textEditWidth;
        private DevExpress.XtraEditors.TextEdit textEditHeight;
        private DevExpress.XtraEditors.SimpleButton simpleButtonSetWidthHeight;
    }
}
