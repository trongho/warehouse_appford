﻿using DevExpress.XtraGrid.Views.Grid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WarehouseManager.Models;
using WarehouseManager.Repository;

namespace WarehouseManager
{
    public partial class frmSearchGoods : Form
    {

        public static Boolean isReceipRequisition;
        public static Boolean isIssueRequisition;
        GoodsDataRepository goodsDataRepository;
        public frmSearchGoods()
        {
            InitializeComponent();
            this.Load += frmSearchGoods_Load;
            goodsDataRepository = new GoodsDataRepository();
        }

        private void frmSearchGoods_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'wARHOUSE_HPDataSet1.WRRHeader' table. You can move, or remove it, as needed.
            sbLoadDataForGridWRRDetailAsync();
            gridViewGoods.DoubleClick += dataGridView_CellDoubleClick;
        }

        private async Task sbLoadDataForGridWRRDetailAsync()
        {
            List<GoodsData> goodsDatas = await goodsDataRepository.GetAll();
            gridControlGoods.DataSource =goodsDatas;
        }

        private void dataGridView_CellDoubleClick(object sender, EventArgs e)
        {
            if (gridViewGoods.RowCount > 0)
            {
                if (isReceipRequisition == true)
                {
                    ReceiptRequisitionControl.selectedGoodsID = (string)(sender as GridView).GetFocusedRowCellValue("GoodsID");
                    ReceiptRequisitionControl.selectedGoodsName = (string)(sender as GridView).GetFocusedRowCellValue("GoodsName");
                    this.Close();
                }
                if (isIssueRequisition == true)
                {
                    IssueRequisitionControl.selectedGoodsID = (string)(sender as GridView).GetFocusedRowCellValue("GoodsID");
                    IssueRequisitionControl.selectedGoodsName = (string)(sender as GridView).GetFocusedRowCellValue("GoodsName");
                    this.Close();
                }

            }
        }
    }
}
