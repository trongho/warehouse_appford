﻿using DevExpress.XtraBars.Docking2010;
using DevExpress.XtraGrid.Views.Grid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WarehouseManager.Models;
using WarehouseManager.Repository;
using webservice.Entities;

namespace WarehouseManager
{
    public partial class frmSearchWID : Form
    {
        HandlingStatusRepository handlingStatusRepository;
        OrderStatusRepository orderStatusRepository;
        BranchRepository branchRepository;
        WIDataHeaderRepository wIDataHeaderRepository;
        public frmSearchWID()
        {
            InitializeComponent();
            this.Load += frmSearchWID_Load;
            handlingStatusRepository = new HandlingStatusRepository();
            branchRepository = new BranchRepository();
            wIDataHeaderRepository = new WIDataHeaderRepository();
            orderStatusRepository = new OrderStatusRepository();
        }

        private void frmSearchWID_Load(object sender, EventArgs e)
        {
            popupMenuSelectBranch();
            popupMenuSelectHandlingStatus();
            popupMenuSelectOrderStatus();
            sbLoadDataForGridWIDHeaderAsync();
            gridViewWIDataHeader.DoubleClick += dataGridView_CellDoubleClick;
            simpleButtonFilter.Click += filterButton_Click;
            windowsUIButtonPanel1.ButtonClick += button_Click;
            this.KeyDown += new KeyEventHandler(Form1_KeyDown);

            if (int.Parse(DateTime.Now.Day.ToString()) == 1)
            {
                dateEditFromDate.Text = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day).ToString("yyyy-MM-dd");
            }
            else
            {
                dateEditFromDate.Text = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day - 1).ToString("yyyy-MM-dd");
            }
            dateEditToDate.Text = DateTime.Now.ToString("yyyy-MM-dd");
        }

        public async void button_Click(object sender, EventArgs e)
        {
            ButtonEventArgs eArg = (DevExpress.XtraBars.Docking2010.ButtonEventArgs)e;
            String tag = eArg.Button.Properties.Tag.ToString();
            switch (tag)
            {
                case "refesh":
                    sbLoadDataForGridWIDHeaderAsync();
                    break;
                case "close":
                    this.Close();
                    break;
            }

        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F5)
            {
                sbLoadDataForGridWIDHeaderAsync();
            }
            if (e.KeyCode == Keys.F10)
            {
                this.Close();
            }
        }

        private async Task popupMenuSelectHandlingStatus()
        {
            List<HandlingStatus> handlingStatuses = await handlingStatusRepository.GetAll();
            foreach (HandlingStatus handlingStatus in handlingStatuses)
            {
                comboBoxEditHandlingStatus.Properties.Items.Add(handlingStatus.HandlingStatusName);
            }
            comboBoxEditHandlingStatus.SelectedIndex = 0;
        }

        private async Task popupMenuSelectOrderStatus()
        {
            List<OrderStatus> orderStatuses = await orderStatusRepository.GetAll();
            foreach (OrderStatus orderStatus in orderStatuses)
            {
                comboBoxEditOrderStatus.Properties.Items.Add(orderStatus.OrderStatusName);
            }
            comboBoxEditOrderStatus.SelectedIndex = 0;
        }

        private async Task popupMenuSelectBranch()
        {
            List<Branch> branches = await branchRepository.getAllBranchAsync();
            foreach (Branch branch in branches)
            {
                comboBoxEditBranch.Properties.Items.Add(branch.BranchName);
            }
            comboBoxEditBranch.SelectedIndex = 0;
        }

        private async Task sbLoadDataForGridWIDHeaderAsync()
        {
            List<WIDataHeader> wIDataHeaders = await wIDataHeaderRepository.GetAll();
            gridControlWIDataHeader.DataSource =wIDataHeaders;
        }

        private async void dataGridView_CellDoubleClick(object sender, EventArgs e)
        {
            if (gridViewWIDataHeader.RowCount > 0)
            {
                WIDataHeader wIDataHeader = await wIDataHeaderRepository.GetUnderID((string)(sender as GridView).GetFocusedRowCellValue("WIDNumber"));
                IssueDataControl.selectedWIDNumber = (string)(sender as GridView).GetFocusedRowCellValue("WIDNumber");
                this.Close();
            }
        }

        private async void filterButton_Click(Object sender,EventArgs e)
        {
            //String branchID = $"{int.Parse(comboBoxEditBranch.SelectedIndex.ToString()) - 1:D3}";
            //String handlingStatusID = (comboBoxEditHandlingStatus.SelectedIndex - 1).ToString();
            //String orderStatusID = (comboBoxEditOrderStatus.SelectedIndex - 1).ToString();
            //DateTime fromDate = DateTime.Parse(dateEditFromDate.Text);
            //DateTime toDate = DateTime.Parse(dateEditToDate.Text);

            //List<WIDataHeader> wIDataHeadersUnderBranch = new List<WIDataHeader>();
            //List<WIDataHeader> wIDataHeadersUnderDate = new List<WIDataHeader>();
            //List<WIDataHeader> wIDataHeadersUnderHandlingStatus = new List<WIDataHeader>();
            //List<WIDataHeader> wIDataHeadersUnderOrderStatus = new List<WIDataHeader>();
            //List<WIDataHeader> wIDataHeaders = new List<WIDataHeader>();
            //if (branchID.Equals("<>"))
            //{
            //    wIDataHeadersUnderBranch = await wIDataHeaderRepository.GetAll();
            //}
            //else
            //{
            //    wIDataHeadersUnderBranch = await wIDataHeaderRepository.GetUnderBranch(branchID);
            //}
            //if (handlingStatusID.Equals("<>"))
            //{
            //    wIDataHeadersUnderHandlingStatus = await wIDataHeaderRepository.GetAll();
            //}
            //else
            //{
            //    wIDataHeadersUnderHandlingStatus = await wIDataHeaderRepository.GetUnderHandlingStatus(handlingStatusID);
            //}
            //if (orderStatusID.Equals("<>"))
            //{
            //    wIDataHeadersUnderOrderStatus = await wIDataHeaderRepository.GetAll();
            //}
            //else
            //{
            //    wIDataHeadersUnderOrderStatus = await wIDataHeaderRepository.GetUnderOrderStatus(orderStatusID);
            //}
            //wIDataHeadersUnderDate = await wIDataHeaderRepository.GetUnderDate(fromDate, toDate);

            //for (int i = 0; i < wIDataHeadersUnderBranch.Count; i++)
            //{
            //    for (int j = 0; j < wIDataHeadersUnderDate.Count; j++)
            //    {
            //        for (int k = 0; k < wIDataHeadersUnderHandlingStatus.Count; k++)
            //        {
            //            for (int h = 0; h < wIDataHeadersUnderOrderStatus.Count; h++)
            //            {
            //                if (wIDataHeadersUnderBranch[i].WIRNumber.Equals(wIDataHeadersUnderDate[j].WIRNumber)
            //                && wIDataHeadersUnderDate[j].WIRNumber.Equals(wIDataHeadersUnderHandlingStatus[k].WIRNumber)
            //                && wIDataHeadersUnderHandlingStatus[k].WIRNumber.Equals(wIDataHeadersUnderOrderStatus[h].WIRNumber))
            //                {
            //                    wIDataHeaders.Add(wIDataHeadersUnderBranch[i]);
            //                }
            //            }
            //        }
            //    }
            //}
            String branchID = "<>";
            String handlingStatusID = "<>";
            if (!comboBoxEditBranch.SelectedItem.ToString().Equals("<>"))
            {
                branchID = $"{int.Parse(comboBoxEditBranch.SelectedIndex.ToString()) - 1:D3}";
            }
            if (!comboBoxEditHandlingStatus.SelectedItem.ToString().Equals("<>"))
            {
                handlingStatusID = (comboBoxEditHandlingStatus.SelectedIndex - 1).ToString();
            }
            String orderStatusID = (comboBoxEditOrderStatus.SelectedIndex - 1).ToString();
            DateTime fromDate = DateTime.Parse(dateEditFromDate.Text);
            DateTime toDate = DateTime.Parse(dateEditToDate.Text);

            List<WIDataHeader> wIDataHeaders = new List<WIDataHeader>();

            wIDataHeaders = await wIDataHeaderRepository.FilterFinal(branchID, fromDate, toDate, handlingStatusID);
            gridControlWIDataHeader.DataSource = wIDataHeaders;

        }

    }
}
