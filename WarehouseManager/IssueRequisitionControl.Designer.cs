﻿
namespace WarehouseManager
{
    partial class IssueRequisitionControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions1 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(IssueRequisitionControl));
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions2 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions3 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions4 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions5 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions6 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions7 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions8 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions9 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition1 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition2 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition3 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition4 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition5 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition6 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition1 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition2 = new DevExpress.XtraLayout.RowDefinition();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.windowsUIButtonPanel1 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.label1 = new System.Windows.Forms.Label();
            this.textEditWIRNumber = new DevExpress.XtraEditors.TextEdit();
            this.dateEditWIRDate = new DevExpress.XtraEditors.DateEdit();
            this.textEditTotalQuantityByItem = new DevExpress.XtraEditors.TextEdit();
            this.textEditTotalQuantityByPack = new DevExpress.XtraEditors.TextEdit();
            this.comboBoxEditHandlingStatus = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEditBranch = new DevExpress.XtraEditors.ComboBoxEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemWIRNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemWIRDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemTotalQuantityByItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemTotalQuantityByPack = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemBranch = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemHandlingStatus = new DevExpress.XtraLayout.LayoutControlItem();
            this.wRRDetailBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.wARHOUSE_HPDataSet1 = new WarehouseManager.WARHOUSE_HPDataSet1();
            this.wRRDetailTableAdapter = new WarehouseManager.WARHOUSE_HPDataSet1TableAdapters.WRRDetailTableAdapter();
            this.layoutControl3 = new DevExpress.XtraLayout.LayoutControl();
            this.gridControlWIRDetail = new DevExpress.XtraGrid.GridControl();
            this.gridViewWIRDetail = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colWIRNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrdinal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGoodsID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGoodsName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOtherGoodsName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPackingVolume = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalQuantity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantityByItem = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantityByPack = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocationID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colScanOption = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNote = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWIRDetailNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGoodsGroupID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPickerName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditWIRNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditWIRDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditWIRDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalQuantityByItem.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalQuantityByPack.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditHandlingStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditBranch.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWIRNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWIRDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalQuantityByItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalQuantityByPack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemBranch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemHandlingStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wRRDetailBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wARHOUSE_HPDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).BeginInit();
            this.layoutControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlWIRDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewWIRDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.windowsUIButtonPanel1);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(639, 159, 812, 500);
            this.layoutControl1.Root = this.Root;
            this.layoutControl1.Size = new System.Drawing.Size(1240, 37);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // windowsUIButtonPanel1
            // 
            this.windowsUIButtonPanel1.AllowGlyphSkinning = false;
            this.windowsUIButtonPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            windowsUIButtonImageOptions1.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions1.Image")));
            windowsUIButtonImageOptions1.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions2.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions2.Image")));
            windowsUIButtonImageOptions2.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions3.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions3.Image")));
            windowsUIButtonImageOptions3.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions4.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions4.Image")));
            windowsUIButtonImageOptions4.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions5.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions5.Image")));
            windowsUIButtonImageOptions5.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions6.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions6.Image")));
            windowsUIButtonImageOptions6.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions7.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions7.Image")));
            windowsUIButtonImageOptions7.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions8.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions8.Image")));
            windowsUIButtonImageOptions8.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions9.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions9.Image")));
            windowsUIButtonImageOptions9.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            this.windowsUIButtonPanel1.Buttons.AddRange(new DevExpress.XtraEditors.ButtonPanel.IBaseButton[] {
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F2) Thêm mới", true, windowsUIButtonImageOptions1, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "new", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F3) Lưu", true, windowsUIButtonImageOptions2, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "save", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F4) Xóa", true, windowsUIButtonImageOptions3, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "delete", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F5) Tìm kiếm", true, windowsUIButtonImageOptions4, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "search", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F6) Làm mới", true, windowsUIButtonImageOptions5, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "refesh", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F7) Nhập dữ liệu", true, windowsUIButtonImageOptions6, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "import", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F8) Xuất dữ liệu", true, windowsUIButtonImageOptions7, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "export", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F9) In ấn", true, windowsUIButtonImageOptions8, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "print", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F10) Thoát", true, windowsUIButtonImageOptions9, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "close", -1, false)});
            this.windowsUIButtonPanel1.ContentAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.windowsUIButtonPanel1.Location = new System.Drawing.Point(2, 2);
            this.windowsUIButtonPanel1.Name = "windowsUIButtonPanel1";
            this.windowsUIButtonPanel1.Padding = new System.Windows.Forms.Padding(5);
            this.windowsUIButtonPanel1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.windowsUIButtonPanel1.Size = new System.Drawing.Size(1236, 32);
            this.windowsUIButtonPanel1.TabIndex = 5;
            this.windowsUIButtonPanel1.Text = "windowsUIButtonPanel1";
            this.windowsUIButtonPanel1.UseButtonBackgroundImages = false;
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.Root.Name = "Root";
            this.Root.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.Root.Size = new System.Drawing.Size(1240, 37);
            this.Root.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.windowsUIButtonPanel1;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(1240, 37);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.label1);
            this.layoutControl2.Controls.Add(this.textEditWIRNumber);
            this.layoutControl2.Controls.Add(this.dateEditWIRDate);
            this.layoutControl2.Controls.Add(this.textEditTotalQuantityByItem);
            this.layoutControl2.Controls.Add(this.textEditTotalQuantityByPack);
            this.layoutControl2.Controls.Add(this.comboBoxEditHandlingStatus);
            this.layoutControl2.Controls.Add(this.comboBoxEditBranch);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl2.Location = new System.Drawing.Point(0, 37);
            this.layoutControl2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(554, 228, 812, 500);
            this.layoutControl2.Root = this.layoutControlGroup1;
            this.layoutControl2.Size = new System.Drawing.Size(1240, 76);
            this.layoutControl2.TabIndex = 1;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // label1
            // 
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(248, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 24);
            this.label1.TabIndex = 6;
            this.label1.Text = "(*)";
            // 
            // textEditWIRNumber
            // 
            this.textEditWIRNumber.Location = new System.Drawing.Point(70, 12);
            this.textEditWIRNumber.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textEditWIRNumber.Name = "textEditWIRNumber";
            this.textEditWIRNumber.Size = new System.Drawing.Size(174, 20);
            this.textEditWIRNumber.StyleController = this.layoutControl2;
            this.textEditWIRNumber.TabIndex = 4;
            // 
            // dateEditWIRDate
            // 
            this.dateEditWIRDate.EditValue = null;
            this.dateEditWIRDate.Location = new System.Drawing.Point(384, 12);
            this.dateEditWIRDate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dateEditWIRDate.Name = "dateEditWIRDate";
            this.dateEditWIRDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditWIRDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditWIRDate.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditWIRDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditWIRDate.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEditWIRDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditWIRDate.Size = new System.Drawing.Size(133, 20);
            this.dateEditWIRDate.StyleController = this.layoutControl2;
            this.dateEditWIRDate.TabIndex = 7;
            // 
            // textEditTotalQuantityByItem
            // 
            this.textEditTotalQuantityByItem.Location = new System.Drawing.Point(1095, 12);
            this.textEditTotalQuantityByItem.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textEditTotalQuantityByItem.Name = "textEditTotalQuantityByItem";
            this.textEditTotalQuantityByItem.Properties.DisplayFormat.FormatString = "G29";
            this.textEditTotalQuantityByItem.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.textEditTotalQuantityByItem.Properties.EditFormat.FormatString = "G29";
            this.textEditTotalQuantityByItem.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.textEditTotalQuantityByItem.Size = new System.Drawing.Size(133, 20);
            this.textEditTotalQuantityByItem.StyleController = this.layoutControl2;
            this.textEditTotalQuantityByItem.TabIndex = 10;
            // 
            // textEditTotalQuantityByPack
            // 
            this.textEditTotalQuantityByPack.Location = new System.Drawing.Point(1095, 40);
            this.textEditTotalQuantityByPack.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textEditTotalQuantityByPack.Name = "textEditTotalQuantityByPack";
            this.textEditTotalQuantityByPack.Properties.DisplayFormat.FormatString = "G29";
            this.textEditTotalQuantityByPack.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.textEditTotalQuantityByPack.Size = new System.Drawing.Size(133, 20);
            this.textEditTotalQuantityByPack.StyleController = this.layoutControl2;
            this.textEditTotalQuantityByPack.TabIndex = 11;
            // 
            // comboBoxEditHandlingStatus
            // 
            this.comboBoxEditHandlingStatus.Location = new System.Drawing.Point(384, 40);
            this.comboBoxEditHandlingStatus.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboBoxEditHandlingStatus.Name = "comboBoxEditHandlingStatus";
            this.comboBoxEditHandlingStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditHandlingStatus.Size = new System.Drawing.Size(133, 20);
            this.comboBoxEditHandlingStatus.StyleController = this.layoutControl2;
            this.comboBoxEditHandlingStatus.TabIndex = 13;
            // 
            // comboBoxEditBranch
            // 
            this.comboBoxEditBranch.Location = new System.Drawing.Point(112, 40);
            this.comboBoxEditBranch.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboBoxEditBranch.Name = "comboBoxEditBranch";
            this.comboBoxEditBranch.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditBranch.Size = new System.Drawing.Size(132, 20);
            this.comboBoxEditBranch.StyleController = this.layoutControl2;
            this.comboBoxEditBranch.TabIndex = 14;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemWIRNumber,
            this.layoutControlItem3,
            this.layoutControlItemWIRDate,
            this.layoutControlItemTotalQuantityByItem,
            this.layoutControlItemTotalQuantityByPack,
            this.layoutControlItemBranch,
            this.layoutControlItemHandlingStatus});
            this.layoutControlGroup1.LayoutMode = DevExpress.XtraLayout.Utils.LayoutMode.Table;
            this.layoutControlGroup1.Name = "Root";
            columnDefinition1.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition1.Width = 19.405592722871681D;
            columnDefinition2.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition2.Width = 2.9720363856415997D;
            columnDefinition3.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition3.Width = 19.405592722871681D;
            columnDefinition4.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition4.Width = 19.405592722871681D;
            columnDefinition5.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition5.Width = 19.405592722871681D;
            columnDefinition6.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition6.Width = 19.405592722871681D;
            this.layoutControlGroup1.OptionsTableLayoutGroup.ColumnDefinitions.AddRange(new DevExpress.XtraLayout.ColumnDefinition[] {
            columnDefinition1,
            columnDefinition2,
            columnDefinition3,
            columnDefinition4,
            columnDefinition5,
            columnDefinition6});
            rowDefinition1.Height = 50D;
            rowDefinition1.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition2.Height = 50D;
            rowDefinition2.SizeType = System.Windows.Forms.SizeType.Percent;
            this.layoutControlGroup1.OptionsTableLayoutGroup.RowDefinitions.AddRange(new DevExpress.XtraLayout.RowDefinition[] {
            rowDefinition1,
            rowDefinition2});
            this.layoutControlGroup1.Size = new System.Drawing.Size(1240, 76);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItemWIRNumber
            // 
            this.layoutControlItemWIRNumber.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.layoutControlItemWIRNumber.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItemWIRNumber.Control = this.textEditWIRNumber;
            this.layoutControlItemWIRNumber.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemWIRNumber.Name = "layoutControlItemWIRNumber";
            this.layoutControlItemWIRNumber.Size = new System.Drawing.Size(236, 28);
            this.layoutControlItemWIRNumber.Text = "Số phiếu";
            this.layoutControlItemWIRNumber.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItemWIRNumber.TextSize = new System.Drawing.Size(53, 17);
            this.layoutControlItemWIRNumber.TextToControlDistance = 5;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.label1;
            this.layoutControlItem3.Location = new System.Drawing.Point(236, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.OptionsTableLayoutItem.ColumnIndex = 1;
            this.layoutControlItem3.Size = new System.Drawing.Size(36, 28);
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItemWIRDate
            // 
            this.layoutControlItemWIRDate.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.layoutControlItemWIRDate.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItemWIRDate.Control = this.dateEditWIRDate;
            this.layoutControlItemWIRDate.Location = new System.Drawing.Point(272, 0);
            this.layoutControlItemWIRDate.Name = "layoutControlItemWIRDate";
            this.layoutControlItemWIRDate.OptionsTableLayoutItem.ColumnIndex = 2;
            this.layoutControlItemWIRDate.Size = new System.Drawing.Size(237, 28);
            this.layoutControlItemWIRDate.Text = "Ngày yêu cầu";
            this.layoutControlItemWIRDate.TextSize = new System.Drawing.Size(97, 17);
            // 
            // layoutControlItemTotalQuantityByItem
            // 
            this.layoutControlItemTotalQuantityByItem.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.layoutControlItemTotalQuantityByItem.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItemTotalQuantityByItem.Control = this.textEditTotalQuantityByItem;
            this.layoutControlItemTotalQuantityByItem.CustomizationFormText = "Σ SL Yêu Cầu";
            this.layoutControlItemTotalQuantityByItem.Location = new System.Drawing.Point(983, 0);
            this.layoutControlItemTotalQuantityByItem.Name = "layoutControlItemTotalQuantityByItem";
            this.layoutControlItemTotalQuantityByItem.OptionsTableLayoutItem.ColumnIndex = 5;
            this.layoutControlItemTotalQuantityByItem.Size = new System.Drawing.Size(237, 28);
            this.layoutControlItemTotalQuantityByItem.Text = "Σ SL yêu cầu";
            this.layoutControlItemTotalQuantityByItem.TextSize = new System.Drawing.Size(97, 17);
            // 
            // layoutControlItemTotalQuantityByPack
            // 
            this.layoutControlItemTotalQuantityByPack.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.layoutControlItemTotalQuantityByPack.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItemTotalQuantityByPack.Control = this.textEditTotalQuantityByPack;
            this.layoutControlItemTotalQuantityByPack.Location = new System.Drawing.Point(983, 28);
            this.layoutControlItemTotalQuantityByPack.Name = "layoutControlItemTotalQuantityByPack";
            this.layoutControlItemTotalQuantityByPack.OptionsTableLayoutItem.ColumnIndex = 5;
            this.layoutControlItemTotalQuantityByPack.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItemTotalQuantityByPack.Size = new System.Drawing.Size(237, 28);
            this.layoutControlItemTotalQuantityByPack.Text = "Σ Mã yêu cầu";
            this.layoutControlItemTotalQuantityByPack.TextSize = new System.Drawing.Size(97, 17);
            // 
            // layoutControlItemBranch
            // 
            this.layoutControlItemBranch.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.layoutControlItemBranch.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItemBranch.Control = this.comboBoxEditBranch;
            this.layoutControlItemBranch.Location = new System.Drawing.Point(0, 28);
            this.layoutControlItemBranch.Name = "layoutControlItemBranch";
            this.layoutControlItemBranch.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItemBranch.Size = new System.Drawing.Size(236, 28);
            this.layoutControlItemBranch.Text = "Chi nhánh";
            this.layoutControlItemBranch.TextSize = new System.Drawing.Size(97, 16);
            // 
            // layoutControlItemHandlingStatus
            // 
            this.layoutControlItemHandlingStatus.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.layoutControlItemHandlingStatus.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItemHandlingStatus.Control = this.comboBoxEditHandlingStatus;
            this.layoutControlItemHandlingStatus.Location = new System.Drawing.Point(272, 28);
            this.layoutControlItemHandlingStatus.Name = "layoutControlItemHandlingStatus";
            this.layoutControlItemHandlingStatus.OptionsTableLayoutItem.ColumnIndex = 2;
            this.layoutControlItemHandlingStatus.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItemHandlingStatus.Size = new System.Drawing.Size(237, 28);
            this.layoutControlItemHandlingStatus.Text = "Trạng thái xử lý";
            this.layoutControlItemHandlingStatus.TextSize = new System.Drawing.Size(97, 17);
            // 
            // wRRDetailBindingSource
            // 
            this.wRRDetailBindingSource.DataMember = "WRRDetail";
            this.wRRDetailBindingSource.DataSource = this.wARHOUSE_HPDataSet1;
            // 
            // wARHOUSE_HPDataSet1
            // 
            this.wARHOUSE_HPDataSet1.DataSetName = "WARHOUSE_HPDataSet1";
            this.wARHOUSE_HPDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // wRRDetailTableAdapter
            // 
            this.wRRDetailTableAdapter.ClearBeforeFill = true;
            // 
            // layoutControl3
            // 
            this.layoutControl3.Controls.Add(this.gridControlWIRDetail);
            this.layoutControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl3.Location = new System.Drawing.Point(0, 113);
            this.layoutControl3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.layoutControl3.Name = "layoutControl3";
            this.layoutControl3.Root = this.layoutControlGroup2;
            this.layoutControl3.Size = new System.Drawing.Size(1240, 533);
            this.layoutControl3.TabIndex = 2;
            this.layoutControl3.Text = "layoutControl3";
            // 
            // gridControlWIRDetail
            // 
            this.gridControlWIRDetail.DataSource = this.wRRDetailBindingSource;
            this.gridControlWIRDetail.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gridControlWIRDetail.Location = new System.Drawing.Point(12, 12);
            this.gridControlWIRDetail.MainView = this.gridViewWIRDetail;
            this.gridControlWIRDetail.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gridControlWIRDetail.Name = "gridControlWIRDetail";
            this.gridControlWIRDetail.Size = new System.Drawing.Size(1216, 509);
            this.gridControlWIRDetail.TabIndex = 5;
            this.gridControlWIRDetail.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewWIRDetail});
            // 
            // gridViewWIRDetail
            // 
            this.gridViewWIRDetail.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colWIRNumber,
            this.colOrdinal,
            this.colGoodsID,
            this.colGoodsName,
            this.colOtherGoodsName,
            this.colPackingVolume,
            this.colTotalQuantity,
            this.colQuantityByItem,
            this.colQuantityByPack,
            this.colLocationID,
            this.colScanOption,
            this.colNote,
            this.colStatus,
            this.colWIRDetailNo,
            this.colGoodsGroupID,
            this.colPickerName});
            this.gridViewWIRDetail.DetailHeight = 284;
            this.gridViewWIRDetail.GridControl = this.gridControlWIRDetail;
            this.gridViewWIRDetail.Name = "gridViewWIRDetail";
            this.gridViewWIRDetail.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.True;
            this.gridViewWIRDetail.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.Click;
            this.gridViewWIRDetail.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom;
            this.gridViewWIRDetail.OptionsView.RowAutoHeight = true;
            // 
            // colWIRNumber
            // 
            this.colWIRNumber.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colWIRNumber.AppearanceCell.Options.UseFont = true;
            this.colWIRNumber.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colWIRNumber.AppearanceHeader.Options.UseFont = true;
            this.colWIRNumber.AppearanceHeader.Options.UseTextOptions = true;
            this.colWIRNumber.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.colWIRNumber.FieldName = "WIRNumber";
            this.colWIRNumber.MinWidth = 21;
            this.colWIRNumber.Name = "colWIRNumber";
            this.colWIRNumber.Width = 81;
            // 
            // colOrdinal
            // 
            this.colOrdinal.FieldName = "Ordinal";
            this.colOrdinal.MinWidth = 21;
            this.colOrdinal.Name = "colOrdinal";
            this.colOrdinal.Width = 81;
            // 
            // colGoodsID
            // 
            this.colGoodsID.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colGoodsID.AppearanceCell.Options.UseFont = true;
            this.colGoodsID.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colGoodsID.AppearanceHeader.Options.UseFont = true;
            this.colGoodsID.Caption = "PART NUMBER";
            this.colGoodsID.FieldName = "GoodsID";
            this.colGoodsID.MinWidth = 86;
            this.colGoodsID.Name = "colGoodsID";
            this.colGoodsID.Visible = true;
            this.colGoodsID.VisibleIndex = 1;
            this.colGoodsID.Width = 153;
            // 
            // colGoodsName
            // 
            this.colGoodsName.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colGoodsName.AppearanceCell.Options.UseFont = true;
            this.colGoodsName.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colGoodsName.AppearanceHeader.Options.UseFont = true;
            this.colGoodsName.Caption = "PART NAME";
            this.colGoodsName.FieldName = "GoodsName";
            this.colGoodsName.MinWidth = 86;
            this.colGoodsName.Name = "colGoodsName";
            this.colGoodsName.Visible = true;
            this.colGoodsName.VisibleIndex = 2;
            this.colGoodsName.Width = 145;
            // 
            // colOtherGoodsName
            // 
            this.colOtherGoodsName.Caption = "Tên khác của hàng hóa";
            this.colOtherGoodsName.FieldName = "OtherGoodsName";
            this.colOtherGoodsName.MinWidth = 86;
            this.colOtherGoodsName.Name = "colOtherGoodsName";
            this.colOtherGoodsName.Width = 214;
            // 
            // colPackingVolume
            // 
            this.colPackingVolume.Caption = "Quy cách đóng gói";
            this.colPackingVolume.FieldName = "PackingVolume";
            this.colPackingVolume.MinWidth = 86;
            this.colPackingVolume.Name = "colPackingVolume";
            this.colPackingVolume.Width = 86;
            // 
            // colTotalQuantity
            // 
            this.colTotalQuantity.Caption = "Tổng lượng";
            this.colTotalQuantity.FieldName = "TotalQuantity";
            this.colTotalQuantity.MinWidth = 86;
            this.colTotalQuantity.Name = "colTotalQuantity";
            this.colTotalQuantity.Width = 86;
            // 
            // colQuantityByItem
            // 
            this.colQuantityByItem.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colQuantityByItem.AppearanceCell.Options.UseFont = true;
            this.colQuantityByItem.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colQuantityByItem.AppearanceHeader.Options.UseFont = true;
            this.colQuantityByItem.Caption = "SL YÊU CẦU";
            this.colQuantityByItem.DisplayFormat.FormatString = "G29";
            this.colQuantityByItem.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colQuantityByItem.FieldName = "QuantityByItem";
            this.colQuantityByItem.MinWidth = 50;
            this.colQuantityByItem.Name = "colQuantityByItem";
            this.colQuantityByItem.Visible = true;
            this.colQuantityByItem.VisibleIndex = 5;
            this.colQuantityByItem.Width = 111;
            // 
            // colQuantityByPack
            // 
            this.colQuantityByPack.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colQuantityByPack.AppearanceCell.Options.UseFont = true;
            this.colQuantityByPack.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colQuantityByPack.AppearanceHeader.Options.UseFont = true;
            this.colQuantityByPack.Caption = "NBR CNT";
            this.colQuantityByPack.FieldName = "QuantityByPack";
            this.colQuantityByPack.MinWidth = 86;
            this.colQuantityByPack.Name = "colQuantityByPack";
            this.colQuantityByPack.Width = 106;
            // 
            // colLocationID
            // 
            this.colLocationID.Caption = "Vị trí";
            this.colLocationID.FieldName = "LocationID";
            this.colLocationID.MinWidth = 86;
            this.colLocationID.Name = "colLocationID";
            this.colLocationID.Width = 91;
            // 
            // colScanOption
            // 
            this.colScanOption.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colScanOption.AppearanceCell.Options.UseFont = true;
            this.colScanOption.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colScanOption.AppearanceHeader.Options.UseFont = true;
            this.colScanOption.Caption = "Tùy chọn quét";
            this.colScanOption.FieldName = "ScanOption";
            this.colScanOption.MinWidth = 50;
            this.colScanOption.Name = "colScanOption";
            this.colScanOption.Visible = true;
            this.colScanOption.VisibleIndex = 6;
            this.colScanOption.Width = 110;
            // 
            // colNote
            // 
            this.colNote.Caption = "Ghi chú";
            this.colNote.FieldName = "Note";
            this.colNote.MinWidth = 86;
            this.colNote.Name = "colNote";
            this.colNote.Width = 145;
            // 
            // colStatus
            // 
            this.colStatus.FieldName = "Status";
            this.colStatus.MinWidth = 21;
            this.colStatus.Name = "colStatus";
            this.colStatus.Width = 100;
            // 
            // colWIRDetailNo
            // 
            this.colWIRDetailNo.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colWIRDetailNo.AppearanceCell.Options.UseFont = true;
            this.colWIRDetailNo.AppearanceCell.Options.UseTextOptions = true;
            this.colWIRDetailNo.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colWIRDetailNo.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colWIRDetailNo.AppearanceHeader.Options.UseFont = true;
            this.colWIRDetailNo.Caption = "NO";
            this.colWIRDetailNo.FieldName = "WIRDetailNo";
            this.colWIRDetailNo.MinWidth = 21;
            this.colWIRDetailNo.Name = "colWIRDetailNo";
            this.colWIRDetailNo.Visible = true;
            this.colWIRDetailNo.VisibleIndex = 0;
            this.colWIRDetailNo.Width = 51;
            // 
            // colGoodsGroupID
            // 
            this.colGoodsGroupID.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colGoodsGroupID.AppearanceHeader.Options.UseFont = true;
            this.colGoodsGroupID.Caption = "Mã xe";
            this.colGoodsGroupID.FieldName = "GoodsGroupID";
            this.colGoodsGroupID.MinWidth = 50;
            this.colGoodsGroupID.Name = "colGoodsGroupID";
            this.colGoodsGroupID.Visible = true;
            this.colGoodsGroupID.VisibleIndex = 3;
            this.colGoodsGroupID.Width = 82;
            // 
            // colPickerName
            // 
            this.colPickerName.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colPickerName.AppearanceCell.Options.UseFont = true;
            this.colPickerName.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10F);
            this.colPickerName.AppearanceHeader.Options.UseFont = true;
            this.colPickerName.Caption = "Người lấy hàng";
            this.colPickerName.FieldName = "PickerName";
            this.colPickerName.MinWidth = 50;
            this.colPickerName.Name = "colPickerName";
            this.colPickerName.Visible = true;
            this.colPickerName.VisibleIndex = 4;
            this.colPickerName.Width = 104;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2});
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(1240, 533);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.gridControlWIRDetail;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(1220, 513);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // IssueRequisitionControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl3);
            this.Controls.Add(this.layoutControl2);
            this.Controls.Add(this.layoutControl1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "IssueRequisitionControl";
            this.Size = new System.Drawing.Size(1240, 646);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEditWIRNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditWIRDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditWIRDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalQuantityByItem.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalQuantityByPack.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditHandlingStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditBranch.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWIRNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWIRDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalQuantityByItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalQuantityByPack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemBranch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemHandlingStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wRRDetailBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wARHOUSE_HPDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).EndInit();
            this.layoutControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlWIRDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewWIRDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraEditors.TextEdit textEditWIRNumber;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemWIRNumber;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.DateEdit dateEditWIRDate;
        private DevExpress.XtraEditors.TextEdit textEditTotalQuantityByItem;
        private DevExpress.XtraEditors.TextEdit textEditTotalQuantityByPack;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemWIRDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemTotalQuantityByItem;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemTotalQuantityByPack;
        private System.Windows.Forms.BindingSource wRRDetailBindingSource;
        private WARHOUSE_HPDataSet1 wARHOUSE_HPDataSet1;
        private WARHOUSE_HPDataSet1TableAdapters.WRRDetailTableAdapter wRRDetailTableAdapter;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditHandlingStatus;
        private DevExpress.XtraLayout.LayoutControl layoutControl3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraGrid.GridControl gridControlWIRDetail;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewWIRDetail;
        private DevExpress.XtraGrid.Columns.GridColumn colWIRNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colOrdinal;
        private DevExpress.XtraGrid.Columns.GridColumn colGoodsID;
        private DevExpress.XtraGrid.Columns.GridColumn colGoodsName;
        private DevExpress.XtraGrid.Columns.GridColumn colOtherGoodsName;
        private DevExpress.XtraGrid.Columns.GridColumn colPackingVolume;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalQuantity;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantityByItem;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantityByPack;
        private DevExpress.XtraGrid.Columns.GridColumn colLocationID;
        private DevExpress.XtraGrid.Columns.GridColumn colScanOption;
        private DevExpress.XtraGrid.Columns.GridColumn colNote;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colWIRDetailNo;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditBranch;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemBranch;
        private DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel windowsUIButtonPanel1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemHandlingStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colGoodsGroupID;
        private DevExpress.XtraGrid.Columns.GridColumn colPickerName;
    }
}
