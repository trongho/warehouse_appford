﻿using DevExpress.XtraBars;
using DevExpress.XtraBars.Docking2010;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraTab;
using DevExpress.XtraTab.ViewInfo;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WarehouseManager.Helpers;
using WarehouseManager.Models;
using WarehouseManager.Repository;

namespace WarehouseManager
{
    public partial class UserManagerControl : DevExpress.XtraEditors.XtraUserControl
    {
        UserRepository userRepository;
        UserRightRepository userRightRepository;
        RightRepository rightRepository;
        ScreenRepository screenRepository;
        UserRightOnScreenRepository userRightOnScreenRepository;
        ReportRepository reportRepository;
        UserRightOnReportRepository UserRightOnReportRepository;
        BranchRepository branchRepository;

        Int16 SaveNewRight = 0;
        Int16 SaveChangeRight = 0;
        Int16 DeleteRight = 0;
        Int16 PrintRight = 0;
        Int16 ImportRight = 0;
        Int16 ExportRight = 0;

        String lastUserID = "";

        List<Models.Screen> curentScreenUnderUserIDs = new List<Models.Screen>();


       
        public UserManagerControl()
        {
            InitializeComponent();
            updateLanguage();
            userRepository = new UserRepository();
            userRightRepository = new UserRightRepository();
            rightRepository = new RightRepository();
            screenRepository = new ScreenRepository();
            userRightOnScreenRepository = new UserRightOnScreenRepository();
            reportRepository = new ReportRepository();
            UserRightOnReportRepository = new UserRightOnReportRepository();
            branchRepository = new BranchRepository();

           
            this.Load += UserManagerControlLoad;

            textEditUserID.CausesValidation = false;
            textEditUsername.CausesValidation = false;
            textEditFullName.CausesValidation = false;
            textEditPassword.CausesValidation = false;
            textEditRePassword.CausesValidation = false;
        }

        private void UserManagerControlLoad(Object sender, EventArgs e)
        {
            windowsUIButtonPanel1.ButtonClick += button_Click;
            windowsUIButtonPanel1.Cursor = Cursors.Hand;
            windowsUIButtonPanel1.AppearanceButton.Pressed.ForeColor = System.Drawing.Color.DarkGray;
            windowsUIButtonPanel1.AppearanceButton.Hovered.ForeColor = System.Drawing.Color.DarkGreen;
            popupMenuSelectBranchAsync();
            popupMenuSelectPosition();
            popupMenuSelectRole();
            //checkGranRight();
            sbLoadDataForGridRightAsync();
            sbLoadDataForGridUserAsync();
            sbLoadDataForGridScreenAsync();
            sbLoadDataForGridReportAsync();
            customCheckEdit();

            gridViewUser.RowClick += gridViewUser_RowClickAsync;
            gridViewUser.CustomColumnDisplayText += userGridView_CustomColumnDisplayText;
            gridViewRight.CustomColumnDisplayText += rightGridView_CustomColumnDisplayText;
            gridViewScreen.CustomColumnDisplayText += screenGridView_CustomColumnDisplayText;
            gridViewReport.CustomColumnDisplayText += reportGridView_CustomColumnDisplayText;

            //this.repositoryItemCheckEdit1.QueryCheckStateByValue += new DevExpress.XtraEditors.Controls.QueryCheckStateByValueEventHandler(this.repositoryItemCheckEdit1_QueryCheckStateByValue);
            //gridViewRight.OptionsSelection.MultiSelect = true;
            //gridViewRight.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.RowSelect;
            //gridViewRight.MouseDown += setRight_MouseUp;

           
        }

        public async void getLastUserAsync()
        {
            lastUserID = await userRepository.GetLastUserID();
            if (lastUserID == "")
            {
                textEditUserID.Text = $"{1:D5}";
            }
            else
            {
                String userID = (int.Parse(lastUserID) + 1).ToString();
                textEditUserID.Text = $"{int.Parse(userID):D5}";
            }
        }

        private void updateLanguage()
        {
            ResourceManager rm;
            String cultureName = WMMessage.msgLanguage;
            if (cultureName.Equals("vi-VN"))
            {
                rm = new
                   ResourceManager("WarehouseManager.Resource.vi_local", typeof(UserManagerControl).Assembly);
            }
            else if(cultureName.Equals("en-US"))
            {
                rm = new
                    ResourceManager("WarehouseManager.Resource.en_local", typeof(UserManagerControl).Assembly);
            }
             else
            {
                rm = new
                    ResourceManager("WarehouseManager.Resource.cn_local", typeof(UserManagerControl).Assembly);
            }

            //WindowsUIButton buttonAdd = windowsUIButtonPanel1.Buttons[0] as WindowsUIButton;
            ((WindowsUIButton)windowsUIButtonPanel1.Buttons[0]).Caption = rm.GetString("addnew");
            ((WindowsUIButton)windowsUIButtonPanel1.Buttons[2]).Caption = rm.GetString("save");
            ((WindowsUIButton)windowsUIButtonPanel1.Buttons[4]).Caption = rm.GetString("delete");
            ((WindowsUIButton)windowsUIButtonPanel1.Buttons[6]).Caption = rm.GetString("search");
            ((WindowsUIButton)windowsUIButtonPanel1.Buttons[8]).Caption = rm.GetString("refesh");
            ((WindowsUIButton)windowsUIButtonPanel1.Buttons[10]).Caption = rm.GetString("import_data");
            ((WindowsUIButton)windowsUIButtonPanel1.Buttons[12]).Caption = rm.GetString("export_data");
            ((WindowsUIButton)windowsUIButtonPanel1.Buttons[14]).Caption = rm.GetString("print");
            ((WindowsUIButton)windowsUIButtonPanel1.Buttons[16]).Caption = rm.GetString("exit");

            layoutControlItemUserId.Text = rm.GetString("userid");
            layoutControlItemUsername.Text = rm.GetString("username");
            layoutControlItemPassword.Text = rm.GetString("password");
            layoutControlItemFullname.Text = rm.GetString("fullname");
            layoutControlItemDescription.Text = rm.GetString("description");
            layoutControlItemBranch.Text = rm.GetString("branch");
            layoutControlItemEmail.Text = rm.GetString("email");

            layoutControlItemAdress.Text = rm.GetString("adress");
            layoutControlItemPhone.Text = rm.GetString("phone");
        }

        private async void popupMenuSelectBranchAsync()
        {
            List<Branch> branches = await branchRepository.getAllBranchAsync();
            foreach (Branch branch in branches)
            {
                comboBoxEditBranch.Properties.Items.Add(branch.BranchName);
            }
            comboBoxEditBranch.SelectedIndex = 0;
        }

        private void popupMenuSelectRole()
        {
            List<String> roles = new List<string>();
            roles.Add("<>");
            roles.Add("Admin");
            roles.Add("User");
            foreach (String role in roles)
            {
                comboBoxEditRole.Properties.Items.Add(role);
            }
            comboBoxEditRole.SelectedIndex = 0;
        }
        private void popupMenuSelectPosition()
        {
            List<String> roles = new List<string>();
            roles.Add("<>");
            roles.Add("Giám đốc");
            roles.Add("Phó giám đốc");
            roles.Add("Trưởng phòng");
            roles.Add("Phó phòng");
            roles.Add("Nhân viên kho");
            foreach (String role in roles)
            {
                comboBoxEditPosition.Properties.Items.Add(role);
            }
            comboBoxEditPosition.SelectedIndex = 0;
        }




        private void customCheckEdit()
        {
            RepositoryItemCheckEdit checkEdit = new RepositoryItemCheckEdit();
            //gridControlScreen.RepositoryItems.Add(checkEdit);
            //gridControlReport.RepositoryItems.Add(checkEdit);
            //gridControlRight.RepositoryItems.Add(checkEdit);
            gridControlUser.RepositoryItems.Add(checkEdit);
            gridViewUser.Columns["Active"].ColumnEdit = checkEdit;
            gridViewUser.Columns["Blocked"].ColumnEdit = checkEdit;
            gridViewScreen.Columns["SaveNewRight"].ColumnEdit = checkEdit;
            gridViewScreen.Columns["SaveChangeRight"].ColumnEdit = checkEdit;
            gridViewScreen.Columns["DeleteRight"].ColumnEdit = checkEdit;
            gridViewScreen.Columns["PrintRight"].ColumnEdit = checkEdit;
            gridViewScreen.Columns["ImportRight"].ColumnEdit = checkEdit;
            gridViewScreen.Columns["ExportRight"].ColumnEdit = checkEdit;
            gridViewReport.Columns["ViewRight"].ColumnEdit = checkEdit;
            gridViewReport.Columns["PrintRight"].ColumnEdit = checkEdit;
            gridViewRight.Columns["GrantRight"].ColumnEdit = checkEdit;
            checkEdit.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.UserDefined;
            checkEdit.PictureChecked = Helpers.ImageHelper.resizeImage(Properties.Resources.active, new System.Drawing.Size(15, 15));
            checkEdit.PictureUnchecked = Helpers.ImageHelper.resizeImage(Properties.Resources.blocked, new System.Drawing.Size(15, 15));

            checkEdit.CheckedChanged += CheckEdit1_CheckedChanged1;

            checkEdit.QueryCheckStateByValue += new DevExpress.XtraEditors.Controls.QueryCheckStateByValueEventHandler(this.repositoryItemCheckEdit1_QueryCheckStateByValue);
        }

        private void CheckEdit1_CheckedChanged1(object sender, EventArgs e)
        {
            var edit = (CheckEdit)sender;
            //XtraMessageBox.Show(string.Format("CheckEdit Value: {0}", edit.EditValue));
           

            //MessageBox.Show(edit.EditValue + "");


        }


        private void repositoryItemCheckEdit1_QueryCheckStateByValue(object sender, DevExpress.XtraEditors.Controls.QueryCheckStateByValueEventArgs e)
        {
            string val = "";
            if (e.Value != null)
            {
                val = e.Value.ToString();
            }
            else
            {
                val = "False";//The default is not selected   
            }
            switch (val)
            {
                case "True":
                    e.CheckState = CheckState.Checked;
                    break;
                case "Yes":
                    e.CheckState = CheckState.Checked;
                    break;
                case "1":
                    e.CheckState = CheckState.Checked;
                    break;
                case "False":
                    e.CheckState = CheckState.Unchecked;
                    break;
                case "No":
                    e.CheckState = CheckState.Unchecked;
                    break;
                case "0":
                    e.CheckState = CheckState.Unchecked;
                    break;
                default:
                    e.CheckState = CheckState.Unchecked;
                    break;
            }
            e.Handled = true;
        }

        private async void sbLoadDataForGridUserAsync()
        {
            gridControlUser.DataSource = await userRepository.getAllUserAsync();
        }

        private async void sbLoadDataForGridRightAsync()
        {          
            gridControlRight.DataSource = (await rightRepository.getAllRightAsync()).OrderBy(x=>x.Ordinal);
        }

        private async void sbLoadDataForGridScreenAsync()
        {
            gridControlScreen.DataSource = (await screenRepository.getAllScreenAsync()).OrderBy(x => x.Ordinal);

        }
        private async void sbLoadDataForGridReportAsync()
        {
            gridControlReport.DataSource = (await reportRepository.getAllReportAsync()).OrderBy(x => x.Ordinal);

        }

        private async void sbLoadDataForGridScreenUnderUserIDAsync(String userID)
        {
            List<Models.Screen> screens = await getListScreenFromUserIdAsync(userID);
            gridControlScreen.DataSource = screens.OrderBy(x => x.Ordinal);

        }
        private async void sbLoadDataForGridReportUnderUserIDAsync(String userID)
        {
            List<Report> reports = await getListReportFromUserIdAsync(userID);
            gridControlReport.DataSource = reports.OrderBy(x => x.Ordinal);

        }
        private async void sbLoadDataForGridRightUnderUserIDAsync(String userID)
        {
            List<Right> rights = await getListRightFromUserIdAsync(userID);
            gridControlRight.DataSource = rights.OrderBy(x => x.Ordinal);

        }

        private void userGridView_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {

            if (e.Column == colNo2)
            {
                int rowHandle = gridViewUser.GetRowHandle(e.ListSourceRowIndex);
                if (rowHandle >= 0)
                {
                    e.DisplayText = (rowHandle + 1).ToString();
                }
            }
        }

        private void rightGridView_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {

            if (e.Column == colNo)
            {
                int rowHandle = gridViewRight.GetRowHandle(e.ListSourceRowIndex);
                if (rowHandle >= 0)
                {
                    e.DisplayText = (rowHandle + 1).ToString();
                }
            }
        }

        private void screenGridView_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {

            if (e.Column == colNo3)
            {
                int rowHandle = gridViewScreen.GetRowHandle(e.ListSourceRowIndex);
                if (rowHandle >= 0)
                {
                    e.DisplayText = (rowHandle + 1).ToString();
                }
            }
        }

        private void reportGridView_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {

            if (e.Column == colNo4)
            {
                int rowHandle = gridViewReport.GetRowHandle(e.ListSourceRowIndex);
                if (rowHandle >= 0)
                {
                    e.DisplayText = (rowHandle + 1).ToString();
                }
            }
        }

        private void clearTextAsync()
        {
            textEditUsername.Text = "";
            textEditFullName.Text = "";
            textEditEmail.Text = "";
            textEditPhone.Text = "";
            textEditAdress.Text = "";
            comboBoxEditPosition.SelectedIndex = 0;
            textEditPassword.Text = "";
            textEditRePassword.Text = "";
            textEditDescription.Text = "";
            comboBoxEditBranch.SelectedIndex = 0;
            comboBoxEditRole.SelectedIndex = 0;
            checkEditActive.Checked = true;
            checkEditBlocked.Checked = false;
        }

        public async void createOrUpdateUser()
        {
            List<User> users = await userRepository.getUserUnderUserIDAsync(textEditUserID.Text);
            if (users.Count==0)
            {
                User user = new User();
                user.Role = comboBoxEditRole.SelectedItem.ToString();
                user.BranchID =comboBoxEditBranch.SelectedIndex==0?"<>":$"{int.Parse(comboBoxEditBranch.SelectedIndex.ToString()) - 1:D3}";
                user.BranchName = comboBoxEditBranch.SelectedItem.ToString();
                user.UserName = textEditUsername.Text;
                user.Password = textEditPassword.Text;
                user.LoginName = textEditFullName.Text;
                user.Email = textEditEmail.Text;
                user.Adress = textEditAdress.Text;
                user.Position = comboBoxEditPosition.Text;
                user.UserID = textEditUserID.Text;
                user.CreatedUserID = WMMessage.User.UserID;
                user.CreateDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));
                user.Active = checkEditActive.Checked;
                user.Blocked = checkEditBlocked.Checked;

                if (WMPublic.sbMessageSaveNewRequest(this)==true)
                {
                    if (await userRepository.createUserAsync(user) > 0)
                    {
                        createUserRightOnScreen();
                        createUserRightOnReport();
                        createUserRightAsync();
                        sbLoadDataForGridUserAsync();

                        WMPublic.sbMessageCreateUserSuccess(this);
                    }
                }
            }
            else
            {
                users[0].Role = comboBoxEditRole.SelectedItem.ToString();
                users[0].BranchID = comboBoxEditBranch.SelectedIndex == 0 ? "<>" : $"{int.Parse(comboBoxEditBranch.SelectedIndex.ToString()) - 1:D3}";
                users[0].BranchName = comboBoxEditBranch.SelectedItem.ToString();
                users[0].UserName = textEditUsername.Text;
                users[0].LoginName = textEditFullName.Text;
                users[0].Email = textEditEmail.Text;
                users[0].Adress = textEditAdress.Text;
                users[0].Position = comboBoxEditPosition.Text;
                if (!textEditPassword.Text.Equals(""))
                {
                    users[0].Password = textEditPassword.Text;
                }
                users[0].UpdatedUserID = WMMessage.User.UserID;
                users[0].UpdatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));
                users[0].Active = checkEditActive.Checked;
                users[0].Blocked = checkEditBlocked.Checked;

                if (WMPublic.sbMessageSaveChangeRequest(this) == true)
                {
                    if (await userRepository.updateUserAsync(users[0], textEditUserID.Text) > 0)
                    {
                        updateUserRightOnScreenAsync();
                        updateUserRightOnReportAsync();
                        updateUserRightAsync();
                        sbLoadDataForGridUserAsync();

                        WMPublic.sbMessageUpdateUserSuccess(this);
                    }
                }
            }
            
        }

        public async void DeleteUserAsync()
        {
            if ((await userRepository.DeleteDataAsync(textEditUserID.Text)) > 0)
            {
                DeleteUserRightOnScreenAsync();
                DeleteUserRightOnReportAsync();
                DeleteUserRightAsync();
                sbLoadDataForGridUserAsync();
                sbLoadDataForGridRightAsync();
                sbLoadDataForGridScreenAsync();
                sbLoadDataForGridReportAsync();
                WMPublic.sbMessageDeleteUserSuccess(this);
            }
            else
            {
                MessageBox.Show("Xóa người dùng ko thành công");
            }
        }

        //create right on screen
        public async void createUserRightOnScreen()
        {
            int num = gridViewScreen.RowCount - 1;
            int i = 0;
            while (true)
            {
                int num2 = i;
                int num3 = num;
                if (num2 > num3 || gridViewScreen.GetRowCellValue(i, gridViewScreen.Columns["colScreenID"]) == "")
                {
                    break;
                }
                int rowHandle = gridViewScreen.GetRowHandle(i);
                UserRightOnScreen userRightOnScreen = new UserRightOnScreen();
                userRightOnScreen.UserID = textEditUserID.Text;
                userRightOnScreen.Ordinal = (short?)(i + 1);
                userRightOnScreen.ScreenID = (string)gridViewScreen.GetRowCellValue(rowHandle, "ScreenID");
                userRightOnScreen.ScreenName = (string)gridViewScreen.GetRowCellValue(rowHandle, "ScreenName");
                userRightOnScreen.ScreenNameEN = (string)gridViewScreen.GetRowCellValue(rowHandle, "ScreenNameEN");
                userRightOnScreen.SaveNewRight = (Int16?)gridViewScreen.GetRowCellValue(rowHandle, "SaveNewRight");
                userRightOnScreen.SaveChangeRight = (Int16?)gridViewScreen.GetRowCellValue(rowHandle, "SaveChangeRight");
                userRightOnScreen.DeleteRight = (Int16?)gridViewScreen.GetRowCellValue(rowHandle, "DeleteRight");
                userRightOnScreen.PrintRight = (Int16?)gridViewScreen.GetRowCellValue(rowHandle, "PrintRight");
                userRightOnScreen.ImportRight = (Int16?)gridViewScreen.GetRowCellValue(rowHandle, "ImportRight");
                userRightOnScreen.ExportRight = (Int16?)gridViewScreen.GetRowCellValue(rowHandle, "ExportRight");
                if (await userRightOnScreenRepository.createUserRightOnscreenAsync(userRightOnScreen) > 0)
                {
                    i++;
                }
            }
        }


        //update right on screen
        public async void updateUserRightOnScreenAsync()
        {

            int num = gridViewScreen.RowCount - 1;
            int i = 0;
            while (true)
            {
                int num2 = i;
                int num3 = num;
                if (num2 > num3 || gridViewScreen.GetRowCellValue(i, gridViewScreen.Columns["colScreenID"]) == "")
                {
                    break;
                }
                int rowHandle = gridViewScreen.GetRowHandle(i);
                string userID = textEditUserID.Text;
                string screenID= (string)gridViewScreen.GetRowCellValue(rowHandle, "ScreenID");
                List<UserRightOnScreen> userRightOnScreens = await userRightOnScreenRepository.getUserRightOnScreenAsync(userID, screenID);
                if (userRightOnScreens.Count > 0)
                {
                    userRightOnScreens[0].SaveNewRight = (Int16?)gridViewScreen.GetRowCellValue(rowHandle, "SaveNewRight");
                    userRightOnScreens[0].SaveChangeRight = (Int16?)gridViewScreen.GetRowCellValue(rowHandle, "SaveChangeRight");
                    userRightOnScreens[0].DeleteRight = (Int16?)gridViewScreen.GetRowCellValue(rowHandle, "DeleteRight");
                    userRightOnScreens[0].PrintRight = (Int16?)gridViewScreen.GetRowCellValue(rowHandle, "PrintRight");
                    userRightOnScreens[0].ImportRight = (Int16?)gridViewScreen.GetRowCellValue(rowHandle, "ImportRight");
                    userRightOnScreens[0].ExportRight = (Int16?)gridViewScreen.GetRowCellValue(rowHandle, "ExportRight");
                    if (await userRightOnScreenRepository.updateUserRightOnScreen(userRightOnScreens[0], userID, screenID) > 0)
                    {

                        i++;
                    }
                }
                else
                {
                    UserRightOnScreen userRightOnScreen = new UserRightOnScreen();
                    userRightOnScreen.UserID = textEditUserID.Text;
                    userRightOnScreen.Ordinal = (short?)(i + 1);
                    userRightOnScreen.ScreenID = (string)gridViewScreen.GetRowCellValue(rowHandle, "ScreenID");
                    userRightOnScreen.ScreenName = (string)gridViewScreen.GetRowCellValue(rowHandle, "ScreenName");
                    userRightOnScreen.ScreenNameEN = (string)gridViewScreen.GetRowCellValue(rowHandle, "ScreenNameEN");
                    userRightOnScreen.SaveNewRight = (Int16?)gridViewScreen.GetRowCellValue(rowHandle, "SaveNewRight");
                    userRightOnScreen.SaveChangeRight = (Int16?)gridViewScreen.GetRowCellValue(rowHandle, "SaveChangeRight");
                    userRightOnScreen.DeleteRight = (Int16?)gridViewScreen.GetRowCellValue(rowHandle, "DeleteRight");
                    userRightOnScreen.PrintRight = (Int16?)gridViewScreen.GetRowCellValue(rowHandle, "PrintRight");
                    userRightOnScreen.ImportRight = (Int16?)gridViewScreen.GetRowCellValue(rowHandle, "ImportRight");
                    userRightOnScreen.ExportRight = (Int16?)gridViewScreen.GetRowCellValue(rowHandle, "ExportRight");
                    if (await userRightOnScreenRepository.createUserRightOnscreenAsync(userRightOnScreen) > 0)
                    {
                        i++;
                    }
                }
            }
        }

        public async void DeleteUserRightOnScreenAsync()
        {
            List<Models.Screen> screens = await getListScreenFromUserIdAsync(textEditUserID.Text);
            foreach (Models.Screen screen in screens)
            {
                if ((await userRightOnScreenRepository.DeleteDataAsync(textEditUserID.Text, screen.ScreenID) > 0))
                {
                    
                }
            }
        }

        //create right on report
        public async void createUserRightOnReport()
        {
            int num = gridViewReport.RowCount - 1;
            int i = 0;
            while (true)
            {
                int num2 = i;
                int num3 = num;
                if (num2 > num3 || gridViewReport.GetRowCellValue(i, gridViewReport.Columns["colReportID"]) == "")
                {
                    break;
                }
                int rowHandle = gridViewReport.GetRowHandle(i);
                UserRightOnReport userRightOnReport = new UserRightOnReport();
                userRightOnReport.UserID = textEditUserID.Text;
                userRightOnReport.Ordinal = (short?)(i + 1);
                userRightOnReport.ReportID = (string)gridViewReport.GetRowCellValue(rowHandle, "ReportID");
                userRightOnReport.ReportName = (string)gridViewReport.GetRowCellValue(rowHandle, "ReportName");
                userRightOnReport.ReportNameEN = (string)gridViewReport.GetRowCellValue(rowHandle, "ReportNameEN");
                userRightOnReport.ViewRight = (Int16)gridViewReport.GetRowCellValue(rowHandle, "ViewRight");
                userRightOnReport.PrintRight = (Int16)gridViewReport.GetRowCellValue(rowHandle, "PrintRight");
                if (await UserRightOnReportRepository.createUserRightOnReportAsync(userRightOnReport) > 0)
                {
                    i++;
                }
            }
        }


        //update right on screen
        public async void updateUserRightOnReportAsync()
        {

            int num = gridViewReport.RowCount - 1;
            int i = 0;
            while (true)
            {
                int num2 = i;
                int num3 = num;
                if (num2 > num3 || gridViewReport.GetRowCellValue(i, gridViewReport.Columns["colReportID"]) == "")
                {
                    break;
                }
                int rowHandle = gridViewReport.GetRowHandle(i);
                string userID = textEditUserID.Text;
                string reportID = (string)gridViewReport.GetRowCellValue(rowHandle, "ReportID");
                List<UserRightOnReport> userRightOnReports = await UserRightOnReportRepository.getUserRightOnReportAsync(userID,reportID);
                if (userRightOnReports.Count > 0)
                {
                    userRightOnReports[0].ViewRight = (Int16)gridViewReport.GetRowCellValue(rowHandle, "ViewRight");
                    userRightOnReports[0].PrintRight = (Int16)gridViewReport.GetRowCellValue(rowHandle, "PrintRight");
                    if (await UserRightOnReportRepository.updateUserRightOnReport(userRightOnReports[0], userID, reportID) > 0)
                    {
                        i++;
                    }
                }
                else
                {
                    UserRightOnReport userRightOnReport = new UserRightOnReport();
                    userRightOnReport.UserID = textEditUserID.Text;
                    userRightOnReport.Ordinal = (short?)(i + 1);
                    userRightOnReport.ReportID = (string)gridViewReport.GetRowCellValue(rowHandle, "ReportID");
                    userRightOnReport.ReportName = (string)gridViewReport.GetRowCellValue(rowHandle, "ReportName");
                    userRightOnReport.ReportNameEN = (string)gridViewReport.GetRowCellValue(rowHandle, "ReportNameEN");
                    userRightOnReport.ViewRight = (Int16)gridViewReport.GetRowCellValue(rowHandle, "ViewRight");
                    userRightOnReport.PrintRight = (Int16)gridViewReport.GetRowCellValue(rowHandle, "PrintRight");
                    if (await UserRightOnReportRepository.createUserRightOnReportAsync(userRightOnReport) > 0)
                    {
                        i++;
                    }
                }

            }
        }

        public async void DeleteUserRightOnReportAsync()
        {
            List<Report> reports = await getListReportFromUserIdAsync(textEditUserID.Text);
            foreach (Report report in reports)
            {
                if ((await UserRightOnReportRepository.DeleteUserRightOnReportAsync(textEditUserID.Text, report.ReportID) > 0))
                {
                    
                }
                
            }
        }

        //create user right
        public async void createUserRightAsync()
        {
            int num = gridViewRight.RowCount - 1;
            int i = 0;
            while (true)
            {
                int num2 = i;
                int num3 = num;
                if (num2 > num3 || gridViewRight.GetRowCellValue(i, gridViewRight.Columns["colRightID"]) == "")
                {
                    break;
                }
                int rowHandle = gridViewRight.GetRowHandle(i);
                UserRight userRight = new UserRight();
                userRight.UserID = textEditUserID.Text;
                userRight.Ordinal = (short?)(i + 1);
                userRight.RightID = (string)gridViewRight.GetRowCellValue(rowHandle, "RightID");
                userRight.RightName = (string)gridViewRight.GetRowCellValue(rowHandle, "RightName");
                userRight.RightNameEN = (string)gridViewRight.GetRowCellValue(rowHandle, "RightNameEN");
                userRight.GrantRight = (Int16)gridViewRight.GetRowCellValue(rowHandle, "GrantRight");
                if (await userRightRepository.createUserRightAsync(userRight) > 0)
                {
                    i++;
                }
            }
        }

        public async void updateUserRightAsync()
        {

            int num = gridViewRight.RowCount - 1;
            int i = 0;
            while (true)
            {
                int num2 = i;
                int num3 = num;
                if (num2 > num3 || gridViewRight.GetRowCellValue(i, gridViewRight.Columns["colRightID"]) == "")
                {
                    break;
                }
                int rowHandle = gridViewRight.GetRowHandle(i);
                string userID = textEditUserID.Text;
                string rightID = (string)gridViewRight.GetRowCellValue(rowHandle, "RightID");
                List<UserRight> userRights = await userRightRepository.getUserRightAsync(userID,rightID);
                if (userRights.Count > 0)
                {
                    userRights[0].GrantRight = (Int16?)gridViewRight.GetRowCellValue(rowHandle, "GrantRight");
                    if (await userRightRepository.updateUserRight(userRights[0], userID, rightID) > 0)
                    {
                        i++;
                    }
                }
                else
                {
                    UserRight userRight = new UserRight();
                    userRight.UserID = textEditUserID.Text;
                    userRight.Ordinal = (short?)(i + 1);
                    userRight.RightID = (string)gridViewRight.GetRowCellValue(rowHandle, "RightID");
                    userRight.RightName = (string)gridViewRight.GetRowCellValue(rowHandle, "RightName");
                    userRight.RightNameEN = (string)gridViewRight.GetRowCellValue(rowHandle, "RightNameEN");
                    userRight.GrantRight = (Int16)gridViewRight.GetRowCellValue(rowHandle, "GrantRight");
                    if (await userRightRepository.createUserRightAsync(userRight) > 0)
                    {
                        i++;
                    }
                }
            }
        }

        public async void DeleteUserRightAsync()
        {
            List<Right> rights = await getListRightFromUserIdAsync(textEditUserID.Text);
            foreach (Right right in rights)
            {
                if ((await userRightRepository.DeleteDataAsync(textEditUserID.Text, right.RightID) > 0))
                {
                    
                }
               
            }
        }

        private async Task<List<Models.Screen>> getListScreenFromUserIdAsync(String userID)
        {
            List<Models.Screen> screens = await screenRepository.getAllScreenAsync();
            List<Models.Screen> screensNew = new List<Models.Screen>();
            List<UserRightOnScreen> userRightOnScreens = await userRightOnScreenRepository.getUserRightOnScreenAsync(userID);
            if (userRightOnScreens.Count > 0)
            {
                foreach (UserRightOnScreen userRightOnScreen in userRightOnScreens)
                {
                    foreach (Models.Screen screen1 in screens)
                    {
                        if (userRightOnScreen.ScreenID.Equals(screen1.ScreenID))
                        {
                            screen1.SaveNewRight = userRightOnScreen.SaveNewRight;
                            screen1.SaveChangeRight = userRightOnScreen.SaveChangeRight;
                            screen1.DeleteRight = userRightOnScreen.DeleteRight;
                            screen1.PrintRight = userRightOnScreen.PrintRight;
                            screen1.ImportRight = userRightOnScreen.ImportRight;
                            screen1.ExportRight = userRightOnScreen.ExportRight;
                            screensNew.Add(screen1);
                        }
                    }

                }
            }
            else
            {
                screensNew = screens;
            }
           
            return screensNew;
        }

        private async Task<List<Report>> getListReportFromUserIdAsync(String userID)
        {
            List<Report> reports = await reportRepository.getAllReportAsync();
            List<Report> reportsNew = new List<Report>();
            List<UserRightOnReport> userRightOnReports = await UserRightOnReportRepository.getUserRightOnReportAsync(userID);
            if (userRightOnReports.Count > 0)
            {
                foreach (UserRightOnReport userRightOnReport in userRightOnReports)
                {
                    foreach (Report report1 in reports)
                    {
                        if (userRightOnReport.ReportID.Equals(report1.ReportID))
                        {
                            report1.ViewRight = userRightOnReport.ViewRight;
                            report1.PrintRight = userRightOnReport.PrintRight;
                            reportsNew.Add(report1);
                        }
                    }
                }
            }
            else
            {
                reportsNew = reports;
            }
            return reportsNew;
        }

        private async Task<List<Right>> getListRightFromUserIdAsync(String userID)
        {
            List<Right> rights = await rightRepository.getAllRightAsync();
            List<Right> rightsNew = new List<Right>();
            List<UserRight> userRights = await userRightRepository.getUserRightByUserIDAsync(userID);
            if (userRights.Count > 0)
            {
                foreach (UserRight userRight in userRights)
                {
                    foreach (Right right1 in rights)
                    {
                        if (userRight.RightID.Equals(right1.RightID))
                        {
                            right1.GrantRight = userRight.GrantRight;
                            rightsNew.Add(right1);
                        }
                    }
                }
            }
            else
            {
                rightsNew = rights;
            }
            return rightsNew;
        }

        private void gridViewUser_RowClickAsync(Object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            string userName = (string)(sender as GridView).GetFocusedRowCellValue("UserName");
            string userID = (string)(sender as GridView).GetFocusedRowCellValue("UserID");
            string loginName = (string)(sender as GridView).GetFocusedRowCellValue("LoginName");
            string email = (string)(sender as GridView).GetFocusedRowCellValue("Email");
            string adress = (string)(sender as GridView).GetFocusedRowCellValue("Adress");
            string position = (string)(sender as GridView).GetFocusedRowCellValue("Position");
            string branchName = (string)(sender as GridView).GetFocusedRowCellValue("BranchName");
            string role = (string)(sender as GridView).GetFocusedRowCellValue("Role");
            bool active = (bool)(sender as GridView).GetFocusedRowCellValue("Active");
            bool blocked = (bool)(sender as GridView).GetFocusedRowCellValue("Blocked");


            textEditUserID.Text = userID;
            textEditUsername.Text = userName;
            textEditFullName.Text = loginName;
            textEditEmail.Text = email;
            textEditAdress.Text = adress;
            comboBoxEditPosition.Text = position;
            comboBoxEditBranch.Text = branchName;
            comboBoxEditRole.Text = role;
            checkEditActive.Checked = active;
            checkEditBlocked.Checked = blocked;
            textEditPassword.Text = "";
            textEditRePassword.Text = "";

            sbLoadDataForGridScreenUnderUserIDAsync(userID);
            sbLoadDataForGridRightUnderUserIDAsync(userID);
            sbLoadDataForGridReportUnderUserIDAsync(userID);

        }

        private void gridView_KeyUp(Object sender, KeyEventArgs e)
        {

            String userID = (string)(sender as GridView).GetFocusedRowCellValue("UserID");
            String username = (string)(sender as GridView).GetFocusedRowCellValue("UserName");
            String fullname = (string)(sender as GridView).GetFocusedRowCellValue("LoginName");
            String email = (string)(sender as GridView).GetFocusedRowCellValue("Email");
            String adress = (string)(sender as GridView).GetFocusedRowCellValue("Adress");
            String position = (string)(sender as GridView).GetFocusedRowCellValue("Position");

            textEditUserID.Text = userID;
            textEditUsername.Text = username;
            textEditFullName.Text = fullname;
            textEditEmail.Text = email;
            textEditAdress.Text = adress;
            comboBoxEditPosition.Text = position;
            textEditPassword.Text = "";

        }


        //check grant right
        private async Task checkGranRight()
        {
            String userId = WMMessage.User.UserID;
            List<Right> rights = await rightRepository.getAllRightAsync();
            bool exitLoop = false;
            foreach (Right right in rights)
            {
                List<UserRight> userRight = await userRightRepository.getUserRightAsync(userId, right.RightID);
                if (userRight.Count>0)
                {
                    Int16 grantRight = (Int16)userRight[0].GrantRight;
                    switch (right.RightID)
                    {
                        case "AdjustGGD":
                            if (grantRight == 0)
                                ((WindowsUIButton)windowsUIButtonPanel1.Buttons[0]).Enabled = false;
                            exitLoop = false;
                            break;
                        case "AdjustIO":
                            if (grantRight == 0)
                                ((WindowsUIButton)windowsUIButtonPanel1.Buttons[2]).Enabled = false;
                            exitLoop = false;
                            break;
                    }
                }
                if (exitLoop) break;
            }
        }


        //barbutton click
        public async void button_Click(object sender, EventArgs e)
        {
            ButtonEventArgs eArg = (DevExpress.XtraBars.Docking2010.ButtonEventArgs)e;
            String tag = eArg.Button.Properties.Tag.ToString();
            switch (tag)
            {
                case "new":
                    if (WMPublic.sbMessageAddNewRequest(this) == true)
                    {
                        getLastUserAsync();
                        clearTextAsync();
                    }

                    break;
                case "save":
                    textEditUserID.DoValidate();
                    textEditUsername.DoValidate();
                    textEditFullName.DoValidate();
                    textEditPassword.DoValidate();
                    textEditRePassword.DoValidate();
                    createOrUpdateUser();
                    break;
                case "delete":
                    DeleteUserAsync();
                    break;
                case "search":
                    break;
                case "refesh":
                    sbLoadDataForGridUserAsync();
                    break;
                case "import":

                    break;
                case "export":
                    break;
                case "print":
                    break;
                case "close":
                    break;
            }

        }


        private void comboBoxEditRole_SelectedValueChanged(object sender, EventArgs e)
        {
            EnumConverter ec = TypeDescriptor.GetConverter(typeof(FontStyle)) as EnumConverter;
            var cBox = sender as ComboBoxEdit;
            if (cBox.SelectedIndex != -1)
            {
                if (cBox.SelectedText.Equals("Admin"))
                {
                    sbLoadDataForGridRightAdmin();
                    sbLoadDataForGridScreenAdmin();
                    sbLoadDataForGridReportAdmin();
                }
                else
                {
                    sbLoadDataForGridRightAsync();
                    sbLoadDataForGridScreenAsync();
                    sbLoadDataForGridReportAsync();
                }

            }
        }

        private async void sbLoadDataForGridRightAdmin()
        {
            List<Right> rights = await rightRepository.getAllRightAsync();
            List<Right> rights1 = new List<Right>();
            foreach (Right right in rights)
            {
                right.GrantRight = 1;
                rights1.Add(right);
            }
            gridControlRight.DataSource = rights1;
        }
        private async void sbLoadDataForGridScreenAdmin()
        {
            List<Models.Screen> screens = await screenRepository.getAllScreenAsync();
            List<Models.Screen> screens1 = new List<Models.Screen>();
            foreach (Models.Screen screen in screens)
            {
                screen.DeleteRight =1;
                screen.ExportRight = 1;
                screen.ImportRight = 1;
                screen.PrintRight = 1;
                screen.SaveChangeRight =1;
                screen.SaveNewRight = 1;
                screens1.Add(screen);
            }
            gridControlScreen.DataSource = screens1;
        }
        private async void sbLoadDataForGridReportAdmin()
        {
            List<Report> reports = await reportRepository.getAllReportAsync();
            List<Report> reports1 = new List<Report>();
            foreach (Report report in reports)
            {
                report.PrintRight = 1;
                report.ViewRight = 1;
                reports1.Add(report);
            }
            gridControlReport.DataSource = reports1;
        }

        private void textEditUserID_Validating(object sender, CancelEventArgs e)
        {
            TextEdit textEdit = sender as TextEdit;
            string editValue = textEdit.EditValue as string;
            if (editValue == "")
            {
                textEdit.ErrorText = "Không được để trống";
                textEdit.ErrorImageOptions.Image = svgImageCollection1.GetImage(0, new System.Drawing.Size(12, 12));
                e.Cancel = true;
            }
        }

        private async void textEditUsername_Validating(object sender, CancelEventArgs e)
        {
            TextEdit textEdit = sender as TextEdit;
            string editValue = textEdit.EditValue as string;
            List<User> users = await userRepository.getUserUnderUsernameAsync(textEditUsername.Text);
           
            if (editValue == "")
            {
                textEdit.ErrorText = "Không được để trống";
                textEdit.ErrorImageOptions.Image = svgImageCollection1.GetImage(0, new System.Drawing.Size(12, 12));
                e.Cancel = true;
            }
            else if (users.Count > 0)
            {
                textEdit.ErrorText = "Tên đăng nhập đã tồn tại";
                textEdit.ErrorImageOptions.Image = svgImageCollection1.GetImage(0, new System.Drawing.Size(12, 12));
                e.Cancel = true;
            }
        }

        private void textEditPassword_Validating(object sender, CancelEventArgs e)
        {
            TextEdit textEdit = sender as TextEdit;
            string editValue = textEdit.EditValue as string;
            if (editValue == "")
            {
                textEdit.ErrorText = "Không được để trống";
                textEdit.ErrorImageOptions.Image = svgImageCollection1.GetImage(0, new System.Drawing.Size(12, 12));
                e.Cancel = true;
            }
            else if (editValue.Length < 6)
            {
                textEdit.ErrorText = "Chiều dài mật khẩu >=6 ký tự";
                textEdit.ErrorImageOptions.Image = svgImageCollection1.GetImage(0, new System.Drawing.Size(12, 12));
                e.Cancel = true;
            }
        }

        private void textEditRePassword_Validating(object sender, CancelEventArgs e)
        {
            TextEdit textEdit = sender as TextEdit;
            string editValue = textEdit.EditValue as string;
            if (editValue == "")
            {
                textEdit.ErrorText = "Không được để trống";
                textEdit.ErrorImageOptions.Image = svgImageCollection1.GetImage(0, new System.Drawing.Size(12, 12));
                e.Cancel = true;
            }
            else if (editValue.Length < 6)
            {
                textEdit.ErrorText = "Chiều dài mật khẩu >=6 ký tự";
                textEdit.ErrorImageOptions.Image = svgImageCollection1.GetImage(0, new System.Drawing.Size(12, 12));
                e.Cancel = true;
            }
            else if (!editValue.Equals(textEditPassword.Text))
            {
                textEdit.ErrorText = "Nhập lại mật khẩu mới không đúng";
                textEdit.ErrorImageOptions.Image = svgImageCollection1.GetImage(0, new System.Drawing.Size(12, 12));
                e.Cancel = true;
            }
        }

        private void textEditFullName_Validating(object sender, CancelEventArgs e)
        {
            TextEdit textEdit = sender as TextEdit;
            string editValue = textEdit.EditValue as string;
            if (editValue =="")
            {
                textEdit.ErrorText = "Không được để trống";
                textEdit.ErrorImageOptions.Image = svgImageCollection1.GetImage(0, new System.Drawing.Size(12, 12));
                e.Cancel = true;
            }
        }
    }
}
