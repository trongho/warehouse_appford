﻿using DevExpress.XtraBars.Docking2010;
using DevExpress.XtraGrid.Views.Grid;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WarehouseManager.Models;
using WarehouseManager.Repository;
using LicenseContext = OfficeOpenXml.LicenseContext;

namespace WarehouseManager
{

    public partial class InventoryDataControl : DevExpress.XtraEditors.XtraUserControl
    {
        InventoryDataRepository inventoryDataRepository;
        GoodsDataRepository goodsDataRepository;
        int sumAddNew = 0;
        int sumUpdate = 0;
        public InventoryDataControl()
        {
            InitializeComponent();
            inventoryDataRepository = new InventoryDataRepository();
            goodsDataRepository = new GoodsDataRepository();
            this.Load += InventoryDataControl_Load;
        }

        private void InventoryDataControl_Load(Object sender, EventArgs e)
        {
            gridViewData.ClearSorting();
            windowsUIButtonPanel1.ButtonClick += button_Click;
            gridViewData.RowClick += gridView_RowClick;
            gridViewData.CustomColumnDisplayText += GridView_CustomColumnDisplayText;
            sbLoadDataForGridAsync();
        }

        private void GridView_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {

            if (e.Column == colNo)
            {
                int rowHandle = gridViewData.GetRowHandle(e.ListSourceRowIndex);
                if (rowHandle >= 0)
                {
                    e.DisplayText = (rowHandle + 1).ToString();
                }
            }
        }

        private async Task CreatAsync()
        {
            InventoryData inventoryData = new InventoryData();
            if (textEditGoodsID.Text.Equals(""))
            {
                return;
            }
            inventoryData.GoodsID = textEditGoodsID.Text;
            inventoryData.GoodsName = textEditGoodsName.Text;
            inventoryData.LocationID = "";
            inventoryData.Description = textEditDescription.Text;
            inventoryData.Status = "New";
            inventoryData.Quantity = Decimal.Parse(textEditQuantity.Text);
            inventoryData.Amount = Decimal.Parse(textEditAmount.Text);
            inventoryData.CreatedUserID = WMMessage.User.UserID;
            inventoryData.CreatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));
            if (await inventoryDataRepository.Create(inventoryData) > 0)
            {
                WMPublic.sbMessageSaveNewSuccess(this);
                sbLoadDataForGridAsync();
            }
        }
        private async Task UpdateAsync()
        {
            if (textEditGoodsID.Text.Equals(""))
            {
                return;
            }
            InventoryData inventoryData = await inventoryDataRepository.GetUnderID(textEditGoodsID.Text);
            inventoryData.GoodsID = textEditGoodsID.Text;
            inventoryData.GoodsName = textEditGoodsName.Text;
            inventoryData.LocationID = "";
            inventoryData.Description = textEditDescription.Text;
            inventoryData.Status = "Updated";
            inventoryData.Quantity = Decimal.Parse(textEditQuantity.Text);
            inventoryData.Amount = Decimal.Parse(textEditAmount.Text);
            inventoryData.UpdatedUserID = WMMessage.User.UserID;
            inventoryData.UpdatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));
            if (await inventoryDataRepository.Update(inventoryData, textEditGoodsID.Text) > 0)
            {
                WMPublic.sbMessageSaveChangeSuccess(this);
                sbLoadDataForGridAsync();
            }
        }

        private async Task DeleteAsync()
        {
            if (await inventoryDataRepository.Delete(textEditGoodsID.Text) > 0)
            {
                WMPublic.sbMessageDeleteSuccess(this);
                sbLoadDataForGridAsync();
            }
        }

        private async Task sbLoadDataForGridAsync()
        {
            List<InventoryData> inventoryDatas = await inventoryDataRepository.GetAll();
            int mMaxRow = 100;
            if (inventoryDatas.Count < mMaxRow)
            {
                int num = mMaxRow - inventoryDatas.Count;
                int i = 1;
                while (true)
                {
                    int num2 = i;
                    int num3 = num;
                    if (num2 > num3)
                    {
                        break;
                    }
                    inventoryDatas.Add(new InventoryData());
                    i++;
                }
            }
            gridControlData.DataSource = inventoryDatas;
        }
        private void clear()
        {
            textEditGoodsID.Text = "";
            textEditDescription.Text = "";
        }
        private async Task<bool> checkExistAsync()
        {
            Boolean isExist = await inventoryDataRepository.checkExistAsync(textEditGoodsID.Text);
            return isExist;
        }

        private void gridView_RowClick(Object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            textEditGoodsID.Text = (string)(sender as GridView).GetFocusedRowCellValue("GoodsID");
            textEditGoodsName.Text = (string)(sender as GridView).GetFocusedRowCellValue("GoodsName");
            textEditQuantity.Text = (string)(sender as GridView).GetFocusedRowCellValue("Quantity").ToString();
            textEditAmount.Text = (string)(sender as GridView).GetFocusedRowCellValue("Amount").ToString();
            textEditDescription.Text = (string)(sender as GridView).GetFocusedRowCellValue("Description");
        }

        private async Task selectFileExcelAsync()
        {
            OpenFileDialog fdlg = new OpenFileDialog();
            fdlg.Title = "Select file";
            fdlg.InitialDirectory = @"d:\";
            fdlg.Filter = "Excel Sheet(*.xls)|*.xlsx|All Files(*.*)|*.*";
            fdlg.FilterIndex = 1;
            fdlg.RestoreDirectory = true;
            if (fdlg.ShowDialog() == DialogResult.OK)
            {
                List<InventoryData> inventoryDatas = await ExcelToListAsync(fdlg.FileName);

                //compare with goodsData
                try
                {
                    sumAddNew = 0;
                    Cursor = Cursors.WaitCursor;

                    await compareAndCreate(inventoryDatas);
                }
                finally
                {
                    MessageBox.Show("Đã thêm " + sumAddNew + "mã hàng, "+ "Đã update " + sumUpdate + "mã hàng");
                    Cursor = Cursors.Default;
                }
                sbLoadDataForGridAsync();
                Application.DoEvents();
            }
        }

        private async Task compareAndCreate(List<InventoryData> list)
        {
            List<InventoryData> inventoryDatasSaved = await inventoryDataRepository.GetAll();
            if (inventoryDatasSaved.Count > 0)
            {
                foreach (InventoryData inventoryData in list)
                {
                    textEditInventoryDataCheck.Text =inventoryData.GoodsID;
                    if (await inventoryDataRepository.checkExistAsync(inventoryData.GoodsID) == false)
                    {
                        inventoryData.LocationID = "";
                        inventoryData.Description = "";
                        inventoryData.Status = "New";
                        inventoryData.CreatedUserID = WMMessage.User.UserID;
                        inventoryData.CreatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));
                        if (await inventoryDataRepository.Create(inventoryData) > 0)
                        {
                            sumAddNew++;
                        }
                    }
                    else
                    {
                        inventoryData.LocationID = "";
                        inventoryData.Description = "";
                        inventoryData.Status = "Updated";
                        inventoryData.CreatedUserID = WMMessage.User.UserID;
                        inventoryData.CreatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));
                        if (await inventoryDataRepository.Update(inventoryData,textEditGoodsID.Text) > 0)
                        {
                            sumUpdate++;
                        }
                    }
                }
            }
            else
            {
                foreach (InventoryData inventoryData in list)
                {
                    inventoryData.LocationID = "";
                    inventoryData.Description = "";
                    inventoryData.Status = "New";
                    inventoryData.CreatedUserID = WMMessage.User.UserID;
                    inventoryData.CreatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));
                    if (await inventoryDataRepository.Create(inventoryData) > 0)
                    {
                        sumAddNew++;
                    }
                }
            }

        }

        private async Task<List<InventoryData>> ExcelToListAsync(String fileName)
        {
            FileInfo file = new FileInfo(Path.Combine(fileName));

            ExcelPackage.LicenseContext = LicenseContext.Commercial;
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            using (ExcelPackage package = new ExcelPackage(file))
            {
                ExcelWorksheet workSheet = package.Workbook.Worksheets.First();
                int totalRows = workSheet.Dimension.Rows;
                List<InventoryData> inventoryDatas = new List<InventoryData>();

                for (int i = 2; i <= totalRows; i++)
                {
                    if (workSheet?.Cells[i, 1].Text.Trim() != "")
                    {
                        inventoryDatas.Add(new InventoryData
                        {
                            GoodsID = workSheet?.Cells[i, 1].Text.Trim().Replace("-", "").Replace(" ", ""),
                            Quantity = Decimal.Parse(workSheet?.Cells[i, 2].Text.Trim()),
                            Amount = Decimal.Parse(workSheet?.Cells[i, 3].Text.Trim()),
                        });
                    }
                }

                //Get goodsname from metadata
                List<GoodsData> goodsDatas = await goodsDataRepository.GetAll();
                for (int i = 0; i < inventoryDatas.Count; i++)
                {
                    foreach (GoodsData goodsData in goodsDatas)
                    {
                        if (goodsData.GoodsID.Equals(inventoryDatas[i].GoodsID) || goodsData.ECNPart.Equals(inventoryDatas[i].GoodsID))
                        {
                            inventoryDatas[i].GoodsName = goodsData.GoodsName;
                        }
                    }
                }
                return inventoryDatas;
            }
        }

        //barbutton click
        public async void button_Click(object sender, EventArgs e)
        {

            ButtonEventArgs eArg = (DevExpress.XtraBars.Docking2010.ButtonEventArgs)e;
            String tag = eArg.Button.Properties.Tag.ToString();
            switch (tag)
            {
                case "new":
                    if (WMPublic.sbMessageAddNewRequest(this) == true)
                    {
                        clear();
                    }
                    break;
                case "save":

                    if (await checkExistAsync() == true)
                    {
                        if (WMPublic.sbMessageSaveChangeRequest(this) == true)
                        {
                            UpdateAsync();
                        }

                    }
                    else
                    {
                        if (WMPublic.sbMessageSaveNewRequest(this) == true)
                        {
                            CreatAsync();
                        }
                    }
                    break;
                case "delete":
                    if (WMPublic.sbMessageDeleteRequest(this) == true)
                    {
                        DeleteAsync();
                    }
                    break;
                case "search":
                    break;
                case "refesh":
                    sbLoadDataForGridAsync();
                    break;
                case "import":
                    selectFileExcelAsync();
                    break;
                case "export":
                    break;
                case "print":
                    break;
                case "close":
                    this.Dispose();
                    break;
            }

        }
    }
}
