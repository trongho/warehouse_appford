﻿using DevExpress.XtraBars.Docking2010;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraLayout.Filtering.Templates;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WarehouseManager.Models;
using WarehouseManager.Repository;
using webservice.Entities;
using LicenseContext = OfficeOpenXml.LicenseContext;
using DevExpress.XtraEditors.Repository;
using System.Globalization;

namespace WarehouseManager
{
    public partial class ReceiptRequisitionControl : DevExpress.XtraEditors.XtraUserControl
    {
        WRRDetailRepository WRRDetailRepository;
        WRRHeaderRepository wRRHeaderRepository;
        HandlingStatusRepository handlingStatusRepository;
        BranchRepository branchRepository;
        WRDataHeaderRepository wRDataHeaderRepository;
        WRDataDetailRepository wRDataDetailRepository;
        WRDataGeneralRepository wRDataGeneralRepository;
        GoodsRepository goodsRepository;
        GoodsDataRepository goodsDataRepository;
        CGDataHeaderRepository cgDataHeaderRepository;
        CGDataGeneralRepository cgDataGeneralRepository;
        public static string SelectedTable = string.Empty;
        public static String selectedGoodsID;
        public static String selectedGoodsName;
        public static String selectedWRRNumber;
        public static String selectedReferenceNumber;
        public static DateTime selectedWRRDate;
        public static String selectedNote;
        public static String selectedBranchID;
        public static String selectedBranchName;
        public static String selectedHandlingID;
        public static String selectedHandlingName;
        String focusGoodsID = "";
        String lastWRRNumber = "";
        Boolean isWRRNumberExist = false;
        public ReceiptRequisitionControl()
        {
            InitializeComponent();
            this.Load += ReceiptRequisitionControl_Load;
            WRRDetailRepository = new WRRDetailRepository();
            wRRHeaderRepository = new WRRHeaderRepository();
            handlingStatusRepository = new HandlingStatusRepository();
            branchRepository = new BranchRepository();
            wRDataHeaderRepository = new WRDataHeaderRepository();
            wRDataDetailRepository = new WRDataDetailRepository();
            wRDataGeneralRepository = new WRDataGeneralRepository();
            goodsRepository = new GoodsRepository();
            cgDataHeaderRepository = new CGDataHeaderRepository();
            cgDataGeneralRepository = new CGDataGeneralRepository();
            goodsDataRepository = new GoodsDataRepository();
        }

        private void ReceiptRequisitionControl_Load(Object sender, EventArgs e)
        {

            popupMenuSelectHandlingStatus();
            popupMenuSelectBranch();
            windowsUIButtonPanel1.ButtonClick += button_Click;
            windowsUIButtonPanel1.Cursor = Cursors.Hand;
            textEditWRRNumber.KeyDown += new KeyEventHandler(WRRNumber_KeyDown);
            gridViewWRRDetail.CustomColumnDisplayText += wRRHeaderGridView_CustomColumnDisplayText;
            gridViewWRRDetail.DoubleClick += dataGridViewWRR_CellDoubleClick;
            gridViewWRRDetail.CellValueChanged += YourDGV_CellValueChanged;
            //gridViewWRRDetail.ValidateRow += GridView1_ValidateRow;
            gridViewWRRDetail.ValidatingEditor += gridView1_ValidatingEditor;
            textEditWRRNumber.DoubleClick += textEditWRRNumer_CellDoubleClick;
            gridViewWRRDetail.ClearSorting();
            sbLoadDataForGridWRRDetailAsync(textEditWRRNumber.Text);

            

        }


        private async Task popupMenuSelectHandlingStatus()
        {
            List<HandlingStatus> handlingStatuses = await handlingStatusRepository.GetAll();
            foreach (HandlingStatus handlingStatus in handlingStatuses)
            {
                comboBoxEditHandlingStatus.Properties.Items.Add(handlingStatus.HandlingStatusName);
            }
            comboBoxEditHandlingStatus.SelectedIndex = 1;
        }

        private async Task popupMenuSelectBranch()
        {
            List<Branch> branches = await branchRepository.getAllBranchAsync();
            foreach (Branch branch in branches)
            {
                comboBoxEditBranch.Properties.Items.Add(branch.BranchName);
            }
            comboBoxEditBranch.SelectedIndex = 1;
        }

        private void YourDGV_CellValueChanged(object sender, CellValueChangedEventArgs e)
        {
            if (e.Column.FieldName.Equals("QuantityByItem"))
            {
                getTotalQuantityByItem();
            }
            if (e.Column.FieldName.Equals("QuantityByPack"))
            {
                getTotalQuantityByPack();
            }
        }

        private void gridView1_ValidatingEditor(object sender, DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs e)
        {
            GridView view = sender as GridView;

            object val = e.Value;

            if (view.FocusedColumn.FieldName == "GoodsID")
            {
                if (val == null || string.IsNullOrWhiteSpace(e.Value.ToString()))
                {
                    e.Valid = false;
                    e.ErrorText = "Bạn nhập mã hàng null";
                }
                if (checkExistGoodsID(e.Value.ToString()) == true)
                {
                    e.Valid = false;
                    e.ErrorText = "Mã hàng hóa trùng";
                }
            }
        }

        private Boolean checkExistGoodsID(String goodsID)
        {
            int num = gridViewWRRDetail.RowCount - 1;
            int i = 0;
            while (true)
            {
                int num2 = i;
                int num3 = num;
                if (num2 > num3 || gridViewWRRDetail.GetRowCellValue(i, gridViewWRRDetail.Columns["colGoodsID"]) == "")
                {
                    break;
                }
                int rowHandle = gridViewWRRDetail.GetRowHandle(i);
                String goodsID1 = (String)gridViewWRRDetail.GetRowCellValue(rowHandle, "GoodsID");
                if (goodsID.Equals(goodsID1))
                {
                    return true;
                }
                i++;
            }
            return false;
        }

        private Boolean checkRowBeforeNull()
        {
            int rowHandle = gridViewWRRDetail.FocusedRowHandle;
            String goodsID1 = (String)gridViewWRRDetail.GetRowCellValue(rowHandle - 1, "GoodsID");
            if (rowHandle > 0 && goodsID1 == null)
            {
                return true;
            }
            return false;
        }

        private void gridView1_ShownEditor(object sender, EventArgs e)
        {
            GridView view = sender as GridView;

            view.ActiveEditor.IsModified = true;
        }

        //private void GridView1_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        //{
        //    e.Valid = !IsDublicatedRowFound(e.Row);
        //    if (!e.Valid)
        //        e.ErrorText = "The record is not unique. ";
        //}
        //bool IsDublicatedRowFound(object row)
        //{
        //    for (int i = 0; i < gridViewWRRDetail.DataRowCount; i++)
        //        if (InvoicesHaveSameValues(row, gridViewWRRDetail.GetRow(i)))
        //            return true;
        //    return false;
        //}
        //bool InvoicesHaveSameValues(object row1, object row2)
        //{
        //    var lastEditedInvoice = row1 as WRRDetail;
        //    var currentlyProcessedRow = row2 as WRRDetail;
        //    if (lastEditedInvoice == null || currentlyProcessedRow == null)
        //        return false;

        //    if (lastEditedInvoice.GoodsID!= currentlyProcessedRow.GoodsID)
        //        return false;

        //    return true;
        //}


        private void getTotalQuantityByItem()
        {
            Decimal totalQuantityByItem = 0;
            int num = gridViewWRRDetail.RowCount - 1;
            int i = 0;
            while (true)
            {
                int rowHandle = gridViewWRRDetail.GetRowHandle(i);
                if (gridViewWRRDetail.GetRowCellValue(rowHandle, "GoodsID") == null)
                {
                    break;
                }
                Decimal quantityByItem = (Decimal)gridViewWRRDetail.GetRowCellValue(rowHandle, "QuantityByItem");
                totalQuantityByItem += quantityByItem;
                i++;
            }
            textEditTotalQuantityByItem.Text = totalQuantityByItem.ToString();
        }

        private void getTotalQuantityByPack()
        {
            //Decimal totalQuantityByPack = 0;
            //int i = 0;
            //while (true)
            //{
            //    int rowHandle = gridViewWRRDetail.GetRowHandle(i);
            //    if (gridViewWRRDetail.GetRowCellValue(rowHandle, "GoodsID") == null)
            //    {
            //        break;
            //    }
            //    Decimal quantityByPack = (Decimal)gridViewWRRDetail.GetRowCellValue(rowHandle, "QuantityByPack");
            //    totalQuantityByPack += quantityByPack;
            //    i++;
            //}
            //textEditTotalQuantityByPack.Text = totalQuantityByPack.ToString();
            Decimal totalQuantityByPack = 0;
            int i = 0;
            while (true)
            {
                int rowHandle = gridViewWRRDetail.GetRowHandle(i);
                if (gridViewWRRDetail.GetRowCellValue(rowHandle, "GoodsID") == null)
                {
                    break;
                }
                Decimal quantityByPack = (Decimal)gridViewWRRDetail.GetRowCellValue(rowHandle, "QuantityByPack");
                totalQuantityByPack += quantityByPack;
                i++;
            }
            textEditTotalQuantityByPack.Text = totalQuantityByPack.ToString();
        }

        private void wRRHeaderGridView_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {

            if (e.Column == colWRRDetailNo)
            {
                int rowHandle = gridViewWRRDetail.GetRowHandle(e.ListSourceRowIndex);
                if (rowHandle >= 0)
                {
                    e.DisplayText = (rowHandle + 1).ToString();
                }
            }
        }

        private async Task sbLoadDataForGridWRRDetailAsync(String wRRNumber)
        {
            List<WRRDetail> wRRDetails = await WRRDetailRepository.GetUnderID(wRRNumber);
            int mMaxRow = 100;
            if (wRRDetails.Count < mMaxRow)
            {
                int num = mMaxRow - wRRDetails.Count;
                int i = 1;
                while (true)
                {
                    int num2 = i;
                    int num3 = num;
                    if (num2 > num3)
                    {
                        break;
                    }
                    wRRDetails.Add(new WRRDetail());
                    i++;
                }
            }
            gridControlWRRDetail.DataSource = wRRDetails;
            getTotalQuantityByItem();
            getTotalQuantityByPack();
        }

        public async Task getlastWRRNumberAsync()
        {
            lastWRRNumber = await wRRHeaderRepository.GetLastWRRNumber();
            String wRRNumberYear = DateTime.Now.Year.ToString().Substring(2, 2);
            if (lastWRRNumber == "")
            {
                textEditWRRNumber.Text = wRRNumberYear + "000" + "-" + $"{1:D5}";
            }
            else
            {
                string[] arrListStr = lastWRRNumber.Split('-');
                String wRRNumberPartOne = arrListStr[0];
                String wRRNumberPartTwo = arrListStr[1];
                wRRNumberPartTwo = (int.Parse(wRRNumberPartTwo) + 1).ToString();
                textEditWRRNumber.Text = wRRNumberYear + "000" + "-" + $"{int.Parse(wRRNumberPartTwo):D5}";
            }


        }

        public async Task createWRRHeaderAsync()
        {
            WRRHeader wRRHeader = new WRRHeader();
            wRRHeader.WRRNumber = textEditWRRNumber.Text;
            wRRHeader.WRRDate = DateTime.Parse(dateEditWRRDate.DateTime.ToString("yyyy-MM-dd"));
            wRRHeader.ReferenceNumber = textEditWRRNumber.Text;
            wRRHeader.HandlingStatusID = (comboBoxEditHandlingStatus.SelectedIndex - 1).ToString();
            wRRHeader.HandlingStatusName = comboBoxEditHandlingStatus.SelectedItem.ToString();
            wRRHeader.BranchID = $"{int.Parse(comboBoxEditBranch.SelectedIndex.ToString()) - 1:D3}";
            wRRHeader.BranchName = comboBoxEditBranch.SelectedItem.ToString();
            wRRHeader.Note = "";
            wRRHeader.TotalQuantity = Decimal.Parse(textEditTotalQuantityByItem.Text);
            wRRHeader.CreatedUserID = WMMessage.User.UserID;
            wRRHeader.CreatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));

            if (await wRRHeaderRepository.Create(wRRHeader) > 0)
            {
                createWRRDetailAsync();
                WMPublic.sbMessageSaveNewSuccess(this);
                if (wRRHeader.HandlingStatusID.Equals("2"))
                {
                    createWRDataHeaderAsync();
                }
            }
        }

        public async Task createWRRDetailAsync()
        {
            int num = gridViewWRRDetail.RowCount - 1;
            int i = 0;
            while (true)
            {
                int num2 = i;
                int num3 = num;
                if (num2 > num3 || gridViewWRRDetail.GetRowCellValue(i, gridViewWRRDetail.Columns["colGoodsID"]) == "")
                {
                    break;
                }
                int rowHandle = gridViewWRRDetail.GetRowHandle(i);
                WRRDetail wRRDetail = new WRRDetail();
                wRRDetail.WRRNumber = textEditWRRNumber.Text;
                wRRDetail.GoodsID = (string?)gridViewWRRDetail.GetRowCellValue(rowHandle, "GoodsID");
                wRRDetail.GoodsName = (string?)gridViewWRRDetail.GetRowCellValue(rowHandle, "GoodsName");
                wRRDetail.Ordinal = i + 1;
                wRRDetail.OtherGoodsName = (string?)gridViewWRRDetail.GetRowCellValue(rowHandle, "OtherGoodsName");
                wRRDetail.TotalQuantity = (Decimal?)gridViewWRRDetail.GetRowCellValue(rowHandle, "TotalQuantity");
                wRRDetail.QuantityByItem = (Decimal?)gridViewWRRDetail.GetRowCellValue(rowHandle, "QuantityByItem");
                wRRDetail.QuantityByPack = (Decimal?)gridViewWRRDetail.GetRowCellValue(rowHandle, "QuantityByPack");

                wRRDetail.PackingVolume = (Decimal?)gridViewWRRDetail.GetRowCellValue(rowHandle, "PackingVolume");
                
                wRRDetail.ReceiptStatus= (string?)gridViewWRRDetail.GetRowCellValue(rowHandle, "ReceiptStatus");
                wRRDetail.SLPart = (string?)gridViewWRRDetail.GetRowCellValue(rowHandle, "SLPart");
                wRRDetail.LocationID = (string?)gridViewWRRDetail.GetRowCellValue(rowHandle, "LocationID");
                wRRDetail.ScanOption = (Int16?)gridViewWRRDetail.GetRowCellValue(rowHandle, "ScanOption");
                wRRDetail.Note = "";
                wRRDetail.SupplierCode = (string?)gridViewWRRDetail.GetRowCellValue(rowHandle, "SupplierCode");
                wRRDetail.ASNNumber = (string?)gridViewWRRDetail.GetRowCellValue(rowHandle, "ASNNumber");
                wRRDetail.PackingSlip = (string?)gridViewWRRDetail.GetRowCellValue(rowHandle, "PackingSlip");
                wRRDetail.QuantityReceived = (Decimal?)gridViewWRRDetail.GetRowCellValue(rowHandle, "QuantityReceived");
                if (await WRRDetailRepository.Create(wRRDetail) > 0)
                {
                    i++;
                }
            }
        }

        public async Task createWRDataHeaderAsync()
        {
            WRRHeader wRRHeader = await wRRHeaderRepository.GetUnderID(textEditWRRNumber.Text);
            WRDataHeader wRDataHeader = new WRDataHeader();
            wRDataHeader.WRDNumber = textEditWRRNumber.Text;
            //wRDataHeader.WRDDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));
            wRDataHeader.WRDDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));
            wRDataHeader.ReferenceNumber = textEditWRRNumber.Text;
            wRDataHeader.WRRNumber = textEditWRRNumber.Text;
            wRDataHeader.WRRReference = textEditWRRNumber.Text;
            wRDataHeader.BranchID = wRRHeader.BranchID;
            wRDataHeader.BranchName = wRRHeader.BranchName;
            wRDataHeader.HandlingStatusID = "0";
            wRDataHeader.HandlingStatusName = "Chưa duyệt";
            wRDataHeader.Note = "";
            wRDataHeader.Status = "0";
            wRDataHeader.TotalQuantity = 0m;
            wRDataHeader.TotalQuantityOrg = Decimal.Parse(textEditTotalQuantityByItem.Text);
            wRDataHeader.CreatedUserID = WMMessage.User.UserID;
            wRDataHeader.CreatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));

            if (await wRDataHeaderRepository.Create(wRDataHeader) > 0)
            {
                //createWRDataDetailAsync();
                createWRDataGeneralAsync();
                MessageBox.Show("Đã tạo dữ liệu nhập");
            }
        }

        public async Task createWRDataDetailAsync()
        {
            int num = gridViewWRRDetail.RowCount - 1;
            int ordinal = 0;
            for (int i = 0; i < gridViewWRRDetail.RowCount - 1; i++)
            {
                Decimal? quantityByPackDecimal = (Decimal?)gridViewWRRDetail.GetRowCellValue(i, "QuantityByPack");
                int quantityByPack = Convert.ToInt32(quantityByPackDecimal);
                Decimal? quantityByItemDecimal = (Decimal?)gridViewWRRDetail.GetRowCellValue(i, "QuantityByItem");
                int quantityByItem = Convert.ToInt32(quantityByItemDecimal);
                Decimal? packingVolumeDecimal = (Decimal?)gridViewWRRDetail.GetRowCellValue(i, "PackingVolume");
                int packingVolume = Convert.ToInt32(packingVolumeDecimal);
                for (int j = 1; j <= quantityByPack; j++)
                {
                    int num2 = i;
                    int num3 = num;
                    if (num2 > num3 || gridViewWRRDetail.GetRowCellValue(i, gridViewWRRDetail.Columns["colGoodsID"]) == "")
                    {
                        break;
                    }
                    WRDataDetail wRDataDetail = new WRDataDetail();
                    wRDataDetail.IDCode = j + "/" + quantityByPack;
                    wRDataDetail.WRDNumber = textEditWRRNumber.Text;
                    wRDataDetail.GoodsID = (string?)gridViewWRRDetail.GetRowCellValue(i, "GoodsID");
                    wRDataDetail.GoodsName = (string?)gridViewWRRDetail.GetRowCellValue(i, "GoodsName");
                    wRDataDetail.Ordinal = ordinal + 1;
                    wRDataDetail.LocationID = "";
                    wRDataDetail.Quantity = 0m;
                    wRDataDetail.TotalQuantity = 0m;
                    wRDataDetail.TotalGoods = 0m;
                    wRDataDetail.PackingQuantity = 0m;
                    if (quantityByItem % quantityByPack != 0)
                    {
                        if (j < quantityByPack)
                        {
                            wRDataDetail.QuantityOrg = packingVolume;
                        }
                        else
                        {
                            wRDataDetail.QuantityOrg = quantityByItem % (packingVolume);
                        }
                    }
                    else
                    {
                        wRDataDetail.QuantityOrg = quantityByItem / quantityByPack;
                    }
                    wRDataDetail.TotalQuantityOrg = Decimal.Parse(textEditTotalQuantityByItem.Text);
                    List<WRRDetail> wRRDetails = await WRRDetailRepository.GetUnderID(textEditWRRNumber.Text);
                    wRDataDetail.TotalGoodsOrg = Convert.ToDecimal(wRRDetails.Count);
                    wRDataDetail.LocationIDOrg = (String?)gridViewWRRDetail.GetRowCellValue(i, "LocationID");

                    wRDataDetail.CreatorID = WMMessage.User.UserID; ;
                    wRDataDetail.CreatedDateTime = DateTime.Now.ToString("yyyy-MM-dd");
                    wRDataDetail.Status = (string?)gridViewWRRDetail.GetRowCellValue(i, "Status");
                    wRDataDetail.PackingVolume = (Decimal?)gridViewWRRDetail.GetRowCellValue(i, "PackingVolume");
                    wRDataDetail.QuantityByPack = (Decimal?)gridViewWRRDetail.GetRowCellValue(i, "QuantityByPack");
                    wRDataDetail.QuantityByItem = (Decimal?)gridViewWRRDetail.GetRowCellValue(i, "QuantityByItem");
                    wRDataDetail.Note = "";
                    wRDataDetail.ScanOption = (Int16?)gridViewWRRDetail.GetRowCellValue(i, "ScanOption");
                    wRDataDetail.SupplierCode = (String?)gridViewWRRDetail.GetRowCellValue(i, "SupplierCode");
                    wRDataDetail.ASNNumber = (String?)gridViewWRRDetail.GetRowCellValue(i, "ASNNumber");
                    wRDataDetail.PackingSlip = (String?)gridViewWRRDetail.GetRowCellValue(i, "PackingSlip");
                    await wRDataDetailRepository.Create(wRDataDetail);
                    ordinal++;
                }
            }
        }

        public async Task createWRDataGeneralAsync()
        {
            int num = gridViewWRRDetail.RowCount - 1;
            for (int i = 0; i < gridViewWRRDetail.RowCount - 1; i++)
            {
                int num2 = i;
                int num3 = num;
                if (num2 > num3 || gridViewWRRDetail.GetRowCellValue(i, gridViewWRRDetail.Columns["colGoodsID"]) == "")
                {
                    break;
                }
                WRDataGeneral wRDataGeneral = new WRDataGeneral();
                wRDataGeneral.IDCode = "";
                wRDataGeneral.WRDNumber = textEditWRRNumber.Text;
                wRDataGeneral.GoodsID = (string?)gridViewWRRDetail.GetRowCellValue(i, "GoodsID");
                wRDataGeneral.GoodsName = (string?)gridViewWRRDetail.GetRowCellValue(i, "GoodsName");
                wRDataGeneral.Ordinal = i + 1;
                wRDataGeneral.LocationID = "";
                wRDataGeneral.Quantity = 0m;
                wRDataGeneral.TotalQuantity = 0m;
                wRDataGeneral.TotalGoods = 0m;
                wRDataGeneral.PackingQuantity = 0m;
                wRDataGeneral.QuantityOrg = (Decimal?)gridViewWRRDetail.GetRowCellValue(i, "QuantityByItem");
                wRDataGeneral.TotalQuantityOrg = Decimal.Parse(textEditTotalQuantityByItem.Text);

                List<WRRDetail> wRRDetails = await WRRDetailRepository.GetUnderID(textEditWRRNumber.Text);
                wRDataGeneral.TotalGoodsOrg = Convert.ToDecimal(wRRDetails.Count);
                wRDataGeneral.LocationIDOrg = (String?)gridViewWRRDetail.GetRowCellValue(i, "LocationID");

                wRDataGeneral.CreatorID = WMMessage.User.UserID; ;
                wRDataGeneral.CreatedDateTime = DateTime.Now;
                wRDataGeneral.ReceiptStatus = (string?)gridViewWRRDetail.GetRowCellValue(i, "ReceiptStatus");
                wRDataGeneral.SLPart = (string?)gridViewWRRDetail.GetRowCellValue(i, "SLPart");
                wRDataGeneral.PackingVolume = (Decimal?)gridViewWRRDetail.GetRowCellValue(i, "PackingVolume");
                wRDataGeneral.QuantityByPack = (Decimal?)gridViewWRRDetail.GetRowCellValue(i, "QuantityByPack");
                wRDataGeneral.QuantityByItem = (Decimal?)gridViewWRRDetail.GetRowCellValue(i, "QuantityByItem");
                wRDataGeneral.Note = "";
                wRDataGeneral.ScanOption = (Int16?)gridViewWRRDetail.GetRowCellValue(i, "ScanOption");
                wRDataGeneral.SupplierCode = (String?)gridViewWRRDetail.GetRowCellValue(i, "SupplierCode");
                wRDataGeneral.ASNNumber = (String?)gridViewWRRDetail.GetRowCellValue(i, "ASNNumber");
                wRDataGeneral.PackingSlip = (String?)gridViewWRRDetail.GetRowCellValue(i, "PackingSlip");
                await wRDataGeneralRepository.Create(wRDataGeneral);
            }
        }

        public async Task createCGDataHeaderAsync()
        {
            WRRHeader wRRHeader = await wRRHeaderRepository.GetUnderID(textEditWRRNumber.Text);
            CGDataHeader cgDataHeader = new CGDataHeader();
            cgDataHeader.CGDNumber = textEditWRRNumber.Text;
            cgDataHeader.CGDDate = DateTime.Parse(dateEditWRRDate.DateTime.ToString("yyyy-MM-dd"));
            cgDataHeader.ReferenceNumber = textEditWRRNumber.Text;
            cgDataHeader.WRRNumber = textEditWRRNumber.Text;
            cgDataHeader.WRRReference = textEditWRRNumber.Text;
            cgDataHeader.BranchID = wRRHeader.BranchID;
            cgDataHeader.BranchName = wRRHeader.BranchName;
            cgDataHeader.HandlingStatusID = "0";
            cgDataHeader.HandlingStatusName = "Chưa duyệt";
            cgDataHeader.Note = "";
            cgDataHeader.Status = "0";
            cgDataHeader.TotalQuantity = 0m;
            cgDataHeader.TotalQuantityOrg = Decimal.Parse(textEditTotalQuantityByItem.Text);
            cgDataHeader.CreatedUserID = WMMessage.User.UserID;
            cgDataHeader.CreatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));

            if (await cgDataHeaderRepository.Create(cgDataHeader) > 0)
            {
                createCGDataGeneralAsync();
                MessageBox.Show("Đã tạo dữ liệu điền hàng");
            }
        }

        public async Task createCGDataGeneralAsync()
        {
            int num = gridViewWRRDetail.RowCount - 1;
            for (int i = 0; i < gridViewWRRDetail.RowCount - 1; i++)
            {
                int num2 = i;
                int num3 = num;
                if (num2 > num3 || gridViewWRRDetail.GetRowCellValue(i, gridViewWRRDetail.Columns["colGoodsID"]) == "")
                {
                    break;
                }
                CGDataGeneral cgDataGeneral = new CGDataGeneral();
                cgDataGeneral.IDCode = "";
                cgDataGeneral.CGDNumber = textEditWRRNumber.Text;
                cgDataGeneral.GoodsID = (string?)gridViewWRRDetail.GetRowCellValue(i, "GoodsID");
                cgDataGeneral.GoodsName = (string?)gridViewWRRDetail.GetRowCellValue(i, "GoodsName");
                cgDataGeneral.Ordinal = i + 1;
                cgDataGeneral.LocationID = "";
                cgDataGeneral.Quantity = 0m;
                cgDataGeneral.TotalQuantity = 0m;
                cgDataGeneral.TotalGoods = 0m;
                cgDataGeneral.PackingQuantity = 0m;
                cgDataGeneral.QuantityOrg = (Decimal?)gridViewWRRDetail.GetRowCellValue(i, "QuantityByItem");
                cgDataGeneral.TotalQuantityOrg = Decimal.Parse(textEditTotalQuantityByItem.Text);

                List<WRRDetail> wRRDetails = await WRRDetailRepository.GetUnderID(textEditWRRNumber.Text);
                cgDataGeneral.TotalGoodsOrg = Convert.ToDecimal(wRRDetails.Count);
                cgDataGeneral.LocationIDOrg = (String?)gridViewWRRDetail.GetRowCellValue(i, "LocationID");

                cgDataGeneral.CreatorID = WMMessage.User.UserID; ;
                cgDataGeneral.CreatedDateTime = DateTime.Now.ToString("dd/MM/yyyy");
                cgDataGeneral.Status = (string?)gridViewWRRDetail.GetRowCellValue(i, "Status");
                cgDataGeneral.PackingVolume = (Decimal?)gridViewWRRDetail.GetRowCellValue(i, "PackingVolume");
                cgDataGeneral.QuantityByPack = (Decimal?)gridViewWRRDetail.GetRowCellValue(i, "QuantityByPack");
                cgDataGeneral.QuantityByItem = (Decimal?)gridViewWRRDetail.GetRowCellValue(i, "QuantityByItem");
                cgDataGeneral.Note = "";
                cgDataGeneral.ScanOption = (Int16?)gridViewWRRDetail.GetRowCellValue(i, "ScanOption");
                await cgDataGeneralRepository.Create(cgDataGeneral);
            }
        }




        public async Task updateWRRHeaderAsync()
        {
            WRRHeader wRRHeader = await wRRHeaderRepository.GetUnderID(textEditWRRNumber.Text);
            wRRHeader.WRRNumber = textEditWRRNumber.Text;
            wRRHeader.WRRDate = DateTime.Parse(dateEditWRRDate.DateTime.ToString("yyyy-MM-dd"));
            wRRHeader.ReferenceNumber = textEditWRRNumber.Text;
            wRRHeader.HandlingStatusID = (comboBoxEditHandlingStatus.SelectedIndex - 1).ToString();
            wRRHeader.HandlingStatusName = comboBoxEditHandlingStatus.SelectedItem.ToString();
            wRRHeader.BranchID = $"{int.Parse(comboBoxEditBranch.SelectedIndex.ToString()) - 1:D3}";
            wRRHeader.BranchName = comboBoxEditBranch.SelectedItem.ToString();
            wRRHeader.Note = "";
            wRRHeader.TotalQuantity = Decimal.Parse(textEditTotalQuantityByItem.Text);
            wRRHeader.UpdatedUserID = WMMessage.User.UserID;
            wRRHeader.UpdatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));

            if (await wRRHeaderRepository.Update(wRRHeader, textEditWRRNumber.Text) > 0)
            {
                updateWRRDetailAsync();
                WMPublic.sbMessageSaveChangeSuccess(this);
                if (wRRHeader.HandlingStatusID.Equals("2"))
                {
                    createWRDataHeaderAsync();
                }
            }

        }

        public async Task updateWRRDetailAsync()
        {
            int num = gridViewWRRDetail.RowCount - 1;
            int i = 0;
            while (true)
            {
                int num2 = i;
                int num3 = num;
                if (num2 > num3 || gridViewWRRDetail.GetRowCellValue(i, gridViewWRRDetail.Columns["colGoodsID"]) == "")
                {
                    break;
                }
                int rowHandle = gridViewWRRDetail.GetRowHandle(i);
                WRRDetail wRRDetail = new WRRDetail();
                wRRDetail.WRRNumber = textEditWRRNumber.Text;
                wRRDetail.GoodsID = (string?)gridViewWRRDetail.GetRowCellValue(rowHandle, "GoodsID");
                wRRDetail.GoodsName = (string?)gridViewWRRDetail.GetRowCellValue(rowHandle, "GoodsName");
                wRRDetail.Ordinal = i + 1;
                wRRDetail.OtherGoodsName = (string?)gridViewWRRDetail.GetRowCellValue(rowHandle, "OtherGoodsName");
                wRRDetail.PackingVolume = (Decimal?)gridViewWRRDetail.GetRowCellValue(rowHandle, "PackingVolume");
                wRRDetail.TotalQuantity = (Decimal?)gridViewWRRDetail.GetRowCellValue(rowHandle, "TotalQuantity");
                wRRDetail.QuantityByItem = (Decimal?)gridViewWRRDetail.GetRowCellValue(rowHandle, "QuantityByItem");
                wRRDetail.QuantityByPack = (Decimal?)gridViewWRRDetail.GetRowCellValue(rowHandle, "QuantityByPack");
                wRRDetail.LocationID = (string?)gridViewWRRDetail.GetRowCellValue(rowHandle, "LocationID");
                wRRDetail.ScanOption = (Int16?)gridViewWRRDetail.GetRowCellValue(rowHandle, "ScanOption");
                wRRDetail.Note = "";
                wRRDetail.SupplierCode = (string?)gridViewWRRDetail.GetRowCellValue(rowHandle, "SupplierCode");
                wRRDetail.ASNNumber = (string?)gridViewWRRDetail.GetRowCellValue(rowHandle, "ASNNumber");
                wRRDetail.PackingSlip = (string?)gridViewWRRDetail.GetRowCellValue(rowHandle, "PackingSlip");
                wRRDetail.ReceiptStatus = (string?)gridViewWRRDetail.GetRowCellValue(rowHandle, "ReceiptStatus");
                wRRDetail.SLPart = (string?)gridViewWRRDetail.GetRowCellValue(rowHandle, "SLPart");

                if (await WRRDetailRepository.Update(wRRDetail, textEditWRRNumber.Text, wRRDetail.GoodsID, wRRDetail.Ordinal) > 0)
                {
                    i++;
                }
            }
        }

        public async Task DeleteWRRHeaderAsync()
        {
            if ((await wRRHeaderRepository.Delete(textEditWRRNumber.Text)) > 0)
            {
                DeleteWRRDetailAsync();
                await DeleteWRDataHeaderAsync();
                sbLoadDataForGridWRRDetailAsync(textEditWRRNumber.Text);
                WMPublic.sbMessageDeleteSuccess(this);
            }
        }

        public async Task DeleteWRRDetailAsync()
        {
            await WRRDetailRepository.Delete(textEditWRRNumber.Text);
            clearAsync();
            gridControlWRRDetail.DataSource = null;
        }

        public async Task DeleteWRDataHeaderAsync()
        {
            if ((await wRDataHeaderRepository.Delete(textEditWRRNumber.Text)) > 0)
            {
                await DeleteWRDataGeneralAsync();
            }
        }

        public async Task DeleteWRDataGeneralAsync()
        {
            await wRDataGeneralRepository.Delete(textEditWRRNumber.Text);          
        }

        private void clearAsync()
        {
            textEditWRRNumber.Text = "";
            dateEditWRRDate.Text = DateTime.Now.ToString("dd-MM-yyyy");
            comboBoxEditHandlingStatus.SelectedIndex = 1;
            textEditTotalQuantityByItem.Text = "0";
            textEditTotalQuantityByPack.Text = "0";
            List<WRRDetail> wRRDetails = new List<WRRDetail>();

            for (int i = 0; i < 100; i++)
            {
                wRRDetails.Add(new WRRDetail());
            }
            gridControlWRRDetail.DataSource = wRRDetails;
        }

        private async Task checkExistWRRNumber(String wRRNumber)
        {
            if (!wRRNumber.Equals("") && (await wRRHeaderRepository.checkExistAsync(wRRNumber)) == true)
            {
                isWRRNumberExist = true;
            }
        }


        private async void WRRNumber_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {

                WRRHeader wRRHeader = new WRRHeader();
                wRRHeader = await wRRHeaderRepository.GetUnderID(textEditWRRNumber.Text);
                if (wRRHeader != null)
                {
                    dateEditWRRDate.Text = wRRHeader.WRRDate.ToString();
                    comboBoxEditBranch.SelectedIndex = int.Parse($"{int.Parse(wRRHeader.BranchID) + 1:D1}");
                    comboBoxEditHandlingStatus.SelectedIndex = int.Parse(wRRHeader.HandlingStatusID) + 1;
                    sbLoadDataForGridWRRDetailAsync(textEditWRRNumber.Text);
                }
                else
                {
                    MessageBox.Show("Số phiếu yêu cầu không tồn tại");
                    return;
                }

            }
        }


        private void dataGridViewWRR_CellDoubleClick(object sender, EventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView sndr =
                sender as DevExpress.XtraGrid.Views.Grid.GridView;

            DevExpress.Utils.DXMouseEventArgs dxMouseEventArgs =
                e as DevExpress.Utils.DXMouseEventArgs;


            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hitInfo =
                   sndr.CalcHitInfo(dxMouseEventArgs.Location);
            if (hitInfo.InRowCell)
            {
                if (checkRowBeforeNull() == true)
                {
                    MessageBox.Show("Hàng phía trước đang trống");
                    return;
                }
                if (gridViewWRRDetail.FocusedColumn.FieldName.Equals("GoodsID"))
                {
                    frmSearchGoods frmSearchGoods = new frmSearchGoods();
                    frmSearchGoods.isReceipRequisition = true;
                    frmSearchGoods.ShowDialog(this);
                    frmSearchGoods.Dispose();
                    insertData();
                }
            }
        }

        private async void textEditWRRNumer_CellDoubleClick(object sender, EventArgs e)
        {
            frmSearchWRR frmSearchWRR = new frmSearchWRR();
            frmSearchWRR.ShowDialog(this);
            frmSearchWRR.Dispose();
            if (selectedWRRNumber != null)
            {
                textEditWRRNumber.Text = selectedWRRNumber;
                dateEditWRRDate.Text = selectedWRRDate.ToString("yyyy-MM-dd");
                comboBoxEditBranch.SelectedIndex = int.Parse(selectedBranchID);
                comboBoxEditHandlingStatus.SelectedIndex = int.Parse(selectedHandlingID) + 1;
                await sbLoadDataForGridWRRDetailAsync(selectedWRRNumber);
            }

        }

        private void insertData()
        {
            int rowHandle = gridViewWRRDetail.FocusedRowHandle;
            gridViewWRRDetail.SetRowCellValue(rowHandle, gridViewWRRDetail.Columns["GoodsID"], selectedGoodsID);
            gridViewWRRDetail.SetRowCellValue(rowHandle, gridViewWRRDetail.Columns["GoodsName"], selectedGoodsName);
            selectedGoodsID = "";
            selectedGoodsName = "";
        }

        private async Task selectFileExcelAsync()
        {
            OpenFileDialog fdlg = new OpenFileDialog();
            fdlg.Title = "Select file";
            fdlg.InitialDirectory = @"d:\";
            fdlg.Filter = "Excel Sheet(*.xls)|*.xlsx|All Files(*.*)|*.*";
            fdlg.FilterIndex = 1;
            fdlg.RestoreDirectory = true;
            if (fdlg.ShowDialog() == DialogResult.OK)
            {
                List<WRRDetail> wRRDetails = await ExcelToListAsync(fdlg.FileName);
                int mMaxRow = 100;
                if (wRRDetails.Count < mMaxRow)
                {
                    int num = mMaxRow - wRRDetails.Count;
                    int i = 1;
                    while (true)
                    {
                        int num2 = i;
                        int num3 = num;
                        if (num2 > num3)
                        {
                            break;
                        }
                        wRRDetails.Add(new WRRDetail());
                        i++;
                    }
                }
                gridControlWRRDetail.DataSource = wRRDetails;
                getTotalQuantityByItem();
                getTotalQuantityByPack();
                Application.DoEvents();
            }
        }


        private async Task<List<WRRDetail>> ExcelToListAsync(String fileName)
        {
            FileInfo file = new FileInfo(Path.Combine(fileName));

            ExcelPackage.LicenseContext = LicenseContext.Commercial;
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            using (ExcelPackage package = new ExcelPackage(file))
            {

                ExcelWorksheet workSheet = package.Workbook.Worksheets["SearchForConveyance"];
                textEditWRRNumber.Text = workSheet?.Cells[16, 1].Text.Trim();
                dateEditWRRDate.Text = DateTime.Now.ToString("yyyy-MM-dd");
                comboBoxEditHandlingStatus.SelectedIndex =1;
                //textEditNote.Text = workSheet?.Cells[4, 1].Text.Trim() + workSheet?.Cells[4, 3].Text.Trim()
                //    + workSheet?.Cells[5, 1].Text.Trim() + workSheet?.Cells[5, 3].Text.Trim() + "-";

                int totalRows = workSheet.Dimension.Rows;
                List<WRRDetail> wRRDetails = new List<WRRDetail>();
                for (int i = 16; i <= totalRows; i++)
                {
                    wRRDetails.Add(new WRRDetail
                    {
                        GoodsID = workSheet?.Cells[i, 28].Text.Trim().Replace("-","").Replace(" ",""),
                        //GoodsName = workSheet?.Cells[i, 3].Text.Trim(),
                        //OtherGoodsName = workSheet?.Cells[i, 4].Text.Trim(),
                        //PackingVolume = Decimal.Parse(workSheet?.Cells[i, 31].Text.Trim()),
                        TotalQuantity = Decimal.Parse(workSheet?.Cells[i, 30].Text.Trim()),
                        QuantityByItem = Decimal.Parse(workSheet?.Cells[i, 30].Text.Trim()),
                        QuantityByPack = Decimal.Parse(workSheet.Cells[i, 31].Text.Trim()),
                        PackingVolume = Math.Floor(Decimal.Parse(workSheet?.Cells[i, 30].Text.Trim())/ Decimal.Parse(workSheet.Cells[i, 31].Text.Trim())),
                    //LocationID = workSheet?.Cells[i, 9].Text.Trim(),
                    //ScanOption = Int16.Parse(workSheet?.Cells[i, 10].Text.Trim()),
                    //Note = workSheet?.Cells[i, 11].Text.Trim(),
                        SupplierCode = workSheet?.Cells[i, 27].Text.Trim(),
                        ASNNumber = workSheet?.Cells[i, 25].Text.Trim(),
                        PackingSlip = workSheet?.Cells[i, 26].Text.Trim(),
                        QuantityReceived=0m,
                        ReceiptStatus =workSheet?.Cells[i,34].Text.Trim(),
                    });
                }
                //Get googname from metadata
                List<GoodsData> goodsDatas = await goodsDataRepository.GetAll();
                for (int i = 0; i < wRRDetails.Count; i++)
                {
                    foreach (GoodsData goodsData in goodsDatas)
                    {
                        if (goodsData.GoodsID.Equals(wRRDetails[i].GoodsID)|| goodsData.ECNPart.Equals(wRRDetails[i].GoodsID))
                        {
                            if (goodsData.ECNPart.Equals(""))
                            {
                                wRRDetails[i].GoodsName = goodsData.GoodsName;
                                wRRDetails[i].SLPart = goodsData.SLPart;
                            }
                            else
                            {
                                wRRDetails[i].GoodsName = goodsData.GoodsName;
                                wRRDetails[i].SLPart = goodsData.SLPart;
                            }
                            
                        }
                    }
                }
                return wRRDetails;
            }
        }


        //barbutton click
        public async void button_Click(object sender, EventArgs e)
        {

            ButtonEventArgs eArg = (DevExpress.XtraBars.Docking2010.ButtonEventArgs)e;
            String tag = eArg.Button.Properties.Tag.ToString();
            switch (tag)
            {
                case "new":
                    if (WMPublic.sbMessageAddNewRequest(this) == true)
                    {
                        clearAsync();
                    }
                    break;
                case "save":
                    isWRRNumberExist = false;
                    await checkExistWRRNumber(textEditWRRNumber.Text);

                    if (isWRRNumberExist == true)
                    {
                        if (WMPublic.sbMessageSaveChangeRequest(this) == true)
                        {
                            await updateWRRHeaderAsync();
                        }
                        isWRRNumberExist = false;
                    }
                    else
                    {
                        if (WMPublic.sbMessageSaveNewRequest(this) == true)
                        {
                            await createWRRHeaderAsync();
                        }
                    }
                    break;
                case "delete":
                    if (WMPublic.sbMessageDeleteRequest(this) == true)
                    {

                        DeleteWRRHeaderAsync();
                    }
                    break;
                case "search":
                    break;
                case "refesh":
                    sbLoadDataForGridWRRDetailAsync(textEditWRRNumber.Text);
                    break;
                case "import":
                    try
                    {
                        Application.UseWaitCursor = true;
                        await selectFileExcelAsync();                    
                    }
                    finally
                    {
                        MessageBox.Show("Tải thành công file excel");
                        Application.UseWaitCursor = false;                       
                    }                   
                    break;
                case "export":
                    break;
                case "print":
                    break;
                case "close":
                    break;
            }

        }


    }
}
