﻿using DevExpress.Utils;
using DevExpress.XtraBars.Docking2010;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using OfficeOpenXml.FormulaParsing.Excel.Operators;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Media;
using WarehouseManager.Models;
using WarehouseManager.Repository;
using webservice.Entities;

namespace WarehouseManager
{
    public partial class IssueOrderControl : DevExpress.XtraEditors.XtraUserControl
    {
        HandlingStatusRepository handlingStatusRepository;
        IOHeaderRepository iOHeaderRepository;
        IODetailRepository iODetailRepository;
        ModalityRepository modalityRepository;
        WarehouseRepository warehouseRepository;
        public static String selectedIONumber;

        public IssueOrderControl()
        {
            InitializeComponent();
            
            handlingStatusRepository = new HandlingStatusRepository();
            iODetailRepository = new IODetailRepository();
            iOHeaderRepository = new IOHeaderRepository();
            modalityRepository = new ModalityRepository();
            warehouseRepository = new WarehouseRepository();
            popupMenuSelectHandlingStatus();
            popupMenuSelectModality();
            popupMenuSelectWarehouse();
            sbLoadDataForGridIODetailAsync(textEditIONumber.Text);
            this.Load += IssueOrderControl_Load;
        }

        private void IssueOrderControl_Load(Object sender, EventArgs e)
        {
            windowsUIButtonPanel1.ButtonClick += button_Click;
            textEditIONumber.DoubleClick += textEditIONumer_CellDoubleClick;
            gridViewIODetail.CustomColumnDisplayText += GridView_CustomColumnDisplayText;
        }

        private void GridView_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {

            if (e.Column == colNo)
            {
                int rowHandle = gridViewIODetail.GetRowHandle(e.ListSourceRowIndex);
                if (rowHandle >= 0)
                {
                    e.DisplayText = (rowHandle + 1).ToString();
                }
            }
        }

        private async Task popupMenuSelectModality()
        {
            List<Modality> modalities = await modalityRepository.GetAll();
            foreach (Modality modality in modalities)
            {
                comboBoxEditModality.Properties.Items.Add(modality.ModalityName);
            }
            comboBoxEditModality.SelectedIndex = 0;
        }

        private async Task popupMenuSelectHandlingStatus()
        {
            List<HandlingStatus> handlingStatuses = await handlingStatusRepository.GetAll();
            foreach (HandlingStatus handlingStatus in handlingStatuses)
            {
                comboBoxEditHandlingStatus.Properties.Items.Add(handlingStatus.HandlingStatusName);
            }
            comboBoxEditHandlingStatus.SelectedIndex = 1;
        }

        private async Task popupMenuSelectWarehouse()
        {
            List<Warehouse> warehouses = await warehouseRepository.getAll();
            warehouses.Remove(warehouses[0]);
            foreach (Warehouse warehouse in warehouses)
            {
                comboBoxEditWarehouse.Properties.Items.Add(warehouse.WarehouseName);
            }
            comboBoxEditWarehouse.SelectedIndex = 0;
        }

        private async Task sbLoadDataForGridIODetailAsync(String iONumber)
        {
            List<IODetail> iODetails = await iODetailRepository.GetUnderID(iONumber);
            int mMaxRow = 100;
            if (iODetails.Count < mMaxRow)
            {
                int num = mMaxRow - iODetails.Count;
                int i = 1;
                while (true)
                {
                    int num2 = i;
                    int num3 = num;
                    if (num2 > num3)
                    {
                        break;
                    }
                    iODetails.Add(new IODetail());
                    i++;
                }
            }
            gridControlIODetail.DataSource = iODetails;
        }


      
        public async Task updateIOHeaderAsync()
        {
            IOHeader iOHeader = await iOHeaderRepository.GetUnderID(textEditIONumber.Text);
            iOHeader.IONumber = textEditIONumber.Text;
            iOHeader.IODate = DateTime.Parse(dateEditIODate.DateTime.ToString("yyyy-MM-dd"));
            iOHeader.ReferenceNumber = textEditIONumber.Text;
            iOHeader.HandlingStatusID = (comboBoxEditHandlingStatus.SelectedIndex - 1).ToString();
            iOHeader.HandlingStatusName = comboBoxEditHandlingStatus.SelectedItem.ToString();
            iOHeader.ModalityID = $"{int.Parse(comboBoxEditModality.SelectedIndex.ToString())+1:D2}";
            iOHeader.ModalityName = comboBoxEditModality.SelectedItem.ToString();
            iOHeader.WarehouseID = $"{int.Parse(comboBoxEditWarehouse.SelectedIndex.ToString()) + 1:D3}";
            iOHeader.WarehouseName = comboBoxEditWarehouse.SelectedItem.ToString();
            iOHeader.Note = textEditNote.Text;
            iOHeader.UpdatedUserID = WMMessage.User.UserID;
            iOHeader.UpdatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));

            if (await iOHeaderRepository.Update(iOHeader, textEditIONumber.Text) > 0)
            {
                updateIODetailAsync();
                WMPublic.sbMessageSaveChangeSuccess(this);
            }
        }

        public async Task updateIODetailAsync()
        {
            int num = gridViewIODetail.RowCount - 1;
            int i = 0;
            while (true)
            {
                int num2 = i;
                int num3 = num;
                if (num2 > num3 || gridViewIODetail.GetRowCellValue(i, gridViewIODetail.Columns["colGoodsID"]) == "")
                {
                    break;
                }
                int rowHandle = gridViewIODetail.GetRowHandle(i);
                String ioNumber = textEditIONumber.Text;
                String goodsID= (string)gridViewIODetail.GetRowCellValue(rowHandle, "GoodsID");
                int Ordinal= i+1;
                List<IODetail> iODetails = await iODetailRepository.GetUnderMultiID(ioNumber,goodsID,Ordinal);
                IODetail iODetail = iODetails[0];
                iODetail.IONumber = textEditIONumber.Text;
                iODetail.GoodsID = (string?)gridViewIODetail.GetRowCellValue(rowHandle, "GoodsID");
                iODetail.GoodsName = (string?)gridViewIODetail.GetRowCellValue(rowHandle, "GoodsName");
                iODetail.Ordinal = i + 1;
                iODetail.IssueQuantity = (Decimal?)gridViewIODetail.GetRowCellValue(rowHandle, "IssueQuantity");
                
                iODetail.Note = textEditNote.Text;
                if ((comboBoxEditHandlingStatus.SelectedIndex - 1).ToString().Equals("2"))
                {
                   iODetail.Status = "0";
                }

                if (await iODetailRepository.Update(iODetail, textEditIONumber.Text, iODetail.GoodsID, iODetail.Ordinal) > 0)
                {
                    i++;
                }
            }
        }

        private async void WRNumber_KeyDown(object sender, KeyEventArgs e)
        {
            
        }

        private async void textEditIONumer_CellDoubleClick(object sender, EventArgs e)
        {
            frmSearchIO frmSearchIO = new frmSearchIO();
            frmSearchIO.ShowDialog(this);
            frmSearchIO.Dispose();
            if (selectedIONumber != null)
            {
                textEditIONumber.Text = selectedIONumber;
                IOHeader iOHeader = await iOHeaderRepository.GetUnderID(textEditIONumber.Text);
                dateEditIODate.Text = iOHeader.IODate.ToString();
                textEditNote.Text = iOHeader.Note;
                comboBoxEditHandlingStatus.SelectedIndex = int.Parse(iOHeader.HandlingStatusID) + 1;
                comboBoxEditModality.SelectedIndex = int.Parse(iOHeader.ModalityID)-1;
                if (iOHeader.WarehouseID != null)
                {
                    comboBoxEditWarehouse.SelectedIndex = int.Parse(iOHeader.WarehouseID)+1;
                }
                await sbLoadDataForGridIODetailAsync(textEditIONumber.Text);
            }

        }

        //barbutton click
        public async void button_Click(object sender, EventArgs e)
        {
            ButtonEventArgs eArg = (DevExpress.XtraBars.Docking2010.ButtonEventArgs)e;
            String tag = eArg.Button.Properties.Tag.ToString();
            switch (tag)
            {
                case "new":
                    break;
                case "save":
                    if (WMPublic.sbMessageSaveChangeRequest(this) == true)
                    {
                        updateIOHeaderAsync();
                    }            
                    break;
                case "delete":           
                    break;
                case "search":

                    break;
                case "refesh":
                    sbLoadDataForGridIODetailAsync(textEditIONumber.Text);
                    break;
                case "import":
                    break;
                case "export":
                    break;
                case "print":
                    break;
                case "close":
                    break;
            }

        }
    }
}
