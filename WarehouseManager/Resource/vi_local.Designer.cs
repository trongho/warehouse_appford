﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WarehouseManager.Resource {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "16.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class vi_local {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal vi_local() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("WarehouseManager.Resource.vi_local", typeof(vi_local).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Hoạt động.
        /// </summary>
        internal static string active {
            get {
                return ResourceManager.GetString("active", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Thêm mới.
        /// </summary>
        internal static string addnew {
            get {
                return ResourceManager.GetString("addnew", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Địa chỉ.
        /// </summary>
        internal static string adress {
            get {
                return ResourceManager.GetString("adress", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bị khóa.
        /// </summary>
        internal static string blocked {
            get {
                return ResourceManager.GetString("blocked", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Chi nhánh.
        /// </summary>
        internal static string branch {
            get {
                return ResourceManager.GetString("branch", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Danh mục.
        /// </summary>
        internal static string category {
            get {
                return ResourceManager.GetString("category", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Đổi mật khẩu.
        /// </summary>
        internal static string change_password {
            get {
                return ResourceManager.GetString("change_password", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Chọn ngôn ngữ.
        /// </summary>
        internal static string choose_language {
            get {
                return ResourceManager.GetString("choose_language", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Xóa.
        /// </summary>
        internal static string delete {
            get {
                return ResourceManager.GetString("delete", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Mô tả.
        /// </summary>
        internal static string description {
            get {
                return ResourceManager.GetString("description", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Email.
        /// </summary>
        internal static string email {
            get {
                return ResourceManager.GetString("email", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tiếng Anh.
        /// </summary>
        internal static string english {
            get {
                return ResourceManager.GetString("english", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Thoát.
        /// </summary>
        internal static string exit {
            get {
                return ResourceManager.GetString("exit", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Xuất dữ liệu.
        /// </summary>
        internal static string export_data {
            get {
                return ResourceManager.GetString("export_data", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tên người dùng.
        /// </summary>
        internal static string fullname {
            get {
                return ResourceManager.GetString("fullname", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Hàng hóa.
        /// </summary>
        internal static string goods {
            get {
                return ResourceManager.GetString("goods", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Nhóm hàng.
        /// </summary>
        internal static string goods_category {
            get {
                return ResourceManager.GetString("goods_category", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ngành hàng.
        /// </summary>
        internal static string goods_line {
            get {
                return ResourceManager.GetString("goods_line", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Quản lý hàng hóa.
        /// </summary>
        internal static string goods_manager {
            get {
                return ResourceManager.GetString("goods_manager", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Loại hàng.
        /// </summary>
        internal static string goods_type {
            get {
                return ResourceManager.GetString("goods_type", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Trợ giúp.
        /// </summary>
        internal static string help {
            get {
                return ResourceManager.GetString("help", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Nhập dữ liệu.
        /// </summary>
        internal static string import_data {
            get {
                return ResourceManager.GetString("import_data", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Đăng nhập.
        /// </summary>
        internal static string login {
            get {
                return ResourceManager.GetString("login", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Đăng nhập.
        /// </summary>
        internal static string login_form {
            get {
                return ResourceManager.GetString("login_form", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Họ tên.
        /// </summary>
        internal static string login_name {
            get {
                return ResourceManager.GetString("login_name", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Đăng xuất.
        /// </summary>
        internal static string logout {
            get {
                return ResourceManager.GetString("logout", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Mật khẩu.
        /// </summary>
        internal static string password {
            get {
                return ResourceManager.GetString("password", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Số điện thoại.
        /// </summary>
        internal static string phone {
            get {
                return ResourceManager.GetString("phone", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Chức vụ.
        /// </summary>
        internal static string position {
            get {
                return ResourceManager.GetString("position", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to In ấn.
        /// </summary>
        internal static string print {
            get {
                return ResourceManager.GetString("print", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Làm mới.
        /// </summary>
        internal static string refesh {
            get {
                return ResourceManager.GetString("refesh", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Lưu.
        /// </summary>
        internal static string save {
            get {
                return ResourceManager.GetString("save", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tìm kiếm.
        /// </summary>
        internal static string search {
            get {
                return ResourceManager.GetString("search", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Hệ thống.
        /// </summary>
        internal static string system {
            get {
                return ResourceManager.GetString("system", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Người dùng.
        /// </summary>
        internal static string user {
            get {
                return ResourceManager.GetString("user", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Quản lý tài khoản.
        /// </summary>
        internal static string user_manager {
            get {
                return ResourceManager.GetString("user_manager", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tạo tài khoản và phân quyền.
        /// </summary>
        internal static string user_role {
            get {
                return ResourceManager.GetString("user_role", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Mã người dùng.
        /// </summary>
        internal static string userid {
            get {
                return ResourceManager.GetString("userid", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tài khoản.
        /// </summary>
        internal static string username {
            get {
                return ResourceManager.GetString("username", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tiếng Việt.
        /// </summary>
        internal static string vietnamese {
            get {
                return ResourceManager.GetString("vietnamese", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kho hàng.
        /// </summary>
        internal static string warehouse {
            get {
                return ResourceManager.GetString("warehouse", resourceCulture);
            }
        }
    }
}
