﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using WarehouseManager.Helpers;
using WarehouseManager.Models;
using WarehouseManager.Repository;

namespace WarehouseManager
{
    public partial class frmChangePW : DevExpress.XtraEditors.XtraForm
    {
        UserRepository userRepository = null;
        public frmChangePW()
        {
            InitializeComponent();
            userRepository = new UserRepository();
            //textEditOldPW.CausesValidation = false;
            //textEditNewPW.CausesValidation = false;
            //textEditReNewPW.CausesValidation = false;
        }

        private async void simpleButtonSave_Click(object sender, EventArgs e)
        {
            textEditOldPW.DoValidate();
            textEditNewPW.DoValidate();
            textEditReNewPW.DoValidate();

            List<User> users = await userRepository.getUserUnderUserIDAsync(WMMessage.User.UserID);
            users[0].Password = textEditNewPW.Text;
            if (await userRepository.updateUserAsync(users[0], users[0].UserID) > 0)
            {
                MessageBox.Show("Đổi mật khẩu thành công");
            }
        }

        private void simpleButtonCancel_Click(object sender, EventArgs e)
        {

        }

        private async void textEditOldPW_Validating(object sender, CancelEventArgs e)
        {
            TextEdit textEdit = sender as TextEdit;
            string editValue = textEdit.EditValue as string;
            List<User> users = await userRepository.getUserUnderUsernameAsync(WMMessage.User.UserName);
            if (editValue==null)
            {
                textEdit.ErrorText = "Không được để trống";
                textEdit.ErrorImageOptions.Image = svgImageCollection1.GetImage(0, new System.Drawing.Size(12, 12));
                e.Cancel = true;
            }
            else if (!users[0].Password.Equals(Helpers.HashingHelper.HashUsingPbkdf2(editValue,users[0].PasswordSalt)))
            {
                textEdit.ErrorText = "Mật khẩu cũ không đúng";
                textEdit.ErrorImageOptions.Image = svgImageCollection1.GetImage(0, new System.Drawing.Size(12, 12));
                e.Cancel = true;
            }
            
        }

        private void textEditNewPW_Validating(object sender, CancelEventArgs e)
        {
            TextEdit textEdit = sender as TextEdit;
            string editValue = textEdit.EditValue as string;
            if (editValue == null)
            {
                textEdit.ErrorText = "Không được để trống";
                textEdit.ErrorImageOptions.Image = svgImageCollection1.GetImage(0, new System.Drawing.Size(12, 12));
                e.Cancel = true;
            }
            else if (editValue.Equals(textEditOldPW.Text))
            {
                textEdit.ErrorText = "Trùng với mật khẩu cũ";
                textEdit.ErrorImageOptions.Image = svgImageCollection1.GetImage(0, new System.Drawing.Size(12, 12));
                e.Cancel = true;
            }
            else if (editValue.Length<6)
            {
                textEdit.ErrorText = "Chiều dài mật khẩu >=6 ký tự";
                textEdit.ErrorImageOptions.Image = svgImageCollection1.GetImage(0, new System.Drawing.Size(12, 12));
                e.Cancel = true;
            }
           
        }

        private void textEditReNewPW_Validating(object sender, CancelEventArgs e)
        {
            TextEdit textEdit = sender as TextEdit;
            string editValue = textEdit.EditValue as string;
            if (editValue == null)
            {
                textEdit.ErrorText = "Không được để trống";
                textEdit.ErrorImageOptions.Image = svgImageCollection1.GetImage(0, new System.Drawing.Size(12, 12));
                e.Cancel = true;
            }
            else if (editValue.Length < 6)
            {
                textEdit.ErrorText = "Chiều dài mật khẩu >=6 ký tự";
                textEdit.ErrorImageOptions.Image = svgImageCollection1.GetImage(0, new System.Drawing.Size(12, 12));
                e.Cancel = true;
            }
            else if (!editValue.Equals(textEditNewPW.Text))
            {
                textEdit.ErrorText = "Nhập lại mật khẩu mới không đúng";
                textEdit.ErrorImageOptions.Image = svgImageCollection1.GetImage(0, new System.Drawing.Size(12, 12));
                e.Cancel = true;
            }
            
        }

        
    }
}