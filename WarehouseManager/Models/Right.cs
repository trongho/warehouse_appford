﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WarehouseManager.Models
{
    public class Right
    {
        public String RightID { get; set; }
        public int? Ordinal { get; set; }
        public String? RightName { get; set; }
        public String? RightNameEN { get; set; }
        public Int16? GrantRight { get; set; }


    }
}
