﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WarehouseManager.Models
{
    public class DataPrintGeneral
    {
        public String DataPrintNumber { get; set; }
        public String GoodsID { get; set; }
        public int Ordinal { get; set; }
        public String? GoodsName { get; set; }
        public Decimal? Quantity { get; set; }
        public String? Status { get; set; }
        public Decimal? PackingVolume { get; set; }
        public Decimal? PackingQuantity { get; set; }
    }
}
