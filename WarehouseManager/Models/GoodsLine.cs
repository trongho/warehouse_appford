﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarehouseManager.Models
{
    public class GoodsLine
    {
        public String GoodsLineID { get; set; }
        public String? GoodsLineName { get; set; }
        public String? Description { get; set; }
        public String? Status { get; set; }
        public String? CreatedUserID { get; set; }
        public DateTime? CreateDate { get; set; }
        public String? UpdatedUserID { get; set; }
        public DateTime? UpdatedDate { get; set; }

        public GoodsLine(string goodsLineID, string goodsLineName, string description, string status, string createdUserID, DateTime? createDate, string updatedUserID, DateTime? updatedDate)
        {
            GoodsLineID = goodsLineID;
            GoodsLineName = goodsLineName;
            Description = description;
            Status = status;
            CreatedUserID = createdUserID;
            CreateDate = createDate;
            UpdatedUserID = updatedUserID;
            UpdatedDate = updatedDate;
        }

        public GoodsLine()
        {
        }
    }
}
