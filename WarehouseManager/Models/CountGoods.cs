﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarehouseManager.Models
{
    class CountGoods
    {
        public String LocationID { get; set; }
        public String GoodsID { get; set; }
        public String? GoodsName { get; set; }
        public Decimal? Quantity { get; set; }
        public String? Status { get; set; }
        public DateTime? CountDate { get; set; }
    }
}
