﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarehouseManager.Models
{
    class UserRight
    {
        public String UserID { get; set; }
        public Int16? Ordinal { get; set; }
        public String? RightID { get; set; }
        public String? RightName { get; set; }
        public String? RightNameEN { get; set; }
        public Int16? GrantRight { get; set; }

        public UserRight(string userID, Int16? ordinal, string? rightID, string? rightName, string? rightNameEN, short? grantRight)
        {
            UserID = userID;
            Ordinal = ordinal;
            RightID = rightID;
            RightName = rightName;
            RightNameEN = rightNameEN;
            GrantRight = grantRight;
        }

        public UserRight()
        {
        }
    }


}
