﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WarehouseManager.Models
{
    public class WIRDetail
    {
        public String WIRNumber { get; set; }
        public int Ordinal { get; set; }
        public String GoodsID { get; set; }
        public String GoodsGroupID { get; set; }
        public String? PickerName { get; set; }
        public String? GoodsName { get; set; }
        public String? OtherGoodsName { get; set; }
        public Decimal? PackingVolume { get; set; }
        public Decimal? TotalQuantity { get; set; }
        public Decimal? QuantityByItem { get; set; }
        public Decimal? QuantityByPack { get; set; }
        public String? LocationID { get; set; }
        public Int16? ScanOption { get; set; }
        public String? Note { get; set; }
        public String? Status { get; set; }
    }
}
