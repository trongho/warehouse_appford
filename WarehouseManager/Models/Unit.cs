﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WarehouseManager.Models
{
    public class Unit
    {
      public String UnitID{get;set;}
      public Double? UnitRate{get;set;}
      public String? StockUnitID{get;set;}
      public Int16? Type{get;set;}
      public String? Description{get;set;}
      public String? Status{get;set;}
      public String? CreatedUserID{get;set;}
      public DateTime? CreatedDate{get;set;}
      public String? UpdatedUserID{get;set;}
      public DateTime? UpdatedDate{get;set;}
    }
}
