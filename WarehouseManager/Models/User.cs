﻿using Microsoft.Build.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;
using Xunit.Sdk;
using RequiredAttribute = System.ComponentModel.DataAnnotations.RequiredAttribute;

namespace WarehouseManager.Models
{
    public class User
    {
        public String UserID { get; set; }

        public String Role { get; set; }
        public String UserName { get; set; }
        public String? Adress { get; set; }
        public String? Position { get; set; }
        public String? BranchID { get; set; }
        public String? BranchName { get; set; }
        public String? Email { get; set; }
        public String LoginName { get; set; }
        public String? Password { get; set; }
        public String? PasswordSalt { get; set; }
        public String? Description { get; set; }
        public String? Status { get; set; }
        public String? CreatedUserID { get; set; }
        public DateTime? CreateDate { get; set; }
        public String? UpdatedUserID { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public bool Active { get; set; }
        public bool Blocked { get; set; }

        public User(string userID, string userName, string adress, string position, string branchID, string branchName, string email, string loginName, string password, string passwordSalt, string description, string status, string createdUserID, DateTime? createDate, string updatedUserID, DateTime? updatedDate, bool active, bool blocked)
        {
            UserID = userID;
            UserName = userName;
            Adress = adress;
            Position = position;
            BranchID = branchID;
            BranchName = branchName;
            Email = email;
            LoginName = loginName;
            Password = password;
            PasswordSalt = passwordSalt;
            Description = description;
            Status = status;
            CreatedUserID = createdUserID;
            CreateDate = createDate;
            UpdatedUserID = updatedUserID;
            UpdatedDate = updatedDate;
            Active = active;
            Blocked = blocked;
        }

        public User()
        {
        }
    }
}
