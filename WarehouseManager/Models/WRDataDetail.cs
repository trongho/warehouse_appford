﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WarehouseManager.Models
{
    public class WRDataDetail
    {
        public String WRDNumber { get; set; }
        public int Ordinal { get; set; }
        public String GoodsID { get; set; }
        public String? GoodsName { get; set; }
        public String? IDCode { get; set; }
        public String? LocationID { get; set; }
        public Decimal? Quantity { get; set; }
        public Decimal? TotalQuantity { get; set; }
        public Decimal? TotalGoods { get; set; }
        public Decimal? QuantityOrg { get; set; }
        public Decimal? TotalQuantityOrg { get; set; }
        public Decimal? TotalGoodsOrg { get; set; }
        public String? LocationIDOrg { get; set; }
        public String? CreatorID { get; set; }
        public String? CreatedDateTime { get; set; }
        public String? EditerID { get; set; }
        public String? EditedDateTime { get; set; }
        public String? Status { get; set; }
        public Decimal? PackingVolume { get; set; }
        public Decimal? QuantityByPack { get; set; }
        public Decimal? QuantityByItem { get; set; }
        public String? Note { get; set; }
        public Int16? ScanOption { get; set; }
        public Decimal? PackingQuantity { get; set; }
        public String? SupplierCode { get; set; }
        public String? ASNNumber { get; set; }
        public String? PackingSlip { get; set; }

        public WRDataDetail(string wRDNumber, int ordinal, string goodsID, string goodsName, string iDCode, string locationID, decimal? quantity, decimal? totalQuantity, decimal? totalGoods, decimal? quantityOrg, decimal? totalQuantityOrg, decimal? totalGoodsOrg, string locationIDOrg, string creatorID, string createdDateTime, string editerID, string editedDateTime, string status, decimal? packingVolume, decimal? quantityByPack, decimal? quantityByItem, string note, short? scanOption, decimal? packingQuantity, string supplierCode, string aSNNumber, string packingSlip) : this(wRDNumber, ordinal, goodsID)
        {
            GoodsName = goodsName;
            IDCode = iDCode;
            LocationID = locationID;
            Quantity = quantity;
            TotalQuantity = totalQuantity;
            TotalGoods = totalGoods;
            QuantityOrg = quantityOrg;
            TotalQuantityOrg = totalQuantityOrg;
            TotalGoodsOrg = totalGoodsOrg;
            LocationIDOrg = locationIDOrg;
            CreatorID = creatorID;
            CreatedDateTime = createdDateTime;
            EditerID = editerID;
            EditedDateTime = editedDateTime;
            Status = status;
            PackingVolume = packingVolume;
            QuantityByPack = quantityByPack;
            QuantityByItem = quantityByItem;
            Note = note;
            ScanOption = scanOption;
            PackingQuantity = packingQuantity;
            SupplierCode = supplierCode;
            ASNNumber = aSNNumber;
            PackingSlip = packingSlip;
        }

        public WRDataDetail()
        {
        }

        public WRDataDetail(string wRDNumber, int ordinal, string goodsID)
        {
            WRDNumber = wRDNumber;
            Ordinal = ordinal;
            GoodsID = goodsID;
        }
    }


}
