﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarehouseManager.Models
{
    class DataPrintDetail
    {
        public String DataPrintNumber { get; set; }
        public String GoodsID { get; set; }
        public int Ordinal { get; set; }
        public Decimal? Quantity { get; set; }
        public String? Status { get; set; }
        public String? DateString { get; set; }
    }
}
