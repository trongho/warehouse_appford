﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WarehouseManager.Models
{
    public class WIDataGeneral
    {
        public String WIDNumber { get; set; }
        public int Ordinal { get; set; }
        public String GoodsID { get; set; }
        public String GoodsGroupID { get; set; }
        public String? PickerName { get; set; }
        public String? GoodsName { get; set; }
        public String? IDCode { get; set; }
        public String? LocationID { get; set; }
        public Decimal? Quantity { get; set; }
        public Decimal? TotalQuantity { get; set; }
        public Decimal? TotalGoods { get; set; }
        public Decimal? QuantityOrg { get; set; }
        public Decimal? TotalQuantityOrg { get; set; }
        public Decimal? TotalGoodsOrg { get; set; }
        public String? LocationIDOrg { get; set; }
        public String? CreatorID { get; set; }
        public String? CreatedDateTime { get; set; }
        public String? EditerID { get; set; }
        public String? EditedDateTime { get; set; }
        public String? Status { get; set; }
        public Decimal? PackingVolume { get; set; }
        public Decimal? QuantityByPack { get; set; }
        public Decimal? QuantityByItem { get; set; }
        public String? Note { get; set; }
        public Int16? ScanOption { get; set; }
        public Decimal? PackingQuantity { get; set; }
    }
}
