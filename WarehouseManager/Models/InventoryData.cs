﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarehouseManager.Models
{
    class InventoryData
    {
        public String GoodsID { get; set; }
        public String? GoodsName { get; set; }
        public Decimal? Quantity { get; set; }
        public Decimal? Amount { get; set; }
        public String? LocationID { get; set; }
        public String? Description { get; set; }
        public String? Status { get; set; }
        public String? CreatedUserID { get; set; }
        public DateTime? CreatedDate { get; set; }
        public String? UpdatedUserID { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
}
