﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WarehouseManager.Models
{
    public class UserRightOnReport
    {
        public String UserID { get; set; }
        public String ReportID { get; set; }
        public String? ReportName { get; set; }
        public String? ReportNameEN { get; set; }
        public Int16? Ordinal { get; set; }
        public Int16? ViewRight { get; set; }
        public Int16? PrintRight { get; set; }
    }
}
