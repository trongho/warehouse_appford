﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WarehouseManager.Models
{
    public class IODetail
    {
        public String IONumber { get; set; }
        public int Ordinal { get; set; }
        public String GoodsID { get; set; }
        public String? GoodsName { get; set; }
        public String? IssueUnitID { get; set; }
        public Decimal? UnitRate { get; set; }
        public String? StockUnitID { get; set; }
        public Decimal? Quantity { get; set; }
        public Decimal? PriceIncludedVAT { get; set; }
        public Decimal? PriceExcludedVAT { get; set; }
        public Decimal? Price { get; set; }
        public Decimal? Discount { get; set; }
        public Decimal? DiscountAmount { get; set; }
        public Decimal? VAT { get; set; }
        public Decimal? VATAmount { get; set; }
        public Decimal? AmountExcludedVAT { get; set; }
        public Decimal? AmountIncludedVAT { get; set; }
        public Decimal? Amount { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public String? SerialNumber { get; set; }
        public String? Note { get; set; }
        public Decimal? QuantityIssued { get; set; }
        public Decimal? PriceIncludedVATIssued { get; set; }
        public Decimal? PriceExcludedVATIssued { get; set; }
        public Decimal? PriceIssued { get; set; }
        public Decimal? DiscountIssued { get; set; }
        public Decimal? DiscountAmountIssued { get; set; }
        public Decimal? VATIssued { get; set; }
        public Decimal? VATAmountIssued { get; set; }
        public Decimal? AmountExcludedVATIssued { get; set; }
        public Decimal? AmountIncludedVATIssued { get; set; }
        public Decimal? AmountIssued { get; set; }
        public Decimal? QuantityOrdered { get; set; }
        public Decimal? OrderQuantityIssued { get; set; }
        public Decimal? IssueQuantityReceived { get; set; }
        public Decimal? IssueQuantity { get; set; }
        public Decimal? IssueAmount { get; set; }
        public Decimal? AverageCost { get; set; }
        public String? SPSNumber { get; set; }
        public String? PSNumberSalePrice { get; set; }
        public String? PSNumberDiscount { get; set; }
        public String? Status { get; set; }
        public String? SupplierCode { get; set; }
        public String? ASNNumber { get; set; }
        public String? PackingSlip { get; set; }
    }
}
