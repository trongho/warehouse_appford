﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WarehouseManager.Models
{
    public class Screen
    {
        public String ScreenID { get; set; }
        public String? ScreenName { get; set; }
        public String? ScreenNameEN { get; set; }
        public Int16? Ordinal { get; set; }
        public Int16? SaveNewRight { get; set; }
        public Int16? SaveChangeRight { get; set; }
        public Int16? DeleteRight { get; set; }
        public Int16? PrintRight { get; set; }
        public Int16? ImportRight { get; set; }
        public Int16? ExportRight { get; set; }
    }
}
