﻿using DevExpress.Utils;
using DevExpress.XtraBars.Docking2010;
using DevExpress.XtraGrid.Accessibility;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraSplashScreen;
using DevExpress.XtraWaitForm;
using OfficeOpenXml;
using OfficeOpenXml.FormulaParsing.Excel.Operators;
using OfficeOpenXml.Style;
using QRCoder;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Media;
using WarehouseManager.Models;
using WarehouseManager.Repository;
using webservice.Entities;

namespace WarehouseManager
{
    public partial class ReceiptDataControl : DevExpress.XtraEditors.XtraUserControl
    {
        WRDataHeaderRepository wRDataHeaderRepository;
        WRDataDetailRepository wRDataDetailRepository;
        WRDataGeneralRepository wRDataGeneralRepository;
        WRRHeaderRepository wRRHeaderRepository;
        WRRDetailRepository wRRDetailRepository;
        HandlingStatusRepository handlingStatusRepository;
        WRHeaderRepository wRHeaderRepository;
        WRDetailRepository wRDetailRepository;
        public static String selectedWRDNumber;
        Timer t = new Timer();
        Boolean isGreen = false;
        Boolean isYellow = false;
        Boolean isRed = false;
        Boolean isGreenTotal = false;
        Boolean isYellowTotal = false;
        Boolean isRedTotal = false;
        Boolean isChange = false;
        String goodsIDChange = "";
        String checkQuantityString = "";
        String checkTotalQuantityString = "";

        public ReceiptDataControl()
        {
            InitializeComponent();
            this.Load += ReceiptDataControl_Load;
            wRDataHeaderRepository = new WRDataHeaderRepository();
            wRDataDetailRepository = new WRDataDetailRepository();
            wRDataGeneralRepository = new WRDataGeneralRepository();
            wRRHeaderRepository = new WRRHeaderRepository();
            wRRDetailRepository = new WRRDetailRepository();
            wRHeaderRepository = new WRHeaderRepository();
            wRDetailRepository = new WRDetailRepository();
            handlingStatusRepository = new HandlingStatusRepository();
            popupMenuSelectHandlingStatus();
            sbLoadDataForGridWRDataGeneralAsync(textEditWRDNumber.Text);

        }

        private void ReceiptDataControl_Load(Object sender, EventArgs e)
        {
            //textEditTotalQuantity.TextChanged += new System.EventHandler(this.textBox_TextChanged);

            //gridViewWRDataDetail.CellValueChanged += gridview_TextChanged;
            textEditTotalQuantity.TextChanged += TextBox_TextChangedAsync;

            windowsUIButtonPanel1.ButtonClick += button_Click;
            windowsUIButtonPanel1.Cursor = Cursors.Hand;
            textEditWRDNumber.KeyDown += new KeyEventHandler(WRDNumber_KeyDown);
            textEditWRDNumber.DoubleClick += textEditWDRNumer_CellDoubleClick;
            gridViewWRDataDetail.CustomDrawCell += grdData_CustomDrawCell;
            gridViewWRDataDetail.CustomColumnDisplayText += GridView_CustomColumnDisplayText;
            simpleButtonOn.Click += butOn_Click;
            simpleButtonOff.Click += butOff_Click;
            t.Tick += new EventHandler(this.Timer_Tick);
            t.Start();


        }

        private void TextBox_TextChangedAsync(object Sender, EventArgs e)
        {
            checkQuantityAsync();
        }

        private void GridView_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {

            if (e.Column == colNo)
            {
                int rowHandle = gridViewWRDataDetail.GetRowHandle(e.ListSourceRowIndex);
                if (rowHandle >= 0)
                {
                    e.DisplayText = (rowHandle + 1).ToString();
                }
            }
        }

        private async Task popupMenuSelectHandlingStatus()
        {
            List<HandlingStatus> handlingStatuses = await handlingStatusRepository.GetAll();
            foreach (HandlingStatus handlingStatus in handlingStatuses)
            {
                comboBoxEditHandlingStatus.Properties.Items.Add(handlingStatus.HandlingStatusName);
            }
            comboBoxEditHandlingStatus.SelectedIndex = 1;
        }

        //private async Task sbLoadDataForGridWRDataDeteilAsync(String wRDNumber)
        //{
        //    List<WRDataDetail> wRDataDetails = await wRDataDetailRepository.GetUnderID(wRDNumber);
        //    int mMaxRow = 100;
        //    if (wRDataDetails.Count < mMaxRow)
        //    {
        //        int num = mMaxRow - wRDataDetails.Count;
        //        int i = 1;
        //        while (true)
        //        {
        //            int num2 = i;
        //            int num3 = num;
        //            if (num2 > num3)
        //            {
        //                break;
        //            }
        //            wRDataDetails.Add(new WRDataDetail());
        //            i++;
        //        }
        //    }
        //    gridControlWRDataDetail.DataSource = wRDataDetails;
        //    getTotalQuantityOrg();
        //    getTotalQuantity();
        //    getPackingQuantity();
        //    textTotalQuantity_CustomDraw();

        //}

        private async Task sbLoadDataForGridWRDataGeneralAsync(String wRDNumber)
        {
            List<WRDataGeneral> wRDataGenerals = await wRDataGeneralRepository.GetUnderID(wRDNumber);
            if (wRDataGenerals.Count == 0)
            {
                do
                {
                    wRDataGenerals.Add(new WRDataGeneral());
                }
                while (wRDataGenerals.Count<100);
            }         

            gridControlWRDataDetail.DataSource = wRDataGenerals;
            getTotalQuantityOrg();
            getTotalQuantity();
            getPackingQuantity();
            getTotalGoodsOrgAsync();
            getTotalGoods();
            textTotalQuantity_CustomDraw();
        }

        //private void textBox_TextChanged(object sender, EventArgs e)
        //{
        //    checkLight();
        //}

        private void gridview_TextChanged(object sender, CellValueChangedEventArgs e)
        {

            //if (gridViewWRDataDetail.GetRowCellValue(gridViewWRDataDetail.FocusedRowHandle,"Quantity") == null)
            //    return;
            //if (gridViewWRDataDetail.GetRowCellValue(gridViewWRDataDetail.FocusedRowHandle, "QuantityOrg") == null)
            //    return;
            //Decimal quantity=(Decimal)gridViewWRDataDetail.GetRowCellValue(e.RowHandle, "Quantity");
            //Decimal quantityOrg = (Decimal)gridViewWRDataDetail.GetRowCellValue(e.RowHandle, "QuantityOrg");
            //if (quantity == quantityOrg)
            //{
            //    Helpers.RGBLightHelper.turnOnGreen();
            //    Helpers.Logger.WriteLogger(isGreen + "-" + isYellow + "-" + isRed + "-" + isGreenTotal);
            //}
            //else if (quantity < quantityOrg)
            //{
            //    Helpers.RGBLightHelper.turnOnYellow();
            //    Helpers.Logger.WriteLogger(isGreen + "-" + isYellow + "-" + isRed + "-" + isGreenTotal);
            //}
            //else if (quantity > quantityOrg)
            //{
            //    Helpers.RGBLightHelper.turnOnRed();
            //    Helpers.Logger.WriteLogger(isGreen + "-" + isYellow + "-" + isRed + "-" + isGreenTotal);
            //}
            //goodsIDChange = (String)gridViewWRDataDetail.GetRowCellValue(e.RowHandle, "GoodsID");

        }



        private void checkLight()
        {

            if (isGreen == true)
            {
                Helpers.RGBLightHelper.turnOnGreen();
                Helpers.Logger.WriteLogger(isGreen + "-" + isYellow + "-" + isRed + "-" + isGreenTotal);
            }
            else if (isYellow == true)
            {
                Helpers.RGBLightHelper.turnOnYellow();
                Helpers.Logger.WriteLogger(isGreen + "-" + isYellow + "-" + isRed + "-" + isGreenTotal);
            }
            else if (isRed == true)
            {
                Helpers.RGBLightHelper.turnOnRed();
                Helpers.Logger.WriteLogger(isGreen + "-" + isYellow + "-" + isRed + "-" + isGreenTotal);
            }
            else if (isGreenTotal == true)
            {
                Helpers.RGBLightHelper.turnOnGreenLampon();
                Helpers.Logger.WriteLogger(isGreen + "-" + isYellow + "-" + isRed + "-" + isGreenTotal);
            }

        }

        private async Task checkQuantityAsync()
        {
            checkQuantityString = await wRDataGeneralRepository.CheckQuantity(textEditWRDNumber.Text, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            checkTotalQuantityString = await wRDataHeaderRepository.CheckTotalQuantity(textEditWRDNumber.Text);
            if (checkTotalQuantityString == "green")
            {
                Helpers.RGBLightHelper.turnOnGreenLampon();
                System.Threading.Thread.Sleep(3000);
                Helpers.RGBLightHelper.turnOff();
                Helpers.Logger.WriteLogger("Light " + checkQuantityString + "-Total light " + checkTotalQuantityString);
            }
            else if (checkQuantityString == "green")
            {
                Helpers.RGBLightHelper.turnOnGreen();
                System.Threading.Thread.Sleep(3000);
                Helpers.RGBLightHelper.turnOff();
                Helpers.Logger.WriteLogger("Light " + checkQuantityString + "-Total light " + checkTotalQuantityString);
            }
            else if (checkQuantityString == "red")
            {
                Helpers.RGBLightHelper.turnOnRed();
                System.Threading.Thread.Sleep(3000);
                Helpers.RGBLightHelper.turnOff();
                Helpers.Logger.WriteLogger("Light " + checkQuantityString + "-Total light " + checkTotalQuantityString);
            }
            else if (checkQuantityString == "yellow")
            {
                Helpers.RGBLightHelper.turnOnYellow();
                System.Threading.Thread.Sleep(3000);
                Helpers.RGBLightHelper.turnOff();
                Helpers.Logger.WriteLogger("Light " + checkQuantityString + "-Total light " + checkTotalQuantityString);
            }
        }

        private void getPackingQuantity()
        {
            Decimal packingQuantity = 0;
            int i = 0;
            while (true)
            {
                int rowHandle = gridViewWRDataDetail.GetRowHandle(i);
                if (gridViewWRDataDetail.GetRowCellValue(rowHandle, "GoodsID") == null)
                {
                    break;
                }
                Decimal quantityOrg = (Decimal)gridViewWRDataDetail.GetRowCellValue(rowHandle, "QuantityOrg");
                Decimal quantity = (Decimal)gridViewWRDataDetail.GetRowCellValue(rowHandle, "Quantity");
                Decimal quantityByPack = (Decimal)gridViewWRDataDetail.GetRowCellValue(rowHandle, "QuantityByPack");
                Decimal packingVolume = quantityOrg / quantityByPack;
                if (quantity % packingVolume != 0)
                {
                    packingQuantity = Math.Floor(quantity / packingVolume) + 1;
                }
                else
                {
                    packingQuantity = quantity / packingVolume;
                }

                gridViewWRDataDetail.SetRowCellValue(rowHandle, "PackingQuantity", packingQuantity);
                i++;
            }
        }




        private void getTotalQuantityOrg()
        {
            Decimal totalQuantityOrg = 0;
            int i = 0;
            while (true)
            {
                int rowHandle = gridViewWRDataDetail.GetRowHandle(i);
                if (gridViewWRDataDetail.GetRowCellValue(rowHandle, "GoodsID") == null)
                {
                    break;
                }
                Decimal quantityOrg = (Decimal)gridViewWRDataDetail.GetRowCellValue(rowHandle, "QuantityOrg");
                totalQuantityOrg += quantityOrg;
                i++;
            }
            textEditTotalQuantityOrg.Text = totalQuantityOrg.ToString("0.#####");
        }

        private void getTotalQuantity()
        {
            Decimal totalQuantity = 0;
            int i = 0;
            while (true)
            {
                int rowHandle = gridViewWRDataDetail.GetRowHandle(i);
                if (gridViewWRDataDetail.GetRowCellValue(rowHandle, "GoodsID") == null)
                {
                    break;
                }
                Decimal quantity = (Decimal)gridViewWRDataDetail.GetRowCellValue(rowHandle, "Quantity");
                totalQuantity += quantity;
                i++;
            }
            textEditTotalQuantity.Text = totalQuantity.ToString("0.#####");
        }

        private async Task getTotalGoodsOrgAsync()
        {
            Decimal totalGoodsOrg = 0;
            List<WRDataGeneral> wRDataGenerals = await wRDataGeneralRepository.GetUnderID(textEditWRDNumber.Text);
            totalGoodsOrg += Convert.ToDecimal(wRDataGenerals.Count);
            textEditTotalGoodsOrg.Text = totalGoodsOrg.ToString("0.#####");
        }

        private void getTotalGoods()
        {
            Decimal totalGoods = 0;
            int i = 0;
            while (true)
            {
                int rowHandle = gridViewWRDataDetail.GetRowHandle(i);
                if (gridViewWRDataDetail.GetRowCellValue(rowHandle, "GoodsID") == null)
                {
                    break;
                }
                if ((Decimal)gridViewWRDataDetail.GetRowCellValue(rowHandle, "Quantity") > 0)
                {
                    totalGoods++;
                }
                i++;
            }
            textEditTotalGood.Text = totalGoods.ToString("0.#####");
        }

        private void QRCodeGeneral(String code)
        {
            QRCodeGenerator.ECCLevel eccLevel = QRCodeGenerator.ECCLevel.Q;
            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeData qrCodeData = qrGenerator.CreateQrCode(code, eccLevel);
            QRCode qrCode = new QRCode(qrCodeData);
            Bitmap qrCodeBitmap = qrCode.GetGraphic(2, System.Drawing.Color.Black, System.Drawing.Color.White, new Bitmap(10, 10));
            pictureBox1.Image = qrCodeBitmap;
            Helpers.ImageHelper.resizeImage(pictureBox1.Image, new Size(100, 100));
            pictureBox1.SizeMode = PictureBoxSizeMode.CenterImage;

            //QRCodeGenerator.ECCLevel eccLevel = QRCodeGenerator.ECCLevel.L;
            //using (QRCodeGenerator qrGenerator = new QRCodeGenerator())
            //{
            //    using (QRCodeData qrCodeData = qrGenerator.CreateQrCode(code, eccLevel))
            //    {
            //        using (QRCode qrCode = new QRCode(qrCodeData))
            //        {

            //            pictureBox1.BackgroundImage = qrCode.GetGraphic(20);

            //            this.pictureBox1.Size = new System.Drawing.Size(pictureBox1.Width, pictureBox1.Height);
            //            //Set the SizeMode to center the image.
            //            this.pictureBox1.SizeMode = PictureBoxSizeMode.CenterImage;

            //            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            //        }
            //    }
            //}
        }

        public async Task updateWRDataHeaderAsync()
        {
            WRDataHeader wRDataHeader = await wRDataHeaderRepository.GetUnderID(textEditWRDNumber.Text);
            if (wRDataHeader.HandlingStatusID.Equals("2"))
            {
                MessageBox.Show("Dữ liệu nhập đã duyệt mức 2, không thể chỉnh sửa");
                return;
            }

            Decimal totalQuantityOrg = Decimal.Parse(textEditTotalQuantityOrg.Text);
            Decimal totalQuantity = Decimal.Parse(textEditTotalQuantity.Text);
            Decimal subQuantity = totalQuantityOrg - totalQuantity;
            if (subQuantity == totalQuantityOrg)
            {
                wRDataHeader.Status = "0";
            }
            else if (subQuantity < totalQuantityOrg)
            {
                wRDataHeader.Status = "1";
            }
            else if (subQuantity == 0)
            {
                wRDataHeader.Status = "2";
            }
            else
            {
                wRDataHeader.Status = "3";
            }


            wRDataHeader.WRDNumber = textEditWRDNumber.Text;
            wRDataHeader.WRDDate = DateTime.Parse(dateEditWRDDate.DateTime.ToString("yyyy-MM-dd"));
            wRDataHeader.ReferenceNumber = textEditWRDNumber.Text;
            wRDataHeader.WRRNumber = textEditWRDNumber.Text;
            wRDataHeader.WRRReference = textEditWRDNumber.Text;
            wRDataHeader.HandlingStatusID = (comboBoxEditHandlingStatus.SelectedIndex - 1).ToString();
            wRDataHeader.HandlingStatusName = comboBoxEditHandlingStatus.SelectedItem.ToString();
            wRDataHeader.Note = "";
            wRDataHeader.TotalQuantity = Decimal.Parse(textEditTotalQuantity.Text);
            wRDataHeader.TotalQuantityOrg = Decimal.Parse(textEditTotalQuantityOrg.Text);
            wRDataHeader.UpdatedUserID = WMMessage.User.UserID;
            wRDataHeader.UpdatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));

            if (await wRDataHeaderRepository.Update(wRDataHeader, textEditWRDNumber.Text) > 0)
            {
                WMPublic.sbMessageSaveChangeSuccess(this);
                if (comboBoxEditHandlingStatus.SelectedIndex == 3)
                {
                    createWRHeaderAsync();
                }
            }

        }

        public async Task updateWRDataDetailAsync()
        {
            int num = gridViewWRDataDetail.RowCount - 1;
            int i = 0;
            while (true)
            {
                int num2 = i;
                int num3 = num;
                if (num2 > num3 || gridViewWRDataDetail.GetRowCellValue(i, gridViewWRDataDetail.Columns["colGoodsID"]) == "")
                {
                    break;
                }
                int rowHandle = gridViewWRDataDetail.GetRowHandle(i);
                WRDataDetail wRDataDetail = await wRDataDetailRepository.GetUnderMultiID(textEditWRDNumber.Text, (string?)gridViewWRDataDetail.GetRowCellValue(rowHandle, "GoodsID"), i + 1);
                wRDataDetail.WRDNumber = textEditWRDNumber.Text;
                wRDataDetail.GoodsID = (string?)gridViewWRDataDetail.GetRowCellValue(rowHandle, "GoodsID");
                wRDataDetail.GoodsName = (string?)gridViewWRDataDetail.GetRowCellValue(rowHandle, "GoodsName");
                wRDataDetail.Ordinal = wRDataDetail.Ordinal;
                wRDataDetail.IDCode = wRDataDetail.IDCode;
                wRDataDetail.LocationID = (string?)gridViewWRDataDetail.GetRowCellValue(rowHandle, "LocationID");
                wRDataDetail.Quantity = (Decimal?)gridViewWRDataDetail.GetRowCellValue(rowHandle, "Quantity");
                wRDataDetail.TotalQuantity = (Decimal?)gridViewWRDataDetail.GetRowCellValue(rowHandle, "TotalQuantity");
                wRDataDetail.TotalGoods = (Decimal?)gridViewWRDataDetail.GetRowCellValue(rowHandle, "TotalGoods");
                wRDataDetail.QuantityOrg = (Decimal?)gridViewWRDataDetail.GetRowCellValue(rowHandle, "QuantityOrg");
                wRDataDetail.TotalQuantityOrg = (Decimal?)gridViewWRDataDetail.GetRowCellValue(rowHandle, "TotalQuantityOrg");
                wRDataDetail.TotalGoodsOrg = (Decimal?)gridViewWRDataDetail.GetRowCellValue(rowHandle, "TotalGoodsOrg");
                wRDataDetail.LocationIDOrg = (string?)gridViewWRDataDetail.GetRowCellValue(rowHandle, "LocationIDOrg");
                wRDataDetail.EditerID = WMMessage.User.UserID; ;
                wRDataDetail.EditedDateTime = DateTime.Now.ToString("yyyy-MM-dd");
                wRDataDetail.Status = (string?)gridViewWRDataDetail.GetRowCellValue(rowHandle, "Status");
                wRDataDetail.PackingVolume = (Decimal?)gridViewWRDataDetail.GetRowCellValue(rowHandle, "PackingVolume");
                wRDataDetail.QuantityByPack = (Decimal?)gridViewWRDataDetail.GetRowCellValue(rowHandle, "QuantityByPack");
                wRDataDetail.QuantityByItem = (Decimal?)gridViewWRDataDetail.GetRowCellValue(rowHandle, "QuantityByItem");
                wRDataDetail.Note = "";
                wRDataDetail.ScanOption = (Int16?)gridViewWRDataDetail.GetRowCellValue(rowHandle, "ScanOption");
                wRDataDetail.PackingQuantity = (Decimal?)gridViewWRDataDetail.GetRowCellValue(rowHandle, "PackingQuantity");


                if (await wRDataDetailRepository.Update(wRDataDetail, textEditWRDNumber.Text, wRDataDetail.GoodsID, wRDataDetail.Ordinal) > 0)
                {
                    i++;
                }
            }
        }

        public async Task updateWRDataGeneralAsync()
        {
            int num = gridViewWRDataDetail.RowCount - 1;
            int i = 0;
            while (true)
            {
                int num2 = i;
                int num3 = num;
                if (num2 > num3 || gridViewWRDataDetail.GetRowCellValue(i, gridViewWRDataDetail.Columns["colGoodsID"]) == "")
                {
                    break;
                }
                int rowHandle = gridViewWRDataDetail.GetRowHandle(i);
                WRDataGeneral wRDataGeneral = new WRDataGeneral();
                wRDataGeneral.WRDNumber = textEditWRDNumber.Text;
                wRDataGeneral.GoodsID = (string?)gridViewWRDataDetail.GetRowCellValue(rowHandle, "GoodsID");
                wRDataGeneral.GoodsName = (string?)gridViewWRDataDetail.GetRowCellValue(rowHandle, "GoodsName");
                wRDataGeneral.Ordinal = i + 1;
                wRDataGeneral.IDCode = "";
                wRDataGeneral.LocationID = (string?)gridViewWRDataDetail.GetRowCellValue(rowHandle, "LocationID");
                wRDataGeneral.Quantity = (Decimal?)gridViewWRDataDetail.GetRowCellValue(rowHandle, "Quantity");
                wRDataGeneral.TotalQuantity = (Decimal?)gridViewWRDataDetail.GetRowCellValue(rowHandle, "TotalQuantity");
                wRDataGeneral.TotalGoods = (Decimal?)gridViewWRDataDetail.GetRowCellValue(rowHandle, "TotalGoods");
                wRDataGeneral.QuantityOrg = (Decimal?)gridViewWRDataDetail.GetRowCellValue(rowHandle, "QuantityOrg");
                wRDataGeneral.TotalQuantityOrg = (Decimal?)gridViewWRDataDetail.GetRowCellValue(rowHandle, "TotalQuantityOrg");
                wRDataGeneral.TotalGoodsOrg = (Decimal?)gridViewWRDataDetail.GetRowCellValue(rowHandle, "TotalGoodsOrg");
                wRDataGeneral.LocationIDOrg = (string?)gridViewWRDataDetail.GetRowCellValue(rowHandle, "LocationIDOrg");
                wRDataGeneral.EditerID = WMMessage.User.UserID; ;
                wRDataGeneral.EditedDateTime = DateTime.Now;
                wRDataGeneral.Status = (string?)gridViewWRDataDetail.GetRowCellValue(rowHandle, "Status");
                wRDataGeneral.PackingVolume = (Decimal?)gridViewWRDataDetail.GetRowCellValue(rowHandle, "PackingVolume");
                wRDataGeneral.QuantityByPack = (Decimal?)gridViewWRDataDetail.GetRowCellValue(rowHandle, "QuantityByPack");
                wRDataGeneral.QuantityByItem = (Decimal?)gridViewWRDataDetail.GetRowCellValue(rowHandle, "QuantityByItem");
                wRDataGeneral.Note = "";
                wRDataGeneral.ScanOption = (Int16?)gridViewWRDataDetail.GetRowCellValue(rowHandle, "ScanOption");
                wRDataGeneral.PackingQuantity = (Decimal?)gridViewWRDataDetail.GetRowCellValue(rowHandle, "PackingQuantity");
                wRDataGeneral.ReceiptStatus = (string?)gridViewWRDataDetail.GetRowCellValue(rowHandle, "ReceiptStatus");
                wRDataGeneral.SLPart = (string?)gridViewWRDataDetail.GetRowCellValue(rowHandle, "SLPart");
                if (await wRDataGeneralRepository.Update(wRDataGeneral, textEditWRDNumber.Text, wRDataGeneral.GoodsID, wRDataGeneral.Ordinal) > 0)
                {
                    i++;
                }
            }
        }

        public async Task DeleteWRDataHeaderAsync()
        {
            if ((await wRDataHeaderRepository.Delete(textEditWRDNumber.Text)) > 0)
            {
                DeleteWRDataGeneralAsync();
                await DeleteWRRHeaderAsync();
                //sbLoadDataForGridWRDataDeteilAsync(textEditWRDNumber.Text);
                sbLoadDataForGridWRDataGeneralAsync(textEditWRDNumber.Text);
                WMPublic.sbMessageDeleteSuccess(this);
            }
        }

        public async Task DeleteWRDataDetailAsync()
        {
            await wRDataDetailRepository.Delete(textEditWRDNumber.Text);
            gridControlWRDataDetail.DataSource = null;
        }

        public async Task DeleteWRDataGeneralAsync()
        {
            await wRDataGeneralRepository.Delete(textEditWRDNumber.Text);
            clearAsync();
            gridControlWRDataDetail.DataSource = null;
        }

        public async Task DeleteWRRHeaderAsync()
        {
            if ((await wRRHeaderRepository.Delete(textEditWRDNumber.Text)) > 0)
            {
                await DeleteWRRDetail();
            }
        }

        public async Task DeleteWRRDetail()
        {
            await wRRDetailRepository.Delete(textEditWRDNumber.Text);
        }


        private void clearAsync()
        {
            textEditWRDNumber.Text = "";
            dateEditWRDDate.Text = DateTime.Now.ToString("dd-MM-yyyy");
            comboBoxEditHandlingStatus.SelectedIndex = 1;
            textEditTotalQuantityOrg.Text = "0";
            textEditTotalQuantity.Text = "0";
            List<WRDataDetail> wRDataDetails = new List<WRDataDetail>();
            List<WRDataGeneral> wRDataGenerals = new List<WRDataGeneral>();


            for (int i = 0; i < 100; i++)
            {
                wRDataGenerals.Add(new WRDataGeneral());
            }
            gridControlWRDataDetail.DataSource = wRDataGenerals;

        }

        public async Task createWRHeaderAsync()
        {
            WRDataHeader wRDataHeader = await wRDataHeaderRepository.GetUnderID(textEditWRDNumber.Text);
            WRHeader wRHeader = new WRHeader();
            wRHeader.WRNumber = textEditWRDNumber.Text;
            wRHeader.WRDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));
            wRHeader.ReferenceNumber = textEditWRDNumber.Text;
            wRHeader.WRRNumber = textEditWRDNumber.Text;
            wRHeader.WRRReference = textEditWRDNumber.Text;
            wRHeader.BranchID = wRDataHeader.BranchID;
            wRHeader.BranchName = wRDataHeader.BranchName;
            wRHeader.TotalQuantity = Decimal.Parse(textEditTotalQuantity.Text);
            wRHeader.HandlingStatusID = "0";
            wRHeader.HandlingStatusName = "Chưa duyệt";
            wRHeader.ModalityID = "09";
            wRHeader.ModalityName = "Khác";
            wRHeader.CreatedUserID = WMMessage.User.UserID;
            wRHeader.CreatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));
            wRHeader.WarehouseID = "<>";

            if (await wRHeaderRepository.Create(wRHeader) > 0)
            {
                createWRDetailAsync();
                MessageBox.Show("Đã tạo phiếu nhập");
            }
        }

        public async Task createWRDetailAsync()
        {
            int num = gridViewWRDataDetail.RowCount - 1;
            int i = 0;
            while (true)
            {
                int num2 = i;
                int num3 = num;
                if (num2 > num3 || gridViewWRDataDetail.GetRowCellValue(i, gridViewWRDataDetail.Columns["colGoodsID"]) == "")
                {
                    break;
                }
                int rowHandle = gridViewWRDataDetail.GetRowHandle(i);
                WRDetail wRDetail = new WRDetail();
                wRDetail.WRNumber = textEditWRDNumber.Text;
                wRDetail.Ordinal = i + 1;
                wRDetail.GoodsID = (string?)gridViewWRDataDetail.GetRowCellValue(rowHandle, "GoodsID");
                wRDetail.GoodsName = (string?)gridViewWRDataDetail.GetRowCellValue(i, "GoodsName");
                wRDetail.Quantity = (Decimal?)gridViewWRDataDetail.GetRowCellValue(i, "Quantity");
                wRDetail.ReceiptQuantity = (Decimal?)gridViewWRDataDetail.GetRowCellValue(i, "Quantity");
                wRDetail.ReceiptAmount = 0m;
                wRDetail.QuantityOrdered = (Decimal?)gridViewWRDataDetail.GetRowCellValue(i, "QuantityOrg");
                wRDetail.QuantityReceived = (Decimal?)gridViewWRDataDetail.GetRowCellValue(i, "Quantity");
                wRDetail.Note = "";
                wRDetail.Status = "-1";
                if (await wRDetailRepository.Create(wRDetail) > 0)
                {
                    i++;
                }
            }
        }


        private async void WRDNumber_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {

                WRDataHeader wRDataHeader = new WRDataHeader();
                wRDataHeader = await wRDataHeaderRepository.GetUnderID(textEditWRDNumber.Text);
                if (wRDataHeader != null)
                {
                    dateEditWRDDate.Text = wRDataHeader.WRDDate.ToString();
                    comboBoxEditHandlingStatus.SelectedIndex = int.Parse(wRDataHeader.HandlingStatusID) + 1;
                    await sbLoadDataForGridWRDataGeneralAsync(textEditWRDNumber.Text);
                }
                else
                {
                    MessageBox.Show("Số phiếu yêu cầu không tồn tại");
                    return;
                }

            }
        }

        private async void textEditWDRNumer_CellDoubleClick(object sender, EventArgs e)
        {
            frmSearchWRD frmSearchWRD = new frmSearchWRD();
            frmSearchWRD.ShowDialog(this);
            frmSearchWRD.Dispose();
            if (selectedWRDNumber != null)
            {
                textEditWRDNumber.Text = selectedWRDNumber;
                WRDataHeader wRDataHeader = await wRDataHeaderRepository.GetUnderID(textEditWRDNumber.Text);
                if (wRDataHeader != null)
                {
                    dateEditWRDDate.Text = wRDataHeader.WRDDate.ToString();
                    comboBoxEditHandlingStatus.SelectedIndex = int.Parse(wRDataHeader.HandlingStatusID) + 1;
                    await sbLoadDataForGridWRDataGeneralAsync(textEditWRDNumber.Text);
                }
                QRCodeGeneral(selectedWRDNumber);
            }

        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            if (textEditWRDNumber.Text != "")
            {

                sbLoadDataForGridWRDataGeneralAsync(textEditWRDNumber.Text);
            }
        }

        private void grdData_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            int i = 0;
            i = e.RowHandle;
            if ((i >= 0) && (String.Compare(e.Column.FieldName, "Quantity", false) == 0))
            {
                double mQuantity = ((gridViewWRDataDetail.GetRowCellValue(i, gridViewWRDataDetail.Columns["Quantity"]) == "") ? 0.0 : Convert.ToDouble(gridViewWRDataDetail.GetRowCellValue(i, gridViewWRDataDetail.Columns["Quantity"])));
                double mQuantityOrg = ((gridViewWRDataDetail.GetRowCellValue(i, gridViewWRDataDetail.Columns["QuantityOrg"]) == "") ? 0.0 : Convert.ToDouble(gridViewWRDataDetail.GetRowCellValue(i, gridViewWRDataDetail.Columns["QuantityOrg"])));
                double mPackingQuantity = ((gridViewWRDataDetail.GetRowCellValue(i, gridViewWRDataDetail.Columns["PackingQuantity"]) == "") ? 0.0 : Convert.ToDouble(gridViewWRDataDetail.GetRowCellValue(i, gridViewWRDataDetail.Columns["PackingQuantity"])));
                e.Appearance.Font = new Font("Tahoma", 8f, FontStyle.Bold);
                if (mQuantity == mQuantityOrg && mQuantity != 0.0)
                {
                    e.Appearance.BackColor = System.Drawing.Color.LightGreen;

                }
                if (mQuantity > mQuantityOrg && mQuantity != 0.0)
                {
                    e.Appearance.BackColor = System.Drawing.Color.Yellow;


                }
                if (mQuantity < mQuantityOrg && mQuantity != 0.0)
                {
                    e.Appearance.BackColor = System.Drawing.Color.Red;


                }
            }
            if ((i >= 0) && (String.Compare(e.Column.FieldName, "PackingQuantity", false) == 0))
            {
                double mQuantity = ((gridViewWRDataDetail.GetRowCellValue(i, gridViewWRDataDetail.Columns["Quantity"]) == "") ? 0.0 : Convert.ToDouble(gridViewWRDataDetail.GetRowCellValue(i, gridViewWRDataDetail.Columns["Quantity"])));
                double mQuantityOrg = ((gridViewWRDataDetail.GetRowCellValue(i, gridViewWRDataDetail.Columns["QuantityOrg"]) == "") ? 0.0 : Convert.ToDouble(gridViewWRDataDetail.GetRowCellValue(i, gridViewWRDataDetail.Columns["QuantityOrg"])));
                e.Appearance.Font = new Font("Tahoma", 8f, FontStyle.Bold);
                if (mQuantity == mQuantityOrg && mQuantity != 0.0)
                {
                    e.Appearance.BackColor = System.Drawing.Color.LightGreen;
                }
                if (mQuantity > mQuantityOrg && mQuantity != 0.0)
                {
                    e.Appearance.BackColor = System.Drawing.Color.Yellow;
                }
                if (mQuantity < mQuantityOrg && mQuantity != 0.0)
                {
                    e.Appearance.BackColor = System.Drawing.Color.Red;
                }
            }
            if ((i >= 0) && (String.Compare(e.Column.FieldName, "No", false) == 0))
            {
                string receiptStatus = ((gridViewWRDataDetail.GetRowCellValue(i, gridViewWRDataDetail.Columns["ReceiptStatus"]) == "") ? "" : Convert.ToString(gridViewWRDataDetail.GetRowCellValue(i, gridViewWRDataDetail.Columns["ReceiptStatus"])));
                string slPart = ((gridViewWRDataDetail.GetRowCellValue(i, gridViewWRDataDetail.Columns["SLPart"]) == "") ? "" : Convert.ToString(gridViewWRDataDetail.GetRowCellValue(i, gridViewWRDataDetail.Columns["SLPart"])));
                e.Appearance.Font = new Font("Tahoma", 8f, FontStyle.Bold);
                if (receiptStatus.Equals("NEW") || receiptStatus.Equals("QC") || receiptStatus.Equals("QDC") || slPart.Equals("Y"))
                {
                    e.Appearance.BackColor = Helpers.ColorHelper.GetSystemDrawingColorFromHexString("#FF99FF");
                }
            }
        }



        private void textTotalQuantity_CustomDraw()
        {
            //if (String.Compare(textEditTotalQuantityOrg.Text, "", false) == 0)
            //{
            double mTotalQuantity = ((textEditTotalQuantity.Text == "") ? 0.0 : Convert.ToDouble(textEditTotalQuantity.Text));
            double mTotalQuantityOrg = ((textEditTotalQuantityOrg.Text == "") ? 0.0 : Convert.ToDouble(textEditTotalQuantityOrg.Text));
            textEditTotalQuantity.Font = new Font("Tahoma", 8f, FontStyle.Bold);
            textEditTotalQuantityOrg.Font = new Font("Tahoma", 8f, FontStyle.Bold);
            if (mTotalQuantity == mTotalQuantityOrg && mTotalQuantity != 0.0)
            {
                textEditTotalQuantity.BackColor = System.Drawing.Color.LightGreen;
                textEditTotalQuantityOrg.BackColor = System.Drawing.Color.LightGreen;
            }
            if (mTotalQuantity > mTotalQuantityOrg && mTotalQuantity != 0.0)
            {
                textEditTotalQuantity.BackColor = System.Drawing.Color.Yellow;
                textEditTotalQuantityOrg.BackColor = System.Drawing.Color.LightGreen;
            }
            if (mTotalQuantity < mTotalQuantityOrg && mTotalQuantity != 0.0)
            {
                textEditTotalQuantity.BackColor = System.Drawing.Color.Red;
                textEditTotalQuantityOrg.BackColor = System.Drawing.Color.LightGreen;
            }
            //}
        }

        private void butOn_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Bắt đầu quét nhập kho");
            t.Enabled = true;

        }

        private void butOff_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Kết thúc quét nhập kho");
            t.Enabled = false;
        }

        //private void butDetail_Click(object sender, EventArgs e)
        //{
        //    sbLoadDataForGridWRDataDeteilAsync(textEditWRDNumber.Text);
        //    isGeneralClick = false;

        //}

        //private void butGeneral_Click(object sender, EventArgs e)
        //{
        //    sbLoadDataForGridWRDataGeneralAsync(textEditWRDNumber.Text);
        //    isGeneralClick = true;
        //}

        //barbutton click
        public async void button_Click(object sender, EventArgs e)
        {
            ButtonEventArgs eArg = (DevExpress.XtraBars.Docking2010.ButtonEventArgs)e;
            String tag = eArg.Button.Properties.Tag.ToString();
            switch (tag)
            {
                case "new":
                    if (WMPublic.sbMessageAddNewRequest(this) == true)
                    {
                        clearAsync();
                    }
                    break;
                case "save":
                    if (WMPublic.sbMessageSaveChangeRequest(this) == true)
                    {
                        updateWRDataHeaderAsync();
                    }
                    break;
                case "delete":
                    if (WMPublic.sbMessageDeleteRequest(this) == true)
                    {
                        DeleteWRDataHeaderAsync();
                    }
                    break;
                case "search":

                    break;
                case "refesh":
                    sbLoadDataForGridWRDataGeneralAsync(textEditWRDNumber.Text);
                    break;
                case "import":
                    break;
                case "export":
                    export(textEditWRDNumber.Text);
                    break;
                case "print":
                    break;
                case "close":
                    Dispose();
                    break;
            }

        }

        private async void export(string wrdNumber)
        {
            List<WRDataGeneral> wRDataGenerals = await wRDataGeneralRepository.GetUnderID(wrdNumber);
            if (wRDataGenerals.Count == 0)
            {
                return;
            }
            DialogResult dialogResult = MessageBox.Show("Xuất báo cáo dữ liệu nhập", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dialogResult == DialogResult.Yes)
            {
                using (SaveFileDialog sfd = new SaveFileDialog() { Filter = "Excel Workbook|*.xlsx", FileName = "Receipt Data Report " + wrdNumber + " " + DateTime.Now.ToString("ddMMyyyy") })
                {
                    if (sfd.ShowDialog() == DialogResult.OK)
                    {
                        var fileInfo = new FileInfo(sfd.FileName);
                        if (fileInfo.Exists)
                        {
                            fileInfo.Delete();
                        }


                        using (ExcelPackage excelPackage = new ExcelPackage(fileInfo))
                        {
                            excelPackage.Workbook.Protection.LockRevision = false;
                            excelPackage.Workbook.Protection.LockStructure = false;
                            excelPackage.Workbook.Protection.LockWindows = false;

                            ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets.Add(DateTime.Now.ToString("ddMMyyyy"));

                            worksheet.Protection.IsProtected = false;
                            worksheet.Protection.AllowFormatCells = true;
                            worksheet.Protection.AllowSelectLockedCells = true;
                            worksheet.Protection.AllowSelectUnlockedCells = true;
                            worksheet.Protection.AllowEditObject = true;
                            worksheet.Protection.AllowEditScenarios = true;
                            worksheet.Protection.AllowFormatColumns = true;
                            worksheet.Protection.AllowFormatRows = true;
                            worksheet.Column(1).Style.Locked = false;
                            worksheet.Column(2).Style.Locked = false;
                            worksheet.Column(3).Style.Locked = false;
                            worksheet.Column(4).Style.Locked = false;
                            worksheet.Column(5).Style.Locked = false;
                            worksheet.Column(6).Style.Locked = false;

                            worksheet.Cells[1, 1].Value = "BÁO CÁO DỮ LIỆU NHẬP";
                            worksheet.Row(1).Style.Font.Bold = true;
                            worksheet.Cells[1, 1, 1, 6].Merge = true;
                            worksheet.Row(1).Style.Font.Size = 26;
                            worksheet.Row(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                            worksheet.TabColor = System.Drawing.Color.Black;
                            worksheet.DefaultRowHeight = 12;
                            //Header of table  
                            //  

                            worksheet.Row(3).Height = 50;
                            worksheet.Row(3).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            worksheet.Row(3).Style.Font.Bold = true;

                            worksheet.Cells[3, 1].Value = "Conveyance_number";
                            worksheet.Cells[3, 2].Value = "Part_No";
                            worksheet.Cells[3, 3].Value = "ASN_Qty";
                            worksheet.Cells[3, 4].Value = "SL_Thực_nhận";
                            worksheet.Cells[3, 5].Value = "Ngày_nhận_hàng";
                            worksheet.Cells[3, 6].Value = "Ghi_chú_khác";


                            //Body of table  
                            //  

                            SplashScreenManager.ShowForm(this, typeof(WaitForm), true, true);
                            int recordIndex = 4;
                            for (int i = 0; i < wRDataGenerals.Count; i++)
                            {
                                string goodsID = wRDataGenerals[i].GoodsID;
                                string note = string.IsNullOrEmpty(wRDataGenerals[i].Note) ? "" : wRDataGenerals[i].Note;
                                decimal quantityOrg = (decimal)wRDataGenerals[i].QuantityOrg;
                                decimal quantity = (decimal)wRDataGenerals[i].Quantity;
                                if (!string.IsNullOrEmpty(wRDataGenerals[i].EditedDateTime + ""))
                                {
                                    worksheet.Cells[recordIndex, 5].Value = wRDataGenerals[i].EditedDateTime.Value.Date.ToString("dd/MM/yyyy");
                                }

                                worksheet.Cells[recordIndex, 1].Value = wrdNumber;
                                worksheet.Cells[recordIndex, 2].Value = goodsID;
                                worksheet.Cells[recordIndex, 3].Value = quantityOrg;
                                worksheet.Cells[recordIndex, 4].Value = quantity;
                                worksheet.Cells[recordIndex, 6].Value = note;

                                recordIndex++;
                            }

                            worksheet.Column(1).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            worksheet.Column(2).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            worksheet.Column(3).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            worksheet.Column(4).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            worksheet.Column(5).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            worksheet.Column(6).Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            worksheet.Column(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            worksheet.Column(2).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            worksheet.Column(3).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            worksheet.Column(4).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            worksheet.Column(5).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            worksheet.Column(6).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                            worksheet.Column(1).Width = 20;
                            worksheet.Column(2).Width = 20;
                            worksheet.Column(3).Width = 20;
                            worksheet.Column(4).Width = 20;
                            worksheet.Column(5).Width = 20;
                            worksheet.Column(6).Width = 30;

                            var modelTable = worksheet.Cells[3, 1, recordIndex - 1, 6];
                            modelTable.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            modelTable.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            modelTable.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            modelTable.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            modelTable.AutoFitColumns();

                            excelPackage.Save();
                        }
                        SplashScreenManager.CloseForm(false);
                        MessageBox.Show("Xuất báo cáo thành công");
                        Process.Start(sfd.InitialDirectory + sfd.FileName);
                    }
                }

            }
        }

    }
}
