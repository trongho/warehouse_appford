﻿
namespace WarehouseManager
{
    partial class TestRGBLight
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonOnYellow = new System.Windows.Forms.Button();
            this.buttonOff = new System.Windows.Forms.Button();
            this.textBoxStatus = new System.Windows.Forms.TextBox();
            this.buttonOnGreen = new System.Windows.Forms.Button();
            this.buttonOnRed = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonOnYellow
            // 
            this.buttonOnYellow.Location = new System.Drawing.Point(84, 78);
            this.buttonOnYellow.Name = "buttonOnYellow";
            this.buttonOnYellow.Size = new System.Drawing.Size(151, 41);
            this.buttonOnYellow.TabIndex = 0;
            this.buttonOnYellow.Text = "Bật đèn vàng";
            this.buttonOnYellow.UseVisualStyleBackColor = true;
            this.buttonOnYellow.Click += new System.EventHandler(this.buttonOn_Click);
            // 
            // buttonOff
            // 
            this.buttonOff.Location = new System.Drawing.Point(597, 78);
            this.buttonOff.Name = "buttonOff";
            this.buttonOff.Size = new System.Drawing.Size(132, 41);
            this.buttonOff.TabIndex = 1;
            this.buttonOff.Text = "Tắt đèn";
            this.buttonOff.UseVisualStyleBackColor = true;
            this.buttonOff.Click += new System.EventHandler(this.buttonOff_Click);
            // 
            // textBoxStatus
            // 
            this.textBoxStatus.Location = new System.Drawing.Point(202, 160);
            this.textBoxStatus.Name = "textBoxStatus";
            this.textBoxStatus.Size = new System.Drawing.Size(381, 20);
            this.textBoxStatus.TabIndex = 2;
            // 
            // buttonOnGreen
            // 
            this.buttonOnGreen.Location = new System.Drawing.Point(260, 78);
            this.buttonOnGreen.Name = "buttonOnGreen";
            this.buttonOnGreen.Size = new System.Drawing.Size(129, 41);
            this.buttonOnGreen.TabIndex = 3;
            this.buttonOnGreen.Text = "Bật đèn xanh";
            this.buttonOnGreen.UseVisualStyleBackColor = true;
            this.buttonOnGreen.Click += new System.EventHandler(this.buttonOnGreen_Click);
            // 
            // buttonOnRed
            // 
            this.buttonOnRed.Location = new System.Drawing.Point(423, 78);
            this.buttonOnRed.Name = "buttonOnRed";
            this.buttonOnRed.Size = new System.Drawing.Size(136, 41);
            this.buttonOnRed.TabIndex = 4;
            this.buttonOnRed.Text = "Bật đèn đỏ";
            this.buttonOnRed.UseVisualStyleBackColor = true;
            this.buttonOnRed.Click += new System.EventHandler(this.buttonOnRed_Click);
            // 
            // TestRGBLight
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.buttonOnRed);
            this.Controls.Add(this.buttonOnGreen);
            this.Controls.Add(this.textBoxStatus);
            this.Controls.Add(this.buttonOff);
            this.Controls.Add(this.buttonOnYellow);
            this.Name = "TestRGBLight";
            this.Text = "TestRGBLight";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonOnYellow;
        private System.Windows.Forms.Button buttonOff;
        private System.Windows.Forms.TextBox textBoxStatus;
        private System.Windows.Forms.Button buttonOnGreen;
        private System.Windows.Forms.Button buttonOnRed;
    }
}