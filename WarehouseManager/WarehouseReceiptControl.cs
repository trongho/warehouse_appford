﻿ using DevExpress.Utils;
using DevExpress.XtraBars.Docking2010;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using OfficeOpenXml.FormulaParsing.Excel.Operators;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Media;
using WarehouseManager.Models;
using WarehouseManager.Repository;
using webservice.Entities;

namespace WarehouseManager
{
    public partial class WarehouseReceiptControl : DevExpress.XtraEditors.XtraUserControl
    {
        HandlingStatusRepository handlingStatusRepository;
        WRHeaderRepository wRHeaderRepository;
        WRDetailRepository wRDetailRepository;
        ModalityRepository modalityRepository;
        WarehouseRepository warehouseRepository;
        public static String selectedWRNumber;

        public WarehouseReceiptControl()
        {
            InitializeComponent();
            
            handlingStatusRepository = new HandlingStatusRepository();
            wRDetailRepository = new WRDetailRepository();
            wRHeaderRepository = new WRHeaderRepository();
            modalityRepository = new ModalityRepository();
            warehouseRepository = new WarehouseRepository();
            popupMenuSelectHandlingStatus();
            popupMenuSelectModality();
            popupMenuSelectWarehouse();
            sbLoadDataForGridWRDetailAsync(textEditWRNumber.Text);
            this.Load +=  WarehouseReceiptControl_Load;
            gridViewWRDetail.ClearSorting();
        }

        private void WarehouseReceiptControl_Load(Object sender, EventArgs e)
        {
            windowsUIButtonPanel1.ButtonClick += button_Click;
            textEditWRNumber.DoubleClick += textEditWRNumer_CellDoubleClick;
            gridViewWRDetail.CustomColumnDisplayText += GridView_CustomColumnDisplayText;

        }

        private void GridView_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {

            if (e.Column == colNo)
            {
                int rowHandle = gridViewWRDetail.GetRowHandle(e.ListSourceRowIndex);
                if (rowHandle >= 0)
                {
                    e.DisplayText = (rowHandle + 1).ToString();
                }
            }
        }

        private async Task popupMenuSelectModality()
        {
            List<Modality> modalities = await modalityRepository.GetAll();
            foreach (Modality modality in modalities)
            {
                comboBoxEditModality.Properties.Items.Add(modality.ModalityName);
            }
            comboBoxEditModality.SelectedIndex = 0;
        }

        private async Task popupMenuSelectHandlingStatus()
        {
            List<HandlingStatus> handlingStatuses = await handlingStatusRepository.GetAll();
            foreach (HandlingStatus handlingStatus in handlingStatuses)
            {
                comboBoxEditHandlingStatus.Properties.Items.Add(handlingStatus.HandlingStatusName);
            }
            comboBoxEditHandlingStatus.SelectedIndex = 1;
        }

        private async Task popupMenuSelectWarehouse()
        {
            List<Warehouse> warehouses = await warehouseRepository.getAll();
            warehouses.Remove(warehouses[0]);
            foreach (Warehouse warehouse in warehouses)
            {
                comboBoxEditWarehouse.Properties.Items.Add(warehouse.WarehouseName);
            }
            comboBoxEditWarehouse.SelectedIndex = 0;
        }

        private async Task sbLoadDataForGridWRDetailAsync(String wRNumber)
        {
            List<WRDetail> wRDetails = await wRDetailRepository.GetUnderID(wRNumber);
            int mMaxRow = 100;
            if (wRDetails.Count < mMaxRow)
            {
                int num = mMaxRow - wRDetails.Count;
                int i = 1;
                while (true)
                {
                    int num2 = i;
                    int num3 = num;
                    if (num2 > num3)
                    {
                        break;
                    }
                    wRDetails.Add(new WRDetail());
                    i++;
                }
            }
            gridControlWRDetail.DataSource = wRDetails;
           
        }


      
        public async Task updateWRHeaderAsync()
        {
            WRHeader wRHeader = await wRHeaderRepository.GetUnderID(textEditWRNumber.Text);
            wRHeader.WRNumber = textEditWRNumber.Text;
            wRHeader.WRDate = DateTime.Parse(dateEditWRDate.DateTime.ToString("yyyy-MM-dd"));
            wRHeader.ReferenceNumber = textEditWRNumber.Text;
            wRHeader.HandlingStatusID = (comboBoxEditHandlingStatus.SelectedIndex - 1).ToString();
            wRHeader.HandlingStatusName = comboBoxEditHandlingStatus.SelectedItem.ToString();
            wRHeader.ModalityID = $"{int.Parse(comboBoxEditModality.SelectedIndex.ToString())+1:D2}";
            wRHeader.ModalityName = comboBoxEditModality.SelectedItem.ToString();
            wRHeader.Note = textEditNote.Text;
            wRHeader.UpdatedUserID = WMMessage.User.UserID;
            wRHeader.UpdatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));

            if (await wRHeaderRepository.Update(wRHeader, textEditWRNumber.Text) > 0)
            {
                updateWRDetailAsync();
                WMPublic.sbMessageSaveChangeSuccess(this);
            }
        }

        public async Task updateWRDetailAsync()
        {
            int num = gridViewWRDetail.RowCount - 1;
            int i = 0;
            while (true)
            {
                int num2 = i;
                int num3 = num;
                if (num2 > num3 || gridViewWRDetail.GetRowCellValue(i, gridViewWRDetail.Columns["colGoodsID"]) == "")
                {
                    break;
                }
                int rowHandle = gridViewWRDetail.GetRowHandle(i);
                String wrNumber = textEditWRNumber.Text;
                String goodsID= (string)gridViewWRDetail.GetRowCellValue(rowHandle, "GoodsID");
                int Ordinal= (int)gridViewWRDetail.GetRowCellValue(rowHandle, "Ordinal");
                List<WRDetail> wRDetails = await wRDetailRepository.GetUnderMultiID(wrNumber,goodsID,Ordinal);
                WRDetail wRDetail = wRDetails[0];
                wRDetail.WRNumber = textEditWRNumber.Text;
                wRDetail.GoodsID = (string?)gridViewWRDetail.GetRowCellValue(rowHandle, "GoodsID");
                wRDetail.GoodsName = (string?)gridViewWRDetail.GetRowCellValue(rowHandle, "GoodsName");
                wRDetail.Ordinal = i + 1;
                wRDetail.ReceiptQuantity = (Decimal?)gridViewWRDetail.GetRowCellValue(rowHandle, "ReceiptQuantity");
                wRDetail.Note = textEditNote.Text;
                if((comboBoxEditHandlingStatus.SelectedIndex - 1).ToString().Equals("2"))
                {
                    wRDetail.Status = "0";
                }

                if (await wRDetailRepository.Update(wRDetail, textEditWRNumber.Text, wRDetail.GoodsID, wRDetail.Ordinal) > 0)
                {
                    i++;
                }
            }
        }

        private async void WRNumber_KeyDown(object sender, KeyEventArgs e)
        {
            
        }

        private async void textEditWRNumer_CellDoubleClick(object sender, EventArgs e)
        {
            frmSearchWR frmSearchWR = new frmSearchWR();
            frmSearchWR.ShowDialog(this);
            frmSearchWR.Dispose();
            if (selectedWRNumber != null)
            {
                textEditWRNumber.Text = selectedWRNumber;
                WRHeader wRHeader = await wRHeaderRepository.GetUnderID(textEditWRNumber.Text);
                dateEditWRDate.Text = wRHeader.WRDate.ToString();
                textEditNote.Text = wRHeader.Note;
                comboBoxEditHandlingStatus.SelectedIndex = int.Parse(wRHeader.HandlingStatusID) + 1;
                comboBoxEditModality.SelectedIndex = int.Parse(wRHeader.ModalityID)-1;
                comboBoxEditWarehouse.SelectedIndex = int.Parse("000");
                await sbLoadDataForGridWRDetailAsync(textEditWRNumber.Text);
            }

        }

        //barbutton click
        public async void button_Click(object sender, EventArgs e)
        {
            ButtonEventArgs eArg = (DevExpress.XtraBars.Docking2010.ButtonEventArgs)e;
            String tag = eArg.Button.Properties.Tag.ToString();
            switch (tag)
            {
                case "new":
                    break;
                case "save":
                    if (WMPublic.sbMessageSaveChangeRequest(this) == true)
                    {
                        updateWRHeaderAsync();
                    }             
                    break;
                case "delete":           
                    break;
                case "search":

                    break;
                case "refesh":
                    sbLoadDataForGridWRDetailAsync(textEditWRNumber.Text);
                    break;
                case "import":
                    break;
                case "export":
     
                    break;
                case "print":
                    break;
                case "close":
                    break;
            }

        }
    }
}
