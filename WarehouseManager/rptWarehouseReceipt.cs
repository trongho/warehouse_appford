﻿using DevExpress.XtraBars.Docking2010;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WarehouseManager.Models;
using WarehouseManager.Repository;
using webservice.Entities;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.TreeView;
using Branch = WarehouseManager.Models.Branch;
using LicenseContext = OfficeOpenXml.LicenseContext;

namespace WarehouseManager
{
    public partial class rptWarehouseReceipt : DevExpress.XtraEditors.XtraUserControl
    {
        HandlingStatusRepository handlingStatusRepository;
        OrderStatusRepository orderStatusRepository;
        BranchRepository branchRepository;
        ModalityRepository modalityRepository;
        WRHeaderRepository wRHeaderRepository;
        public rptWarehouseReceipt()
        {
            InitializeComponent();
            this.Load += rptWarehouseReceipt_Load;
            handlingStatusRepository = new HandlingStatusRepository();
            branchRepository = new BranchRepository();
            wRHeaderRepository = new WRHeaderRepository();
            orderStatusRepository = new OrderStatusRepository();
            modalityRepository = new ModalityRepository();
        }

        private void rptWarehouseReceipt_Load(Object sender,EventArgs e)
        {
            popupMenuSelectBranch();
            popupMenuSelectHandlingStatus();
            popupMenuSelectModality();
            sbLoadDataForGridDataAsync();
            windowsUIButtonPanel1.ButtonClick += button_Click;
            simpleButtonFilter.Click += filterButton_Click;
            dateEditFromDate.Text = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day - 1).ToString("yyyy-MM-dd");
            dateEditToDate.Text = DateTime.Now.ToString("yyyy-MM-dd");
        }

        private async Task popupMenuSelectHandlingStatus()
        {
            List<HandlingStatus> handlingStatuses = await handlingStatusRepository.GetAll();
            foreach (HandlingStatus handlingStatus in handlingStatuses)
            {
                comboBoxEditHandlingStatus.Properties.Items.Add(handlingStatus.HandlingStatusName);
            }
            comboBoxEditHandlingStatus.SelectedIndex = 0;
        }



        private async Task popupMenuSelectBranch()
        {
            List<Branch> branches = await branchRepository.getAllBranchAsync();
            foreach (Branch branch in branches)
            {
                comboBoxEditBranch.Properties.Items.Add(branch.BranchName);
            }
            comboBoxEditBranch.SelectedIndex = 0;
        }

        private async Task popupMenuSelectModality()
        {
            List<Modality> modalities = await modalityRepository.GetAll();
            foreach (Modality modality in modalities)
            {
                comboBoxEditmodality.Properties.Items.Add(modality.ModalityName);
            }
            comboBoxEditmodality.SelectedIndex = 0;
        }

        private async Task sbLoadDataForGridDataAsync()
        {
            List<WRHeader> wRHeaders = await wRHeaderRepository.GetAll();
           gridControlData.DataSource = wRHeaders;
            getTotalQuantity();
            //getTotalAmount();
        }

        private void getTotalQuantity()
        {
            Decimal totalQuantity = 0;
            int i = 0;
            while (true)
            {
                int rowHandle = gridViewData.GetRowHandle(i);
                if (gridViewData.GetRowCellValue(rowHandle, "WRNumber") == null)
                {
                    break;
                }
                Decimal quantity = (Decimal)gridViewData.GetRowCellValue(rowHandle, "TotalQuantity");
                totalQuantity += quantity;
                i++;
            }
            textEditTotalQuantity.Text = totalQuantity.ToString("0.#####");
        }

        private void getTotalAmount()
        {
            Decimal totalAmount = 0;
            int i = 0;
            while (true)
            {
                int rowHandle = gridViewData.GetRowHandle(i);
                if (gridViewData.GetRowCellValue(rowHandle, "WRNumber") == null)
                {
                    break;
                }
                Decimal amount = (Decimal)gridViewData.GetRowCellValue(rowHandle, "TotalAmount");
                totalAmount += amount;
                i++;
            }
            textEditTotalAmount.Text = totalAmount.ToString("0.#####");
        }


        public async Task ExportExcelAsync()
        {
            FolderBrowserDialog folder = new FolderBrowserDialog();
            DialogResult result = folder.ShowDialog();
            string path = "";
            String fromDateS = dateEditFromDate.Text.ToString();
            string fileName = @"RPTWarehouseReceipt"+fromDateS+".xlsx";

            path = folder.SelectedPath;
            FileInfo file = new FileInfo(Path.Combine(path, fileName));
            ExcelPackage.LicenseContext = LicenseContext.Commercial;
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            if (result == DialogResult.OK)
            {
                using (ExcelPackage package = new ExcelPackage(file))
                {

                    List<WRHeader> wRHeaders = await getData();

                    if (package.Workbook.Worksheets.Count > 1)
                    {
                        //result = "Non empty worksheet";
                    }
                    else
                    {
                        ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("WarehouseReceipt");
                        int totalRows = wRHeaders.Count();
                       

                        worksheet.Cells[1, 1].Value = "WRNumber";
                        worksheet.Cells[1, 2].Value = "WRDate";
                        worksheet.Cells[1, 3].Value = "Description";
                        worksheet.Cells[1, 4].Value = "Status";
                        int i = 0;
                        for (int row = 2; row <= totalRows + 1; row++)
                        {
                            worksheet.Cells[row, 1].Value = wRHeaders[i].WRNumber;
                            worksheet.Cells[row, 2].Value = wRHeaders[i].WRDate;
                            i++;
                        }

                        //result = "GoodsLine list has been exported successfully";
                    }
                    package.Save();
                }
                MessageBox.Show("Xuất thành công file excel");
            }

            //return result;
        }

        private async Task<List<WRHeader>> getData()
        {
            String branchID = $"{int.Parse(comboBoxEditBranch.SelectedIndex.ToString()) - 1:D3}";
            String handlingStatusID = (comboBoxEditHandlingStatus.SelectedIndex - 1).ToString();
            String modalityID = $"{int.Parse(comboBoxEditmodality.SelectedIndex.ToString()) + 1:D2}";
            DateTime fromDate = DateTime.Parse(dateEditFromDate.Text);
            DateTime toDate = DateTime.Parse(dateEditToDate.Text);

            List<WRHeader> wRHeadersUnderBranch = new List<WRHeader>();
            List<WRHeader> wRHeadersUnderDate = new List<WRHeader>();
            List<WRHeader> wRHeadersUnderHandlingStatus = new List<WRHeader>();
            List<WRHeader> wRHeadersUnderModality = new List<WRHeader>();
            List<WRHeader> wRHeaders = new List<WRHeader>();
            if (branchID.Equals("<>"))
            {
                wRHeadersUnderBranch = await wRHeaderRepository.GetAll();
            }
            else
            {
                wRHeadersUnderBranch = await wRHeaderRepository.GetUnderBranch(branchID);
            }
            if (handlingStatusID.Equals("<>"))
            {
                wRHeadersUnderHandlingStatus = await wRHeaderRepository.GetAll();
            }
            else
            {
                wRHeadersUnderHandlingStatus = await wRHeaderRepository.GetUnderHandlingStatus(handlingStatusID);
            }
            if (modalityID.Equals("<>"))
            {
                wRHeadersUnderModality = await wRHeaderRepository.GetAll();
            }
            else
            {
                wRHeadersUnderModality = await wRHeaderRepository.GetUnderModality(modalityID);
            }
            wRHeadersUnderDate = await wRHeaderRepository.GetUnderDate(fromDate, toDate);

            for (int i = 0; i < wRHeadersUnderBranch.Count; i++)
            {
                for (int j = 0; j < wRHeadersUnderDate.Count; j++)
                {
                    for (int k = 0; k < wRHeadersUnderHandlingStatus.Count; k++)
                    {
                        for (int h = 0; h < wRHeadersUnderModality.Count; h++)
                        {
                            if (wRHeadersUnderBranch[i].WRNumber.Equals(wRHeadersUnderDate[j].WRNumber)
                            && wRHeadersUnderDate[j].WRNumber.Equals(wRHeadersUnderHandlingStatus[k].WRNumber)
                            && wRHeadersUnderHandlingStatus[k].WRNumber.Equals(wRHeadersUnderModality[h].WRNumber))
                            {
                                wRHeaders.Add(wRHeadersUnderBranch[i]);
                            }
                        }
                    }
                }
            }
            return wRHeaders;
        }

        private async void filterButton_Click(Object sender, EventArgs e)
        {
            List<WRHeader> wRHeaders = await getData();
            gridControlData.DataSource = wRHeaders;
            getTotalQuantity();
        }



        //barbutton click
        public async void button_Click(object sender, EventArgs e)
        {

            ButtonEventArgs eArg = (DevExpress.XtraBars.Docking2010.ButtonEventArgs)e;
            String tag = eArg.Button.Properties.Tag.ToString();
            switch (tag)
            {
        
                case "refesh":
                    break;
                case "export":
                    ExportExcelAsync();
                    break;
                case "print":
                    break;
                case "close":
                    break;
            }

        }


    }
}
