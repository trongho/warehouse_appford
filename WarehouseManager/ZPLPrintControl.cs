﻿using BinaryKits.Zpl.Label;
using BinaryKits.Zpl.Label.Elements;
using BinaryKits.Zpl.Viewer;
using DevExpress.XtraBars.Docking2010;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WarehouseManager.Helpers;
using WarehouseManager.Models;
using WarehouseManager.Repository;
using LicenseContext = OfficeOpenXml.LicenseContext;

namespace WarehouseManager
{
    public partial class ZPLPrintControl : DevExpress.XtraEditors.XtraUserControl
    {
        private DataPrintHeaderRepository dataPrintHeaderRepository;
        private DataPrintGeneralRepository dataPrintGeneralRepository;
        private Point _Offset = Point.Empty;
        String output = null;
        List<String> ouputs = new List<string>();
        List<DataPrintDetail> dataPrintDetailsPublic = new List<DataPrintDetail>();
        List<DataPrintDetail> dataPrintDetailsModified = new List<DataPrintDetail>();
        List<DataPrintGeneral> dataPrintGeneralsPuclic = new List<DataPrintGeneral>();
        int labelWidth=600;
        int labelHeight=300;
        public ZPLPrintControl()
        {
            InitializeComponent();
            this.Load += ZPLPrintControl_Load;

        }

        private void ZPLPrintControl_Load(Object sender, EventArgs e)
        {
            pictureBox1.Size = new Size(600, 300);
            windowsUIButtonPanel1.ButtonClick += button_Click;
            windowsUIButtonPanel1.Cursor = Cursors.Hand;
            dataPrintHeaderRepository = new DataPrintHeaderRepository();
            dataPrintGeneralRepository = new DataPrintGeneralRepository();
            gridViewDataPrintGeneral.CustomColumnDisplayText += GridViewDataPrintGeneral_CustomColumnDisplayText;
            gridViewDataPrintDetail.CustomColumnDisplayText += GridViewDataPrintDetail_CustomColumnDisplayText;

            //label1.MouseDown += ctrlToMove_MouseDown;
            //label1.MouseUp += ctrlToMove_MouseUp;
            //label1.MouseMove += ctrlToMove_MouseMove;
            //barCodeControl1.MouseDown += ctrlToMove2_MouseDown;
            //barCodeControl1.MouseUp += ctrlToMove2_MouseUp;
            //barCodeControl1.MouseMove += ctrlToMove2_MouseMove;
            
        }
        private void GridViewDataPrintGeneral_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {

            if (e.Column == colLineNo)
            {
                int rowHandle = gridViewDataPrintGeneral.GetRowHandle(e.ListSourceRowIndex);
                if (rowHandle >= 0)
                {
                    e.DisplayText = (rowHandle + 1).ToString();
                }
            }
        }

        private void GridViewDataPrintDetail_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {

            if (e.Column == colLineNo2)
            {
                int rowHandle = gridViewDataPrintDetail.GetRowHandle(e.ListSourceRowIndex);
                if (rowHandle >= 0)
                {
                    e.DisplayText = (rowHandle + 1).ToString();
                }
            }
        }



        //private void ctrlToMove_MouseDown(object sender, MouseEventArgs e)
        //{
        //    if (e.Button == MouseButtons.Left)
        //    {
        //        _Offset = new Point(e.X, e.Y);
        //    }
        //}

        //private void ctrlToMove_MouseUp(object sender, MouseEventArgs e)
        //{
        //    _Offset = Point.Empty;
        //}

        //private void ctrlToMove_MouseMove(object sender, MouseEventArgs e)
        //{
        //    if (_Offset != Point.Empty)
        //    {
        //        Point newlocation = label1.Location;
        //        newlocation.X += e.X - _Offset.X;
        //        newlocation.Y += e.Y - _Offset.Y;
        //        label1.Location = newlocation;
        //    }
        //}

        //private void ctrlToMove2_MouseDown(object sender, MouseEventArgs e)
        //{
        //    if (e.Button == MouseButtons.Left)
        //    {
        //        _Offset = new Point(e.X, e.Y);
        //    }
        //}

        //private void ctrlToMove2_MouseUp(object sender, MouseEventArgs e)
        //{
        //    _Offset = Point.Empty;
        //}

        //private void ctrlToMove2_MouseMove(object sender, MouseEventArgs e)
        //{
        //    if (_Offset != Point.Empty)
        //    {
        //        Point newlocation = barCodeControl1.Location;
        //        newlocation.X += e.X - _Offset.X;
        //        newlocation.Y += e.Y - _Offset.Y;
        //        barCodeControl1.Location = newlocation;
        //    }
        //}

        private void buttonPrint_Click(object sender, EventArgs e)
        {
            //var barcodeString = "ABCCAEEE";
            //var qrcodeString = "EEDDDDD";
            //var font = new ZplFont(fontWidth: 20, fontHeight: 20);
            //var elements = new List<ZplElementBase>();
            //elements.Add(new ZplDownloadGraphics('R', "SAMPLE", System.IO.File.ReadAllBytes("sample2.bmp")));
            //elements.Add(new ZplRecallGraphic(0, 0, 'R', "SAMPLE", 1, 1));
            //elements.Add(new ZplBarcode128(barcodeString, pictureBox1.Width / 2, 10, 100, 10, printInterpretationLine: false));
            //elements.Add(new ZplTextField(barcodeString, pictureBox1.Width / 2, 40, font));
            //elements.Add(new ZplGraphicBox(20, 140, 2000, 1000, 1, LineColor.Black));
            //elements.Add(new ZplGraphicBox(20, 140, pictureBox1.Width / 2, 150, 1, LineColor.Black));

            //elements.Add(new ZplGraphicBox(20, 190, pictureBox1.Width / 2, 50, 1, LineColor.Black));
            //elements.Add(new ZplGraphicBox(20, 190, pictureBox1.Width / 2, 50, 1, LineColor.Black));

            //elements.Add(new ZplQrCode(qrcodeString, pictureBox1.Width / 2 + 50, 150, 2, 20));


            //var renderEngine = new ZplEngine(elements);
            //var output = renderEngine.ToZplString(new ZplRenderOptions { AddEmptyLineBeforeElementStart = true });

            //// Open connection
            //var tcpClient = new System.Net.Sockets.TcpClient();
            //tcpClient.Connect("127.0.0.1", 9100);

            //// Send Zpl data to printer
            //var writer = new System.IO.StreamWriter(tcpClient.GetStream());
            //writer.Write(output);
            //writer.Flush();

            //// Close Connection
            //writer.Close();
            //tcpClient.Close();
        }

        private void buttonShowImage_Click(object sender, EventArgs e)
        {
            //PictureBox pictureBox = new PictureBox();
            //pictureBox.Location = new Point(pictureEdit1.Location.X, pictureEdit1.Location.Y);
            //pictureBox.Size = new Size(500, 300);
            //pictureBox.BackColor = System.Drawing.Color.LightGreen;
            //var barcodeString = "ABCCAEEE";
            //var qrcodeString = "EEDDDDD";
            //var font = new ZplFont(fontWidth: 20, fontHeight: 20);
            //var elements = new List<ZplElementBase>();
            //elements.Add(new ZplDownloadGraphics('R', "SAMPLE", System.IO.File.ReadAllBytes("sample2.bmp")));
            //elements.Add(new ZplRecallGraphic(0, 0, 'R', "SAMPLE", 1, 1));
            //elements.Add(new ZplBarcode128(barcodeString, pictureBox1.Width / 2, 10, 30, 10));
            //elements.Add(new ZplTextField(barcodeString, pictureBox1.Width / 2, 40, font));
            //elements.Add(new ZplGraphicBox(20, 140, 560, 150, 1, LineColor.Black));
            //elements.Add(new ZplGraphicBox(20, 140, pictureBox1.Width / 2, 150, 1, LineColor.Black));

            //elements.Add(new ZplGraphicBox(20, 190, pictureBox1.Width / 2, 50, 1, LineColor.Black));
            //elements.Add(new ZplGraphicBox(20, 190, pictureBox1.Width / 2, 50, 1, LineColor.Black));

            //elements.Add(new ZplQrCode(qrcodeString, pictureBox1.Width / 2 + 50, 150, 5));


            //var renderEngine = new ZplEngine(elements);
            //var output = renderEngine.ToZplString(new ZplRenderOptions { AddEmptyLineBeforeElementStart = true });

            //IPrinterStorage printerStorage = new PrinterStorage();
            //var drawer = new ZplElementDrawer(printerStorage);

            //var analyzer = new ZplAnalyzer(printerStorage);
            //var analyzeInfo = analyzer.Analyze(output);

            //foreach (var labelInfo in analyzeInfo.LabelInfos)
            //{
            //    var imageData = drawer.Draw(labelInfo.ZplElements);
            //    pictureBox1.Image = ByteToImage(imageData);
            //}


        }

        protected override void OnMouseWheel(MouseEventArgs e)
        {
            if (e.Delta != 0)
            {
                if (e.Delta <= 0)
                {
                    //set minimum size to zoom
                    if (pictureBox1.Width < 100)
                        // lbl_Zoom.Text = pictureBox1.Image.Size; 
                        return;
                }
                else
                {
                    //set maximum size to zoom
                    if (pictureBox1.Width > 1000)
                        return;
                }
                pictureBox1.Width += Convert.ToInt32(pictureBox1.Width * e.Delta / 1000);
                pictureBox1.Height += Convert.ToInt32(pictureBox1.Height * e.Delta / 1000);
            }
        }

        public static Bitmap ByteToImage(byte[] blob)
        {
            MemoryStream mStream = new MemoryStream();
            byte[] pData = blob;
            mStream.Write(pData, 0, Convert.ToInt32(pData.Length));
            Bitmap bm = new Bitmap(mStream, false);
            mStream.Dispose();
            return bm;
        }

        private void buttonCustomWidthHeight_Click(object sender, EventArgs e)
        {
            //pictureBox1.Width = int.Parse(textBoxWidth.Text);
            //pictureBox1.Height = int.Parse(textBoxHeight.Text);
        }

        private async Task getTotalGoodsID()
        {
            Decimal totalGoodsID = 0;
            int i = 0;
            while (true)
            {
                int rowHandle = gridViewDataPrintGeneral.GetRowHandle(i);
                if (gridViewDataPrintGeneral.GetRowCellValue(rowHandle, "GoodsID") == null)
                {
                    break;
                }
                totalGoodsID++;
                i++;
            }
            textEditTotalGoodsID.Text = totalGoodsID.ToString("0.#####");
        }

        private void getTotalGoodsIDPrinted()
        {
            Decimal totalGoodsIDPrinted = 0;
            int i = 0;
            while (true)
            {
                int rowHandle = gridViewDataPrintGeneral.GetRowHandle(i);
                if (gridViewDataPrintGeneral.GetRowCellValue(rowHandle, "GoodsID") == null)
                {
                    break;
                }
                if (gridViewDataPrintGeneral.GetRowCellValue(rowHandle, "Status") == "PRINTED")
                {
                    totalGoodsIDPrinted++;
                }
                i++;
            }
            textEditTotalGoodsIDPrinted.Text = totalGoodsIDPrinted.ToString("0.#####");
        }

        private async Task getTotalLabel()
        {
            Decimal totalGoodsID = 0;
            int i = 0;
            while (true)
            {
                int rowHandle =gridViewDataPrintDetail.GetRowHandle(i);
                if (gridViewDataPrintDetail.GetRowCellValue(rowHandle, "GoodsID") == null)
                {
                    break;
                }
                totalGoodsID++;
                i++;
            }
            textEditTotalLabel.Text = totalGoodsID.ToString("0.#####");
        }

        private void getTotalLabelPrinted()
        {
            Decimal totalLabelPrinted = 0;
            int i = 0;
            while (true)
            {
                int rowHandle = gridViewDataPrintDetail.GetRowHandle(i);
                if (gridViewDataPrintDetail.GetRowCellValue(rowHandle, "GoodsID") == null)
                {
                    break;
                }
                if (gridViewDataPrintDetail.GetRowCellValue(rowHandle, "Status") == "PRINTED")
                {
                    totalLabelPrinted++;
                }
                i++;
            }
            textEditTotalLabelPrinted.Text = totalLabelPrinted.ToString("0.#####");
        }


        private void createDataPrintDetail()
        {
            int num = gridViewDataPrintGeneral.RowCount - 1;
            int ordinal = 0;
            for (int i = 0; i < gridViewDataPrintGeneral.RowCount - 1; i++)
            {
                Decimal? packingQuantityDecimal = (Decimal?)gridViewDataPrintGeneral.GetRowCellValue(i, "PackingQuantity");
                int packingQuantity = Convert.ToInt32(packingQuantityDecimal);
                Decimal? quantityDecimal = (Decimal?)gridViewDataPrintGeneral.GetRowCellValue(i, "Quantity");
                int quantity = Convert.ToInt32(quantityDecimal);
                Decimal? packingVolumeDecimal = (Decimal?)gridViewDataPrintGeneral.GetRowCellValue(i, "PackingVolume");
                int packingVolume = Convert.ToInt32(packingVolumeDecimal);
                for (int j = 1; j <= packingQuantity; j++)
                {
                    int num2 = i;
                    int num3 = num;
                    if (num2 > num3 || gridViewDataPrintGeneral.GetRowCellValue(i, gridViewDataPrintGeneral.Columns["colGoodsID"]) == "")
                    {
                        break;
                    }
                    DataPrintDetail dataPrintDetail = new DataPrintDetail();
                    dataPrintDetail.DataPrintNumber = textEditDataPrintNumber.Text;
                    dataPrintDetail.GoodsID = (string?)gridViewDataPrintGeneral.GetRowCellValue(i, "GoodsID");
                    dataPrintDetail.Ordinal = ordinal + 1;
                    dataPrintDetail.Status = "IMPORTED";
                    dataPrintDetail.DateString = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss.fff");
                    if (quantity % packingQuantity != 0)
                    {
                        if (j < packingQuantity)
                        {
                            dataPrintDetail.Quantity = packingVolume;
                        }
                        else
                        {
                            dataPrintDetail.Quantity = quantity % (packingVolume);
                        }
                    }
                    else
                    {
                        dataPrintDetail.Quantity = quantity / packingQuantity;
                    }

                    ordinal++;
                }
            }
        }

        private void sbLoadDataForGridDataPrintDetail(List<DataPrintGeneral> dataPrintGenerals)
        {
            List<DataPrintDetail> dataPrintDetails = new List<DataPrintDetail>();
            int ordinal = 0;
            int number = 1;
            foreach (DataPrintGeneral dataPrintGeneral in dataPrintGenerals)
            {
                number = 1;
                int packingQuantity = Convert.ToInt32(dataPrintGeneral.PackingQuantity);
                int quantity = Convert.ToInt32(dataPrintGeneral.Quantity);
                int packingVolume = Convert.ToInt32(dataPrintGeneral.PackingVolume);

                for (int j = 1; j <= packingQuantity; j++)
                {
                    DataPrintDetail dataPrintDetail = new DataPrintDetail();
                    dataPrintDetail.DataPrintNumber = textEditDataPrintNumber.Text;
                    dataPrintDetail.GoodsID = dataPrintGeneral.GoodsID;
                    dataPrintDetail.Ordinal = ordinal + 1;
                    dataPrintDetail.Status = "IMPORTED";
                    dataPrintDetail.DateString = DateTime.Now.ToString("yyyyMMddhhmmss") + "-" + number;
                    if (quantity % packingQuantity != 0)
                    {
                        if (j < packingQuantity)
                        {
                            dataPrintDetail.Quantity = packingVolume;
                        }
                        else
                        {
                            dataPrintDetail.Quantity = quantity % (packingVolume);
                        }
                    }
                    else
                    {
                        dataPrintDetail.Quantity = quantity / packingQuantity;
                    }
                    dataPrintDetails.Add(dataPrintDetail);
                    ordinal++;
                    number++;
                }
            }
            dataPrintDetailsPublic = dataPrintDetails;
            gridControlDataPrintDetail.DataSource = dataPrintDetails;
        }

        private async Task selectFileExcelAsync()
        {
            OpenFileDialog fdlg = new OpenFileDialog();
            fdlg.Title = "Select file";
            fdlg.InitialDirectory = @"d:\";
            fdlg.Filter = "Excel Sheet(*.xls)|*.xlsx|All Files(*.*)|*.*";
            fdlg.FilterIndex = 1;
            fdlg.RestoreDirectory = true;
            if (fdlg.ShowDialog() == DialogResult.OK)
            {
                List<DataPrintGeneral> dataPrintGenerals = await ExcelToListAsync(fdlg.FileName);

                gridControlDataPrintGeneral.DataSource = dataPrintGenerals;

                sbLoadDataForGridDataPrintDetail(dataPrintGenerals);
                Application.DoEvents();
            }
        }


        private async Task<List<DataPrintGeneral>> ExcelToListAsync(String fileName)
        {
            FileInfo file = new FileInfo(Path.Combine(fileName));

            ExcelPackage.LicenseContext = LicenseContext.Commercial;
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            using (ExcelPackage package = new ExcelPackage(file))
            {

                ExcelWorksheet workSheet = package.Workbook.Worksheets.First();
                textEditDataPrintNumber.Text = workSheet?.Cells[2, 2].Text.Trim();
                textEditDataPrintDate.Text = workSheet?.Cells[2, 3].Text.Trim();

                int totalRows = workSheet.Dimension.Rows;
                List<DataPrintGeneral> dataPrintGenerals = new List<DataPrintGeneral>();
                for (int i = 10; i <= totalRows; i++)
                {
                    dataPrintGenerals.Add(new DataPrintGeneral
                    {
                        GoodsID = workSheet?.Cells[i, 2].Text.Trim().Replace("-", "").Replace(" ", ""),
                        GoodsName = workSheet?.Cells[i, 3].Text.Trim(),

                        PackingVolume = Decimal.Parse(workSheet?.Cells[i, 5].Text.Trim()),
                        Quantity = Decimal.Parse(workSheet?.Cells[i, 7].Text.Trim()),
                        PackingQuantity = Decimal.Parse(workSheet?.Cells[i, 8].Text.Trim()),
                        Status = "IMPORTED",
                    });
                }
                return dataPrintGenerals;
            }
        }

        //barbutton click
        public async void button_Click(object sender, EventArgs e)
        {

            ButtonEventArgs eArg = (DevExpress.XtraBars.Docking2010.ButtonEventArgs)e;
            String tag = eArg.Button.Properties.Tag.ToString();
            switch (tag)
            {
                case "new":
                    if (WMPublic.sbMessageAddNewRequest(this) == true)
                    {

                    }
                    break;
                case "save":

                    break;
                case "delete":
                    if (WMPublic.sbMessageDeleteRequest(this) == true)
                    {


                    }
                    break;
                case "setting":
                    break;
                case "refesh":

                    break;
                case "import":
                    selectFileExcelAsync();
                    getTotalGoodsID();
                    getTotalGoodsIDPrinted();
                    getTotalLabel();
                    getTotalLabelPrinted();
                    break;
                case "export":
                    break;
                case "print":
                    break;
                case "close":
                    break;
            }

        }

        private void simpleButtonShow_Click(object sender, EventArgs e)
        {
            int logoWidth = 50;
            int logoHeight = 50;
            int padding = 10;
            var barcodeString = "AAAAAAAAAAAA";
            var qrcodeString = "BBBBBBBBBBBB";
            var font = new ZplFont(fontWidth: 20, fontHeight: 20);
            var elements = new List<ZplElementBase>();
            elements.Add(new ZplDownloadGraphics('R', "SAMPLE", Helpers.ImageHelper.Resize2Max50Kbytes(System.IO.File.ReadAllBytes("sample2.bmp")), isCompressionActive: true));
            elements.Add(new ZplRecallGraphic(0, 0, 'R', "SAMPLE", 1/2, 1/2));

            elements.Add(new ZplGraphicBox(padding,logoHeight+padding,pictureBox1.Width-padding*2,pictureBox1.Height-logoHeight-padding*2, 1, LineColor.Black));
            elements.Add(new ZplGraphicBox(padding, logoHeight + padding, (pictureBox1.Width - padding * 2) / 2, pictureBox1.Height - logoHeight - padding * 2, 1, LineColor.Black));

            elements.Add(new ZplGraphicBox(padding, logoHeight + padding, (pictureBox1.Width - padding * 2) / 2, (pictureBox1.Height - logoHeight - padding * 2) / 3, 1, LineColor.Black));
            elements.Add(new ZplGraphicBox(padding, logoHeight + padding+ (pictureBox1.Height - logoHeight - padding * 2) / 3, (pictureBox1.Width - padding * 2) / 2, (pictureBox1.Height - logoHeight - padding * 2) / 3, 1, LineColor.Black));

            elements.Add(new ZplTextField("AAAAAAAAAAAA",padding+10, logoHeight + padding+ (pictureBox1.Height - logoHeight - padding * 2)/6, font));
            elements.Add(new ZplTextField( "12",padding+10, logoHeight + padding + (pictureBox1.Height - logoHeight - padding * 2) / 6 *3, font));
            elements.Add(new ZplTextField("091121458741-1",padding+10, logoHeight + padding + (pictureBox1.Height - logoHeight - padding * 2) / 6 *5, font));
            elements.Add(new ZplQrCode(qrcodeString, pictureBox1.Width / 2 + 50, 150, magnificationFactor: 5));


            var renderEngine = new ZplEngine(elements);
            output = renderEngine.ToZplString(new ZplRenderOptions { AddEmptyLineBeforeElementStart = true });
            ouputs.Add(output);


            IPrinterStorage printerStorage = new PrinterStorage();
            var drawer = new ZplElementDrawer(printerStorage);

            var analyzer = new ZplAnalyzer(printerStorage);
            var analyzeInfo = analyzer.Analyze(output);

            foreach (var labelInfo in analyzeInfo.LabelInfos)
            {
                var imageData = drawer.Draw(labelInfo.ZplElements);
                pictureBox1.Image = ByteToImage(imageData);
            }
        }

        private void simpleButtonHide_Click(object sender, EventArgs e)
        {
            dataPrintDetailsModified.Clear();
            PrintDialog PrintDialog = new PrintDialog();
            if (PrintDialog.ShowDialog() == DialogResult.OK)
            {
                foreach (DataPrintDetail dataPrintDetail in dataPrintDetailsPublic)
                {
                    pictureBox1.Size = new Size(600,300);
                    var barcodeString = dataPrintDetail.GoodsID + "-" + dataPrintDetail.Quantity + "-" + dataPrintDetail.DateString;
                    var qrcodeString = "EEDDDDDEEER";
                    var font = new ZplFont(fontWidth: 20, fontHeight: 20);
                    var elements = new List<ZplElementBase>();
                    elements.Add(new ZplDownloadGraphics('R', "SAMPLE",Helpers.ImageHelper.Resize2Max50Kbytes(System.IO.File.ReadAllBytes("sample2.bmp")), isCompressionActive: true));
                    elements.Add(new ZplRecallGraphic(0, 0, 'R', "SAMPLE", 1, 1));
                    
                    elements.Add(new ZplGraphicBox(10, 100, 580, 180, 1, LineColor.Black));
                    elements.Add(new ZplGraphicBox(10, 100, pictureBox1.Width / 2, pictureBox1.Height - 120, 1, LineColor.Black));

                    elements.Add(new ZplGraphicBox(10, 100, pictureBox1.Width / 2, (pictureBox1.Height - 120) / 3, 1, LineColor.Black));
                    elements.Add(new ZplGraphicBox(10, 160, pictureBox1.Width / 2, (pictureBox1.Height - 120) / 3, 1, LineColor.Black));

                    elements.Add(new ZplTextField(dataPrintDetail.GoodsID, 20, 120, font));
                    elements.Add(new ZplTextField(dataPrintDetail.Quantity + "", 20, 180, font));
                    elements.Add(new ZplTextField(dataPrintDetail.DateString, 20, 240, font));
                    elements.Add(new ZplQrCode(qrcodeString, pictureBox1.Width / 2 + 50, 150,magnificationFactor:5));


                    var renderEngine = new ZplEngine(elements);
                    output = renderEngine.ToZplString(new ZplRenderOptions { AddEmptyLineBeforeElementStart = true });
                    Logger.WriteLogger(output);



                    var tcpClient = new System.Net.Sockets.TcpClient();
                    try
                    {
                        tcpClient.Connect("127.0.0.1", 9100);

                        // Send Zpl data to printer
                        foreach (String o in ouputs)
                        {
                            var writer = new System.IO.StreamWriter(tcpClient.GetStream());
                            writer.Write(output);
                            writer.Flush();
                            // Close Connection
                            writer.Close();
                            tcpClient.Close();
                        }



                    }
                    catch (SocketException ex)
                    {
                        MessageBox.Show("Không thể kết nối máy in "+ex.Message);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Any other exception Error "+ex.Message);
                    }

                    //ouputs.Add(output);
                    dataPrintDetail.Status = "PRINTED";
                    dataPrintDetailsModified.Add(dataPrintDetail);
                }
                gridControlDataPrintDetail.DataSource = dataPrintDetailsModified;
                MessageBox.Show("In xong " + dataPrintDetailsModified.Count + " tem");
                getTotalGoodsID();
                getTotalGoodsIDPrinted();
                getTotalLabel();
                getTotalLabelPrinted();
            }

        }

        private void simpleButtonSetWidthHeight_Click(object sender, EventArgs e)
        {
            pictureBox1.Width = int.Parse(textEditWidth.Text);
            pictureBox1.Height = int.Parse(textEditHeight.Text);
        }
    }
}
