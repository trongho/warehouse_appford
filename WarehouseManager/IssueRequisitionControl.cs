﻿using DevExpress.XtraBars.Docking2010;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraLayout.Filtering.Templates;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WarehouseManager.Models;
using WarehouseManager.Repository;
using webservice.Entities;
using LicenseContext = OfficeOpenXml.LicenseContext;
using DevExpress.XtraEditors.Repository;
using System.Collections;
using System.Threading;

namespace WarehouseManager
{
    public partial class IssueRequisitionControl : DevExpress.XtraEditors.XtraUserControl
    {
        WIRDetailRepository wIRDetailRepository;
        WIRHeaderRepository wIRHeaderRepository;
        HandlingStatusRepository handlingStatusRepository;
        BranchRepository branchRepository;
        WIDataHeaderRepository wIDataHeaderRepository;
        WIDataDetailRepository wIDataDetailRepository;
        WIDataGeneralRepository wIDataGeneralRepository;
        GoodsRepository goodsRepository;
        public static string SelectedTable = string.Empty;
        public static String selectedGoodsID;
        public static String selectedGoodsName;
        public static String selectedWIRNumber;
        String lastWIRNumber = "";
        Boolean isWIRNumberExist = false;
        Boolean isCreatWIDataGeneralComplete = false;
        public IssueRequisitionControl()
        {
            InitializeComponent();
            this.Load += IssueRequisitionControl_Load;
            wIRDetailRepository = new WIRDetailRepository();
            wIRHeaderRepository = new WIRHeaderRepository();
            handlingStatusRepository = new HandlingStatusRepository();
            branchRepository = new BranchRepository();
            wIDataHeaderRepository = new WIDataHeaderRepository();
            wIDataDetailRepository = new WIDataDetailRepository();
            wIDataGeneralRepository = new WIDataGeneralRepository();
            goodsRepository = new GoodsRepository();
        }

        private void IssueRequisitionControl_Load(Object sender, EventArgs e)
        {
            popupMenuSelectHandlingStatus();
            popupMenuSelectBranch();
            windowsUIButtonPanel1.ButtonClick += button_Click;
            textEditWIRNumber.KeyDown += new KeyEventHandler(WRRNumber_KeyDown);
            gridViewWIRDetail.CustomColumnDisplayText += wIRHeaderGridView_CustomColumnDisplayText;
            gridViewWIRDetail.DoubleClick += dataGridViewWRR_CellDoubleClick;
            gridViewWIRDetail.CellValueChanged += YourDGV_CellValueChanged;
            gridViewWIRDetail.ValidatingEditor += gridView1_ValidatingEditor;
            textEditWIRNumber.DoubleClick += textEditWRRNumer_CellDoubleClick;
            gridViewWIRDetail.ClearSorting();
            sbLoadDataForGridWIRDetailAsync(textEditWIRNumber.Text);

        }

        private async Task popupMenuSelectHandlingStatus()
        {
            List<HandlingStatus> handlingStatuses = await handlingStatusRepository.GetAll();
            foreach (HandlingStatus handlingStatus in handlingStatuses)
            {
                comboBoxEditHandlingStatus.Properties.Items.Add(handlingStatus.HandlingStatusName);
            }
            comboBoxEditHandlingStatus.SelectedIndex = 1;
        }

        private async Task popupMenuSelectBranch()
        {
            List<Branch> branches = await branchRepository.getAllBranchAsync();
            foreach (Branch branch in branches)
            {
                comboBoxEditBranch.Properties.Items.Add(branch.BranchName);
            }
            comboBoxEditBranch.SelectedIndex = 0;
        }


        private void YourDGV_CellValueChanged(object sender, CellValueChangedEventArgs e)
        {
            if (e.Column.FieldName.Equals("QuantityByItem"))
            {
                getTotalQuantityByItem();
            }
            if (e.Column.FieldName.Equals("QuantityByPack"))
            {
                getTotalQuantityByPack();
            }
        }

        private void gridView1_ValidatingEditor(object sender, DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs e)
        {
            GridView view = sender as GridView;

            object val = e.Value;

            if (view.FocusedColumn.FieldName == "GoodsID")
            {
                if (val == null || string.IsNullOrWhiteSpace(e.Value.ToString()))
                {
                    e.Valid = false;
                    e.ErrorText = "Bạn nhập mã hàng null";
                }
                if (checkExistGoodsID(e.Value.ToString()) == true)
                {

                    e.Valid = false;
                    e.ErrorText = "Mã hàng hóa trùng";
                }
            }
        }

        private Boolean checkExistGoodsID(String goodsID)
        {
            int num = gridViewWIRDetail.RowCount - 1;
            int i = 0;
            while (true)
            {
                int num2 = i;
                int num3 = num;
                if (num2 > num3 || gridViewWIRDetail.GetRowCellValue(i, gridViewWIRDetail.Columns["colGoodsID"]) == "")
                {
                    break;
                }
                int rowHandle = gridViewWIRDetail.GetRowHandle(i);
                String goodsID1 = (String)gridViewWIRDetail.GetRowCellValue(rowHandle, "GoodsID");
                if (goodsID.Equals(goodsID1))
                {
                    return true;
                }
                i++;
            }
            return false;
        }

        private Boolean checkRowBeforeNull()
        {
            int rowHandle = gridViewWIRDetail.FocusedRowHandle;
            String goodsID1 = (String)gridViewWIRDetail.GetRowCellValue(rowHandle - 1, "GoodsID");
            if (rowHandle > 0 && goodsID1 == null)
            {
                return true;
            }
            return false;
        }

        private void gridView1_ShownEditor(object sender, EventArgs e)
        {
            GridView view = sender as GridView;

            view.ActiveEditor.IsModified = true;
        }

        private void getTotalQuantityByItem()
        {
            Decimal totalQuantityByItem = 0;
            int num = gridViewWIRDetail.RowCount - 1;
            int i = 0;
            while (true)
            {
                int rowHandle = gridViewWIRDetail.GetRowHandle(i);
                if (gridViewWIRDetail.GetRowCellValue(rowHandle, "GoodsID") == null)
                {
                    break;
                }
                Decimal quantityByItem = (Decimal)gridViewWIRDetail.GetRowCellValue(rowHandle, "QuantityByItem");
                totalQuantityByItem += quantityByItem;
                i++;
            }
            textEditTotalQuantityByItem.Text = totalQuantityByItem.ToString();
        }

        private void getTotalQuantityByPack()
        {
            Decimal totalQuantityByPack = 0;
            int i = 0;
            while (true)
            {
                int rowHandle = gridViewWIRDetail.GetRowHandle(i);
                if (gridViewWIRDetail.GetRowCellValue(rowHandle, "GoodsID") == null)
                {
                    break;
                }
                totalQuantityByPack++;
                i++;
            }
            textEditTotalQuantityByPack.Text = totalQuantityByPack + "";
        }

        private void wIRHeaderGridView_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {

            if (e.Column == colWIRDetailNo)
            {
                int rowHandle = gridViewWIRDetail.GetRowHandle(e.ListSourceRowIndex);
                if (rowHandle >= 0)
                {
                    e.DisplayText = (rowHandle + 1).ToString();
                }
            }
        }

        private async Task sbLoadDataForGridWIRDetailAsync(String wRRNumber)
        {
            List<WIRDetail> WIRDetails = await wIRDetailRepository.GetUnderID(wRRNumber);
            int mMaxRow = 100;
            if (WIRDetails.Count < mMaxRow)
            {
                int num = mMaxRow - WIRDetails.Count;
                int i = 1;
                while (true)
                {
                    int num2 = i;
                    int num3 = num;
                    if (num2 > num3)
                    {
                        break;
                    }
                    WIRDetails.Add(new WIRDetail());
                    i++;
                }
            }
            gridControlWIRDetail.DataSource = WIRDetails;
            getTotalQuantityByItem();
            getTotalQuantityByPack();
        }

        private async Task sbLoadDataForGridWIRDetailByGoodsGroupID(String wRRNumber, String goodsGroupID)
        {
            List<WIRDetail> WIRDetails = await wIRDetailRepository.GetUnderGoodsGroupID(wRRNumber, goodsGroupID);
            int mMaxRow = 100;
            if (WIRDetails.Count < mMaxRow)
            {
                int num = mMaxRow - WIRDetails.Count;
                int i = 1;
                while (true)
                {
                    int num2 = i;
                    int num3 = num;
                    if (num2 > num3)
                    {
                        break;
                    }
                    WIRDetails.Add(new WIRDetail());
                    i++;
                }
            }
            gridControlWIRDetail.DataSource = WIRDetails;
            getTotalQuantityByItem();
            getTotalQuantityByPack();
        }

        public async Task getlastWIRNumberAsync()
        {
            lastWIRNumber = await wIRHeaderRepository.GetLastWRRNumber();
            String wRRNumberYear = DateTime.Now.Year.ToString().Substring(2, 2);
            if (lastWIRNumber == "")
            {
                textEditWIRNumber.Text = wRRNumberYear + "000" + "-" + $"{1:D5}";
            }
            else
            {
                string[] arrListStr = lastWIRNumber.Split('-');
                String wRRNumberPartOne = arrListStr[0];
                String wRRNumberPartTwo = arrListStr[1];
                wRRNumberPartTwo = (int.Parse(wRRNumberPartTwo) + 1).ToString();
                textEditWIRNumber.Text = wRRNumberYear + "000" + "-" + $"{int.Parse(wRRNumberPartTwo):D5}";
            }


        }

        public async Task createWIRHeaderAsync()
        {
            WIRHeader wIRHeader = new WIRHeader();
            wIRHeader.WIRNumber = textEditWIRNumber.Text;
            wIRHeader.WIRDate = DateTime.Parse(dateEditWIRDate.DateTime.ToString("yyyy-MM-dd"));
            wIRHeader.ReferenceNumber = wIRHeader.WIRNumber = textEditWIRNumber.Text;
            wIRHeader.HandlingStatusID = (comboBoxEditHandlingStatus.SelectedIndex - 1).ToString();
            wIRHeader.HandlingStatusName = comboBoxEditHandlingStatus.SelectedItem.ToString();
            wIRHeader.BranchID = $"{int.Parse(comboBoxEditBranch.SelectedIndex.ToString()) - 1:D3}";
            wIRHeader.BranchName = comboBoxEditBranch.SelectedItem.ToString();
            wIRHeader.Note = "";
            wIRHeader.TotalQuantity = Decimal.Parse(textEditTotalQuantityByItem.Text);
            wIRHeader.CreatedUserID = WMMessage.User.UserID;
            wIRHeader.CreatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));

            if (await wIRHeaderRepository.Create(wIRHeader) > 0)
            {
                try
                {
                    Cursor = Cursors.WaitCursor;
                    await createWIRDetailAsync();
                }
                finally
                {
                    WMPublic.sbMessageSaveNewSuccess(this);
                    Cursor = Cursors.Default;
                }

            }
        }

        public async Task createWIRDetailAsync()
        {
            int num = gridViewWIRDetail.RowCount - 1;
            int i = 0;
            while (true)
            {
                int num2 = i;
                int num3 = num;
                if (num2 > num3 || gridViewWIRDetail.GetRowCellValue(i, gridViewWIRDetail.Columns["colGoodsID"]) == "")
                {
                    break;
                }
                int rowHandle = gridViewWIRDetail.GetRowHandle(i);
                WIRDetail WIRDetail = new WIRDetail();
                WIRDetail.WIRNumber = textEditWIRNumber.Text;
                WIRDetail.GoodsID = (string?)gridViewWIRDetail.GetRowCellValue(rowHandle, "GoodsID");
                WIRDetail.GoodsName = (string?)gridViewWIRDetail.GetRowCellValue(rowHandle, "GoodsName");
                WIRDetail.Ordinal = i + 1;
                WIRDetail.OtherGoodsName = (string?)gridViewWIRDetail.GetRowCellValue(rowHandle, "OtherGoodsName");
                WIRDetail.PackingVolume = (Decimal?)gridViewWIRDetail.GetRowCellValue(rowHandle, "PackingVolume");
                WIRDetail.TotalQuantity = (Decimal?)gridViewWIRDetail.GetRowCellValue(rowHandle, "TotalQuantity");
                WIRDetail.QuantityByItem = (Decimal?)gridViewWIRDetail.GetRowCellValue(rowHandle, "QuantityByItem");
                WIRDetail.QuantityByPack = (Decimal?)gridViewWIRDetail.GetRowCellValue(rowHandle, "QuantityByPack");
                WIRDetail.LocationID = (string?)gridViewWIRDetail.GetRowCellValue(rowHandle, "LocationID");
                WIRDetail.ScanOption = (Int16?)gridViewWIRDetail.GetRowCellValue(rowHandle, "ScanOption");
                WIRDetail.Note = "";
                WIRDetail.GoodsGroupID = (string?)gridViewWIRDetail.GetRowCellValue(rowHandle, "GoodsGroupID");
                WIRDetail.PickerName = (string?)gridViewWIRDetail.GetRowCellValue(rowHandle, "PickerName");

                if (await wIRDetailRepository.Create(WIRDetail) > 0)
                {
                    i++;
                }
                //if (num2==num3)
                //{
                //    WMPublic.sbMessageSaveNewSuccess(this);
                //}
                //else
                //{
                //    MessageBox.Show("Đang lưu cơ sở dữ liệu.........");
                //}
            }
        }

        public async Task createWIDataHeaderAsync()
        {
            WIRHeader wIRHeader = await wIRHeaderRepository.GetUnderID(textEditWIRNumber.Text);
            WIDataHeader WIDataHeader = new WIDataHeader();
            WIDataHeader.WIDNumber = textEditWIRNumber.Text;
            WIDataHeader.WIDDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));
            WIDataHeader.ReferenceNumber = textEditWIRNumber.Text;
            WIDataHeader.WIRNumber = textEditWIRNumber.Text;
            WIDataHeader.WIRReference = textEditWIRNumber.Text;
            WIDataHeader.BranchID = wIRHeader.BranchID;
            WIDataHeader.BranchName = wIRHeader.BranchName;
            WIDataHeader.HandlingStatusID = "0";
            WIDataHeader.HandlingStatusName = "Chưa duyệt";
            WIDataHeader.Note = "";
            WIDataHeader.Status = "0";
            WIDataHeader.TotalQuantity = Decimal.Parse(textEditTotalQuantityByItem.Text);
            WIDataHeader.TotalQuantityOrg = Decimal.Parse(textEditTotalQuantityByItem.Text);
            WIDataHeader.CreatedUserID = WMMessage.User.UserID;
            WIDataHeader.CreatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));

            if (await wIDataHeaderRepository.Create(WIDataHeader) > 0)
            {
                try
                {
                    Application.UseWaitCursor = true;
                    await createWIDataGeneralAsync();
                }
                finally
                {

                    MessageBox.Show("Đã tạo dữ liệu xuất");
                    Application.UseWaitCursor = false;
                }
            }
        }

        public async Task createWIDataGeneralAsync()
        {
            int num = gridViewWIRDetail.RowCount - 1;
            for (int i = 0; i < gridViewWIRDetail.RowCount - 1; i++)
            {
                int num2 = i;
                int num3 = num;
                if (num2 > num3 || gridViewWIRDetail.GetRowCellValue(i, gridViewWIRDetail.Columns["colGoodsID"]) == "")
                {
                    break;
                }
                WIDataGeneral wIDataGeneral = new WIDataGeneral();
                wIDataGeneral.IDCode = "";
                wIDataGeneral.WIDNumber = textEditWIRNumber.Text;
                wIDataGeneral.GoodsID = (string?)gridViewWIRDetail.GetRowCellValue(i, "GoodsID");
                wIDataGeneral.GoodsName = (string?)gridViewWIRDetail.GetRowCellValue(i, "GoodsName");
                wIDataGeneral.Ordinal = i + 1;
                wIDataGeneral.LocationID = "";
                wIDataGeneral.Quantity = 0m;
                wIDataGeneral.TotalQuantity = Decimal.Parse(textEditTotalQuantityByItem.Text);
                wIDataGeneral.TotalGoods = Convert.ToDecimal(gridViewWIRDetail.RowCount - 1);
                wIDataGeneral.PackingQuantity = 0m;
                wIDataGeneral.QuantityOrg = (Decimal?)gridViewWIRDetail.GetRowCellValue(i, "QuantityByItem");
                wIDataGeneral.TotalQuantityOrg = Decimal.Parse(textEditTotalQuantityByItem.Text);

                //List<WIRDetail> wIRDetails = await wIRDetailRepository.GetUnderID(textEditWIRNumber.Text);
                wIDataGeneral.TotalGoodsOrg = Convert.ToDecimal(gridViewWIRDetail.RowCount - 1);
                wIDataGeneral.LocationIDOrg = (String?)gridViewWIRDetail.GetRowCellValue(i, "LocationID");

                wIDataGeneral.CreatorID = WMMessage.User.UserID; ;
                wIDataGeneral.CreatedDateTime = DateTime.Now.ToString("dd/MM/yyyy");
                wIDataGeneral.Status = "1";
                wIDataGeneral.PackingVolume = (Decimal?)gridViewWIRDetail.GetRowCellValue(i, "PackingVolume");
                wIDataGeneral.QuantityByPack = (Decimal?)gridViewWIRDetail.GetRowCellValue(i, "QuantityByPack");
                wIDataGeneral.QuantityByItem = (Decimal?)gridViewWIRDetail.GetRowCellValue(i, "QuantityByItem");
                wIDataGeneral.Note = "";
                wIDataGeneral.ScanOption = (Int16?)gridViewWIRDetail.GetRowCellValue(i, "ScanOption");
                wIDataGeneral.GoodsGroupID = (String?)gridViewWIRDetail.GetRowCellValue(i, "GoodsGroupID");
                wIDataGeneral.PickerName = (String?)gridViewWIRDetail.GetRowCellValue(i, "PickerName");

                await wIDataGeneralRepository.Create(wIDataGeneral);
            }
            isCreatWIDataGeneralComplete = true;

        }


        public async Task updatewIRHeaderAsync()
        {
            WIRHeader wIRHeader = await wIRHeaderRepository.GetUnderID(textEditWIRNumber.Text);
            wIRHeader.WIRNumber = textEditWIRNumber.Text;
            wIRHeader.WIRDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));
            wIRHeader.ReferenceNumber = textEditWIRNumber.Text;
            wIRHeader.HandlingStatusID = (comboBoxEditHandlingStatus.SelectedIndex - 1).ToString();
            wIRHeader.HandlingStatusName = comboBoxEditHandlingStatus.SelectedItem.ToString();
            wIRHeader.BranchID = $"{int.Parse(comboBoxEditBranch.SelectedIndex.ToString()) - 1:D3}";
            wIRHeader.BranchName = comboBoxEditBranch.SelectedItem.ToString();
            wIRHeader.Note = "";
            wIRHeader.TotalQuantity = Decimal.Parse(textEditTotalQuantityByItem.Text);
            wIRHeader.UpdatedUserID = WMMessage.User.UserID;
            wIRHeader.UpdatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));

            if (await wIRHeaderRepository.Update(wIRHeader, textEditWIRNumber.Text) > 0)
            {
                updateWIRDetailAsync();
               
                WMPublic.sbMessageSaveChangeSuccess(this);
                if (wIRHeader.HandlingStatusID.Equals("2"))
                {
                    createWIDataHeaderAsync();
                }
            }

        }

        public async Task updateWIRDetailAsync()
        {
            int num = gridViewWIRDetail.RowCount - 1;
            int i = 0;
            while (true)
            {
                int num2 = i;
                int num3 = num;
                if (num2 > num3 || gridViewWIRDetail.GetRowCellValue(i, gridViewWIRDetail.Columns["colGoodsID"]) == "")
                {
                    break;
                }
                int rowHandle = gridViewWIRDetail.GetRowHandle(i);
                WIRDetail WIRDetail = new WIRDetail();
                WIRDetail.WIRNumber = textEditWIRNumber.Text;
                WIRDetail.GoodsID = (string?)gridViewWIRDetail.GetRowCellValue(rowHandle, "GoodsID");
                WIRDetail.GoodsName = (string?)gridViewWIRDetail.GetRowCellValue(rowHandle, "GoodsName");
                WIRDetail.Ordinal = i + 1;
                WIRDetail.OtherGoodsName = (string?)gridViewWIRDetail.GetRowCellValue(rowHandle, "OtherGoodsName");
                WIRDetail.PackingVolume = (Decimal?)gridViewWIRDetail.GetRowCellValue(rowHandle, "PackingVolume");
                WIRDetail.TotalQuantity = (Decimal?)gridViewWIRDetail.GetRowCellValue(rowHandle, "TotalQuantity");
                WIRDetail.QuantityByItem = (Decimal?)gridViewWIRDetail.GetRowCellValue(rowHandle, "QuantityByItem");
                WIRDetail.QuantityByPack = (Decimal?)gridViewWIRDetail.GetRowCellValue(rowHandle, "QuantityByPack");
                WIRDetail.LocationID = (string?)gridViewWIRDetail.GetRowCellValue(rowHandle, "LocationID");
                WIRDetail.ScanOption = (Int16?)gridViewWIRDetail.GetRowCellValue(rowHandle, "ScanOption");
                WIRDetail.Note = "";
                WIRDetail.GoodsGroupID = (string?)gridViewWIRDetail.GetRowCellValue(rowHandle, "GoodsGroupID");
                WIRDetail.PickerName = (string?)gridViewWIRDetail.GetRowCellValue(rowHandle, "PickerName");

                if (await wIRDetailRepository.Update(WIRDetail, textEditWIRNumber.Text, WIRDetail.GoodsID, WIRDetail.Ordinal, WIRDetail.GoodsGroupID) > 0)
                {
                    i++;
                }
            }
        }

        public async Task DeletewIRHeaderAsync()
        {
            if ((await wIRHeaderRepository.Delete(textEditWIRNumber.Text)) > 0)
            {
                await DeleteWIRDetailAsync();
                sbLoadDataForGridWIRDetailAsync(textEditWIRNumber.Text);
                WMPublic.sbMessageDeleteSuccess(this);
            }
        }

        public async Task DeleteWIRDetailAsync()
        {
            await wIRDetailRepository.Delete(textEditWIRNumber.Text);
            clearAsync();
            gridControlWIRDetail.DataSource = null;
        }

        private void clearAsync()
        {
            getlastWIRNumberAsync();
            dateEditWIRDate.DateTime = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));
            comboBoxEditHandlingStatus.SelectedIndex = 1;
            textEditTotalQuantityByItem.Text = "0";
            textEditTotalQuantityByPack.Text = "0";
            List<WIRDetail> WIRDetails = new List<WIRDetail>();

            for (int i = 0; i < 100; i++)
            {
                WIRDetails.Add(new WIRDetail());
            }
            gridControlWIRDetail.DataSource = WIRDetails;
        }

        private async Task checkExistWIRNumber(String wIRNumber)
        {
            if (!wIRNumber.Equals("") && (await wIRHeaderRepository.checkExistAsync(wIRNumber)) == true)
            {
                isWIRNumberExist = true;
            }
        }


        private async void WRRNumber_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {

                WIRHeader wIRHeader = new WIRHeader();
                wIRHeader = await wIRHeaderRepository.GetUnderID(textEditWIRNumber.Text);
                if (wIRHeader != null)
                {
                    dateEditWIRDate.Text = wIRHeader.WIRDate.ToString();
                    comboBoxEditBranch.SelectedIndex = int.Parse($"{int.Parse(wIRHeader.BranchID) + 1:D1}");
                    comboBoxEditHandlingStatus.SelectedIndex = int.Parse(wIRHeader.HandlingStatusID) + 1;
                    sbLoadDataForGridWIRDetailAsync(textEditWIRNumber.Text);
                }
                else
                {
                    MessageBox.Show("Số phiếu yêu cầu không tồn tại");
                    return;
                }

            }
        }


        private void dataGridViewWRR_CellDoubleClick(object sender, EventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView sndr =
                sender as DevExpress.XtraGrid.Views.Grid.GridView;

            DevExpress.Utils.DXMouseEventArgs dxMouseEventArgs =
                e as DevExpress.Utils.DXMouseEventArgs;


            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hitInfo =
                   sndr.CalcHitInfo(dxMouseEventArgs.Location);
            if (hitInfo.InRowCell)
            {
                if (checkRowBeforeNull() == true)
                {
                    MessageBox.Show("Hàng phía trước đang trống");
                    return;
                }
                if (gridViewWIRDetail.FocusedColumn.FieldName.Equals("GoodsID"))
                {

                    frmSearchGoods frmSearchGoods = new frmSearchGoods();
                    frmSearchGoods.isIssueRequisition = true;
                    frmSearchGoods.ShowDialog(this);
                    frmSearchGoods.Dispose();
                    insertData();
                }
            }
        }

        private async void textEditWRRNumer_CellDoubleClick(object sender, EventArgs e)
        {
            frmSearchWIR frmSearchWIR = new frmSearchWIR();
            frmSearchWIR.ShowDialog(this);
            frmSearchWIR.Dispose();
            if (selectedWIRNumber != null)
            {
                textEditWIRNumber.Text = selectedWIRNumber;
                WIRHeader wIRHeader = await wIRHeaderRepository.GetUnderID(textEditWIRNumber.Text);
                textEditWIRNumber.Text = wIRHeader.WIRNumber;
                dateEditWIRDate.Text = wIRHeader.WIRDate.ToString();
                comboBoxEditBranch.SelectedIndex = int.Parse($"{int.Parse(wIRHeader.BranchID) + 1:D1}");
                comboBoxEditHandlingStatus.SelectedIndex = int.Parse(wIRHeader.HandlingStatusID) + 1;
                await sbLoadDataForGridWIRDetailAsync(selectedWIRNumber);
            }

        }

        private void insertData()
        {
            int rowHandle = gridViewWIRDetail.FocusedRowHandle;
            gridViewWIRDetail.SetRowCellValue(rowHandle, gridViewWIRDetail.Columns["GoodsID"], selectedGoodsID);
            gridViewWIRDetail.SetRowCellValue(rowHandle, gridViewWIRDetail.Columns["GoodsName"], selectedGoodsName);
            selectedGoodsID = "";
            selectedGoodsName = "";
        }

        private async Task selectFileExcelAsync()
        {
            OpenFileDialog fdlg = new OpenFileDialog();
            fdlg.Title = "Select file";
            fdlg.InitialDirectory = @"d:\";
            fdlg.Filter = "Excel Sheet(*.xls)|*.xlsx|All Files(*.*)|*.*";
            fdlg.FilterIndex = 1;
            fdlg.RestoreDirectory = true;
            if (fdlg.ShowDialog() == DialogResult.OK)
            {
                List<WIRDetail> WIRDetails = await ExcelToListAsync(fdlg.FileName);
                int mMaxRow = 100;
                if (WIRDetails.Count < mMaxRow)
                {
                    int num = mMaxRow - WIRDetails.Count;
                    int i = 1;
                    while (true)
                    {
                        int num2 = i;
                        int num3 = num;
                        if (num2 > num3)
                        {
                            break;
                        }
                        WIRDetails.Add(new WIRDetail());
                        i++;
                    }
                }
                gridControlWIRDetail.DataSource = WIRDetails;
                getTotalQuantityByItem();
                getTotalQuantityByPack();
                Application.DoEvents();
            }
        }


        private async Task<List<WIRDetail>> ExcelToListAsync(String fileName)
        {
            FileInfo file = new FileInfo(Path.Combine(fileName));

            ExcelPackage.LicenseContext = LicenseContext.Commercial;
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            using (ExcelPackage package = new ExcelPackage(file))
            {

                ExcelWorksheet workSheet = package.Workbook.Worksheets.First();
                textEditWIRNumber.Text = workSheet?.Cells[2, 3].Text.Trim();
                dateEditWIRDate.Text = DateTime.Now.ToString("yyyy-MM-dd");

                int totalRows = workSheet.Dimension.Rows;
                int totalColumns = workSheet.Dimension.Columns;
                List<WIRDetail> wIRDetails = new List<WIRDetail>();
                for (int i = 6; i <= totalRows; i++)
                {
                    for (int j = 7; j <= totalColumns; j++)
                    {
                        if (workSheet.Cells[i, j].Text.Trim() != ""&& workSheet.Cells[i,5].Text.Trim().Equals("1"))
                        {
                            wIRDetails.Add(new WIRDetail
                            {
                                GoodsID = workSheet?.Cells[i, 2].Text.Trim(),
                                GoodsName = workSheet?.Cells[i, 3].Text.Trim(),
                                TotalQuantity = Decimal.Parse(workSheet?.Cells[i, j].Text.Trim()),
                                QuantityByItem = Decimal.Parse(workSheet?.Cells[i, j].Text.Trim()),
                                QuantityByPack = Decimal.Parse(workSheet.Cells[i, j].Text.Trim()),
                                PackingVolume = Decimal.Parse(workSheet.Cells[i, j].Text.Trim()),
                                GoodsGroupID = workSheet?.Cells[5, j].Text.Trim(),
                                PickerName = workSheet?.Cells[i, 4].Text.Trim(),
                                ScanOption = Int16.Parse(workSheet?.Cells[i, 5].Text.Trim()),
                            });
                        }
                        //else
                        //{
                        //    wIRDetails.Add(new WIRDetail
                        //    {
                        //        GoodsID = workSheet?.Cells[i, 2].Text.Trim(),
                        //        GoodsName = workSheet?.Cells[i, 3].Text.Trim(),
                        //        TotalQuantity = 0m,
                        //        QuantityByItem = 0m,
                        //        QuantityByPack = 0m,
                        //        PackingVolume = 0m,
                        //        GoodsGroupID = workSheet?.Cells[5, j].Text.Trim(),
                        //        PickerName = workSheet?.Cells[i, 4].Text.Trim(),
                        //        ScanOption = Int16.Parse(workSheet?.Cells[i, 5].Text.Trim()),
                        //    });
                        //}
                    }
                }
                return wIRDetails;
            }
        }


        //barbutton click
        public async void button_Click(object sender, EventArgs e)
        {

            ButtonEventArgs eArg = (DevExpress.XtraBars.Docking2010.ButtonEventArgs)e;
            String tag = eArg.Button.Properties.Tag.ToString();
            switch (tag)
            {
                case "new":
                    if (WMPublic.sbMessageAddNewRequest(this) == true)
                    {
                        clearAsync();
                    }
                    break;
                case "save":
                    checkExistWIRNumber(textEditWIRNumber.Text);

                    if (isWIRNumberExist == true)
                    {
                        if (WMPublic.sbMessageSaveChangeRequest(this) == true)
                        {
                            await updatewIRHeaderAsync();
                        }

                    }
                    else
                    {
                        if (WMPublic.sbMessageSaveNewRequest(this) == true)
                        {
                            await getlastWIRNumberAsync();
                            await createWIRHeaderAsync();
                        }
                    }
                    break;
                case "delete":
                    if (WMPublic.sbMessageDeleteRequest(this) == true)
                    {
                        await DeletewIRHeaderAsync();
                    }
                    break;
                case "search":
                    break;
                case "refesh":
                    break;
                case "import":
                    try
                    {
                        Application.UseWaitCursor = true;
                        await selectFileExcelAsync();
                    }
                    finally
                    {
                        MessageBox.Show("Tải thành công file excel");
                        Application.UseWaitCursor = false;
                    }
                    break;
                case "export":
                    break;
                case "print":
                    break;
                case "close":
                    break;
            }

        }

    }
}
