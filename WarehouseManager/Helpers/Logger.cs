﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarehouseManager.Helpers
{
    public class Logger
    {
        public static void WriteLogger(string lines)
        {
            //Write the string to a file.append mode is enabled so that the log
            //lines get appended to  test.txt than wiping content and writing the log

            using (System.IO.StreamWriter file = new System.IO.StreamWriter("c:\\debug.txt", true))
            {
                file.WriteLine(lines);
            }
        }
    }
}
