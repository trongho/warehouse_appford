﻿using DevExpress.XtraBars.Docking2010;
using DevExpress.XtraGrid.Views.Grid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WarehouseManager.Models;
using WarehouseManager.Repository;
using webservice.Entities;

namespace WarehouseManager
{
    public partial class frmSearchCGD : Form
    {
        HandlingStatusRepository handlingStatusRepository;
        OrderStatusRepository orderStatusRepository;
        BranchRepository branchRepository;
        CGDataHeaderRepository cgDataHeaderRepository;
        public frmSearchCGD()
        {
            InitializeComponent();
            this.Load += frmSearchCGD_Load;
            handlingStatusRepository = new HandlingStatusRepository();
            branchRepository = new BranchRepository();
            cgDataHeaderRepository = new CGDataHeaderRepository();
            orderStatusRepository = new OrderStatusRepository();
        }

        private void frmSearchCGD_Load(object sender, EventArgs e)
        {
            popupMenuSelectBranch();
            popupMenuSelectHandlingStatus();
            popupMenuSelectOrderStatus();
            sbLoadDataForGridCGDHeaderAsync();
            gridViewWRDataHeader.DoubleClick += dataGridView_CellDoubleClick;
            simpleButtonFilter.Click += filterButton_Click;
            windowsUIButtonPanel1.ButtonClick += button_Click;
            this.KeyDown += new KeyEventHandler(Form1_KeyDown);

            if (int.Parse(DateTime.Now.Day.ToString()) == 1)
            {
                dateEditFromDate.Text = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day).ToString("yyyy-MM-dd");
            }
            else
            {
                dateEditFromDate.Text = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day - 1).ToString("yyyy-MM-dd");
            }
            dateEditToDate.Text = DateTime.Now.ToString("yyyy-MM-dd");
        }

        public async void button_Click(object sender, EventArgs e)
        {
            ButtonEventArgs eArg = (DevExpress.XtraBars.Docking2010.ButtonEventArgs)e;
            String tag = eArg.Button.Properties.Tag.ToString();
            switch (tag)
            {
                case "refesh":
                    sbLoadDataForGridCGDHeaderAsync();
                    break;
                case "close":
                    this.Close();
                    break;
            }

        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F5)
            {
                sbLoadDataForGridCGDHeaderAsync();
            }
            if (e.KeyCode == Keys.F10)
            {
                this.Close();
            }
        }

        private async Task popupMenuSelectHandlingStatus()
        {
            List<HandlingStatus> handlingStatuses = await handlingStatusRepository.GetAll();
            foreach (HandlingStatus handlingStatus in handlingStatuses)
            {
                comboBoxEditHandlingStatus.Properties.Items.Add(handlingStatus.HandlingStatusName);
            }
            comboBoxEditHandlingStatus.SelectedIndex = 0;
        }

        private async Task popupMenuSelectOrderStatus()
        {
            List<OrderStatus> orderStatuses = await orderStatusRepository.GetAll();
            foreach (OrderStatus orderStatus in orderStatuses)
            {
                comboBoxEditOrderStatus.Properties.Items.Add(orderStatus.OrderStatusName);
            }
            comboBoxEditOrderStatus.SelectedIndex = 0;
        }

        private async Task popupMenuSelectBranch()
        {
            List<Branch> branches = await branchRepository.getAllBranchAsync();
            foreach (Branch branch in branches)
            {
                comboBoxEditBranch.Properties.Items.Add(branch.BranchName);
            }
            comboBoxEditBranch.SelectedIndex = 0;
        }

        private async Task sbLoadDataForGridCGDHeaderAsync()
        {
            List<CGDataHeader> cgDataHeaders = await cgDataHeaderRepository.GetAll();
            gridControlWRDataHeader.DataSource =cgDataHeaders;
        }

        private async void dataGridView_CellDoubleClick(object sender, EventArgs e)
        {
            if (gridViewWRDataHeader.RowCount > 0)
            {
                CGDataHeader cgDataHeader = await cgDataHeaderRepository.GetUnderID((string)(sender as GridView).GetFocusedRowCellValue("CGDNumber"));
                CheckGoodsDataControl.selectedCGDNumber = (string)(sender as GridView).GetFocusedRowCellValue("CGDNumber");
                this.Close();
            }
        }

        private async void filterButton_Click(Object sender,EventArgs e)
        {
            String branchID = $"{int.Parse(comboBoxEditBranch.SelectedIndex.ToString()) - 1:D3}";
            String handlingStatusID = (comboBoxEditHandlingStatus.SelectedIndex - 1).ToString();
            String orderStatusID = (comboBoxEditOrderStatus.SelectedIndex-1).ToString();
            DateTime fromDate = DateTime.Parse(dateEditFromDate.Text);
            DateTime toDate = DateTime.Parse(dateEditToDate.Text);

            List<CGDataHeader> cgDataHeadersUnderBranch = new List<CGDataHeader>();
            List<CGDataHeader> cgDataHeadersUnderDate = new List<CGDataHeader>();
            List<CGDataHeader> cgDataHeadersUnderHandlingStatus = new List<CGDataHeader>();
            List<CGDataHeader> cgDataHeadersUnderOrderStatus = new List<CGDataHeader>();
            List<CGDataHeader> cgDataHeaders = new List<CGDataHeader>();
            if (branchID.Equals("<>"))
            {
                cgDataHeadersUnderBranch = await cgDataHeaderRepository.GetAll();
            }
            else
            {
                cgDataHeadersUnderBranch = await cgDataHeaderRepository.GetUnderBranch(branchID);
            }
            if (handlingStatusID.Equals("<>"))
            {
                cgDataHeadersUnderHandlingStatus = await cgDataHeaderRepository.GetAll();
            }
            else
            {
                cgDataHeadersUnderHandlingStatus = await cgDataHeaderRepository.GetUnderHandlingStatus(handlingStatusID);
            }
            if (orderStatusID.Equals("<>"))
            {
               cgDataHeadersUnderOrderStatus = await cgDataHeaderRepository.GetAll();
            }
            else
            {
                cgDataHeadersUnderOrderStatus = await cgDataHeaderRepository.GetUnderOrderStatus(orderStatusID);
            }
            cgDataHeadersUnderDate = await cgDataHeaderRepository.GetUnderDate(fromDate, toDate);

            for (int i = 0; i < cgDataHeadersUnderBranch.Count; i++)
            {
                for (int j = 0; j < cgDataHeadersUnderDate.Count; j++)
                {
                    for (int k = 0; k < cgDataHeadersUnderHandlingStatus.Count; k++)
                    {
                        for (int h = 0; h < cgDataHeadersUnderOrderStatus.Count; h++)
                        {
                            if (cgDataHeadersUnderBranch[i].CGDNumber.Equals(cgDataHeadersUnderDate[j].CGDNumber)
                            && cgDataHeadersUnderDate[j].CGDNumber.Equals(cgDataHeadersUnderHandlingStatus[k].CGDNumber)
                            && cgDataHeadersUnderHandlingStatus[k].CGDNumber.Equals(cgDataHeadersUnderOrderStatus[h].CGDNumber))
                            {
                                cgDataHeaders.Add(cgDataHeadersUnderBranch[i]);
                            }
                        }
                    }
                }
            }
            gridControlWRDataHeader.DataSource = cgDataHeaders;
        }

    }
}
