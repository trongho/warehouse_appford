﻿using DevExpress.XtraBars.Docking2010;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WarehouseManager.Models;
using WarehouseManager.Repository;
using webservice.Entities;

namespace WarehouseManager
{
    public partial class TallySheetControl : DevExpress.XtraEditors.XtraUserControl
    {
        WarehouseRepository warehouseRepository;
        HandlingStatusRepository handlingStatusRepository;
        TSHeaderRepository tSHeaderRepository;
        TSDetailRepository tSDetailRepository;
        String lastID = "";
        public static String selectedID="";
        public TallySheetControl()
        {
            InitializeComponent();
            this.Load += TallySheetControl_Load;
            warehouseRepository = new WarehouseRepository();
            handlingStatusRepository = new HandlingStatusRepository();
            tSHeaderRepository = new TSHeaderRepository();
            tSDetailRepository = new TSDetailRepository();
        }

        private void TallySheetControl_Load(Object sender,EventArgs e)
        {
            popupMenuSelectWarehouse();
            popupMenuSelectHandlingStatus();
            windowsUIButtonPanel1.ButtonClick += button_Click;
            textEditTSNumber.DoubleClick += textEdit_CellDoubleClick;
        }


        private async Task popupMenuSelectWarehouse()
        {
            List<Warehouse> warehouses = await warehouseRepository.getAll();
            foreach (Warehouse warehouse in warehouses)
            {
                comboBoxEditWarehouse.Properties.Items.Add(warehouse.WarehouseName);
            }
            comboBoxEditWarehouse.SelectedIndex = 0;
        }

        private async Task popupMenuSelectHandlingStatus()
        {
            List<HandlingStatus> handlingStatuses = await handlingStatusRepository.GetAll();
            foreach (HandlingStatus handlingStatus in handlingStatuses)
            {
                comboBoxEditHandlingStatus.Properties.Items.Add(handlingStatus.HandlingStatusName);
            }
            comboBoxEditHandlingStatus.SelectedIndex = 1;
        }

        private async Task sbLoadDataForGridDataAsync(String tsNumber)
        {
            List<TSDetail> tSDetails = await tSDetailRepository.GetUnderID(tsNumber);
            int mMaxRow = 100;
            if (tSDetails.Count < mMaxRow)
            {
                int num = mMaxRow - tSDetails.Count;
                int i = 1;
                while (true)
                {
                    int num2 = i;
                    int num3 = num;
                    if (num2 > num3)
                    {
                        break;
                    }
                    tSDetails.Add(new TSDetail());
                    i++;
                }
            }
            gridControlData.DataSource = tSDetails;
            getTotalQuantity();
            getTotalVATAmount();
            getTotalAmount();
        }

        public async Task getlastIDAsync()
        {
            lastID= await tSHeaderRepository.GetLastID();
            String year = DateTime.Now.Year.ToString().Substring(2, 2);
            if (lastID == "")
            {
                textEditTSNumber.Text = year + "000" + "-" + $"{1:D5}";
            }
            else
            {
                string[] arrListStr = lastID.Split('-');
                String partOne = arrListStr[0];
                String partTwo = arrListStr[1];
                partTwo = (int.Parse(partTwo) + 1).ToString();
                textEditTSNumber.Text = year + "000" + "-" + $"{int.Parse(partTwo):D5}";
            }
        }

        private void getTotalQuantity()
        {
            Decimal total = 0;
            int i = 0;
            while (true)
            {
                int rowHandle = gridViewData.GetRowHandle(i);
                if (gridViewData.GetRowCellValue(rowHandle, "GoodsID") == null)
                {
                    break;
                }
                Decimal quantity = (Decimal)gridViewData.GetRowCellValue(rowHandle, "Quantity");
                total += quantity;
                i++;
            }
            textEditTotalQuantity.Text = total.ToString("0.#####");
        }

        private void getTotalVATAmount()
        {
            Decimal total = 0;
            int i = 0;
            while (true)
            {
                int rowHandle = gridViewData.GetRowHandle(i);
                if (gridViewData.GetRowCellValue(rowHandle, "GoodsID") == null)
                {
                    break;
                }
                Decimal vatAmount = (Decimal)gridViewData.GetRowCellValue(rowHandle, "VATAmount");
                total += vatAmount;
                i++;
            }
            textEditTotalAmountIncludedVAT.Text = total.ToString("0.#####");
        }

        private void getTotalAmount()
        {
            Decimal total = 0;
            int i = 0;
            while (true)
            {
                int rowHandle = gridViewData.GetRowHandle(i);
                if (gridViewData.GetRowCellValue(rowHandle, "GoodsID") == null)
                {
                    break;
                }
                Decimal amount = (Decimal)gridViewData.GetRowCellValue(rowHandle, "Amount");
                total += amount;
                i++;
            }
            textEditTotalAmount.Text = total.ToString("0.#####");
        }

        public async Task createTSHeaderAsync()
        {
            TSHeader tSHeader = new TSHeader();
            tSHeader.TSNumber = textEditTSNumber.Text;
            tSHeader.TSDate = DateTime.Parse(dateEditTallyDate.DateTime.ToString("yyyy-MM-dd"));
            tSHeader.ReferenceNumber = textEditReferenceNumber.Text;
            tSHeader.HandlingStatusID = (comboBoxEditHandlingStatus.SelectedIndex - 1).ToString();
            tSHeader.HandlingStatusName = comboBoxEditHandlingStatus.SelectedItem.ToString();
            tSHeader.WarehouseID = $"{int.Parse(comboBoxEditWarehouse.ToString()) - 1:D3}";
            tSHeader.WarehouseName = comboBoxEditWarehouse.SelectedItem.ToString();
            tSHeader.BranchID = WMMessage.User.BranchID;
            tSHeader.BranchName = WMMessage.User.BranchName;
            tSHeader.Note = textEditNote.Text;
            tSHeader.TotalQuantity = Decimal.Parse(textEditTotalQuantity.Text);
            tSHeader.TotalVATAmount = Decimal.Parse(textEditTotalAmountIncludedVAT.Text);
            tSHeader.TotalAmount = Decimal.Parse(textEditTotalAmount.Text);
            tSHeader.CreatedUserID = WMMessage.User.UserID;
            tSHeader.CreatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));

            if (await tSHeaderRepository.Create(tSHeader) > 0)
            {
                createTSDetailAsync();
                WMPublic.sbMessageSaveNewSuccess(this);
            }
        }

        public async Task createTSDetailAsync()
        {
            int num = gridViewData.RowCount - 1;
            int i = 0;
            while (true)
            {
                int num2 = i;
                int num3 = num;
                if (num2 > num3 ||gridViewData.GetRowCellValue(i,gridViewData.Columns["colGoodsID"]) == "")
                {
                    break;
                }
                int rowHandle = gridViewData.GetRowHandle(i);
                TSDetail tSDetail = new TSDetail();
                tSDetail.TSNumber=textEditTSNumber.Text;
                tSDetail.GoodsID = (string?)gridViewData.GetRowCellValue(rowHandle, "GoodsID");
                tSDetail.GoodsName = (string?)gridViewData.GetRowCellValue(rowHandle, "GoodsName");
                tSDetail.Ordinal = i + 1;
                tSDetail.TallyUnitID = (string?)gridViewData.GetRowCellValue(rowHandle, "TallyUnitID");
                tSDetail.UnitRate = (Decimal?)gridViewData.GetRowCellValue(rowHandle, "UnitRate");
                tSDetail.StockUnitID = (string?)gridViewData.GetRowCellValue(rowHandle, "StockUnitID");
                tSDetail.Quantity = (Decimal?)gridViewData.GetRowCellValue(rowHandle, "Quantity");
                tSDetail.Price = (Decimal?)gridViewData.GetRowCellValue(rowHandle, "Price");
                tSDetail.Amount = (Decimal?)gridViewData.GetRowCellValue(rowHandle, "Amount");
                tSDetail.ExpiryDate= (DateTime?)gridViewData.GetRowCellValue(rowHandle, "ExpiryDate");
                tSDetail.SerialNumber = (string?)gridViewData.GetRowCellValue(rowHandle, "SerialNumber");
                tSDetail.Note = textEditNote.Text;

                if (await tSDetailRepository.Create(tSDetail) > 0)
                {
                    i++;
                }
            }
        }

        public async Task updateTSHeaderAsync()
        {
            TSHeader tSHeader = new TSHeader();
            tSHeader.TSNumber = textEditTSNumber.Text;
            tSHeader.TSDate = DateTime.Parse(dateEditTallyDate.DateTime.ToString("yyyy-MM-dd"));
            tSHeader.ReferenceNumber = textEditReferenceNumber.Text;
            tSHeader.HandlingStatusID = (comboBoxEditHandlingStatus.SelectedIndex - 1).ToString();
            tSHeader.HandlingStatusName = comboBoxEditHandlingStatus.SelectedItem.ToString();
            tSHeader.WarehouseID = $"{int.Parse(comboBoxEditWarehouse.ToString()) - 1:D3}";
            tSHeader.WarehouseName = comboBoxEditWarehouse.SelectedItem.ToString();
            tSHeader.BranchID = WMMessage.User.BranchID;
            tSHeader.BranchName = WMMessage.User.BranchName;
            tSHeader.Note = textEditNote.Text;
            tSHeader.TotalQuantity = Decimal.Parse(textEditTotalQuantity.Text);
            tSHeader.TotalVATAmount = Decimal.Parse(textEditTotalAmountIncludedVAT.Text);
            tSHeader.TotalAmount = Decimal.Parse(textEditTotalAmount.Text);
            tSHeader.CreatedUserID = WMMessage.User.UserID;
            tSHeader.CreatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));

            if (await tSHeaderRepository.Update(tSHeader,textEditTSNumber.Text) > 0)
            {
                updateTSDetailAsync();
                WMPublic.sbMessageSaveChangeSuccess(this);
            }
        }

        public async Task updateTSDetailAsync()
        {
            int num = gridViewData.RowCount - 1;
            int i = 0;
            while (true)
            {
                int num2 = i;
                int num3 = num;
                if (num2 > num3 || gridViewData.GetRowCellValue(i, gridViewData.Columns["colGoodsID"]) == "")
                {
                    break;
                }
                int rowHandle = gridViewData.GetRowHandle(i);
                TSDetail tSDetail = new TSDetail();
                tSDetail.TSNumber = textEditTSNumber.Text;
                tSDetail.GoodsID = (string?)gridViewData.GetRowCellValue(rowHandle, "GoodsID");
                tSDetail.GoodsName = (string?)gridViewData.GetRowCellValue(rowHandle, "GoodsName");
                tSDetail.Ordinal = i + 1;
                tSDetail.TallyUnitID = (string?)gridViewData.GetRowCellValue(rowHandle, "TallyUnitID");
                tSDetail.UnitRate = (Decimal?)gridViewData.GetRowCellValue(rowHandle, "UnitRate");
                tSDetail.StockUnitID = (string?)gridViewData.GetRowCellValue(rowHandle, "StockUnitID");
                tSDetail.Quantity = (Decimal?)gridViewData.GetRowCellValue(rowHandle, "Quantity");
                tSDetail.Price = (Decimal?)gridViewData.GetRowCellValue(rowHandle, "Price");
                tSDetail.Amount = (Decimal?)gridViewData.GetRowCellValue(rowHandle, "Amount");
                tSDetail.ExpiryDate = (DateTime?)gridViewData.GetRowCellValue(rowHandle, "ExpiryDate");
                tSDetail.SerialNumber = (string?)gridViewData.GetRowCellValue(rowHandle, "SerialNumber");
                tSDetail.Note = textEditNote.Text;

                if (await tSDetailRepository.Update(tSDetail,tSDetail.TSNumber,tSDetail.GoodsID,tSDetail.Ordinal) > 0)
                {
                    i++;
                }
            }
        }

        public async Task DeleteTSHeaderAsync()
        {
            if ((await tSHeaderRepository.Delete(textEditTSNumber.Text)) > 0)
            {
                await DeleteTSDetailAsync();
                sbLoadDataForGridDataAsync(textEditTSNumber.Text);
                WMPublic.sbMessageDeleteSuccess(this);
            }
        }

        public async Task DeleteTSDetailAsync()
        {
            await tSHeaderRepository.Delete(textEditTSNumber.Text);
            clearAsync();
            gridControlData.DataSource = null;
        }

        private void clearAsync()
        {
            textEditTSNumber.Text = "";
            dateEditTallyDate.Text = DateTime.Now.ToString("dd-MM-yyyy");
            textEditReferenceNumber.Text = "";
            comboBoxEditHandlingStatus.SelectedIndex = 1;
            textEditNote.Text = "";
            textEditTotalQuantity.Text = "0";
            textEditTotalAmountIncludedVAT.Text = "0";
            textEditTotalAmount.Text = "0";
            List<TSDetail> tSDetails = new List<TSDetail>();

            for (int i = 0; i < 100; i++)
            {
                tSDetails.Add(new TSDetail());
            }
            gridControlData.DataSource = tSDetails;
        }

        //private async Task checkExist(String id)
        //{
        //    if (!id.Equals("") && (await tSHeaderRepository.GetUnderID(id)))
        //    {
        //        isWRRNumberExist = true;
        //    }
        //}




        private async void textEdit_CellDoubleClick(object sender, EventArgs e)
        {
            frmSearchTSH frmSearch = new frmSearchTSH();
            frmSearch.ShowDialog(this);
            frmSearch.Dispose();
            if ( selectedID!= null)
            {
                textEditTSNumber.Text = selectedID;
                TSHeader tSHeader = await tSHeaderRepository.GetUnderID(textEditTSNumber.Text);
                textEditReferenceNumber.Text =tSHeader.ReferenceNumber;
                dateEditTallyDate.Text = tSHeader.TSDate.ToString();
                textEditNote.Text = tSHeader.Note;
                comboBoxEditWarehouse.SelectedIndex=int.Parse($"{int.Parse(tSHeader.WarehouseID)+1:D3}");
                comboBoxEditHandlingStatus.SelectedIndex = int.Parse(tSHeader.HandlingStatusID) + 1;
                comboBoxEditWarehouse.SelectedIndex = int.Parse("000");
                await sbLoadDataForGridDataAsync(textEditTSNumber.Text);
            }
        }

        public async void button_Click(object sender, EventArgs e)
        {
            ButtonEventArgs eArg = (DevExpress.XtraBars.Docking2010.ButtonEventArgs)e;
            String tag = eArg.Button.Properties.Tag.ToString();
            switch (tag)
            {
                case "new":
                    break;
                case "save":
                    if (WMPublic.sbMessageSaveChangeRequest(this) == true)
                    {
                        sbLoadDataForGridDataAsync(textEditTSNumber.Text);
                    }
                    break;
                case "delete":
                    break;
                case "search":

                    break;
                case "refesh":
                    sbLoadDataForGridDataAsync(textEditTSNumber.Text);
                    break;
                case "import":
                    break;
                case "export":

                    break;
                case "print":
                    break;
                case "close":
                    break;
            }

        }
    }
}
