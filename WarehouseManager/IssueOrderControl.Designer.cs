﻿
namespace WarehouseManager
{
    partial class IssueOrderControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions1 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(IssueOrderControl));
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions2 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions3 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions4 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions5 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions6 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions7 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions8 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions9 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition1 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition2 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition3 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition4 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition5 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition6 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition7 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition1 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition2 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition3 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition4 = new DevExpress.XtraLayout.RowDefinition();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.windowsUIButtonPanel1 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.wRDataDetailBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.wARHOUSE_HPDataSet1 = new WarehouseManager.WARHOUSE_HPDataSet1();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.label1 = new System.Windows.Forms.Label();
            this.textEdit4 = new DevExpress.XtraEditors.TextEdit();
            this.textEditIONumber = new DevExpress.XtraEditors.TextEdit();
            this.comboBoxEditModality = new DevExpress.XtraEditors.ComboBoxEdit();
            this.dateEditIODate = new DevExpress.XtraEditors.DateEdit();
            this.textEditNote = new DevExpress.XtraEditors.TextEdit();
            this.comboBoxEditWarehouse = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEditOrderNumber = new DevExpress.XtraEditors.TextEdit();
            this.dateEditOrderDate = new DevExpress.XtraEditors.DateEdit();
            this.textEditCustomer = new DevExpress.XtraEditors.TextEdit();
            this.textEditContractNumber = new DevExpress.XtraEditors.TextEdit();
            this.comboBoxEditPriceType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEditPaymentType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEditHandlingStatus = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEditTotalQuantity = new DevExpress.XtraEditors.TextEdit();
            this.textEditTotalDiscountAmount = new DevExpress.XtraEditors.TextEdit();
            this.textEditTotalVATAmount = new DevExpress.XtraEditors.TextEdit();
            this.textEditTotalAmount = new DevExpress.XtraEditors.TextEdit();
            this.textEditRONumber = new DevExpress.XtraEditors.TextEdit();
            this.dateEditContractDate = new DevExpress.XtraEditors.DateEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemIONumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemModality = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemIODate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemWarehouse = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemOrderNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemOrderDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemContractNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemTotalQuantity = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemTotalDiscountAmount = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemTotalVATAmount = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemTotalAmount = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemRONumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemContractDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemPriceType = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemPaymentType = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemHandlingStatus = new DevExpress.XtraLayout.LayoutControlItem();
            this.wRDataDetailTableAdapter = new WarehouseManager.WARHOUSE_HPDataSet1TableAdapters.WRDataDetailTableAdapter();
            this.layoutControl3 = new DevExpress.XtraLayout.LayoutControl();
            this.gridControlIODetail = new DevExpress.XtraGrid.GridControl();
            this.iODetailBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.wARHOUSE_HPDataSet2 = new WarehouseManager.WARHOUSE_HPDataSet2();
            this.gridViewIODetail = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colIONumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrdinal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGoodsID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGoodsName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIssueUnitID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnitRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStockUnitID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPriceIncludedVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPriceExcludedVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDiscount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDiscountAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVATAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAmountExcludedVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAmountIncludedVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpiryDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSerialNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNote = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantityIssued = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPriceIncludedVATIssued = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPriceExcludedVATIssued = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPriceIssued = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDiscountIssued = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDiscountAmountIssued = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVATIssued = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVATAmountIssued = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAmountExcludedVATIssued = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAmountIncludedVATIssued = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAmountIssued = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantityOrdered = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrderQuantityIssued = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIssueQuantityReceived = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIssueQuantity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIssueAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAverageCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSPSNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPSNumberSalePrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPSNumberDiscount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.wRDetailBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.wRDetailBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.wRDetailTableAdapter = new WarehouseManager.WARHOUSE_HPDataSet1TableAdapters.WRDetailTableAdapter();
            this.iODetailTableAdapter = new WarehouseManager.WARHOUSE_HPDataSet2TableAdapters.IODetailTableAdapter();
            this.layoutControlItemCustomer = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemNote = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wRDataDetailBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wARHOUSE_HPDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditIONumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditModality.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditIODate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditIODate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditNote.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditWarehouse.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditOrderNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditOrderDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditOrderDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditCustomer.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditContractNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditPriceType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditPaymentType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditHandlingStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalQuantity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalDiscountAmount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalVATAmount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalAmount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditRONumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditContractDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditContractDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemIONumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemModality)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemIODate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWarehouse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemOrderNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemOrderDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemContractNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalQuantity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalDiscountAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalVATAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemRONumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemContractDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPriceType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPaymentType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemHandlingStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).BeginInit();
            this.layoutControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlIODetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iODetailBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wARHOUSE_HPDataSet2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewIODetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wRDetailBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wRDetailBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemCustomer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemNote)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.windowsUIButtonPanel1);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(684, 208, 812, 495);
            this.layoutControl1.Root = this.Root;
            this.layoutControl1.Size = new System.Drawing.Size(1298, 37);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // windowsUIButtonPanel1
            // 
            this.windowsUIButtonPanel1.AllowGlyphSkinning = false;
            this.windowsUIButtonPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            windowsUIButtonImageOptions1.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions1.Image")));
            windowsUIButtonImageOptions1.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions2.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions2.Image")));
            windowsUIButtonImageOptions2.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions3.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions3.Image")));
            windowsUIButtonImageOptions3.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions4.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions4.Image")));
            windowsUIButtonImageOptions4.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions5.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions5.Image")));
            windowsUIButtonImageOptions5.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions6.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions6.Image")));
            windowsUIButtonImageOptions6.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions7.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions7.Image")));
            windowsUIButtonImageOptions7.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions8.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions8.Image")));
            windowsUIButtonImageOptions8.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            windowsUIButtonImageOptions9.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions9.Image")));
            windowsUIButtonImageOptions9.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
            this.windowsUIButtonPanel1.Buttons.AddRange(new DevExpress.XtraEditors.ButtonPanel.IBaseButton[] {
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F2) Thêm mới", true, windowsUIButtonImageOptions1, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "new", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F3) Lưu", true, windowsUIButtonImageOptions2, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "save", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F4) Xóa", true, windowsUIButtonImageOptions3, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "delete", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F5) Tìm kiếm", true, windowsUIButtonImageOptions4, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "search", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F6) Làm mới", true, windowsUIButtonImageOptions5, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "refesh", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F7) Nhập dữ liệu", true, windowsUIButtonImageOptions6, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "import", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F8) Xuất dữ liệu", true, windowsUIButtonImageOptions7, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "export", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F9) In ấn", true, windowsUIButtonImageOptions8, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "print", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUISeparator(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("(F10) Thoát", true, windowsUIButtonImageOptions9, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "close", -1, false)});
            this.windowsUIButtonPanel1.ContentAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.windowsUIButtonPanel1.Location = new System.Drawing.Point(2, 2);
            this.windowsUIButtonPanel1.Name = "windowsUIButtonPanel1";
            this.windowsUIButtonPanel1.Padding = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.windowsUIButtonPanel1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.windowsUIButtonPanel1.Size = new System.Drawing.Size(1294, 32);
            this.windowsUIButtonPanel1.TabIndex = 6;
            this.windowsUIButtonPanel1.Text = "windowsUIButtonPanel1";
            this.windowsUIButtonPanel1.UseButtonBackgroundImages = false;
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.Root.Name = "Root";
            this.Root.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.Root.Size = new System.Drawing.Size(1298, 37);
            this.Root.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.windowsUIButtonPanel1;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(1298, 37);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // wRDataDetailBindingSource
            // 
            this.wRDataDetailBindingSource.DataMember = "WRDataDetail";
            this.wRDataDetailBindingSource.DataSource = this.wARHOUSE_HPDataSet1;
            // 
            // wARHOUSE_HPDataSet1
            // 
            this.wARHOUSE_HPDataSet1.DataSetName = "WARHOUSE_HPDataSet1";
            this.wARHOUSE_HPDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.label1);
            this.layoutControl2.Controls.Add(this.textEdit4);
            this.layoutControl2.Controls.Add(this.textEditIONumber);
            this.layoutControl2.Controls.Add(this.comboBoxEditModality);
            this.layoutControl2.Controls.Add(this.dateEditIODate);
            this.layoutControl2.Controls.Add(this.textEditNote);
            this.layoutControl2.Controls.Add(this.comboBoxEditWarehouse);
            this.layoutControl2.Controls.Add(this.textEditOrderNumber);
            this.layoutControl2.Controls.Add(this.dateEditOrderDate);
            this.layoutControl2.Controls.Add(this.textEditCustomer);
            this.layoutControl2.Controls.Add(this.textEditContractNumber);
            this.layoutControl2.Controls.Add(this.comboBoxEditPriceType);
            this.layoutControl2.Controls.Add(this.comboBoxEditPaymentType);
            this.layoutControl2.Controls.Add(this.comboBoxEditHandlingStatus);
            this.layoutControl2.Controls.Add(this.textEditTotalQuantity);
            this.layoutControl2.Controls.Add(this.textEditTotalDiscountAmount);
            this.layoutControl2.Controls.Add(this.textEditTotalVATAmount);
            this.layoutControl2.Controls.Add(this.textEditTotalAmount);
            this.layoutControl2.Controls.Add(this.textEditRONumber);
            this.layoutControl2.Controls.Add(this.dateEditContractDate);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl2.Location = new System.Drawing.Point(0, 37);
            this.layoutControl2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(480, 99, 812, 500);
            this.layoutControl2.Root = this.layoutControlGroup1;
            this.layoutControl2.Size = new System.Drawing.Size(1298, 113);
            this.layoutControl2.TabIndex = 1;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // label1
            // 
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(217, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 20);
            this.label1.TabIndex = 7;
            this.label1.Text = "(*)";
            // 
            // textEdit4
            // 
            this.textEdit4.Location = new System.Drawing.Point(2540, 10);
            this.textEdit4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textEdit4.Name = "textEdit4";
            this.textEdit4.Size = new System.Drawing.Size(53, 20);
            this.textEdit4.StyleController = this.layoutControl2;
            this.textEdit4.TabIndex = 11;
            // 
            // textEditIONumber
            // 
            this.textEditIONumber.Location = new System.Drawing.Point(114, 10);
            this.textEditIONumber.Name = "textEditIONumber";
            this.textEditIONumber.Size = new System.Drawing.Size(99, 20);
            this.textEditIONumber.StyleController = this.layoutControl2;
            this.textEditIONumber.TabIndex = 12;
            // 
            // comboBoxEditModality
            // 
            this.comboBoxEditModality.Location = new System.Drawing.Point(114, 34);
            this.comboBoxEditModality.Name = "comboBoxEditModality";
            this.comboBoxEditModality.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditModality.Size = new System.Drawing.Size(99, 20);
            this.comboBoxEditModality.StyleController = this.layoutControl2;
            this.comboBoxEditModality.TabIndex = 13;
            // 
            // dateEditIODate
            // 
            this.dateEditIODate.EditValue = null;
            this.dateEditIODate.Location = new System.Drawing.Point(114, 58);
            this.dateEditIODate.Name = "dateEditIODate";
            this.dateEditIODate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditIODate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditIODate.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditIODate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditIODate.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEditIODate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditIODate.Size = new System.Drawing.Size(99, 20);
            this.dateEditIODate.StyleController = this.layoutControl2;
            this.dateEditIODate.TabIndex = 14;
            // 
            // textEditNote
            // 
            this.textEditNote.Location = new System.Drawing.Point(114, 82);
            this.textEditNote.Name = "textEditNote";
            this.textEditNote.Size = new System.Drawing.Size(964, 20);
            this.textEditNote.StyleController = this.layoutControl2;
            this.textEditNote.TabIndex = 16;
            // 
            // comboBoxEditWarehouse
            // 
            this.comboBoxEditWarehouse.Location = new System.Drawing.Point(361, 10);
            this.comboBoxEditWarehouse.Name = "comboBoxEditWarehouse";
            this.comboBoxEditWarehouse.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditWarehouse.Size = new System.Drawing.Size(305, 20);
            this.comboBoxEditWarehouse.StyleController = this.layoutControl2;
            this.comboBoxEditWarehouse.TabIndex = 17;
            // 
            // textEditOrderNumber
            // 
            this.textEditOrderNumber.Location = new System.Drawing.Point(361, 34);
            this.textEditOrderNumber.Name = "textEditOrderNumber";
            this.textEditOrderNumber.Size = new System.Drawing.Size(99, 20);
            this.textEditOrderNumber.StyleController = this.layoutControl2;
            this.textEditOrderNumber.TabIndex = 18;
            // 
            // dateEditOrderDate
            // 
            this.dateEditOrderDate.EditValue = null;
            this.dateEditOrderDate.Location = new System.Drawing.Point(361, 58);
            this.dateEditOrderDate.Name = "dateEditOrderDate";
            this.dateEditOrderDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditOrderDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditOrderDate.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditOrderDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditOrderDate.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEditOrderDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditOrderDate.Size = new System.Drawing.Size(99, 20);
            this.dateEditOrderDate.StyleController = this.layoutControl2;
            this.dateEditOrderDate.TabIndex = 19;
            // 
            // textEditCustomer
            // 
            this.textEditCustomer.Location = new System.Drawing.Point(567, 34);
            this.textEditCustomer.Name = "textEditCustomer";
            this.textEditCustomer.Size = new System.Drawing.Size(99, 20);
            this.textEditCustomer.StyleController = this.layoutControl2;
            this.textEditCustomer.TabIndex = 20;
            // 
            // textEditContractNumber
            // 
            this.textEditContractNumber.Location = new System.Drawing.Point(773, 34);
            this.textEditContractNumber.Name = "textEditContractNumber";
            this.textEditContractNumber.Size = new System.Drawing.Size(99, 20);
            this.textEditContractNumber.StyleController = this.layoutControl2;
            this.textEditContractNumber.TabIndex = 21;
            // 
            // comboBoxEditPriceType
            // 
            this.comboBoxEditPriceType.Location = new System.Drawing.Point(979, 10);
            this.comboBoxEditPriceType.Name = "comboBoxEditPriceType";
            this.comboBoxEditPriceType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditPriceType.Size = new System.Drawing.Size(99, 20);
            this.comboBoxEditPriceType.StyleController = this.layoutControl2;
            this.comboBoxEditPriceType.TabIndex = 23;
            // 
            // comboBoxEditPaymentType
            // 
            this.comboBoxEditPaymentType.Location = new System.Drawing.Point(979, 34);
            this.comboBoxEditPaymentType.Name = "comboBoxEditPaymentType";
            this.comboBoxEditPaymentType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditPaymentType.Size = new System.Drawing.Size(99, 20);
            this.comboBoxEditPaymentType.StyleController = this.layoutControl2;
            this.comboBoxEditPaymentType.TabIndex = 24;
            // 
            // comboBoxEditHandlingStatus
            // 
            this.comboBoxEditHandlingStatus.Location = new System.Drawing.Point(979, 58);
            this.comboBoxEditHandlingStatus.Name = "comboBoxEditHandlingStatus";
            this.comboBoxEditHandlingStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditHandlingStatus.Size = new System.Drawing.Size(99, 20);
            this.comboBoxEditHandlingStatus.StyleController = this.layoutControl2;
            this.comboBoxEditHandlingStatus.TabIndex = 25;
            // 
            // textEditTotalQuantity
            // 
            this.textEditTotalQuantity.Location = new System.Drawing.Point(1185, 10);
            this.textEditTotalQuantity.Name = "textEditTotalQuantity";
            this.textEditTotalQuantity.Size = new System.Drawing.Size(102, 20);
            this.textEditTotalQuantity.StyleController = this.layoutControl2;
            this.textEditTotalQuantity.TabIndex = 27;
            // 
            // textEditTotalDiscountAmount
            // 
            this.textEditTotalDiscountAmount.Location = new System.Drawing.Point(1185, 34);
            this.textEditTotalDiscountAmount.Name = "textEditTotalDiscountAmount";
            this.textEditTotalDiscountAmount.Size = new System.Drawing.Size(102, 20);
            this.textEditTotalDiscountAmount.StyleController = this.layoutControl2;
            this.textEditTotalDiscountAmount.TabIndex = 28;
            // 
            // textEditTotalVATAmount
            // 
            this.textEditTotalVATAmount.Location = new System.Drawing.Point(1185, 58);
            this.textEditTotalVATAmount.Name = "textEditTotalVATAmount";
            this.textEditTotalVATAmount.Size = new System.Drawing.Size(102, 20);
            this.textEditTotalVATAmount.StyleController = this.layoutControl2;
            this.textEditTotalVATAmount.TabIndex = 29;
            // 
            // textEditTotalAmount
            // 
            this.textEditTotalAmount.Location = new System.Drawing.Point(1185, 82);
            this.textEditTotalAmount.Name = "textEditTotalAmount";
            this.textEditTotalAmount.Size = new System.Drawing.Size(102, 20);
            this.textEditTotalAmount.StyleController = this.layoutControl2;
            this.textEditTotalAmount.TabIndex = 30;
            // 
            // textEditRONumber
            // 
            this.textEditRONumber.Location = new System.Drawing.Point(773, 10);
            this.textEditRONumber.Name = "textEditRONumber";
            this.textEditRONumber.Size = new System.Drawing.Size(99, 20);
            this.textEditRONumber.StyleController = this.layoutControl2;
            this.textEditRONumber.TabIndex = 32;
            // 
            // dateEditContractDate
            // 
            this.dateEditContractDate.EditValue = null;
            this.dateEditContractDate.Location = new System.Drawing.Point(773, 58);
            this.dateEditContractDate.Name = "dateEditContractDate";
            this.dateEditContractDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditContractDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditContractDate.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditContractDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditContractDate.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEditContractDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditContractDate.Size = new System.Drawing.Size(99, 20);
            this.dateEditContractDate.StyleController = this.layoutControl2;
            this.dateEditContractDate.TabIndex = 33;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3,
            this.layoutControlItemIONumber,
            this.layoutControlItemModality,
            this.layoutControlItemIODate,
            this.layoutControlItemWarehouse,
            this.layoutControlItemOrderNumber,
            this.layoutControlItemOrderDate,
            this.layoutControlItemContractNumber,
            this.layoutControlItemTotalQuantity,
            this.layoutControlItemTotalDiscountAmount,
            this.layoutControlItemTotalVATAmount,
            this.layoutControlItemTotalAmount,
            this.layoutControlItemRONumber,
            this.layoutControlItemContractDate,
            this.layoutControlItemPriceType,
            this.layoutControlItemPaymentType,
            this.layoutControlItemHandlingStatus,
            this.layoutControlItemCustomer,
            this.layoutControlItemNote});
            this.layoutControlGroup1.LayoutMode = DevExpress.XtraLayout.Utils.LayoutMode.Table;
            this.layoutControlGroup1.Name = "Root";
            columnDefinition1.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition1.Width = 16.129032258064516D;
            columnDefinition2.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition2.Width = 3.225806451612903D;
            columnDefinition3.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition3.Width = 16.129032258064516D;
            columnDefinition4.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition4.Width = 16.129032258064516D;
            columnDefinition5.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition5.Width = 16.129032258064516D;
            columnDefinition6.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition6.Width = 16.129032258064516D;
            columnDefinition7.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition7.Width = 16.129032258064516D;
            this.layoutControlGroup1.OptionsTableLayoutGroup.ColumnDefinitions.AddRange(new DevExpress.XtraLayout.ColumnDefinition[] {
            columnDefinition1,
            columnDefinition2,
            columnDefinition3,
            columnDefinition4,
            columnDefinition5,
            columnDefinition6,
            columnDefinition7});
            rowDefinition1.Height = 25D;
            rowDefinition1.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition2.Height = 25D;
            rowDefinition2.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition3.Height = 25D;
            rowDefinition3.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition4.Height = 25D;
            rowDefinition4.SizeType = System.Windows.Forms.SizeType.Percent;
            this.layoutControlGroup1.OptionsTableLayoutGroup.RowDefinitions.AddRange(new DevExpress.XtraLayout.RowDefinition[] {
            rowDefinition1,
            rowDefinition2,
            rowDefinition3,
            rowDefinition4});
            this.layoutControlGroup1.Size = new System.Drawing.Size(1298, 113);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.label1;
            this.layoutControlItem3.Location = new System.Drawing.Point(206, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.OptionsTableLayoutItem.ColumnIndex = 1;
            this.layoutControlItem3.Size = new System.Drawing.Size(41, 24);
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItemIONumber
            // 
            this.layoutControlItemIONumber.Control = this.textEditIONumber;
            this.layoutControlItemIONumber.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemIONumber.Name = "layoutControlItemIONumber";
            this.layoutControlItemIONumber.Size = new System.Drawing.Size(206, 24);
            this.layoutControlItemIONumber.Text = "Conveyance Number";
            this.layoutControlItemIONumber.TextSize = new System.Drawing.Size(100, 13);
            // 
            // layoutControlItemModality
            // 
            this.layoutControlItemModality.Control = this.comboBoxEditModality;
            this.layoutControlItemModality.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItemModality.Name = "layoutControlItemModality";
            this.layoutControlItemModality.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItemModality.Size = new System.Drawing.Size(206, 24);
            this.layoutControlItemModality.Text = "Phương thức";
            this.layoutControlItemModality.TextSize = new System.Drawing.Size(100, 13);
            // 
            // layoutControlItemIODate
            // 
            this.layoutControlItemIODate.Control = this.dateEditIODate;
            this.layoutControlItemIODate.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItemIODate.Name = "layoutControlItemIODate";
            this.layoutControlItemIODate.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlItemIODate.Size = new System.Drawing.Size(206, 24);
            this.layoutControlItemIODate.Text = "Ngày phiếu xuất";
            this.layoutControlItemIODate.TextSize = new System.Drawing.Size(100, 13);
            // 
            // layoutControlItemWarehouse
            // 
            this.layoutControlItemWarehouse.Control = this.comboBoxEditWarehouse;
            this.layoutControlItemWarehouse.Location = new System.Drawing.Point(247, 0);
            this.layoutControlItemWarehouse.Name = "layoutControlItemWarehouse";
            this.layoutControlItemWarehouse.OptionsTableLayoutItem.ColumnIndex = 2;
            this.layoutControlItemWarehouse.OptionsTableLayoutItem.ColumnSpan = 2;
            this.layoutControlItemWarehouse.Size = new System.Drawing.Size(412, 24);
            this.layoutControlItemWarehouse.Text = "Kho xuất hàng";
            this.layoutControlItemWarehouse.TextSize = new System.Drawing.Size(100, 13);
            // 
            // layoutControlItemOrderNumber
            // 
            this.layoutControlItemOrderNumber.Control = this.textEditOrderNumber;
            this.layoutControlItemOrderNumber.Location = new System.Drawing.Point(247, 24);
            this.layoutControlItemOrderNumber.Name = "layoutControlItemOrderNumber";
            this.layoutControlItemOrderNumber.OptionsTableLayoutItem.ColumnIndex = 2;
            this.layoutControlItemOrderNumber.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItemOrderNumber.Size = new System.Drawing.Size(206, 24);
            this.layoutControlItemOrderNumber.Text = "Số đơn hàng";
            this.layoutControlItemOrderNumber.TextSize = new System.Drawing.Size(100, 13);
            // 
            // layoutControlItemOrderDate
            // 
            this.layoutControlItemOrderDate.Control = this.dateEditOrderDate;
            this.layoutControlItemOrderDate.Location = new System.Drawing.Point(247, 48);
            this.layoutControlItemOrderDate.Name = "layoutControlItemOrderDate";
            this.layoutControlItemOrderDate.OptionsTableLayoutItem.ColumnIndex = 2;
            this.layoutControlItemOrderDate.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlItemOrderDate.Size = new System.Drawing.Size(206, 24);
            this.layoutControlItemOrderDate.Text = "Ngày đơn hàng";
            this.layoutControlItemOrderDate.TextSize = new System.Drawing.Size(100, 13);
            // 
            // layoutControlItemContractNumber
            // 
            this.layoutControlItemContractNumber.Control = this.textEditContractNumber;
            this.layoutControlItemContractNumber.Location = new System.Drawing.Point(659, 24);
            this.layoutControlItemContractNumber.Name = "layoutControlItemContractNumber";
            this.layoutControlItemContractNumber.OptionsTableLayoutItem.ColumnIndex = 4;
            this.layoutControlItemContractNumber.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItemContractNumber.Size = new System.Drawing.Size(206, 24);
            this.layoutControlItemContractNumber.Text = "Số hợp đồng";
            this.layoutControlItemContractNumber.TextSize = new System.Drawing.Size(100, 13);
            // 
            // layoutControlItemTotalQuantity
            // 
            this.layoutControlItemTotalQuantity.Control = this.textEditTotalQuantity;
            this.layoutControlItemTotalQuantity.Location = new System.Drawing.Point(1071, 0);
            this.layoutControlItemTotalQuantity.Name = "layoutControlItemTotalQuantity";
            this.layoutControlItemTotalQuantity.OptionsTableLayoutItem.ColumnIndex = 6;
            this.layoutControlItemTotalQuantity.Size = new System.Drawing.Size(209, 24);
            this.layoutControlItemTotalQuantity.Text = "Tổng lượng";
            this.layoutControlItemTotalQuantity.TextSize = new System.Drawing.Size(100, 13);
            // 
            // layoutControlItemTotalDiscountAmount
            // 
            this.layoutControlItemTotalDiscountAmount.Control = this.textEditTotalDiscountAmount;
            this.layoutControlItemTotalDiscountAmount.Location = new System.Drawing.Point(1071, 24);
            this.layoutControlItemTotalDiscountAmount.Name = "layoutControlItemTotalDiscountAmount";
            this.layoutControlItemTotalDiscountAmount.OptionsTableLayoutItem.ColumnIndex = 6;
            this.layoutControlItemTotalDiscountAmount.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItemTotalDiscountAmount.Size = new System.Drawing.Size(209, 24);
            this.layoutControlItemTotalDiscountAmount.Text = "Tổng chiết khấu";
            this.layoutControlItemTotalDiscountAmount.TextSize = new System.Drawing.Size(100, 13);
            // 
            // layoutControlItemTotalVATAmount
            // 
            this.layoutControlItemTotalVATAmount.Control = this.textEditTotalVATAmount;
            this.layoutControlItemTotalVATAmount.Location = new System.Drawing.Point(1071, 48);
            this.layoutControlItemTotalVATAmount.Name = "layoutControlItemTotalVATAmount";
            this.layoutControlItemTotalVATAmount.OptionsTableLayoutItem.ColumnIndex = 6;
            this.layoutControlItemTotalVATAmount.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlItemTotalVATAmount.Size = new System.Drawing.Size(209, 24);
            this.layoutControlItemTotalVATAmount.Text = "Tổng thuế";
            this.layoutControlItemTotalVATAmount.TextSize = new System.Drawing.Size(100, 13);
            // 
            // layoutControlItemTotalAmount
            // 
            this.layoutControlItemTotalAmount.Control = this.textEditTotalAmount;
            this.layoutControlItemTotalAmount.Location = new System.Drawing.Point(1071, 72);
            this.layoutControlItemTotalAmount.Name = "layoutControlItemTotalAmount";
            this.layoutControlItemTotalAmount.OptionsTableLayoutItem.ColumnIndex = 6;
            this.layoutControlItemTotalAmount.OptionsTableLayoutItem.RowIndex = 3;
            this.layoutControlItemTotalAmount.Size = new System.Drawing.Size(209, 25);
            this.layoutControlItemTotalAmount.Text = "Tổng tiền";
            this.layoutControlItemTotalAmount.TextSize = new System.Drawing.Size(100, 13);
            // 
            // layoutControlItemRONumber
            // 
            this.layoutControlItemRONumber.Control = this.textEditRONumber;
            this.layoutControlItemRONumber.Location = new System.Drawing.Point(659, 0);
            this.layoutControlItemRONumber.Name = "layoutControlItemRONumber";
            this.layoutControlItemRONumber.OptionsTableLayoutItem.ColumnIndex = 4;
            this.layoutControlItemRONumber.Size = new System.Drawing.Size(206, 24);
            this.layoutControlItemRONumber.Text = "Số phiếu nhận";
            this.layoutControlItemRONumber.TextSize = new System.Drawing.Size(100, 13);
            // 
            // layoutControlItemContractDate
            // 
            this.layoutControlItemContractDate.Control = this.dateEditContractDate;
            this.layoutControlItemContractDate.Location = new System.Drawing.Point(659, 48);
            this.layoutControlItemContractDate.Name = "layoutControlItemContractDate";
            this.layoutControlItemContractDate.OptionsTableLayoutItem.ColumnIndex = 4;
            this.layoutControlItemContractDate.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlItemContractDate.Size = new System.Drawing.Size(206, 24);
            this.layoutControlItemContractDate.Text = "Ngày hợp đồng";
            this.layoutControlItemContractDate.TextSize = new System.Drawing.Size(100, 13);
            // 
            // layoutControlItemPriceType
            // 
            this.layoutControlItemPriceType.Control = this.comboBoxEditPriceType;
            this.layoutControlItemPriceType.Location = new System.Drawing.Point(865, 0);
            this.layoutControlItemPriceType.Name = "layoutControlItemPriceType";
            this.layoutControlItemPriceType.OptionsTableLayoutItem.ColumnIndex = 5;
            this.layoutControlItemPriceType.Size = new System.Drawing.Size(206, 24);
            this.layoutControlItemPriceType.Text = "Loại giá";
            this.layoutControlItemPriceType.TextSize = new System.Drawing.Size(100, 13);
            // 
            // layoutControlItemPaymentType
            // 
            this.layoutControlItemPaymentType.Control = this.comboBoxEditPaymentType;
            this.layoutControlItemPaymentType.Location = new System.Drawing.Point(865, 24);
            this.layoutControlItemPaymentType.Name = "layoutControlItemPaymentType";
            this.layoutControlItemPaymentType.OptionsTableLayoutItem.ColumnIndex = 5;
            this.layoutControlItemPaymentType.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItemPaymentType.Size = new System.Drawing.Size(206, 24);
            this.layoutControlItemPaymentType.Text = "Loại thanh toán";
            this.layoutControlItemPaymentType.TextSize = new System.Drawing.Size(100, 13);
            // 
            // layoutControlItemHandlingStatus
            // 
            this.layoutControlItemHandlingStatus.Control = this.comboBoxEditHandlingStatus;
            this.layoutControlItemHandlingStatus.Location = new System.Drawing.Point(865, 48);
            this.layoutControlItemHandlingStatus.Name = "layoutControlItemHandlingStatus";
            this.layoutControlItemHandlingStatus.OptionsTableLayoutItem.ColumnIndex = 5;
            this.layoutControlItemHandlingStatus.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlItemHandlingStatus.Size = new System.Drawing.Size(206, 24);
            this.layoutControlItemHandlingStatus.Text = "Trạng thái xử lý";
            this.layoutControlItemHandlingStatus.TextSize = new System.Drawing.Size(100, 13);
            // 
            // wRDataDetailTableAdapter
            // 
            this.wRDataDetailTableAdapter.ClearBeforeFill = true;
            // 
            // layoutControl3
            // 
            this.layoutControl3.Controls.Add(this.gridControlIODetail);
            this.layoutControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl3.Location = new System.Drawing.Point(0, 150);
            this.layoutControl3.Name = "layoutControl3";
            this.layoutControl3.Root = this.layoutControlGroup2;
            this.layoutControl3.Size = new System.Drawing.Size(1298, 476);
            this.layoutControl3.TabIndex = 2;
            this.layoutControl3.Text = "layoutControl3";
            // 
            // gridControlIODetail
            // 
            this.gridControlIODetail.DataSource = this.iODetailBindingSource;
            this.gridControlIODetail.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gridControlIODetail.Location = new System.Drawing.Point(11, 10);
            this.gridControlIODetail.MainView = this.gridViewIODetail;
            this.gridControlIODetail.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gridControlIODetail.Name = "gridControlIODetail";
            this.gridControlIODetail.Size = new System.Drawing.Size(1276, 456);
            this.gridControlIODetail.TabIndex = 5;
            this.gridControlIODetail.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewIODetail});
            // 
            // iODetailBindingSource
            // 
            this.iODetailBindingSource.DataMember = "IODetail";
            this.iODetailBindingSource.DataSource = this.wARHOUSE_HPDataSet2;
            // 
            // wARHOUSE_HPDataSet2
            // 
            this.wARHOUSE_HPDataSet2.DataSetName = "WARHOUSE_HPDataSet2";
            this.wARHOUSE_HPDataSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridViewIODetail
            // 
            this.gridViewIODetail.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridViewIODetail.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridViewIODetail.Appearance.HeaderPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.gridViewIODetail.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colIONumber,
            this.colOrdinal,
            this.colGoodsID,
            this.colGoodsName,
            this.colIssueUnitID,
            this.colUnitRate,
            this.colStockUnitID,
            this.colQuantity,
            this.colPriceIncludedVAT,
            this.colPriceExcludedVAT,
            this.colPrice,
            this.colDiscount,
            this.colDiscountAmount,
            this.colVAT,
            this.colVATAmount,
            this.colAmountExcludedVAT,
            this.colAmountIncludedVAT,
            this.colAmount,
            this.colExpiryDate,
            this.colSerialNumber,
            this.colNote,
            this.colQuantityIssued,
            this.colPriceIncludedVATIssued,
            this.colPriceExcludedVATIssued,
            this.colPriceIssued,
            this.colDiscountIssued,
            this.colDiscountAmountIssued,
            this.colVATIssued,
            this.colVATAmountIssued,
            this.colAmountExcludedVATIssued,
            this.colAmountIncludedVATIssued,
            this.colAmountIssued,
            this.colQuantityOrdered,
            this.colOrderQuantityIssued,
            this.colIssueQuantityReceived,
            this.colIssueQuantity,
            this.colIssueAmount,
            this.colAverageCost,
            this.colSPSNumber,
            this.colPSNumberSalePrice,
            this.colPSNumberDiscount,
            this.colStatus,
            this.colNo});
            this.gridViewIODetail.DetailHeight = 284;
            this.gridViewIODetail.GridControl = this.gridControlIODetail;
            this.gridViewIODetail.Name = "gridViewIODetail";
            this.gridViewIODetail.OptionsView.ColumnHeaderAutoHeight = DevExpress.Utils.DefaultBoolean.True;
            // 
            // colIONumber
            // 
            this.colIONumber.FieldName = "IONumber";
            this.colIONumber.Name = "colIONumber";
            this.colIONumber.Width = 64;
            // 
            // colOrdinal
            // 
            this.colOrdinal.FieldName = "Ordinal";
            this.colOrdinal.Name = "colOrdinal";
            this.colOrdinal.Width = 64;
            // 
            // colGoodsID
            // 
            this.colGoodsID.Caption = "Part Number";
            this.colGoodsID.FieldName = "GoodsID";
            this.colGoodsID.MinWidth = 150;
            this.colGoodsID.Name = "colGoodsID";
            this.colGoodsID.Visible = true;
            this.colGoodsID.VisibleIndex = 1;
            this.colGoodsID.Width = 150;
            // 
            // colGoodsName
            // 
            this.colGoodsName.Caption = "Part Name";
            this.colGoodsName.FieldName = "GoodsName";
            this.colGoodsName.MinWidth = 214;
            this.colGoodsName.Name = "colGoodsName";
            this.colGoodsName.Visible = true;
            this.colGoodsName.VisibleIndex = 2;
            this.colGoodsName.Width = 214;
            // 
            // colIssueUnitID
            // 
            this.colIssueUnitID.Caption = "Đơn vị xuất";
            this.colIssueUnitID.FieldName = "IssueUnitID";
            this.colIssueUnitID.MinWidth = 50;
            this.colIssueUnitID.Name = "colIssueUnitID";
            this.colIssueUnitID.Visible = true;
            this.colIssueUnitID.VisibleIndex = 3;
            this.colIssueUnitID.Width = 80;
            // 
            // colUnitRate
            // 
            this.colUnitRate.Caption = "Tỉ lệ đơn vị";
            this.colUnitRate.FieldName = "UnitRate";
            this.colUnitRate.MinWidth = 80;
            this.colUnitRate.Name = "colUnitRate";
            this.colUnitRate.Visible = true;
            this.colUnitRate.VisibleIndex = 4;
            this.colUnitRate.Width = 80;
            // 
            // colStockUnitID
            // 
            this.colStockUnitID.Caption = "Đơn vị kho";
            this.colStockUnitID.FieldName = "StockUnitID";
            this.colStockUnitID.MinWidth = 80;
            this.colStockUnitID.Name = "colStockUnitID";
            this.colStockUnitID.Visible = true;
            this.colStockUnitID.VisibleIndex = 5;
            this.colStockUnitID.Width = 80;
            // 
            // colQuantity
            // 
            this.colQuantity.Caption = "Lượng xuất";
            this.colQuantity.FieldName = "Quantity";
            this.colQuantity.MinWidth = 80;
            this.colQuantity.Name = "colQuantity";
            this.colQuantity.Width = 80;
            // 
            // colPriceIncludedVAT
            // 
            this.colPriceIncludedVAT.FieldName = "PriceIncludedVAT";
            this.colPriceIncludedVAT.Name = "colPriceIncludedVAT";
            this.colPriceIncludedVAT.Width = 64;
            // 
            // colPriceExcludedVAT
            // 
            this.colPriceExcludedVAT.FieldName = "PriceExcludedVAT";
            this.colPriceExcludedVAT.Name = "colPriceExcludedVAT";
            this.colPriceExcludedVAT.Width = 64;
            // 
            // colPrice
            // 
            this.colPrice.Caption = "Đơn giá";
            this.colPrice.FieldName = "Price";
            this.colPrice.MinWidth = 80;
            this.colPrice.Name = "colPrice";
            this.colPrice.Visible = true;
            this.colPrice.VisibleIndex = 7;
            this.colPrice.Width = 80;
            // 
            // colDiscount
            // 
            this.colDiscount.Caption = "% Chiết khấu";
            this.colDiscount.FieldName = "Discount";
            this.colDiscount.MinWidth = 80;
            this.colDiscount.Name = "colDiscount";
            this.colDiscount.Visible = true;
            this.colDiscount.VisibleIndex = 8;
            this.colDiscount.Width = 80;
            // 
            // colDiscountAmount
            // 
            this.colDiscountAmount.Caption = "Tiền chiết khấu";
            this.colDiscountAmount.FieldName = "DiscountAmount";
            this.colDiscountAmount.MinWidth = 80;
            this.colDiscountAmount.Name = "colDiscountAmount";
            this.colDiscountAmount.Visible = true;
            this.colDiscountAmount.VisibleIndex = 9;
            this.colDiscountAmount.Width = 80;
            // 
            // colVAT
            // 
            this.colVAT.Caption = "% Thuế GTGT";
            this.colVAT.FieldName = "VAT";
            this.colVAT.MinWidth = 80;
            this.colVAT.Name = "colVAT";
            this.colVAT.Visible = true;
            this.colVAT.VisibleIndex = 10;
            this.colVAT.Width = 80;
            // 
            // colVATAmount
            // 
            this.colVATAmount.Caption = "Tiền thuế GTGT";
            this.colVATAmount.FieldName = "VATAmount";
            this.colVATAmount.MinWidth = 80;
            this.colVATAmount.Name = "colVATAmount";
            this.colVATAmount.Visible = true;
            this.colVATAmount.VisibleIndex = 11;
            this.colVATAmount.Width = 80;
            // 
            // colAmountExcludedVAT
            // 
            this.colAmountExcludedVAT.FieldName = "AmountExcludedVAT";
            this.colAmountExcludedVAT.Name = "colAmountExcludedVAT";
            this.colAmountExcludedVAT.Width = 64;
            // 
            // colAmountIncludedVAT
            // 
            this.colAmountIncludedVAT.FieldName = "AmountIncludedVAT";
            this.colAmountIncludedVAT.Name = "colAmountIncludedVAT";
            this.colAmountIncludedVAT.Width = 64;
            // 
            // colAmount
            // 
            this.colAmount.Caption = "Thành tiền";
            this.colAmount.FieldName = "Amount";
            this.colAmount.MinWidth = 80;
            this.colAmount.Name = "colAmount";
            this.colAmount.Visible = true;
            this.colAmount.VisibleIndex = 12;
            this.colAmount.Width = 80;
            // 
            // colExpiryDate
            // 
            this.colExpiryDate.Caption = "Hạn sử dụng";
            this.colExpiryDate.FieldName = "ExpiryDate";
            this.colExpiryDate.MinWidth = 80;
            this.colExpiryDate.Name = "colExpiryDate";
            this.colExpiryDate.Visible = true;
            this.colExpiryDate.VisibleIndex = 13;
            this.colExpiryDate.Width = 80;
            // 
            // colSerialNumber
            // 
            this.colSerialNumber.Caption = "Số Serial";
            this.colSerialNumber.FieldName = "SerialNumber";
            this.colSerialNumber.MinWidth = 80;
            this.colSerialNumber.Name = "colSerialNumber";
            this.colSerialNumber.Visible = true;
            this.colSerialNumber.VisibleIndex = 14;
            this.colSerialNumber.Width = 80;
            // 
            // colNote
            // 
            this.colNote.Caption = "Ghi chú";
            this.colNote.FieldName = "Note";
            this.colNote.MinWidth = 150;
            this.colNote.Name = "colNote";
            this.colNote.Visible = true;
            this.colNote.VisibleIndex = 15;
            this.colNote.Width = 150;
            // 
            // colQuantityIssued
            // 
            this.colQuantityIssued.Caption = "Lượng đã xuất";
            this.colQuantityIssued.FieldName = "QuantityIssued";
            this.colQuantityIssued.MinWidth = 80;
            this.colQuantityIssued.Name = "colQuantityIssued";
            this.colQuantityIssued.Visible = true;
            this.colQuantityIssued.VisibleIndex = 17;
            this.colQuantityIssued.Width = 80;
            // 
            // colPriceIncludedVATIssued
            // 
            this.colPriceIncludedVATIssued.FieldName = "PriceIncludedVATIssued";
            this.colPriceIncludedVATIssued.Name = "colPriceIncludedVATIssued";
            this.colPriceIncludedVATIssued.Width = 64;
            // 
            // colPriceExcludedVATIssued
            // 
            this.colPriceExcludedVATIssued.FieldName = "PriceExcludedVATIssued";
            this.colPriceExcludedVATIssued.Name = "colPriceExcludedVATIssued";
            this.colPriceExcludedVATIssued.Width = 64;
            // 
            // colPriceIssued
            // 
            this.colPriceIssued.FieldName = "PriceIssued";
            this.colPriceIssued.Name = "colPriceIssued";
            this.colPriceIssued.Width = 64;
            // 
            // colDiscountIssued
            // 
            this.colDiscountIssued.FieldName = "DiscountIssued";
            this.colDiscountIssued.Name = "colDiscountIssued";
            this.colDiscountIssued.Width = 64;
            // 
            // colDiscountAmountIssued
            // 
            this.colDiscountAmountIssued.FieldName = "DiscountAmountIssued";
            this.colDiscountAmountIssued.Name = "colDiscountAmountIssued";
            this.colDiscountAmountIssued.Width = 64;
            // 
            // colVATIssued
            // 
            this.colVATIssued.FieldName = "VATIssued";
            this.colVATIssued.Name = "colVATIssued";
            this.colVATIssued.Width = 64;
            // 
            // colVATAmountIssued
            // 
            this.colVATAmountIssued.FieldName = "VATAmountIssued";
            this.colVATAmountIssued.Name = "colVATAmountIssued";
            this.colVATAmountIssued.Width = 64;
            // 
            // colAmountExcludedVATIssued
            // 
            this.colAmountExcludedVATIssued.FieldName = "AmountExcludedVATIssued";
            this.colAmountExcludedVATIssued.Name = "colAmountExcludedVATIssued";
            this.colAmountExcludedVATIssued.Width = 64;
            // 
            // colAmountIncludedVATIssued
            // 
            this.colAmountIncludedVATIssued.FieldName = "AmountIncludedVATIssued";
            this.colAmountIncludedVATIssued.Name = "colAmountIncludedVATIssued";
            this.colAmountIncludedVATIssued.Width = 64;
            // 
            // colAmountIssued
            // 
            this.colAmountIssued.FieldName = "AmountIssued";
            this.colAmountIssued.Name = "colAmountIssued";
            this.colAmountIssued.Width = 64;
            // 
            // colQuantityOrdered
            // 
            this.colQuantityOrdered.Caption = "Lượng yêu cầu";
            this.colQuantityOrdered.FieldName = "QuantityOrdered";
            this.colQuantityOrdered.MinWidth = 120;
            this.colQuantityOrdered.Name = "colQuantityOrdered";
            this.colQuantityOrdered.Visible = true;
            this.colQuantityOrdered.VisibleIndex = 16;
            this.colQuantityOrdered.Width = 120;
            // 
            // colOrderQuantityIssued
            // 
            this.colOrderQuantityIssued.FieldName = "OrderQuantityIssued";
            this.colOrderQuantityIssued.Name = "colOrderQuantityIssued";
            this.colOrderQuantityIssued.Width = 64;
            // 
            // colIssueQuantityReceived
            // 
            this.colIssueQuantityReceived.FieldName = "IssueQuantityReceived";
            this.colIssueQuantityReceived.Name = "colIssueQuantityReceived";
            this.colIssueQuantityReceived.Width = 64;
            // 
            // colIssueQuantity
            // 
            this.colIssueQuantity.Caption = "Lượng xuất";
            this.colIssueQuantity.FieldName = "IssueQuantity";
            this.colIssueQuantity.MinWidth = 120;
            this.colIssueQuantity.Name = "colIssueQuantity";
            this.colIssueQuantity.Visible = true;
            this.colIssueQuantity.VisibleIndex = 6;
            this.colIssueQuantity.Width = 120;
            // 
            // colIssueAmount
            // 
            this.colIssueAmount.FieldName = "IssueAmount";
            this.colIssueAmount.Name = "colIssueAmount";
            this.colIssueAmount.Width = 64;
            // 
            // colAverageCost
            // 
            this.colAverageCost.FieldName = "AverageCost";
            this.colAverageCost.Name = "colAverageCost";
            this.colAverageCost.Width = 64;
            // 
            // colSPSNumber
            // 
            this.colSPSNumber.FieldName = "SPSNumber";
            this.colSPSNumber.Name = "colSPSNumber";
            this.colSPSNumber.Width = 64;
            // 
            // colPSNumberSalePrice
            // 
            this.colPSNumberSalePrice.FieldName = "PSNumberSalePrice";
            this.colPSNumberSalePrice.Name = "colPSNumberSalePrice";
            this.colPSNumberSalePrice.Width = 64;
            // 
            // colPSNumberDiscount
            // 
            this.colPSNumberDiscount.FieldName = "PSNumberDiscount";
            this.colPSNumberDiscount.Name = "colPSNumberDiscount";
            this.colPSNumberDiscount.Width = 64;
            // 
            // colStatus
            // 
            this.colStatus.FieldName = "Status";
            this.colStatus.Name = "colStatus";
            this.colStatus.Width = 64;
            // 
            // colNo
            // 
            this.colNo.Caption = "Line No";
            this.colNo.FieldName = "No";
            this.colNo.MinWidth = 43;
            this.colNo.Name = "colNo";
            this.colNo.Visible = true;
            this.colNo.VisibleIndex = 0;
            this.colNo.Width = 81;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2});
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(1298, 476);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.gridControlIODetail;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(1280, 460);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // wRDetailBindingSource1
            // 
            this.wRDetailBindingSource1.DataMember = "WRDetail";
            this.wRDetailBindingSource1.DataSource = this.wARHOUSE_HPDataSet1;
            // 
            // wRDetailBindingSource
            // 
            this.wRDetailBindingSource.DataMember = "WRDetail";
            this.wRDetailBindingSource.DataSource = this.wARHOUSE_HPDataSet1;
            // 
            // wRDetailTableAdapter
            // 
            this.wRDetailTableAdapter.ClearBeforeFill = true;
            // 
            // iODetailTableAdapter
            // 
            this.iODetailTableAdapter.ClearBeforeFill = true;
            // 
            // layoutControlItemCustomer
            // 
            this.layoutControlItemCustomer.Control = this.textEditCustomer;
            this.layoutControlItemCustomer.Location = new System.Drawing.Point(453, 24);
            this.layoutControlItemCustomer.Name = "layoutControlItemCustomer";
            this.layoutControlItemCustomer.OptionsTableLayoutItem.ColumnIndex = 3;
            this.layoutControlItemCustomer.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItemCustomer.Size = new System.Drawing.Size(206, 24);
            this.layoutControlItemCustomer.Text = "Khách hàng";
            this.layoutControlItemCustomer.TextSize = new System.Drawing.Size(100, 13);
            // 
            // layoutControlItemNote
            // 
            this.layoutControlItemNote.Control = this.textEditNote;
            this.layoutControlItemNote.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItemNote.Name = "layoutControlItemNote";
            this.layoutControlItemNote.OptionsTableLayoutItem.ColumnSpan = 6;
            this.layoutControlItemNote.OptionsTableLayoutItem.RowIndex = 3;
            this.layoutControlItemNote.Size = new System.Drawing.Size(1071, 25);
            this.layoutControlItemNote.Text = "Ghi chú";
            this.layoutControlItemNote.TextSize = new System.Drawing.Size(100, 13);
            // 
            // IssueOrderControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl3);
            this.Controls.Add(this.layoutControl2);
            this.Controls.Add(this.layoutControl1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "IssueOrderControl";
            this.Size = new System.Drawing.Size(1298, 626);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wRDataDetailBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wARHOUSE_HPDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditIONumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditModality.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditIODate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditIODate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditNote.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditWarehouse.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditOrderNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditOrderDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditOrderDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditCustomer.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditContractNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditPriceType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditPaymentType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditHandlingStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalQuantity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalDiscountAmount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalVATAmount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTotalAmount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditRONumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditContractDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditContractDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemIONumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemModality)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemIODate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWarehouse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemOrderNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemOrderDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemContractNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalQuantity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalDiscountAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalVATAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTotalAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemRONumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemContractDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPriceType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPaymentType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemHandlingStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).EndInit();
            this.layoutControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlIODetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iODetailBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wARHOUSE_HPDataSet2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewIODetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wRDetailBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wRDetailBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemCustomer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemNote)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.TextEdit textEdit4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private System.Windows.Forms.BindingSource wRDataDetailBindingSource;
        private WARHOUSE_HPDataSet1 wARHOUSE_HPDataSet1;
        private WARHOUSE_HPDataSet1TableAdapters.WRDataDetailTableAdapter wRDataDetailTableAdapter;
        private DevExpress.XtraEditors.TextEdit textEditIONumber;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditModality;
        private DevExpress.XtraEditors.DateEdit dateEditIODate;
        private DevExpress.XtraEditors.TextEdit textEditNote;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditWarehouse;
        private DevExpress.XtraEditors.TextEdit textEditOrderNumber;
        private DevExpress.XtraEditors.DateEdit dateEditOrderDate;
        private DevExpress.XtraEditors.TextEdit textEditCustomer;
        private DevExpress.XtraEditors.TextEdit textEditContractNumber;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditPriceType;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditPaymentType;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditHandlingStatus;
        private DevExpress.XtraEditors.TextEdit textEditTotalQuantity;
        private DevExpress.XtraEditors.TextEdit textEditTotalDiscountAmount;
        private DevExpress.XtraEditors.TextEdit textEditTotalVATAmount;
        private DevExpress.XtraEditors.TextEdit textEditTotalAmount;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemIONumber;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemModality;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemIODate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemWarehouse;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemOrderNumber;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemOrderDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemContractNumber;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemTotalQuantity;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemTotalDiscountAmount;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemTotalVATAmount;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemTotalAmount;
        private DevExpress.XtraLayout.LayoutControl layoutControl3;
        private DevExpress.XtraGrid.GridControl gridControlIODetail;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewIODetail;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private System.Windows.Forms.BindingSource wRDetailBindingSource1;
        private System.Windows.Forms.BindingSource wRDetailBindingSource;
        private WARHOUSE_HPDataSet1TableAdapters.WRDetailTableAdapter wRDetailTableAdapter;
        private DevExpress.XtraEditors.TextEdit textEditRONumber;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemRONumber;
        private System.Windows.Forms.BindingSource iODetailBindingSource;
        private WARHOUSE_HPDataSet2 wARHOUSE_HPDataSet2;
        private DevExpress.XtraGrid.Columns.GridColumn colIONumber;
        private DevExpress.XtraGrid.Columns.GridColumn colOrdinal;
        private DevExpress.XtraGrid.Columns.GridColumn colGoodsID;
        private DevExpress.XtraGrid.Columns.GridColumn colGoodsName;
        private DevExpress.XtraGrid.Columns.GridColumn colIssueUnitID;
        private DevExpress.XtraGrid.Columns.GridColumn colUnitRate;
        private DevExpress.XtraGrid.Columns.GridColumn colStockUnitID;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantity;
        private DevExpress.XtraGrid.Columns.GridColumn colPriceIncludedVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colPriceExcludedVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colPrice;
        private DevExpress.XtraGrid.Columns.GridColumn colDiscount;
        private DevExpress.XtraGrid.Columns.GridColumn colDiscountAmount;
        private DevExpress.XtraGrid.Columns.GridColumn colVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colVATAmount;
        private DevExpress.XtraGrid.Columns.GridColumn colAmountExcludedVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colAmountIncludedVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colAmount;
        private DevExpress.XtraGrid.Columns.GridColumn colExpiryDate;
        private DevExpress.XtraGrid.Columns.GridColumn colSerialNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colNote;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantityIssued;
        private DevExpress.XtraGrid.Columns.GridColumn colPriceIncludedVATIssued;
        private DevExpress.XtraGrid.Columns.GridColumn colPriceExcludedVATIssued;
        private DevExpress.XtraGrid.Columns.GridColumn colPriceIssued;
        private DevExpress.XtraGrid.Columns.GridColumn colDiscountIssued;
        private DevExpress.XtraGrid.Columns.GridColumn colDiscountAmountIssued;
        private DevExpress.XtraGrid.Columns.GridColumn colVATIssued;
        private DevExpress.XtraGrid.Columns.GridColumn colVATAmountIssued;
        private DevExpress.XtraGrid.Columns.GridColumn colAmountExcludedVATIssued;
        private DevExpress.XtraGrid.Columns.GridColumn colAmountIncludedVATIssued;
        private DevExpress.XtraGrid.Columns.GridColumn colAmountIssued;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantityOrdered;
        private DevExpress.XtraGrid.Columns.GridColumn colOrderQuantityIssued;
        private DevExpress.XtraGrid.Columns.GridColumn colIssueQuantityReceived;
        private DevExpress.XtraGrid.Columns.GridColumn colIssueQuantity;
        private DevExpress.XtraGrid.Columns.GridColumn colIssueAmount;
        private DevExpress.XtraGrid.Columns.GridColumn colAverageCost;
        private DevExpress.XtraGrid.Columns.GridColumn colSPSNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colPSNumberSalePrice;
        private DevExpress.XtraGrid.Columns.GridColumn colPSNumberDiscount;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus;
        private WARHOUSE_HPDataSet2TableAdapters.IODetailTableAdapter iODetailTableAdapter;
        private DevExpress.XtraEditors.DateEdit dateEditContractDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemContractDate;
        private DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel windowsUIButtonPanel1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemPriceType;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemPaymentType;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemHandlingStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colNo;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemCustomer;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemNote;
    }
}
