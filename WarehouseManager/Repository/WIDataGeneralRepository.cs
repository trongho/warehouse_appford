﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WarehouseManager.Helpers;
using WarehouseManager.Models;

namespace WarehouseManager.Repository
{
    class WIDataGeneralRepository
    {
        public WIDataGeneralRepository()
        {
        }

        public async Task<long> Create(WIDataGeneral entry)
        {
            long result = -1;
            string URI = Constant.BaseURL+"widatageneral/post";

            using (var client = new HttpClient())
            {
                var content = new StringContent(JsonConvert.SerializeObject(entry), Encoding.UTF8, "application/json");
                try
                {
                    using (var response = await client.PostAsync(URI, content))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            result = 1;
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return result;
        }

        public async Task<List<WIDataGeneral>> GetAll()
        {
            string URI = Constant.BaseURL+"widatageneral";
            List<WIDataGeneral> entrys = new List<WIDataGeneral>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<WIDataGeneral[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }

        public async Task<List<WIDataGeneral>> GetUnderID(String id)
        {
            string URI = Constant.BaseURL+"widatageneral/GetByID/" + id;
            List<WIDataGeneral> entrys = new List<WIDataGeneral>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<WIDataGeneral[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }

        public async Task<List<WIDataGeneral>> GetUnderMultiID(String wIDNumber, String goodsID, int ordinal)
        {
            string URI = Constant.BaseURL+"widatageneral/GetByMultiID/" + wIDNumber + "/" + goodsID + "/" + ordinal;
            List<WIDataGeneral> entrys = new List<WIDataGeneral>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<WIDataGeneral[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }

        public async Task<List<WIDataGeneral>> GetUnderMultiID2(String wIDNumber, String goodsID, int ordinal,String goodsGroupID)
        {
            string URI = Constant.BaseURL + "widatageneral/GetByMultiID2/" + wIDNumber + "/" + goodsID + "/" + ordinal + "/" + goodsGroupID;
            List<WIDataGeneral> entrys = new List<WIDataGeneral>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<WIDataGeneral[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }

        public async Task<List<WIDataGeneral>> GetUnderPickerName(String wIRNumber, String pickerName)
        {
            string URI = Constant.BaseURL + "widatageneral/GetByPickerName/" + wIRNumber + "/" + pickerName;
            List<WIDataGeneral> entrys = new List<WIDataGeneral>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<WIDataGeneral[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }

        public async Task<List<WIDataGeneral>> GetUnderGoodsGroupID(String wIRNumber, String goodsGroupID)
        {
            string URI = Constant.BaseURL + "widatageneral/GetByGoodsGroupID/" + wIRNumber + "/" + goodsGroupID;
            List<WIDataGeneral> entrys = new List<WIDataGeneral>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<WIDataGeneral[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }

        public async Task<List<WIDataGeneral>> GetUnderScanOption(String wIRNumber, String scanOption)
        {
            string URI = Constant.BaseURL + "widatageneral/GetByScanOption/" + wIRNumber + "/" + scanOption;
            List<WIDataGeneral> entrys = new List<WIDataGeneral>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<WIDataGeneral[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }

        public async Task<List<WIDataGeneral>> GetUnderGPS(String widNumber, String GoodsGroupID, String PickerName, String ScanOption)
        {
            string URI = Constant.BaseURL + "widatageneral/Filter/" + widNumber + "/" +GoodsGroupID + "/" + PickerName + "/" + ScanOption;
            List<WIDataGeneral> entrys = new List<WIDataGeneral>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<WIDataGeneral[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }


        public async Task<List<WIDataGeneral>> Filter(String widNumber,String selectedGoodsGroupID,String selectedPickerName,String selectedScanOption)
        {
            List<WIDataGeneral> listByGoodsGroupID = await GetUnderGoodsGroupID(widNumber, selectedGoodsGroupID);
            List<WIDataGeneral> listByPickerName = await GetUnderPickerName(widNumber, selectedPickerName);
            List<WIDataGeneral> listByScanOption = await GetUnderScanOption(widNumber, selectedScanOption);
            List<WIDataGeneral> entrys = new List<WIDataGeneral>();

            if (selectedGoodsGroupID == "All" && selectedPickerName == "All" && selectedScanOption == "All")
            {
                entrys = await GetUnderID(widNumber);
            }
            else if (selectedGoodsGroupID == "All" && selectedPickerName == "All" && selectedScanOption != "All")
            {
                entrys = listByScanOption;
            }
            else if (selectedGoodsGroupID == "All" && selectedPickerName != "All" && selectedScanOption == "All")
            {
                entrys = listByPickerName;
            }
            else if (selectedGoodsGroupID != "All" && selectedPickerName == "All" && selectedScanOption == "All")
            {
                entrys = listByGoodsGroupID;
            }
            else if (selectedGoodsGroupID == "All" && selectedPickerName != "All" && selectedScanOption != "All")
            {
                foreach (WIDataGeneral item in listByPickerName)
                {
                    foreach (WIDataGeneral item2 in listByScanOption)
                    {
                        if (item.GoodsGroupID == item2.GoodsGroupID && item.GoodsID == item2.GoodsID)
                        {
                            entrys.Add(item);
                        }
                    }
                }
            }
            else if (selectedGoodsGroupID != "All" && selectedPickerName == "All" && selectedScanOption != "All")
            {

                foreach (WIDataGeneral item in listByGoodsGroupID)
                {
                    foreach (WIDataGeneral item2 in listByScanOption)
                    {
                        if (item.GoodsGroupID == item2.GoodsGroupID && item.GoodsID == item2.GoodsID)
                        {
                            entrys.Add(item);
                        }
                    }
                }
            }
            else if (selectedGoodsGroupID != "All" && selectedPickerName != "All" && selectedScanOption == "All")
            {

                foreach (WIDataGeneral item in listByPickerName)
                {
                    foreach (WIDataGeneral item2 in listByGoodsGroupID)
                    {
                        if (item.GoodsGroupID == item2.GoodsGroupID && item.GoodsID == item2.GoodsID)
                        {
                            entrys.Add(item);
                        }
                    }
                }
            }
            else if (selectedGoodsGroupID != "All" && selectedPickerName != "All" && selectedScanOption != "All")
            {

                entrys = await GetUnderGPS(widNumber, selectedGoodsGroupID, selectedPickerName, selectedScanOption);
            }
            return entrys;
        }

        public async Task<long> Update(WIDataGeneral entry, String wIDNumber, String goodsID, int ordinal,String goodsGroupID)
        {
            long result = -1;
            string URI = Constant.BaseURL+"widatageneral/Put/" + wIDNumber + "/" + goodsID + "/" + ordinal + "/" +goodsGroupID;
            using (var client = new HttpClient())
            {
                using (var response = await client.PutAsJsonAsync(URI, entry))
                {

                    if (response.IsSuccessStatusCode)
                    {
                        result = 1;
                    }
                }
            }
            return result;
        }

        public async Task<long> Delete(String id)
        {
            long result = -1;
            string URI = Constant.BaseURL+"widatageneral/Delete/" + id;
            using (var client = new HttpClient())
            {
                using (var response = await client.DeleteAsync(URI))
                {


                    if (response.IsSuccessStatusCode)
                    {
                        result = 1;
                    }
                }
            }
            return result;
        }

        public async Task<Boolean> checkExistAsync(String wIDNumber, String goodsID)
        {
            Boolean result = false;
            string URI = Constant.BaseURL+"widatageneral/CheckExist/" + wIDNumber + "/" + goodsID;
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            result = Boolean.Parse(await response.Content.ReadAsStringAsync());
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return result;
        }

        public async Task<String> GetLastWIDNumber()
        {
            string URI = Constant.BaseURL+"widatageneral/lastWIDNumber";
            String lastWIDNumber = "";
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            lastWIDNumber = fileJsonString;
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return lastWIDNumber;
        }
    }
}
