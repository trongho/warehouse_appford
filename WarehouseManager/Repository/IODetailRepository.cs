﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WarehouseManager.Helpers;
using WarehouseManager.Models;

namespace WarehouseManager.Repository
{
    class IODetailRepository
    {
        public IODetailRepository()
        {
        }
        public async Task<long> Create(IODetail entry)
        {
            long result = -1;
            string URI = Constant.BaseURL+"iodetail/post";

            using (var client = new HttpClient())
            {
                var content = new StringContent(JsonConvert.SerializeObject(entry), Encoding.UTF8, "application/json");
                try
                {
                    using (var response = await client.PostAsync(URI, content))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            result = 1;
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return result;
        }

        public async Task<List<IODetail>> GetAll()
        {
            string URI = Constant.BaseURL+"iodetail";
            List<IODetail> entrys = new List<IODetail>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<IODetail[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }

        public async Task<List<IODetail>> GetUnderID(String id)
        {
            string URI = Constant.BaseURL+"iodetail/GetByID/" + id;
            List<IODetail> entrys = new List<IODetail>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<IODetail[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }

        public async Task<List<IODetail>> GetUnderMultiID(String IONumber, String goodsID, int ordinal)
        {
            string URI = Constant.BaseURL+"iodetail/GetByMultiID/" + IONumber + "/" + goodsID + "/" + ordinal;
            List<IODetail> entrys = new List<IODetail>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<IODetail[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }

        public async Task<long> Update(IODetail entry, String IONumber, String goodsID, int ordinal)
        {
            long result = -1;
            string URI = Constant.BaseURL+"iodetail/Put/" + IONumber + "/" + goodsID + "/" + ordinal;
            using (var client = new HttpClient())
            {
                using (var response = await client.PutAsJsonAsync(URI, entry))
                {

                    if (response.IsSuccessStatusCode)
                    {
                        result = 1;
                    }
                }
            }
            return result;
        }

        public async Task<long> Delete(String id)
        {
            long result = -1;
            string URI = Constant.BaseURL+"iodetail/Delete/" + id;
            using (var client = new HttpClient())
            {
                using (var response = await client.DeleteAsync(URI))
                {


                    if (response.IsSuccessStatusCode)
                    {
                        result = 1;
                    }
                }
            }
            return result;
        }

        public async Task<Boolean> checkExistAsync(String id)
        {
            Boolean result = false;
            string URI = Constant.BaseURL+"iodetail/CheckExist/" + id;
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            result = Boolean.Parse(await response.Content.ReadAsStringAsync());
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return result;
        }

        public async Task<String> GetLastIONumber()
        {
            string URI = Constant.BaseURL+"iodetail/lastIONumber";
            String lastIONumber = "";
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            lastIONumber = fileJsonString;
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return lastIONumber;
        }

        public async Task<Boolean> CheckDataChanged()
        {
            Boolean result = false;
            string URI = Constant.BaseURL + "iodetail/CheckDataChanged";
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            result = Boolean.Parse(await response.Content.ReadAsStringAsync());
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return result;
        }
    }
}
