﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WarehouseManager.Helpers;
using webservice.Entities;

namespace WarehouseManager.Repository
{
    class InventoryRepository
    {
        public InventoryRepository()
        {
        }

        public async Task<long> Create(Inventory entry)
        {
            long result = -1;
            string URI = Constant.BaseURL+"inventory/post";

            using (var client = new HttpClient())
            {
                var content = new StringContent(JsonConvert.SerializeObject(entry), Encoding.UTF8, "application/json");
                try
                {
                    using (var response = await client.PostAsync(URI, content))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            result = 1;
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return result;
        }

        public async Task<List<Inventory>> GetAll()
        {
            string URI = Constant.BaseURL+"inventory";
            List<Inventory> entrys = new List<Inventory>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<Inventory[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }

        public async Task<List<Inventory>> GetUnderMultiID(Int16 year, Int16 month, String warehouseID)
        {
            string URI = Constant.BaseURL+"inventory/GetByMultiID/" + year + "/" +month + "/" + warehouseID;
            List<Inventory> entrys = new List<Inventory>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<Inventory[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }

        public async Task<List<Inventory>> GetUnderMultiGoodsID(Int16 year, Int16 month, String warehouseID,String goodsID)
        {
            string URI = Constant.BaseURL+"inventory/GetByMultiGoodsID/" + year + "/" + month + "/" + warehouseID + "/" + goodsID;
            List<Inventory> entrys = new List<Inventory>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<Inventory[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }

        public async Task<long> Update(Inventory entry, Int16 year, Int16 month, String warehouseID, String goodsID)
        {
            long result = -1;
            string URI = Constant.BaseURL+"inventory/Put/" +year + "/" + month + "/" + warehouseID + "/" +goodsID;
            using (var client = new HttpClient())
            {
                using (var response = await client.PutAsJsonAsync(URI, entry))
                {

                    if (response.IsSuccessStatusCode)
                    {
                        result = 1;
                    }
                }
            }
            return result;
        }

        public async Task<long> Delete(Int16 year, Int16 month, String warehouseID)
        {
            long result = -1;
            string URI = Constant.BaseURL+"inventory/Delete/" + year + "/" + month + "/" + warehouseID;
            using (var client = new HttpClient())
            {
                using (var response = await client.DeleteAsync(URI))
                {


                    if (response.IsSuccessStatusCode)
                    {
                        result = 1;
                    }
                }
            }
            return result;
        }

        public async Task<Boolean> checkExistAsync(Int16 year, Int16 month, String warehouseID)
        {
            Boolean result = false;
            string URI = Constant.BaseURL+"inventory/CheckExist/" +year + "/" + month + "/" + warehouseID;
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            result = Boolean.Parse(await response.Content.ReadAsStringAsync());
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return result;
        }

        public async Task<Boolean> checkExistAsync(Int16 year, Int16 month, String warehouseID,String goodsID)
        {
            Boolean result = false;
            string URI = Constant.BaseURL+"inventory/CheckExistWithGoods/" + year + "/" + month + "/" + warehouseID + "/" +goodsID;
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            result = Boolean.Parse(await response.Content.ReadAsStringAsync());
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return result;
        }
    }
}
