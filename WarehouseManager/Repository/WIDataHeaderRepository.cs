﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WarehouseManager.Helpers;
using WarehouseManager.Models;

namespace WarehouseManager.Repository
{
    class WIDataHeaderRepository
    {
        public WIDataHeaderRepository()
        {
        }

        public async Task<long> Create(WIDataHeader entry)
        {
            long result = -1;
            string URI = Constant.BaseURL+"widataheader/post";

            using (var client = new HttpClient())
            {
                var content = new StringContent(JsonConvert.SerializeObject(entry), Encoding.UTF8, "application/json");
                try
                {
                    using (var response = await client.PostAsync(URI, content))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            result = 1;
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return result;
        }

        public async Task<List<WIDataHeader>> GetAll()
        {
            string URI = Constant.BaseURL+"widataheader";
            List<WIDataHeader> entrys = new List<WIDataHeader>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<WIDataHeader[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }

        public async Task<WIDataHeader> GetUnderID(String id)
        {
            string URI = Constant.BaseURL+"widataheader/GetByID/" + id;
            List<WIDataHeader> entrys = new List<WIDataHeader>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<WIDataHeader[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys[0];
        }

        public async Task<long> Update(WIDataHeader entry, String id)
        {
            long result = -1;
            string URI = Constant.BaseURL+"widataheader/Put/" + id;
            using (var client = new HttpClient())
            {
                using (var response = await client.PutAsJsonAsync(URI, entry))
                {

                    if (response.IsSuccessStatusCode)
                    {
                        result = 1;
                    }
                }
            }
            return result;
        }

        public async Task<long> Delete(String id)
        {
            long result = -1;
            string URI = Constant.BaseURL+"widataheader/Delete/" + id;
            using (var client = new HttpClient())
            {
                using (var response = await client.DeleteAsync(URI))
                {


                    if (response.IsSuccessStatusCode)
                    {
                        result = 1;
                    }
                }
            }
            return result;
        }

        public async Task<Boolean> checkExistAsync(String id)
        {
            Boolean result = false;
            string URI = Constant.BaseURL+"widataheader/CheckExist/" + id;
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            result = Boolean.Parse(await response.Content.ReadAsStringAsync());
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return result;
        }

        public async Task<String> GetLastWIDNumber()
        {
            string URI = Constant.BaseURL+"widataheader/lastWIDNumber";
            String lastWIDNumber = "";
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            lastWIDNumber = fileJsonString;
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return lastWIDNumber;
        }

        public async Task<List<WIDataHeader>> GetUnderDate(DateTime fromDate, DateTime toDate)
        {
            String fromDateString = fromDate.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
            String toDateString = toDate.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
            string URI = Constant.BaseURL+"widataheader/FilterByDate/" + fromDateString + "/" + toDateString;
            List<WIDataHeader> entrys = new List<WIDataHeader>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<WIDataHeader[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }

        public async Task<List<WIDataHeader>> GetUnderBranch(String branchID)
        {
            string URI = Constant.BaseURL+"widataheader/FilterByBranch/" + branchID;
            List<WIDataHeader> entrys = new List<WIDataHeader>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<WIDataHeader[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }

        public async Task<List<WIDataHeader>> GetUnderHandlingStatus(String handlingStatusID)
        {
            string URI = Constant.BaseURL+"widataheader/FilterByHandlingStatus/" + handlingStatusID;
            List<WIDataHeader> entrys = new List<WIDataHeader>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<WIDataHeader[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }

        public async Task<List<WIDataHeader>> GetUnderOrderStatus(String orderStatusID)
        {
            string URI = Constant.BaseURL+"widataheader/FilterByOrderStatus/" + orderStatusID;
            List<WIDataHeader> entrys = new List<WIDataHeader>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<WIDataHeader[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }

        public async Task<List<WIDataHeader>> Fillter(string branchID, DateTime fromDate, DateTime toDate, string handlingStatusID)
        {
            String fromDateString = fromDate.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
            String toDateString = toDate.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
            string URI = Constant.BaseURL + "widataheader/Filter/" + branchID + "/" + fromDateString + "/" + toDateString + "/" + handlingStatusID;
            List<WIDataHeader> entrys = new List<WIDataHeader>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<WIDataHeader[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }

        public async Task<List<WIDataHeader>> FilterByDateAndHandlingStatus(DateTime fromDate, DateTime toDate, string handlingStatusID)
        {
            String fromDateString = fromDate.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
            String toDateString = toDate.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
            string URI = Constant.BaseURL + "widataheader/FilterByDateAndHandlingStatus/" + fromDateString + "/" + toDateString + "/" + handlingStatusID;
            List<WIDataHeader> entrys = new List<WIDataHeader>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<WIDataHeader[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }

        public async Task<List<WIDataHeader>> FilterByDateAndBranch(DateTime fromDate, DateTime toDate, string branchID)
        {
            String fromDateString = fromDate.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
            String toDateString = toDate.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
            string URI = Constant.BaseURL + "widataheader/FilterByDateAndBranch/" + fromDateString + "/" + toDateString + "/" + branchID;
            List<WIDataHeader> entrys = new List<WIDataHeader>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<WIDataHeader[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }

        public async Task<List<WIDataHeader>> FilterFinal(string branchID, DateTime fromDate, DateTime toDate, string handlingStatusID)
        {
            List<WIDataHeader> entrys = new List<WIDataHeader>();
            if (branchID.Equals("<>"))
            {
                if (handlingStatusID.Equals("<>"))
                {
                    entrys = await GetUnderDate(fromDate, toDate);
                }
                else
                {
                    entrys = await FilterByDateAndHandlingStatus(fromDate, toDate, handlingStatusID);
                }
            }
            else
            {
                if (handlingStatusID.Equals("<>"))
                {
                    entrys = await FilterByDateAndBranch(fromDate, toDate, branchID);
                }
                else
                {
                    entrys = await Fillter(branchID, fromDate, toDate, handlingStatusID);
                }

            }

            return entrys;
        }
    }
}
