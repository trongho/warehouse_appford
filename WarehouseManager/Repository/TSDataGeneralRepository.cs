﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WarehouseManager.Helpers;
using WarehouseManager.Models;

namespace WarehouseManager.Repository
{
    class TSDataGeneralRepository
    {
        public TSDataGeneralRepository()
        {
        }
        public async Task<long> Create(TSDataGeneral entry)
        {
            long result = -1;
            string URI = Constant.BaseURL+"tsdatageneral/post";

            using (var client = new HttpClient())
            {
                var content = new StringContent(JsonConvert.SerializeObject(entry), Encoding.UTF8, "application/json");
                try
                {
                    using (var response = await client.PostAsync(URI, content))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            result = 1;
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return result;
        }

        public async Task<List<TSDetail>> GetUnderID(String warehouseID,DateTime tallyDate)
        {
            String tallyDateString = tallyDate.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
            string URI = Constant.BaseURL+"tsdatageneral/GetByID/" + warehouseID+"/"+tallyDateString;
            List<TSDetail> entrys = new List<TSDetail>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<TSDetail[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }

        public async Task<TSDetail> GetUnderMultiID(String warehouseID,String goodsID,int ordinal, DateTime tallyDate)
        {
            String tallyDateString = tallyDate.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
            string URI = Constant.BaseURL+"tsdatageneral/GetByMultiID/" + warehouseID + "/" +goodsID+"/"+ordinal+"/"+ tallyDateString;
            List<TSDetail> entrys = new List<TSDetail>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<TSDetail[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys[0];
        }

        public async Task<long> Update(TSDetail entry, String warehouseID, String goodsID, int ordinal, DateTime tallyDate)
        {
            String tallyDateString = tallyDate.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
            long result = -1;
            string URI = Constant.BaseURL+"tsdatageneral/Put/" + warehouseID + "/" + goodsID + "/" + ordinal + "/" + tallyDateString;
            using (var client = new HttpClient())
            {
                using (var response = await client.PutAsJsonAsync(URI, entry))
                {

                    if (response.IsSuccessStatusCode)
                    {
                        result = 1;
                    }
                }
            }
            return result;
        }

        public async Task<long> Delete(String warehouseID,DateTime tallyDate)
        {
            long result = -1;
            String tallyDateString = tallyDate.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
            string URI = Constant.BaseURL+"tsdatageneral/Delete/" + warehouseID + "/" + tallyDateString;
            using (var client = new HttpClient())
            {
                using (var response = await client.DeleteAsync(URI))
                {


                    if (response.IsSuccessStatusCode)
                    {
                        result = 1;
                    }
                }
            }
            return result;
        }
    }
}
