﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WarehouseManager.Helpers;
using webservice.Entities;

namespace WarehouseManager.Repository
{
    class WRDataHeaderRepository
    {
        public WRDataHeaderRepository()
        {
        }

        public async Task<long> Create(WRDataHeader entry)
        {
            long result = -1;
            string URI = Constant.BaseURL+"wrdataheader/post";

            using (var client = new HttpClient())
            {
                var content = new StringContent(JsonConvert.SerializeObject(entry), Encoding.UTF8, "application/json");
                try
                {
                    using (var response = await client.PostAsync(URI, content))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            result = 1;
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return result;
        }

        public async Task<List<WRDataHeader>> GetAll()
        {
            string URI = Constant.BaseURL+"wrdataheader";
            List<WRDataHeader> entrys = new List<WRDataHeader>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<WRDataHeader[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }

        public async Task<WRDataHeader> GetUnderID(String id)
        {
            string URI = Constant.BaseURL+"wrdataheader/GetByID/" + id;
            List<WRDataHeader> entrys = new List<WRDataHeader>();
            WRDataHeader wRDataHeader = new WRDataHeader();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<WRDataHeader[]>(fileJsonString).ToList();
                            if (entrys.Count > 0)
                            {
                                wRDataHeader = entrys[0];
                            }
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return wRDataHeader;
        }

        public async Task<long> Update(WRDataHeader entry, String id)
        {
            long result = -1;
            string URI = Constant.BaseURL+"wrdataheader/Put/" + id;
            using (var client = new HttpClient())
            {
                using (var response = await client.PutAsJsonAsync(URI, entry))
                {

                    if (response.IsSuccessStatusCode)
                    {
                        result = 1;
                    }
                }
            }
            return result;
        }

        public async Task<long> Delete(String id)
        {
            long result = -1;
            string URI = Constant.BaseURL+"wrdataheader/Delete/" + id;
            using (var client = new HttpClient())
            {
                using (var response = await client.DeleteAsync(URI))
                {


                    if (response.IsSuccessStatusCode)
                    {
                        result = 1;
                    }
                }
            }
            return result;
        }

        public async Task<Boolean> checkExistAsync(String id)
        {
            Boolean result = false;
            string URI = Constant.BaseURL+"wrdataheader/CheckExist/" + id;
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            result = Boolean.Parse(await response.Content.ReadAsStringAsync());
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return result;
        }

        public async Task<String> GetLastWRDNumber()
        {
            string URI = Constant.BaseURL+"wrdataheader/lastWRDNumber";
            String lastWRDNumber = "";
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            lastWRDNumber = fileJsonString;
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return lastWRDNumber;
        }

        public async Task<String> CheckTotalQuantity(String wrdNumber)
        {
            string URI = Constant.BaseURL + "wrdataheader/CheckTotalQuantity/" + wrdNumber;
            String result = "";
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            result = fileJsonString;
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return result;
        }

        public async Task<List<WRDataHeader>> GetUnderDate(DateTime fromDate, DateTime toDate)
        {
            
            string URI = Constant.BaseURL+"wrdataheader/FilterByDate/" + fromDate + "/" + toDate;
            List<WRDataHeader> entrys = new List<WRDataHeader>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<WRDataHeader[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }

        public async Task<List<WRDataHeader>> GetUnderBranch(String branchID)
        {
            string URI = Constant.BaseURL+"wrdataheader/FilterByBranch/" + branchID;
            List<WRDataHeader> entrys = new List<WRDataHeader>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<WRDataHeader[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }

        public async Task<List<WRDataHeader>> GetUnderHandlingStatus(String handlingStatusID)
        {
            string URI = Constant.BaseURL+"wrdataheader/FilterByHandlingStatus/" + handlingStatusID;
            List<WRDataHeader> entrys = new List<WRDataHeader>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<WRDataHeader[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }

        public async Task<List<WRDataHeader>> GetUnderOrderStatus(String orderStatusID)
        {
            string URI = Constant.BaseURL+"wrdataheader/FilterByOrderStatus/" + orderStatusID;
            List<WRDataHeader> entrys = new List<WRDataHeader>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<WRDataHeader[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }


        public async Task<List<WRDataHeader>> Fillter(string branchID, DateTime fromDate, DateTime toDate, string handlingStatusID)
        {
            String fromDateString = fromDate.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
            String toDateString = toDate.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
            string URI = Constant.BaseURL + "wrdataheader/Filter/" +branchID+"/"+fromDateString+"/"+toDateString+"/"+handlingStatusID;
            List<WRDataHeader> entrys = new List<WRDataHeader>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<WRDataHeader[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }

        public async Task<List<WRDataHeader>> FilterByDateAndHandlingStatus(DateTime fromDate, DateTime toDate, string handlingStatusID)
        {
            String fromDateString = fromDate.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
            String toDateString = toDate.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
            string URI = Constant.BaseURL + "wrdataheader/FilterByDateAndHandlingStatus/"+ fromDateString + "/" + toDateString + "/" + handlingStatusID;
            List<WRDataHeader> entrys = new List<WRDataHeader>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<WRDataHeader[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }

        public async Task<List<WRDataHeader>> FilterByDateAndBranch(DateTime fromDate, DateTime toDate, string branchID)
        {
            String fromDateString = fromDate.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
            String toDateString = toDate.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
            string URI = Constant.BaseURL + "wrdataheader/FilterByDateAndBranch/" + fromDateString + "/" + toDateString + "/" + branchID;
            List<WRDataHeader> entrys = new List<WRDataHeader>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<WRDataHeader[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }

        public async Task<List<WRDataHeader>> FilterFinal(string branchID, DateTime fromDate, DateTime toDate, string handlingStatusID)
        {
            List<WRDataHeader> entrys = new List<WRDataHeader>();
            if (branchID.Equals("<>"))
            {
                if (handlingStatusID.Equals("<>"))
                {
                    entrys = await GetUnderDate(fromDate,toDate);
                }
                else
                {
                    entrys = await FilterByDateAndHandlingStatus(fromDate, toDate, handlingStatusID);
                }
            }
            else
            {
                if (handlingStatusID.Equals("<>"))
                {
                    entrys = await FilterByDateAndBranch(fromDate, toDate,branchID);
                }
                else
                {
                    entrys = await Fillter(branchID, fromDate, toDate, handlingStatusID);
                }
                
            }

            return entrys;
        }

    }


}
