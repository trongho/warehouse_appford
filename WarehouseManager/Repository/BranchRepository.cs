﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WarehouseManager.Helpers;
using WarehouseManager.Models;

namespace WarehouseManager.Repository
{
    class BranchRepository
    {
        public BranchRepository()
        {
        }

        public async Task<List<Branch>> getAllBranchAsync()
        {
            string URI = Constant.BaseURL+"branch";
            List<Branch> branchs = new List<Branch>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            branchs = JsonConvert.DeserializeObject<Branch[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return branchs;
        }
    }
}
