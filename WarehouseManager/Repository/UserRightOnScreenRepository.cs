﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WarehouseManager.Helpers;
using WarehouseManager.Models;

namespace WarehouseManager.Repository
{
    class UserRightOnScreenRepository
    {
        public UserRightOnScreenRepository()
        {
        }

        public async Task<long> createUserRightOnscreenAsync(UserRightOnScreen userRightOnScreen)
        {
            long result = -1;
            string URI = Constant.BaseURL+"userrightonscreen/PostUserRightOnScreen/";

            using (var client = new HttpClient())
            {
                var content = new StringContent(JsonConvert.SerializeObject(userRightOnScreen), Encoding.UTF8, "application/json");
                try
                {
                    using (var response = await client.PostAsync(URI, content))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            result = 1;
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return result;
        }

        public async Task<List<UserRightOnScreen>> getUserRightOnScreenAsync(String userID, String screenID)
        {
            string URI = Constant.BaseURL+"userrightonscreen/GetByID/" + userID + "/" + screenID;
            List<UserRightOnScreen> entrys = new List<UserRightOnScreen>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<UserRightOnScreen[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }

        public async Task<List<UserRightOnScreen>> getUserRightOnScreenAsync(String userID)
        {
            string URI = Constant.BaseURL+"userrightonscreen/GetByUserID/" + userID;
            List<UserRightOnScreen> entrys = new List<UserRightOnScreen>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<UserRightOnScreen[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }


        public async Task<long> updateUserRightOnScreen(UserRightOnScreen userRightOnScreen, String userId,String screenId)
        {
            long result = -1;
            string URI = Constant.BaseURL+"userrightonscreen/PutUserRightOnScreen/" +userId+"/"+screenId;
            using (var client = new HttpClient())
            {
                using (var response = await client.PutAsJsonAsync(URI, userRightOnScreen))
                {

                    if (response.IsSuccessStatusCode)
                    {
                        result = 1;
                    }
                }
            }
            return result;
        }

        public async Task<long> DeleteDataAsync(String userId,String screenId)
        {
            long result = -1;
            string URI = Constant.BaseURL+"userrightonscreen/DeleteUserRightOnScreen/" +userId+"/"+screenId;
            using (var client = new HttpClient())
            {
                using (var response = await client.DeleteAsync(URI))
                {


                    if (response.IsSuccessStatusCode)
                    {
                        result = 1;
                    }
                }
            }
            return result;
        }

        public async Task<Boolean> checkExistAsync(String userId,String screenId)
        {
            Boolean result = false;
            string URI = Constant.BaseURL+"userrightonscreen/CheckExist/" + userId+"/"+screenId;
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            result = Boolean.Parse(await response.Content.ReadAsStringAsync());
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return result;
        }
    }

   
}
