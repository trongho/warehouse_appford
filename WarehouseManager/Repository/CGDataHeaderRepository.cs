﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WarehouseManager.Models;

namespace WarehouseManager.Repository
{
    class CGDataHeaderRepository
    {
        public CGDataHeaderRepository()
        {
        }

        public async Task<long> Create(CGDataHeader entry)
        {
            long result = -1;
            string URI = "http://www.wmservice.hp.com:6379/api/cgdataheader/post";

            using (var client = new HttpClient())
            {
                var content = new StringContent(JsonConvert.SerializeObject(entry), Encoding.UTF8, "application/json");
                try
                {
                    using (var response = await client.PostAsync(URI, content))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            result = 1;
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return result;
        }

        public async Task<List<CGDataHeader>> GetAll()
        {
            string URI = "http://www.wmservice.hp.com:6379/api/cgdataheader";
            List<CGDataHeader> entrys = new List<CGDataHeader>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<CGDataHeader[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }

        public async Task<CGDataHeader> GetUnderID(String id)
        {
            string URI = "http://www.wmservice.hp.com:6379/api/cgdataheader/GetByID/" + id;
            List<CGDataHeader> entrys = new List<CGDataHeader>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<CGDataHeader[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys[0];
        }

        public async Task<long> Update(CGDataHeader entry, String id)
        {
            long result = -1;
            string URI = "http://www.wmservice.hp.com:6379/api/cgdataheader/Put/" + id;
            using (var client = new HttpClient())
            {
                using (var response = await client.PutAsJsonAsync(URI, entry))
                {

                    if (response.IsSuccessStatusCode)
                    {
                        result = 1;
                    }
                }
            }
            return result;
        }

        public async Task<long> Delete(String id)
        {
            long result = -1;
            string URI = "http://www.wmservice.hp.com:6379/api/cgdataheader/Delete/" + id;
            using (var client = new HttpClient())
            {
                using (var response = await client.DeleteAsync(URI))
                {


                    if (response.IsSuccessStatusCode)
                    {
                        result = 1;
                    }
                }
            }
            return result;
        }

        public async Task<Boolean> checkExistAsync(String id)
        {
            Boolean result = false;
            string URI = "http://www.wmservice.hp.com:6379/api/cgdataheader/CheckExist/" + id;
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            result = Boolean.Parse(await response.Content.ReadAsStringAsync());
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return result;
        }

        public async Task<String> GetLastWRDNumber()
        {
            string URI = "http://www.wmservice.hp.com:6379/api/cgdataheader/lastWRDNumber";
            String lastWRDNumber = "";
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            lastWRDNumber = fileJsonString;
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return lastWRDNumber;
        }

        public async Task<List<CGDataHeader>> GetUnderDate(DateTime fromDate, DateTime toDate)
        {
            String fromDateString = fromDate.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
            String toDateString = toDate.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
            string URI = "http://www.wmservice.hp.com:6379/api/cgdataheader/FilterByDate/" + fromDateString + "/" + toDateString;
            List<CGDataHeader> entrys = new List<CGDataHeader>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<CGDataHeader[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }

        public async Task<List<CGDataHeader>> GetUnderBranch(String branchID)
        {
            string URI = "http://www.wmservice.hp.com:6379/api/cgdataheader/FilterByBranch/" + branchID;
            List<CGDataHeader> entrys = new List<CGDataHeader>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<CGDataHeader[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }

        public async Task<List<CGDataHeader>> GetUnderHandlingStatus(String handlingStatusID)
        {
            string URI = "http://www.wmservice.hp.com:6379/api/cgdataheader/FilterByHandlingStatus/" + handlingStatusID;
            List<CGDataHeader> entrys = new List<CGDataHeader>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<CGDataHeader[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }

        public async Task<List<CGDataHeader>> GetUnderOrderStatus(String orderStatusID)
        {
            string URI = "http://www.wmservice.hp.com:6379/api/cgdataheader/FilterByOrderStatus/" + orderStatusID;
            List<CGDataHeader> entrys = new List<CGDataHeader>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<CGDataHeader[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }
    }
}
