﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WarehouseManager.Helpers;
using WarehouseManager.Models;

namespace WarehouseManager.Repository
{
    class TSDetailRepository
    {
        public TSDetailRepository()
        {
        }

        public async Task<long> Create(TSDetail entry)
        {
            long result = -1;
            string URI = Constant.BaseURL+"tsdetail/post";

            using (var client = new HttpClient())
            {
                var content = new StringContent(JsonConvert.SerializeObject(entry), Encoding.UTF8, "application/json");
                try
                {
                    using (var response = await client.PostAsync(URI, content))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            result = 1;
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return result;
        }

        public async Task<List<TSDetail>> GetUnderID(String id)
        {
            string URI = Constant.BaseURL+"tsdetail/GetByID/" + id;
            List<TSDetail> entrys = new List<TSDetail>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<TSDetail[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }

        public async Task<List<TSDetail>> GetUnderMultiID(String tsNumber,String goodsID,int ordinal)
        {
            string URI = Constant.BaseURL+"tsdetail/GetByMultiID/" + tsNumber + "/" + goodsID + "/" + ordinal;
            List<TSDetail> entrys = new List<TSDetail>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<TSDetail[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }

        public async Task<long> Update(TSDetail entry, String tsNumber, String goodsID, int ordinal)
        {
            long result = -1;
            string URI = Constant.BaseURL+"tsdetail/Put/" + tsNumber + "/" + goodsID + "/" + ordinal;
            using (var client = new HttpClient())
            {
                using (var response = await client.PutAsJsonAsync(URI, entry))
                {

                    if (response.IsSuccessStatusCode)
                    {
                        result = 1;
                    }
                }
            }
            return result;
        }

        public async Task<long> Delete(String id)
        {
            long result = -1;
            string URI = Constant.BaseURL+"tsdetail/Delete/" + id;
            using (var client = new HttpClient())
            {
                using (var response = await client.DeleteAsync(URI))
                {


                    if (response.IsSuccessStatusCode)
                    {
                        result = 1;
                    }
                }
            }
            return result;
        }
    }
}
