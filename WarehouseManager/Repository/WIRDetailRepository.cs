﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WarehouseManager.Helpers;
using WarehouseManager.Models;

namespace WarehouseManager.Repository
{
    class WIRDetailRepository
    {
        public WIRDetailRepository()
        {
        }
        public async Task<long> Create(WIRDetail entry)
        {
            long result = -1;
            string URI = Constant.BaseURL+"wirdetail/post";

            using (var client = new HttpClient())
            {
                var content = new StringContent(JsonConvert.SerializeObject(entry), Encoding.UTF8, "application/json");
                try
                {
                    using (var response = await client.PostAsync(URI, content))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            result = 1;
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return result;
        }

        public async Task<List<WIRDetail>> GetAll()
        {
            string URI = Constant.BaseURL+"wirdetail";
            List<WIRDetail> entrys = new List<WIRDetail>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<WIRDetail[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }

        public async Task<List<WIRDetail>> GetUnderID(String id)
        {
            string URI = Constant.BaseURL+"wirdetail/GetByID/" + id;
            List<WIRDetail> entrys = new List<WIRDetail>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<WIRDetail[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }

        public async Task<List<WIRDetail>> GetUnderMultiID(String wIRNumber, String goodsID, int ordinal)
        {
            string URI = Constant.BaseURL+"wirdetail/GetByMultiID/" + wIRNumber + "/" + goodsID + "/" + ordinal;
            List<WIRDetail> entrys = new List<WIRDetail>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<WIRDetail[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }

        public async Task<List<WIRDetail>> GetUnderMultiID2(String wIRNumber, String goodsID, int ordinal,String goodsGroupID)
        {
            string URI = Constant.BaseURL + "wirdetail/GetByMultiID2/" + wIRNumber + "/" + goodsID + "/" + ordinal + "/" + goodsGroupID;
            List<WIRDetail> entrys = new List<WIRDetail>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<WIRDetail[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }

        public async Task<List<WIRDetail>> GetUnderPickerName(String wIRNumber, String pickerName)
        {
            string URI = Constant.BaseURL + "wirdetail/GetByPickerName/" + wIRNumber + "/" +pickerName;
            List<WIRDetail> entrys = new List<WIRDetail>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<WIRDetail[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }

        public async Task<List<WIRDetail>> GetUnderGoodsGroupID(String wIRNumber, String goodsGroupID)
        {
            string URI = Constant.BaseURL + "wirdetail/GetByGoodsGroupID/" + wIRNumber + "/" +goodsGroupID;
            List<WIRDetail> entrys = new List<WIRDetail>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<WIRDetail[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }

        public async Task<long> Update(WIRDetail entry, String wIRNumber, String goodsID, int ordinal,String goodsGroupID)
        {
            long result = -1;
            string URI = Constant.BaseURL+"wirdetail/Put/" + wIRNumber + "/" + goodsID + "/" + ordinal + "/" +goodsGroupID;
            using (var client = new HttpClient())
            {
                using (var response = await client.PutAsJsonAsync(URI, entry))
                {

                    if (response.IsSuccessStatusCode)
                    {
                        result = 1;
                    }
                }
            }
            return result;
        }

        public async Task<long> Delete(String id)
        {
            long result = -1;
            string URI = Constant.BaseURL+"wirdetail/Delete/" + id;
            using (var client = new HttpClient())
            {
                using (var response = await client.DeleteAsync(URI))
                {


                    if (response.IsSuccessStatusCode)
                    {
                        result = 1;
                    }
                }
            }
            return result;
        }

        public async Task<Boolean> checkExistAsync(String id)
        {
            Boolean result = false;
            string URI = Constant.BaseURL+"wirdetail/CheckExist/" + id;
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            result = Boolean.Parse(await response.Content.ReadAsStringAsync());
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return result;
        }

        public async Task<String> GetLastWIRNumber()
        {
            string URI = Constant.BaseURL+"wirdetail/lastWIRNumber";
            String lastWIRNumber = "";
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            lastWIRNumber = fileJsonString;
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return lastWIRNumber;
        }
    }
}
