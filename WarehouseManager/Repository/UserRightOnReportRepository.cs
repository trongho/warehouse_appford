﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WarehouseManager.Helpers;
using WarehouseManager.Models;

namespace WarehouseManager.Repository
{
    class UserRightOnReportRepository
    {
        public UserRightOnReportRepository()
        {
        }

        public async Task<long> createUserRightOnReportAsync(UserRightOnReport userRightOnReport)
        {
            long result = -1;
            string URI = Constant.BaseURL+"userrightonreport/PostUserRightOnReport/";

            using (var client = new HttpClient())
            {
                var content = new StringContent(JsonConvert.SerializeObject(userRightOnReport), Encoding.UTF8, "application/json");
                try
                {
                    using (var response = await client.PostAsync(URI, content))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            result = 1;
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return result;
        }

        public async Task<List<UserRightOnReport>> getUserRightOnReportAsync(String userID, String reportID)
        {
            string URI = Constant.BaseURL+"userrightonreport/GetByID/" + userID + "/" + reportID;
            List<UserRightOnReport> entrys = new List<UserRightOnReport>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<UserRightOnReport[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }

        public async Task<List<UserRightOnReport>> getUserRightOnReportAsync(String userID)
        {
            string URI = Constant.BaseURL+"userrightonreport/GetByUserID/" + userID;
            List<UserRightOnReport> entrys = new List<UserRightOnReport>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<UserRightOnReport[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }


        public async Task<long> updateUserRightOnReport(UserRightOnReport userRightOnReport, String userId, String reportId)
        {
            long result = -1;
            string URI = Constant.BaseURL+"userrightonreport/PutUserRightOnReport/" + userId + "/" + reportId;
            using (var client = new HttpClient())
            {
                using (var response = await client.PutAsJsonAsync(URI, userRightOnReport))
                {

                    if (response.IsSuccessStatusCode)
                    {
                        result = 1;
                    }
                }
            }
            return result;
        }

        public async Task<long> DeleteUserRightOnReportAsync(String userId, String reportId)
        {
            long result = -1;
            string URI = Constant.BaseURL+"userrightonreport/DeleteUserRightOnReport/" + userId + "/" + reportId;
            using (var client = new HttpClient())
            {
                using (var response = await client.DeleteAsync(URI))
                {


                    if (response.IsSuccessStatusCode)
                    {
                        result = 1;
                    }
                }
            }
            return result;
        }

        public async Task<Boolean> checkExistAsync(String userId, String reportId)
        {
            Boolean result = false;
            string URI = Constant.BaseURL+"userrightonreport/CheckExist/" + userId + "/" + reportId;
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            result = Boolean.Parse(await response.Content.ReadAsStringAsync());
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return result;
        }
    }
}
