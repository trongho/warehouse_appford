﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WarehouseManager.Helpers;
using WarehouseManager.Models;

namespace WarehouseManager.Repository
{
    class UserRightRepository
    {
        public UserRightRepository()
        {
        }

        public async Task<long> createUserRightAsync(UserRight userRight)
        {
            long result = -1;
            string URI = Constant.BaseURL+"userright/PostUserRight/";

            using (var client = new HttpClient())
            {
                //client.DefaultRequestHeaders.ExpectContinue = false;
                var content = new StringContent(JsonConvert.SerializeObject(userRight), Encoding.UTF8, "application/json");
                try
                {
                    using (var response = await client.PostAsync(URI, content))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            result = 1;
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return result;
        }


        public async Task<List<UserRight>> getUserRightAsync(String userID,String rightID)
        {
            string URI = Constant.BaseURL+"userright/GetByUserRightID/" + userID+"/"+rightID;
            List<UserRight> userRights = new List<UserRight>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            userRights = JsonConvert.DeserializeObject<UserRight[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return userRights;
        }

        public async Task<List<UserRight>> getUserRightByUserIDAsync(String userID)
        {
            string URI = Constant.BaseURL+"userright/GetByUserID/" + userID;
            List<UserRight> entrys = new List<UserRight>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<UserRight[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }

        public async Task<long> updateUserRight(UserRight userRight, String userId, String rightId)
        {
            long result = -1;
            string URI = Constant.BaseURL+"userright/PutUserRight/" + userId + "/" + rightId;
            using (var client = new HttpClient())
            {
                using (var response = await client.PutAsJsonAsync(URI, userRight))
                {

                    if (response.IsSuccessStatusCode)
                    {
                        result = 1;
                    }
                }
            }
            return result;
        }

        public async Task<long> DeleteDataAsync(String userId, String rightId)
        {
            long result = -1;
            string URI = Constant.BaseURL+"userright/DeleteUserRight/" + userId + "/" + rightId;
            using (var client = new HttpClient())
            {
                using (var response = await client.DeleteAsync(URI))
                {


                    if (response.IsSuccessStatusCode)
                    {
                        result = 1;
                    }
                }
            }
            return result;
        }
    }
}
