﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WarehouseManager.Helpers;
using WarehouseManager.Models;

namespace WarehouseManager.Repository
{
    class OrderStatusRepository
    {
        public OrderStatusRepository()
        {
        }

        public async Task<List<OrderStatus>> GetAll()
        {
            string URI = Constant.BaseURL+"orderstatus";
            List<OrderStatus> entrys = new List<OrderStatus>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<OrderStatus[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }
    }
}
