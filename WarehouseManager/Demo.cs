﻿using QRCoder;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WarehouseManager
{
    public partial class Demo : Form
    {
        public Demo()
        {
            InitializeComponent();
            QRCodeGeneral("21000-00004");
        }

        private void QRCodeGeneral(String code)
        {
            
            QRCodeGenerator.ECCLevel eccLevel = QRCodeGenerator.ECCLevel.Q;
            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeData qrCodeData = qrGenerator.CreateQrCode(code, eccLevel);
            QRCode qrCode = new QRCode(qrCodeData);
            Bitmap qrCodeBitmap = qrCode.GetGraphic(2,Color.Black,Color.White,new Bitmap(10,10));
            pictureBox1.Image = qrCodeBitmap;
           

            //QRCodeGenerator.ECCLevel eccLevel = QRCodeGenerator.ECCLevel.L;
            //using (QRCodeGenerator qrGenerator = new QRCodeGenerator())
            //{
            //    using (QRCodeData qrCodeData = qrGenerator.CreateQrCode(code, eccLevel))
            //    {
            //        using (QRCode qrCode = new QRCode(qrCodeData))
            //        {

            //            pictureBox1.BackgroundImage = qrCode.GetGraphic(20);

            //            this.pictureBox1.Size = new System.Drawing.Size(pictureBox1.Width, pictureBox1.Height);
            //            //Set the SizeMode to center the image.
            //            this.pictureBox1.SizeMode = PictureBoxSizeMode.CenterImage;

            //            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            //        }
            //    }
            //}
        }

    }
}
